<!DOCTYPE html>

<?php

    if(!isset($_titulo)){
        $_titulo = "Trilhas de Auditoria";
    }
 
?>

<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link href="../../fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../master/master.css" rel="stylesheet" type="text/css"/>
        
        
        <script src="../../js/jquery-1.12.3.js" type="text/javascript"></script>
        <script src="../../js/bootstrap.js" type="text/javascript"></script>
        <script src="../../js/jquery-ui.js" type="text/javascript"></script>
        <script src="../../master/master.js" type="text/javascript"></script>
        
        <link rel="shortcut icon" href="../../images/favicon.ico">

        <title><?php echo $_titulo; ?></title>
        

    </head>
    <body>
        <div id="master_div">
        <div class="container-fluid" style="background-color: #222;">
            <div class="container">
                <nav class="navbar navbar-inverse" style="margin-bottom: 0px; border-color: #222;">
                    <div class="container-fluid">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                            
                        </div>
                        <!-- andre - mudar a cor do menu-->
                        <div class="collapse navbar-collapse" id="myNavbar">
                          <ul class="nav navbar-nav">
                              
                            <?php
                                if(!isset($_SESSION)){
                                    session_start();
                                }
                                //adnre - ver o tratamento da sessao
                               @ $perfil = $_SESSION['sessao_perfil'];
                                
                                if(isset($_SESSION['sessao_id'])){   
                                    require_once('../../obj/conexao.php');
                                    $con = new conexao();

                                    $ativo = "Home";

                                    $sql = "Select * From menu Where exibir = true Order By posicao, cod_menu";

                                    $query = $con->select($sql);
                                    if($con->erro != ""){
                                        echo $con->erro;
                                    }else{

                                        while ($row = mysqli_fetch_array($query)){

                                            if($row['nome'] == $ativo){
                                                $classe = 'active';
                                            }else{
                                                $classe = '';
                                            }
                                            $sql = "Select * From sub_menu Where exibir = true And cod_menu = ".$row['cod_menu']." Order By posicao, cod_sub_menu";
                                            $query2 = $con->select($sql);
                                            if($con->erro != ""){
                                                echo $con->erro;
                                            }else{
                                                if($con->num_rows > 0){

                                                    if($row['nome']  == 'Usuários'){

                                                        if( @$_SESSION['cpf_sgi'] !== ""){

                                                            echo ''.
                                                            '<li class="dropdown '.$classe.'">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" href="'.$row['destino'].'" title="'.$row['descricao'].'">'.$row['icone']." ".$row['nome'].' <span class="caret"></span></a>
                                                                <ul class="dropdown-menu">';
        
                                                            while ($row2 = mysqli_fetch_array($query2)){
                                                                
                                                                if(strpos($row2['PerfisPermitidos'], $perfil) === false){
                                                                    echo '<li style="margin-left: 20px; margin-bottom: 3px; color: silver;">'.$row2['icone']." ".$row2['nome'].'</li>'; 
                                                                    //echo '<li><a href="'.$row2['destino'].'" title="'.$row2['descricao'].'">'.$row2['icone']." ".$row2['nome'].'</a></li>';
                                                                }else{
                                                                    echo '<li><a href="'.$row2['destino'].'" title="'.$row2['descricao'].'">'.$row2['icone']." ".$row2['nome'].'</a></li>'; 
                                                                }
                                                                
                                                            }     
        
                                                            echo ''.
                                                                    '</ul>
                                                                </li>';
                                                            
                                                        
                                                        } 
                                                    //SGTA-143 - desabilitar campo "Relatórios" para analistas e supervisores
                                                    } elseif($row['nome']  == 'Relatórios') {

                                                        if($perfil  < 5) {


                                                            echo ''.
                                                            '<li class="dropdown '.$classe.'">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" href="'.$row['destino'].'" title="'.$row['descricao'].'">'.$row['icone']." ".$row['nome'].' <span class="caret"></span></a>
                                                                <ul class="dropdown-menu">';
        
                                                            while ($row2 = mysqli_fetch_array($query2)){
                                                                
                                                                if(strpos($row2['PerfisPermitidos'], $perfil) === false){
                                                                    echo '<li style="margin-left: 20px; margin-bottom: 3px; color: silver;">'.$row2['icone']." ".$row2['nome'].'</li>'; 
                                                                    //echo '<li><a href="'.$row2['destino'].'" title="'.$row2['descricao'].'">'.$row2['icone']." ".$row2['nome'].'</a></li>';
                                                                }else{
                                                                    echo '<li><a href="'.$row2['destino'].'" title="'.$row2['descricao'].'">'.$row2['icone']." ".$row2['nome'].'</a></li>'; 
                                                                }
                                                                
                                                            }     
        
                                                            echo ''.
                                                                    '</ul>
                                                                </li>';

                                                        }
                                                       
                                                    } else {

                                                        echo ''.
                                                        '<li class="dropdown '.$classe.'">
                                                            <a class="dropdown-toggle" data-toggle="dropdown" href="'.$row['destino'].'" title="'.$row['descricao'].'">'.$row['icone']." ".$row['nome'].' <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">';
    
                                                        while ($row2 = mysqli_fetch_array($query2)){
                                                            
                                                            if(strpos($row2['PerfisPermitidos'], $perfil) === false){
                                                                echo '<li style="margin-left: 20px; margin-bottom: 3px; color: silver;">'.$row2['icone']." ".$row2['nome'].'</li>'; 
                                                                //echo '<li><a href="'.$row2['destino'].'" title="'.$row2['descricao'].'">'.$row2['icone']." ".$row2['nome'].'</a></li>';
                                                            }else{
                                                                echo '<li><a href="'.$row2['destino'].'" title="'.$row2['descricao'].'">'.$row2['icone']." ".$row2['nome'].'</a></li>'; 
                                                            }
                                                            
                                                        }     
    
                                                        echo ''.
                                                                '</ul>
                                                            </li>';
                                                        
                                                    }

                                                  
                                                }else{

                                                    echo '<li class="'.$classe.'"><a href="'.$row['destino'].'" title="'.$row['descricao'].'">'.$row['icone']." ".$row['nome'].'</a></li>';

                                                } 
                                            }
                                        }
                                    }
                                    
                                    echo  
                                    '</ul>
                                    <ul class="nav navbar-nav navbar-right">
                                      <li><a href="http://helpdesk.tce.sc.gov.br" target ="_blank"><span class="glyphicon glyphicon-wrench"></span>&nbsp; Suporte</a></li>
                                     <!-- andre <li><a href="../login/login.php?logout=1"><span class="glyphicon glyphicon-log-out"></span>&nbsp; Logout</a></li>-->
                                    </ul>';
                                }else{
                                    echo  
                                    '</ul>
                                    <ul class="nav navbar-nav navbar-right">
                                      <li><a href="http://helpdesk.tce.sc.gov.br" target ="_blank"><span class="glyphicon glyphicon-wrench"></span>&nbsp; Suporte</a></li>
                                      <!-- andre  <li><a href="../login/login.php"><span class="glyphicon glyphicon-log-in"></span>&nbsp; Login</a></li>-->
                                    </ul>';
                                }

                            ?>
                                    
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        
        <style>
            .contentLogoCGE {
                margin-top: 13px;
                float: right;
                /* margin-right: 20px; */
            }
            .cgeLogo {
                color: #E84902;
                font-size: 25px;
                font-weight: bold;
                border-right: 2px solid #E84902;
                float: left;
                height: 30px;
                margin: 0px;
                padding-right: 10px;
                margin-right: 10px;
                text-shadow: 1px 1px #000;
                box-shadow: 1px 0px #000;
            }
            .controladoriGeralDoEstado {
                float: left;
                width: 145px;
                font-weight: bold;
                font-size: 14px;
            }
        </style>
        <!-- http://www.tce.sc.gov.br/sites/all/themes/tce/media/css/img/brasao.jpg -->
        <?php 
            echo ' <div class="container-fluid"  style="background-image: url(http://'.$_SERVER['HTTP_HOST'] .'/trilhas/images/fundo_intra1.jpg); background-repeat: no-repeat; background-size: auto;">' 
        ?>
       
            <div class="container" style="color: white;">
                
                <div class="row">
                    <div class="col-sm-6">
                        <br/>
                        
                         <div style="margin-top: -14px;"><img src="../../images/logo_cge.png" alt="TRIBUNAL DE CONTAS DE SANTA CATARINA" style="max-width: 120px;"/></div>
                      
                         <div style="font-size: 130%; color: black; font-weight: bold; margin: 10px 0px -7px 0px;">Sistema de Gestão de Trilhas de Auditoria - SGTA</div>
                        <div style="font-size: 85%; color: black; font-weight: bold;">Auditorias Realizadas por Meio de Cruzamento de Bases de Dados.</div>
                       
                    </div>
                    <div class="col-sm-3 text-right">
                        <img src="../../images/logo_cge_mini.png" alt="Sistema cedido pela CGE-GO" style="margin-top: 10px; max-width: 190px;"/>
                    </div>
                    <div class="col-sm-3 text-right">
                           <img src="../../images/odp_transparent.png" alt="Sistema cedido pela CGE-GO" style="margin-top: 0px; max-width: 212px;"/>   
                    </div>
                </div>
                
                        
            </div>
        </div>
        
        <!--<div class="container"> 
            <input type="button" value="Erro" onclick='$("#popup_erro").dialog("open");' class="btn btn-success"/>
            <input type="button" value="Alerta" onclick='$("#popup_alerta").dialog("open");' class="btn btn-default"/>
            <input type="button" value="Mensagem" onclick='$("#popup_mensagem").dialog("open");' class="btn btn-primary"/>
            <input type="button" value="Diálogo" onclick='$("#popup_dialogo").dialog("open");' class="btn btn-info"/>
            <input type="button" value="Pergunta" onclick='$("#popup_pergunta").dialog("open");' class="btn btn-warning"/>
            <input type="button" value="Modal" onclick='$("#popup_modal").dialog("open");' class="btn btn-danger"/>
            <input type="button" value="Sucesso" onclick='$("#popup_sucesso").dialog("open");' class="btn btn-info"/>
        </div>
        <br><br>-->
        
        <div class="navbar navbar-fixed-bottom" role="navigation" style=" min-height: 10px !important;">
            <div class="container-fluid" style="background-color: #222; color: white; padding: 10px 0px 7px 0px;">
                <div class="container">
                    <div class="row" style="color: white; font-size: 70%; text-transform: uppercase;">
                        <div class="col-sm-3">
                            <?php
                 
                                require_once '../../code/funcoes.php';
                                echo data_extenso();
                            ?>
                        </div>
                        <div class="col-sm-3 text-center">
                            <?php 
                                //andre - verificar os registros de sessao aqui tb
                           
                                echo @$_SESSION['sessao_nome']; 
                            ?>
                        </div>
                        <div class="col-sm-3 text-center">
                            <?php echo @$_SESSION['sessao_nome_orgao']; ?>
                        </div>
                        <div class="col-sm-3 text-right">
                            <?php echo @$_SESSION['sessao_nome_perfil']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div style="min-height: 300px; padding-bottom: 30px;">
            <?php
               
                echo $pagemaincontent;
          
            ?>
        </div>
        

 
            
        
        <div id="popup_sucesso" title="Mensagem" style="display:none;">
            <div class="ui-widget">
                <div class="alert alert-success progress-bar-striped ui-corner-all" style="padding: 0 .7em; max-width: 600px; min-width: 250px; min-height: 70px; font-weight: bold; margin-bottom: 0px;">
                    <p><div id="msg_sucesso">Mensagem de Sucesso.</div></p>
                </div>
            </div>
        </div>
        
        <div id="popup_erro" title="Erro" style="display:none;">
            <div class="ui-widget">
                <div class="alert alert-danger progress-bar-striped ui-corner-all" style="padding: 0 .7em; min-height: 70px; min-width: 250px; font-weight: bold; margin-bottom: 0px;">
                    <p><div id="msg_erro">Mensagem de Erro.</div></p>
                </div>
            </div>
        </div>

        <div id="popup_alerta" title="Aviso" style="display:none;">
            <div class="ui-widget">
                <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em; min-height: 70px; min-width: 250px; font-weight: bold;">
                    <p><div id="msg_alerta">Mensagem de Alerta.</div></p>
                </div>
            </div>
        </div>
        
        <div id="popup_mensagem" title="Mensagem" style="display:none; min-height: 70px; min-width: 250px;">
            <p id="msg_texto">Alguma Mensagem Aqui.</p>
        </div>

        <div id="popup_dialogo" title="Pergunta" style="display:none; min-height: 70px; font-weight: bold;">
            <p id="msg_dialogo">As informações estão corretas?</p>
            <hr>
            <div style="text-align: right;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-danger" onclick='$("#popup_dialogo").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
                <button type="button" class="btn btn-success" onclick='submit_form(destino);'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
            </div>
        </div>

        <div id="popup_pergunta" title="Pergunta" style="display:none; min-height: 70px; font-weight: bold;">
            <p id="msg_pergunta">Alguma Pergunta Aqui.</p>
            <hr>
            <div style="text-align: right;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-danger" onclick='$("#popup_pergunta").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
                <button type="button" class="btn btn-success" onclick='pergunta_ok();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
            </div>
        </div>

        <div id="popup_modal" title="Mensagem" style="display:none; min-height: 70px; font-weight: bold;">
            <p id="modal_msg">Algum Modal Aqui.</p>
        </div>

        <div id="loading">
        </div>

        <div id="imgloader" style="display:none; width: 60px; height: 60px; color: darkslateblue;"><i  class="fa fa-cog fa-spin fa-3x fa-fw" ></i></div>
    </div>    
    </body>
</html>
