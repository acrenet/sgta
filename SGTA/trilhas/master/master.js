var formdata;

function submit_form(destino){
    $("#popup_dialogo").dialog("close");
    $("#loading").show();
    $("#imgloader").show();
    if(typeof tempopassado != "undefined"){
        tempopassado = 0;
    }
    $.ajax({
        type: 'post',
        url: destino,
        data: formdata,
        cache: false,
        timeout: 90000,
        success: function(data){
            try{
                try{
                    data = $.parseJSON(data);
                }catch(err){
                    $("#msg_erro").html("ERRO AO DECODIFICAR O PACOTE RECEBIDO DO SERVIDOR: ->" + data + "<-");
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.noticia !== ""){
                    alert("Notícia: " + data.noticia);
                    return false;
                }
                if(data.erro !== ""){
                    //alert(JSON.stringify(data));
                    $("#msg_erro").html(data.erro);
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.alerta !== ""){
                    $("#msg_alerta").html(data.alerta);
                    $("#popup_alerta").dialog("open");
                    return false;
                }
                
                resposta(data);
                    
            }
            catch(err){
                $("#msg_erro").html(err + "   " + data);
                $("#popup_erro").dialog("open");
                return false;
            } 
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            $("#msg_erro").html(errorThrown);
            $("#popup_erro").dialog("open");
            return false;
        },
        complete: function(XMLHttpRequest, status){
            $("#loading").hide();
            $("#imgloader").hide();
            return false;
        }
    });
};

function submit_file(destino){
    $("#popup_dialogo").dialog("close");
    $("#loading").show();
    $("#imgloader").show();
    if(typeof tempopassado != "undefined"){
        tempopassado = 0;
    }
    $.ajax({
        type: 'post',
        url: destino,
        data: formdata,
        cache: false,
        timeout: 120000,
        contentType: false,
        processData: false,
        success: function(data){
            try{
                try{
                    data = $.parseJSON(data);
                }catch(err){
                    $("#msg_erro").html("ERRO AO DECODIFICAR O PACOTE RECEBIDO DO SERVIDOR: ->" + data + "<-");
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.noticia !== ""){
                    alert("Notícia: " + data.noticia);
                    return false;
                }
                if(data.erro !== ""){
                    $("#msg_erro").html(data.erro);
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.alerta !== ""){
                    $("#msg_alerta").html(data.alerta);
                    $("#popup_alerta").dialog("open");
                    return false;
                }
                
                resposta_up(data);
                    
            }
            catch(err){
                $("#msg_erro").html(err + "   " + data);
                $("#popup_erro").dialog("open");
                return false;
            } 
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            $("#msg_erro").html(errorThrown);
            $("#popup_erro").dialog("open");
            return false;
        },
        complete: function(XMLHttpRequest, status){
            $("#loading").hide();
            $("#imgloader").hide();
            return false;
        }
    });
};

/* ++++++++++++++++++++++++Popup Sucesso++++++++++++++++++++++++ */
$(function() {
    $("#popup_sucesso").dialog({
        autoOpen: false,
        modal: true,
        width: 'auto',
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        buttons: {
            'Ok': function() {
                $(this).dialog("close");
                execute();
            }
        },
        close: function( event, ui ) {
            execute();
        }
    });
});

/* ++++++++++++++++++++++++Popup Sucesso++++++++++++++++++++++++ */
$(function() {
    $("#popup_sucesso_continuo").dialog({
        autoOpen: false,
        modal: true,
        width: 'auto',
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        buttons: {
            'Ok': function() {
                $(this).dialog("close");
                execute_continuo();
            }
        },
        close: function( event, ui ) {
            execute_continuo();
        }
    });
});


/* ++++++++++++++++++++++++Popup Erro++++++++++++++++++++++++ */
$(function() {
    $("#popup_erro").dialog({
        autoOpen: false,
        modal: true,
        width: 'auto',
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        buttons: {
            'Ok': function() {
                $(this).dialog("close");
            }
        }
    });
});           


/* ++++++++++++++++++++++++Popup Alerta++++++++++++++++++++++++ */
$(function() {
    $("#popup_alerta").dialog({
        autoOpen: false,
        modal: true,
        width: 'auto',
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        buttons: {
            'Ok': function() {
                $(this).dialog("close");
            }
        }
    });
});                  



/* ++++++++++++++++++++++++Popup Mensagem++++++++++++++++++++++++ */
$(function() {
    $("#popup_mensagem").dialog({
        autoOpen: false,
        width: 'auto',
        maxWidth: 800,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        buttons: {
            'Ok': function() {
                $(this).dialog("close");
                execute();
            }
        },
        close: function( event, ui ) {
            execute();
        }
    });
});

/* ++++++++++++++++++++++++Popup Mensagem Trilhas Continuas++++++++++++++++++++++++ */
$(function() {
    $("#popup_mensagem_continua").dialog({
        autoOpen: false,
        width: 'auto',
        maxWidth: 800,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        buttons: {
            'Ok': function() {
                $(this).dialog("close");
                execute_continuo();
            }
        },
        close: function( event, ui ) {
            execute_continuo();
        }
    });
});


/* ++++++++++++++++++++++++Popup Diálogo++++++++++++++++++++++++ */
$(function() {
    $("#popup_dialogo").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
});

/* ++++++++++++++++++++++++Popup Pergunta++++++++++++++++++++++++ */
$(function() {
    $("#popup_pergunta").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
});

/* ++++++++++++++++++++++++Popup Modal++++++++++++++++++++++++ */
$(function() {
    $("#popup_modal").dialog({
        autoOpen: false,
        modal: true,
        width: 'auto',
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        buttons: {
            'Ok': function() {
                modal_ok();
                $(this).dialog("close");
            }
        }
    });
    $("#popup_modal").on( "dialogclose", function( event, ui ) {
       modal_ok();
    } );
});

/* +++++++++++++++++++++++++++++Funções Úteis++++++++++++++++++++++++ */

function validadata(data){
    var dia = data.substring(0,2);
    var mes = data.substring(3,5);
    var ano = data.substring(6,10);
 
    //Criando um objeto Date usando os valores ano, mes e dia.
    var novaData = new Date(ano,(mes-1),dia);
 
    var mesmoDia = parseInt(dia,10) == parseInt(novaData.getDate());
    var mesmoMes = parseInt(mes,10) == parseInt(novaData.getMonth())+1;
    var mesmoAno = parseInt(ano) == parseInt(novaData.getFullYear());
 
    if (!((mesmoDia) && (mesmoMes) && (mesmoAno)))
    {
        return false;
    }  
    return true;
}

function validaemail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function databr(data){
    var dia = data.substring(8,2);
    var mes = data.substring(5,2);
    var ano = data.substring(0,4);
    return dia + "/" + mes + "/" + ano;
}

function compara_datas(data_menor, data_maior){
    var nova_data_menor = parseInt(data_menor.split("/")[2].toString() + data_menor.split("/")[1].toString() + data_menor.split("/")[0].toString());
    var nova_data_maior = parseInt(data_maior.split("/")[2].toString() + data_maior.split("/")[1].toString() + data_maior.split("/")[0].toString());
    
    if( nova_data_menor > nova_data_maior){
        return false;
    }else{
        return true;
    }
}

function maiuscula(campo) {
    try {
        var texto;
        texto = document.getElementById(campo).value;
        texto = texto.toUpperCase();
        document.getElementById(campo).value = texto;
    }
    catch (e) {
        alert(e);
    }
}


function minuscula(campo){
    try{
        var texto = document.getElementById(campo).value;
        texto = texto.toLowerCase();
        document.getElementById(campo).value = texto;
    }
    catch (e) {
        alert(e);
    }
}

function isNumeric(valor, alerta) {
    try {

        if (isNaN(valor)) {
            if (alerta === "sim") {
                alert("Valor não numérico");
            }
            valor.value = "";
            return false;
        }
        else {
            return true;
        }
    }
    catch (e) {
        alert("Isnumeric: " + e);
        return false;
    }
}

function validarCPF(cpf) {  
    cpf = cpf.replace(/[^\d]+/g,'');    
    if(cpf == '') return false; 
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 || 
        cpf == "00000000000" || 
        cpf == "11111111111" || 
        cpf == "22222222222" || 
        cpf == "33333333333" || 
        cpf == "44444444444" || 
        cpf == "55555555555" || 
        cpf == "66666666666" || 
        cpf == "77777777777" || 
        cpf == "88888888888" || 
        cpf == "99999999999")
            return false;       
    // Valida 1o digito 
    add = 0;    
    for (i=0; i < 9; i ++)       
        add += parseInt(cpf.charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(cpf.charAt(9)))     
            return false;       
    // Valida 2o digito 
    add = 0;    
    for (i = 0; i < 10; i ++)        
        add += parseInt(cpf.charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(cpf.charAt(10)))
        return false;       
    return true;   
}

function contar_caracteres(campoentrada, camposaida, tamanho) {
    var texto;
    var n;
    var t;
    try {
        texto = document.getElementById(campoentrada).value;
        n = texto.length;
        t = tamanho;
        document.getElementById(camposaida).value = t - n;
    }
    catch (e) {
        alert(e);
    }
}

/* +++++++++++++++++++++Responsividade dos Popups++++++++++++++++++++++++ */
// on window resize run function
$(window).resize(function () {
    fluidDialog();
});

// catch dialog if opened within a viewport smaller than the dialog width
$(document).on("dialogopen", ".ui-dialog", function (event, ui) {
    fluidDialog();
});

function fluidDialog() {
    var $visible = $(".ui-dialog:visible");
    // each open dialog
    $visible.each(function () {
        var $this = $(this);
        var dialog = $this.find(".ui-dialog-content").data("ui-dialog");
        // if fluid option == true
        if (dialog.options.fluid) {
            var wWidth = $(window).width();
            // check window width against dialog width
            if (wWidth < (parseInt(dialog.options.maxWidth) + 50))  {
                // keep dialog from filling entire screen
                $this.css("max-width", "90%");
            } else {
                // fix maxWidth bug
                $this.css("max-width", dialog.options.maxWidth + "px");
            }
            //reposition dialog
            dialog.option("position", dialog.options.position);
        }
    });

}
