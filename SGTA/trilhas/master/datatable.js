$(document).ready(function(){ 
    
    $.fn.DataTable.ext.type.search.string = function ( data ) {
        
        return ! data ?
            '' :
            typeof data === 'string' ?
                data
                    .replace( /έ/g, 'ε' )
                    .replace( /[ύϋΰ]/g, 'υ' )
                    .replace( /ό/g, 'ο' )
                    .replace( /ώ/g, 'ω' )
                    .replace( /ά/g, 'α' )
                    .replace( /[ίϊΐ]/g, 'ι' )
                    .replace( /ή/g, 'η' )
                    .replace( /\n/g, ' ' )
                    .replace( /á/g, 'a' )
                    .replace( /é/g, 'e' )
                    .replace( /í/g, 'i' )
                    .replace( /ó/g, 'o' )
                    .replace( /ú/g, 'u' )
                    .replace( /ê/g, 'e' )
                    .replace( /î/g, 'i' )
                    .replace( /ô/g, 'o' )
                    .replace( /è/g, 'e' )
                    .replace( /ï/g, 'i' )
                    .replace( /ü/g, 'u' )
                    .replace( /ã/g, 'a' )
                    .replace( /õ/g, 'o' )
                    .replace( /ç/g, 'c' )
                    .replace( /ì/g, 'i' )
                    .replace( /à/g, 'a' )
                    .replace( /À/g, 'A' )
                    .replace( /Á/g, 'A' )
                    .replace( /É/g, 'E' )
                    .replace( /Í/g, 'I' )
                    .replace( /Ó/g, 'O' )
                    .replace( /Ú/g, 'U' )
                    .replace( /Ê/g, 'E' )
                    .replace( /Î/g, 'I' )
                    .replace( /Ô/g, 'O' )
                    .replace( /È/g, 'E' )
                    .replace( /Ã/g, 'A' )
                    .replace( /Õ/g, 'O' )
                    .replace( /Ç/g, 'C' )
                    .replace( /Ì/g, 'I' ):
                data;
         
    };
    
});

function datatable_(nome, coluna, ordem, display, data, header, nomearquivo) {
    
    coluna = typeof coluna !== 'undefined' ? coluna : true;
    ordem = typeof ordem !== "undefined" ? ordem : [];
    display = typeof display !== "undefined" ? display : -1;
    data = typeof data !== "undefined" ? data : [];
    header = typeof header !== 'undefined' ? header : document.title;
    nomearquivo = typeof nomearquivo !== 'undefined' ? nomearquivo : document.title;
    
    var classificavel = coluna === true ? coluna : false;
    coluna = coluna === true ? 0 : coluna;
    
    i = data.length;
    while (i < coluna) {
        data.push({"sType": "string"});
        i++;
    }
    
    minha_tabela = $('#' + nome).DataTable( {
        select: true,
        keys: true,
        dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"lBfr>t<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',
        buttons: [
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title: header,
                filename: nomearquivo,
                footer: false,
                message: function ( dt ) {
                            var termo = $( "#" + nome + "_filter" ).find('input').val();
                            termo = termo.trim();
                            termo = termo.replace(/ /g, ", ");
                            if (termo != ""){
                                termo = "Filtros: " + termo;
                            }else{
                                termo = "Exibindo todos os registros.";
                            }
                            
                            return termo;
                          },
                customize: function ( doc ) {
                    
                    var data = new Date();
                    
                    var cols = [];

                    //document.write ("<h1> Hoje é " + dayName[now.getDay() ] + ", " + now.getDate () + " de " + monName [now.getMonth() ]   +  " de "  +     now.getFullYear () + ". </h1>")
                    
                    doc["footer"]=function(currentPage, pageCount) { 
                        
                        var dia  = data.getDate();
                        if(dia < 10){
                            dia = "0" + dia;
                        }
                        var mes = data.getMonth();
                        mes + mes + 1;
                        if(mes < 10){
                            mes = "0" + mes;
                        }
                        var hora = data.getHours();
                        if(hora < 10){
                            hora = "0" + hora;
                        }
                        var min = data.getMinutes();
                        if(min < 10){
                            min = "0" + min;
                        }
                        var seg = data.getSeconds();
                        if(seg < 10){
                            seg = "0" + seg;
                        }
                        
                        var objFooter = [
                            {text:[{ text: '.', fontSize: 8, color: 'white' },{ text: "                  ________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________", fontSize: 8}]},
                            {
                              columns: [
                                  {text:[
                                        { text: '.', fontSize: 8, color: 'white' },
                                        {
                                          text: "              " + dia + "/" + mes + "/" + data.getFullYear() + " " + hora + ":" + min + ":" + seg, alignment: 'left'
                                        }
                                    ]},
                                {
                                  text: 'Página ' + currentPage.toString() + ' de ' + pageCount, alignment: 'right', margin:[0,0,46]
                                }
                              ],
                              // optional space between columns
                              columnGap: 10
                            }
                        ];
                        
                        //cols[0] = {text: dia + "/" + mes + "/" + data.getFullYear() + " " + hora + ":" + min + ":" + seg, alignment: 'left', margin:[40] };
                        //cols[1] = {text: 'Página ' + currentPage.toString() + ' de ' + pageCount, alignment: 'right', margin:[0,0,46] };
                        //var objFooter = {};
                        //objFooter['content'] = [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }, {colums: cols},{aligment: 'center'}];
                        //objFooter['alignment'] = 'center';
                        //objFooter['columns'] = cols;
                        
                        return objFooter;
                    };
                    
                    

                    doc.content.splice( 0, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'left',
                        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN0AAAAeCAYAAACouBsAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7AAAAOwAFq1okJAAAAGXRFWHRTb2Z0d2FyZQBwYWludC5uZXQgNC4wLjEyQwRr7AAAIz5JREFUeF7tXAdck0fYz95AQoCEPRJWwk5YgkRZsnEQZQo4UFAEVOo2zuLCvWetrVaqdWtdTd1W6bK2WqpNa2tRSMJUEfG9772XEAmg1db26/f9+P9+zy8vd8/dveP+d8/z3B24XvSiF/9htIx3cdNYC+V6EQXLayNLrXTZbw3I91vt6xxdhxi0ZS2RP3BM5OlUetGL/79AWn8MbOgvWaf19K5R2wrrNDaC5hcibEbTNBqf6GpNUOyo+k8RU12xv4SG1KCSeg/BebW9sxbW3a0ta6FGbeNcre2Xu0WbPt9eV+yVQH7bO0VrI6hRvwWpdY04pY7fYK2rGjd79uxjqKgUCkWkLqkXvfjreJwfbduYE7VT6+DyDO3w4HVELRn4c31Edkz1pJNMXTV/ivqyeE5DVFB2nY/r7xrbnuvtUdyCgSYsdUd9dIkQAEDSVdcNz3/7aF5dT+X/gqjdwq9rorfb6qqGpLuMShNKujhdUi968dfwbOes5PpA0TdaR9ceO98rxc3/uaZPxprapK1GuupeitZds/o2hIlPa13FPdf1J4LOeojGL+6nxqWHBqPEI+iqNUAv6Xrxn8ezEyjhJOKaN5p1uorAt7Vu5NISoHz5DNSq3BHUlBTxi8ZeiPRYx+uKrQvQiFKu3gsqoeuqNsC/Tbpflve1zIkQrCfg8Q/QP2v4HmFnsvc3BLXn4nByGY4VJaLvZVCJNXgSuYbuFn4M55/Nh3mH56SPE1ia1nDdh/xwFkEw8/mLFclzcDxRDc4v3XPvBM+5aFJNV7ELTqtZPa+4Bn0BBulEO0mVJH1WCHqNIV2CC+kjNPoBj8fVUE1taohegxbjcApssDo0J+MC28y6JkNxqBhT7gIEOWqfLKB9xeTwaqLHbSxsTzvNLUrwP0o1aJdZEzhk5tZTCKL39dP9cP2DnVk/opc1VDO7GqL34IXoNR7mHVKkX8Pj8Z3Ko8K2uxqz+GJcU9Mh3tgIn5MUnEfNkba2bKgPUTbM6xQOZ1Iz4eOv5uuSumFHQRw/P056hk4lt9dp6ng5s+zgTJiHPktMmDHjLpbeSTwiR9TEFezgLxkZbZCOia3/sdHHEE/0GsPRd8LcpU6c73E4Xk35rbZ0XTKuLCeyJmTI5JqUaXvNdUmvxuM9M2wbBwR/o7FzNuxwsGM7eD5E/bdzmtCUeQ+tnHJqbIQjGgb0v6gVON9X2/RAHLfYu5p5h8S6qg2A3Lts2pTsv03r2KUMKmo711aN0Pe2xlV25qGVcw4UtP4RDX7uH9Z5ev2BDgY9klQbPXcruA8Yuib06JF0Dt7oLCk8j14r30TUbhEbq6PWW+iq7ka66/sXWMa4k/ezORxApLO/phBxl41oxEfGDt71uZtuyO6eX5OQ2c+llcwwaSFSqF8QiYSLXGPaY6egQWDFqR/SDs/JKBVYmQIilfXUJ6t8dsVNQLm6fOAyHF8McNIs331FXvloM0o7S86PVDwRMTKzqqdQqZcEfTMulE/Pq6PjGMDC3v4O1GGzmZVcBhWxDRz8Tcyqqzantk6eLBFYAIaZVSOa/znHhH6bQSK2ecePe7D67M/eh+akf882twEZcw7PgM/SFadmBMi5LGa9mQUPOEXl7VTcBCwEOW5elBBwiYZjAgs7+ypUTWlrafqAxTACIekz9iiuVhkfXlc4w9PeDDDNrRvQ/M+5bHoVg0h47ps04d66Kyp39JnvkKl0wGRb/ATL0yjEq2wWtYUnTQdf1t+RjA33vkbBeYLDbW1j4X2or75jE+dthjDpDOCRWoZ2+nbydkbVrhibocE2twlk1iMjU+53RAL+gpkxTU0gcMGoDSd2oaRLRknXQKZyWpgmJtfRIkoonlG5yoSJm8yWjBwAjM1sAIFIvgzTrbmsn0noPduHj698H0Ew12lerk+K2NEC0AgE4Ddm+3aYBlGWGwVCU0pB6rRDrxf0e1IcP1frKjLsoKhog5PvqGMmRwO5nKhTxQAqFJT6eE9JQ7z0etcyGhtXUFe08YZO1QBPP5i5ok7U3aRUu4e21nonjasbv9kRNRcNXiaCVFEfLciTNgQJ52tdDMth4hzwuH7y6W4BjR5J5zkIVHtlvrbf+TJ0Jd3lHaNK+UwWCB85+6xsyU3+5RIcfU526FJHSdT76QsO+CzO8TtJZxohganTKvqUbjVatSqGqsgOWc/nMIFv1sJvOkhnbs4HjgGJtUnTdok6kw5rFAVStXysNYXZFjV63vmbAPABULKnp8o+o+NEYP+jlhJM549N/tFuvDbviAyQU3bMs3Sg6KG1KBiE5i3KRb8cfkdZssPUlAAV3ZjbJslZMvFVpENqDxpNHRZ0wNo7BswdHXnHRpr0IAD9Ri9IJwaHWlvHQ929q8cEjIrxeG4tiW2S5pY5FSe519p5yUDfgmUZMH/Lu4nCKSkB9+ls3jO/7CWjIelM+Y5ou4ew2VP5boowytfudxzNB1x6Uje3K+nOrs49xefbPh4YG6FlW/qAdy48iILpnfHhlOgzLGMz4Bg6cunii7esKjdJyBsK+g9lmbvszliwfVQH6SzsE347CoCPrpgekHTBg4pBquIgNlufXyyfbsygPOW7Dbp35iniBdPKCwb8HDY077cYd4tnpm4Dbk25oLWD6W9EOoCOGE1ZMU8MOicqau/I542bboYCRbsZ0hMa82RmTUPDDcphZZ1krY2nEHedGgb0Q4kaw6UwOmmoL+oLNMNWDFPKFC81SSHAWhmrKTvySNfyant3RJs6b6tOTY9/k3QbUqmL8GbOgBsxrh+mgEK1Q0GT5CkYCHLLaniQyQ8kGrNJOP3zRF027vw0oYxnQqqm+wx7eGrzzHWQdCnjFwAvKz7wHb9+5aXlg1e8PumIgEKnN6EqNSQSQUuh0gBbMuij7//4MSzWhdRm5BF50V6hxExZiAszBZMIVKPnXimlm48uHPHzy0j33fYc25Qw55+905d8de6jmZv4HHsw/8CVJQhy0qIr6SDmJtC2EewkYN3ePcOihcTnxr7Jn7lMVJrpsnHnZwqn4GkmiH/mjJVHF+b+TiASAZFMwe6bQMBryCTCc1ZYfuWDJxecOpMOgF+d8sKsqpkuCXcqVhUfMzdhgYyyIxfba32Bsf25v7Is7JtiFpzRv2dQISdaWiYwYNCtg3QEAuU5kUTSotmYCek5uHRngRKwIOng7IuavWqYTiMTm1GTHDFNUGxWYuXv+sf72oM+I+csrZibqjW3F7dlLTkhh+28EemeX16/oE7QpXM6egPtoDXjdCovBQAKwpN385drnXWBF3v3Vo2D2+8aB68va7M/jtCpYXi6vmiNxsm1zaAdJw+gHTqv/KbiJkWn9kq0rClKqgvybx8gHNyhSfpAYyO6qw0buRQ8BCydGoZ/l3TkRWSeG3BPn6GfcQ+OwBlZxxTaoDO1KDvY4mcKndUUvLByoC4b9/XCPv2t2NQHLK8UzWd7yvdC0uXM2dtYPCTgAt9dBj7eMOOH1ycdA/AdHX7mGFGqiaa2wMIpaO2wA9UOCPJTcqQjqc3cJ+5K3wXXLXXV4L5+N2QKgcp63m/E3P1HF+XdfxnpdisGhfs52YCB07bsTAx0jOrjzgf9Ctf/8AD1uXoi3cos+52QdIc+PzKlvz0JsQyUn5Mtq9ST7suFITPxVGMkrnDxh0fLRj3sMC/tzFkPcSTaYxvZ8Ar/ce9yATjK70y6qxuGDrdiUZtcZCP275yXNMLeiNJg4ZdyZ8KyLRK02lBU4GDnmR3MuE9m82uFJYdlsL1lo2MQ9AedV3DA3C7u99/At3N6Mi+9hkxakHcYMDqbl1amjCacke0vfllz16M6GK5vTF3JItMesnnixOOrUhdyWBwQXVBWfrwKob4R6R7Pzrxl0DFR0YZm36tLVrB1Kq9E84LRfvUJgytq7UQZte59k+ukYb7VUUv1/g8Egly2fiSP/LKbz+gV8XtdwQfdpvmXAdzdZNIg81xU6z5gDlygV4uCAmotE9x02Qb4N0l39cOCRVzU14gp2rAvb1MlGaaVp7utN+Y7fhM1YWN2WZb0Cyqd9dx36Ky15fcQOqjcRF6QGTjXjEV95pe+4F6HeZk6c9+9E6vSk72tWVqhszN4E/PyQHPz5I1TY7JdLWj1Fg5+jwfP+yAHlpk00OMZ217SPGTxJwnoKIlH7iy1KBrg+gUdNcMkmWUzX2VebiuSHSMRKIBtZq6hkIh3TFh0YOwUDj5/+HB4T6QrTXKutZXEg8C0xc5FCeLnpk5BTRmrjkfDPKBawi+MdvmawbEE/pmLJnQ2Lz+eGhWYFGSPmNr7NhVsO5uPICcsO0j3aVtL8cQ49z0sKhGYmFs/MTZm/WpEJbSx+cInwWlTFqNVv4dKBSpTVuf6XaYacYEwqXS2BP0O5Xkx8pycyMXudOrjzqR7HfNyfqbfOIElF9j6xarLLtWGI8gZ61HBVrUkCg1QGEa/m5mZPqCT8CBo6OTfstac5r426RDkZ9emBP86g44JJXnqfqCo7Bac6AmgooJYnTmJCc1UXVI3tBwe71bn711l2A5KwODMC42VjfqR8HWgktnTgGQT1rFfhX+TdPsXDbKJFBnfJVMZz9ii/vv8na0/sjJltZLpjk2pu79M+uX0nEIHY2Ib0cSyBc/gLKeTcfPtOMTHeKY1mHbw5uzOpIOELBtq/x5qbiFv6tNVVuaR3y8K2UUkUhCHvpmnU/ZWmR9ZlbuRQ0PNOEtRIw5HmOLEJRwxohHaGNY+2rmnfomApGOw2MDRs98JtInSdjEpzd+mjMwO47dYSuPPuSSMhwObuVzKWm3MIIO0JccOtZOOD0KGDDkAywzuI/qRTqECz7g85UTlbbO9izO2s2kkQLTygIGUd5zNCadYVMJzI3v/GsXpu6Fdfbpt0+OLrU2oQDxw2uNHjw5adZDuZPWtOSEuxlcdw7PhjAUHc/PdeRYzCVQ28EudVh6fn89B0+A3pdaenxUR7GQG8ETWIzdZ/HEez3xesNimik7EI51JxzRxrXf08lqBlml/XoegUpxIjpmXHaS796nCNDXIVkkhkYBf1tIzt1f1STahExsFsflX4T1AGRduspvMk4K5Z64lQtI5esoAlcmZp68XZ1W68g7igV6/AFL/8dA6Z/dHBh3Txh3U5a9Yg6BTpk7tb+PZipH96oMlDQbt2LkC1Bfbp1N56+iRdAJ/oLZ2/qDWRvAeJtY66bjumo6K2sljU/NZxFtXLYaupAOgkrxrxsA0Jy7lHPqnCu3cKpqR4Lu0Jbv0oe2K+Wlj7EwJXxEJeBUOj1cRODbfxpasnaQAgHBIkZknsDRVpc7YB6NmOOTWCtd4D95POL5IhfNP1UeCUdJlWJMZd6NGzauoQhBz1E82npYWthslnepA85ORmA46kI7t5/g101KkmrDxUObVXQrj3CjnlVwWEUYZVQQyTYXnuV0at/7cEKh/WJFxCiU4es/wvjvEWLV552qVEc1Y5Z5UMm8HADSoe2zhsCIrLuuuR3LhNxOHhB1FEzuVoagshaGXinYoMb/2wvp8TqrMbh2XSYTRSbRdugrHc79YuPliMsxHSXcOJZ0qXXEIm5F/PDLdOk3mfpaIx6mKd17YlR/hfYiC81DtvXZ6jcSJowoYs20PqoYN7Ij6eIqXKVFlG5a9O3JRpQlM68DGPN9Ed1vOF6gvBpcGVAQq/Xu3PgPPjdt8KQFBjkWhpPsOphuIQ7AKF5jKWzIyWoWSTpWpOIlZalUHp2a4mTNvsW2cb5bkpX3FpJFU/WacmQ7zIPatGjfbjIZXSUsPLn43N8qwTkysVMurkBT0+gXajhYORf0sQ9I5+AG1OH2wTuWtoGV6VnJ9oNSQAHauz+tHbVypU3nr6JF0f0UEns3Np5B4XbUYupKuAzfKQjmocypCB15x6uKDVtCc02VhGBdhxeUxyWIymSl2TJygN0MOK9LMBDyOeNjUT1x0SbgNhUkitJOKcbJsrMNDgF/Xc/gUlmjAGIUDdOwBqCBOGSZzYOLcxZ80NHB1ariKAhEMmojdRm/U+3GFMbYC9EfMFPiLhTGF+gEVJbwQ/YHE7iRM8TvFw8VMDk8sSVPoLZHqpV5MrhHFVeCeKJ75zigxam50KuMu/hTpvh1wQqS1M/oj5riHiEVyhd53PzIzzZnDE4hHKj7VlykeYGfJRN+PNKPUNT/C04mM8xaf+Pqs2JaJE8uXfKEPBEGkhvDE/okF4g5zvjOgP82i4ODsLLYMybJTKNo3UCDIQSMZi4WlG4htCPqeFaRFowaIQxKLxQqFUh/Umxhr5UJG34O/uwB73iIl0LtdeQmWDAGPKR5S/J540fABhnViwhOvUL3Qx/B0w8ChGkdnQ9I5SYHaN+etku5RYWJyvdTHsDPbOLfWCqfpFxe74tkHc/dqBO497oXsKtqwITWN0/b01RXF8L9Bul7894GcypPiKCaluXM3D0JN9NcK4L1V9Eg6RwlKuqy3TLqBKOl8DTqz2sb5mdp3Tq5OpRtaP5hzUit0NyjzMqmTpYCmGXvDdUUx9JKuFz1hY47rVmvXyK+m770eoEv6d9H2yRjUvHQxJJ2tF9AOnFYEADBYEP87aJmbk1wfHGDQmdX2bkh94Wb9in5X/COkc5KAWmvhJ+h1xeuK2slzV/MZxE9XLYaupFMqsml5MQFwvUYvwcNmyOXyCoN3+MMaf64VmwLXkOQmov7hQeWXu21hA+CwWYwVF9PpLI6SeHly0Q7MVJmX5evON6XLcUS6PHr03P4q1Oe6qRBR4r250J8yKAd1BoyZ3w/1AakVchwlRADD6yL5kZYWaPZhQE1Z6Hfoy7gGD5LHFK4yhnkdqFolpPYXmcB3LKcYcZMky253C4ChPia1ODYgHH2oTvfAl6+qQqBpi2FlfnynvBc6SzrpdMZnFbMk5h16ksxuC+IQw4LNsXy+LAf+6gGjl8FJ4+Upnd5zhgQ30UkSPSVbccBBl4QBOT0OmvZYPSPLjxrU81aBVO8eWi/sGkhxBQ2TNn6I3EN63NP4VwA3UjdEyDq1gQoMpAyZ+dJAyj9COs9k0CgMNa/meTFfV+5bShigwnAA6kq6QwtTeaNjpc3opV4IJHKzd3j+7aW3EUeoU3X9/ahAa3wl6uDDxeBmPJ7QiHMbfOZKw6NAmN8BBDkcHGPNvYs6gwb1OUnjmmMmbPG6uH1Msp8ttw6tp70dE6tGQtSUzBvrQznxPlwYgTQohyMxmmPHLjwBAy/biiOc4qSOMIrZHLDsW/067IbxiTDCqC9DIBCbXQOG1S6+8UgK8xFE5Zvqgf+UgMfDrWSoDr4Zb+VfufLTSmwZAupAwJ0qE2Kln6EO44v2UV2OpfT3wi3tC9arxsZhz28ols3Lb7eNQq8NsHVav00mZGK9/l2QGI1Rw2deWbj3K2x/49WthdGBjpQ7JAIey8cTSc0sd9mNCVuuYLtHlo2OaQ5MGt88fMER7FgW8lN5jJsxtZpiwm+2jivJhGkQn63OnuBgQod7ZrF6SBRqMy5iahmW+bYBgMqhKda7+y6RiMKj1Znvv1ZovWVXoaghNvGu2s7tQ7UoKbc2Yqh/3TCFwSgCPp3mVh/s123JQBuUfrnpzIMe1zWe7pqzVCN077YPsi4gQNt1ve/1SffPLBkcVWTz82KlwFM2DJDJ1NGeDpxSH2feTxQ6C/gPm72t+tYeh8IEr6dUJruFIwrbxWKQ8wKEnNM0CqlNKp/6x/pfAQx7Y0CQYyGx1tw/aHSXx4GJiUvQJBjdy3Hyj8uJLtliuq4ovtXExg2wLOxm89j0CZ4OZrdMpSmqo8qj/DhfTgzUzUmWnqTTLUBC1sizBKpRblzeuzFwNvxkdXapn6sDMDdigoDUBXeUuqgkSrpndqI+wNTaeTqHRR0TLOafp5KJbV6Jk+5cuaV0mJXqf4dEprSxBdIzBLrxmH5iznZTI1qLR+xYMP/aY/1GcATZb4GS7jIVZwNix4x5H03KiQ12+dzciA48YkZ/Mfb9byxWj4177uApAxyewzswv114OStuIa6wDggYINo+PkzBZlCeGlm5/GHlLIUEyAkRW9zmWDiCgveuzP16T5FDUqDDj3QmB5h5hJ2E+RG+/POmTPTZ5CWXSvZeNoWL40FJhSBDcdwGWm7bxga+a2Rs1uordgUBaVNPTTmtNUHU+22y+rp+QybRgHNExvZQqdNsJzbjFsPc7lr/8u8MotZvDS1lY2oMOiYU78EN9UElf3owFe6TfLK4cESdp6ilvSxKJDdJvVoUdLN+/HtpOjUcUnvRqGlIn3Mae0OyaKUDGhon7oQ7Cl4brbvmntYKDGfA/wrpBoxaCrIVSqwj/3F1Q4qfFfEZVxxx5eSW3DlMMvWZY/jws0tP3sHC0SrlCp8B7ka/kunmDeFbqvRmk550DPdHYampcM0HM3ns+yT1z1YC2vr8/t8bmVkBCsPhVOjgYbP9IxITcLwQ8Q+dopdXPhy/wsRYCDafuboe/Ub6bXxz0wMeSuJzbxYlBzYweW71BbtvQpJipPMOzwRZ845iW/ceXF0fFOHKrKYYWdWu2rJ4jpURuZbuHF5ddrwKWzP89cJ6zsgol9M0EhXINtzUL46/IJ0HONzais2kCHLPdIZcXMX1CAfCgZMFkHQCnwhgauMMy2HP5jmoyMCcazycZxbrwztHpAvBnJNfTdMl4/atz3eKCo+Sc3K2204dKJB42xsB76Gzfyy/WY/1VXj6YVKC+10z7wFAkDzRtjPpHlyZxhsUYHvZO64ErJoYfdbMPaqx/PhlGdKwkzs8xOlzCpUFBN7B13lO3qUp+TPkPIG3uGiF8rU2iLwxnp2bP0fbtXPaoqbfwLUKncpLAZQK0pPpw691PQ6ktvNuazjwW5ZODcOT2YOXaroejLXzBJqwwoldzbdX4f8K6eBev1R/ZgveVnLl4NpJd2hMY2AXU7gSDlS6fML4aOtKY1MLJH/TFXjkBUMH6Qh4AkIgEp+gSZjJYx+YfHrkIYS3eZSXSx9no19RExEQyeSneBq3LmvmplMVFS/8x55Ih6iP5UotmM1Ut4i8S6vTPmSYmD+XDl+EdeiupIMzzchQ41t4ukntoiXzPrPjsZ9bRxbc6qgLPkNRjO0yNoME8jdeqoRpED2RDmLZMN5mvK0U4PyGY6QjEElwf+NjNAt7NnHyBPT3hZmqLGDwfa3x13CiRHD4/n0GAKed+rGZE9AsbNF5YMnW0rMHlq+yM8IBr5J9a7FCOqzIdtuO53qCfV//sBAlHegg3bHFWdJIP6dnnkUfbdi9omCCo5kpGLL23GL4LDuz6NIwDwsNavIDPIHYgpqp9aGpM86jvujrHdN5U2AbnlNCmww6JypqcRhSP233UFAJetz9gSAnmU+WFoyv9/U2KAdFmzAFLogaAKhPRzaGB3bxH1Fd3wSgEWT20am9EjcVcsrTlROVWic3gzr+i6RDak5YxrrgW8iOgVc+Xjr8QzKN8ZwXlrsF/ZDYGhlStcp4qNT4Bo3Ffhy//Av9mbGXmZf2gQNjL935xsIbR3VY/X65d6y9ZXo/EXuDuQn9DxKTB6adfoCdMoDoiXR7Jvb7mMU2QwKSC7aXzx+10pFFQYKGFF9e+8VDfjfS/XqUk+JF+onIYNfOnT9tE8+E1mTkl/LTorsAW4iGu2ZGyHibWFQCiFl2aQ1Mg3gZ6d5NsT5AcQwE1D6jMdJ1NS89k4vgr550N9f680NdTa7hrYLB1lt1/dF3Et6Pw/qWTCTcp1PJYGDxVrBzych0ZzMScM5Z97GuGIY5g+2PUOwDwaJ9nw3oTLqtM+NKeRxrkDG1rCLaX5jv52IF+o1aBiorlWYyb3sHWXaZw5IRA3JE1sytIlvTGmNLJyArWGswcbxVPJokL6kT93DkxqOvVhMzbgrotKgJASoU/Efjhu6o8/Ft7VpGI4oA6sRy/eHNzmhZnTezx+M5tn4PNBG5cx7K1hpsWu6Mp3tLPOuiQtZrvVAf1Pa/6dN1kA5BPuEuG+6/wpRGaROEZR39+cZWuS+P0Woh8FM7DF+RpJCLKIo0r6lWJozHxvYh6oU3mjDHH6KDdCyTsPqjABgcWUKQL6VxQtYNml3A8fkHvwi7vDeFni2z3gb3JYrmXX8p6ZCflovDhex7dAsHGMSA+xQr7Ni4KkvfmGbpiBWunUkHI5BbJkVNsDUiN7CdZPc/v3FK3s+J/TuNZdHimTZrOq7wOHXdcNtoiZCnppt5glnXNLFYoyh6It3huemxcRJLxLHPkOq+E/fYQtJJY0eDYTM+6TFaCQFUClphtHA3i0oFtv2zzkxeu8NHxmW5vZMm2yS04mKke6+4vzjYzUJr7uAPvPNX5cFyKwu8S3xsuUAgy7gvm7yD39m8nDXM7y6ZbXkfTyLDbWsV5iycisoRgHkfnZgm87K9ZOTS9/vxm69ifWhbnmgfmc0H5pEF/xzpGrcVmtfLPC9r7Lp0UtRHU9u6tqhtnGs00cN31No4ldbaCqfUSTw1agcXwxMDHZI89ROkpHsoHAJ5fxKzeXRytw3WUNoPscrOaWziJ+nUsWMZjwoSkxvlcdc0Arc6tZ1zj22+NulcQuHzzMCe443Es7R57dGYjlnjZaSj0Fhwf2ANgUBQU4j4JzSm1dNRa84Vw21iRzaUXDKjEwFqusAIYA2ZiMf2Xio+unSwotPyzAvzkoyQKJQ6NKn9BDNPXGMWMcL3vZIBV3EEYhuJTGlAp4ZaEtqOmSACbFDV6o8VdSXdvuKALL4xtanfqHJ40BUOOswPZvRfRyOaglEbzy7bUJj0HG7kxRMIqImFr6WQCI/wOCaSu/z0HrQ8+dr+BbvtjAgInkhGTUI82iaukUjngoIVH3+J3rt+QH5BOpL+qBGFTGyCwY6Ike9uPnwfMCDpsLbwBA3M18uo9u1gHfhxw1DvPi7shwQi+Tlq8mHHcGgU0mMiwxwUlh+4rlIp2SsLY46bMKiAQKZCUxXNJ7YwTa3QwWPnKhgk6iDdpTNbB/pbMUCf9HeOxk1eAne2MKcP9txlRKeAlKlbv86P9jhDo1AA+k6xb0Ml41tMLAWNQxedG4D+/c+h7dyGnIZ+odUGnfQNRe3a/zvNkEVwC8xL8bQ43K/OX3S7x1Pnf0HUts6Ito+8smHCh/p1J4geSfeXxRM0rTm2rVJnancl3aeKFNPRsVLsmEiHCP2ilKkzPtYHkyrzcOSCfswMFo1wDP1TSTG1qaAmL45AHSS9WQWBks4z1oa7H000qA/HFylxvpnCtQUy1thYn+XGDAqajldy7SQf5b93sT9KDn09V3cXjjM2Fio3nb5aAoCKNiMjcCqbRlD6LPpWvw/w9L6ySA8+RemTWb5h3fgkg7b4jl7KpAmbijrfW/lAmsyGS92LXioJZPoJpiw/E8567bntgBuwC2P916H2daf6LJSROTMVx5F2/2j12PjPXuR1klGHsD2ZnQHPYEpouImmjHYdpkPA/vj8ssjORE+X4OQeNgwsn+Xgo8T5pSe052BLBkqUdMoza8euQ2cuJT9qvH5W/mpDRhqbRTnFch/83u8PT/oUJkkWMnT34uAbrYyZsAX7tv84Wg8tCWlKjjynFbj00PFeLWpx0k2NTbr+f0m8DPAkeGt2qKQhTvplT/W8kfjIgMYxfJ565DqDA7MQ/ybpetGLvwXk3DLbRznxOzR2zt39tZ7EyQto4vI+eRg32WBD6p+hcWKC2eMJaQfr3Dx6rvdVYocOCn3lP9QnjZPeFBn6mx3oJV0v/s8BubXRs97ddbrW2U2pEbjeVVsLVWprASrw1/kn1M+7WB+Te1Y9oPhv/cPVpnGxUY1DYk5p3Dy/R01OldZV9Lxbpxd4NaFm5C9qZ9+bmsCErQ1Td/6pg9v2656JWuxe/67AZ/ZQNa09vrjjv5yhhNuPyvezZs3qjzXWi168bbRk9HGtthWIO6TG2se5SvjieMjfBfRHkM0KG41AIG6M7tfl36oL5fVhKVHVTl4e6nd22eiK/CmQO+9baDrd898Tb/GjNaesOvym+fPnO86cOdMdneleGm3tRS960Yte/K8Ch/sfs2aumsFnSD8AAAAASUVORK5CYII='
                    } );
                    
                    
                }
            },
            {
                extend: 'excelHtml5',
                title: header,
                filename: nomearquivo,
                customize: function (xlsx) {
                            
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            var downrows = 3;
                            var clRow = $('row', sheet);
                            //update Row
                            clRow.each(function () {
                                var attr = $(this).attr('r');
                                var ind = parseInt(attr);
                                ind = ind + downrows;
                                $(this).attr("r",ind);
                            });

                            // Update  row > c
                            $('row c ', sheet).each(function () {
                                var attr = $(this).attr('r');
                                var pre = attr.substring(0, 1);
                                var ind = parseInt(attr.substring(1, attr.length));
                                ind = ind + downrows;
                                $(this).attr("r", pre + ind);
                            });

                            function Addrow(index,data) {
                                msg='<row r="'+index+'">'
                                for(i=0;i<data.length;i++){
                                    var key=data[i].k;
                                    var value=data[i].v;
                                    msg += '<c t="inlineStr" r="' + key + index + '" s="2">';
                                    msg += '<is>';
                                    msg +=  '<t>'+value+'</t>';
                                    msg+=  '</is>';
                                    msg+='</c>';
                                }
                                msg += '</row>';
                                return msg;
                            }
                            
                            var termo = $( "#" + nome + "_filter" ).find('input').val();
                            termo = termo.trim();
                            termo = termo.replace(/ /g, ", ");
                            if (termo != ""){
                                termo = "Filtros: " + termo;
                            }else{
                                termo = "Exibindo todos os registros.";
                            }
                            
                            //insert
                            var r1 = Addrow(1, [{ k: 'A', v: termo }]);

                            sheet.childNodes[0].childNodes[1].innerHTML = r1 + sheet.childNodes[0].childNodes[1].innerHTML;
                        }
            },
            'csvHtml5',
            {
                text: 'Limpar Procura',
                action: function ( e, dt, node, config ) {
                    minha_tabela.search("").draw();
                }
            }
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        "iDisplayLength": display,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "orderCellsTop": true,
        "order": ordem,
        "columnDefs": [
            {
                "targets": [ coluna ],
                "bSortable": classificavel
            }
        ],
        fixedHeader: true,
        "language": {
            "decimal": ",",
            "thousands": ".",
            "sProcessing":   "A processar...",
            "sLengthMenu":   "&nbsp&nbsp Mostrar _MENU_ registros por página.&nbsp&nbsp&nbsp&nbsp&nbsp",
            "sZeroRecords":  "Não foram encontrados resultados.",
            "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros.&nbsp",
            "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros.",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)&nbsp",
            "sInfoPostFix":  "",
            "sSearch":       "Procurar:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Primeiro",
                "sPrevious": "Anterior",
                "sNext":     "Seguinte",
                "sLast":     "Último"
            },
            select: {
                rows: {
                    _: "%d registros selecionados.",
                    0: "Nenhum registro selecionado.",
                    1: "1 registro selecionado."
                }
            }
        },
        "aoColumns": data
    } );
    
    
    minha_tabela.on( 'draw.dt', function () {
        var termo = $( "#" + nome + "_filter" ).find('input').val();
        var sem_acento = removerAcentos(termo);

        if(termo != sem_acento){
            
            $( "#" + nome + "_filter" ).find('input').val(sem_acento);
            minha_tabela.search(sem_acento).draw();
            termo = sem_acento;
        }
    } );
    
    $( ".dt-button" ).css({padding: "0.3em 0.5em" });
    $( "[name='" + nome + "_length']" ).css({width: "80px", padding: "0.3em 0.4em", border: "1px solid #ccc", "border-radius": "3px" });
    
    $( ".dt-button" ).removeClass( "dt-button" ).addClass( "btn btn-default" );
    $( "#" + nome + "_filter" ).css({width: "33%", display: "inline"});
    $( "#" + nome + "_filter" ).find('input').addClass( "form-control input-sm" );
    $( "#" + nome + "_filter" ).find('input').css({display: "inline", width: "200px" });
    
}

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-uk-pre": function ( a ) {
        var ukDatea = a.split('/');
        return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    },

    "date-uk-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "date-uk-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
} );

function filtrar_tabela(tabela, termo){
    var conteudo = $( "#" + tabela + "_filter" ).find('input').val(); 
    termo = removerAcentos(termo);
    
    minha_tabela.search(termo + " " + conteudo).draw();
    return false;
}

function removerAcentos( newStringComAcento ) {
    var string = newStringComAcento;
          var mapaAcentosHex 	= {
                a : /[\xE0-\xE6]/g,
                A : /[\xC0-\xC6]/g,
                e : /[\xE8-\xEB]/g,
                E : /[\xC8-\xCB]/g,
                i : /[\xEC-\xEF]/g,
                I : /[\xCC-\xCF]/g,
                o : /[\xF2-\xF6]/g,
                O : /[\xD2-\xD6]/g,
                u : /[\xF9-\xFC]/g,
                U : /[\xD9-\xDC]/g,
                c : /\xE7/g,
                C : /\xC7/g,
                n : /\xF1/g,
                N : /\xD1/g
          };

          for ( var letra in mapaAcentosHex ) {
                  var expressaoRegular = mapaAcentosHex[letra];
                  string = string.replace( expressaoRegular, letra );
          }

          return string;
}


