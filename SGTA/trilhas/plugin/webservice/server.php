<?php
setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});
        
require_once('lib/nusoap.php'); 
require_once('../../obj/conexao.php');
require_once('../../code/funcoes.php');

$server = new nusoap_server;

$server->configureWSDL('server', 'urn:server');

$server->wsdl->schemaTargetNamespace = 'urn:server';

//SOAP complex type return type (an array/struct)
$server->wsdl->addComplexType(
    'Person',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'tabela' => array('name' => 'tabela', 'type' => 'xsd:string'),
        'num_rows' => array('name' => 'num_rows', 'type' => 'xsd:string'),
        'erro' => array('name' => 'erro', 'type' => 'xsd:string')
    )
);

$resposta = array(
            'tabela'=>"",
            'num_rows'=>"0",
            'erro'=>""
            );
    
$server->register('consultar', 
                        array(
                                'DataAberturaInicialOS' => 'xsd:string', 
                                'DataAberturaFinalOS' => 'xsd:string', 
                                'DataFechamentoInicialOS' => 'xsd:string', 
                                'DataFechamentoFinalOS' => 'xsd:string', 
                                'StatusOS' => 'xsd:string'
                            ),
                        array('return' => 'xsd:string'),  //output
                        'urn:server',   //namespace
			'urn:server#consultar',  //soapaction
			'rpc', // style
			'encoded', // use
			'Consulta à Ordem de Serviço de Trilhas.'); 

function consultar($DataAberturaInicialOS, $DataAberturaFinalOS, $DataFechamentoInicialOS, $DataFechamentoFinalOS, $StatusOS){
    try{
        global $resposta;
        
        
        if($DataAberturaInicialOS != ""){
            $DataAberturaInicialOS = isdate($DataAberturaInicialOS);
            if($DataAberturaInicialOS == false){
                throw new Exception("A data inicial de abertura da Ordem de Serviço é inválida.");
            }
        }
        
        if($DataAberturaFinalOS != ""){
            $DataAberturaFinalOS = isdate($DataAberturaFinalOS);
            if($DataAberturaFinalOS == false){
                throw new Exception("A data final de abertura da Ordem de Serviço é inválida.");
            }
        }
        
        if($DataFechamentoInicialOS != ""){
            $DataFechamentoInicialOS = isdate($DataFechamentoInicialOS);
            if($DataFechamentoInicialOS == false){
                throw new Exception("A data inicial de fechamento da Ordem de Serviço é inválida.");
            }
        }
        
        if($DataFechamentoFinalOS != ""){
            $DataFechamentoFinalOS = isdate($DataFechamentoFinalOS);
            if($DataFechamentoFinalOS == false){
                throw new Exception("A data final de fechamento da Ordem de Serviço é inválida.");
            }
        }
        
        
        $Where = "CodRegistro > 0";
        
        if($DataAberturaInicialOS != "" && $DataAberturaFinalOS != ""){
            $Where = $Where . " And TO_DAYS(DataOrdem) BETWEEN TO_DAYS(DATE('$DataAberturaInicialOS')) AND TO_DAYS(DATE('$DataAberturaFinalOS'))";
        }elseif ($DataAberturaInicialOS != ""){
            $Where = $Where . " And TO_DAYS(DataOrdem) >= TO_DAYS(DATE('$DataAberturaInicialOS'))";
        }elseif ($DataAberturaFinalOS != ""){
            $Where = $Where . " And TO_DAYS(DataOrdem) <= TO_DAYS(DATE('$DataAberturaFinalOS'))";
        }
        
        if($DataFechamentoInicialOS != "" && $DataFechamentoFinalOS != ""){
            $Where = $Where . " And TO_DAYS(DataFechamento) BETWEEN TO_DAYS(DATE('$DataFechamentoInicialOS')) AND TO_DAYS(DATE('$DataFechamentoFinalOS'))";
        }elseif ($DataFechamentoInicialOS != ""){
            $Where = $Where . " And TO_DAYS(DataFechamento) >= TO_DAYS(DATE('$DataFechamentoInicialOS'))";
        }elseif ($DataFechamentoFinalOS != ""){
            $Where = $Where . " And TO_DAYS(DataFechamento) <= TO_DAYS(DATE('$DataFechamentoFinalOS'))";
        }
        
        if($StatusOS != ""){
            $Where = $Where . " And StatusOS = '$StatusOS'";
        }
        
        $sql = "Select * From view_ordem_servico Where $Where Order By DataOrdem, DataFechamento";
        
        $objcon = new conexao();
        
        $query = $objcon->select($sql);
        if($objcon->erro != ""){
            throw new Exception($objcon->erro);
        }
        
        $resposta['num_rows'] = $objcon->num_rows;
        
        if($objcon->num_rows > 0){
            while ($row = mysqli_fetch_array($query)){
                $tabela[] = array(
                                    "CodigoOS" => $row['CodOrdem'],
                                    "NumeroOS" => $row['NumOrdem']."-".$row['AnoOrdem'],
                                    "ResponsavelCPF" => $row['CPF'],
                                    "ResponsavelNome" => $row['Nome'],
                                    "DataAbertura" => data_br($row['DataOrdem']),
                                    "DataFechamento" => data_br($row['DataFechamento']),
                                    "DescricaoOS" => $row['Descricao'],
                                    "ResultadoFinal" => $row['Resultado'],
                                    "JustificativaDoAuditor" => $row['Justificativa'],
                                    "StatusOS" => $row['StatusOS']
                                 );
            }
        }else{
            $tabela = "";
        }
        
        $resposta['tabela'] = $tabela;
        
        return json_encode($resposta);
    } catch (Exception $ex) {
        $resposta['erro'] = $ex->getMessage();   
        return json_encode($resposta);
    } 
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';

$server->service($HTTP_RAW_POST_DATA);