<?php
require_once('globais.php');  
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>TCE</title>
        <script src="js/jquery331.min.js"></script>
        <script src="underscore.js"></script>
        <script src="funcoes.js"></script>
        <script>

        async function loginInicial() {
        
            await sleep(300);
    
            var token = window.localStorage.getItem('token_local');
      
            $.ajax({
                type: "GET",
                    beforeSend: function(request) {
                    request.setRequestHeader("AUTH_TOKEN", token);
                },
               
                url: '<?php echo $_SESSION['sgi'].'usuarios/getTCEUser' ?>',
            
                success: function(data,textStatus,jqXHR ) { 
                //var perfil = getCodPerfil(data.authorities);

                if(data.cpf.length == 9){
                    data.cpf = '00'+data.cpf;
                } else if(data.cpf.length == 10){
                    data.cpf = '0'+data.cpf;
                }
        
                $.ajax({
                        type: "post",
                        beforeSend: function(request) {
                            request.setRequestHeader("Access-Control-Allow-Origin", "*");
                            request.setRequestHeader("Access-Control-Allow-Methods", "GET, POST");
                            request.setRequestHeader("Access-Control-Allow-Headers", "X-Requested-With");
                        },
                        url:"app/usuario/server_automatico.php", 
                        
                        data:{  
                            operacao: "verifica_cpf", 
                            cpf:data.cpf    
                        },
                    
                        success: function( verifica ) {
                            var verifica = JSON.parse(verifica);
                           // console.log(verifica.resultado);
                     
                            if(parseInt(verifica.resultado) == 0){
                            
                                window.location="https://"+window.location.host+"/trilhas/sem_permissao.php";
                                return;
                            }
                            //somente um cpf
                            else if(parseInt(verifica.resultado) == 1){
                    
                                logar(data.cpf,'individual');
                                return;
                            } else if (parseInt(verifica.resultado) >= 2 ) {
                                window.location="https://"+window.location.host+"/trilhas/app/login/escolhe_usuario.php?cpf="+data.cpf;
                                return;
                            }
                    }
                })
                    
                },
                error: function (request2, status2, error2) {
                 
            
                    $.ajax({
                        type: "GET",
                            beforeSend: function(request) {
                            request.setRequestHeader("AUTH_TOKEN", token);
                        },
                    
                        url: 'http://virtual.tce.sc.gov.br/sgi/rest/usuarios/getTCEUser',
                    
                        success: function(data2,textStatus,jqXHR ) { 
        
                        //var perfil = getCodPerfil(data.authorities);

                        if(data2.cpf.length == 9){
                            data2.cpf = '00'+data2.cpf;
                        } else if(data2.cpf.length == 10){
                            data2.cpf = '0'+data2.cpf;
                        }
                    
                        $.ajax({
                                type: "post",
                                beforeSend: function(request) {
                                    request.setRequestHeader("Access-Control-Allow-Origin", "*");
                                    request.setRequestHeader("Access-Control-Allow-Methods", "GET, POST");
                                    request.setRequestHeader("Access-Control-Allow-Headers", "X-Requested-With");
                                },
                                url:"app/usuario/server_automatico2.php", 
                                
                                data:{  
                                    operacao: "verifica_cpf", 
                                    cpf:data2.cpf    
                                },
                            
                                success: function( verifica ) {
                                    var verifica = JSON.parse(verifica);
                                // console.log(verifica.resultado);
                            
                                    if(parseInt(verifica.resultado) == 0){
                                        window.location="http://"+window.location.host+"/trilhas/sem_permissao.php";
                                        return;
                                    }
                                    //somente um cpf
                                    else if(parseInt(verifica.resultado) == 1){
                                        logar(data2.cpf,'individual');
                                        return;
                                    } else if (parseInt(verifica.resultado) >= 2 ) {
                                        window.location="http://"+window.location.host+"/trilhas/app/login/escolhe_usuario.php?cpf="+data2.cpf;
                                        return;
                                    }
                            }
                        })
                            
                        },
                        error: function (request3, status3, error3) {
                            //tratar sem acesso
                            window.location="http://"+window.location.host+"/trilhas/sem_acesso.php";
                            return;
                        }
                    }); 
                    
                }
            }); 
        }
        loginInicial();
        </script>
    </head>
    <body>
    </body>
</html>