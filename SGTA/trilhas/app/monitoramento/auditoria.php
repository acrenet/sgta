
<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    $titulo = "Pedido de Abertura de Processo de Inspeção/Auditoria";

    if($perfil > 2){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Trilhas - Pedido de Abertura de Processo de Inspeção/Auditoria.";
    
?>
<script src="auditoria.js" type="text/javascript"></script>

<form name="form1" id="form1" action='../registros/exibir.php' method="POST" target="_blank">
    <input type="hidden" name="operacao" id="operacao" value="" />
    <input type="hidden" name="filtro" id="filtro" value="" />
    <input type="hidden" name="Orgao" id="Orgao" value="" />
    <input type="hidden" name="CodTrilha" id="CodTrilha" value="" />
    <input type="hidden" name="CodRegistro" id="CodRegistro" value="" />
    <input type="hidden" name="CodTipo" id="CodTipo" value="" />
    <input type="hidden" name="NomeTrilha" id="NomeTrilha" value="" />
    <input type="hidden" name="detalhe" id="detalhe" value="" />
    <input type="hidden" name="acao" id="acao" value="" />
</form>

<br>
<div class="container-fluid">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4 id="h_titulo" ><?php echo $titulo; ?></h4></div>
            <div class="panel-body">
                
                <table id="tbl_trilhas" class="display" width="100%">
                    <thead>
                        <tr>
                            <th>Área</th>
                            <th>Trilha</th>
                            <th>Órgão</th>
                            <th style="text-align: right;">Identificador</th>
                            <th>Nome/Descrição</th>
                            <th>Requisitado Por</th>
                            <th style="text-align: center;">Dt Análise</th>
                            <th>Parecer do Auditor</th>
                            <th style="width: 40px;"></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Área</th>
                            <th>Trilha</th>
                            <th>Órgão</th>
                            <th style="text-align: right;">Identificador</th>
                            <th>Nome/Descrição</th>
                            <th>Requisitado Por</th>
                            <th style="text-align: center;">Dt Análise</th>
                            <th>Parecer do Auditor</th>
                            <th style="width: 40px;"></th>
                        </tr>
                    </tfoot>
                    <tbody id="tb_trilhas">
                        
                    </tbody>
                </table>

                
            </div>
        </div>
    </div>
</div>

<div id="popup_form" title="Responder Pedido">
    <p class="validateTips">Uso exclusivo de servidores com função de Superintendente.</p>
    <form id="form2" name="form1" role="form" class="form-horizontal">
        
        <div class="form-group"> 
            <label class="control-label col-sm-3" for="email">Resultado:</label>
            <div class="col-sm-9">
                <div class="radio">
                    <label style="color: red; font-weight: bold;"><input type="radio" name="atividade" id="opt1" value="1" checked="checked" /> Abrir Processo de Auditoria.</label>
                    <label style="color: orange; font-weight: bold;"><input type="radio" name="atividade" id="opt2" value="2" /> Abrir Processo de Inspeção.</label><br>
                    <label style="color: green; font-weight: bold;"><input type="radio" name="atividade" id="opt3" value="3" /> Arquivar.</label>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">Processo:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="numprocesso" id="numprocesso" value="" maxlength="20"/>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">Resposta:</label>
            <div class="col-sm-9">
                <textarea class="form-control" id="resposta" name="resposta" rows="4" cols="20" maxlength="2000"></textarea>
            </div>
        </div>
        <input type="hidden" name="operacao" id="operacao2" value="" />
        <input type="hidden" name="CodRegistro" id="CodRegistro2" value="" />
        
        <hr>
        <div style="text-align: right;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#popup_form").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
            <button type="button" class="btn btn-success" id="btn_concluir" onclick='gravar_registro();'>Concluir a Análise &nbsp;<span class="fa fa-check-square-o"></span></button>
        </div>
    </form>
</div>



<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');