
<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    require_once("../../obj/autorizacoes.php");
 
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    $_SESSION['redistribui'] =false;
    
    if(!isset($_GET['i'])){
        $i = "2";
        $titulo = "Consulta Registros Aguardando Justificativa.";
        $msg_data = "Dt Publicação";
    }else{
        $i = "1";
        $titulo = "Consulta Registros Justificados Aguardando Análise.";
        $msg_data = "Dt Justificativa";
        $_SESSION['redistribui'] =true;
    }
    
    if(($perfil == 4 || $perfil == 7)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    

    $ObjAut = new autorizacoes();

    $autorizacoes = $ObjAut->trilhas_autorizadas_usuario($_SESSION['sessao_id']);


    $_titulo = "Trilhas - Registros não Justificados";

    
?>
<script src="aguardando.js" type="text/javascript"></script>

<form name="form1" id="form1" action='../registros/exibir.php' method="POST">
    <input type="hidden" name="operacao" id="operacao" value="" />
    <input type="hidden" name="filtro" id="filtro" value="" />
    <input type="hidden" name="Orgao" id="Orgao" value="" />
    <input type="hidden" name="CodTrilha" id="CodTrilha" value="" />
    <input type="hidden" name="NomeTrilha" id="NomeTrilha" value="" />
    <input type="hidden" name="detalhe" id="detalhe" value="" />
    <input type="hidden" name="acao" id="acao" value="" />
    <input type="hidden" name="i" id="acao" value="<?php echo $i; ?>" />
    <input type="hidden" name="WhereContinuo" id="WhereContinuo" value="" />
</form>

<br>
<div class="container-fluid">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4 id="h_titulo" ><?php echo $titulo; ?></h4></div>
            <div class="panel-body">
            <!--niceee -->
            <?php if(  $_SESSION['redistribui'] ){ ?>

            
                    <table  width="100%" class="ui-widget-header dataTables_wrapper dt-jqueryui ui-corner-all " style="box-sizing: border-box; display: block; padding: 8px; ">
                                <tr>
                                    <td colspan=12 >
                                    Selecionar registros por trilha: 
                                        <select name="trilhaSel" id="trilhaSel" onchange="myFunction2()">
                                            <option value="0" selected>Selecione...</option> 
                                            <?php   
                                                
                                                foreach($autorizacoes as $row){
                                                    echo '<option value="' .$row['CodTrilha'] . '" >'.$row['CodTrilha'] . " - ". $row['NomeTrilha'] . '</option>';

                                                }

                                        ?>
                                        </select>
                                    </td>   
                                </tr>
                    </table> 
                
                <?php }?>

                <table id="tbl_trilhas" class="display" width="100%">
                    <thead>
                        <tr>
                            <th>Seleção</th>
                            <th>Área</th>
                            <th>Trilha</th>
                            <th>Órgão</th>
                            <th style="text-align: left;">Identificador</th>
                            <th>Nome/Descrição</th>
                            <th>Justificado Por</th>
                            <th>Reservado Para</th>
                            <th style="text-align: right;">Status</th>
                            <th style="text-align: center;"><?php echo $msg_data; ?></th>
                            <th style="width: 40px;">Ações</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Seleção</th>
                            <th>Área</th>
                            <th>Trilha</th>
                            <th>Órgão</th>
                            <th style="text-align: right;">Identificador</th>
                            <th>Nome/Descrição</th>
                            <th>Justificado Por</th>
                            <th>Reservado Para</th>
                            <th style="text-align: right;">Status</th>
                            <th style="text-align: center;"><?php echo $msg_data; ?></th>
                            <th style="width: 40px;">Ações</th>
                        </tr>
                    </tfoot>
                    <tbody id="tb_trilhas">
                        
                    </tbody>
                </table>

                
            </div>
        </div>
    </div>
</div>

<div id="popup_redistribuicao" title="Redistribuir Registro" style="overflow: hidden;">
    <p class="validateTips">Selecione o novo servidor que fará a análise.</p>
    <form id="form_redistribuicao" name="form1" role="form">
        
        <div>
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Servidor</th>
                        <th>Perfil</th>
                        <th>Registros</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="13_tb_usuarios">

                </tbody>
            </table>
        </div>
        
        <input type="hidden" name="CodRegistro" id="13_CodRegistro" value="" />
        <input type="hidden" name="ReservadoPor" id="13_ReservadoPor" value="" />
        <input type="hidden" name="CodTipo" id="13_CodTipo" value="" />
        <input type="hidden" name="operacao" id="13_operacao" value="" />

        <hr>
        <div style="text-align: right;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#popup_redistribuicao").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        </div>
    </form>
</div>


<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');