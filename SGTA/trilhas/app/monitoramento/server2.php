<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");

require_once("../../obj/monitoramento.php");
require_once("../../obj/usuario.php");

$obj_monitoramento = new monitoramento();
$obj_usuario = new usuario();

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$cpf = $_SESSION['sessao_id'];

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    switch ($array["operacao"]) {
        case "load":
            
            $qtd = $obj_monitoramento->consulta_pendentes($prazo_legal_pre, $prazo_legal_pos);
            if($obj_monitoramento->erro != ""){
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }
            $row = $obj_monitoramento->query;
            
            $html = "";
            
            if($qtd > 0){
                for ($index = 0; $index < count($row); $index++){

                    $row[$index]['CodTrilha'] = base64_encode($row[$index]['CodTrilha'] * 2999);
                    
                    if($row[$index]['TipoTrilha'] == "pós"){
                        $prazo = $prazo_legal_pos + $row[$index]["Prorrogado"];
                    }else{
                        $prazo = $prazo_legal_pre + $row[$index]["Prorrogado"];
                    }
                    
                    $hoje = new DateTime();
                    $publicacao = new DateTime($row[$index]['DataPublicacao']);
                    $dias = $publicacao->diff($hoje);
                    $dias = $dias->days;
                    
                    $nome = $obj_usuario->consulta_nome($row[$index]['ReservadoPor']);
                    
                    if($dias > $prazo){
                        $html = $html.
                                '
                                <tr>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row[$index]['DescricaoTipo'].'\')" >'.$row[$index]['DescricaoTipo'].'</a></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row[$index]['NomeTrilha'].'\')" >'.$row[$index]['NomeTrilha'].'</a></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row[$index]['SiglaOrgao'].'\')" >'.$row[$index]['SiglaOrgao'].'</a></td>
                                    <td style="text-align: right;">'.$row[$index]['CPF_CNPJ_Proc'].'</td>
                                    <td>'.$row[$index]['Nome_Descricao'].'</td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$nome.'\')" >'.$nome.'</a></td>
                                    <td style="text-align: center;">'.data_br($row[$index]['DataPublicacao']).'</td>
                                    <td style="text-align: right;">'.$prazo.'</td>
                                    <td style="text-align: right;">'.$dias.'</td>    
                                    <td><a href="#" title="Abre o Registro em um Nova Aba" class="consultar" onclick="return abrir_registro(0, \''.$row[$index]['CodTrilha'].'\', \''.$row[$index]['NomeTrilha'].'\', \''.$row[$index]['TabelaDetalhe'].'\', \''.$row[$index]['CPF_CNPJ_Proc'].'\')"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a></td>
                                </tr>  
                                ';
                    }
                       
                }
            }
            
                
            
            //<a href="#" style="'.$css.'" title="Clique para filtrar." onclick="return filtrar_tabela(\''.$termo.'\')" ></a>
            
            $array['html'] = $html;
            
            if($_SESSION['sessao_perfil'] == 4 || $_SESSION['sessao_perfil'] == 7){
                $array['acao'] = "consulta";
            }elseif($_SESSION['sessao_perfil'] == 5 || $_SESSION['sessao_perfil'] == 6){    
                $array['acao'] = "análise";
            }else{
                $array['acao'] = "validação";
            }
            
            retorno();
            break;
        case "consultar":
            //SGTA-39
            //todos podem pedir prorrogação
           /* if(($_SESSION['sessao_perfil'] == 5) || $_SESSION['sessao_perfil'] == 1 || $_SESSION['sessao_perfil'] == 2){
                //continua
                //$_SESSION['sessao_funcao'] == 12 && 
            }else{
                throw new Exception("Nivel minimo requerido para a realização desta ação: Supervisor");
            }
            */
            $qtd = $obj_monitoramento->consulta_pendentes($prazo_legal_pre, $prazo_legal_pos);
            if($obj_monitoramento->erro != ""){
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }
            $row = $obj_monitoramento->query;
            
            $html = "";
            
            if($qtd > 0){
                for ($index = 0; $index < count($row); $index++){

                    $row[$index]['CodTrilha'] = base64_encode($row[$index]['CodTrilha'] * 2999);
                    
                    if($row[$index]['TipoTrilha'] == "pós"){
                        $prazo = $prazo_legal_pos;
                    }else{
                        $prazo = $prazo_legal_pre;
                    }
                    
                    $hoje = new DateTime();
                    $publicacao = new DateTime($row[$index]['DataPublicacao']);
                    $dias = $publicacao->diff($hoje);
                    $dias = $dias->days;
                    
                    $nome = $obj_usuario->consulta_nome($row[$index]['ReservadoPor']);
                    
                    $resto = 30 - $row[$index]['Prorrogado'];
                   // SGTA-39
                    //pode pedir mais prorrogações ou acima de 30 dias
                    //  if($resto < 1){
                       // $atributos = 'disabled="disabled"';
                    //}else{
                        $atributos = 'checked="checked"';
                    //}

                    $html = $html.
                            '
                            <tr>
                                <td><input type="checkbox" name="registro[]" value="'.$row[$index]['CodRegistro'].'" '.$atributos.'/></td>
                                <td>'.$row[$index]['NomeTrilha'].'</td>
                                <td style="text-align: right;">'.$row[$index]['CPF_CNPJ_Proc'].'</td>
                                <td>'.$row[$index]['Nome_Descricao'].'</td>
                                <td style="text-align: right;">'.$dias.'</td>    
                                <!--<td style="text-align: right;">'.$resto.'</td>-->
                            </tr>  
                            ';
                }
            }
            
            $array['html'] = $html;
            
            retorno();
            break;
        case "prorrogar":

            //SGTA-92
            if($_SESSION['sessao_perfil'] == 1){
                throw new Exception("Administradores não podem solicitar prorrogoção do prazo.");
            }

            if(!isset($_POST['registro'])){
                 throw new Exception("Não há nenhum registro selecionado para se prorrogar, selecione pelo menos 1 registro.");
            }
            
            $registro = $_POST['registro'];
            $motivo = trim($_POST['motivo']);
            $dias = $_POST['dias'];
         
            
            if($motivo == ""){
                throw new Exception("É obrigatório o preenchimento do campo ''Justificativa''.");
            } 
            
            $n = 0;
            foreach( $registro as $key => $CodRegistro ) {
                $registros[] = $CodRegistro; 
                $n = 1;
            }
            
            if($n == 0){
                throw new Exception("Não há nenhum registro selecionado para se prorrogar.");
            }
            
            require_once("../../obj/pedidos.php");
            $obj_pedido = new pedidos();

        
            //SGTA-39 comentar linha abaixo quando necessario
            $CodPedido = $obj_pedido->cadastra_pedido($dias, $motivo, $registros);
            if($obj_pedido->erro != ""){
                $array['erro'] = $obj_pedido->erro;
                retorno();
            }
            
            if(isset($_FILES['upload_file']['name'])){
                if($_FILES['upload_file']['name'] != ""){
                    $tmpFilePath = $_FILES['upload_file']['tmp_name'];
                    $fileName = $_FILES['upload_file']['name'];
                    $fileName = $fileName = sanitizeString($fileName);
                    $fileName = str_replace(",","",$fileName);
                    $fileName = str_replace(" ","_",$fileName);

                    $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/pedidos/" . $CodPedido;

                    if (!file_exists($targetDir)) {
                        $retorno = mkdir($targetDir);
                    }

                    if($retorno == false){
                        $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                        retorno();
                    }                                    

                    $newFilePath = $targetDir . "/" . $fileName;

                    if (file_exists($newFilePath)) {
                        unlink($newFilePath);
                    }

                    if(move_uploaded_file($tmpFilePath, $newFilePath) == false) {
                        $array['erro'] = "Não foi possível realizar o upload do arquivo: $newFilePath.  ". print_r($_FILES);
                    }
                }
            }

            //SGTA-39 enviar mensagem na prorrogação

            require_once("../../obj/email.php");
            $obj_email = new email();

            require_once("../../obj/registros.php");
            $obj_registros = new registros();

            $assunto = "Pedido(s) de Prorrogação.";
            $mensagem = "Prezado(a), Foi enviado um pedido de prorrogação do processo abaixo:<br>";
        
            for ($index = 0; $index < count($registros); $index++){
               
                $obj_registros->consulta_registro($registros[$index]);
                if($obj_registros->erro == ""){
                
                    $query = $obj_registros->query;
                    $row = mysqli_fetch_array($query);
              
                    $mensagem = $mensagem . "<br><br><i>Informações da Trilha:</i><br><br>";
                    $mensagem = $mensagem . "Código: <b>" .$row['CodTrilha'] . "</b><br>";
                    $mensagem = $mensagem . "Órgão: <b>" . $row['NomeOrgao'] . "</b><br>";
                    $mensagem = $mensagem . "Nome: <b>"  . $row['NomeTrilha'] . "</b><br>";
                    $mensagem = $mensagem . "Registrado com o motivo: <br><br> ". $motivo."<br><br>" ;
                    $mensagem = $mensagem . "Para acessar o sistema ".'<a href = ' .  $_SESSION['virtual'] . '#/sgta" target = "_blank">clique aqui.</a><br><br>';
                    $mensagem = $mensagem . "<i>Este é uma mensagem automática, em caso de dúvidas entre em contato com o suporte.</i>";
                 //sgta-20 enviar
                  //  $obj_email->enviarMensagens( $assunto,  $mensagem , $row['CodOrgao'], -1, $row['CodTipo']); 
                    //die('asdfsa333');
                }
            }


            
          
        
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}

function sanitizeString($str) {
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
    //$str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
    //$str = preg_replace('/[^a-z0-9]/i', '_', $str);
    //$str = preg_replace('/_+/', '_', $str); // ideia do Bacco :)
    return $str;
}
    

function retorno(){
    global $array;
       
    usleep(100000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}
