<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");

require_once("../../obj/monitoramento.php");
require_once("../../obj/usuario.php");

$obj_monitoramento = new monitoramento();
$obj_usuario = new usuario();

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$cpf = $_SESSION['sessao_id'];

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    switch ($array["operacao"]) {
        case "load":
            
            if($_POST['i'] == 2){
                $obj_monitoramento->consulta_nao_justificados();
            }else{
                require_once("../../obj/andamentos.php");
                $obj_andamento = new andamentos();
                $obj_monitoramento->consulta_nao_analisados();
            }
            
            if($obj_monitoramento->erro != ""){
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }
            $query = $obj_monitoramento->query;
            
            $html = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];
            
            if(is_int($query) == true){
                goto fim;
            }
            
            if(mysqli_num_rows($query) > 0){
                while ($row = mysqli_fetch_array($query)){
                    
                    $_CodTrilha = $row['CodTrilha'];
                    $row['CodTrilha'] = base64_encode($row['CodTrilha'] * 2999);

                    $usuario = $obj_usuario->consulta_nome($row['UsuarioOrgao']);
                    $nome = $obj_usuario->consulta_nome($row['ReservadoPor']);
                    
                    if($CodPerfil <= 3){
                        $acao = "validação";
                        $CodOrgao = "0";
                    }else if($CodPerfil == 5 || $CodPerfil == 6){
                        $acao = "análise";
                        $CodOrgao = $_SESSION['sessao_orgao'];
                    }else{
                        $acao = "consulta";
                        if($CodPerfil == 4){
                            $CodOrgao = "0";
                        }else{
                            $CodOrgao = $_SESSION['sessao_orgao'];
                        }
                    }
                    
                    $dt = "";
                    
                    if($_POST['i'] == 1){
                        $data = data_br($row['DataPublicacao']);
                        //sgta 151
                        $And = " And ( Descricao Like 'Redistribuido o registro de inconsistência para o servidor:%' OR Descricao Like 'Reservado o registro de inconsistência para o servidor:%' ) ";
                        $dr = $obj_andamento->consultar($row['CodRegistro'], $And);
                        if($obj_andamento->erro == ""){
                            if($dr[0]['qtd'] > 0){
                        
                                $dt = "<br>a " . $dr[0]['Dias']." dias";
                            }
                        }else{
                            throw new Exception($obj_andamento->erro);
                        }
                        
                    }else{
                        $data = data_br($row['DataConclusaoJustificativa']);
                    }
                        

                    $vmBtnRedistribuição = "";
                    if($CodPerfil < 3 &&  $_SESSION['redistribui']){
                        $vmBtnRedistribuição = '<a href="#" title="Redistribuir o registro." onclick="return redistribuir_registro('.$row['CodRegistro'].','.$row['ReservadoPor'].','. $row['CodTipo'].');" class="editar"><i class="fa fa-paper-plane fa-lg fa-fw" aria-hidden="true"></i></a> ';
                    }

                    $html = $html. 
                                '
                                <tr>
                                  <td><center><input type="checkbox"  class="chk" value="' . $row['CodRegistro']. "|" .$row['CPF_CNPJ_Proc'] . "|" .$_CodTrilha.  '" id="'.$row['CodRegistro'].'"></center></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['DescricaoTipo'].'\')" >'.$row['DescricaoTipo'].'</a></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['NomeTrilha'].'\')" >'.$_CodTrilha.' - '.$row['NomeTrilha'].'</a></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['SiglaOrgao'].'\')" >'.$row['SiglaOrgao'].'</a></td>
                                    <td style="text-align: left;">'.$row['CPF_CNPJ_Proc'].'</td>
                                    <td>'.$row['Nome_Descricao'].'</td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$usuario.'\')" >'.$usuario.'</a></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$nome.'\')" >'.$nome.$dt.'</a></td> 
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['StatusRegistro'].'\')" >'.$row['StatusRegistro'].'</a></td>    
                                    <td style="text-align: center;">'.$data.'</td>
                                    <td>
                                        <center>
                                            ' . $vmBtnRedistribuição . '
                                            <a href="#" title="Abrir Registro" class="consultar" onclick="return abrir_registro('.$CodOrgao.', \''.$row['CodTrilha'].'\', \''.$row['NomeTrilha'].'\', \''.$row['TabelaDetalhe'].'\', \''.$row['CPF_CNPJ_Proc'].'\',\''.$acao.'\')"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
                                        </center>
                                    </td>
                                </tr>';
                }
            }
            
fim:
            $array['html'] = $html;
            
            if($_SESSION['sessao_perfil'] == 4){
                $array['acao'] = "consulta";
            }else{
                $array['acao'] = "validação";
            }
            
            retorno();
            break;
        case "redistribuir_consulta":
            $CodRegistro = $_POST['CodRegistro'];
            $CodTipo = $_POST['CodTipo'];
            
            $html = "";
            
            require_once("../../obj/monitoramento.php");
            $obj_monitoramento = new monitoramento();
            $tab = $obj_monitoramento->selecionar_usuarios($unidade_controle, $CodTipo);
            if($obj_monitoramento->erro != ""){
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }
            
            if($tab != false){
                for ($index = 0; $index < count($tab); $index++){
                    
                    $html = $html.
                    '<tr>
                        <td>'.$tab[$index]['Nome'].'</td>
                        <td>'.$tab[$index]['NomePerfil'].'</td>
                        <td style="text-align: right">'.$tab[$index]['qtd'].'</td>
                        <td><a href="#" class="editar" title="Redistribuir para este Auditor" tabIndex="-1" onclick="return redistribuir_para('.$CodRegistro.','.$tab[$index]['cpfSGI'].',\''.$tab[$index]['Nome'].'\')"><i class="fa fa-hand-o-up fa-lg" aria-hidden="true"></i></a></td>
                    </tr>';
                }   
            }
            
            $array['html'] = $html;
            retorno();
        break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
        break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}
    
function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}
