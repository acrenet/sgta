
<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    require_once("../../obj/autorizacoes.php");
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    $titulo = "Consulta Registros Justificados Aguardando Distribuição.";

    if($perfil > 2){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Trilhas - Registros Justificados Aguardando Distribuição";
    

    $ObjAut = new autorizacoes();

    $autorizacoes = $ObjAut->trilhas_autorizadas_usuario($_SESSION['sessao_id']);

    $vmQtdAnalise = "1";
    if(isset($_POST['id'])){
        $vmQtdAnalise = count($_POST['id']);
    }
?>

<script src="distribuicao.js" type="text/javascript"></script>

<form name="form1" id="form1" action='../registros/exibir.php' method="POST" target="_blank">
    <input type="hidden" name="operacao" id="operacao" value="" />
    <input type="hidden" name="filtro" id="filtro" value="" />
    <input type="hidden" name="Orgao" id="Orgao" value="" />
    <input type="hidden" name="CodTrilha" id="CodTrilha" value="" />
    <input type="hidden" name="CodRegistro" id="CodRegistro" value="" />
    <input type="hidden" name="CodTipo" id="CodTipo" value="" />
    <input type="hidden" name="NomeTrilha" id="NomeTrilha" value="" />
    <input type="hidden" name="detalhe" id="detalhe" value="" />
    <input type="hidden" name="acao" id="acao" value="" />
</form>

<br>
<div class="container-fluid">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4 id="h_titulo" ><?php echo $titulo; ?></h4></div>
            <div class="panel-body">
            <table  width="100%" class="ui-widget-header dataTables_wrapper dt-jqueryui ui-corner-all " style="box-sizing: border-box; display: block; padding: 8px; ">
                        <tr>
                            <td colspan=12 >
                             Selecionar registros por trilha:   
                                <select name="trilhaSel" id="trilhaSel" onchange="myFunction()">
                                    <option value="0" selected>Selecione...</option> 
                                    <?php   
                                         foreach($autorizacoes as $row){
                                                echo '<option value="' .$row['CodTrilha'] . '" >'.$row['CodTrilha'] . " - ". $row['NomeTrilha'] . '</option>';
                                         }
                                   ?>
                                </select>
                            </td>   
                        </tr>
              </table>    

            <table id="tbl_trilhas" class="display" width="100%">
                    <thead>
                        <tr>
                            <th>Seleção</th>
                            <th>Área</th>
                            <th>Cod Trilha</th>
                            <th>Trilha</th>
                            <th>Órgão</th>
                            <th style="text-align: left;">Identificador</th>
                            <th>Nome/Descrição</th>
                            <th>Justificado Por</th>
                            <th style="text-align: center;">Dt Publicação</th>
                            <th style="text-align: center;">Dt Justificativa</th>
                            <th style="width: 40px;"></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Seleção</th>
                            <th>Área</th>
                            <th>Cod Trilha</th>
                            <th>Trilha</th>
                            <th>Órgão</th>
                            <th style="text-align: right;">Identificador</th>
                            <th>Nome/Descrição</th>
                            <th>Justificado Por</th>
                            <th style="text-align: center;">Dt Publicação</th>
                            <th style="text-align: center;">Dt Justificativa</th>
                            <th style="width: 40px;"></th>
                        </tr>
                    </tfoot>
                    <tbody id="tb_trilhas">
                        
                    </tbody>
                </table>

                
            </div>
        </div>
    </div>
</div>

<div id="popup_form" title="Distribuir Registro">
    <p class="validateTips">Selecione o servidor que fará a análise do(s) registro(s)</p>
    <form id="form2" name="form1" role="form">
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
                    <th>Servidor</th>
                    <th>Perfil</th>
                    <th>Registros</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="tb_usuarios">
                
            </tbody>
        </table>

        <hr>
        <div style="text-align: right;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#popup_form").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        </div>
    </form>
</div>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');