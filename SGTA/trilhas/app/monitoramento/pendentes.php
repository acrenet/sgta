
<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if(($perfil == 4 || $perfil == 7)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Trilhas - Registros não Justificados";
    
?>

<link href="form_upoload.css" rel="stylesheet" type="text/css"/>
<script src="pendentes.js" type="text/javascript"></script>

<form name="form1" id="form1" action='../registros/exibir.php' method="POST" target="_blank">
    <input type="hidden" name="operacao" id="operacao" value="" />
    <input type="hidden" name="filtro" id="filtro" value="" />
    <input type="hidden" name="Orgao" id="Orgao" value="" />
    <input type="hidden" name="CodTrilha" id="CodTrilha" value="" />
    <input type="hidden" name="NomeTrilha" id="NomeTrilha" value="" />
    <input type="hidden" name="detalhe" id="detalhe" value="" />
    <input type="hidden" name="acao" id="acao" value="" />
</form>

<br>
<div class="container-fluid">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Consulta Registros de Inconsistência não Justificados no Prazo Legal</h4></div>
            <div class="panel-body">
                
                <button type="button" class="btn btn-danger" onclick="popup_prorrogar();">Solicitar Prorrogação do Prazo</button>
                <br><br>
                <table id="tbl_trilhas" class="display" width="100%">
                    <thead>
                        <tr>
                            <th>Área</th>
                            <th>Trilha</th>
                            <th>Órgão</th>
                            <th style="text-align: right;">CPF/CNPJ/Proc</th>
                            <th>Nome/Descrição</th>
                            <th>Reservado Por</th>
                            <th style="text-align: center;">Dt Publicação</th>
                            <th style="text-align: right;">Prazo Legal + Prorrogações</th>
                            <th style="text-align: right;">Dias Passados</th>
                            <th style="width: 40px;"></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Área</th>
                            <th>Trilha</th>
                            <th>Órgão</th>
                            <th style="text-align: right;">CPF/CNPJ/Proc</th>
                            <th>Nome/Descrição</th>
                            <th>Reservado Por</th>
                            <th style="text-align: center;">Dt Publicação</th>
                            <th style="text-align: right;">Prazo Legal + Prorrogações</th>
                            <th style="text-align: right;">Dias Passados</th>
                            <th style="width: 40px;"></th>
                        </tr>
                    </tfoot>
                    <tbody id="tb_trilhas">
                        
                    </tbody>
                </table>

                
            </div>
        </div>
    </div>
</div>

<div id="popup_form" title="Solicitar Prorrogação de Prazo">
    <form id="form2" name="form2" class="form-horizontal" role="form">
        
        <div id="7_row1" class="row">
            <div class="col-sm-4">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="7_nome">Quantidade de Dias:</label>
                    <!-- SGTA-31 -->
                    <?php
                     echo '<input type="number" name="dias" id="dias" class="form-control" min="1" max="' . @$_SESSION['diasMaxioProrrogacao'] . '"  style="width: 70px;"  onkeyup="nop180()">'; 
                     echo '<input type="hidden" name="diasMaxioProrrogacao" id="diasMaxioProrrogacao" value="' .@$_SESSION['diasMaxioProrrogacao'] . '" />';
                     ?>
                </div>
            </div>
        </div>

        <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
            <label for="motivo">Justificativa:</label>
            <textarea name="motivo" id="7_motivo" rows="5" cols="20" class="form-control input-sm" onkeyup="contar_caracteres('7_motivo', '7_contador', 2000)" maxlength="2000" placeholder="(obrigatório)"></textarea>
            <input type="text" class="form-control" id="7_contador" name="contador" style="width: 70px; float: right;" disabled="">
        </div>

        <div class="form-group"> 
            <div class="col-sm-offset-0 col-sm-12">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-primary btn-file"><span class="fileupload-new">Anexar Arquivo (Opcional) &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>
                        <span class="fileupload-exists">Alterar &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>         <input id="uploadfile" name="upload_file" type="file" onchange="validar_arquivo()"/></span>
                    <span class="fileupload-preview"></span>
                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                </div>
            </div>
        </div>
        
        <hr>
        <div style="text-align: right;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#popup_form").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
            <button type="button" class="btn btn-success" onclick='prorrogar_prazo();'>Salvar e Enviar &nbsp;<span class="fa fa-save"></span></button><br>
        </div>
        <br>
        <p class="validateTips"><b>Desmarque os registros que não gostaria de solicitar prorrogação de prazo.</b></p>
        <table class="table table-striped table-hover table-condensed" style="font-size: 90%;">
            <thead>
                <tr>
                    <th></th>
                    <th>Trilha</th>
                    <th>CPF/CNPJ/Proc</th>
                    <th>Nome/Descrição</th>
                    <th>Dias Passados</th>
                   <!-- <th>Prazo Máximo a Pedir</th>-->
                </tr>
            </thead>
            <tbody id="tb_usuarios">
                
            </tbody>
        </table>

        <input type="hidden" name="operacao" id="operacao2" value="prorrogar" />
        
    </form>
</div>


<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');