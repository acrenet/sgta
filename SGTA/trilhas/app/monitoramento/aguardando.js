var minha_tabela;
var destino = "server3.php";
var pos = 7;

var ordem = [
                [8, "asc" ]
            ];

var data = [
            null,
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "date-uk"},
            null
           
        ];
            
$(document).ready(function(){
  
    popup_redistribuicao_();
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
    
    $( document ).tooltip({
        track: true,
        tooltipClass: "custom-tooltip-styling"
    });
});


function popup_redistribuicao_(){
    $("#popup_redistribuicao").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 'auto',
        maxWidth: 900,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}


function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                
                $("#acao").val(retorno.acao);
                $("#tb_trilhas").html(retorno.html);
                datatable_("tbl_trilhas", 10, ordem, -1, data, $("#h_titulo").text(), $("#h_titulo").text()); 
                
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../inicio/inicio.php");
            break;
        case 2:
            $("#operacao").val("update");
            formdata = $("#form1").serialize();
            submit_form(destino);
            break;
        case 3:
            $("#operacao").val("load");
            formdata = $("#form1").serialize();
            submit_form(destino);
            break;
    }
    execute_cod = 0;
}

function abrir_registro(CodOrgao, CodTrilha, NomeTrilha, TabDetalhe, Filtro, Acao){
    
    $("#Orgao").val(CodOrgao);
    $("#CodTrilha").val(CodTrilha);
    $("#NomeTrilha").val(NomeTrilha);
    $("#detalhe").val(TabDetalhe);
    $("#filtro").val(Filtro);
    $("#acao").val(Acao);
    
    //console.log(CodOrgao + " " + CodTrilha + " " + NomeTrilha + " " + TabDetalhe + " " + Filtro + " " + Acao);
    document.getElementById("form1").submit();
    return false;
}

function redistribuir_registro(CodRegistro, ReservadoPor, CodTipo){

    var elements = document.getElementsByClassName('input_field');
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }

    /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
	$(".chk:checked").each(function() {
        // chkArray.push($(this).val());
         $('#form_redistribuicao').append(
             $('<input>')
                 .attr('class', 'input_field ')
                 .attr('type', 'hidden')
                 .attr('name', 'id[]')
                 .attr('id', $(this).val())
                 .val($(this).val())//so chora
         );
     });

    $("#13_CodRegistro").val(CodRegistro);
    $("#13_CodTipo").val(CodTipo);
    $("#13_ReservadoPor").val(ReservadoPor);
    $("#13_operacao").val("redistribuir_consulta");
    formdata = $("#form_redistribuicao").serialize();
    submit_data("server.php");
    return false;
}

function retorno_redistribuir_consulta(retorno){
    $("#13_tb_usuarios").html(retorno.html);
    $("#popup_redistribuicao").dialog("open");
    return false;
}

function redistribuir_para(CodRegistro, cpf, nome){
    $("#13_operacao").val("redistribuir_para");
    $("#13_CodRegistro").val(CodRegistro);
    $("#13_CodTipo").val(cpf);
    $("#13_ReservadoPor").val(nome);
    formdata = $("#form_redistribuicao").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").text("Você está certo desta redistribuição para: " + nome);
    $("#popup_pergunta").dialog("open");
    return false;
}

async  function retorno_redistribuir_para(retorno){
    $("#popup_redistribuicao").dialog("close");
    $("#msg_sucesso").text("Redistribuição efetuada com sucesso.");
    $("#popup_sucesso").dialog("open");
    await sleep(2000);
    document.location.reload();

}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function submit_data(destino){
 
    $("#popup_dialogo").dialog("close");
    $("#loading").show();
    $("#imgloader").show();
    $.ajax({
        type: 'post',
        url: destino,
        data: formdata,
        cache: false,
        timeout: 300000,
        success: function(data){
            try{
                try{
                    
                    data = $.parseJSON(data);
                }catch(err){
                    console.log('veio au3i');
                    $("#msg_erro").html("ERRO AO DECODIFICAR O PACOTE RECEBIDO DO SERVIDOR: ->" + data + "<-");
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.noticia !== ""){
                    alert("Notícia: " + data.noticia);
                    return false;
                }
                if(data.erro !== ""){
                    $("#msg_erro").html(data.erro);
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.alerta !== ""){
                    $("#msg_alerta").html(data.alerta);
                    $("#popup_alerta").dialog("open");
                    return false;
                }
               
                server(data);
               
                    
            }
            catch(err){
                $("#msg_erro").html(err + "   " + data);
                $("#popup_erro").dialog("open");
                return false;
            } 
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            $("#msg_erro").html(errorThrown);
            $("#popup_erro").dialog("open");
            return false;
        },
        complete: function(XMLHttpRequest, status){
            $("#loading").hide();
            $("#imgloader").hide();
            return false;
        }
    });
};


function myFunction2(){

    $(".chk").each(function() {
            var aux = null;
            $(this)[0].checked =false;
            aux = $(this).val().split("|");
            if(aux[2]  == $("#trilhaSel").val() ){
                $(this)[0].checked =true;
            }
            
    });
}


function pergunta_ok(){
    
    switch (pergunta_ok_acao){
        case 1:
            $("#popup_pergunta").dialog("close");
            submit_data("server.php");
            break;
        case 2:
            submit_arquivo("form_upload/server.php");
            break;
        case 3:
            submit_data("server.php");
            break;    
        default:
            alert("js: Operação Inválida.");
    };
    
        
}


function server(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "observacao_cge":
                retorno_info(retorno);
                break;
            case "gravar_observacao_cge":
                retorno_observacao_cge(retorno);
                break;
            case "listar_anexos":
                retorno_visualizar_anexos(retorno);
                break;
            case "upload":
                retorno_subir_arquivo(retorno);
                break;
            case "upload_trilha":
                retorno_subir_arquivo(retorno);
                break;
            case "justificar_inconsistencia":
                retorno_justificar_inconsistencia(retorno);
                break;
            case "consultar_justificativa":
                retorno_consultar_justificativa(retorno);
                break;
            case "concluir_analise":
                retorno_analise(retorno);
                break;
            case "salvar_analise":
                retorno_analise(retorno);
                break;    
            case "recusar_trilha":
                execute_cod = 1;
                $("#msg_sucesso").html("Trilha recusada com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "enviar_trilha":
                execute_cod = 2;
                
                var msg = "Trilha distribuída aos órgãos com sucesso.<br>";
                
                if(retorno.erro_email != ""){
                    msg = msg + retorno.erro_email;
                }
                
                if(retorno.alerta_email != ""){
                    msg = msg + retorno.alerta_email;
                }
                
                msg = msg + retorno.mensagem_email;
                 
                $("#msg_sucesso").html(msg);
                $("#popup_sucesso").dialog("open");
                execute();
                break;
            case "rejeitar_registro":
                execute_cod = 2;
                $("#msg_sucesso").html("Registro Rejeitado com Sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "reincluir_registro":
                execute_cod = 2;
                $("#msg_sucesso").html("Registro Re-incluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "excluir_anexo":
                execute_cod = 2;
                $("#msg_sucesso").html("Arquivo excluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "excluir_arquivo":
                execute_cod = 2;
                $("#popup_anexos").dialog("close");
                $("#msg_sucesso").html("Arquivo excluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "analisar_justificativa":
                analisar_justificativa_retorno(retorno);
                break;
            case "salvar_validacao":
                retorno_salvar_validacao(retorno);
                break;
            case "andamento":
                consulta_andamento_retorno(retorno);
                break;
            case "consulta_justificativas":
                consulta_justificativas_retorno(retorno);
                break;
            case "solicitar_alteracao_justificativa":
                retorno_alteracao(retorno);
                break;
            case "analisar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "gravar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "aceitar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "negar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "salvar_instrucao":
                retorno_salvar_instrucao();
                break;
            case "consulta_complementar":
                retorno_acoes_complementares(retorno);
                break;
            case "acoes_complementares":
                retorno_salvar_complementar(retorno);
                break;
            case "ordem_servico":
                retorno_consulta_ost(retorno);
                break;
            case "mesclar_consulta":
                retorno_mesclar(retorno);
                break;
            case "mesclar_save":
                retorno_mesclar_save(retorno);
                break;    
            case "consultar_msg":
                retorno_visualizar_mensagens(retorno);
                break;
            case "equipe_msg":
                retorno_cadastrar_msg(retorno);
                break;
            case "cadastrar_msg":
                retorno_salvar_msg(retorno);
                break;
            case "salvar_msg":
                retorno_salvar_msg(retorno);
                break;
            case "editar_msg":
                retorno_alterar_msg(retorno);
                break;
            case "excluir_msg":
                retorno_excluir_msg(retorno);
                break;
            case "consultar_dist":
                retorno_distribuir_registro(retorno);
                break;
            case "vincular_dist":
                retorno_vincular_registro(retorno);
                break;
            case "concluir_pre_analise":
                retorno_concluir_pre_analise(retorno);
                break;
            case "auditoria_consulta":
                retorno_auditoria_consulta(retorno);
                break;
            case "auditoria_pre_analise":
                retorno_auditoria_pre_analise(retorno);
                break;
            case "redistribuir_consulta":
                retorno_redistribuir_consulta(retorno);
                break;
             case "redistribuir_para":
                retorno_redistribuir_para(retorno);
                break;  
            case "salvar_mensagem_trilha":
                retorno_salvar_mensagem_trilha(retorno);
                break;
            case "alterar_mensagem_trilha":
                retorno_alterar_mensagem_trilha(retorno);
                break;
            case "excluir_mensagem_trilha":
                retorno_excluir_mensagem_trilha(retorno);
                break;
            case "mostrar_observacao":
                retorno_alterar_observacao(retorno);
                break;
            case "ocultar_observacao":
                retorno_alterar_observacao(retorno);
                break;    
            default:
                alert("js: Operação Inválida --> ");
        };
    }
}