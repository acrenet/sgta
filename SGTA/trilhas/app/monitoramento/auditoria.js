var minha_tabela;
var destino = "server5.php";
var pos = 7;
var execute_cod = 0;

var ordem = [
                [6, "asc" ]
            ];

var data = [
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            null,
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "date-uk"},
            {"sType": "string"},
            null
        ];
          
$(document).ready(function(){
    
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
    
    $( document ).tooltip({
        track: true,
        tooltipClass: "custom-tooltip-styling"
    });
    
    popup_form_();

});

function popup_form_(){
    $("#popup_form").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 'auto',
        maxWidth: 800,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "modificar":
                minha_tabela.destroy();
                $("#popup_form").dialog("close");
                $("#msg_sucesso").text("Informações gravadas com sucesso.");
                $("#popup_sucesso").dialog("open");
                
            case "load":
                
                $("#acao").val(retorno.acao);
                $("#tb_trilhas").html(retorno.html);
                datatable_("tbl_trilhas", 8, ordem, -1, data, $("#h_titulo").text(), $("#h_titulo").text()); 
                
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            
            break;
        case 2:
            
            break
    }
    execute_cod = 0;
}

function abrir_registro(CodOrgao, CodTrilha, NomeTrilha, TabDetalhe, Filtro, Acao){
    
    $("#Orgao").val(CodOrgao);
    $("#CodTrilha").val(CodTrilha);
    $("#NomeTrilha").val(NomeTrilha);
    $("#detalhe").val(TabDetalhe);
    $("#filtro").val(Filtro);
    $("#acao").val(Acao);
    
    //console.log(CodOrgao + " " + CodTrilha + " " + NomeTrilha + " " + TabDetalhe + " " + Filtro + " " + Acao);
    
    document.getElementById("form1").submit();
    return false;
}

function responder_pedido(CodRegistro, CodTipo){
    $("#numprocesso").val("");
    $("#resposta").val("");
    $("#opt1").prop("checked", true);
    $("#CodRegistro2").val(CodRegistro);
    $("#popup_form").dialog("open");
    return false;
}

function gravar_registro(){
    $("#operacao2").val("modificar");
    formdata = $("#form2").serialize();
    $("#msg_dialogo").text("Você está certo desta decisão?");
    $("#popup_dialogo").dialog("open");
}