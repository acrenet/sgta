<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");

require_once("../../obj/monitoramento.php");
require_once("../../obj/registros.php");
require_once("../../obj/email.php");
require_once("../../obj/usuario.php");
require_once("../../obj/ordem_servico.php");


$obj_monitoramento = new monitoramento();

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$cpf = $_SESSION['sessao_id'];

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    switch ($array["operacao"]) {
        case "load":
            
            $obj_monitoramento->consulta_monitoramento();
            if($obj_monitoramento->erro != ""){
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }
            $query = $obj_monitoramento->query;
            
            $html = "";
            
            if(mysqli_num_rows($query)>0){
                while ($row = mysqli_fetch_array($query)){
                    
                    
                    if($row['TipoTrilha'] == "pós"){
                        $prazo = $prazo_legal_pos;
                    }else{
                        $prazo = $prazo_legal_pre;
                    }

                    $one= new DateTime($row['QuarentenaAte']);
                    $two = new DateTime();

                    if($two > $one){
                        $prazo_estourado = true;
                    }else{
                        $prazo_estourado = false;
                    }

                    if($prazo_estourado == true){
                        $css = "color: red;";
                        $termo = "vencido";
                    }else{
                        $css = "";
                        $termo = "a vencer";
                    }
                    
                    if($row['ReservadoNome'] != ""){
                        $nome = $row['ReservadoNome'];
                    }else{
                        $nome = $row['Nome'];
                    }
                    
                    if($row['DataConclusaoAnalise'] == ""){
                        $nome = $nome ." ***em análise***";
                    }
                    
                    $row['CodTrilha'] = base64_encode($row['CodTrilha'] * 2999);
                    
                    $html = $html.
                            '
                            <tr>
                                <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['DescricaoTipo'].'\')" >'.$row['DescricaoTipo'].'</a><span style="display: none;">'.$termo.'</span></td>
                                <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['NomeTrilha'].'\')" >'.$row['NomeTrilha'].'</a></td>
                                <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['SiglaOrgao'].'\')" >'.$row['SiglaOrgao'].'</a></td>
                                <td style="text-align: right;">'.$row['CPF_CNPJ_Proc'].'</td>
                                <td>'.$row['Nome_Descricao'].'</td>
                                <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$nome.'\')" >'.$nome.'</a></td>
                                <td style="text-align: center;">'.data_br($row['DataConclusaoAnalise']).'</td>
                                <td style="text-align: center; '.$css.'">'.data_br($row['QuarentenaAte']).'</td>
                                <td><a href="#" title="Abrir Registro" class="consultar" onclick="return abrir_registro(0, \''.$row['CodTrilha'].'\', \''.$row['NomeTrilha'].'\', \''.$row['TabelaDetalhe'].'\', \'monitoramento '.$row['Nome'].'\')"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a></td>
                            </tr>  
                            ';
                }
            }
            
            //<a href="#" style="'.$css.'" title="Clique para filtrar." onclick="return filtrar_tabela(\''.$termo.'\')" ></a>
            
            $array['html'] = $html;
            
            if($_SESSION['sessao_perfil'] == 4){
                $array['acao'] = "consulta";
            }else{
                $array['acao'] = "validação";
            }
            
            retorno();
            break;

        case "redistribuir_consulta":
       
            $CodRegistro = $_POST['CodRegistro'];
            $CodTipo = $_POST['CodTipo'];
            
            $html = "";
        
            
            $obj_monitoramento = new monitoramento();
     
            require_once("../../obj/monitoramento.php");
           
            $tab = $obj_monitoramento->selecionar_usuarios($unidade_controle, $CodTipo);
            if($obj_monitoramento->erro != ""){
     
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }
       
            if($tab != false){
                for ($index = 0; $index < count($tab); $index++){
                    
                    $html = $html.
                    '<tr>
                        <td>'.$tab[$index]['Nome'].'</td>
                        <td>'.$tab[$index]['NomePerfil'].'</td>
                        <td style="text-align: right">'.$tab[$index]['qtd'].'</td>
                        <td><a href="#" class="editar" title="Redistribuir para este Auditor" tabIndex="-1" onclick="return redistribuir_para('.$CodRegistro.','.$tab[$index]['cpfSGI'].',\''.$tab[$index]['Nome'].'\')"><i class="fa fa-hand-o-up fa-lg" aria-hidden="true"></i></a></td>
                    </tr>';
                }   
            }
            //die($html);
            $array['html'] = $html;
            
            retorno();
        break;    
        case "redistribuir_para":

           
            $CodRegistro = $_POST['CodRegistro'];
            $cpf = $_POST['CodTipo'];
            //sgta 151
            if(isset($_POST['id'])){
        
                $ids = @$_POST['id']; 
               
                foreach ($ids as $key => $registro) {
                       
                        $values = explode("|",$registro);    
                     
                        redistribuir_para($values[0], $cpf,$array);
                }

            } else {
           
                redistribuir_para( $CodRegistro,$cpf, $array);
            }
        
            retorno();
        break;  
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}
    
function redistribuir_para($CodRegistro, $cpf,&$array){
    
 
    $obj_registros = new registros();

    $obj_registros->reservar_registro($CodRegistro, $cpf, false, true);
    if($obj_registros->erro != ""){
        $array['erro'] = $obj_registros->erro;
    }
    
   
    $obj_email = new email();

    try{
        $obj_registros->consulta_registro($CodRegistro);

     
        if($obj_registros->erro == ""){
            $query = $obj_registros->query;
            $row = mysqli_fetch_array($query);
            
        
            $obj_usuario = new usuario();
        
            $obj_usuario->consulta_nome($cpf);
           
            $emaildestinatario = $obj_usuario->email;
            $assunto = "Um registro de inconsistência foi enviado para sua análise.";
            $mensagem = "Detalhes: <br>";
            $mensagem = $mensagem."Trilha: <b>".$row['NomeTrilha']."</b><br>";
            $mensagem = $mensagem."Órgão/Unidade: <b>".$row['NomeOrgao']."</b><br>";
            $mensagem = $mensagem."CPF/CNPJ/Processo: <b>".$row['CPF_CNPJ_Proc']."</b><br>";
            //$mensagem = $mensagem."Para acessar o sistema ".'<a href = "http://sgta.tce.sc.gov.br" target = "_blank">clique aqui.</a><br><br>';
            $mensagem = $mensagem."Grato. ";
            
            $obj_email->enviarEmail($assunto, $mensagem, $emaildestinatario);
        }else{
      
            $obj_email->enviarEmail("Erro no script 04", $obj_registros->erro, "atriches@gmail.com");
        }
    } catch (Exception $exc){
        $obj_email->enviarEmail("Erro no script 03", $exc->getMessage(), "atriches@gmail.com");
        $array['erro'] = "aqui 5";
    
    } 

    $obj_os = new ordem_servico();
    $obj_os->transfere_os($CodRegistro, $cpf);
    if($obj_os->erro != ""){
        $array['erro'] = $obj_os->erro;
    }

    return true;
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}
