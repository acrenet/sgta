<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");

require_once("../../obj/monitoramento.php");
require_once("../../obj/registros.php");
require_once("../../obj/ordem_servico.php");
require_once("../../obj/usuario.php");

$obj_monitoramento = new monitoramento();
$obj_registros = new registros();
$obj_os = new ordem_servico();
$obj_usuario = new usuario();

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$cpf = $_SESSION['sessao_id'];

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);



function envia_notificacao($CodRegistro,$cpf){
    
    require_once("../../obj/monitoramento.php");
    require_once("../../obj/registros.php");
    require_once("../../obj/ordem_servico.php");
    require_once("../../obj/usuario.php");

    $obj_monitoramento = new monitoramento();
    $obj_registros = new registros();
    $obj_os = new ordem_servico();
    $obj_usuario = new usuario();

    $obj_registros->reservar_registro($CodRegistro, $cpf, false);
    if($obj_registros->erro != ""){
        $array['erro'] = $obj_registros->erro;
    
    }
    
    require_once("../../obj/email.php");
    $obj_email = new email();
    
   // try{
    $obj_registros->consulta_registro($CodRegistro);
    if($obj_registros->erro == ""){

        $query = $obj_registros->query;
        $row = mysqli_fetch_array($query);
    
        $obj_usuario->consulta_nome($cpf);

        //andre - email individual ou pra grupo?
        // $emaildestinatario = $obj_usuario->email;
        $emaildestinatario = $obj_usuario->email_geral;
       
        $assunto = "Um registro de inconsistência foi enviado para sua análise.";
        $mensagem = "Detalhes: <br>";
        $mensagem = $mensagem."Trilha: <b>".$row['NomeTrilha']."</b><br>";
        $mensagem = $mensagem."Órgão/Unidade: <b>".$row['NomeOrgao']."</b><br>";
        $mensagem = $mensagem."CPF/CNPJ/Processo: <b>".$row['CPF_CNPJ_Proc']."</b><br><br>";
        $mensagem = $mensagem."Grato.";
  
     
        $obj_email->enviarEmail($assunto, $mensagem, $emaildestinatario);
    }else{
        $obj_email->enviarEmail("Erro no script 04", $obj_registros->erro, "nie@tce.sc.gov.br");
    }


    $obj_os->transfere_os($CodRegistro, $cpf);
    if($obj_os->erro != ""){
        $array['erro'] = $obj_os->erro;
    }
             
}

try {
    
    switch ($array["operacao"]) {
        case "vincular":
       
           // SGTA-144
            if(isset($_POST['id'])){
        
                $ids = @$_POST['id']; 
               
                foreach ($ids as $key => $registro) {
                        $CodRegistro = $cpf = null;
                        $values = explode("|",$registro);    
                        //somente necessita do cpf
                        $cpf = $_POST['filtro'];     
                              
                        envia_notificacao($values[0],$cpf);
                }

            } else {
             
                $cpf = $_POST['filtro'];
                $CodRegistro = $_POST['CodRegistro'];
                envia_notificacao($CodRegistro,$cpf);
 
            }
            if($array['erro'] != ""){
                retorno();
            }
            
        case "load":
            
            $obj_monitoramento->consulta_nao_distribuidos();
            if($obj_monitoramento->erro != ""){
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }
            $query = $obj_monitoramento->query;
            
            $html = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];
            
            if(mysqli_num_rows($query) > 0){
                while ($row = mysqli_fetch_array($query)){
                    
                    $CodTrilha = $row['CodTrilha'];
                    $row['CodTrilha'] = base64_encode($row['CodTrilha'] * 2999);
                    $usuario = $obj_usuario->consulta_nome($row['UsuarioOrgao']);
                    $acao = "consulta";

                    $html = $html. 
                                '
                                <tr>
                                    <td><center><input type="checkbox"  class="chk" value="' . $row['CodRegistro']. "|" .$row['CPF_CNPJ_Proc'] . "|" .$CodTrilha .  '" id="'.$row['CodRegistro'].'"></center></td>
                                    <td style="text-align: right;">'.$CodTrilha.'</td>    
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['DescricaoTipo'].'\')" >'.$row['DescricaoTipo'].'</a></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['NomeTrilha'].'\')" >'.$row['NomeTrilha'].'</a></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['SiglaOrgao'].'\')" >'.$row['SiglaOrgao'].'</a></td>
                                    <td style="text-align: left;">'.$row['CPF_CNPJ_Proc'].'</td>
                                    <td>'.$row['Nome_Descricao'].'</td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$usuario.'\')" >'.$usuario.'</a></td>
                                    <td style="text-align: center;">'.data_br($row['DataPublicacao']).'</td>
                                    <td style="text-align: center;">'.data_br($row['DataConclusaoJustificativa']).'</td>    
                                    <td><a href="#" title="Distribuir Registro" class="editar" onclick="return distribuir_registro('.$row['CodRegistro'].','.$row['CodTipo'].')"><i class="fa fa fa-hand-o-up fa-lg" aria-hidden="true"></i></a> &nbsp
                                        <a href="#" title="Abre o registro em uma nova janela para consulta." class="consultar" onclick="return abrir_registro(0, \''.$row['CodTrilha'].'\', \''.$row['NomeTrilha'].'\', \''.$row['TabelaDetalhe'].'\', \''.$row['CPF_CNPJ_Proc'].'\',\''.$acao.'\')"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a></td>
                                </tr>';
                }
            }
            
            $array['html'] = $html;
            
            if($_SESSION['sessao_perfil'] == 4){
                $array['acao'] = "consulta";
            }else{
                $array['acao'] = "validação";
            }
            
            retorno();
            break;
        case "consultar":
            $CodRegistro = $_POST['CodRegistro'];
            $CodTipo = $_POST['CodTipo'];
            $ids =[];
            $ids =@$_POST['id'];
            $html = "";
            $auxMsg= "";
            if(isset($_POST['id'])){
                $auxMsg = count($ids).' registros ';
            }
            
            $tab = $obj_monitoramento->selecionar_usuarios($unidade_controle, $CodTipo);
            if($obj_monitoramento->erro != ""){
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }
            
            if($tab != false){
                for ($index = 0; $index < count($tab); $index++){
                    
                    $html = $html.
                    '<tr>
                        <td>'.$tab[$index]['Nome'].'</td>
                        <td>'.$tab[$index]['NomePerfil'].'</td>
                        <td style="text-align: right">'.$tab[$index]['qtd'].'</td>
                        <td><a href="#" class="editar" title="Distribuir ' . $auxMsg .' para este Auditor" tabIndex="-1" onclick="return vincular_registro('.$CodRegistro.','.$tab[$index]['cpfSGI'].',\''.$tab[$index]['Nome'].'\')"><i class="fa fa-hand-o-up fa-lg" aria-hidden="true"></i></a></td>
                    </tr>';
                }   
            }
            
            $array['html'] = $html;
            
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}
    
function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}