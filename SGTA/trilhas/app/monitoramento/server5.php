<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");

require_once("../../obj/monitoramento.php");
require_once("../../obj/registros.php");
require_once("../../obj/justificativas.php");
require_once("../../obj/usuario.php");

$obj_monitoramento = new monitoramento();
$obj_registros = new registros();
$obj_usuario = new usuario();
$obj_justificativa = new justificativas();

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$cpf = $_SESSION['sessao_id'];

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    switch ($array["operacao"]) {
        case "modificar":
            
            $CodRegistro = $_POST['CodRegistro'];
            $atividade = $_POST['atividade'];
            $NumProcAuditoria = $_POST['numprocesso'];
            $JustificativaCGE = $_POST['resposta'];
            
            if($atividade == 1){
                $TipoProcesso = "auditoria";
                if(is_numeric($NumProcAuditoria) == false){
                    $array['erro'] = "Informe o número do processo de auditoria.";
                    retorno();
                }
            }elseif($atividade == 2){
                $TipoProcesso = "inspeção";
                if(is_numeric($NumProcAuditoria) == false){
                    $array['erro'] = "Informe o número do processo de inspeção.";
                    retorno();
                }
            }else{
                $TipoProcesso = "arquivar";
            }
            
            $comando = "confirmar_auditoria";
            $UsuarioCGE = 0;
            
            $obj_registros->salvar_analise($comando, $CodRegistro, $JustificativaCGE, 0, false, "", $NumProcAuditoria, $TipoProcesso);
            if($obj_registros->erro != ""){
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            
            $obj_justificativa->incluir_justificativa($CodRegistro, "Resposta à Pedido de Abertura de Auditoria", $JustificativaCGE);
            if($obj_justificativa->erro != ""){
                $array['erro'] = $obj_justificativa->erro;
                retorno();
            }
            
        case "load":
            
            $obj_monitoramento->consulta_pedidos_auditoria();
            if($obj_monitoramento->erro != ""){
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }

            $query = $obj_monitoramento->query;
            $html = "";
            $CodPerfil = $_SESSION['sessao_perfil'];
            
            if(mysqli_num_rows($query) > 0){
                while ($row = mysqli_fetch_array($query)){
                    
                    $row['CodTrilha'] = base64_encode($row['CodTrilha'] * 2999);

                    $usuario = $obj_usuario->consulta_nome($row['UsuarioCGE']);
                    
                    $acao = "consulta";

                    $html = $html.
                                '<tr>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['DescricaoTipo'].'\')" >'.$row['DescricaoTipo'].'</a></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['NomeTrilha'].'\')" >'.$row['NomeTrilha'].'</a></td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$row['SiglaOrgao'].'\')" >'.$row['SiglaOrgao'].'</a></td>
                                    <td style="text-align: right;">'.$row['CPF_CNPJ_Proc'].'</td>
                                    <td>'.$row['Nome_Descricao'].'</td>
                                    <td><a href="#" title="Clique para filtrar." onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$usuario.'\')" >'.$usuario.'</a></td>
                                    <td style="text-align: center;">'.data_br($row['DataConclusaoAnalise']).'</td>
                                    <td>'.str_replace("\r", '<br>', $row['JustificativaCGE']).'</td>    
                                    <td><a href="#" title="Responder Pedido" class="editar" onclick="return responder_pedido('.$row['CodRegistro'].','.$row['CodTipo'].')"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></a> &nbsp
                                        <a href="#" title="Abre o registro em outra janela para consulta" class="consultar" onclick="return abrir_registro(0, \''.$row['CodTrilha'].'\', \''.$row['NomeTrilha'].'\', \''.$row['TabelaDetalhe'].'\', \''.$row['CPF_CNPJ_Proc'].' '.$row['SiglaOrgao']. '\',\''.$acao.'\')"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a></td>
                                </tr>';
                }
            }
            
            $array['html'] = $html;
            
            if($_SESSION['sessao_perfil'] == 4){
                $array['acao'] = "consulta";
            }else{
                $array['acao'] = "validação";
            }
            
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}
    

function retorno(){
    global $array;
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}
