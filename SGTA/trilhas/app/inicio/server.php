<?php
$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
//sgta-36
require_once("../../globais.php");


if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../obj/suporte.php");
require_once("../../obj/trilhas.php");
require_once("../../obj/monitoramento.php");
require_once("../../obj/orgaos.php");
require_once("../../obj/conexao.php");


if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

$labels = array();

$obj_suporte = new suporte();
$obj_trilhas = new trilhas();
$obj_monitoramento = new monitoramento();
$obj_orgaos = new orgaos();
$objcon = new conexao();

try {
    
    switch ($array["operacao"]) {
        case "load":                   //------------------------------------------------------------------------------------------------------
   
            $perfil = $_SESSION['sessao_perfil'];
            
            $array['perfil'] = $perfil;
                        
            if($perfil < 5){ //Administrador, Gestor, Auditor, Consulta CGE
                
                if($perfil < 3){
                 
                    $qtd = $obj_suporte->contar();
                    if($obj_suporte->erro != ""){
                        $array['erro'] = $obj_suporte->erro;
                        retorno();
                    }

                    if($qtd > 0){
                        $array['exibir'] = true;
                        if($qtd == 1){
                            $array['html'] = '<span style="color: red; font-weight: bold;">1 chamada no suporte aguarda resposta.</span> <a href="../suporte/atendimento.php">Clique aqui para visualizar.</a>';
                        }else{
                           $array['html'] = '<span style="color: red; font-weight: bold;">'  .$qtd . ' chamadas no suporte aguardam resposta.</span> <a href="../suporte/atendimento.php">Clique aqui para visualizar.</a>'; 
                        }    
                    }else{
                        $array['exibir'] = false;
                    }
                }
                    
               
                $qtd = $obj_monitoramento->contar_monitoramento();
                if($obj_monitoramento->erro != ""){
                    $array['erro'] = $obj_monitoramento->erro;
                    retorno();
                }
                
                if($qtd > 0){
                    $array['exibir2'] = true;
                    if($qtd == 1){
                        $array['html2'] = '<span style="color: darkorange; font-weight: bold;">1 registro em monitoramento está com o prazo vencido.</span> <a href="../monitoramento/monitoramento.php">Clique aqui para visualizar.</a>';
                    }else{
                        $array['html2'] = '<span style="color: darkorange; font-weight: bold;">' . $qtd . ' registros em monitoramento estão com o prazo vencido.</span> <a href="../monitoramento/monitoramento.php">Clique aqui para visualizar.</a>'; 
                    }    
                }else{
                    $array['exibir2'] = false;
                }
                
            }else{
                $array['exibir'] = false;
                $array['exibir2'] = false;
            }
            
            if($perfil == 8){
                $array['exibir3'] = false;
                $array['exibir4'] = false;
                $array['exibir5'] = false;
                $array['exibir6'] = false;
                $array['exibir7'] = false;
            }else{
              
                $qtd = $obj_monitoramento->contar_pendentes($prazo_legal_pre, $prazo_legal_pos);
               
                if($obj_monitoramento->erro != ""){
                    $array['erro'] = $obj_monitoramento->erro;
                    retorno();
                }

                if($qtd > 0){
                    $array['exibir3'] = true;
                    $array['html3'] = '<span style="color: darkred; font-weight: bold;">' . $qtd . ' registros estão com o prazo de justificativa vencido.</span> <a href="../monitoramento/pendentes.php">Clique aqui para visualizar.</a>'; 
                }else{
                    $array['exibir3'] = false;
                }
                      
                $array['exibir4'] = false;
            
                if($perfil != 3 && $perfil !=4){
                 
                    $qtd = $obj_monitoramento->contar_nao_justificados();
                  
                    if($obj_monitoramento->erro != ""){
                        //die($obj_monitoramento->erro);
                        $array['erro'] = $obj_monitoramento->erro;
                        retorno();
                    }

                    if($qtd > 0){
                        $array['exibir4'] = true;
                        $array['html4'] = '<span style="color: darkgreen; font-weight: bold;">' . $qtd . ' registros estão aguardando justificativa.</span> <a href="../monitoramento/aguardando.php">Clique aqui para visualizar.</a>'; 
                    }else{
                        $array['exibir4'] = false;
                    }
                }
               
                $qtd = $obj_monitoramento->contar_nao_analisados();
              
                if($obj_monitoramento->erro != ""){
                    $array['erro'] = $obj_monitoramento->erro;
                    retorno();
                }

                if($qtd > 0){
                    $array['exibir5'] = true;
                    $array['html5'] = '<span style="color: MediumSlateBlue; font-weight: bold;">' . $qtd . ' registros distribuídos estão aguardando análise.</span> <a href="../monitoramento/aguardando.php?i=2">Clique aqui para visualizar.</a>'; 
                }else{
                    $array['exibir5'] = false;
                }
                
                $qtd = $obj_monitoramento->contar_nao_distribuidos();
                if($obj_monitoramento->erro != ""){
                    $array['erro'] = $obj_monitoramento->erro;
                    retorno();
                }

                if($qtd > 0){
                    $array['exibir6'] = true;
                    $array['html6'] = '<span style="color: red; font-weight: bold;">' . $qtd . ' registros justificados estão aguardando distribuição.</span> <a href="../monitoramento/distribuicao.php">Clique aqui para visualizar.</a>'; 
                }else{
                    $array['exibir6'] = false;
                }


                $qtd = $obj_monitoramento->contar_pedidos_auditoria();
                if($obj_monitoramento->erro != ""){
                    $array['erro'] = $obj_monitoramento->erro;
                    retorno();
                }

                if($qtd > 0){
                    $array['exibir7'] = true;
                    $array['html7'] = '<span style="color: DarkSlateBlue; font-weight: bold;">' . $qtd . ' registros requerem abertura de processo de inspeção/auditoria.</span> <a href="../monitoramento/auditoria.php">Clique aqui para visualizar.</a>'; 
                }else{
                    $array['exibir7'] = false;
                }
            }
                        
            if($perfil < 5){ //somente servidores do TCE
                atualiza_graficos(0, 7);
                
                $areas_perfil = $obj_monitoramento->areas_perfil();
                if($obj_monitoramento->erro){
                    throw new Exception($obj_monitoramento->erro);
                }

                $media = 0;
                $sql[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() ) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha = 'pós' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 1 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha = 'pós' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 2 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha = 'pós' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 3 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha = 'pós' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 4 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha = 'pós' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 5 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha = 'pós' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 6 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha = 'pós' AND StatusRegistro <> 'excluído' $areas_perfil";
                
                $array['linemax'] = 0;

                for ($index = 0; $index < count($sql); $index++){
                    $query = $objcon->select($sql[$index]);
                    if ($objcon->erro != ""){
                        throw new Exception($objcon->erro);
                    }
                    if(mysqli_num_rows($query) == 0){
                        $calc[] = 0;
                    }else{
                        while ($row = mysqli_fetch_array($query)){
                            $media = $media + $row['quantidade_dias'];
                        }
                        $media = $media / mysqli_num_rows($query);
                        $media = round($media);
                        $calc[] = $media;
                        if($media > $array['linemax']){
                            $array['linemax'] = $media;
                        }
                    }
                }

                $media = 0;
                $sql2[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() ) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha <> 'pós' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql2[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 1 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha <> 'pré' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql2[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 2 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha <> 'pré' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql2[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 3 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha <> 'pré' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql2[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 4 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha <> 'pré' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql2[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 5 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha <> 'pré' AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql2[] = "SELECT DataConclusaoJustificativa, DataPublicacao, DATEDIFF (DataConclusaoJustificativa, DataPublicacao) AS quantidade_dias FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 6 MONTH) AND DataConclusaoJustificativa IS NOT NULL AND DataPublicacao IS NOT NULL AND TipoTrilha <> 'pré' AND StatusRegistro <> 'excluído' $areas_perfil";

                for ($index = 0; $index < count($sql2); $index++){
                    $query = $objcon->select($sql2[$index]);
                    if ($objcon->erro != ""){
                        throw new Exception($objcon->erro);
                    }
                    if(mysqli_num_rows($query) == 0){
                        $calc2[] = 0;
                    }else{
                        while ($row = mysqli_fetch_array($query)){
                            $media = $media + $row['quantidade_dias'];
                        }
                        $media = $media / mysqli_num_rows($query);
                        $media = round($media);
                        $calc2[] = $media;
                        if($media > $array['linemax']){
                            $array['linemax'] = $media;
                        }
                    }
                }

                $array['linemax'] = $array['linemax'] + 1;

                $lineChartData = array(
                    "labels" => $labels,
                    "datasets" => array(
                        array(
                            "label" => "Prazo Legal (pós)",
                            "backgroundColor" => 'rgb(255, 99, 132)', //red
                            "borderColor" => 'rgb(255, 99, 132)',
                            "fill" => false,
                            "data" => array(
                                $prazo_legal_pos, 
                                $prazo_legal_pos, 
                                $prazo_legal_pos,
                                $prazo_legal_pos, 
                                $prazo_legal_pos, 
                                $prazo_legal_pos, 
                                $prazo_legal_pos
                            )
                        ),
                        array(
                            "label" => "Tempo (pós)",
                            "backgroundColor" => 'rgb(54, 162, 235)', //blue
                            "borderColor" => 'rgb(54, 162, 235)',
                            "fill" => false,
                            "data" => array(
                                $calc[6], 
                                $calc[5], 
                                $calc[4], 
                                $calc[3], 
                                $calc[2], 
                                $calc[1], 
                                $calc[0]
                            )
                        ),
                        array(
                            "label" => "Prazo Legal (pré)",
                            "backgroundColor" => 'rgb(255, 159, 64)', //orange
                            "borderColor" => 'rgb(255, 159, 64)',
                            "fill" => false,
                            "data" => array(
                                $prazo_legal_pre, 
                                $prazo_legal_pre, 
                                $prazo_legal_pre,
                                $prazo_legal_pre, 
                                $prazo_legal_pre, 
                                $prazo_legal_pre, 
                                $prazo_legal_pre
                            )
                        ),
                        array(
                            "label" => "Tempo (pré)",
                            "backgroundColor" => 'rgb(75, 192, 192)', //green
                            "borderColor" => 'rgb(75, 192, 192)', 
                            "fill" => false,
                            "data" => array(
                                $calc2[6], 
                                $calc2[5], 
                                $calc2[4], 
                                $calc2[3], 
                                $calc2[2], 
                                $calc2[1], 
                                $calc2[0]
                            )
                        ),
                    )
                );

                $sql3[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE()) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql3[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 1 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql3[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 2 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql3[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 3 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql3[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 4 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql3[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 5 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql3[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoJustificativa) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 6 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";

                $array['barmax'] = 0;

                for ($index = 0; $index < count($sql3); $index++){
                    $query = $objcon->select($sql3[$index]);
                    if ($objcon->erro != ""){
                        throw new Exception($objcon->erro);
                    }
                    if(mysqli_num_rows($query) == 0){
                        $calc3[] = 0;
                    }else{
                        $row = mysqli_fetch_array($query);
                        $calc3[] = $row['qtd'];
                        if($row['qtd'] > $array['barmax']){
                            $array['barmax'] = $row['qtd'];
                        }
                    }
                }


                $sql4[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataPublicacao) = EXTRACT(YEAR_MONTH FROM CURDATE() ) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql4[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataPublicacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 1 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql4[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataPublicacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 2 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql4[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataPublicacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 3 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql4[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataPublicacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 4 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql4[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataPublicacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 5 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql4[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataPublicacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 6 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";

                for ($index = 0; $index < count($sql4); $index++){
                    $query = $objcon->select($sql4[$index]);
                    if ($objcon->erro != ""){
                        throw new Exception($objcon->erro);
                    }
                    if(mysqli_num_rows($query) == 0){
                        $calc4[] = 0;
                    }else{
                        $row = mysqli_fetch_array($query);
                        $calc4[] = $row['qtd'];
                        if($row['qtd'] > $array['barmax']){
                            $array['barmax'] = $row['qtd'];
                        }
                    }
                }
                
                $sql5[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataLiberacao) = EXTRACT(YEAR_MONTH FROM CURDATE() ) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql5[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataLiberacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 1 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql5[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataLiberacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 2 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql5[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataLiberacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 3 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql5[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataLiberacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 4 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql5[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataLiberacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 5 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";
                $sql5[] = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataLiberacao) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 6 MONTH) AND StatusRegistro <> 'excluído' $areas_perfil";

                for ($index = 0; $index < count($sql5); $index++){
                    $query = $objcon->select($sql5[$index]);
                    if ($objcon->erro != ""){
                        throw new Exception($objcon->erro);
                    }
                    if(mysqli_num_rows($query) == 0){
                        $calc5[] = 0;
                    }else{
                        $row = mysqli_fetch_array($query);
                        $calc5[] = $row['qtd'];
                        if($row['qtd'] > $array['barmax']){
                            $array['barmax'] = $row['qtd'];
                        }
                    }
                }

                $array['barmax'] = $array['barmax'] + 1;

                $barChartData = array(
                    "labels" => $labels,
                    "datasets" => array(
                        array(
                            "label" => "Registros Liberados",
                            "backgroundColor" => 'rgb(75, 192, 192)',
                            "borderColor" => 'rgb(75, 192, 192)',
                            "borderWidth" => 1,
                            "data" => array(
                                $calc5[6], 
                                $calc5[5], 
                                $calc5[4], 
                                $calc5[3], 
                                $calc5[2], 
                                $calc5[1], 
                                $calc5[0]
                            )
                        ),
                        array(
                            "label" => "Registros Publicados",
                            "backgroundColor" => 'rgb(54, 162, 235)', 
                            "borderColor" => 'rgb(54, 162, 235)', 
                            "borderWidth" => 1,
                            "data" => array(
                                $calc4[6], 
                                $calc4[5], 
                                $calc4[4], 
                                $calc4[3], 
                                $calc4[2], 
                                $calc4[1], 
                                $calc4[0]
                            )
                        ),
                        array(
                            "label" => "Registros Justificados",
                            "backgroundColor" => 'rgb(255, 99, 132)',
                            "borderColor" => 'rgb(255, 99, 132)',
                            "borderWidth" => 1,
                            "data" => array(
                                $calc3[6], 
                                $calc3[5], 
                                $calc3[4], 
                                $calc3[3], 
                                $calc3[2], 
                                $calc3[1], 
                                $calc3[0]
                            )
                        )
                    )
                );

                //monitoramento
                $sql = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoAnalise) >= EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 6 MONTH) AND QuarentenaAte is not null $areas_perfil";
                $query = $objcon->select($sql);
                if ($objcon->erro != ""){
                    throw new Exception($objcon->erro);
                }
                $row = mysqli_fetch_array($query);
                $qtd_monitoramento = $row['qtd'];

                //auditoria
                $sql = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoAnalise) >= EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 6 MONTH) AND (NumProcAuditoria <> '' Or SolicitadoAuditoria = true) $areas_perfil";
                $query = $objcon->select($sql);
                if ($objcon->erro != ""){
                    throw new Exception($objcon->erro);
                }
                $row = mysqli_fetch_array($query);
                $qtd_auditoria = $row['qtd'];

                //devolução
                $sql = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoAnalise) >= EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 6 MONTH) AND StatusRegistro = 'devolvido' $areas_perfil";
                $query = $objcon->select($sql);
                if ($objcon->erro != ""){
                    throw new Exception($objcon->erro);
                }
                $row = mysqli_fetch_array($query);
                $qtd_devolucao = $row['qtd'];

                //arquivo
                $sql = "SELECT COUNT(*) AS qtd FROM `view_registros` WHERE EXTRACT(YEAR_MONTH FROM DataConclusaoAnalise) >= EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 6 MONTH) AND QuarentenaAte is null And SolicitadoAuditoria = false and NumProcAuditoria is null and StatusRegistro = 'concluído' $areas_perfil";
                $query = $objcon->select($sql);
                if ($objcon->erro != ""){
                    throw new Exception($objcon->erro);
                }
                $row = mysqli_fetch_array($query);
                $qtd_arquivo = $row['qtd'];

                $pieChartData = array(
                    "backgroundColor" => array(
                            'rgb(75, 192, 192)',
                            'rgb(255, 159, 64)',
                            'rgb(255, 99, 132)',
                            'rgb(54, 162, 235)'
                        ),
                    "data" => array(),
                );

                array_push($pieChartData["data"], $qtd_arquivo);
                array_push($pieChartData["data"], $qtd_monitoramento);
                array_push($pieChartData["data"], $qtd_devolucao);
                array_push($pieChartData["data"], $qtd_auditoria);

                $array["lineChartData"] = $lineChartData;
                $array["barChartData"] = $barChartData;
                $array["pieChartData"] = $pieChartData;
            }else{
                $array["linemax"] = "ocultar";
            }
                       
            retorno();
            break;
            
        case "update":
            
            /*
             * 
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(231,233,237)'
             * 
             */
            
            atualiza_graficos(0, 7);
            
            $lineChartData = array(
                "labels" => $labels,
                "datasets" => array(
                    array(
                        "label" => "Prazo Legal (pós)",
                        "backgroundColor" => 'rgb(255, 99, 132)', //red
                        "borderColor" => 'rgb(255, 99, 132)',
                        "fill" => false,
                        "data" => array(
                            90, 
                            90, 
                            90,
                            90, 
                            90, 
                            90, 
                            90
                        )
                    ),
                    array(
                        "label" => "Tempo (pós)",
                        "backgroundColor" => 'rgb(54, 162, 235)', //blue
                        "borderColor" => 'rgb(54, 162, 235)',
                        "fill" => false,
                        "data" => array(
                            50, 
                            88, 
                            72, 
                            84, 
                            70, 
                            45, 
                            77
                        )
                    ),
                    array(
                        "label" => "Prazo Legal (pré)",
                        "backgroundColor" => 'rgb(255, 159, 64)', //orange
                        "borderColor" => 'rgb(255, 159, 64)',
                        "fill" => false,
                        "data" => array(
                            30, 
                            30, 
                            30,
                            30, 
                            30, 
                            30, 
                            30
                        )
                    ),
                    array(
                        "label" => "Tempo (pré)",
                        "backgroundColor" => 'rgb(75, 192, 192)', //green
                        "borderColor" => 'rgb(75, 192, 192)', 
                        "fill" => false,
                        "data" => array(
                            18, 
                            25, 
                            60, 
                            37, 
                            32, 
                            18, 
                            28
                        )
                    ),
                )
            );
            
            $barChartData = array(
                "labels" => $labels,
                "datasets" => array(
                    array(
                        "label" => "Registros Justificados",
                        "backgroundColor" => 'rgb(255, 99, 132)',
                        "borderColor" => 'rgb(255, 99, 132)',
                        "borderWidth" => 1,
                        "data" => array(
                            40, 
                            45, 
                            55, 
                            10, 
                            43, 
                            52, 
                            98
                        )
                    ),
                    array(
                        "label" => "Registros Publicados",
                        "backgroundColor" => 'rgb(54, 162, 235)', 
                        "borderColor" => 'rgb(54, 162, 235)', 
                        "borderWidth" => 1,
                        "data" => array(
                            20, 
                            55, 
                            72, 
                            65, 
                            75, 
                            66, 
                            40
                        )
                    )
                )
            );
            
            
            $pieChartData = array(
                "backgroundColor" => array(
                        'rgb(75, 192, 192)',
                        'rgb(255, 159, 64)',
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)'
                    ),
                "data" => array(),
            );
            
            array_push($pieChartData["data"],30, 50, 98, 60);
            
            $array["lineChartData"] = $lineChartData;
            $array["barChartData"] = $barChartData;
            $array["pieChartData"] = $pieChartData;
            
            retorno();
            break;
        default:                    //------------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
    retorno();
}

function atualiza_graficos($orgao, $meses = 6){
    try{
        global $array;
        
        if($orgao == -1){
            
        }else{
            
        }
        
        $data = new DateTime();
        $mes = $data->format('m');
        $ano = $data->format('Y');
        
        for ($index = 0; $index < $meses; $index++){
            $mes--;
            if($mes == 0){
                $mes = 12;
                $ano --;
            }
            preenche_labels($mes, $ano);
        }
        
        
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage();
        retorno();
    }
}

function preenche_labels($mes, $ano){
    try{
        global $array, $labels;
        
        switch ($mes){
            case 1:
                $periodo = "Jan/".$ano; 
                break;
            case 2:
                $periodo = "Fev/".$ano; 
                break;
            case 3:
                $periodo = "Mar/".$ano; 
                break;
            case 4:
                $periodo = "Abr/".$ano; 
                break;
            case 5:
                $periodo = "Mai/".$ano; 
                break;
            case 6:
                $periodo = "Jun/".$ano; 
                break;
            case 7:
                $periodo = "Jul/".$ano; 
                break;
            case 8:
                $periodo = "Ago/".$ano; 
                break;
            case 9:
                $periodo = "Set/".$ano; 
                break;
            case 10:
                $periodo = "Out/".$ano; 
                break;
            case 11:
                $periodo = "Nov/".$ano; 
                break;
            case 12:
                $periodo = "Dez/".$ano; 
                break;
            default:
                $periodo = "Indefinido";
                break;
        }
        
        array_unshift($labels, $periodo);
        
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage();
        retorno();
    }
}

function retorno(){
    global $array;
       
    usleep(900000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}