<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    //require_once 'sub_menu.php';
    
    require_once("../../obj/usuario.php");
    
    $perfil = $_SESSION['sessao_perfil'];
    $CPF = $_SESSION['sessao_id'];
    
    if(isset($_SESSION['sessao_ultima_dica'])){
        $CodDica = $_SESSION['sessao_ultima_dica'];
    }else{
        $_SESSION['sessao_ultima_dica'] = 0;
        $_SESSION['sessao_exibido_dica'] = false;
        $CodDica = 0;
    }
    
    $obj_usuario = new usuario();
    $arquivo = "";
    $exibir_dica = "0";
    
    if($_SESSION['sessao_exibido_dica'] == false){
        $row = $obj_usuario->exibir_dicas($perfil, $CodDica, $CPF);
        if($obj_usuario->erro != ""){
            echo $obj_usuario->erro;
        } else {
            if($row != false){
                $arquivo = $row['Arquivo'];
                $exibir_dica = "1";
                $_SESSION['sessao_exibido_dica'] = true;
            }
        }
    }
?>

<link href="../../css/zTreeStyle.css" rel="stylesheet" type="text/css"/>
<script src="../../js/jquery.ztree.core.js" type="text/javascript"></script>
<script src="../../js/datepicker.js" type="text/javascript"></script>
<script src="../../js/Chart.bundle.js" type="text/javascript"></script>
<script src="../../js/utils.js" type="text/javascript"></script>
<script src="inicio_js.js" type="text/javascript"></script>

<style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }

</style>
<br>
<div class="container">
    
    <div class="well well-sm">
        <h5><b>
            <p>
                Pedimos a todos os Supervisores que acessem no "Menu TCE Virtual - SGI Usuários - Atualizar meus Dados" e verifique se os dados
                foram corretamente cadastrados e complete/altere alguma informação que porventura esteja incorreta ou faltando.
            </p>
            <p>
               Após acessem no "Menu TCE Virtual - SGI Usuários - Cadastrar Usuários" para efetuar a inclusão de seus respectivos Analistas.
            </p> 
            </b></h5>
    </div>
    <br>
    
    <div id="div_suporte" class="well well-sm" style="margin-bottom: 5px; display: none;"></div>
    <div id="div_monitoramento" class="well well-sm" style="margin-bottom: 5px; display: none;"></div>
    <div id="div_pendentes" class="well well-sm" style="margin-bottom: 5px; display: none;"></div>
    <div id="div_nao_justificados" class="well well-sm" style="margin-bottom: 5px; display: none;"></div>
    <div id="div_nao_distribuidos" class="well well-sm" style="margin-bottom: 5px; display: none;"></div>
    <div id="div_nao_analisados" class="well well-sm" style="margin-bottom: 5px; display: none;"></div>
    <div id="div_pedido_auditoria" class="well well-sm" style="margin-bottom: 5px; display: none;"></div>
</div>

<br>
<div class="container" id="div_titulo_graficos" style="display:none">
    <div class="well well-sm">
        <h5 style="text-align: center;"><b>GRÁFICOS GERENCIAIS DE SUA ÁREA DE ATUAÇÃO.</b></h5>
    </div>
</div>
<div class="container-fluid">
    <div class="row" id="div_graficos" style="display:none;">
        <div id="grafico1" class="col-sm-4" style="display:none;">
            <canvas id="canvas1" height="400"></canvas>
        </div>
        <div id="grafico2" class="col-sm-4" style="margin-left:20%;">
            <canvas id="canvas2" height="400"></canvas>
        </div>
        <div id="grafico3" class="col-sm-4">
            <canvas id="canvas3" height="400" style="max-height: 400px; "></canvas>
        </div>
        <br>
    </div>
    <br>
    <!-- desativando manual do sistema -->
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size: 140%;"><b>Manual de Utilização</b></div>
                <div class="panel-body">
                    <div>
                       <!-- <ul id="treeDemo" class="ztree"></ul>-->
                       <a href="https://confluence.tce.sc.gov.br/pages/viewpage.action?pageId=8782737" target="_blank"> Acessar manual</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <!--<iframe id="iframe1" src="" style=" width: 100%; border:none; margin-top: -7px; min-height: 500px;" onload="resizeIframe(this)"></iframe>-->
        </div>
    </div>
    <form name="form1" id="form1" action="#" method="POST">
        <input type="hidden" name="operacao" id="operacao" value="load" />
    </form>
    <br><br>
</div>

<!-- SGTA-46
<?php

if($perfil == 6){
    
    echo '<div id="popup_form" title="Dica do Dia" style="margin-bottom: 30px;">
                <div id="div_dica" style="text-align: justify;">';
    
    if($arquivo != "") {
        include_once "dicas/$arquivo/dica.php"; 
    }

    echo "<input type='hidden' name='exibir_dica' id='exibir_dica' value=" .$exibir_dica ." />
         </div> 
        <hr>
            <div style='text-align: right;'>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type='button' class='btn btn-info' onclick='$('#popup_form').dialog('close');'>Sair &nbsp;<span class='fa fa-sign-out'></span></button>
            </div>
        </div>";
}

    
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 
  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  //Include com o Template

  include("../../master/master.php");