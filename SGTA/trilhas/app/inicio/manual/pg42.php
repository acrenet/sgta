<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg42.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Analistas - Visão Geral</h2>
    <p>
        Os Analistas são os responsáveis por justificar as inconsistências geradas pela trilha. Sua função é analisar as informações
        apresentadas e verificar se elas apresentam inconsistências cadastrais e/ou de pagamentos que se revelam incompatíveis com
        o ordenamento legal.
    </p>
    <p>
        Caso isso seja constatado, deve-se informar quais as atitudes corretivas foram ou estão sendo tomadas para sanar o problema. Documentos
        comprobatórios devem se anexados.
    </p>
    <p>
        O Analista deve estar atento aos prazos estabelecidos em norma a fim de se evitar cobranças ou penalidades ao seu órgão/unidade administrativa.
    </p>
    
</div>