<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg37.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo"/></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Gestores - Abertura de Inspeção ou Auditoria.</h2>
    
    <p>
        Terminada a análise da justificativa o Auditor poderá solicitar que seja aberto processo de Auditoria ou Inspeção. O Superintendente
        responsável pela área que se encontra a trilha com perfil de Gestor no Sistema é o responsável por decidir sobre a requisição do 
        Auditor.
    </p>
    <p>
        Ao efetuar login no Sistema o Gestor será direcionado para a tela inicial. Caso haja algum registro de sua área requerendo abertura de processo de inspeção ou auditoria, 
        nesta tela haverá um alerta (fig.1) informando a quantidade, bastando clicar sobre o link para ser direcionado à tela de análise. 
        Para retornar à tela inicial, basta clicar no botão Home do menu superior.
    </p>
    <sub>Fig.1</sub><br>
    <img src="img/img37_1.png" alt=""/>
    <p>
        A tela de análise (Fig.2) listará todos os registros requerendo abertura de processo de inspeção ou auditoria, bem como a justificativa
        apresentada pelo Auditor. Para decidir sobre um registro basta clicar em seu botão de análise correspondente: <img src="img/img37_3.png" alt=""/>
    </p>
    <p>
        <b>Nota: O Sistema não permitirá a gravação de informações se o Gestor não tiver a função de Superintendente.</b>
    </p>
    <sub>Fig.2</sub><br>
    <img src="img/img37_2.png" alt=""/>
    <p>
        Será aberto uma tela popup (Fig.3) onde será exibido onde o Gestor selecionará o resultado de sua análise, informará o número do processo
        caso tenha optado por abertura de processo de auditoria ou inspeção e a sua resposta.
    </p>
    <p>
        O tribunal de contas  possui outro sistema próprio de Auditoria/Inspeção, apenas o número é informado neste sistema para fins de controle.
    </p>
    <p>
        Qualquer que seja o resultado o registro é finalizado no Sistema de Gestão de Trilhas de Auditoria.
    </p>
    <sub>Fig.3</sub><br>
    <img src="img/img37_4.png" alt=""/>
    
</div>