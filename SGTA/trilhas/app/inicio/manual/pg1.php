<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg1.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Visão Geral</h2>
    
    <p>
        O <b>Tribunal de Contas de Santa Catarina</b> é o órgão responsável por assistir diretamente o Governador do Estado quanto a assuntos
        relativos à defesa do patrimônio público e ao incremento da transparência da gestão, por meio das 
        atividades de controle interno, auditoria pública, correição, ouvidoria, prevenção e combate à corrupção.
    </p>
    
    <p>
        Uma das ferramentas utilizadas na prevenção e combate à corrupção é o desenvolvimento trilhas de auditoria. Este consiste estudos e cruzamentos de 
        informações constantes em diferentes bases de dados (RHNet, SiofiNet, ComprasNet, Juceg, Receita Federal, etc) em busca de situações de inconsistências 
        cadastrais e/ou de pagamentos que se revelam incompatíveis com o ordenamento legal.
    </p>
    
    <p>
        É chamado de inconsistência cada registro gerado pela trilha de auditoria. Este é um indício de irregularidade mas não representa necessariamente um ilícito.
    </p>
    
    <p>
        <b>Exemplos de Trilhas de Auditoria:</b>
    </p>
    
    <ul title="1">
        <li>Fornecedores impedidos de contratar com o Governo.</li>
        <li>Fracionamento para não licitar.</li>
        <li>Acumulação ilegal de cargos.</li>
        <li>Servidores falecidos recebendo remuneração.</li>
    </ul>
    
    <p>
        O <b>Sistema de Trilhas de Auditoria</b> é a ferramenta que viabiliza que as inconsistências encontradas sejam levadas até os órgão/unidades 
        administrativas do Governo para que possam ser analisadas, justificadas e se for o caso corrigidas.
    </p>
    
    <p>
        <b>Suas características são:</b>
    </p>
    
    <ul type="1">
        <li>Aceita trilhas contendo qualquer tipo de informação.</li>
        <li>Realiza automaticamente agrupamento dos registros bem como resumos, somatórios, médias, etc., Facilitando assim sua análise.</li>
        <li>Concentra em um único local todas as trilhas de auditorias que antes eram armazenadas de forma difusa.</li>
        <li>Possibilita maior transparência pois todas as análises, justificativas, pareceres, etc., podem ser acompanhados em tempo real e 
            ficam armazenado para consultas futuras.</li>
        <li>Proporciona mais agilidade e economia de mão de obra pois possibilita ao Órgão/Unidade Administrativa justificar e se for o caso
            corrigir a inconsistência antes de abertura de processo de auditoria. Sendo este mais burocrático, requer mais recursos humanos e
            demanda mais tempo.</li>
        <li>Acompanha os prazos e emite alertas até que a inconsistência seja justificada.</li>
        <li>Registra o histórico de todas as ações executadas no sistema.</li>
        <li>Possibilita a inclusão de documentos comprobatórios e os armazena de forma segura para consultas futuras.</li>
        <li>Emite relatórios tanto detalhados quanto estatísticos em tempo real.</li>
        <li>Possui uma área destinada a estudos para fomentar a geração de novas trilhas ou melhorar as que já existem.</li>
        <li>Possibilita o cruzamento entre suas próprias trilhas, podendo gerar à partir daí novas trilhas ou estudos.</li>
    </ul>
    
    <p>
        <b>O processo de trilhas de auditoria se divide em 6 etapas: </b>       
    </p>
    
    <ol type="1">
        <li>Cruzamento das bases de dados para geração dos registros de inconsistências.</li>
        <li>Homologação do resultado do cruzamento se determinada sua consistência.</li>
        <li>Distribuição do resultado entre os órgãos/unidades do Governo.</li>
        <li>Justificativa das inconsistências pelos órgãos/unidades do Governo.</li>
        <li>Análise das justificativas apresentadas pelos órgãos/unidades do Governo.</li>
        <li>Emissão do parecer final.</li>
    </ol>
    <p>
        <b>A cada usuário cadastrado é atribuído um perfil, existem 6 perfis disponíveis no sistema cuja a principal atividade de cada um é: </b>       
    </p>
    <ol type="1"> 
        <li><b>Administrador (TCE):</b> Responsável por mapear as trilhas, efetuar o cruzamento dos dados e dar a carga no sistema.</li>
        <li><b>Gestor (TCE):</b> Responsável por realizar a Homologação do resultado da trilha e supervisionar o trabalho dos Auditores e cobrar dos
            Supervisores em sua área de atuação.</li>
        <li><b>Supervisor (Órgão):</b> Responsável por supervisionar o trabalho dos Analistas  de seu órgão/unidade, intervir caso seja necessário 
            para garantir o cumprimento dos prazos definidos em norma para a análise das inconsistências.</li>
        <li><b>Analista (Órgão):</b> Responsável por justificar a inconsistência apresentada na trilha, podendo concordar ou não com o apontamento. 
            Também é o responsável, quando for o caso, por levantar o montante do prejuízo causado ao erário.</li>
        <li><b>Auditor (TCE):</b> Responsável pela análise da justificativa apresentada pelo Analista do órgão/unidade, concluindo ou devolvendo 
            o registro para novas justificativas ou colocando em monitoramento.</li>
        <li><b>Consulto TCE:</b> Acompanhar o resultado dos trabalhos e gerar relatórios em qualquer órgão/unidade.</li>
        <li><b>Consulta Órgão:</b> Acompanhar o resultado dos trabalhos e gerar relatórios em seu órgão/unidade.</li>
    </ol>
</div>
