<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg33.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Homologação</h2>
    <p>
        Os Gestores são os responsáveis por realizar a homologação do resultado da trilha antes de sua liberação aos órgão/Unidades Administrativas.
    </p>
    <p>
        Para acessar a Homologação basta ir no menu superior "Gestão/Homologação", o sistema irá listar todas as trilhas que estão nesta etapa
        bastando escolher a desejada.
    </p>
    <p>
        Feita a análise caso a trilha não apresente resultado consistente o Gestor deverá devolve-la aos Administradores para que sejam feitas correções em seu método
        de geração.
    </p>
    <p> 
        Se desejar, é possível anexar arquivos à trilha que serão restritos apenas aos servidores do TCE ou disponíveis a todos os usuários
        bastando para isso clicar no botão "Anexar um Arquivo à Trilha", uma janela popup irá aparecer onde será feita uma descrição do arquivo
        e a seleção do mesmo. Nota: O limite por arquivo é de 10 mb. Podem ser anexados quantos arquivos forem necessários.
    </p>
    <sub style="font-size: 70%;">Fig.1</sub><br>
    <img src="img/img33_1.png" width="764" height="315"/>
    
    <p>
        Os registros de inconsistências são exibidos um a um em forma de tabela. A informação constante na tabela depende de cada trilha, 
        entretanto as últimas 5 colunas são idênticas para todas as trilhas e possuem funções conforme descritas a seguir:<br>
        <sub style="font-size: 70%;">Fig.2</sub><br>
        <img src="img/img33_4.png"/>
    </p>

    <ul>
        <li><b>Obs TCE: </b>Contém alguma informação pertinente cadastrada pelo Gestor ou Auditor, informação que balizará o trabalhos dos Analistas.</li>
        <li><b>Gestor Resp.: </b>Nome do Auditor que é responsável pela análise prévia do registro, caso o Gestor tenha feito a distribuição.</li>
        <li><b>Status: </b> Indica o status que se encontra o registro, que poderá ser "pré-análise" ou "excluído".</li>
        <li><b>Docs: </b> Indica:
            <ol>
                <li>A quantidade de documentos anexados ao registro, quando maior que zero o ícone fica ativo, bastando clicar
                    sobre ele para visualizar os documentos anexados.</li>
                <li>A quantidade de mensagens constantes no registro (exclusivo para os servidores do TCE), bastando clicar sobre o ícone
                    para visualizar/cadastrar mensagens.</li>
            </ol>
        </li>
        <li><b>Ações: </b>São o conjunto de botões de comando que quando ativos permitem que sejam realizadas ações sobre o registro, mais informações estão disponíveis abaixo.</li>
    </ul>

    
    <h2>Ações:</h2>
    
    <p>Ações são o conjunto de botões de comando que exibem detalhes sobre o registro correspondente ou modificam suas propriedades.</p>
    
    <table border="1">
        <thead>
            <tr>
                <th>Botão</th>
                <th>Ação</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_8.png" width="16" height="16" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Exibir mais Detalhes: </b>Por falta de espaço não é possível exibir todas as informações referentes a um registro
                        de inconsistência na tela.
                    </p>
                    <p>
                        Ao clicá-lo será exibido em uma tela popup mais detalhes referentes ao registro, sendo que seu conteúdo varia de trilha para trilha.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img44_2.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Incluir uma Observação no Registro: </b> Utilizado para gravar alguma informação para auxiliar o Analista
                        da Unidade Administrativa. Está disponível em qualquer status exceto o concluído.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img33_2.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Remover da Trilha o Registro: </b> O registro não será excluído, apenas não será mais visível após a distribuição
                        da trilha entre os órgão. Utilizado quando notadamente o registro não necessita de ser analisado. É importante que se cadastre
                        na observação a justificativa para o caso de questionamentos futuros.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_9.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Anexar um Arquivo: </b> Abre um janela popup onde se pode anexar arquivos pdf, documentos de texto e planilhas eletrônicas
                        com até 10 megabytes no registro.
                    </p>
                    <p>
                        Observações:<br>
                        <ul>
                            <li>Um arquivo só pode ser excluído por que o anexou.</li>
                            <li>A qualquer momento pode-se incluir novos arquivos mesmo após a análise ser concluída.</li>
                        </ul>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img33_3.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Reincluir o Registro no Resultado da Trilha: </b> Caso deseje, um registro removido pode ser re-incluído novamente no resultado
                        da trilha, bastando acionar este botão no registro correspondente.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img33_6.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Mesclar Registro com Outro: </b> Em alguns casos especiais pode-se agrupar dois ou mais registros para serem analisados
                        em conjunto pelo órgão. Uma vez agrupados não poderão mais ser separados.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img33_7.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Distribuir Registro a um Auditor: </b> Se desejar, o Gestor pode distribuir o registro a um Auditor para que o mesmo faça a
                        pré-análise do mesmo. O registro ficará reservado ao Auditor e somente ele poderá fazer a análise.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img33_8.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Concluir a Análise: </b> Se o registro tiver sido distribuido a um Autidor, deve ele registrar a conclusão ao término da análise.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img33_9.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Solicitar Abertura de Processo de Inspeção/Auditoria Direto: </b> Caso o registro de inconsistência apresente indícios
                        suficientes e não seja passível de justificativa pelo órgão, pode-se solicitar a abertura de processo de inspeção/auditoria já nesta
                        fase, o registro não será enviado ao órgão para justificativa.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_12.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Consultar Justificativas/Pareceres/Eventos: </b> Esta opção permite visualizar justificativas/pareceres emitidos pelos Analistas e Auditores.
                        Também é possível ver o históricos dos eventos que ocorreram com o registro, as datas e os usuários que nele trabalharam.
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    
    <p>
        É de responsabilidade do Gestor o preenchimento das orientações sobre a trilha que balizaram o trabalho dos Analistas e dos Auditores.
        Neste campo (Fig.3) deverá ser informado como deverá ser feita a justificativa e a análise, bem como quais documentos deverão ser anexados.
    </p>
    <sub style="font-size: 70%;">Fig.3</sub><br>
    <img src="img/img33_5.png" alt=""/>
    <p>
        Terminado o trabalho de homologação basta clicar no botão "Enviar Trilha aos Órgãos" (Fig.1), após a confirmação a trilha
        será distribuída aos órgãos/unidades administrativas. O sistema enviará e-mails informativos aos respectivos supervisores.
    </p>
    
</div>