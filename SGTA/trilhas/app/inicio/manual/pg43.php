<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg43.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Analistas - Justificar Inconsistências</h2>
    <p>
        Inconsistências são registros que possivelmente revelam problemas cadastrais e/ou de pagamentos incompatíveis com o ordenamento legal. 
        São obtidos através do cruzamento de diferentes bases de dados conforme critérios específicos para cada tipo de trilha.
    </p>
    <p>
        Trata-se de uma suspeita que necessita de uma análise mais detalhada e não necessariamente representa um ilícito.
    </p>
    
    
    <p>
        Será aberto a tela de seleção de trilha, onde o Analista poderá escolher a trilha que deseja atuar clicando no ícone: <img src="img/img43_1.png" width="15" height="11"/>
    </p>
    <p>
        Se este ícone não estiver ativo significa que o usuário não tem acesso à aquela trilha. Para ter o acesso é necessário procurar o Supervisor
        responsável em seu órgão.
    </p>
    <p>
        No canto superior direito da tabela de seleção de trilhas há um campo de procura que pode ser utilizado para filtrar os registros exibidos,
        podendo ser preenchido com a Área, Trilha, Status ou a combinação destes, também pode ser lançado o termo "permitido" (sem aspas) para 
        que o sistema exiba apenas as trilhas que você possui acesso.<br>
        <br><sub>Fig.1</sub><br>
        <img src="img/img43_2.png" alt=""/>
    </p>
    <p>
        Selecionado a trilha você será direcionado à tela onde serão lançadas as justificativas.
    </p>
    <p>
        Os registros de inconsistências são exibidos um a um em forma de tabela. A informação constante na tabela depende de cada trilha, 
        entretanto as últimas 6 colunas são idênticas para todas as trilhas e possuem funções conforme descritas a seguir:<br>
        <br><sub>Fig.2</sub><br>
        <img src="img/img43_3.png" width="718" height="94"/>
    </p>

    <ul>
        <li><b>Observação TCE: </b>Contém alguma informação pertinente cadastrada pelo Gestor ou Auditor.</li>
        <li><b>Reservado Por: </b>Indica o usuário que está analisando o registro para que não haja duplicidade no trabalho.</li>
        <li><b>Status: </b> Indica o status que se encontra o registro, mais informações estão disponíveis abaixo.</li>
        <li><b>Flags: </b> São marcações que indicam ações que foram realizadas sobre o registro, mais informações estão disponíveis abaixo.</li>
        <li><b>Docs: </b> Indica a quantidade de documentos anexados ao registro, quando maior que zero o ícone fica ativo, bastando clicar
        sobre ele para visualizar os documentos anexados.</li>
        <li><b>Ações: </b>São o conjunto de botões de comando que quando ativos permitem que sejam realizadas ações sobre o registro, mais informações estão disponíveis abaixo.</li>
    </ul>

    <br>
    <h2>Status:</h2>
    <p>
        Os registros de inconsistências podem assumir 6 status diferentes, a tabela abaixo indica a seqüência natural do registro:
    </p>
    
    <table border="1">
        <thead>
            <tr>
                <th><b>Status</b></th>
                <th><b>Descrição</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="color: darkorange;"><b>pendente</b></td>
                <td>Indica que o registro ainda não foi analisado.</td>
            </tr>
            <tr>
                <td style="color: darkblue;"><b>em justificativa</b></td>
                <td>Indica que algum Analista está atuando com o registro, a reserva é feita automaticamente e após a conclusão da análise
                a reserva é retirada automaticamente. Nota: é possível analisar um registro que já esteja reservado por outro Analista entretanto a reserva
                será transferida para o seu nome.</td>
            </tr>
            <tr>
                <td style="color: darkgreen;"><b>justificado</b></td>
                <td>Indica que o analista emitiu o seu parecer, concordando ou não com a inconsistência. Neste status o Auditor não iniciou sua análise.</td>
            </tr>
            <tr>
                <td style="color: blue;"><b>em análise</b></td>
                <td>Indica que algum Analista do TCE iniciou o trabalho de análise sobre o que justificado pelo Analista.</td>
            </tr>
            <tr>
                <td style="color: magenta;"><b>concluído</b></td>
                <td>Indica que o Auditor considerou conclusas as informações prestadas pelo Analista. 
                    Ações posteriores poderão ter sido desencadeadas, ver em Flags abaixo.</td>
            </tr>
            <tr>
                <td style="color: red;"><b>devolvido</b></td>
                <td>Indica que o Auditor não considerou conclusas as informações prestadas pelo Analista
                e o devolveu para novas diligências.</td>
            </tr>
        </tbody>
    </table>
    
    <br>
    <h2>Flags:</h2>
    <p>
        Flags são marcações chamam a atenção sobre a necessidade de se realizar alguma ação sobre o registro, ou demonstram ações que já ocorreram.
    </p>
    <p>
        Ao posicionar o mouse sobre uma flag mais informações serão exibidas à respeito da mesma.
    </p>
    
    <ul>
        <li style="list-style-image: url('img/img43_5.png')">
            Indica que o registro já teve a análise concluída pelo Auditor e este colocou o registro em monitoramento para uma reavaliação em um data futura. Muito comum
            quando na justificativa da inconsistência foi informado alguma atitude corretiva que demandará prazo para ser implementada.
        </li>
        <li style="list-style-image: url('img/img44_10.png')">
            Indica que o prazo do monitoramento se esgotou e o registro está aguardando uma ação do Auditor. Um alerta é emitido na tela inicial
            para o Auditor responsável.
        </li>
        <li style="list-style-image: url('img/img43_14.png')">
            Indica que o Auditor solicitou abertura de processo de Inspeção ou Auditoria. A abertura de dará em sistema de controle próprio, apenas
            o número do processo é armazenado neste sistema.
        </li>
        <li style="list-style-image: url('img/img43_4.png')">
            Indica que o Analista solicitou a alteração das informações por ele prestadas. Uma vez concluída a justificativa
            ela não pode ser mais alterada. Caso o Analista deseje modificar estas informações, deverá
            preencher o formulário próprio solicitando a alteração. O pedido chegará ao Auditor que não poderá
            concluir sua análise até que decida sobre o pedido. Nota: Não é possível solicitar alterações em registros com status ''concluído''.
        </li>
        <li style="list-style-image: url('img/img43_7.png')">
            Indica que o pedido de alteração foi aceito, o registro retorna para o status ''em análise'' reservado para o Analista que pediu a alteração.
            O solicitante será comunicado via e-mail.
        </li>
        <li style="list-style-image: url('img/img43_6.png')">
            Indica que o pedido de alteração não foi aceito. O solicitante será comunicado via e-mail.
        </li>
        <li style="list-style-image: url('img/img44_9.png')">
            Indica o descumprimento do prazo de justificativa determinado em decreto, o registro pode ser invocado pelo TCE para ações corretivas.
        </li>
    </ul>

    <br>
    <h2>Ações:</h2>
    
    <p>Ações são o conjunto de botões de comando que exibem detalhes sobre o registro correspondente ou modificam suas propriedades.</p>
    <p>Note que nem todas as ações estão disponíveis em todo o momento, variando principalmente em relação ao status que o registro se encontra.
    Assim se um botão de comando não estiver ativo significa que naquele momento a ação referente a ele não é permitida.</p>
    
    <table border="1">
        <thead>
            <tr>
                <th>Botão</th>
                <th>Ação</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_8.png" width="16" height="16" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Exibir mais Detalhes: </b>Por falta de espaço não é possível exibir todas as informações referentes a um registro
                        de inconsistência na tela.
                    </p>
                    <p>
                        Ao clicá-lo será exibido em uma tela popup mais detalhes referentes ao registro, sendo que seu conteúdo varia de trilha para trilha.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_9.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Anexar um Arquivo: </b> Abre um janela popup onde se pode anexar arquivos pdf, documentos de texto e planilhas eletrônicas
                        com até 10 megabytes.
                    </p>
                    <p>
                        Observações:<br>
                        <ul>
                            <li>Um arquivo só pode ser excluído por que o anexou.</li>
                            <li>Um arquivo só pode ser excluído até a justificativa ser concluída.</li>
                            <li>A qualquer momento pode-se incluir novos arquivos mesmo após a justificativa ser concluída.</li>
                        </ul>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_10.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Justificar Inconsistência: </b> Abre uma janela popup para o Analista emitir o seu parecer (fig.3), concordando ou não com
                        o registro de inconsistência, calcular o valor a devolver caso tenha ocorrido prejuízo ao erário, justificar a inconsistência
                        e informar as ações corretivas que foram tomadas se for o caso.
                    </p>
                    <p>Caso tenha originado algum processo administrativo é importante que se informe o seu número para o acompanhamento do TCE.</p>
                    <p>
                        Nesta tela também estão disponíveis atalhos para anexar documentos comprobatórios, visualizar os arquivos anexados e acesso a detalhes do registro
                        para que o Analista não necessite em momento algum fechar a tela de análise.
                    </p>
                    <p>
                        Caso não tenha concluído a análise é possível apenas salvar para continuar posteriormente. O registro ficará reservado em seu nome.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_11.png" alt=""/>
                </td>
                <td><p>
                        <b>Botão Solicitar Alteração de Justificativa: </b> Uma vez concluída a justificativa ela não pode ser mais alterada. 
                        Caso o Analista deseje modificar sua justificativa deverá clicar neste botão e preencher a justificativa. Esta opção
                        está disponível até o Auditor concluir sua análise.
                    </p>
                    <p>
                        O Auditor poderá ou não aceitar o pedido de alteração, caso aceite o registro retorna para o status de
                        ''em justificativa'' reservado para quem solicitou a alteração.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_12.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Consultar Justificativas/Pareceres/Eventos: </b> Esta opção permite visualizar justificativas/pareceres emitidos pelos Analistas e Auditores.
                        Também é possível ver o históricos dos eventos que ocorreram com o registro, as datas e os usuários que nele trabalharam.
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    
    <br><sub>Fig.3</sub><br>
    <img src="img/img43_16.png"/>
    
    <br>
    <h2>Exportar o Conteúdo da Tabela.</h2>
    <br><sub>Fig.4</sub><br>
    <img src="img/img43_13.png"/>
    <p>No cabeçalho da tabela encontram-se três botões: PDF, Excel, CSV, conforme exibido acima. Eles podem ser utilizados para exportar o rapidamente conteúdo
    que estiver sendo exibido na tabela.</p>
    <p>Note que só é exportado o que estiver sendo exibido, caso tenha sido utilizado a procura no canto superior direito na tabela apenas os
    registros filtrados serão exportados.</p>
    
</div>

