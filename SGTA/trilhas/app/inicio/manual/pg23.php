<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg23.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Administradores - Suporte</h2>
    <p>
        É de responsabilidade dos Administradores prestar o atendimento ao suporte quando o mesmo for referente a mau funcionamento ou 
        sugestões de melhorias do sistema. Orientações quanto a utilização do sistema são de responsabilidade dos Gestores do TCE.
    </p>
    <p>
        Após efetuar login, na tela inicial, se houver chamadas no suporte aguardando atendimento, será exibido um alerta informando a quantidade
        de chamadas em aberto, juntamente com um link para poder visualizá-las.
    </p>
    <p>
        Na tela de atendimento estarão visíveis todas as chamadas em aberto bem como o botão de comando <img src="img/img23_1.png" width="18" height="16" alt="img23_1"/>
        para se cadastrar a resposta. Um formuláro popup será exibido para o cadastro.
        O sistema automaticamente enviará um e-mail ao requerente contendo a resposta e fechará a chamada, ficando registrado o atendente e o
        horário de atendimento.
    </p>

</div>