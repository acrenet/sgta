<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg31.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Gestores (Visão Geral)</h2>
    <p>
        Os Gestores são os responsáveis por garantir o correto andamento as Trilhas de Auditoria em sua área de responsabilidade.
    </p>
    <p>
        Suas principais atribuições são:
    </p>
    
    <ul>
        <li>
            Realizar a homologação do resultado da trilha gerada pelos Administradores para determinar sua consistência, distribuindo-a
            aos Órgão/Unidades Administrativas ou devolvendo aos Administradores para ajustes.
        </li>
        <li>
            Preenchar as orientações para a análise e justificativa da trilha bem como os documentos que deverão ser anexados.
        </li>
        <li>
            Cadastrar os Supervisores bem como efetuar o seu treinamento.
        </li>
        <li>
            Atender às chamadas de suporte quando forem referente à utilização do sistema ou assuntos refefentes à trilhas
            de sua área de responsabilidade.
        </li>
        <li>
            Distribuir os registros justificados entre seus Auditores.
        </li>
        <li>
            Quando Superintendente, decidir sobre a abertura de processo de inspeção ou auditoria quando requerido pelos Auditores.
        </li>
        <li>
            Acompanhar e zelar pelo trabalho dos Auditores sob sua responsabilidade quanto à qualidade dos trabalhos e cumprimento de prazos estabelecidos em decreto.
        </li>
        <li>
            Cobrar dos Supervisores pelo atendimento dos prazos estabelecidos em decreto.
        </li>
    </ul>

    <p>
        <b>Nota: </b> O Gestor pode efetuar qualquer ação que é permitido ao Auditor como reservar um registro ou concluir uma análise.
    </p>
    
</div>