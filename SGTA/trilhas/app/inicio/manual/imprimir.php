<?php
    ob_start()
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Manual</title>
        
        <style>
            body{
                font-family: arial;
            }
            div.especial{
                border-width: 0px !important; 
            }
            .imprimir{
                display: none;
            }
        </style>
    </head>
    <body>
        
        <htmlpagefooter name="myFooter">
            <hr style="margin-bottom: 1px;">
            <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; 
             font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">
                <tr>
                    <td width="33%" style=" border-width: 0px; border-color:  #FFFFFF; border-style:  none; text-align: left;"><span style="font-weight: bold; font-style: italic;">MANUAL DO SISTEMA</span></td>
                    <td width="33%" align="center" style="font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">Página: {PAGENO} de {nbpg}.</td>
                    <td width="33%" style="text-align: right;  border-width: 0px; border-color:  #FFFFFF; border-style:  none;"><?php echo date("d-m-Y H:i:s") ?></td>
                </tr>
            </table>
        </htmlpagefooter>
    
        <sethtmlpagefooter name="myFooter" page="1" value="on" show-this-page="1"></sethtmlpagefooter>
        
        <table border="0" style="width: 100%;">
            <tr>
                <td style="width: 33%;">
                    <img src="../../../images/logo_cge_2.png" width="230" style="float: right; display: block;" />
                </td>
                <td style="width: 34%; text-align: center;">
                    <img src="../../../images/logo_uf_1.JPG" width="150" height="80" /> 
                </td>
                <td style="width: 33%; text-align: right;">
                    <img src="../../../images/bannerobservatorio.png" width="230"/>
                </td>
            </tr>
        </table>
        
        <h1 style="text-align: center;">Sistema de Trilhas de Auditoria</h1>
        
        <?php
        
        if(isset($_GET['p'])){
            $pagina = $_GET['p'];

            include $pagina;
        }
            
                
        ?>
    </body>
</html>

<?php

    $formato = 'A4'; //A4 para retrato e A4-L para paisagem
    $margin_left = 15;
    $margin_right = 10;
    $margin_top = 15;
    $margin_bottom = 15;
    $margin_header = 11;
    $margin_footer = 7;
    
    $html = ob_get_clean();
    define('MPDF_PATCH', '../../../plugin/mpdf60/');        
    include(MPDF_PATCH.'mpdf.php');
    $mpdf = new mPdf('',$formato,'','',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,'');
    $mpdf->allow_charset_conversion=true;
    $mpdf->charset_in='UTF-8';
    $mpdf->WriteHTML($html);
    //$mpdf->setFooter('Página: {PAGENO} de {nbpg}.');  
    $mpdf->Output();
    exit();
 
