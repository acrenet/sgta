<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg38.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo"/></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Solicitar Prorrogação de Prazo.</h2>
    
    <p>
        <b>Atenção: </b>Conforme determinado pelo decreto 8.936 de 2017 apenas o titular do órgão ou entidade pode solicitar a prorrogação do
        prazo para justificativa.
    </p>
    <p>
        O decreto 8.936 de 2017 determina que o órgão ou entidade possui um prazo para justificar os registros de inconsistências de 10 dias para
        despesas ainda não empenhadas ou pagas e 20 dias para os demais casos.
    </p>
    <p>
        No momento da liberação da trilha pelo TCE um e-mail informativo é automaticamente enviado para os Supervisores dos órgãos constantes na trilha.
        A partir deste momento o prazo começa a correr.
    </p>
    <p>
        Decorrido o prazo o sistema passa a exibir um alerta na tela de inicio conforme fig.1.
    </p>
    
    <sub>Fig.1</sub><br>
    <img src="img/img38_1.png" alt=""/>
    
    <p>
        Clicando no link do aviso o usuário será direcionado para tela onde serão listados os registros com prazo exaurido conforme fig. 2.
    </p>
    
    <sub>Fig.2</sub><br>
    <img src="img/img38_2.png" alt="" style="width: 100%;"/>
    
    <p>
        Para solicitar a prorrogação de prazo, deverá titular do órgão ou entidade clicar sobre o botão vermelho "Solicitar Prorrogação de Prazo."
    </p>
    
    <p>
        Uma janela popup irá aparecer conforme fig. 3 bastando então selecionar a quantidade de dias desejados, preecher a justificativa e se desejar
        inserir algum anexo.
    </p>
    <p>
        Por padrão todos os registros com prazo vencido são listados bastando apenas desmarcar os registros que não queira solicitar a prorrogação.
    </p>
    <p>
        Caso a quantidade de dias selecionado seja maior que o prazo máximo a pedir para algum registro, o sistema fará a prorragação do registro para
        este último.
    </p>
    <p>
        Imediatamente os registros terão o prazo prorrogado, caso o pedido de prorrogação não seja aceito o prazo será retidado e um e-mail
        informativo será enviado para o titular do órgão ou entidade.
    </p>
    
    <sub>Fig.3</sub><br>
    <img src="img/img38_3.png" alt=""/>
    
</div>