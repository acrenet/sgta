<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg41.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Auditores - Visão Geral</h2>
    <p>
        Os Auditores são os responsáveis por analisar as justificativas apresentadas pelos Analistas dos órgãos/Unidades Administrativas. Sua função é verificar
        a coerência entre as informações geradas pela trilha com justificativa e os documentos apresentados.
    </p>
    <p>
        Concluída sua análise o Auditor tomará uma das seguintes decisões:
    </p>
    <ul>
        <li>Aceitar a justificativa e arquivar o registro.</li>
        <li>Devolver o registro ao órgão para novos esclarecimentos.</li>
        <li>Colocar o registro em monitoramento para uma nova verificação futura.</li>
        <li>Solicitar a abertura de processo de Inspeção ou Auditoria.</li>
    </ul>
    
</div>