<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg63.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Supervisores - Cadastrar/Administrar Usuários</h2>
    <p>
        É de responsabilidade dos Supervisores cadastrar outros Supervisores e seus Auditores, bem como efetuar seu treinamento.
    </p>
    <p>
        Ao receber uma trilha o Supervisor deve buscar em sua unidade os servidores mais indicados para analisar e responder os registros de inconsistência.<br>
        O Supervisor então os cadastra no perfil de Analista no SGTA.
    </p>
    <p>
        Para cadastrar um usuário basta ir no menu superior Usuários/Cadastrar e preencher os campos. Quando necessário as informações de preenchimento 
        estão contidas dentro do próprio campo.
    </p>
    <p>
        Após o cadastramento do Supervisor será direcionado para tela onde se determinará quais as áreas de trilhas o novo usuário terá acesso. 
        Para autorizar ou desautorizar o acesso basta arrastar as respectivas áreas entre as listas (Áreas de Trilhas Disponíveis) e (Áreas de 
        Trilhas Autorizadas), o salvamento é feito automaticamente.
    </p>
    <p>
        Um e-mail com a senha será enviado para o endereço cadastrado do novo usuário, que poderá alterá-la caso deseje no meu superior Usuários/Trocar de Senha.
    </p>
    <p>
        Ao final dos trabalhos o Supervisor deverá bloqueá-los no SGTA ou simplismente remover a permissão de acesso à sua área de atuação (menu superior Usuários/Administrar).
    </p>
    <sub style="font-size: 70%;">Fig.1</sub>
    <img class="imagem" src="img/img22-1.png" alt="" style="width: 100%;"/>

</div>