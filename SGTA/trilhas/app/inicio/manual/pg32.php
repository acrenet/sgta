<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg32.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Supervisores (Visão Geral)</h2>
    <p>
        Os Supervidores são os responsáveis por garantir o correto andamento as Trilhas de Auditoria sob sua responsabilidade.
    </p>
    <p>
        Suas principais atribuições são:
    </p>
    
    <ul>
        <li>
            Cadastrar os Analstas e outros Supervisores em sua Unidade Administrativa bem como efetuar seu treinamento.
        </li>
        <li>
            Acompanhar e zelar pelo trabalho dos Analistas sob sua responsabilidade quanto à qualidade dos trabalhos e cumprimento de prazos estabelecidos em norma.
        </li>
    </ul>

    <p>
        <b>Nota: </b> O Supervisor pode efetuar qualquer ação que é permitido ao Analista como reservar um registro ou concluir uma justificativa.
    </p>
    
</div>