<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg44.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Auditores - Analisar Justificativas</h2>
    <p>
        Inconsistências são registros que possivelmente revelam problemas cadastrais e/ou de pagamentos incompatíveis com o ordenamento legal. 
        São obtidos através do cruzamento de diferentes bases de dados conforme critérios específicos para cada tipo de trilha.
    </p>
    <p>
        Trata-se de uma suspeita que necessita de uma análise mais detalhada e não necessariamente representa um ilícito.
    </p>
    <p>
        É competência do Auditor emitir o parecer sobre a justificativa apresentada pelos Analistas das Unidades Administrativas.
    </p>
    <p>
        Após o login na tela inicial o Auditor recebe um alerta caso haja algum registro em sua área de atuação aguardando análise. (Fig 1).
    </p>
    
    <sub>Fig.1</sub><br>
    <img src="img/img43_15.png" alt=""/>
    
    <p>
        Clicar neste link direciona o usuário para a tela onde são exibidos todos os registros aguardando análise (Fig 2). Auditor poderá escolher o 
        registro que deseja atuar clicando no ícone: <img src="img/img43_1.png" width="15" height="11"/>
    </p>
    
    <sub>Fig.2</sub><br>
    <img src="img/img44_5.png" alt="" style="width: 100%"/>
    
    <p>
        É possível também acessar as trilhas de auditoria pelo menu superior no item: "Trilhas/Auditoria". Por este caminho o Auditor visualiza
        as trilhas que estão disponíveis e não os registros (fig. 3). Auditor poderá escolher a trilha que deseja atuar clicando no ícone: 
        <img src="img/img43_1.png" width="15" height="11"/>, todos os registros da trilha serão exibidos.
    </p>
        
    <sub>Fig.3</sub><br>
    <img src="img/img44_6.png" alt="" style="width: 100%"/>
    
    <p>
        Se este ícone não estiver ativo significa que o usuário não tem acesso à aquela trilha. Para ter o acesso é necessário solicitar para Gestor responsável.
    </p>
    
    <p>
        No canto superior direito da tabela de seleção de trilhas há um campo de procura que pode ser utilizado para filtrar os registros exibidos,
        podendo ser preenchido com a Área, Trilha, Status ou a combinação destes, também pode ser lançado o termo "permitido" (sem aspas) para 
        que o sistema exiba apenas as trilhas que o Auditor possui acesso (fig. 4).<br>
    </p>
    <sub>Fig.4</sub><br>
    <img src="img/img43_2.png" width="264" height="122" alt=""/>
    
    <p>
        Selecionado a trilha você será direcionado à tela onde serão feitas as análises das justificativas.
    </p>
    <p>
        Os registros de inconsistências são exibidos um a um em forma de tabela. A informação constante na tabela depende de cada trilha, 
        entretanto as últimas 6 colunas são idênticas para todas as trilhas (fig. 5) e possuem funções conforme descritas a seguir:<br>
    </p>

    <ul>
        <li><b>Observação TCE: </b>Contém alguma informação pertinente cadastrada pelos Gestores ou Auditores.</li>
        <li><b>Resp. TCE: </b>Indica o usuário que está analisando o registro para que não haja duplicidade no trabalho.</li>
        <li><b>Status: </b> Indica o status que se encontra o registro, mais informações estão disponíveis abaixo.</li>
        <li><b>Flags: </b> São marcações que indicam ações que foram realizadas sobre o registro, mais informações estão disponíveis abaixo.</li>
        <li><b>Docs: </b> Indica a quantidade de documentos anexados ao registro, quando maior que zero o ícone fica ativo, bastando clicar
        sobre ele para visualizar os documentos anexados.</li>
        <li><b>Ações: </b>São o conjunto de botões de comando que quando ativos permitem que sejam realizadas ações sobre o registro, mais informações estão disponíveis abaixo.</li>
    </ul>
    
    <sub>Fig.5</sub><br>
    <img src="img/img44_1.png"/>

    <br>
    <h2>Status:</h2>
    <p>
        Os registros de inconsistências podem assumir 6 status diferentes, a tabela abaixo indica a seqüência natural do registro:
    </p>
    
    <table border="1">
        <thead>
            <tr>
                <th><b>Status</b></th>
                <th><b>Descrição</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="color: darkorange;"><b>pendente</b></td>
                <td>Indica que o registro ainda não foi analisado.</td>
            </tr>
            <tr>
                <td style="color: darkblue;"><b>em análise</b></td>
                <td>Indica que algum Auditor está atuando com o registro, a reserva é feita automaticamente e após a conclusão da análise
                a reserva também é retirada automaticamente. Nota: é possível analisar um registro que já esteja reservado por outro Auditor entretanto a reserva
                será transferida automaticamente.</td>
            </tr>
            <tr>
                <td style="color: darkgreen;"><b>justificado</b></td>
                <td>Indica que o Analista emitiu o seu parecer, concordando ou não com a inconsistência. Neste status o Auditor não iniciou sua análise.</td>
            </tr>
            <tr>
                <td style="color: blue;"><b>em análise</b></td>
                <td>Indica que algum Auditor iniciou o trabalho de análise sobre o que justificado pelo Analista.</td>
            </tr>
            <tr>
                <td style="color: magenta;"><b>concluído</b></td>
                <td>Indica que o Auditor considerou conclusas as informações prestadas pelo Analista do Órgão/Unidade Administrativa. 
                    Ações posteriores poderão ter sido desencadeadas, ver em Flags abaixo.</td>
            </tr>
            <tr>
                <td style="color: red;"><b>devolvido</b></td>
                <td>Indica que o Auditor não considerou conclusas as informações prestadas pelo Analista
                e o devolveu o registro para novas justificativas.</td>
            </tr>
        </tbody>
    </table>
    
    <br>
    <h2>Flags:</h2>
    <p>
        Flags são marcações chamam a atenção sobre a necessidade de se realizar alguma ação sobre o registro, ou demonstram ações que já ocorreram.
    </p>
    <p>
        Ao posicionar o mouse sobre uma flag mais informações serão exibidas à respeito da mesma.
    </p>
    
    <ul>
        <li style="list-style-image: url('img/img43_5.png')">
            Indica que o registro já teve a análise concluída pelo Auditor e este colocou o registro em monitoramento para uma reavaliação em um data futura. Muito comum
            quando na justificativa da inconsistência foi informado alguma atitude corretiva que demandará prazo para ser implementada.
        </li>
        <li style="list-style-image: url('img/img44_10.png')">
            Indica que o prazo do monitoramento se esgotou e o registro está aguardando uma ação do Auditor. Um alerta é emitido na tela inicial
            para o Auditor responsável.
        </li>
        <li style="list-style-image: url('img/img43_14.png')">
            Indica que o Auditor solicitou abertura de processo de Inspeção ou Auditoria. A abertura de dará em sistema de controle próprio, apenas
            o número do processo é armazenado neste sistema.
        </li>
        <li style="list-style-image: url('img/img43_4.png')">
            Indica que o Analista solicitou a alteração das informações por ele prestadas. Uma vez concluída a justificativa
            ela não pode ser mais alterada. Caso o Analista deseje modificar estas informações, deverá
            preencher o formulário próprio solicitando a alteração. O pedido chegará ao Auditor que não poderá
            concluir sua análise até que decida sobre o pedido. Nota: Não é possível solicitar alterações em registros com status ''concluído''.
        </li>
        <li style="list-style-image: url('img/img43_7.png')">
            Indica que o pedido de alteração foi aceito, o registro retorna para o status ''em análise'' reservado para o Analista que pediu a alteração.
            O solicitante será comunicado via e-mail.
        </li>
        <li style="list-style-image: url('img/img43_6.png')">
            Indica que o pedido de alteração não foi aceito. O solicitante será comunicado via e-mail.
        </li>
        <li style="list-style-image: url('img/img44_9.png')">
            Indica o descumprimento do prazo de justificativa determinado em decreto, o registro pode ser invocado pelo TCE para ações corretivas.
        </li>
    </ul>

    <br>
    <h2>Ações:</h2>
    
    <p>Ações são o conjunto de botões de comando que exibem detalhes sobre o registro correspondente ou modificam suas propriedades.</p>
    <p>Note que nem todas as ações estão disponíveis em todo o momento, variando principalmente em relação ao status que o registro se encontra.
    Assim se um botão de comando não estiver ativo significa que naquele momento a ação referente a ele não é permitida.</p>
    
    <table border="1">
        <thead>
            <tr>
                <th>Botão</th>
                <th>Ação</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_8.png" width="16" height="16" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Exibir mais Detalhes: </b>Por falta de espaço não é possível exibir todas as informações referentes a um registro
                        de inconsistência na tela.
                    </p>
                    <p>
                        Ao clicá-lo será exibido em uma tela popup mais detalhes referentes ao registro, sendo que seu conteúdo varia de trilha para trilha.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img44_2.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Incluir uma Observação no Registro: </b> Utilizado para gravar alguma informação para auxiliar o Analista
                        com seu trabalho. Está disponível em qualquer status exceto o concluído.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_9.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Anexar um Arquivo: </b> Abre um janela popup onde se pode anexar arquivos pdf, documentos de texto e planilhas eletrônicas
                        com até 10 megabytes.
                    </p>
                    <p>
                        Observações:<br>
                        <ul>
                            <li>Um arquivo só pode ser excluído por que o anexou.</li>
                            <li>A qualquer momento pode-se incluir novos arquivos mesmo após a análise ser concluída.</li>
                        </ul>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img44_3.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Validar Justificativa: </b> Abre uma janela popup para o Auditor emitir o seu parecer sobre a justificativa
                        apresentada pelo Analista e os documentos por ele anexados.
                    </p>

                    <img src="img/img52_1.png" alt="" style="width: 99%"/>

                    <p>
                        Se estiver satisfeito com a justificativa e os documentos anexados o registro poderá ser aquivado.
                    </p>
                    <p>
                        Caso haja na justificativa atitudes corretivas que ainda não foram implementadas o registro poderá ser colocado em monitoramento,
                        selecionando uma data futura para uma re-análise.
                    </p>
                    <p>
                        Se julgar necessário, poderá o Auditor solicitar a abertura de um processo de inspeção ou auditoria. Pedido este que será 
                        decidido pelo Superintendente da Área.
                    </p>
                    <p>
                        Caso o Auditor não julgue suficientes ou não concorde as justificativas apresentadas, poderá ele devolver o registro à 
                        Unidade Administrativa para complementações.
                    </p>
                    <p>
                        Poderá ser solicitado ao Analista que se anexe novos documentos sem a devolução do registro, entretanto o mesmo não poderá alterar
                        seu parecer.
                    </p>
                    <p>
                        Nesta tela também estão disponíveis atalhos para anexar documentos comprobatórios, visualizar os arquivos anexados e acesso a detalhes do registro,
                        para que não seja necessário fechar a tela de análise.
                    </p>
                    <p>
                        Caso não tenha concluído a análise é possível apenas salvar para continuar posteriormente. O registro continuará reservado.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img44_4.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Analisar Pedido de Alteração: </b> Uma vez concluída a justificativa ela não pode ser mais alterada. 
                        Caso o Analista deseje modificar sua justificativa ele envia um pedido de alteração. 
                    </p>
                    <p>
                        A partir deste momento o Auditor não poderá mais emitir seu parecer até que decida sobre o pedido.
                    </p>
                    <p>
                        O Auditor poderá aceitar ou não o pedido de alteração, caso aceite o registro retorna para o status de
                        ''em justificativa'' reservado para quem solicitou a alteração.
                    </p>
                    <p>
                        Aceitando ou não a decisão deverá ser justificada.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img44_7.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Ações Complementares:</b> Exibe um formulário popup que permite as seguintes ações: Alterar o prazo do monitoramento, Encerrar o 
                        monitoramento e arquivar o registro de inconsistência, Encerrar o monitoramento e requisitar abertura de inspeção/auditoria,
                        Reabrir o registro de inconsistência e Invocar registro não justificado pelo órgão no prazo legal (fig. 7).
                    </p>
                    <p>
                        Alguns botões poderão estar desabilitados de acordo com o status do registro. Ex: Não é possível reabrir um registro que
                        ainda não foi fechado ou invocar um registro que está dentro do prazo de justificativa.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img src="img/img43_12.png" alt=""/>
                </td>
                <td>
                    <p>
                        <b>Botão Consultar Justificativas/Pareceres/Eventos: </b> Esta opção permite visualizar justificativas/pareceres emitidos pelos Analistas e Auditores.
                        Também é possível ver o históricos dos eventos que ocorreram com o registro, as datas e os usuários que nele trabalharam.
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    
    <br>
    <h2>Exportar o Conteúdo da Tabela.</h2>
    
    <p>No cabeçalho da tabela encontram-se três botões: PDF, Excel, CSV, (Fig 6). Eles podem ser utilizados para exportar o rapidamente conteúdo
    que estiver sendo exibido na tabela.</p>
    <p>Note que só é exportado o que estiver sendo exibido, caso tenha sido utilizado a procura no canto superior direito na tabela apenas os
    registros filtrados serão exportados.</p>
    
    <sub>Fig.6</sub><br>
    <img src="img/img43_13.png"/>
    <br><br>
    <sub>Fig.7 (Tela Ações Complementares).</sub><br>
    <img src="img/img44_8.png"/>
    
</div>