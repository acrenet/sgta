<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg36.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo"/></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Gestores - Distribuição das Inconsistências entre os Auditores.</h2>
    
    <p>
        Uma vez justificada a inconsistência pelo Analista ela deverá ser distribuída para um Auditor emitir o seu parecer. O responsável pela 
        distribuição é o Gestor da área onde está inserida a trilha.
    </p>
    <p>
        Ao efetuar login no Sistema o Gestor será direcionado para a tela inicial. Caso haja algum registro de sua área aguardando distribuição, nesta tela
        haverá um alerta (fig.1) informando a quantidade, bastando clicar sobre o link para ser direcionado à tela de distribuição. Para retornar à 
        tela inicial, basta clicar no botão Home do menu superior.
    </p>
    <sub>Fig.1</sub><br>
    <img src="img/img36_1.png" alt=""/>
    <p>
        A tela de distribuição (Fig.2) listará todos os registros aguardando distribuição, para distribuir um registro basta clicar em seu 
        botão de distribuição correspondente: 
        <img src="img/img36_3.png" alt=""/>
    </p>
    <sub>Fig.2</sub><br>
    <img src="img/img36_2.png" alt=""/>
    <p>
        Será aberto uma tela popup (Fig.3) onde será exibido o nome de todos os Gestores e Auditores vinculados à area e a quantidade de registros que 
        atualmente cada um é responsável. Para distribuir basta clicar no botão: 
        <img src="img/img36_5.png" alt=""/> do Auditor correspondente.
    </p>
    <br>
    <sub>Fig.3</sub><br>
    <img src="img/img36_4.png" alt=""/>
    <p>
        Após a distribuição o popup é fechado e a tabela é atualizada.
    </p>
</div>