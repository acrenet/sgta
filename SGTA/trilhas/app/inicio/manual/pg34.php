<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg34.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Permissões de Acesso à Trilhas</h2>
    
    <p>
        As trilhas são divididas em áreas, quando um usuário é cadastrado deve-se definir quais áreas o usuário poderá ter acesso. Entretando
        é util também visualizar quais os usuários podem acessar determinadas áreas, facilitando assim a organização por parte dos Gestor responsável.
    </p>
    <p>
        Para acessar a tela vá no menu superior "Gestão/Permissões de Acesso à Trilha", Gestores podem escolher qualquer Órgão/Unidade, os
        demais ficam restritos ao Órgão/Unidade onde são lotados.
    </p>
    <p>
        Em seguida basta escolher a área de trilha desejada e o sistema irá separar os funcionarios do órgão selecionado em duas listas, à esquerda
        os que não possuem acesso e a direita os que possuem acesso.
    </p>
    <p>
        Para liberar ou revogar o acesso basta transferir o usuário de uma lista para a outra clicando e arrastando. O salvamento é automático.
    </p>
    <p>
        Para os usuários a ação terá efeito após um novo login.
    </p>
    <br>
    <img src="img/img34_1.png" class="imagem" style="width: 100%;"/>
    
</div>