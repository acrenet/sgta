<link href="manual.css" rel="stylesheet" type="text/css"/>

<a  class="imprimir" href="imprimir.php?p=pg21.php" target="_blank" title="Imprimir Artigo" style="position: fixed; left: 20px; top: 20px;"><img class="imprimir" src="../../../images/print.jpg" width="32" height="32" alt="Imprimir Artigo" onmouseover="this.src='../../../images/print2.jpg';" onmouseout="this.src='../../../images/print.jpg';" /></a>

<div class="especial">
    
    <h2 style="text-align: center;" >Administradores - Visão Geral</h2>
    <p>
        Este é o perfil com maior autonomia dentro do sistema, além de suas atribuições específicas tem também a prerrogativa de realizar quaisquer 
        atividades atribuídas aos Gestores e aos Auditores.
    </p>
    <p>
        São as seguintes responsabilidades dos Administradores:
    </p>
    <ul style="text-indent: 0em;">
        <li>Efetuar o cruzamento dos dados e dar a carga inicial no sistema.</li>
        <li>Juntamente com os Gestores do TCE responder e acompanhar os pedidos de suporte registrados no sistema.</li>
        <li>Cadastrar dos Gestores do TCE bem como dar-lhes permissões de acesso.</li>
    </ul>
</div>