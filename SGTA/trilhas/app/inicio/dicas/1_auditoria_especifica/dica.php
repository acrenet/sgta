<img src="/trilhas/images/dica.jpeg" alt="" width="150px;" align="left" style="margin-right: 10px; float: left;"/>

<span style="font-weight: bold; font-size: 120%; display: inline-block; margin: 60px 0 15px;">
    Auditoria Específica em um Processo/CPF/CNPJ/Nome/Razão Social.
</span><br>
<p style="color: red; font-size: 70%; font-weight: bold;">
    * Esta dica é de grande relevância.<br>
    * Esta dica é exibida apenas aos servidores do TCE.<br>
    * Esta dica será exibida somente uma vez.
</p>
<p>
    Em diversas circunstâncias, pode ser interessante realizar um aprofundamento no processo investigativo sobre uma pessoa, empresa ou processo, a exemplo
    de uma empresa que foi considerada inidônea e seus sócios abriram uma nova para continuar cometendo contratando com o Estado, ou pode-se querer listar
    todos os processos que determinada pessoa que cometeu ilícitos participa.
</p>
<p>
    A reincidência de uma pessoa ou empresa em trilhas mesmo que de temas diferentes pode apresentar algum nexo de causa e efeito que pode contribuir
    em análises investigativas em curso.
</p>
<p>
    Assim além de ajudar na tomada de decisão do Auditor pode-se também abrir novos processos de auditoria ou expedição de orientações e 
    alertas pelo TCE.
</p>
<p>
    Na maioria dos sistemas de auditoria o input dos dados é feito através de formulários pré-configurados e os detalhes
    específicos que cada auditoria acabam sendo inseridos em campos meramente descritivos, por mais detalhado que tenha sido o projeto da aplicação é impossível
    prever todas as especificidades das auditorias existentes e das que deverão aparecer no futuro.
</p>
<p>
     Portanto o funcionamento destes sistemas se assemelham muito mais a um carteiro entregando pacotes, que têm acesso a algumas informações que
     estão escritas na caixa mas que nunca o sabe seu conteúdo exato.
</p>
<p>
    O SGTA projetado a aceitar qualquer conteúdo, sendo que cada campo de cada trilha tem um local armazenamento específico, sem
    campos genéricos descritivos e compartilhados entre trilhas, sendo a entrada de dados feito de forma totalmente automática direto dos arquivos de resultado,
    evitando erros de digitação.
</p>
<p>
    Essa parametrização de conteúdo permite que o SGTA possa cruzar o resultado de qualquer trilha mesmo de áreas diferentes como compras,
     pessoal, O.S., etc.
</p>
<p>
    Imagine que determinada trilha na área de Organizações Sociais possua um campo com o número do PIS e outra trilha da área de Pessoal 
    também tenha um campo com o número do PIS, internamente é rápido configurar a aplicação de modo que ela "saiba" todas as trilhas que possuem campos do tipo "número
    do PIS" e assim realizar seu cruzamento.
</p>
<p>
    Para realizar a busca é bastante simples, na tela "exibir mais detalhes" de qualquer registro de inconsistência, descer até o rodapé e
    no formulário de pesquisa conforme figura 1, colar ou digitar o CPF, CNPJ, número de processo, nome ou razão social e fazer a
    busca clicando no botão correspondente.
</p>
<p>
    Inicialmente a aplicação está programada para cruzar estes tipos de informações citadas mas como já foi exemplificado, outros tipos
    poderão ser incluídos.
</p>
<sub style="font-size: 70%;">Fig.1</sub><br>
<img src="/trilhas/app/inicio/dicas/1_auditoria_especifica/img1.png" alt=""/>
<p>
    Note que para o nome ou razão social é possível digitar apenas parte, também é indiferente se for maiúsculo ou minúsculo. Acentos 
    são desprezados assim buscar por "ARAUJO" ou por "araújo" obtêm-se o mesmo resultado.
</p>
<p>
    O sistema abrirá em um nova aba com o resultado, as informações serão exibidas de forma resumida, sendo as trilhas com conteúdos diferentes
    não é possível que sejam exibidas na mesma tabela.
</p>
<p>
    Note que no exemplo apresentado na figura 2 foi feita uma busca pela palavra chave "cardoso" mas no primeiro registro não aparece esta palavra chave,
    isso é muito comum, a palavra chave será encontrada nos detalhes do registro de inconsistência.
</p>
<p>
    Outro ponto importante é que embora você veja o resultado resumido você não terá acesso aos detalhes do registro caso ele seja de uma área
    que você não possui acesso.
</p>
<sub style="font-size: 70%;">Fig.2</sub><br>
<img src="/trilhas/app/inicio/dicas/1_auditoria_especifica/img2.png" alt="" width="990"/>