<img src="/trilhas/images/dica.jpeg" alt="" width="150px;" align="left" style="margin-right: 10px; float: left;"/>

<span style="font-weight: bold; font-size: 120%; display: inline-block; margin: 60px 0 15px;">
    Resultado da Audioria.
</span><br>
<p style="color: red; font-size: 70%; font-weight: bold;">
    * Esta dica é de grande relevância.<br>
    * Esta dica é exibida apenas aos servidores do TCE.<br>
    * Esta dica será exibida somente uma vez.
</p>
<br><br>

<div id="4_orientacao" class="well well-sm" style="text-align: justify;">
    <p>
        Ao concluir o trabalho de auditoria em um registro de inconsistência será solicitado ao auditor que preencha alguns parâmetros conforme 
        figura 1.
    </p>
    <br>
    <sub style="font-size: 80%;">Fig.1</sub><br>
    <img src="/trilhas/app/inicio/dicas/3_resultado_da_auditoria/Img1.png" alt="" style="width: 70%;"/>
    <br><br>
    <p>
        <b>O preenchimento destes parâmetros destina-se ao acompanhamento da Trilha de Auditoria, é através deles que poderam ser 
        mensurados os resultados alcançados.</b><br>
        <b>Atenção: </b>Os itens que forem marcados como "Solicitado" tem interpretação diferente pelo sistema de acordo com o resultado da 
        análise e influenciam nos cálculos estatísticos e nos relatórios que o sistema emite:<br>
        <ol>
            <li>Se o registro estiver em monitoramento: O sistema entende que o órgão acatou a recomendação porém ainda não a implementou.
                Ou seja, o TCE está aguardando a implementação.</li>
            <li>Se o registro foi arquivado ou foi recomendado inspeção ou auditoria: O sistema entende que o órgão deixou de 
                cumprir ou se recusou a atender a recomendação.</li>
        </ol>
    </p>
    <p>
        <b>Expedição de Instrumentos Legais:</b> Marcar "Sim" caso tenha sido feito a recomendação de expedição de 
        algum instrumento legal (leis, decretos, etc.) e esta foi efetivada pelo órgão/entidade, marcar "Solicitado" caso a 
        recomendação tenha sido aceita mas ainda não implementada, o registro deverá ficar em monitoramento até a implementação. 
        Marcar "Solicitado" caso a recomendação não tenha sido atendida, o registro deverá ser arquivado ou deverá ser 
        solicitado a abertura de processo de inspeção ou auditoria.
    </p>
    <p>
        <b>Criação/Reformulação de Sistemas:</b> Marcar "Sim" caso tenha sido feito a recomendação de criação ou 
        reformulação de algum sistema de gerenciamento de dados e esta foi implementada pelo órgão. Marcar "Solicitado" caso 
        a recomendação tenha sido aceita mas ainda não implementada, o registro deverá ficar em monitoramento até a implementação. 
        Marcar "Solicitado" caso a recomendação não tenha sido atendida, o registro deverá ser arquivado ou deverá ser 
        solicitado a abertura de processo de inspeção ou auditoria.
    </p>
    <p>
        <b>Capacitação Técnica dos Agentes:</b> Marcar "Sim" caso tenha sido feito a recomendação de capacitação ou 
        orientação de agentes/servidores envolvidos no processo e esta foi implementada pelo órgão. Marcar "Solicitado" 
        se a recomendação foi aceita mas ainda não implementada, o registro deverá ficar em monitoramento até a implementação. 
        Marcar "Solicitado" caso a recomendação não tenha sido atendida, o registro deverá ser arquivado ou deverá ser solicitado a 
        abertura de processo de inspeção ou auditoria.
    </p>
    <p>
        <b>Devolução de Numerário:</b> Marcar "Sim" caso tenha sido solicitado a devolução de numerário e foi efetivada pelo órgão. 
        Marcar "Solicitado" se a recomendação será efetivada ou se estiver ocorrendo de forma parcelada, o registro 
        deverá ficar em monitoramento até a quitação das parcelas. Marcar "Solicitado" caso a recomendação não tenha sido 
        atendida, o registro deverá ser arquivado ou deverá ser solicitado a abertura de processo de inspeção ou auditoria.
    </p>
    <p>
        <b>Valor Ressarcido:</b> Preencher o valor que foi ressarcido ao Estado, caso haja apenas a promessa de devolução
        o registro deverá permanecer em monitoramento até a efetivação da devolução.
    </p>
    <p>
        <b>Economia Mensal:</b> Preencher o valor quando se puder determinar a economia mensal gerada para o Estado em
        virtude do trabalho de auditoria.
    </p>
</div>


