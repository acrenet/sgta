var execute_cod = 0;
var destino = "server.php";

$(document).ready(function(){
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
    
    popup_form_();
});

function printMousePos(event) {
  console.log("clientX: " + event.clientX + " - clientY: " + event.clientY);
}

document.addEventListener("click", printMousePos);

function popup_form_(){
    $("#popup_form").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 1000,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        open: function( event, ui ) {
            rolar_para("#div_dica");
        }
    });
}

function rolar_para(elemento) {
    $('html, body').animate({
      scrollTop: $(elemento).offset().top-1200
    }, 300);
}

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                
                if(retorno.exibir == true){
                    $("#div_suporte").html(retorno.html);
                    $("#div_suporte").slideDown();
                }
                if(retorno.exibir2 == true){
                    $("#div_monitoramento").html(retorno.html2);
                    $("#div_monitoramento").slideDown();
                }
                
                if(retorno.exibir3 == true){
                    $("#div_pendentes").html(retorno.html3);
                    $("#div_pendentes").slideDown();
                }
                
                if(retorno.exibir4 == true){
                    $("#div_nao_justificados").html(retorno.html4);
                    $("#div_nao_justificados").slideDown();
                }
                
                if(retorno.exibir5 == true){
                    $("#div_nao_analisados").html(retorno.html5);
                    $("#div_nao_analisados").slideDown();
                }
                
                if(retorno.exibir6 == true){
                    $("#div_nao_distribuidos").html(retorno.html6);
                    $("#div_nao_distribuidos").slideDown();
                }
                
                if(retorno.exibir7 == true){
                    $("#div_pedido_auditoria").html(retorno.html7);
                    $("#div_pedido_auditoria").slideDown();
                }
                
                if($("#exibir_dica").val() == "1"){
                    setTimeout(function(){
                        $("#popup_form").dialog("open");
                    }, 1000);
                }
                
                if(retorno.linemax == "ocultar"){
                    $("#div_graficos").hide();
                    $("#div_titulo_graficos").hide();
                }else{
                    $("#div_titulo_graficos").show();
                    $("#div_graficos").show();
                    config.data = retorno.lineChartData;
                    config.options.scales.yAxes[0]['ticks']['max'] = retorno.linemax;

                    config2.data = retorno.barChartData;
                    config2.options.scales.yAxes[0]['ticks']['max'] = retorno.barmax;

                    pieChartData.datasets.splice(0, 1);
                    pieChartData.datasets.push(retorno.pieChartData);

                    config3.data = pieChartData;
                    config3.options.title.text = "Resultado das Análise das Inconsistências (últimos 6 meses)";

                    var ctx1 = document.getElementById("canvas1").getContext("2d");
                    window.myLine = new Chart(ctx1, config);

                    var ctx2 = document.getElementById("canvas2").getContext("2d");
                    window.myBar = new Chart(ctx2, config2);

                    var ctx3 = document.getElementById("canvas3").getContext("2d");
                    window.myDoughnut = new Chart(ctx3, config3);
                }
                
                break;
            case "update":
                
                config.data = retorno.lineChartData;
                config.options.scales.yAxes[0]['ticks']['max'] = 100;
                                            
                config2.data = retorno.barChartData;
                config2.options.scales.yAxes[0]['ticks']['max'] = 100;
                
                pieChartData.datasets.splice(0, 1);
                pieChartData.datasets.push(retorno.pieChartData);
                config3.data = pieChartData;
                config3.options.title.text = "Resultado das Análise das Inconsistências (últimos 7 meses)";
                
                window.myLine.update();
                window.myBar.update();
                window.myDoughnut.update();
                
                break
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            alert("funciona!!!");
    }
    execute_cod = 0;
}

var setting = {
        view: {
                selectedMulti: false
        },
        data: {
                keep: {
                        parent:true,
                        leaf:true
                },
                key: {
                        title:"t"
                },
                simpleData: {
                        enable: true
                }
        },
        callback: {
                onClick: onClick
        }
};

var zNodes =[
        /*{ id:1.2, pId:0, name: "Decreto 8.936 de 07 de abril de 2017", t:"", ename: "http://www.gabinetecivil.go.gov.br/pagina_decretos.php?id=16556"},
    */    
    { id:1, pId:0, name:"Visão Geral", t:"", ename:"pg1.php"},
        { id:1.1, pId:0, name:"Fluxograma do Processo", t:"", ename:"pg11.php"},
        { id:2, pId:0, name:"Administradores", t:"", open:true},
        { id:21, pId:2, name:"Visão Geral", t:"", ename:"pg21.php"},
        { id:23, pId:2, name:"Cadastrar/Administrar Usuários", t:"", ename:"pg22.php"},
        { id:22, pId:2, name:"Suporte", t:"", ename:"pg23.php"},
        { id:3, pId:0, name:"Gestores", t:"", open:true},
        { id:31, pId:3, name:"Visão Geral", t:"", ename:"pg31.php"},
        { id:35, pId:3, name:"Cadastrar/Administrar Usuários", t:"", ename:"pg35.php"},
        { id:33, pId:3, name:"Homologação", t:"", ename:"pg33.php"},
        { id:35, pId:3, name:"Distribuição", t:"", ename:"pg36.php"},
        { id:36, pId:3, name:"Auditoria/Inspeção", t:"", ename:"pg37.php"},
        { id:34, pId:3, name:"Permissões de Acesso à Trilhas", t:"", ename:"pg34.php"},
        { id:5, pId:0, name:"Auditores", t:"", open:true},
        { id:51, pId:5, name:"Visão Geral", t:"", ename:"pg41.php"},
        { id:52, pId:5, name:"Analisar Justificativas", t:"", ename:"pg44.php"},
        { id:6, pId:0, name:"Supervisores", t:"", open:true},
        { id:61, pId:6, name:"Visão Geral", t:"", ename:"pg32.php"},
        { id:62, pId:6, name:"Pedido de Prorrogação de Prazo", t:"", ename:"pg38.php"},
        { id:63, pId:6, name:"Cadastrar/Administrar Usuários", t:"", ename:"pg63.php"},
        { id:4, pId:0, name:"Analistas", t:"", open:true},
        { id:42, pId:4, name:"Visão Geral", t:"", ename:"pg42.php"},
        { id:43, pId:4, name:"Justificar Inconsistências", t:"", ename:"pg43.php"},
        { id:7, pId:0, name:"Dicas do Dia", t:"", open:true},
        { id:71, pId:7, name:"Cruzamento entre Trilhas", t:"", ename:"dicas/1_auditoria_especifica/dica.php"},
        { id:72, pId:7, name:"Relatório de Acompanhamento da Trilha", t:"", ename:"dicas/2_relatorio_trilha/dica.php"},
        { id:73, pId:7, name:"Resultado da Auditoria", t:"", ename:"dicas/3_resultado_da_auditoria/dica.php"}

];

var log, className = "dark";

function onClick(event, treeId, treeNode, clickFlag) {
        if(!treeNode.isParent){
            consultar();
        }
}		
function showLog(str) {
        if (!log) log = $("#log");
        log.append("<li class='"+className+"'>"+str+"</li>");
        if(log.children("li").length > 8) {
                log.get(0).removeChild(log.children("li")[0]);
        }
}
function getTime() {
        var now= new Date(),
        h=now.getHours(),
        m=now.getMinutes(),
        s=now.getSeconds();
        return (h+":"+m+":"+s);
}

$(document).ready(function(){
        $.fn.zTree.init($("#treeDemo"), setting, zNodes);
});


function consultar(){


    var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
    nodes = zTree.getSelectedNodes(),
    treeNode = nodes[0];

    //console.log(treeNode.ename);

    var $iframe = $('#iframe1');
    
    var str = treeNode.ename;
    if(str.substring(0, 5) == "http:"){
        $iframe.attr('src', treeNode.ename);
    }else if(str.substring(0, 5) == "dicas"){
        $iframe.attr('src', treeNode.ename);
    }else{
        $iframe.attr('src',"manual/" + treeNode.ename);
    }

}

function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}


//line++++++++++++++++++++++++++++++++++++++++++
        
        var lineChartData = {};

        var config = {
            type: 'line',
            data: lineChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    position: 'bottom'
                },
                title:{
                    display:true,
                    text:'Tempo médio para justificativa em dias.'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                                display: true,
                                ticks: {
                                    max: 40,
                                    beginAtZero:true
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Dias'
                                }
                            }]
                }
            }
        };
        
        
//bar++++++++++++++++++++++++++
        var color = Chart.helpers.color;
        var barChartData = {};
        
        var config2 = {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    position: 'bottom'
                },
                title: {
                    display: true,
                    text: 'Comparativo entre registros liberados, publicados e justificados.'
                },
                scales: {
                    yAxes: [{
                                display: true,
                                ticks: {
                                    max: 100,
                                    beginAtZero:true
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Quantidade'
                                }
                            }]
                }
            }
        };

        var colorNames = Object.keys(window.chartColors);

//pie+++++++++++++++++++++++++++++++++
    
    var pieChartData = {
        datasets: [],
        labels: [
            "Arquivado",
            "Monitoramento",
            "Devolução",
            "Auditoria"
        ]        
    };

    var config3 = {
        type: 'doughnut',
        data: pieChartData,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scaleShowLabels: true,
            tooltipFontSize: 12,
            tooltipYPadding: 3,
            tooltipXPadding: 3,
            tooltipTemplate: "<%= label %>: <%= value %>",
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%>: <%=segments[i].value%><%}%></li><%}%></ul>",
            legend: {
                    position: 'bottom'
                },
            showTooltips: true,
            title: {
                    display: true,
                    text: 'Resultado das Análise das Inconsistências (últimos 6 meses).'
                },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };
    