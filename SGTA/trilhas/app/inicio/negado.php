<?php

   //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
?>

<div class="container">
    <br>
    <h3>Desculpe mas seu perfil não permite o acesso à pagina.</h3>
    <br><br>
    <a class="btn btn-info" href="inicio.php"><span class="fa fa-home fa-lg"></span> Ir para a página inicial.</a>
</div>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");