var execute_cod = 0;
var destino = "server2.php";

$(document).ready(function(){
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
    
    popup_form_();
    
});

function popup_form_(){
    $("#popup_form").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 'auto',
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function rolar_para(elemento) {
    $('html, body').animate({
      scrollTop: $(elemento).offset().top-300
    }, 300);
}

function resposta(retorno){
    
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                
                $("#cmb_area").html(retorno.areas);
                $("#tb_trilhas").html(retorno.tabela);
                $("#operacao").val("cadastrar");
                $("#CodGrupo").val("-1");
                
                break;
            case "consultar":
                $("#operacao").val("editar");
                $("#CodGrupo").val(retorno.trilha.CodGrupo);
                $("#cmb_area").val(retorno.trilha.CodTipo);
                $("#cmb_codigo").val(retorno.trilha.CodigoOrgao);
                $("#txt_nome").val(retorno.trilha.NomeTrilha);
                $("#txt_plugin").val(retorno.trilha.TabelaDetalhe);
                $("#cmb_tipo").val(retorno.trilha.TipoTrilha);
                
                rolar_para("#cmb_area");
                
                break;
            case "cadastrar":
                $("#tb_trilhas").html(retorno.tabela);
                $("#msg_sucesso").html("Trilha cadastrada com sucesso.");
                $("#popup_sucesso").dialog("open");
                cancelar();
                break;
            case "editar":
                $("#tb_trilhas").html(retorno.tabela);
                $("#msg_sucesso").html("Alterações efetuadas com sucesso.");
                $("#popup_sucesso").dialog("open");
                cancelar();
                break;
            case "excluir":
                $("#tb_trilhas").html(retorno.tabela);
                $("#msg_sucesso").html("Trilha excluída com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "cadastrar_area":
                $("#cmb_area").html(retorno.areas);
                $("#popup_form").dialog("close");
                $("#msg_sucesso").html("Área cadastrada com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../inicio/inicio.php");
    }
    execute_cod = 0;
}

function salvar(){
    formdata = $("#form1").serialize();
    if($("#operacao").val() == "cadastrar"){
        $("#CodGrupo").val("-1");
        $("#msg_dialogo").html("Você está certo da inclusão desta trilha?");
    }else{
        $("#msg_dialogo").html("Você está certo da alteração dos dados desta trilha?");
    }
    $("#popup_dialogo").dialog("open");
}

function editar(CodGrupo){
    $("#operacao").val("consultar");
    $("#CodGrupo").val(CodGrupo);
    formdata = $("#form1").serialize();
    submit_form(destino);
    $("#operacao").val("cadastrar");
    return false;
}

function excluir(CodGrupo){
    $("#operacao").val("excluir");
    $("#CodGrupo").val(CodGrupo);
    formdata = $("#form1").serialize();
    $("#msg_dialogo").html("<b>Esta operação não poderá ser desfeita.<br>Deseja continuar?</b>");
    $("#popup_dialogo").dialog("open");
    return false;
}

function cancelar(){
    $("#operacao").val("cadastrar");
    $("#CodGrupo").val("-1");
    $("#cmb_area").val("0");
    $("#cmb_codigo").val("0");
    $("#txt_nome").val("");
    $("#txt_plugin").val("");
    $("#cmb_tipo").val("0");
}

function cadastrar_area(){
    formdata = $("#form2").serialize();
    $("#msg_dialogo").html("<b>A informação está correta?</b>");
    $("#popup_dialogo").dialog("open");
}
