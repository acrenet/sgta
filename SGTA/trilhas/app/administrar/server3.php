<?php
$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../obj/conexao.php");
require_once("../../obj/orgaos.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    $objcon = new conexao();
    
    switch ($array["operacao"]) {
        case "load":                        //------------------------------------------------------------------------------------------------------

            gera_tabela();
            
            retorno();
            break;
        case "consultar":
            $CodOrgao = $_POST['CodOrgao'];
            
            $obj_orgao = new orgaos();
            $obj_orgao->consulta_orgaos($CodOrgao);
            if($obj_orgao->erro != ""){
                throw new Exception($obj_orgao->erro);
            }
            $query = $obj_orgao->query;
            $array['orgao'] = mysqli_fetch_assoc($query);
            
            retorno();
            break;
        case "cadastrar":    
        case "editar":    
            $CodOrgao = $_POST['CodOrgao'];
            $RHNet = trim($_POST['txt_rhnet']);
            $ComprasNet = $_POST['txt_comprasnet'];
            $SiofiNet = $_POST['txt_siofinet'];
            $CodUnidadeEsfinge =  $_POST['txt_sfinge'];
            $NomeOrgao = trim(strtoupper($_POST['txt_nome']));
            $SiglaOrgao = trim(strtoupper($_POST['txt_sigla']));
            if(isset($_POST['cb_controle'])){
                $UnidadeDeControle = "true";
            }else{
                $UnidadeDeControle = "false";
            }
            
            if($NomeOrgao == ""){
                $array["alerta"] = $array["alerta"]."O Nome do Órgão é de preenchimento obrigatório.<br>";
            }
            
            if($SiglaOrgao == ""){
                $array["alerta"] = $array["alerta"]."A Sigla do Órgão é de preenchimento obrigatório.<br>";
            }
            
            if(!is_numeric($RHNet)){
                $array["alerta"] = $array["alerta"]."O código do RHNet é inválido.";
            }
            
            if($array['alerta'] != ""){
                retorno();
            }
            
            $obj_orgao = new orgaos();
            $obj_orgao->cadastra_orgao($CodOrgao, $RHNet, $ComprasNet, $SiofiNet, $NomeOrgao, $SiglaOrgao, $UnidadeDeControle,$CodUnidadeEsfinge);
            if($obj_orgao->erro != ""){
                throw new Exception($obj_orgao->erro);
            }
            
            gera_tabela();
            
            retorno();
            break;
        case "excluir":
            $CodOrgao = $_POST['CodOrgao'];
            
            $obj_orgao = new orgaos();
            $obj_orgao->excluir_orgao($CodOrgao);
            if($obj_orgao->erro != ""){
                throw new Exception($obj_orgao->erro);
            }
            
            gera_tabela();
            
            retorno();
            break;
        default:                            //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}

function gera_tabela(){
    try{
        global $array, $objcon;
        
            $sql = "Select * From orgaos Order By NomeOrgao";
            $query = $objcon->select($sql);
            if($objcon->erro != ""){
                throw new Exception($objcon->erro);
            }
            
            $tabela = "";
            
            while ($row = mysqli_fetch_array($query)){
                
                if($row['UnidadeDeControle'] == true){
                    $UnidadeDeControle = "<b><i><u>SIM</u></i></b>";
                }else{
                    $UnidadeDeControle = "NÃO";
                }
                               
                $tabela = $tabela .
                '
                <tr>
                    <td>'.$row['NomeOrgao'].'</td>
                    <td>'.$row['SiglaOrgao'].'</td>
                    <td>'.$row['CodOrgao'].'</td>
                    <td>'.$row['ComprasNet'].'</td>
                    <td>'.$row['SiofiNet'].'</td>
                    <td>'.$row['CodUnidadeEsfinge'].'</td>
                    <td>'.$UnidadeDeControle.'</td>
                    <td style="font-size: 140%; width: 60px;">
                        <a href="#" title="Editar Registro" class="especial" onclick="return editar('.$row['CodOrgao'].');"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href="#" title="Excluir Registro" class="excluir" onclick="return excluir('.$row['CodOrgao'].');"><span class="glyphicon glyphicon-remove-circle"></span></a>
                    </td>
                </tr>
                ';
            }
            
            $array['tabela'] = $tabela;
        
    } catch (Exception $exc){
        $array["erro"] = $exc->getMessage();
        retorno();
    }
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}