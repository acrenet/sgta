var execute_cod = 0;
var destino = "server3.php";

$(document).ready(function(){
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
    
    //rolar_para("#txt_nome");
});


function rolar_para(elemento) {
    $('html, body').animate({
      scrollTop: $(elemento).offset().top-200
    }, 300);
}

function resposta(retorno){
    
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                
                $("#tb_orgaos").html(retorno.tabela);
                $("#operacao").val("cadastrar");
                $("#CodOrgao").val("-1");
                
                break;
            case "consultar":
                $("#operacao").val("editar");
                $("#CodOrgao").val(retorno.orgao.CodOrgao);
                $("#txt_nome").val(retorno.orgao.NomeOrgao);
                $("#txt_sigla").val(retorno.orgao.SiglaOrgao);
                $("#txt_rhnet").val(retorno.orgao.CodOrgao);
                $("#txt_comprasnet").val(retorno.orgao.ComprasNet);
                $("#txt_siofinet").val(retorno.orgao.SiofiNet);
                $("#txt_sfinge").val(retorno.orgao.CodUnidadeEsfinge);
                
                
                if(retorno.orgao.UnidadeDeControle == 1){
                    $('#cb_controle').prop('checked', true);
                }else{
                    $('#cb_controle').prop('checked', false);
                }
                
                rolar_para("#txt_nome");
                
                break;
            case "cadastrar":
                $("#tb_orgaos").html(retorno.tabela);
                $("#msg_sucesso").html("Órgão/Unidade cadastrada com sucesso.");
                $("#popup_sucesso").dialog("open");
                cancelar();
                break;
            case "editar":
                $("#tb_orgaos").html(retorno.tabela);
                $("#msg_sucesso").html("Alterações efetuadas com sucesso.");
                $("#popup_sucesso").dialog("open");
                cancelar();
                break;
            case "excluir":
                $("#tb_orgaos").html(retorno.tabela);
                $("#msg_sucesso").html("Órgão/Unidade excluída com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../inicio/inicio.php");
    }
    execute_cod = 0;
}

function salvar(){
    formdata = $("#form1").serialize();
    if($("#operacao").val() == "cadastrar"){
        $("#CodOrgao").val("-1");
        $("#msg_dialogo").html("Você está certo da inclusão do órgão/unidade?");
    }else{
        $("#msg_dialogo").html("Você está certo da alteração dos dados do órgão/unidade?");
    }
    $("#popup_dialogo").dialog("open");
}

function editar(CodOrgao){
    $("#operacao").val("consultar");
    $("#CodOrgao").val(CodOrgao);
    formdata = $("#form1").serialize();
    submit_form(destino);
    $("#operacao").val("cadastrar");
    return false;
}

function excluir(CodOrgao){
    $("#operacao").val("excluir");
    $("#CodOrgao").val(CodOrgao);
    formdata = $("#form1").serialize();
    $("#msg_dialogo").html("<b>Esta operação não poderá ser desfeita.<br>Deseja continuar?</b>");
    $("#popup_dialogo").dialog("open");
    return false;
}

function cancelar(){
    $("#operacao").val("cadastrar");
    $("#CodOrgao").val("-1");
    $("#txt_nome").val("");
    $("#txt_sigla").val("");
    $("#txt_rhnet").val("");
    $("#txt_comprasnet").val("");
    $("#txt_siofinet").val("");
    $("#txt_sfinge").val("");

    $('#cb_controle').prop('checked', false);
}