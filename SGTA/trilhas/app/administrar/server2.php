<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../obj/conexao.php");
require_once("../../obj/grupos_trilhas.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    $objcon = new conexao();
    
    switch ($array["operacao"]) {
        case "load":                        //------------------------------------------------------------------------------------------------------
            
            $sql = "Select * From tipos_trilhas Where StatusTipo = 'ativo' Order By DescricaoTipo";
            $query = $objcon->select($sql);
            if($objcon->erro != ""){
                throw new Exception($objcon->erro);
            }
            
            $tipo = '<option value="0"></option>';
            
            while ($row = mysqli_fetch_array($query)){
                $tipo = $tipo.'<option value="'.$row['CodTipo'].'">'.$row['DescricaoTipo'].'</option>';
            }
            
            $array['areas'] = $tipo;
            
            gera_tabela();
            
            retorno();
            break;
        case "consultar":
            $CodGrupo = $_POST['CodGrupo'];
            
            $grupos = new grupos_trilhas();
            $query = $grupos->consultar_grupo($CodGrupo);
            if($grupos->erro != ""){
                throw new Exception($grupos->erro);
            }
            
            $array['trilha'] = mysqli_fetch_assoc($query);
            
            retorno();
            break;
        case "cadastrar":
        case "editar":
            $CodGrupo = $_POST['CodGrupo'];
            $CodTipo = $_POST['cmb_area'];
            $NomeTrilha = trim($_POST['txt_nome']);
            $TabelaDetalhe = trim($_POST['txt_plugin']);
            $CodigoOrgao = $_POST['cmb_codigo'];
            $TipoTrilha = $_POST['cmb_tipo'];
            
            if($CodTipo == 0){
                $array["alerta"] = $array["alerta"]."Selecione a área da trilha na lista.<br>";
            }
            
            if($CodigoOrgao == "0"){
                $array["alerta"] = $array["alerta"]."Selecione o padrão do código do órgão na lista.<br>";
            }
            
            if($NomeTrilha == ""){
                $array["alerta"] = $array["alerta"]."O nome da trilha preenchimento obrigatório.<br>";
            }
            
            if($TabelaDetalhe == ""){
                $array["alerta"] = $array["alerta"]."O nome do plugin é de preenchimento obrigatório.<br>";
            }
            
            if($TipoTrilha == "0"){
                $array["alerta"] = $array["alerta"]."Selecione o tipo da trilha na lista.";
            }

            if($array['alerta'] != ""){
                retorno();
            }
            
            $grupos = new grupos_trilhas();
            $grupos->cadastrar_grupo($CodGrupo, $CodTipo, $NomeTrilha, $TabelaDetalhe, $CodigoOrgao, $TipoTrilha);
            if($grupos->erro != ""){
                throw new Exception($grupos->erro);
            }
            
            gera_tabela();
            
            retorno();
            break;
        case "excluir":
            $CodGrupo = $_POST['CodGrupo'];
            
            $grupos = new grupos_trilhas();
            $grupos->excluir_grupo($CodGrupo);
            if($grupos->erro != ""){
                throw new Exception($grupos->erro);
            }
            
            gera_tabela();
            
            retorno();
            break;
        case "cadastrar_area":
            $DescricaoTipo = $_POST['popup_nome'];
            
            $grupos = new grupos_trilhas();
            $grupos->cadastrar_tipo($DescricaoTipo);
            if($grupos->erro != ""){
                throw new Exception($grupos->erro);
            }
            
            $sql = "Select * From tipos_trilhas Where StatusTipo = 'ativo' Order By DescricaoTipo";
            $query = $objcon->select($sql);
            if($objcon->erro != ""){
                throw new Exception($objcon->erro);
            }
            
            $tipo = '<option value="0"></option>';
            
            while ($row = mysqli_fetch_array($query)){
                $tipo = $tipo.'<option value="'.$row['CodTipo'].'">'.$row['DescricaoTipo'].'</option>';
            }
            
            $array['areas'] = $tipo;
            
            retorno();
            break;
        default:                            //------------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}

function gera_tabela(){
    try{
        global $array, $objcon;
        
            $sql = "SELECT tipos_trilhas.CodTipo, tipos_trilhas.DescricaoTipo, grupos_trilhas.CodGrupo, grupos_trilhas.NomeTrilha, 
                    grupos_trilhas.TabelaDetalhe, grupos_trilhas.CodigoOrgao, grupos_trilhas.TipoTrilha
                    FROM tipos_trilhas INNER JOIN grupos_trilhas ON tipos_trilhas.CodTipo = grupos_trilhas.CodTipo
                    ORDER BY NomeTrilha;";
            $query = $objcon->select($sql);
            if($objcon->erro != ""){
                throw new Exception($objcon->erro);
            }
            
            $tabela = "";
            
            while ($row = mysqli_fetch_array($query)){
                
                if($row['TipoTrilha'] == "pós"){
                    $TipoTrilha = "Pós-Pagamento";
                }else{
                    $TipoTrilha = "Pré-Pagamento";
                }
                
                $tabela = $tabela .
                '
                <tr>
                    <td>'.$row['DescricaoTipo'].'</td>
                    <td>'.$row['NomeTrilha'].'</td>
                    <td>'.$row['TabelaDetalhe'].'</td>
                    <td>'.$row['CodigoOrgao'].'</td>
                    <td>'.$TipoTrilha.'</td>
                    <td style="font-size: 130%; width: 60px;">
                        <a href="#" title="Editar Registro" class="especial" onclick="return editar('.$row['CodGrupo'].');"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href="#" title="Excluir Registro" class="excluir" onclick="return excluir('.$row['CodGrupo'].');"><span class="glyphicon glyphicon-remove-circle"></span></a>
                    </td>
                </tr>
                ';
            }
            
            $array['tabela'] = $tabela;
        
    } catch (Exception $exc){
        $array["erro"] = $exc->getMessage();
        retorno();
    }
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}