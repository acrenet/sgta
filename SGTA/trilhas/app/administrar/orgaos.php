<?php

    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if(($perfil != 1)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Cadastro de Unidades";
    
?>

<script src="orgaos.js" type="text/javascript"></script>
<br>

<div class="container">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4>Cadastro de Órgãos/Unidades</h4>
            </div>
            <div class="panel-body">
                
                <form id="form1" name="form1" method="POST" class="form-horizontal">
                                                      
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cpf">Nome do Órgão/Unidade:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="txt_nome" name="txt_nome" maxlength="255" placeholder="" value="" style="text-transform:uppercase;">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cpf">Sigla do Órgão/Unidade:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="txt_sigla" name="txt_sigla" maxlength="30" placeholder="" value="" style="text-transform:uppercase;">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cpf">Código RHNet:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="txt_rhnet" name="txt_rhnet" maxlength="20" placeholder="" value="">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cpf">Código ComprasNet:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="txt_comprasnet" name="txt_comprasnet" maxlength="30" placeholder="" value="">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cpf">Cód. Unidade(Sfinge):</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="txt_siofinet" name="txt_siofinet" maxlength="30" placeholder="" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cpf">Código Sfinge:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="txt_sfinge" name="txt_sfinge" maxlength="30" placeholder="" value="">
                        </div>
                    </div>


                    <div class="form-group"> 
                        <div class="col-sm-offset-4 col-sm-8">
                            <div class="checkbox">
                                <label><input type="checkbox" name="cb_controle" id="cb_controle"> Unidade de Controle?</label>
                            </div>            
                        </div>
                    </div>
                    
                    <div class="form-group"> 
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="button" class="btn btn-danger" onclick="return cancelar();" id="btn_cancelar">Cancelar &nbsp;&nbsp;<i class="fa fa-mail-reply fa-lg"></i></button>
                            <button type="button" class="btn btn-primary" onclick="return salvar();">Salvar &nbsp;&nbsp;<i class="fa fa-database fa-lg"></i></button>
                        </div>
                    </div>
                    
                    <input type="hidden" name="operacao" id="operacao" value="load" />
                    <input type="hidden" name="CodOrgao" id="CodOrgao" value="" />
                </form>
                
                <table class="table table-bordered table-condensed table-hover table-striped" style="font-size: 80%;">
                    <thead>
                        <tr>
                            <th>Nome do Órgão/Unidade</th>
                            <th>Sigla</th>
                            <th>RHNet</th>
                            <th>ComprasNet</th>
                            <th>Cod. Unidade</th>
                            <th>Sfinge</th>
                            <th>Controle</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="tb_orgaos">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
    // pagemaincontent recebe o conteudo do buffer
    $pagemaincontent = ob_get_contents(); 

    // Descarta o conteudo do Buffer
    ob_end_clean(); 

    //Include com o Template
    include("../../master/master.php");
    include('../../master/datatable.php');