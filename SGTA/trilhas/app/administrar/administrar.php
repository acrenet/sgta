<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $cpf = $_SESSION['sessao_id'];
    $orgao = $_SESSION['sessao_orgao'];
    $perfil = $_SESSION['sessao_perfil'];
    
    if(!($perfil == 1 || $perfil == 2 || $perfil == 5)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Trilhas - Administrar Usuários";
?>

<script src="administrar_js.js" type="text/javascript"></script>
<br>
<div class="container">
    
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Relação de Usuários.</h4></div>
            <div class="panel-body">
                
                <div class="table-responsive">
                    <table id="tbl_usuarios"  class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>CPF</th>
                                <th>Nome</th>
                                <th>Órgão</th>
                                <th>Perfil</th>
                                <th>Áreas</th>
                                <th>Dt.Acesso</th>
                                <th>Status</th>
                                <th style="width: 50px;"></th>
                            </tr>
                        </thead>
                        <tbody id="tb_usuarios">
                            
                        </tbody>
                        <tfoot>
                            <tr style="display: none;">
                                <th>CPF</th>
                                <th>Nome</th>
                                <th>Órgão</th>
                                <th>Perfil</th>
                                <th>Dt.Acesso</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <form id="form1" class="form-horizontal" role="form" action="../usuario/usuario.php" method="POST">
                    <input type="hidden" name="operacao" id="operacao" value="consultar" />
                    <input type="hidden" name="cpf" id="cpf" value="" />
                </form>
            </div>
        </div>
    </div>            
</div>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');