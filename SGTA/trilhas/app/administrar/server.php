<?php
$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");
require_once("../../obj/usuario.php");
require_once("../../obj/autorizacoes.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

$obj_usuario = new usuario();
$obj_autorizacoes = new autorizacoes();

try {
    
    switch ($array["operacao"]) {
        case "consultar":                   //----------------------------------------------------------------------------------------------
            
            $CodOrgao = $_SESSION['sessao_orgao'];
            $CodPerfil = $_SESSION['sessao_perfil'];
            
            if($CodPerfil == 5){
                $obj_usuario->consulta_usuarios($CodOrgao);
            }else{
                $obj_usuario->consulta_usuarios(-1);
            }
            if($obj_usuario->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
            $query = $obj_usuario->query;
            
            $query2 = $obj_autorizacoes->consulta_todas();
            if($obj_autorizacoes->erro != ""){
                $array['erro'] = $obj_autorizacoes->erro;
                retorno();
            }
            while ($row = mysqli_fetch_assoc($query2)){
                $aut[] = $row;
            }
            
            $array['html'] = "";
            
            while ($row = mysqli_fetch_array($query)){
                
                $autorizacoes = "";
                for ($index = 0; $index < count($aut); $index++){
                    if($row['CPF'] == $aut[$index]['CPF']){
                        if($autorizacoes == ""){
                            $autorizacoes = "- ".$aut[$index]['DescricaoTipo'];
                        }else{
                            $autorizacoes = $autorizacoes."<br>- ".$aut[$index]['DescricaoTipo'];
                        }
                    }
                }
                
                $array['html'] = $array['html'] .
                    '<tr>
                        <td>'.$row['CPF'].'</td>
                        <td>'.$row['Nome'].'</td>
                        <td><a href="#" onclick="return filtrar_tabela(\'tbl_usuarios\', \''.$row['SiglaOrgao'].'\')" >'.$row['SiglaOrgao'].'</a></td>
                        <td><a href="#" onclick="return filtrar_tabela(\'tbl_usuarios\', \''.$row['NomePerfil'].'\')" >'.$row['NomePerfil'].'</a></td>
                        <td>'.$autorizacoes.'</td>    
                        <td>'.data_br($row['UltimoAcesso']).'</td>    
                        <td><a href="#" onclick="return filtrar_tabela(\'tbl_usuarios\', \''.$row['Status'].'\')" >'.$row['Status'].'</a></td>
                        <td>
                            <a href="#" title="Consultar Registro" onclick="return consultar(\''.$row['CPF'].'\');" class="consultar">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>&nbsp;
                            <a href="#" title="Editar Registro" onclick="return editar(\''.$row['CPF'].'\');" class="editar">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>&nbsp;
                            <a href="#" title="Ver Permissões" onclick="return permissoes(\''.$row['CPF'].'\');" class="especial">
                                <span class="glyphicon glyphicon-check"></span>
                            </a>
                        </td>
                    </tr>';
            }
            
            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}


function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}