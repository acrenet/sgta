var execute_cod = 0;
var destino = "server.php";

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "trocar_senha":
                execute_cod = 1;
                $('#popup_form').dialog("close");
                $("#msg_sucesso").html(retorno.resposta);
                $("#popup_sucesso").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../inicio/inicio.php");
    }
    execute_cod = 0;
}

function alterar(){
    var nova_senha = $("#nova_senha").val();
    var repetir = $("#nova_senha_2").val();
    
    if(nova_senha != repetir){
        $("#msg_alerta").html("A repetição da nova senha não confere.");
        $("#popup_alerta").dialog("close");
        return false;
    }
        
    formdata = $("#form1").serialize();
    $("#popup_dialogo").dialog("open");
    return false;
}



