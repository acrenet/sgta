<?php
require_once("../../obj/usuario.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

$obj_usuario = new usuario();

try {
    
    switch ($array["operacao"]) {
        case "trocar_senha":                   //------------------------------------------------------------------------------------------------------
            $usuario = $_POST['cpf'];
            $senha = $_POST['senha'];
            $nova_senha = $_POST['nova_senha'];

            if($usuario == ""){
                $array['alerta'] = "CPF Inválido.";
                retorno();
            }
            
            $obj_usuario->consulta_usuario($usuario);
            if($obj_usuario->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
            $query = $obj_usuario->query;
            
            if(mysqli_num_rows($query) == 0){
                $array['alerta'] = "Usuário não cadastrado.";
                retorno();
            }

            $row = mysqli_fetch_array($query);
            
            if(md5($senha) != $row["Senha"]){
                $array['alerta'] = "A senha não confere.";
                retorno();
            }
            
            if(strlen($nova_senha) < 6){
                $array['alerta'] = "Senha muito curta, o tamanho mínimo são 6 caracteres.";
                retorno();
            }

            $texto = "01234567890123450000001111112222223333334444445555556666667777778888889999990000000987654321098765aaaaaabbbbbbasdfghqwertyzxcvbn      ";

            if (strpos($texto, $nova_senha) !== false) {
                $array['alerta'] = "Esta senha é considerada muito fácil.";
                retorno();
            }
            
            $obj_usuario->trocar_senha($usuario, $nova_senha);
            if($obj_usuario->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
            
            $array['resposta'] = "Senha alterada com sucesso.";
            
            retorno();
            break;
        default:                        //------------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}


function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}