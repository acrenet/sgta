<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
?>

<script src="senha_js.js" type="text/javascript"></script>
<br>
<div class="container">
    
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Trocar de Senha.</h4></div>
            <div class="panel-body">
                <form id="form1" class="form-horizontal" role="form">


                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cpf">CPF:</label>
                        <div class="col-sm-4">
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="senha">Senha:</label>
                        <div class="col-sm-4"> 
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="senha">Nova Senha:</label>
                        <div class="col-sm-4"> 
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                <input type="password" class="form-control" id="nova_senha" name="nova_senha" placeholder="Nova Senha">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="senha">Repita a Nova Senha:</label>
                        <div class="col-sm-4"> 
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                <input type="password" class="form-control" id="nova_senha_2" name="nova_senha_2" placeholder="Repita a Nova Senha">
                            </div>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="button" class="btn btn-primary" onclick="return alterar();">Alterar &nbsp;&nbsp;<i class="fa fa-repeat fa-lg"></i></button>
                        </div>
                    </div>

                    <input type="hidden" name="operacao" id="operacao" value="trocar_senha" />

                </form>
            </div>
        </div>
    </div>            
</div>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");