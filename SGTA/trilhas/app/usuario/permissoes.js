var execute_cod = 0;
var destino = "server.php";

$(document).ready(function(){
    
    formdata = $("#form1").serialize();
    submit_form(destino);
    
});

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "ver_permissoes":
                $( "#servidor" ).html("SERVIDOR: " + retorno.nome);
                $( "#sortable1" ).html(retorno.autorizado);
                $( "#sortable2" ).html(retorno.negado);
                
                $( "#sortable1, #sortable2" ).sortable({
                    connectWith: ".connectedSortable",
                    placeholder: "ui-state-highlight",
                    cursor: "move",
                    revert: true,
                    receive: function( event, ui ) {
                        var sortedIDs = $( "#sortable2" ).sortable( "toArray" );
                        $("#permitidos").val(JSON.stringify(sortedIDs));
                        $("#operacao").val("permissoes");
                        formdata = $("#form1").serialize();
                        submit_form(destino);
                    }
                }).disableSelection();
                
                break;
            case "permissoes":
                
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../administrar/administrar.php");
    }
    execute_cod = 0;
}