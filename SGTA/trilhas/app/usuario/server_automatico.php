<?php

$__servidor = true;
$operacao = "";

require_once("../../code/funcoes.php");
require_once("../../obj/usuario.php");
require_once("../../obj/orgaos.php");
require_once("../../obj/cargos.php");
require_once("../../obj/vinculo.php");
require_once("../../obj/funcoes.php");
require_once("../../obj/autorizacoes.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

$obj_orgaos = new orgaos();
$obj_usuario = new usuario();
$obj_cargo = new cargos();
$obj_vinculo = new vinculo();
$obj_funcao = new funcoes();
$obj_autorizacoes = new autorizacoes();

//$cpf = $_SESSION['sessao_id'];
//$orgao = $_SESSION['sessao_orgao'];
//$perfil = $_SESSION['sessao_perfil'];

$obj_orgao = new orgaos();

$unidade_controle = $obj_orgao->consulta_unidade_controle();

if($obj_orgao->erro != ""){
    $array['erro'] = $obj_orgao->erro;
    retorno();
}

if(!isset($_SESSION)){
    session_start();
}


$_SESSION['unidade_controle'] = $unidade_controle;

try {
    
    switch ($array["operacao"]) {

        case "verifica_cpf":   
        
        $obj_usuario->consultaCpfSgi($_POST['cpf']);   
        if($obj_usuario->erro != ""){
            $array['erro'] = $obj_usuario->erro;
            retorno();
        }          
       
        $array['resultado'] = $obj_usuario->query->num_rows;
        retorno();
        break;

        case "load":                        //------------------------------------------------------------------------------------------------------

            carrega_dados();
            
            retorno();
            break;
        case "editar":
        case "consultar":                   //------------------------------------------------------------------------------------------------------
            
            carrega_dados();
            
            $cpf_antigo = $_POST['cpf_antigo'];
            $orgao = $_POST['orgao'];
            $perfil = $_POST['perfil'];
            
            $obj_usuario->consulta_usuario($cpf_antigo);
            if($obj_usuario->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
            $query = $obj_usuario->query;
            $row = mysqli_fetch_array($query);
            $array['cpf'] = $row['CPF'];
            $array['nome'] = $row['Nome'];
            $array['email'] = $row['Email'];
            $array['ccargo'] = $row['CodCargo'];
            $array['corgao'] = $row['CodOrgao'];
            $array['cvinculo'] = $row['CodVinculo'];
            $array['cperfil'] = $row['CodPerfil'];
            $array['cfuncao'] = $row['CodFuncao'];
            $array['telefone'] = $row['Telefone'];
            $array['lotacao'] = $row['Lotacao'];
            
            $array['cperfil'] = base64_encode($array['cperfil'] * 399);
            
            if($row['Status'] == 'ativo'){
                $array['status'] = true;
            }else{
                $array['status'] = false;
            }
            
            retorno();
            break;
        case "cadastrar":                   //------------------------------------------------------------------------------------------------------
            $cpf = $_POST['cpf'];
            $nome = $_POST['nome'];
            $email = $_POST['email'];
            $CodOrgao = $_POST['orgao'];
            $CodPerfil = $_POST['perfil'];
            $CodCargo = $_POST['cargo'];
            $CodVinculo = $_POST['vinculo'];
            $CodFuncao = $_POST['funcao'];
            $lotacao = $_POST['lotacao'];
            $telefone = $_POST['telefone'];
            $email_geral = $_POST['email_geral'];
            
          //  $CodPerfil = base64_decode($CodPerfil)/399;
            
            if(isset($_POST['status'])){
                $status = 'ativo';
            }else{
                $status = 'bloqueado';
            }
            
            $cpf = trim($cpf);
            $nome = trim($nome);
            $lotacao = trim($lotacao);
            $email = trim($email);
            
            if($cpf == ""){
               $array['alerta'] = $array['alerta']."<li>CPF inválido.</li>";
            }
            if($nome == ""){
               $array['alerta'] = $array['alerta']."<li>Nome inválido.</li>";
            }
            
            if($CodCargo == -1){
                $array['alerta'] = $array['alerta']."<li>Cargo inválido.</li>";
            }
            
            if($CodVinculo == -1){
                $array['alerta'] = $array['alerta']."<li>Vínculo inválido.</li>";
            }
            
            if($CodFuncao == -1){
                $array['alerta'] = $array['alerta']."<li>Função inválida.</li>";
            }
            
            if($CodOrgao == -1){
                $array['alerta'] = $array['alerta']."<li>Órgão inválido.</li>";
            }
            
            if($CodPerfil == -1){
                $array['alerta'] = $array['alerta']."<li>Perfil inválido.</li>";
            }
            
            if($CodPerfil <= 4 && $CodPerfil > 0 && $CodOrgao != $unidade_controle){
                $array['alerta'] = $array['alerta']."<li>Órgão inválido para o perfil selecionado.<br>O perfil selecionado é exclusivo para servidores do TCE.</li>";
            }
            
            if($lotacao == ""){
               $array['alerta'] = $array['alerta']."<li>Lotação inválida.</li>";
            }
            
            if(!valida_email($email)){
                $array['alerta'] = $array['alerta']."<li>E-mail inválido.</li>";
            }

            if(!valida_email($email_geral)){
                $array['alerta'] = $array['alerta']."<li>E-mail Geral inválido.</li>";
            }
            
            if($array['alerta'] != ""){
                $array['alerta'] = "As seguintes pendências impedem a gravação:<br><br><ul>".$array['alerta']."</ul>";
                retorno();
            }
            
            
            $obj_usuario->cadastra_usuario($cpf, $nome, $telefone, $email, $lotacao, $status, $CodCargo, $CodOrgao, $CodVinculo, $CodPerfil, $CodFuncao, $email_geral);
            if($obj_usuario->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
          
            $array['retorno'] = "Usuário cadastrado com sucesso.";

            retorno();
            break;
        case "alterar":         //----------------------------------------------------------------------------------------------------------
            $novo_cpf = $_POST['cpf'];
            $cpf = $_POST['cpf_antigo'];
            $nome = $_POST['nome'];
            $email = $_POST['email'];
            $CodOrgao = $_POST['orgao'];
            $CodPerfil = $_POST['perfil'];
            $CodCargo = $_POST['cargo'];
            $CodVinculo = $_POST['vinculo'];
            $CodFuncao = $_POST['funcao'];
            $lotacao = $_POST['lotacao'];
            $telefone = $_POST['telefone'];
            
            $CodPerfil = base64_decode($CodPerfil)/399;
            
            $novo_cpf = trim($novo_cpf);
            $nome = trim($nome);
            $email = trim($email);
            
            if(isset($_POST['status'])){
                $status = 'ativo';
            }else{
                $status = 'bloqueado';
            }
            
            $cpf = trim($cpf);
            $nome = trim($nome);
            $lotacao = trim($lotacao);
            $email = trim($email);
            
            if($cpf == ""){
               $array['alerta'] = $array['alerta']."<li>CPF inválido.</li>";
            }
            if($nome == ""){
               $array['alerta'] = $array['alerta']."<li>Nome inválido.</li>";
            }
            
            if($CodCargo == -1){
                $array['alerta'] = $array['alerta']."<li>Cargo inválido.</li>";
            }
            
            if($CodVinculo == -1){
                $array['alerta'] = $array['alerta']."<li>Vínculo inválido.</li>";
            }
            
            if($CodFuncao == -1){
                $array['alerta'] = $array['alerta']."<li>Função inválida.</li>";
            }
            
            if($CodOrgao == -1){
                $array['alerta'] = $array['alerta']."<li>Órgão inválido.</li>";
            }
            
            if($CodPerfil == -1){
                $array['alerta'] = $array['alerta']."<li>Perfil inválido.</li>";
            }
            
            if($CodPerfil <= 4 && $CodPerfil > 0 && $CodOrgao != $unidade_controle){
                $array['alerta'] = $array['alerta']."<li>Órgão inválido para o perfil selecionado.<br>O perfil selecionado é exclusivo para servidores do TCE.</li>";
            }
            
            if($lotacao == ""){
               $array['alerta'] = $array['alerta']."<li>Lotação inválida.</li>";
            }
            
            if(!valida_email($email)){
                $array['alerta'] = $array['alerta']."<li>E-mail inválido.</li>";
            }
            
            if($array['alerta'] != ""){
                $array['alerta'] = "As seguintes pendências impedem a gravação:<br><br><ul>".$array['alerta']."</ul>";
                retorno();
            }
            
            $obj_usuario->altera_cadastro($cpf, $novo_cpf, $nome, $email, $status, $CodCargo, $CodOrgao, $CodVinculo, $CodPerfil, $CodFuncao, $telefone, $lotacao);
            if($obj_usuario->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
          
            $array['retorno'] = "Dados gravados com sucesso.";

            retorno();
            break;
        case "ver_permissoes":
            $cpf = $_POST['cpf'];
            $cpfsessao = $_SESSION['sessao_id'];
            
            $obj_usuario->consulta_usuario($cpf);
            if($obj_usuario->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
            
            $query = $obj_usuario->query;
            
            $row = mysqli_fetch_array($query);
            $array['nome'] = $row['Nome'];
            
            $permissoes = $obj_autorizacoes->autorizacoes_usuario($cpf);
            if($obj_autorizacoes->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
            
            $permissaosessao = $obj_autorizacoes->autorizacoes_usuario($cpfsessao);
            if($obj_autorizacoes->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
            
            $ativo = "";
            $bloqueado = "";
            
            for ($index = 0; $index < count($permissoes); $index++){
                if($permissoes[$index]['permitido'] == false){
                    
                    $exibir = false;
                    
                    for ($index1 = 0; $index1 < count($permissaosessao); $index1++){
                        if($permissaosessao[$index1]["CodTipo"] == $permissoes[$index]["CodTipo"] && $permissaosessao[$index1]["permitido"] == true){
                            $exibir = true;
                        }
                    }
                    
                    if($_SESSION['sessao_perfil'] == 1){
                        $exibir = true;
                    }
                    
                    if($exibir == true){
                        $ativo = $ativo.'<li id="'.$permissoes[$index]['CodTipo'].'" class="alert alert-danger">'.$permissoes[$index]['DescricaoTipo'].'</li>';
                    }
                    
                }else{

                    $bloqueado = $bloqueado.'<li id="'.$permissoes[$index]['CodTipo'].'" class="alert alert-success">'.$permissoes[$index]['DescricaoTipo'].'</li>';
                    
                }
            }
            
            $array['autorizado'] = $ativo;
            $array['negado'] = $bloqueado;
            
            retorno();
            break;
        case "permissoes":
           // die(var_dump($_POST['permitidos']));
            $permitidos = $_POST['permitidos'];
            $cpf = $_POST['cpf'];
            
            $obj_autorizacoes->autoriza_usuarios($permitidos, $cpf);
            if($obj_autorizacoes->erro != ""){
                $array['erro'] = $obj_autorizacoes->erro;
                retorno();
            }
            
            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}

function carrega_dados(){
    global $array, $obj_orgaos, $obj_cargo, $obj_vinculo, $obj_funcao, $orgao, $perfil;
    
    if($perfil == 1){ //Administrador
        $html = '<option value="'.base64_encode(-1 * 399).'"></option>
                 <option value="'.base64_encode(1 * 399).'">Administrador</option>
                 <option value="'.base64_encode(2 * 399).'">Gestor</option>
                 <option value="'.base64_encode(3 * 399).'">Auditor</option>
                 <option value="'.base64_encode(4 * 399).'">Consulta TCE</option>
                 <option value="'.base64_encode(5 * 399).'">Supervisor</option>
                 <option value="'.base64_encode(6 * 399).'">Analista</option>
                 <option value="'.base64_encode(7 * 399).'">Consulta Órgão</option>
                 <option value="'.base64_encode(8 * 399).'">Upload de Arquivos Somente</option>    
                ';
        $obj_orgaos->consulta_orgaos(-1);

    }elseif($perfil == 2){ //Gestor
        $html = '<option value="'.base64_encode(-1 * 399).'"></option>
                 <option value="'.base64_encode(3 * 399).'">Auditor</option>
                 <option value="'.base64_encode(4 * 399).'">Consulta TCE</option>
                 <option value="'.base64_encode(2 * 399).'">Gestor</option>    
                 <option value="'.base64_encode(5 * 399).'">Supervisor</option>
                 <option value="'.base64_encode(8 * 399).'">Upload de Arquivos Somente</option>    
                ';
        $obj_orgaos->consulta_orgaos(-1);
    }elseif($perfil == 5){ //Supervisor
        $html = '<option value="'.base64_encode(-1 * 399).'"></option>
                 <option value="'.base64_encode(6 * 399).'">Analista</option>
                 <option value="'.base64_encode(7 * 399).'">Consulta Órgão</option> 
                 <option value="'.base64_encode(5 * 399).'">Supervisor</option>
                ';
        $obj_orgaos->consulta_orgaos($orgao);
    }

    $array['perfil'] = $html;

    if($obj_orgaos->erro != ""){
        $array['erro'] = $obj_orgaos->erro;
        retorno();
    }

    $query = $obj_orgaos->query;

    $html = '<option value="-1"></option>';

    if(mysqli_num_rows($query) > 0){
        while ($row = mysqli_fetch_array($query)){
            $html = $html.'<option value="'.$row['CodOrgao'].'">'.$row['NomeOrgao'].'</option>';
        }
    }

    $array['orgao'] = $html;

    $obj_cargo->consulta_cargos(-1);
    if($obj_cargo->erro != ""){
        $array['erro'] = $obj_cargo->erro;
        retorno();
    }
    $query = $obj_cargo->query;

    $html = '<option value="-1"></option>';

    if(mysqli_num_rows($query) > 0){
        while ($row = mysqli_fetch_array($query)){
            $html = $html.'<option value="'.$row['CodCargo'].'">'.$row['NomeCargo'].'</option>';
        }
    }

    $array['cargo'] = $html;

    $obj_vinculo->consulta_vinculos(-1);
    if($obj_vinculo->erro != ""){
        $array['erro'] = $obj_vinculo->erro;
        retorno();
    }
    $query = $obj_vinculo->query;

    $html = '<option value="-1"></option>';

    if(mysqli_num_rows($query) > 0){
        while ($row = mysqli_fetch_array($query)){
            $html = $html.'<option value="'.$row['CodVinculo'].'">'.$row['NomeVinculo'].'</option>';
        }
    }

    $array['vinculo'] = $html;

    $obj_funcao->consulta_funces(-1);
    if($obj_funcao->erro != ""){
        $array['erro'] = $obj_funcao->erro;
        retorno();
    }
    $query = $obj_funcao->query;

    $html = '<option value="-1"></option>';

    if(mysqli_num_rows($query) > 0){
        while ($row = mysqli_fetch_array($query)){
            $html = $html.'<option value="'.$row['CodFuncao'].'">'.$row['DescricaoFuncao'].'</option>';
        }
    }

    $array['funcao'] = $html;
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}