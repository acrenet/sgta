var execute_cod = 0;
var destino = "server.php";

$(document).ready(function(){
    
    if($("#operacao").val() == "load"){
        $("#btn_retornar").hide();
    }
    
    formdata = $("#form1").serialize();
    submit_form(destino);
    
});

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                $("#perfil").html(retorno.perfil);
                $("#orgao").html(retorno.orgao);
                $("#cargo").html(retorno.cargo);
                $("#vinculo").html(retorno.vinculo);
                $("#funcao").html(retorno.funcao);
                $("#operacao").val("cadastrar");
                break;
            case "consultar":
                $("#form1 :input").attr("readonly", true);
                $("#form1 :input").attr("disabled", true);
                $("#form1 :text").attr("disabled", false);
                $("#btn_retornar").attr("disabled", false);
            case "editar":
                $("#perfil").html(retorno.perfil);
                $("#orgao").html(retorno.orgao);
                $("#cargo").html(retorno.cargo);
                $("#vinculo").html(retorno.vinculo);
                $("#funcao").html(retorno.funcao);
                
                $("#cpf").val(retorno.cpf);
                $("#cpf_antigo").val(retorno.cpf);
                $("#nome").val(retorno.nome);
                $("#email").val(retorno.email);
                $("#perfil").val(retorno.cperfil);
                $("#orgao").val(retorno.corgao);
                $("#cargo").val(retorno.ccargo);
                $("#vinculo").val(retorno.cvinculo);
                $("#funcao").val(retorno.cfuncao);
                $("#telefone").val(retorno.telefone);
                $("#lotacao").val(retorno.lotacao);
                $("#status").attr("checked", retorno.status);
                $("#operacao").val("alterar");
                break;
            case "cadastrar":
                execute_cod = 1;
                $("#msg_sucesso").html(retorno.retorno);
                $("#popup_sucesso").dialog("open");
                break;
            case "alterar": 
                execute_cod = 2;
                $("#msg_sucesso").html(retorno.retorno);
                $("#popup_sucesso").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            document.getElementById("form1").action = "permissoes.php";
            document.getElementById("form1").submit();
            break;
        case 2:
            window.location = "../administrar/administrar.php";
            break;
    }
    execute_cod = 0;
}

function salvar(){
    formdata = $("#form1").serialize();
    $("#popup_dialogo").dialog("open");
}

