<?php

    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    
    $orgao = $_SESSION['sessao_orgao'];
    $perfil = $_SESSION['sessao_perfil'];
    
    if(!($perfil == 1 || $perfil == 2 || $perfil == 5)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $operacao = "ver_permissoes";
    
    $cpf = $_POST['cpf'];
    
?>
  
<link href="permissoes.css" rel="stylesheet" type="text/css"/>
<script src="permissoes.js" type="text/javascript"></script>
<br>
<div class="container">
    
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Permissões do Usuário.</h4></div>
            <div class="panel-body">
                
                <div id="servidor" class="well well-sm" style="font-weight: bold;"></div>
                
                <P style="text-align: center;">Arraste os itens entre as listas para autorizar ou desautorizar o acesso.</P>
                
                <div class="row">
                    <div class="col-sm-6" style="text-align: center; font-weight: bold;">
                        ÁREAS DE TRILHAS DISPONÍVEIS
                    </div>
                    <div class="col-sm-6" style="text-align: center; font-weight: bold;">
                        ÁREAS DE TRILHAS AUTORIZADAS
                    </div>
                </div> 
                
                <div class="row">
                    <div class="col-sm-6">
                        <ul id="sortable1" class="connectedSortable">
                            
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul id="sortable2" class="connectedSortable">
                            
                        </ul>
                    </div>
                </div> 
                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <a class=" btn btn-info" href="../administrar/administrar.php"><span class="fa fa-mail-reply"></span> Retornar</a>
                    </div>
                </div>
                
                <form id="form1" class="form-horizontal" role="form">
                    <input type="hidden" name="operacao" id="operacao" value="<?php echo $operacao; ?>" />
                    <input type="hidden" name="permitidos" id="permitidos" value=""/>
                    <input type="hidden" name="cpf" id="cpf" value="<?php echo $cpf; ?>" />
                </form>
                
            </div>
        </div>
    </div>            
</div>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");