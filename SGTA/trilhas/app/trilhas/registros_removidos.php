<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if($perfil != 1){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $operacao = "load_registros_removidos";
    $CodTrilha = $_POST['CodTrilha'];
    $detalhe = $_REQUEST['detalhe'];
     
  
?>
<script src="../../js/datepicker.js" type="text/javascript"></script>
<script src="registros_removidos.js" type="text/javascript"></script>
<br>
<div class="container">
    <div class="panel-group" >
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Registros excluidos da trilha</h4></div>
            <div class="panel-body">
                <table class="table table-bordered table-striped table-hover table-condensed display">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>CPF_CNPJ_Proc</th>
                            <th>Nome_Descricao</th>
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Motivo Exclusão</th>
                            <th style="width: 137px;">Ações</th>
                        </tr>
                    </thead>
                    <tbody id="tabela">
 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
        <div class="panel-group" style="visibility: hidden;">
            <div class="panel panel-primary">
                <div class="panel-heading"><h4>Registros Removidos</h4></div>
                <div class="panel-body">
                    
                    <form id="form1" class="form-horizontal" role="form" method="post">

                        <input type="hidden" name="operacao" id="operacao" value="load_registros_removidos" />
                        <input type="hidden" name="detalhe" id="detalhe" value="<?php echo $detalhe; ?>" />
                        <input type="hidden" name="CodTrilha" id="CodTrilha" value="<?php echo $CodTrilha; ?>" />
                        <input type="hidden" name="CodRegistro" id="CodRegistro" value="" />
                        <input type="hidden" name="CodAnexo" id="CodAnexo" value="" />
                    </form>   
                </div>
            </div>
        </div>
</div>


<form name="form2" id="form2" action="../registros/exibir.php" method="POST">
    <input type="hidden" name="operacao2" id="operacao2" value="" />
    <input type="hidden" name="operacao" id="operacao" value="" />
    <input type="hidden" name="Orgao" id="Orgao" value="" />
    <input type="hidden" name="CodTrilha" id="CodTrilha" value="" />
    <input type="hidden" name="filtro" id="filtro" value="" />
    <input type="hidden" name="NomeTrilha" id="NomeTrilha" value="" />
    <input type="hidden" name="detalhe" id="detalhe" value="" />
    <input type="hidden" name="acao" id="acao" value="consulta" />
    
</form>


<?php
    
  include("form_upload/form_upload.php");

  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");