<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array("operacao" => $operacao,"erro" => "","alerta" => "A sessão expirou, será necessário realizar um novo login.","noticia" => "","retorno" => "");
    retorno();
}
require_once("../../code/funcoes.php");

require_once("../../obj/trilhas.php");
require_once("../../obj/registros.php");

$obj_trilhas = new trilhas();
$obj_registros = new registros();

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$cpf = $_SESSION['sessao_id'];

$array = array("operacao" => $operacao,"erro" => "","alerta" => "","noticia" => "","retorno" => "");

try {
    
    switch ($array["operacao"]) {
        case "load":                    //--------------------------------------------------------------------------------------------------

            $CodPerfil = $_POST['CodPerfil'];
            $CodOrgao = $_POST['CodOrgao'];
            $CPF =$_POST['CPF'];
            $SelecionarOrgao = $_POST['SelecionarOrgao'];
            $acao = $_POST['acao'];
            $CodUnidade = $_POST['CodUnidade'];
            
            require_once("../../obj/orgaos.php");
            $obj_orgaos = new  orgaos();
            
            $array['SelecionarOrgao'] = $SelecionarOrgao;
            if($SelecionarOrgao == 1){
                $html = '<option value="0">TODAS</option>';
                $obj_orgaos->consulta_orgaos(-1);
            }else{
                $html = '';
                $obj_orgaos->consulta_orgaos($CodOrgao);
                $CodUnidade = $CodOrgao;
            }
            if($obj_orgaos->erro != ""){
                $array['erro'] = $obj_orgaos->erro;
                retorno();
            }
            $query = $obj_orgaos->query;
            while ($row = mysqli_fetch_array($query)){
                if($row['NomeOrgao'] != "NENHUM"){
                    $html = $html.'<option value="'.$row['CodOrgao'].'">'.$row['NomeOrgao'].'</option>';
                }
            }
            $array['orgaos'] = $html;
            
            gera_tabela();
            
            fim:
            retorno();
            break;
        case "consulta_detalhe":
            $CodTrilha = base64_decode($_POST['CodTrilha']) / 2999;
            
            $obj_trilhas->consulta_trilha($CodTrilha);
            if($obj_trilhas->erro != ""){
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }
            $query = $obj_trilhas->query;
            
            $row = mysqli_fetch_array($query);
            
            $array['detalhe'] = $row['TabelaDetalhe'];
            $array['NomeTrilha'] = $row['NomeTrilha'];
            
            retorno();
            break;
        case "escolher_trilhas":
            $acao = $_POST['acao'];
            $CodUnidade = $_POST['CodUnidade'];
            
            gera_tabela();
            
            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}

function gera_tabela(){
    try{
        global $array, $acao, $obj_trilhas, $obj_registros, $CodUnidade, $cpf, $prazo_legal_pos, $prazo_legal_pre;
        
        $CodPerfil = $_SESSION['sessao_perfil'];
        
        require_once("../../obj/usuario.php");
        $obj_usuarios = new usuario();

        if($acao == "pré-análise"){
            $Where = "Where StatusTrilha = 'pré-análise' Order By CodTrilha Desc";
        }else if($acao == "análise"){
            $Where = "Where StatusTrilha <> 'pendente' And StatusTrilha <> 'pré-análise' Order By CodTrilha Desc";
        }else if($acao == "consulta"){
            $Where = "Where StatusTrilha <> 'pendente' And StatusTrilha <> 'pré-análise' Order By CodTrilha Desc";
        }else if($acao == "validação"){
            $Where = "Where StatusTrilha <> 'pendente' And StatusTrilha <> 'pré-análise' Order By CodTrilha Desc";
        }
        
        
        
        $obj_usuarios->consulta_autorizacoes($cpf);
        if($obj_usuarios->erro != ""){
            $array['erro'] = $obj_usuarios->erro;
            retorno();
        }
        $autorizacoes = $obj_usuarios->array;
        
        $obj_trilhas->consulta_trilhas($Where);
        if($obj_trilhas->erro != ""){
            $array['erro'] = $obj_trilhas->erro;
            retorno();
        }
        $query = $obj_trilhas->query;
        
        $html = "";
        
        
        
        if(mysqli_num_rows($query) > 0){
            while( $row = mysqli_fetch_assoc($query)){
                $trilhas[] = $row;
            }

            for ($index = 0; $index < count($trilhas); $index++){

                $CodTrilha = $trilhas[$index]['CodTrilha'];

                $qtd = $obj_registros->contar_registros($CodTrilha, $CodUnidade);
                if($obj_registros->erro != ""){
                    $array['erro'] = $obj_registros->erro;
                    retorno();
                }
                
                if($CodUnidade == 0){
                    $status = $trilhas[$index]['StatusTrilha'];
                }else{
                    if($acao != "pré-análise"){
                        $obj_trilhas->verifica_status($CodTrilha, $CodUnidade);
                        if($obj_trilhas->erro != ""){
                            throw new Exception($obj_trilhas->erro);
                        }
                    }   
                    $status = $obj_trilhas->consulta_status_orgao($CodTrilha, $CodUnidade);
                }
                
                switch ($status) {
                    case "distribuído":
                        $cor = "darkorange";
                        break;
                    case "justificado":
                        $cor = "darkblue";
                        break;
                    case "concluído":
                        $cor = "darkgreen";
                        break;
                    default :
                        $cor = "black";
                }
                
                $autorizado = false;
                
              
                
                for ($index1 = 0; $index1 < count($autorizacoes); $index1++){
                    
                    if(isset($trilhas[$index]['CodTipo']) && isset($autorizacoes[$index1]["CodTipo"])){
                        if($trilhas[$index]['CodTipo'] == $autorizacoes[$index1]["CodTipo"]){
                            $autorizado = true;
                        }
                    }    
                }

                //SGTA-75 - adm pode acessar
                if($CodPerfil ==1){
                    $autorizado = true;
                }
                 
                if($autorizado == true){
                    if($qtd['registros'] > 0 || $acao == "pré-análise" || $CodPerfil < 5){
                        $icone = '<a href="#" title="Visualizar Trilha" class="editar" onclick="return consultar(\''.base64_encode($trilhas[$index]['CodTrilha'] * 2999).'\','.$qtd['registros'].');"><span class="glyphicon glyphicon-eye-open"></span></a><span style="display: none;">permitido</span> &nbsp';
                      
                        //sgta 146 - impressao temporariamente desativada para supervisores e analistas
                        if( $CodPerfil < 4) {
                            $icone = $icone . '<a href="#" title="Relatório da Trilha" class="consultar" onclick="return relatorio('.$trilhas[$index]['CodTrilha'].');"><span class="glyphicon glyphicon-print"></span></a> &nbsp';
                        }

                        $icone = $icone . '<a href="#" title="Gráfico da Trilha" class="incluir" onclick="return grafico('.$trilhas[$index]['CodTrilha'].');"><span class="glyphicon glyphicon-stats"></span></a>';
                        $exibir = true;
                        if ($qtd['registros'] == 0){
                            $qtd['registros'] = $qtd['total_registros'];
                            $qtd['concluidos'] = $qtd['total_registros'];
                            $status = "concluído sem envio";
                            $cor = "BlueViolet";
                        }
                    }else{
                        $icone = '<span class="glyphicon glyphicon-eye-open" style="color: gray;"  title="Nenhum Registro para Consultar."></span> &nbsp';
                        $icone = $icone . '<span class="glyphicon glyphicon-print" style="color: gray;"  title="Nenhum Registro para Consultar."></span> &nbsp';
                        $icone = $icone . '<span class="glyphicon glyphicon-stats" style="color: purple;"  title="Nenhum Registro para Consultar."></span>';
                        $exibir = false;
                    } 
                }else{
                    $icone = '<span class="glyphicon glyphicon-eye-open" style="color: gray;"  title="Seu perfil não possui acesso a área desta trilha."></span> &nbsp';
                    $icone = $icone . '<span class="glyphicon glyphicon-print" style="color: gray;"  title="Seu perfil não possui acesso a área desta trilha."></span> &nbsp';
                    $icone = $icone . '<span class="glyphicon glyphicon-stats" style="color: purple;"  title="Nenhum Registro para Consultar."></span>';
                    $exibir = false;
                }
                
                if($trilhas[$index]['TipoTrilha'] == "pós"){
                    $prazo = $prazo_legal_pos;
                }else{
                    $prazo = $prazo_legal_pre;
                }
                
                $one= new DateTime($trilhas[$index]['DataPublicacao']);
                $two = new DateTime();

                // Resgata diferença entre as datas
                $dias = $one->diff($two);
                $dias = $dias->days;
                
                if($dias > $prazo){
                    $prazo_estourado = true;
                }else{
                    $prazo_estourado = false;
                }
 
                if($prazo_estourado == true){
                    $css = "font-weight: bold; color: red;";
                }else{
                    $css = "";
                }
                
                if($status != "distribuído"){
                    $css = "";
                    $dias = 0;
                }
                
                if($exibir === true){
                    $html = $html.
                    '<tr>
                        <td>'.$trilhas[$index]['CodTrilha'].'<span style="display: none;">id='.$trilhas[$index]['CodTrilha'].'</span></td>
                        <td><a href="#" onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$trilhas[$index]['DescricaoTipo'].'\')">'.$trilhas[$index]['DescricaoTipo'].'</a></td>
                        <td><a href="#" onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$trilhas[$index]['NomeTrilha'].'\')">'.$trilhas[$index]['NomeTrilha'].'</a></td>
                        <td style="text-align: center;">'.data_br($trilhas[$index]['DataInicial']).'</td>
                        <td style="text-align: center;">'.data_br($trilhas[$index]['DataFinal']).'</td>
                        <td style="text-align: center;">'.data_br($trilhas[$index]['DataPublicacao']).'</td> 
                        <td id="qtd_'.$trilhas[$index]['CodTrilha'].'" style="text-align: center;">'.$qtd['registros'].'</td>
                        <td id="qtd_'.$trilhas[$index]['CodTrilha'].'" style="text-align: center;">'.$qtd['pendentes'].'</td>
                        <td id="qtd_'.$trilhas[$index]['CodTrilha'].'" style="text-align: center;">'.$qtd['justificados'].'</td>
                        <td id="qtd_'.$trilhas[$index]['CodTrilha'].'" style="text-align: center;">'.$qtd['concluidos'].'</td>
                        <td style="text-align: right; '.$css.'">'.$prazo.'</td>  
                        <td style="text-align: right; '.$css.'">'.$dias.'</td>
                        <td><a style="font-weight: bold; color: '.$cor.';" href="#" onclick="return filtrar_tabela(\'tbl_trilhas\',\''.$status.'\')">'.$status.'</a></td>
                        <td>
                            '.$icone.'
                        </td>
                    </tr>';  
                }
            }
        }

        $array['html'] = $html;
        
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage();
        retorno();
    }    
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}