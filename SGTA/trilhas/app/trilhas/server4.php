<?php


try {
    ini_set('precision', '15');
    
    
    $__servidor = true;
    $operacao = "";
    require_once("../../code/verificar.php");
    if($__autenticado == false){
        $array = array("operacao" => $operacao,"erro" => "","alerta" => "A sessão expirou, será necessário realizar um novo login.","noticia" => "","retorno" => "");
        retorno();
    }
    require_once("../../code/funcoes.php");
    require_once("../../obj/trilhas.php");
    require_once("../../obj/registros.php");
    require_once("../../obj/orgaos.php");
    
    if(isset($_POST['operacao'])){
        $operacao = $_POST['operacao'];
    }else{
        $operacao = "indefinido";
    }
    
    $array = array("operacao" => $operacao,"erro" => "","alerta" => "","noticia" => "","retorno" => "");

    $obj_trilhas = new trilhas();
    $obj_registros = new registros();
    

    $array['operacao'] = $operacao;
    
    switch ($array["operacao"]) {
        case "load":
            
            $CodTrilha = $_POST['CodTrilha'];
            
            $obj_trilhas = new trilhas();
            $obj_trilhas->consulta_trilha($CodTrilha);
            if($obj_trilhas->erro != ""){
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }

            $query = $obj_trilhas->query;
            $row = mysqli_fetch_array($query);
            $tabela_ = $row['TabelaDetalhe'];
            
            if (file_exists("../registros/$tabela_/campos.php")) {
                require_once "../registros/$tabela_/campos.php";
            }else{
                require_once "../registros/$tabela_/$tabela_"."_carga.php";
            }
            
            $html = "";
            
            for ($index = 0; $index < count($var); $index++){

                if($index > 3){
                    $cor = "black";
                }else{
                    $cor = "MediumVioletRed";
                }

                $html = $html.  '<tr>
                                    <td style="color:'.$cor.';">'.$var[$index]['coluna'].'</td>
                                    <td>'.$var[$index]['descrição'].'</td>
                                    <td>'.$var[$index]['formato'].'</td>
                                    <td style="text-align: right;">'.$var[$index]['tamanho'].'</td>
                                </tr>';
            }
            
            $array['html'] = $html;
            
            retorno();
            break;
        case "exibir":
            $CodTrilha = $_POST['CodTrilha'];
           
            $obj_orgaos = new orgaos();
            
            $obj_orgaos->consulta_orgaos(-1);
            if($obj_orgaos->erro != ""){
                $array['erro'] = $obj_orgaos->erro;
                retorno();
            }
            $query = $obj_orgaos->query;
            while ($row = mysqli_fetch_assoc($query)){
                $orgaos[] = $row; 
            }
            
            $obj_trilhas->consulta_trilha($CodTrilha);
            if($obj_trilhas->erro != ""){
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }
            
            $query = $obj_trilhas->query;
            $row = mysqli_fetch_array($query);

            $tabela_ = $row['TabelaDetalhe'];
            $NomeTrilha = $row['NomeTrilha'];
            $TabOrgaos = $row['CodigoOrgao'];
            $continua = $row['continua'];
            
            if($TabOrgaos == "RHNet"){
                $TabOrgaos = "CodOrgao";
            }
            
            if (file_exists("../registros/$tabela_/campos.php")) {
                require_once "../registros/$tabela_/campos.php";
            }else{
                require_once "../registros/$tabela_/$tabela_"."_carga.php";
            }
            
            $total_colunas = count($var);
            
            $sql = "";
         

         /*   $sql_header = "--> Insert Into $tabela_ (CodReg, ";
            for ($index = 0; $index < count($var); $index++){
                if($index > 3){
                    $sql_header = $sql_header . $var[$index]['coluna'];
                    if($index < count($var) - 1){
                        $sql_header = $sql_header .", ";
                    }
                }
            }
            $sql_header = $sql_header . ") Values ({CodReg}, ";
            */
            
             // carregar a classe PHPExcel
            require_once '../../plugin/PHPExcel.php';
            
            // definir a abertura do ficheiro em modo só de leitura
            //$objReader = new PHPExcel_Reader_Excel2007();
            $objReader = PHPExcel_IOFactory::createReaderForFile("../../intra/carga/$CodTrilha/$tabela_.xlsx");
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load("../../intra/carga/$CodTrilha/$tabela_.xlsx");
            $objPHPExcel->setActiveSheetIndex(0);

            $colunas = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
            //$total_colunas = PHPExcel_Cell::columnIndexFromString($colunas);
            $total_linhas = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

            
            
            
            $error_list = "";
            $html = "";
            
            $n = 0;
            // navegar na linha
            for($linha=1; $linha<=$total_linhas; $linha++){
                $html = $html .  "<tr>";
                    // navegar nas colunas da respectiva linha

                for($coluna=0; $coluna < $total_colunas; $coluna++){

                    if($linha==1){
                        // escreve o cabeçalho da tabela a bold
                        //$html = $html .  "<th>".$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue()."</th>";

                        if($coluna == 0){
                            $html = $html .  "<th style='width: 10px;'><b>" . $linha . "</b></th>";
                        }
                        
                        if($coluna > 3){
                            $cor = "black";
                        }else{
                            $cor = "MediumVioletRed";
                        }
                        
                        switch ($var[$coluna]["formato"]){
                            case "decimal":
                                $html = $html .  "<th style='text-align: right; color: $cor;'>".$var[$coluna]["coluna"]."</th>";
                                break;
                            case "ignorar":
                            case "texto":
                               $html = $html .  "<th style='color: $cor;'>".$var[$coluna]["coluna"]."</th>";
                                break;
                            case "data":
                                $html = $html .  "<th style='text-align: center; color: $cor;'>".$var[$coluna]["coluna"]."</th>";
                                break;
                            case "moeda":
                                $html = $html .  "<th style='text-align: right; color: $cor;'>".$var[$coluna]["coluna"]."</th>";
                                break;
                        }
                        
                        $tabela[$linha - 1]["col_".$coluna] = $var[$coluna]["coluna"];

                    }else{
                        if($coluna == 0){
                            $html = $html .  "<td><b>" . $linha . "</b></td>";
                        }
                        
                        // escreve os dados da tabela  
                        $cell = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
                        
                        $CodigoInvalido = false;
                        
                        if($coluna == 0){
                            $cpf = trim($cell);
                            $cpf = ltrim($cpf, "0");
                            $cell = ltrim($cell, "0");
                            
                            if($cpf == ""){
                                $CodigoInvalido = true;
                                $error_list = $error_list . "Linha: $linha. CPF_CNPJ_Proc não Informado.<br>";
                            }
                        }
                        
                        if($coluna == 1){
                            $nome = trim($cell);
                            
                            if($nome == ""){
                                $CodigoInvalido = true;
                                $error_list = $error_list . "Linha: $linha. Nome_Descricao não Informado.<br>";
                            }
                        }
                        
                        if($coluna == 2){
                            $CodOrgao = trim($cell);
                            if(!is_numeric($CodOrgao)){
                                $CodigoInvalido = true;
                                $error_list = $error_list . "Linha: $linha. Código do Órgão Inválido.<br>";
                            }else{ 
                                
                                $CodigoInvalido = true;
                                
                                for ($index = 0; $index < count($orgaos); $index++){
                                    //if($CodOrgao == $orgaos[$index][$TabOrgaos]){
                                    //    $CodigoInvalido = false;
                                    //}
                                    //if(strpos($orgaos[$index][$TabOrgaos],$CodOrgao) !== false){
                                    //   $CodigoInvalido = false;
                                    //}
                                    
                                    $a1 = explode(";", $orgaos[$index][$TabOrgaos]);
                                    for ($index2 = 0; $index2 < count($a1); $index2++){
                                        if($a1[$index2] == $CodOrgao){
                                            $CodigoInvalido = false;
                                        }
                                    }
                                }
                                
                                if($CodigoInvalido == true){
                                    $error_list = $error_list . "Linha: $linha. Código do Órgão Inválido.<br>";
                                }
                            }
                        }

                        switch ($var[$coluna]["formato"]){
                            case "decimal":
                                $cell = substr(trim($cell),0,$var[$coluna]["tamanho"]);
                                if(is_numeric($cell) && $CodigoInvalido == false){
                                    $tabela[$linha - 1]["col_".$coluna] = $cell;
                                    $cell = "<td style='text-align: right;color:DodgerBlue;'>".$cell."</td>";
                                }else{
                                    $tabela[$linha - 1]["col_".$coluna] = "null";
                                    $cell = "<td style='text-align: right; background-color: LightSalmon; color:red; font-weight: bold;'>".$cell."</td>";
                                }
                                break;
                            case "ignorar":
                            case "texto":
                                $cell = str_replace("'", "", trim($cell));
                                $tabela[$linha - 1]["col_".$coluna] = substr($cell,0,$var[$coluna]["tamanho"]);
                                if($CodigoInvalido == false){
                                    $cell = "<td>".substr($cell,0,$var[$coluna]["tamanho"])."</td>";
                                }else{
                                    $cell = "<td style='background-color: LightSalmon; color:red; font-weight: bold;'>".substr($cell,0,$var[$coluna]["tamanho"])."</td>";
                                }
                                
                                break;
                            case "data":
                                if(is_numeric($cell)){
                                    $cell = $cell + 1;
                                    $cell = date('d/m/Y', PHPExcel_Shared_Date::ExcelToPHP($cell));
                                    $tabela[$linha - 1]["col_".$coluna] = isdate($cell);
                                    $cell = "<td style='text-align: center; color:SlateBlue;'>".$cell."</td>";
                                }else{
                                    $cell = "<td style='text-align: center; background-color: LightSalmon; color:red; font-weight: bold;'>".$cell."</td>";
                                    $tabela[$linha - 1]["col_".$coluna] = "null";
                                }
                                
                                break;
                            case "moeda":
                                
                                if(is_numeric($cell)){
                                    $numeromysql = numero_br($cell);
                                    $numeromysql = str_replace(".", "", $numeromysql);
                                    $numeromysql = str_replace(",", ".", $numeromysql);
                                    $tabela[$linha - 1]["col_".$coluna] = $numeromysql;
                                    $cell = "<td style='text-align: right;color:ForestGreen;'>".numero_br($cell)."</td>";
                                }else{
                                    $tabela[$linha - 1]["col_".$coluna] = "null";
                                    $cell = "<td style='text-align: right; background-color: LightSalmon;color:red; font-weight: bold;'>".$cell."</td>";
                                }
                                
                                break;
                        }

                        $html = $html .  $cell;
                        
                    }
                }
                $html = $html .  "</tr>"; 
            }
            
            pulo:
            
            $info = "";
            
            $sort = array();
            foreach($tabela as $k => $v) {
                $sort['col_3'][$k] = $v['col_3']; //nome do órgão
                $sort['col_0'][$k] = $v['col_0']; //cnpj
            }
            # sort by event_type desc and then title asc
            array_multisort($sort['col_3'], SORT_DESC, $sort['col_0'], SORT_DESC, $tabela);

            if($error_list != ""){
                $info = $info .  "<b>OS SEGUINTES ERROS IMPEDEM A GRAVAÇÃO:<br></b>";
                $info = $info .  $error_list;
                $array['contem_erro'] = true;
            }else{
                $array['contem_erro'] = false;
            }

            //$info = $info .  print_r($tabela);
            $sql = "";
            $m1 = "--,";
            $m2 = "--.";

            $CodParecer = 6;

            // SGTA 173 controlar inclusão de codigo do registro
            $obj_registros->getMaxCodigo();
            if($obj_registros->erro != ""){
                $array['erro'] = $obj_registros->erro;
              
            }
            $query = $obj_registros->query;
            $row = mysqli_fetch_array($query);
            $auxMaxCod = $row['codigo'] + 1;


            $sqllistNOtificaDuplicado =[];
            $sqllist = [];

            $NumRegistrosTrilha = null;

            $NumRegistrosTrilha = $obj_trilhas->conta_registros_trilha($CodTrilha);

            if($obj_trilhas->erro != ""){
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }
            $saida = "";
            for ($linha=0; $linha < count($tabela); $linha++) {

                //SGTA 173
                $Usuario = $_SESSION['sessao_id'];
                //verificar se tem registro, se sim, colocar objservação 
              
              //  if( ($continua == 1 && ($rowqueryDuplicado ==0  or $rowqueryDuplicado >= 3 ) ) or ($continua == 0) or ($NumRegistrosTrilha == 0) ){
       

                    if ($tabela[$linha]["col_0"] != "CPF_CNPJ_Proc") {
                        if ($m1 != $tabela[$linha]["col_3"] || $m2 != $tabela[$linha]["col_0"]) {
                            $sql_corpo = "";

                            $CodOrgao = $tabela[$linha]["col_2"];
                            $cpf = $tabela[$linha]["col_0"];
                            $nome = $tabela[$linha]["col_1"];
                            
                            if ($TabOrgaos != "CodOrgao") {
                                for ($index1 = 0; $index1 < count($orgaos); $index1++) {
                     
                                    $a1 = explode(";", $orgaos[$index1][$TabOrgaos]);
                                    for ($index2 = 0; $index2 < count($a1); $index2++) {
                                        if ($a1[$index2] == $CodOrgao) {
                                            $CodOrgao = $orgaos[$index1]["CodOrgao"];
                                        }
                                    }
                                }
                            }
                    //testar se trilha não esta viasia
                            $rowqueryDuplicado = null ;
                            $rowqueryDuplicado =   $obj_registros->RegraVerificaDuplicado($CodTrilha, $CodOrgao, $cpf);
                            $saida .= ('status do registro - '.$rowqueryDuplicado. "<br>");
                          
                          //  if ( ($continua == 1 &&  ($rowqueryDuplicado ==0  or $rowqueryDuplicado >= 3 )  ) or ($NumRegistrosTrilha == 0) ) {
                            if( ($continua == 1 && ($rowqueryDuplicado ==0  or $rowqueryDuplicado >= 3 ) )  or ($NumRegistrosTrilha == 0) ){
                                $sql_corpo = "Insert Into registros (CodRegistro, CodTrilha, CodOrgao, CodParecer, CPF_CNPJ_Proc, Nome_Descricao,StatusRegistro) Values ( $auxMaxCod ,$CodTrilha,$CodOrgao,$CodParecer,'$cpf','$nome','rascunho');";
                                //adiciona obervação se duplicado
                                if (intval($rowqueryDuplicado) == 3) {
                                    $sqllistNOtificaDuplicado[]= "Insert Into chat_registros (CodRegistro, Usuario, Mensagem) Values ('$auxMaxCod', '$Usuario', 'O presente registro foi adicionado automaticamento, e e consta em duplicidade nessa trilha.');";
                                //$sqllistNOtificaDuplicado[]= "update registros set   ObsCGE = 'O presente registro foi adicionado automaticamento, e e consta em duplicidade nessa trilha.' where  CodRegistro = '" . $auxMaxCod."' ;";
                                } elseif (intval($rowqueryDuplicado) == 5) {
                                    $sqllistNOtificaDuplicado[]= "Insert Into chat_registros (CodRegistro, Usuario, Mensagem) Values ('$auxMaxCod', '$Usuario', 'Uma copia deste registro se encontra excluido nessa trilha');";
                                } elseif (intval($rowqueryDuplicado) == 8) {
                                    $sqllistNOtificaDuplicado[]= "Insert Into chat_registros (CodRegistro, Usuario, Mensagem) Values ('$auxMaxCod', '$Usuario', 'Uma copia deste registro se encontra excluido, e outra esta com o status concluído nessa trilha');";
                                }
                            } elseif ($continua == 0) {
                                $sql_corpo = "Insert Into registros (CodTrilha, CodOrgao, CodParecer, CPF_CNPJ_Proc, Nome_Descricao) Values ( $CodTrilha,$CodOrgao,$CodParecer,'$cpf','$nome');";
                            }
                            
                          
                            $sqllist[] = $sql_corpo;
                        }
                    
                        $m1 = $tabela[$linha]["col_3"];
                        $m2 = $tabela[$linha]["col_0"];


                     
                        if ( ($continua == 1 && ($rowqueryDuplicado ==0  or $rowqueryDuplicado >= 3) ) or ($continua == 0) or ($NumRegistrosTrilha == 0)) {

                            //cabeçalho de inclusão do template
                            $sql_header = null;

                            if ($continua == 1) {
                                $sql_header = "Insert Into $tabela_ (CodReg, ";
                            } else{
                                $sql_header = "--> Insert Into $tabela_ (CodReg, ";
                            }
                           

                            for ($index = 0; $index < count($var); $index++){
                                if($index > 3){
                                    $sql_header = $sql_header . $var[$index]['coluna'];
                                    if($index < count($var) - 1){
                                        $sql_header = $sql_header .", ";
                                    }
                                }
                            }
                            //tratamento para verificar o id do registro para trilha continuas e não continuas
                            if($continua == 1){
                                $sql_corpo = $sql_header . ") Values ('$auxMaxCod', ";
                            } else {
                                $sql_corpo = $sql_header . ") Values ({CodReg}, ";
                            }
                            

                            for ($coluna=0; $coluna < count($tabela[$linha]); $coluna++) {
                                if ($coluna > 3) {
                                    $cell = $tabela[$linha]["col_".$coluna];

                                    if ($cell != "null") {
                                        $cell = "'$cell'";
                                    }

                                    $sql_corpo = $sql_corpo . $cell;

                                    if ($coluna<$total_colunas - 1 && $var[$coluna]["formato"] != "ignorar") {
                                        $sql_corpo = $sql_corpo .",";
                                    }
                                }
                            }

                            $sql_corpo = $sql_corpo . ");";
                        }

                      
                        $sqllist[] = $sql_corpo;
                    }
               // }    
                $auxMaxCod ++;
            }
            
            
          
            $_SESSION['sqllist'] = $sqllist;
            $_SESSION['sqllistContinuo'] = $sqllistNOtificaDuplicado;
            
       
            $array['html'] = $html;
            $array['info'] = $info;
                   
            
            retorno();
            break;
        
        case "execute":
            $CodTrilha = $_POST['CodTrilha'];
            $sqllist = $_SESSION['sqllist'];
            $sqllistNOtificaDuplicado = $_SESSION['sqllistContinuo'] ;
         
            
            $array['info'] = "";//$info;
            
            require_once("../../obj/conexao.php");
            $obj_conexao = new conexao();
            
            $reg = 1;
            $af = 1;
     
            $obj_trilhas->consulta_trilha($CodTrilha);
            if($obj_trilhas->erro != ""){
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }
            
            $query = $obj_trilhas->query;
            $row = mysqli_fetch_array($query);

            $tabela_ = $row['TabelaDetalhe'];
            $NomeTrilha = $row['NomeTrilha'];
            $TabOrgaos = $row['CodigoOrgao'];
            $continua = $row['continua'];



            $obj_conexao->iniciar_transacao();
        
            if(count($sqllist) > 0  ){
             
            //   die( var_dump($sqllist));

                $NumRegistrosTrilha = null;
                $NumRegistrosTrilha = $obj_trilhas->conta_registros_trilha($CodTrilha);

                if( ($continua == 0) or ($NumRegistrosTrilha == 0) ) {

                    //so vale para trilha não continua
                    for ($index = 0; $index < count($sqllist); $index++) {
                
                        $mystring = $sqllist[$index];
                        $findme   = 'Insert Into registros';
                        $pos = strpos($mystring, $findme);
                       
                        if($pos === false){
                            $sql = $mystring;
                            $sql = str_replace("--> ", "", $sql);
                            $sql = str_replace("{CodReg}", $id, $sql);
                            $obj_conexao->insert($sql);
                        }else{
                            $sql = $mystring;
                            $id = $obj_conexao->insert($sql);
                            $reg++;
                        }
                    
                        if($obj_conexao->erro != ""){
                         
                            $array["erro"] = "Linha: $af - " . $obj_conexao->erro . "  sql: ".$sql;
                            $obj_conexao->rollback();
                            retorno();
                        }
                        $af++; 
                    }

                } else {
                        //trilha continua
                        //não necessita de tratamento para a inserção do registro no template
                        for ($index = 0; $index < count($sqllist); $index++) {
                            if($sqllist[$index] != ""){

                                $sql  = $sqllist[$index];
                                $id = $obj_conexao->insert( $sql);
                                $reg++;
                            
                                if($obj_conexao->erro != ""){
                                 
                                    $array["erro"] = "Linha: $af - " . $obj_conexao->erro . "  sql: ".$sql;
                                    $obj_conexao->rollback();
                                    retorno();
                                }
                             

                            }
                            $af++; 
                        }

                }
              

            } else {

                $array["erro"] = "Todos os registros dessa importação ja estão no sistema.";
                retorno();
            }
            

           
            $sql = "Select Distinct CodOrgao From registros Where CodTrilha = $CodTrilha";
            $query = $obj_conexao->select($sql);
            if($obj_conexao->erro != ""){
                $array["erro"] = "Linha: $af - " . $obj_conexao->erro;
                $obj_conexao->rollback();
                retorno();
            }
            
            while ($row = mysqli_fetch_array($query)){
                $CodOrgao = $row['CodOrgao'];
                
                $CodTrilhaOrgao = $CodTrilha."00".$CodOrgao;

                $sql2 = "Replace Into trilha_orgao (CodTrilhaOrgao, CodTrilha, CodOrgao) Values ($CodTrilhaOrgao, $CodTrilha, $CodOrgao)";
                $obj_conexao->execute($sql2);
                if($obj_conexao->erro != ""){
                    $array["erro"] = "Linha: $af - " . $obj_conexao->erro . "  sql: ".$sql;
                    $obj_conexao->rollback();
                    retorno();
                }
            }
                
            $af--;
            $reg--;
   
            $obj_conexao->commit();

             //SGTA 173
    
             if(count($sqllistNOtificaDuplicado) >0){
                $obj_conexao->iniciar_transacao();
                for ($index = 0; $index < count($sqllistNOtificaDuplicado); $index++){
                    $mystring2 = null;
                    $mystring2 = $sqllistNOtificaDuplicado[$index];
                    $obj_conexao->insert( $mystring2);
                 }
               
                 $obj_conexao->commit();
             }
            
            
            $array['mensagem'] = "SQL executado com sucesso, ".($af - $reg)." registros do excel foram incluídos e agrupados em $reg registros no sistema.";
            
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}
    

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}