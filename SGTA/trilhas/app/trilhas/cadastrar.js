var execute_cod = 0;
var destino = "server.php";

$(document).ready(function(){ 
    
    $( "#data_inicial" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy'
    });
    
    $( "#data_final" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy'
    });
    
    $( "#data_trilha" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy'
    });
    
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    limpa_campos();
    submit_form(destino);
    
    popup_form_1();
});

function popup_form_1(){
    $("#popup_form1").dialog({
        autoOpen: false,
        resizable: false,
        width: 630, 
        modal: false,
        show: {
            effect: "fade",
            duration: 200
        },
        hide: {
            effect: "fade",
            duration: 200
        },
        buttons: {
            "Salvar": function() {
                $(function() {

                    $("#dialog_msg").text("Gostaria de gravar as informações?");
                    $("#popup_dialog").dialog("open");
                    formdata = $("#form3").serialize();

                });
            },
            "Cancelar": function() {
                $(this).dialog("close");
            }
        }
    });
    
    $("#popup_anexos").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 800,
        maxWidth: 800,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":				
                $("#tipo").html(retorno.html);
                $("#tabela").html(retorno.tabela);
                break;
            case "consulta_grupos":
                $("#grupo").html(retorno.html);
                break;
            case "salvar":
                execute_cod = 1;
                $("#id").val(retorno.id);
                $("#CodTrilha").val(retorno.id);
                $("#tabela").html(retorno.tabela);
                $("#msg_sucesso").html(retorno.msg_sucesso);
                $("#popup_sucesso").dialog("open");
                break;
            case "editar":
                $("#id").val(retorno.campos[0]['CodTrilha']);
                $("#CodTrilha").val(retorno.campos[0]['CodTrilha']);
                $("#tipo").val(retorno.campos[0]['CodTipo']);
                $("#grupo").html(retorno.html);
                $("#grupo").val(retorno.campos[0]['CodGrupo']);
                $("#data_inicial").val(retorno.campos[0]['DataInicial']);
                $("#data_final").val(retorno.campos[0]['DataFinal']);
                $("#data_trilha").val(retorno.campos[0]['DataTrilha']);
                $("#criterios").val(retorno.campos[0]['Criterios']);
                rolar_para("#id");
                break;
            case "sql":
                $("#msg_texto").html(retorno.mensagem);
                $("#popup_mensagem").dialog("open");
                break;
            case "consulta_detalhe":
                $("#detalhe").val(retorno.detalhe);
                $("#NomeTrilha").val(retorno.NomeTrilha);
                $("#CodTrilha2").val(retorno.CodTrilha);
                document.getElementById("form2").action = "../registros/exibir.php";
                document.getElementById("form2").submit();
                break;
            case "liberar":
                $("#msg_sucesso").html(retorno.mensagem);
                $("#popup_sucesso").dialog("open");
                $("#tabela").html(retorno.tabela);
                break;
            case "excluir":
                $("#msg_sucesso").html(retorno.mensagem);
                $("#popup_sucesso").dialog("open");
                $("#tabela").html(retorno.tabela);
                break;
            case "excluir_anexo":
                $("#msg_sucesso").html(retorno.mensagem);
                $("#popup_sucesso").dialog("open");
                $("#tb_anexos").html(retorno.anexos);
                break;
            case "visualizar_anexos":
                $("#tb_anexos").html(retorno.anexos);
                $("#popup_anexos").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            rolar_para("#tabela");
            limpa_campos();
            break;
        case 2:
            $("#operacao").val("load");
            formdata = $("#form1").serialize();
            limpa_campos();
            submit_form(destino);
            break
    }
    execute_cod = 0;
}

function consulta_grupos(){
    $("#operacao").val("consulta_grupos");
    formdata = $("#form1").serialize();
    submit_form(destino);
}

function editar(cod){
    $("#CodTrilha").val(cod);
    $("#operacao").val("editar");
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}

function salvar(){
    $("#operacao").val("salvar");
    formdata = $("#form1").serialize();
    $("#msg_dialogo").html("As informações estão corretas?");
    $("#popup_dialogo").dialog("open");
}

function sql(cod){
    $("#CodTrilha").val(cod);
    $("#operacao").val("sql");
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}

function cancelar(){
    limpa_campos();
    rolar_para("#tabela");
}

function rolar_para(elemento) {
    $('html, body').animate({
      scrollTop: $(elemento).offset().top - 200
    }, 300);
}

function limpa_campos(){
    $("#id").val("");
    $("#tipo").val('-1');
    $("#grupo").html('<option value="-1"></option>');
    $("#data_inicial").val("");
    $("#data_final").val("");
    $("#data_trilha").val("");
    $("#criterios").val("");
    $("#CodTrilha").val('0');
}

function incluir(cod){
    $("#CodTrilha1").val(cod);
    $("#lbl_trilha").text("TRILHA: " + cod + " - " + $("#nt_" + cod).text());
    $('#form_upload').dialog('open');
    return false;
}

function anexar(cod){
    $("#CodTrilha2").val(cod);
    $("#lbl_trilha1").text("TRILHA: " + cod + " - " + $("#nt_" + cod).text());
    $('#form_anexo').dialog('open');
    return false;
}

function consultar(cod){
    var qtd = $("#qtd_" + cod).text();
    if(qtd == "0"){
        $("#msg_alerta").html("Esta trilha não possui nenhum registro cadastrado.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    $("#CodTrilha2").val(cod);
    $("#operacao2").val("consulta_detalhe");
    $("#acao").val("consulta");
    $("#Orgao").val("0");
    formdata = $("#form2").serialize();
    submit_form(destino);
    return false;
}

function liberar(cod){
    var qtd = $("#qtd_" + cod).text();
    if(qtd == "0"){
        $("#msg_alerta").html("Esta trilha não possui nenhum registro cadastrado.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    
    $("#CodTrilha").val(cod);
    $("#operacao").val("liberar");
    formdata = $("#form1").serialize();
    $("#msg_dialogo").html("Gostaria de enviar os " + qtd + " registros da trilha para homologação?");
    $("#popup_dialogo").dialog("open");
    return false;
}

function excluir(cod){
    
    $("#CodTrilha").val(cod);
    $("#operacao").val("excluir");
    formdata = $("#form1").serialize();
    
    var qtd = $("#qtd_" + cod).text();
    if(qtd == "0"){
        $("#msg_dialogo").html("Você tem certeza que gostaria de EXCLUIR a trilha selecionada?<br><span style='color: red;'>AVISO:</span> Esta operação não poderá ser desfeita.");
        $("#popup_dialogo").dialog("open");
    }else{
        $("#msg_dialogo").html("Você tem certeza que gostaria de excluir os " + qtd + " registros da trilha selecionada?<br><span style='color: red;'>AVISO:</span> Esta operação não poderá ser desfeita.");
        $("#popup_dialogo").dialog("open");
    }
    
    return false;
}

function visualizar_anexos(cod){
    $("#CodTrilha").val(cod);
    $("#operacao").val("visualizar_anexos");
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
    $("#popup_anexos").dialog("open");
}

function excluir_anexo(CodAnexo, CodTrilha){
    $("#CodTrilha").val(CodTrilha);
    $("#CodAnexo").val(CodAnexo);
    $("#operacao").val("excluir_anexo");
    formdata = $("#form1").serialize();
    $("#msg_dialogo").html("<span style='color: red;'>Você tem certeza que gostaria de excluir este arquivo?</span>");
    $("#popup_dialogo").dialog("open");
    return false;
}

function efetuar_carga(CodTrilha){
    $("#CodTrilha2").val(CodTrilha);
    document.getElementById("form2").action = "importar.php";
    document.getElementById("form2").submit();
    return false;
}