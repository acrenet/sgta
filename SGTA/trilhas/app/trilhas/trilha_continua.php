<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if($perfil != 1){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $operacao = "load_continuo";
    $CodTrilha = "0";
     
  
?>
<script src="../../js/datepicker.js" type="text/javascript"></script>
<script src="trilha_continua.js" type="text/javascript"></script>
<br>
<div class="container">
    <div class="panel-group" >
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Trilhas Continuas.</h4></div>
            <div class="panel-body">
                <table class="table table-bordered table-striped table-hover table-condensed display">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Área</th>
                            <th>Trilha</th>
                            <th>Data Inicial</th>
                           <!-- <th>Data Final</th>
                            <th>Critérios</th>
                            <th>Contínua?</th>
                            <th style="text-align: center;"><span class="glyphicon glyphicon-paperclip"></span></th>-->
                            <th style="text-align: center;">Reg. em Homologação</th>
                            <th style="text-align: center;">Reg. Pendentes</th>
                            <th style="text-align: center;">Reg. Encerrados</th>
                            <th style="text-align: center;">Reg. Excluídos</th>
                            <th style="text-align: center;">Reg. Total</th>
                            <th style="width: 137px;">Ações</th>
                        </tr>
                    </thead>
                    <tbody id="tabela">
 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
        <div class="panel-group" style="visibility: hidden;">
            <div class="panel panel-primary">
                <div class="panel-heading"><h4>Cadastrar Nova Trilha.</h4></div>
                <div class="panel-body">
                    
                    <form id="form1" class="form-horizontal" role="form" method="post">
                        
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="tipo">Código Trilha:</label>
                            <div class="col-sm-1">
                                <input type="text" class="form-control" id="id" name="id" readonly="" disabled="">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="tipo">Área de Trilha:</label>
                            <div class="col-sm-6">
                                <select name="tipo" id="tipo" class="form-control" onchange="consulta_grupos();">
                                    <option value="-1"></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="grupo">Grupo/Tabela da Trilha:</label>
                            <div class="col-sm-6">
                                <select name="grupo" id="grupo" class="form-control">
                                    <option value="-1"></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="data_inicial">Data Inicial:</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="data_inicial" name="data_inicial" readonly="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="data_final">Data Final:</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="data_final" name="data_final" readonly="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="data_trilha">Data de Geração:</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="data_trilha" name="data_trilha"  readonly="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="criterios">Contínua:</label>
                            <div class="col-sm-6">
                                <select name="continua" id="continua" class="form-control">
                                    <option value="0">NÃO</option>
                                    <option value="1">SIM</option>
                                </select>
                               
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="criterios">Critérios Utilizados:</label>
                            <div class="col-sm-6">
                                <textarea name="criterios" id="criterios" rows="5" cols="20" class="form-control" onkeyup="contar_caracteres('criterios', 'contador', 2000)" maxlength="2000" placeholder="(Opcional) Registra os critérios utilizados na geração da trilha bem como instruções para análise. (2000 caracteres)."></textarea>
                                <input type="text" class="form-control" id="contador" name="contador"  disabled="" style="width: 70px; float: right;">
                            </div>
                        </div>

                        
                        

                        <div class="form-group"> 
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="button" class="btn btn-danger" onclick="return cancelar();">Cancelar &nbsp;&nbsp;<i class="fa fa-mail-reply fa-lg"></i></button>
                                <button type="button" class="btn btn-primary" onclick="return salvar();">Salvar &nbsp;&nbsp;<i class="fa fa-database fa-lg"></i></button>
                            </div>
                        </div>

                        <input type="hidden" name="operacao" id="operacao" value="load_continuo" />
                        <input type="hidden" name="CodTrilha" id="CodTrilha" value="<?php echo $CodTrilha; ?>" />
                        <input type="hidden" name="CodAnexo" id="CodAnexo" value="" />
                    </form>
                    
                </div>
            </div>
        </div>
    
</div>

<div id="popup_anexos" title="Anexos">
        
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover table-condensed" style="font-size: 80%; width: 100%;">
            <thead>
                <tr>
                    <th>Nome Anexo</th>
                    <th>Descrição</th>
                    <th>Usuário</th>
                    <th>Etapa</th>
                    <th>Data Upload</th>
                    <th>Restrito ao TCE</th>
                </tr>
            </thead>
            <tbody id="tb_anexos">
                
            </tbody>
        </table>
    </div>

        
        <hr>
        <div style="text-align: right;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-info" onclick='$("#popup_anexos").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
        </div>
</div>

<form name="form2" id="form2" action="../registros/exibir.php" method="POST">
    <input type="hidden" name="operacao2" id="operacao2" value="" />
    <input type="hidden" name="operacao" id="operacao" value="" />
    <input type="hidden" name="Orgao" id="Orgao" value="" />
    <input type="hidden" name="CodTrilha" id="CodTrilha2" value="" />
    <input type="hidden" name="filtro" id="filtro" value="" />
    <input type="hidden" name="NomeTrilha" id="NomeTrilha" value="" />
    <input type="hidden" name="detalhe" id="detalhe" value="" />
    <input type="hidden" name="acao" id="acao" value="pré-análise" />
    <input type="hidden" name="WhereContinuo" id="WhereContinuo" value="<?php echo @$_POST['WhereContinuo']; ?>" />
</form>


<?php
    
  include("form_upload/form_upload.php");

  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");