<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");

require_once("../../obj/trilhas.php");
require_once("../../obj/orgaos.php");
require_once("../../obj/usuario.php");
require_once("../../obj/autorizacoes.php");

$obj_orgaos = new  orgaos();
$obj_trilhas = new trilhas();
$obj_usuarios = new usuario();
$obj_autorizacoes = new autorizacoes();

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$cpf = $_SESSION['sessao_id'];

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    switch ($array["operacao"]) {
        case "load":
            
            $perfil = $_SESSION['sessao_perfil'];
            $orgao  = $_SESSION['sessao_orgao'];
            $cpf = $_SESSION['sessao_id'];
            
            $orgaos = '';
            
            if($perfil < 3){
                $orgao = -1;
                $orgaos = '<option value="0"></option><option value="-2">TODOS</option>';
            }
            
            $obj_orgaos->consulta_orgaos($orgao);
            if($obj_orgaos->erro != ""){
                $array['erro'] = $obj_orgaos->erro;
                retorno();
            }
            
            $query = $obj_orgaos->query;
            
            while ($row = mysqli_fetch_array($query)){
                $orgaos = $orgaos.'<option value="'.$row['CodOrgao'].'">'.$row['NomeOrgao'].'</option>';
            }
            
            if($perfil > 3){
                $value = $row['NomeOrgao'];
            }
            
            $array['orgaos'] = $orgaos;
            
            $obj_trilhas->consulta_tipos(-1, $cpf);
            if($obj_trilhas->erro != ''){
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }
            
            $query = $obj_trilhas->query;
            
            $tipos = '<option value="0"></option>';
            
             while ($row = mysqli_fetch_array($query)){
                $tipos = $tipos.'<option value="'.$row['CodTipo'].'">'.$row['DescricaoTipo'].'</option>';
            }
            
            $array['tipos'] = $tipos;
            
            retorno();
            break;
        case "ver_permissoes":
            $CodOrgao = $_POST['lst_orgaos'];
            $CodTipo = $_POST['lst_tipos'];
            
            if($CodOrgao == 0){
                $array['exibir'] = false;
                retorno();
            }
            
            if($CodTipo == 0){
                $array['exibir'] = false;
                retorno();
            }
            
            $array['exibir'] = true;
            
            $obj_usuarios->consulta_autorizacoes(0, $CodOrgao);
            if($obj_usuarios->erro != ""){
                $array['erro'] = $obj_usuarios->erro;
                retorno();
            }
            
            $autorizacoes = $obj_usuarios->array;
            
            $obj_usuarios->consulta_usuarios($CodOrgao);
            if($obj_usuarios->erro != ""){
                $array['erro'] = $obj_usuarios->erro;
                retorno();
            }
            
            $query = $obj_usuarios->query;
            
            $lstpermitidos = "";
            $lstnegados = "";
            
            if(mysqli_num_rows($query) > 0 && $autorizacoes[0]['qtd'] > 0){
                while ($row = mysqli_fetch_array($query)){

                    $permitido = false;
                    for ($index = 0; $index < count($autorizacoes); $index++){
                        if($CodTipo == $autorizacoes[$index]["CodTipo"]){
                            if($row["CPF"] == $autorizacoes[$index]["CPF"]){
                                $permitido = true;
                            }
                        }
                    }

                    if($permitido == true){
                        $lstpermitidos = $lstpermitidos .'<li id="'.$row["CPF"].'" class="alert alert-success">'.$row["Nome"].'<br>(<b><i>'.$row['SiglaOrgao'].' - '.$row['NomePerfil'].'</i></b>)</li>';
                    }else{
                        $lstnegados = $lstnegados .'<li id="'.$row["CPF"].'" class="alert alert-danger">'.$row["Nome"].'<br>(<b><i>'.$row['SiglaOrgao'].' - '.$row['NomePerfil'].'</i></b>)</li>';
                    }

                }
            }
            
            $array['autorizado'] = $lstpermitidos;
            $array['negado'] = $lstnegados;
            
            retorno();
            break;
        case "permissoes":
            $permitidos = json_decode($_POST['permitidos']);
            $negados = json_decode($_POST['negados']);
            $CodOrgao = $_POST['lst_orgaos'];
            $CodTipo = $_POST['lst_tipos'];
            
            $obj_autorizacoes->autoriza_usuarios_por_tipo($CodTipo, $permitidos, $negados);
            if($obj_autorizacoes->erro != ""){
                $array['erro'] = $obj_autorizacoes->erro;
                retorno();
            }
            
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}
    

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}