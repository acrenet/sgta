var execute_cod = 0;
var destino = "server3.php";

$(document).ready(function(){
    
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
    
});

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                $("#lst_orgaos").html(retorno.orgaos);
                $("#lst_tipos").html(retorno.tipos);
                break;
            case "ver_permissoes":

                if(retorno.exibir == true){
                    $("#div_listas").slideDown();
                }else{
                    $("#div_listas").slideUp();
                }
                
                $( "#sortable2" ).html(retorno.autorizado);
                $( "#sortable1" ).html(retorno.negado);
                
                $( "#sortable1, #sortable2" ).sortable({
                    connectWith: ".connectedSortable",
                    placeholder: "ui-state-highlight",
                    cursor: "move",
                    revert: true,
                    receive: function( event, ui ) {
                        var sortedIDs = $( "#sortable2" ).sortable( "toArray" );
                        $("#permitidos").val(JSON.stringify(sortedIDs));
                        
                        sortedIDs = $( "#sortable1" ).sortable( "toArray" );
                        $("#negados").val(JSON.stringify(sortedIDs));
                        
                        $("#operacao").val("permissoes");
                        formdata = $("#form1").serialize();
                        submit_form(destino);
                    }
                }).disableSelection();
                
                break;
            case "permissoes":
                
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../administrar/administrar.php");
    }
    execute_cod = 0;
}

function ver_permissoes(){
    $("#operacao").val("ver_permissoes");
    formdata = $("#form1").serialize();
    submit_form(destino);
}
