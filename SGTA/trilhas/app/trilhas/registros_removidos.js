var execute_cod = 0;
var destino = "server.php";

$(document).ready(function(){ 
    
    $( "#data_inicial" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy'
    });
    
    $( "#data_final" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy'
    });
    
    $( "#data_trilha" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy'
    });
    
    $("#operacao").val("load_registros_removidos");
    formdata = $("#form1").serialize();
    limpa_campos();
    submit_form(destino);
    
    popup_form_1();
});

function popup_form_1(){
    $("#popup_form1").dialog({
        autoOpen: false,
        resizable: false,
        width: 630, 
        modal: false,
        show: {
            effect: "fade",
            duration: 200
        },
        hide: {
            effect: "fade",
            duration: 200
        },
        buttons: {
            "Salvar": function() {
                $(function() {

                    $("#dialog_msg").text("Gostaria de gravar as informações?");
                    $("#popup_dialog").dialog("open");
                    formdata = $("#form3").serialize();

                });
            },
            "Cancelar": function() {
                $(this).dialog("close");
            }
        }
    });
    
    $("#popup_anexos").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 800,
        maxWidth: 800,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "retornar_registro":	
           
                execute_cod = 1;
                $("#operacao").val('retornar_registro');
                $("#msg_sucesso").html(retorno.msg_sucesso);
                //$("#popup_sucesso").dialog("open");
                window.location.reload();
                
            break;
            case "load_registros_removidos":				
                $("#tipo").html(retorno.html);
                $("#tabela").html(retorno.tabela);
                break;
            case "consulta_grupos":
                $("#grupo").html(retorno.html);
                break;
            case "salvar":
                execute_cod = 1;
                $("#id").val(retorno.id);
                $("#CodTrilha").val(retorno.id);
                $("#tabela").html(retorno.tabela);
                $("#msg_sucesso").html(retorno.msg_sucesso);
                $("#popup_sucesso").dialog("open");
                break;
            case "editar":
                $("#id").val(retorno.campos[0]['CodTrilha']);
                $("#CodTrilha").val(retorno.campos[0]['CodTrilha']);
                $("#tipo").val(retorno.campos[0]['CodTipo']);
                $("#grupo").html(retorno.html);
                $("#grupo").val(retorno.campos[0]['CodGrupo']);
                $("#data_inicial").val(retorno.campos[0]['DataInicial']);
                $("#data_final").val(retorno.campos[0]['DataFinal']);
                $("#data_trilha").val(retorno.campos[0]['DataTrilha']);
                $("#criterios").val(retorno.campos[0]['Criterios']);
                rolar_para("#id");
                break;
            case "sql":
                $("#msg_texto").html(retorno.mensagem);
                $("#popup_mensagem_continua").dialog("open");
                break;
            case "consulta_detalhe":
                $("#detalhe").val(retorno.detalhe);
                $("#NomeTrilha").val(retorno.NomeTrilha);
                $("#CodTrilha2").val(retorno.CodTrilha);
                document.getElementById("form2").action = "../registros/exibir.php";
                document.getElementById("form2").submit();
                break;
            case "liberar":
                $("#msg_sucesso").html(retorno.mensagem);
                $("#popup_sucesso").dialog("open");
                $("#tabela").html(retorno.tabela);
                break;
            case "excluir":
                $("#msg_sucesso").html(retorno.mensagem);
                $("#popup_sucesso").dialog("open");
                $("#tabela").html(retorno.tabela);
                break;
            case "excluir_anexo":
                $("#msg_sucesso").html(retorno.mensagem);
                $("#popup_sucesso").dialog("open");
                $("#tb_anexos").html(retorno.anexos);
                break;
            case "visualizar_anexos":
                $("#tb_anexos").html(retorno.anexos);
                $("#popup_anexos").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            rolar_para("#tabela");
            limpa_campos();
            break;
        case 2:
            $("#operacao").val("load");
            formdata = $("#form1").serialize();
            limpa_campos();
            submit_form(destino);
            break
    }
    execute_cod = 0;
}

function consulta_grupos(){
    $("#operacao").val("consulta_grupos");
    formdata = $("#form1").serialize();
    submit_form(destino);
}

function editar(cod){
    $("#CodTrilha").val(cod);
    $("#operacao").val("editar");
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}

function salvar(){
    $("#operacao").val("salvar");
    formdata = $("#form1").serialize();
    $("#msg_dialogo").html("As informações estão corretas?");
    $("#popup_dialogo").dialog("open");
}

function sql(cod){
    $("#CodTrilha").val(cod);
    $("#operacao").val("sql");
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}

function cancelar(){
    limpa_campos();
    rolar_para("#tabela");
}

function rolar_para(elemento) {
    $('html, body').animate({
      scrollTop: $(elemento).offset().top - 200
    }, 300);
}

function limpa_campos(){
    $("#id").val("");
    $("#tipo").val('-1');
    $("#grupo").html('<option value="-1"></option>');
    $("#data_inicial").val("");
    $("#data_final").val("");
    $("#data_trilha").val("");
    $("#criterios").val("");
    $("#CodTrilha").val('0');
}

function incluir(cod){
    $("#CodTrilha1").val(cod);
    $("#lbl_trilha").text("TRILHA: " + cod + " - " + $("#nt_" + cod).text());
    $('#form_upload').dialog('open');
    return false;
}

function anexar(cod){
    $("#CodTrilha2").val(cod);
    $("#lbl_trilha1").text("TRILHA: " + cod + " - " + $("#nt_" + cod).text());
    $('#form_anexo').dialog('open');
    return false;
}


function liberar(cod){
    var qtd = $("#qtd_" + cod).text();
    if(qtd == "0"){
        $("#msg_alerta").html("Esta trilha não possui nenhum registro cadastrado.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    
    $("#CodTrilha").val(cod);
    $("#operacao").val("liberar");
    formdata = $("#form1").serialize();
    $("#msg_dialogo").html("Gostaria de enviar os " + qtd + " registros da trilha para homologação?");
    $("#popup_dialogo").dialog("open");
    return false;
}

function excluir(cod){
    
    $("#CodTrilha").val(cod);
    $("#operacao").val("excluir");
    formdata = $("#form1").serialize();
    
    var qtd = $("#qtd_" + cod).text();
    if(qtd == "0"){
        $("#msg_dialogo").html("Você tem certeza que gostaria de EXCLUIR a trilha selecionada?<br><span style='color: red;'>AVISO:</span> Esta operação não poderá ser desfeita.");
        $("#popup_dialogo").dialog("open");
    }else{
        $("#msg_dialogo").html("Você tem certeza que gostaria de excluir os " + qtd + " registros da trilha selecionada?<br><span style='color: red;'>AVISO:</span> Esta operação não poderá ser desfeita.");
        $("#popup_dialogo").dialog("open");
    }
    
    return false;
}

function visualizar_anexos(cod){
    $("#CodTrilha").val(cod);
    $("#operacao").val("visualizar_anexos");
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
    $("#popup_anexos").dialog("open");
}

function excluir_anexo(CodAnexo, CodTrilha){
    $("#CodTrilha").val(CodTrilha);
    $("#CodAnexo").val(CodAnexo);
    $("#operacao").val("excluir_anexo");
    formdata = $("#form1").serialize();
    $("#msg_dialogo").html("<span style='color: red;'>Você tem certeza que gostaria de excluir este arquivo?</span>");
    $("#popup_dialogo").dialog("open");
    return false;
}

function efetuar_carga(CodTrilha){
    $("#CodTrilha2").val(CodTrilha);
    document.getElementById("form2").action = "importar_continuo.php";
    document.getElementById("form2").submit();
    return false;
}


function server(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "registros_excluidos":
                retorno_registros_excluidos(retorno);
                break;
           
            case "rejeitar_registro":
                execute_cod = 2;
                $("#msg_sucesso").html("Registro Rejeitado com Sucesso.");
                $("#popup_sucesso").dialog("open");
             break;
            case "observacao_cge":
                retorno_info(retorno);
                break;
            case "gravar_observacao_cge":
                retorno_observacao_cge(retorno);
                break;
            case "listar_anexos":
                retorno_visualizar_anexos(retorno);
                break;
            case "upload":
                retorno_subir_arquivo(retorno);
                break;
            case "upload_trilha":
                retorno_subir_arquivo(retorno);
                break;
            case "upload_trilha_continuo":
                retorno_subir_arquivo_continuo(retorno);
                break;
            case "justificar_inconsistencia":
                retorno_justificar_inconsistencia(retorno);
                break;
            case "consultar_justificativa":
                retorno_consultar_justificativa(retorno);
                break;
            case "concluir_analise":
                retorno_analise(retorno);
                break;
            case "salvar_analise":
                retorno_analise(retorno);
                break;    
            case "recusar_trilha":
                execute_cod = 1;
                $("#msg_sucesso").html("Trilha recusada com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "enviar_trilha":
                execute_cod = 1;
                
                var msg = "Trilha distribuída aos órgãos com sucesso.<br>";
                
                if(retorno.erro_email != ""){
                    msg = msg + retorno.erro_email;
                }
                
                if(retorno.alerta_email != ""){
                    msg = msg + retorno.alerta_email;
                }
                
                msg = msg + retorno.mensagem_email;
                
                $("#msg_sucesso").html(msg);
                $("#popup_sucesso").dialog("open");
                break;
            case "enviar_registros":
                    execute_cod = 1;
                    
                    var msg = "Registros distribuídos aos órgãos com sucesso.<br>";
                    
                    if(retorno.erro_email != ""){
                        msg = msg + retorno.erro_email;
                    }
                    
                    if(retorno.alerta_email != ""){
                        msg = msg + retorno.alerta_email;
                    }
                    
                    msg = msg + retorno.mensagem_email;
                    
                    $("#msg_sucesso").html(msg);
                    $("#popup_sucesso").dialog("open");
                    break;
            case "enviar_registro":
                        execute_cod = 2;
                        
                        var msg = "Registro distribuído ao órgão com sucesso.<br>";
                        
                        if(retorno.erro_email != ""){
                            msg = msg + retorno.erro_email;
                        }
                        
                        if(retorno.alerta_email != ""){
                            msg = msg + retorno.alerta_email;
                        }
                        
                        msg = msg + retorno.mensagem_email;
                        
                        $("#msg_sucesso").html(msg);
                        $("#popup_sucesso").dialog("open");
                      //  execute();
                        break;
            case "rejeitar_registro":
                execute_cod = 2;
                $("#msg_sucesso").html("Registro Rejeitado com Sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "reincluir_registro":
                execute_cod = 2;
                $("#msg_sucesso").html("Registro Re-incluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "excluir_anexo":
                execute_cod = 2;
                $("#msg_sucesso").html("Arquivo excluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "excluir_arquivo":
                execute_cod = 2;
                $("#popup_anexos").dialog("close");
                $("#msg_sucesso").html("Arquivo excluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "analisar_justificativa":
                analisar_justificativa_retorno(retorno);
                break;
            case "salvar_validacao":
                retorno_salvar_validacao(retorno);
                break;
            case "andamento":
                consulta_andamento_retorno(retorno);
                break;
            case "consulta_justificativas":
                consulta_justificativas_retorno(retorno);
                break;
            case "solicitar_alteracao_justificativa":
                retorno_alteracao(retorno);
                break;
            case "analisar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "gravar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "aceitar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "negar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "salvar_instrucao":
                retorno_salvar_instrucao();
                break;
            case "consulta_complementar":
                retorno_acoes_complementares(retorno);
                break;
            case "acoes_complementares":
                retorno_salvar_complementar(retorno);
                break;
            case "ordem_servico":
                retorno_consulta_ost(retorno);
                break;
            case "mesclar_consulta":
                retorno_mesclar(retorno);
                break;
            case "mesclar_save":
                retorno_mesclar_save(retorno);
                break;    
            case "consultar_msg":
                retorno_visualizar_mensagens(retorno);
                break;
            case "equipe_msg":
                retorno_cadastrar_msg(retorno);
                break;
            case "cadastrar_msg":
                retorno_salvar_msg(retorno);
                break;
            case "salvar_msg":
                retorno_salvar_msg(retorno);
                break;
            case "editar_msg":
                retorno_alterar_msg(retorno);
                break;
            case "excluir_msg":
                retorno_excluir_msg(retorno);
                break;
            case "consultar_dist":
                retorno_distribuir_registro(retorno);
                break;
            case "vincular_dist":
                retorno_vincular_registro(retorno);
                break;
            case "concluir_pre_analise":
                retorno_concluir_pre_analise(retorno);
                break;
            case "auditoria_consulta":
                retorno_auditoria_consulta(retorno);
                break;
            case "auditoria_pre_analise":
                retorno_auditoria_pre_analise(retorno);
                break;
            case "redistribuir_consulta":
                retorno_redistribuir_consulta(retorno);
                break;
             case "redistribuir_para":
                retorno_redistribuir_para(retorno);
                break;  
            case "salvar_mensagem_trilha":
                retorno_salvar_mensagem_trilha(retorno);
                break;
            case "alterar_mensagem_trilha":
                retorno_alterar_mensagem_trilha(retorno);
                break;
            case "excluir_mensagem_trilha":
                retorno_excluir_mensagem_trilha(retorno);
                break;
            case "mostrar_observacao":
                retorno_alterar_observacao(retorno);
                break;
            case "ocultar_observacao":
                retorno_alterar_observacao(retorno);
                break;    
            default:
                alert("js: Operação Inválida --> ");
        };
    }
}

function consultar_continuo_homologacao(cod){
    //SGTA 173
    var acao = $("#acao").val();
     
    $("#CodTrilha").val(cod);
 
    $("#operacao").val("consulta_detalhe");
    $("#acao").val("consulta");
    $("#Orgao").val($("#orgao").val());
    $("#WhereContinuo").val('sim');
    $("#filtro").val('');
    formdata = $("#form1").serialize();
    $("#acao").val(acao);
    submit_form(destino);
    return false;
}

function consultar(cod,filtro){

    //SGTA 173
    $("#CodTrilha").val(cod);
    $("#CodTrilha2").val(cod);
    $("#operacao2").val("consulta_detalhe");
    $("#operacao").val("consulta_detalhe");
    $("#acao").val("consulta");
    $("#Orgao").val("0");
    $("#filtro").val(filtro);
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}



function retornar_registro_trilha(cod,CodTrilha,CodOrgao ){

    //SGTA 173
    $("#CodTrilha").val(CodTrilha);
    $("#CodRegistro").val(cod);
    $("#CodOrgao").val(CodOrgao);
    $("#operacao").val("retornar_registro");
    $("#acao").val("retornar_registro");

    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}