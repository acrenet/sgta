<?php

    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if(!($perfil == 1 || $perfil == 2 || $perfil == 5)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $operacao = "ver_permissoes";
    
?>
<link href="permissoes.css" rel="stylesheet" type="text/css"/>
<script src="permissoes.js" type="text/javascript"></script>
<br>
<div class="container">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4>Permissões de Acesso por Grupo de Trilha.</h4>
            </div>
            <div class="panel-body">
                
                <form id="form1" class="form-horizontal" method="post">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Órgão/Unidade:</label>
                        <div class="col-sm-9">
                            <select name="lst_orgaos" id="lst_orgaos" class="form-control" onchange="ver_permissoes();">
                                <option value="0"></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-3" for="pwd">Área de Trilhas:</label>
                      <div class="col-sm-5"> 
                            <select name="lst_tipos" id="lst_tipos" class="form-control" onchange="ver_permissoes();">
                                <option value="0"></option>    
                            </select>
                      </div>
                    </div>
                    <input type="hidden" name="operacao" id="operacao" value="" />
                    <input type="hidden" name="permitidos" id="permitidos" value=""/>
                    <input type="hidden" name="negados" id="negados" value=""/>
                </form>
                
                <P style="text-align: center;">Arraste os itens entre as listas para autorizar ou desautorizar o acesso.</P>
                
                <div class="row">
                    <div class="col-sm-6" style="text-align: center; font-weight: bold;" class="ui-sta">
                        SERVIDORES SEM ACESSO
                    </div>
                    <div class="col-sm-6" style="text-align: center; font-weight: bold;">
                        SERVIDORES COM ACESSO
                    </div>
                </div> 
                
                <div class="row" id="div_listas" style="display: none; font-size: 80%;">
                    <div class="col-sm-6">
                        <ul id="sortable1" class="connectedSortable">
                            
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul id="sortable2" class="connectedSortable">
                            
                        </ul>
                    </div>
                </div> 
                <br>
               
            </div>
        </div>
    </div>
</div>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");