<?php

$__servidor = true;
$operacao = "";
require_once("../../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../../code/funcoes.php");

require_once("../../../obj/trilhas.php");
require_once("../../../obj/orgaos.php");
require_once("../../../obj/registros.php");
require_once("../../../obj/conexao.php");
require_once("../../../obj/usuario.php");

$obj_orgaos = new  orgaos();
$obj_trilhas = new trilhas();
$obj_registros = new registros();
$obj_conexao = new conexao();
$obj_usuario = new usuario();


if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    $obj_trilhas = new trilhas();
    
    switch ($array["operacao"]) {
        case "upload":                    //--------------------------------------------------------------------------------------------------
            
            if(!isset($_FILES['uploadfile-0'])){
                $array['erro'] = "Arquivo não recebido.";
                retorno();
            }
                        
            $CodTrilha = $_POST['CodTrilha'];
            $NomeTrilha = $_POST['NomeTrilha'];
            
            $tmpFilePath = $_FILES['uploadfile-0']['tmp_name'];
            $fileName = $_FILES['uploadfile-0']['name'];
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            
            $fileName = $NomeTrilha . "." . $ext;
            
            $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/carga/" . $CodTrilha;
          //  die( $targetDir);
            
            $retorno = true;
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }                                    
            
            $newFilePath = $targetDir . "/" . $fileName;
            
            if (file_exists($newFilePath)) {
                unlink($newFilePath);
                //$array['erro'] = "Já existe um arquivo com este nome.<br>Renomeie o arquivo.";
                //retorno();
            }

            if(move_uploaded_file($tmpFilePath, $newFilePath) == false) {
                $array['erro'] = "Não foi possível realizar o upload do arquivo: $newFilePath.  ". print_r($_FILES);
            }
            
            retorno();
            break;
        case "anexo":
            
            if(!isset($_FILES['uploadfile-0'])){
                $array['erro'] = "Arquivo não recebido.";
                retorno();
            }
                        
            $CodTrilha = $_POST['CodTrilha'];
            $Restrito = $_POST['Restrito'];
            $DescricaoAnexo = $_POST['DescricaoAnexo'];
            $Usuario = $_SESSION['sessao_id'];
            $Etapa = "Criação";
            
            $tmpFilePath = $_FILES['uploadfile-0']['tmp_name'];
            $fileName = $_FILES['uploadfile-0']['name'];
            $fileName = sanitizeString($fileName);
            $fileName = str_replace(",","",$fileName);
            $fileName = str_replace(" ","_",$fileName);
            
            $targetDir = $_SERVER['DOCUMENT_ROOT']."/sgta/trilhas/intra/sgta/trilhas/" . $CodTrilha; 
           // die(  $targetDir);
            
            $retorno = true;
            
            $DescricaoAnexo = trim($DescricaoAnexo);
            if($DescricaoAnexo == ""){
                $array['erro'] = "É obrigatório informar a descrição do arquivo.";
                retorno();
            }
            
            $id = $obj_trilhas->upload_arquivo($CodTrilha, $fileName, $DescricaoAnexo, $Usuario, $Etapa, $Restrito);
            if($obj_trilhas->erro != ""){
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }                                    
            
            $targetDir = $targetDir."/".$id;
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }        
            
            $newFilePath = $targetDir . "/" . $fileName;
            
            if (file_exists($newFilePath)) {
                //unlink($newFilePath);
                $array['erro'] = "Já existe um arquivo com este nome.<br>Renomeie o arquivo.";
                retorno();
            }
            
            if(move_uploaded_file($tmpFilePath, $newFilePath) == false) {
                $array['erro'] = "Não foi possível realizar o upload do arquivo: $newFilePath.  ". print_r($_FILES);
            }
            
            $array['resposta'] = "Arquivo anexado com sucesso.";
            
            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}

function sanitizeString($str) {
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
    //$str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
    //$str = preg_replace('/[^a-z0-9]/i', '_', $str);
    //$str = preg_replace('/_+/', '_', $str); // ideia do Bacco :)
    return $str;
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}