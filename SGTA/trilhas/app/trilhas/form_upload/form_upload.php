<?php


?>

<link href="form_upload/form_upoload.css" rel="stylesheet" type="text/css"/>
<script src="form_upload/form_upload.js" type="text/javascript"></script>

<div id="form_upload" title="Upload do Arquivo xlsx." style="display:none; font-size: 80%;">  
    <form name="form_upload1" id="form_upload1" action="#" method="POST">
        <div>
            <label id="lbl_trilha" style="font-size: 130%; margin-top: 10px; margin-bottom: 10px;"></label>
        </div>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <span class="btn btn-primary btn-file"><span class="fileupload-new">Selecione o Arquivo &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>
                <span class="fileupload-exists">Alterar &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>         <input id="uploadfile" name="uploadfile" type="file" onchange="validar_arquivo()"/></span>
            <span class="fileupload-preview"></span>
            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
        </div>
        <input type="hidden" name="CodTrilha" id="CodTrilha1" value="0" />
        <hr>
        <div style="text-align: right; font-size: 120%;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#form_upload").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
            <button type="button" class="btn btn-success" onclick='subir_arquivo();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
        </div>
    </form>
</div>

<div id="form_anexo" title="Anexar Arquivo" style="display:none; font-size: 80%; overflow: hidden;">  
    
    <div>
        <label id="lbl_trilha1" style="font-size: 130%; margin-top: 10px; margin-bottom: 10px;"></label>
    </div>
    
    <form name="form_anexo1" id="form_anexo1" action="#" method="POST" class="form-horizontal" style="margin: 0px; padding: 0px;">
        
        <div class="form-group" style="margin-bottom: 0px;">
            <label class="control-label col-sm-3" for="DescricaoArquivo">Descrição do arquivo:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="DescricaoAnexo" name="DescricaoAnexo" maxlength="300">
            </div>
        </div>
        
        <div class="form-group"> 
            <div class="col-sm-offset-3 col-sm-9">
                <div class="checkbox">
                    <label><input type="checkbox" id="Restrito"> Restrito ao TCE (não será visível a outros órgãos).</label>
                </div>
            </div>
        </div>
        
        <div class="form-group"> 
            <div class="col-sm-offset-3 col-sm-9">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-primary btn-file"><span class="fileupload-new">Selecione o Arquivo &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>
                        <span class="fileupload-exists">Alterar &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>         <input id="uploadfile2" name="uploadfile" type="file" onchange="validar_arquivo()"/></span>
                    <span class="fileupload-preview"></span>
                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                </div>
            </div>
        </div>
        
                
        <input type="hidden" name="CodTrilha" id="CodTrilha2" value="0" />
        <hr>
        <div style="text-align: right; font-size: 120%;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#form_anexo").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
            <button type="button" class="btn btn-success" onclick='subir_anexo();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
        </div>
    </form>
</div>

