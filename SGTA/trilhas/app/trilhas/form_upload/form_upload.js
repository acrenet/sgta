
$(document).ready(function(){ 
    popup_form_upload();
});

function resposta_up(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "upload":
                execute_cod = 2;
                document.getElementById("form_upload1").reset();
                $("#popup_pergunta").dialog("close");
                $("#form_upload").dialog("close");
                exibir();
                
                break;
            case "anexo":
                execute_cod = 2;
                document.getElementById("form_anexo1").reset();
                $("#popup_pergunta").dialog("close");
                $("#form_anexo").dialog("close");
                $("#msg_sucesso").html(retorno.resposta);
                $("#popup_sucesso").dialog("open");
                
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function popup_form_upload(){
    $("#form_upload").dialog({
        autoOpen: false,
        resizable: false,
        width: 630, 
        modal: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
    $("#form_anexo").dialog({
        autoOpen: false,
        resizable: false,
        width: 750, 
        modal: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

!function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)


function validar_arquivo(cod){
    
    var upl;
    
    if(cod == 1){
        upl = $("#uploadfile");
    }else{
        upl = $("#uploadfile2");
    }
    
    var files = upl[0].files;
    var log = "";
    var erro  = false;
    var msg = "";
    var ext = "";

    for (var i = 0; i < files.length; i++){
        log = log + '"' + files[i].name + '"  ';
        if(files[i].size > 10240000){
            erro = true;
            msg = msg + "O arquivo ''" + files[i].name + "'' ultrapaça o limite de 10 megabytes.<br>";
        }

        ext = files[0].name.split('.').pop();
        if(cod == 1){
             var str = ",xlsx";
            if(str.indexOf(ext) == -1){
                erro = true;
                msg = msg + "O arquivo ''" + files[i].name + "'' possui um formato não permitido.<br>É permitido apenas arquivos do excel ''xlsx''.";
            }
        }else{
            var str = "txt,pdf,xls,xlsx,doc,docx,ods,odt";
            if(str.indexOf(ext) == -1){
                erro = true;
                msg = msg + "O arquivo ''" + files[i].name + "'' possui um formato não permitido.<br>São permitidos arquivos pdf, xls, xlsx, doc, docx, ods e odt.";
            }
        }
            
    }

    if(erro == true){
        $("#msg_erro").html(msg);
        $("#popup_erro").dialog("open");
        return false;
    }else{
        return files.length;
    }

}

function subir_arquivo(){
    var resposta = validar_arquivo(1);
    if (resposta === false){
        return false;
    }else if(resposta == 0){
        $("#msg_alerta").html("Nenhum arquivo selecionado.<br>Selecione um arquivo primeiro.");
        $("#popup_alerta").dialog("open");
        return false;
    }else{

        var data = new FormData();
        jQuery.each(jQuery('#uploadfile')[0].files, function(i, file) {
            data.append('uploadfile-'+i, file);
        });

        data.append("CodTrilha", $("#CodTrilha").val());
        data.append("NomeTrilha", $("#NomeTrilha").val());
        data.append("operacao","upload");

        formdata = data;
        
        $("#msg_pergunta").text("Gostaria de subir o arquivo?");
        $("#popup_pergunta").dialog("open");
    }
}

function subir_anexo(){
    var resposta = validar_arquivo(2);
    if (resposta === false){
        return false;
    }else if(resposta == 0){
        $("#msg_alerta").html("Nenhum arquivo selecionado.<br>Selecione um arquivo primeiro.");
        $("#popup_alerta").dialog("open");
        return false;
    }else{

        var data = new FormData();
        jQuery.each(jQuery('#uploadfile2')[0].files, function(i, file) {
            data.append('uploadfile-'+i, file);
        });

        data.append("CodTrilha", $("#CodTrilha2").val());
        data.append("operacao","anexo");
        data.append("Restrito",document.getElementById('Restrito').checked);
        data.append("DescricaoAnexo", $("#DescricaoAnexo").val());

        formdata = data;
        
        $("#msg_pergunta").text("Gostaria de anexar o arquivo?");
        $("#popup_pergunta").dialog("open");
    }
}

function pergunta_ok(){
    submit_file("form_upload/server.php");
}
