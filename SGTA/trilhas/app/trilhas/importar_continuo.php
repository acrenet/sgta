<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    require_once '../../code/funcoes.php';
    require_once '../../obj/trilhas.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if($perfil != 1){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    if(!isset($_POST['CodTrilha'])){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
        
    }
    
    $CodTrilha = $_POST['CodTrilha'];
    
    $obj_trilhas = new trilhas();
    $obj_trilhas->consulta_trilha($CodTrilha);
    if($obj_trilhas->erro != ""){
        echo $obj_trilhas->erro;
        die();
    }

    $query = $obj_trilhas->query;
    $row = mysqli_fetch_array($query);

    $tabela_ = $row['TabelaDetalhe'];
    $NomeTrilha = $row['NomeTrilha'];
    $TabelaDetalhe = $row['TabelaDetalhe'];
    
    
    if (file_exists("../registros/$tabela_/campos.php")) {
        require_once "../registros/$tabela_/campos.php";
    }else{
        require_once "../registros/$tabela_/$tabela_"."_carga.php";
    }
    
   
     
?>
<script src="importar.js" type="text/javascript"></script>

<form name="form1" id="form1" method="POST">
    <input type="hidden" id="operacao" name="operacao" value="load" />
    <input type="hidden" id="CodTrilha" name="CodTrilha" value="<?php echo $CodTrilha; ?>" />
    <input type="hidden" id="NomeTrilha" name="NomeTrilha" value="<?php echo $TabelaDetalhe; ?>" />
</form>

<div class="container-fluid">
    <br>
    
    <div class="container">
        
        <div class="panel panel-primary">
            <div class="panel-heading" style="font-size: 150%;">Efetuar Carga em Trilha</div>
            <div class="panel-body">
                <h3>Trilha: <?php echo $NomeTrilha; ?>.</h3>
                <h4>Colunas do arquivo de importação requerido para a trilha:</h4>
            <div class="well well-sm">

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>COLUNAS</th>
                            <th>DESCRIÇÃO</th>
                            <th>FORMATO</th>
                            <th style="text-align: right;">TAMANHO</th>
                        </tr>
                    </thead>
                    <tbody id="tb_colunas">

                    </tbody>
                </table>
            </div>
            </div>
        </div>
           
        <input type="button" class="btn btn-primary" value="Carregar Arquivo" onclick="$('#form_upload').dialog('open');" />
        <input id="btn_carga" type="button" class="btn btn-info" value="Efetuar Carga no Sistema" onclick="execute_();" style="display: none;" />
    </div>
    
    <br>
    <div id="div_info">
            
    </div>
    <br>
    <h3>Conteúdo do Arquivo:</h3>
    <div>
        <table id="tbl_excel" class='table table-striped table-bordered table-hover table-condensed' style="font-size: 80%; display: none;">
        

        </table>
    </div>
    <br>
</div>


<?php
    include("form_upload/form_upload.php");

  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
