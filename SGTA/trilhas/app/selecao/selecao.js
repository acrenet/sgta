var execute_cod = 0;
var destino = "server.php";
var minha_tabela;

$(document).ready(function(){ 
    
    $( document ).tooltip({
        track: true,
        tooltipClass: "custom-tooltip-styling"
    });

    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
    
});

var ordem = [
                [0, "desc" ]
            ];

var data = [
                {"sType": "string"},
                {"sType": "string"},
                {"sType": "string"},
                {"sType": "date-uk"},
                {"sType": "date-uk"},
                {"sType": "string"},
                {"sType": "string"},
                {"sType": "string"},
                {"sType": "string"},
                {"sType": "string"}
            ];

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                
                $("#tb_trilhas").html(retorno.html);
                datatable_("tbl_trilhas", 10, ordem, 50, data);
                $("#tbl_trilhas").slideDown();

                $("#orgao").html(retorno.orgaos);
                
                if(retorno.SelecionarOrgao == 0){
                    //carregar as trilhas do órgão
                }
                
                break;
            case "escolher_trilhas":
                
                minha_tabela.destroy();
                
                $("#tb_trilhas").html(retorno.html);
                datatable_("tbl_trilhas", 10, ordem, 50, data);    
                
                break;
            case "consulta_detalhe":    
                $("#detalhe").val(retorno.detalhe);
                $("#NomeTrilha").val(retorno.NomeTrilha);
                $("#acao2").val(retorno.acao);
                document.getElementById("form3").submit();
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            
            break;
        case 2:
            
            break
    }
    execute_cod = 0;
}

function escolher_trilhas(){
    $("#CodUnidade").val($("#orgao").val());
    $("#operacao").val("escolher_trilhas");
    formdata = $("#form1").serialize();
    submit_form(destino);
}

function consultar(cod){
  
    var acao = $("#acao").val();
    
    $("#CodTrilha2").val(cod);
    $("#operacao2").val("consulta_detalhe");
    $("#acao").val("consulta");
    $("#Orgao").val($("#orgao").val());
    $("#WhereContinuo").val('nao');
    formdata = $("#form3").serialize();
    $("#acao").val(acao);
    submit_form(destino);
    return false;
}

function consultar_continuo(cod){
    //SGTA 173
 
    var acao = $("#acao").val();
     
    $("#CodTrilha2").val(cod);
    $("#operacao2").val("consulta_detalhe");
    $("#acao").val("consulta");
    $("#Orgao").val($("#orgao").val());
    $("#WhereContinuo").val('sim');

    document.getElementById('WhereContinuo').value ='sim'
    $("#filtro").val('rascunho');
    formdata = $("#form3").serialize();
    $("#acao").val(acao);
    submit_form(destino);
    return false;
}




function relatorio(cod){
    $("#CodTrilha3").val(cod);
    document.getElementById("form4").submit();
    return false;
}

function grafico(cod){
    $("#CodTrilha4").val(cod);
    document.getElementById("form5").submit();
    return false;
}

function graficoAjax(cod){
    
    $.post( "http://"+window.location.host+"/trilhas/rpt/trilhas/graficos.php", {CodTrilha:cod}).done(function( cadastro ) {

        $("#res_grafico").html(cadastro);

    });
    return false;

}