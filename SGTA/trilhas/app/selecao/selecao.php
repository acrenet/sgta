<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
	
    $CodPerfil = $_SESSION['sessao_perfil'];
    $CodOrgao = $_SESSION['sessao_orgao'];
    $CPF = $_SESSION['sessao_id'];
    
    if($CodPerfil < 5){
        $SelecionarOrgao = 1;
    }else{
        $SelecionarOrgao = 0;
    }
    
    if(isset($_GET['p'])){
        $acao = $_GET['p'];
        $css = "";
        $css2 = "";
    }else{
        $acao = "pré-análise";
        $titulo = "(Homologação)";
        $css = "display: none;";
        $css2 = "display: none;"; 
    }
    
    if($acao == 1){
        $acao = "consulta";
        $titulo = "(Consulta)";
    }else if($acao == 2){
        $titulo = "(Justificar Inconsistências)";
        $acao = "análise";
    }else if($acao == 3){
        $titulo = "(Auditoria)";
        $acao = "validação";
    }
    
    $operacao = "load";
    $CodTrilha = "0";
    
    if($acao == "pré-análise"){
        if($CodPerfil > 3){
            Header("Location: /trilhas/app/inicio/negado.php");
            die();
        }
    }else if($acao == "análise"){
        if($CodPerfil != 5 && $CodPerfil != 6 && $CodPerfil != 1){
            Header("Location: /trilhas/app/inicio/negado.php");
            die();
        }
    }else if($acao == "validação"){
        if($CodPerfil > 3){
            Header("Location: /trilhas/app/inicio/negado.php");
            die();
        }
    }
    
    $_titulo = "Trilhas - Seleção";
    
?>

<script src="selecao.js" type="text/javascript"></script>

<form name="form1" id="form1" method="POST">
    <input type="hidden" name="CodPerfil" id="CodPerfil" value="<?php echo $CodPerfil; ?>" />
    <input type="hidden" name="CodOrgao" id="CodOrgao" value="<?php echo $CodOrgao; ?>" />
    <input type="hidden" name="CodUnidade" id="CodUnidade" value="0" />
    <input type="hidden" name="CodTrilha" id="CodTrilha" value="<?php echo $CodTrilha; ?>" />
    <input type="hidden" name="CPF" id="CPF" value="<?php echo $CPF; ?>" />
    <input type="hidden" name="operacao" id="operacao" value="<?php echo $operacao; ?>" />
    <input type="hidden" name="acao" id="acao" value="<?php echo $acao; ?>" />
    <input type="hidden" name="SelecionarOrgao" id="SelecionarOrgao" value="<?php echo $SelecionarOrgao; ?>" />    
    <input type="hidden" name="WhereContinuo" id="WhereContinuo" value="<?php echo @$_REQUEST['WhereContinuo']; ?>" />
</form>
<br>
<div class="container-fluid">
    
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Seleção de Trilha <?php echo $titulo; ?>.</h4></div>
            <div class="panel-body">
                
                <div class="container">
                    <form id="form2" class="form-horizontal" role="form" method="post" style="<?php echo $css; ?>">

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="tipo">Órgão/Unidade Adm:</label>
                            <div class="col-sm-10">
                                <select name="orgao" id="orgao" class="form-control" onchange="escolher_trilhas();">
                                    <option value="-1"></option>
                                </select>
                            </div>
                        </div>

                    </form>
                </div>
                
                <div style="font-size: 85%;">
                    <table id="tbl_trilhas" class="display" width="100%" style="<?php echo $css2; ?>">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Área</th>
                                <th>Trilha</th>
                                <th style="text-align: center;">Data Inicial</th>
                                <th style="text-align: center;">Data Final</th>
                                <th style="text-align: center;">Registros</th>
                                <th style="text-align: center;">Homologados</th>
                                <th style="text-align: center;">Excluídos</th>
                                <th style="text-align: right;">Dias da Liberação</th>
                                <th>Status</th>
                                <th style="width: 36px;"></th>
                            </tr>
                        </thead>
                        <tbody id="tb_trilhas">

                        </tbody>
                        <tfoot style="display: none;">
                            <tr>
                                <th>Id</th>
                                <th>Área</th>
                                <th>Trilha</th>
                                <th style="text-align: center;">Data Inicial</th>
                                <th style="text-align: center;">Data Final</th>
                                <th style="text-align: center;">Registros</th>
                                <th style="text-align: center;">Homologados</th>
                                <th style="text-align: center;">Excluídos</th>
                                <th style="text-align: right;">Dias da Liberação</th>
                                <th>Status</th>
                                <th style="width: 36px;"></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<form name="form3" id="form3" action="../registros/exibir.php" method="POST">
    <input type="hidden" name="operacao" id="operacao2" value="consulta_detalhe" />
    <input type="hidden" name="Orgao" id="Orgao" value="" />
    <input type="hidden" name="CodTrilha" id="CodTrilha2" value="" />
    <input type="hidden" name="NomeTrilha" id="NomeTrilha" value="" />
    <input type="hidden" name="detalhe" id="detalhe" value="" />
    <input type="hidden" name="acao" id="acao2" value="<?php echo $acao; ?>" />
    <input type="hidden" name="filtro" id="filtro" value="" />
    <input type="hidden" name="WhereContinuo" id="WhereContinuo" value="" />
</form>

<form name="form4" id="form4" target="_blank" action="../../rpt/trilhas/acompanhamento.php" method="POST">
    <input type="hidden" name="CodTrilha" id="CodTrilha3" value="" />
</form>

<form name="form4" id="form5" target="_blank" action="../../rpt/trilhas/graficos.php" method="POST">
    <input type="hidden" name="CodTrilha" id="CodTrilha4" value="" />
</form>


<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');