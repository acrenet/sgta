<?php

    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if(($perfil > 4)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Cruzamento entre Trilhas";
    
    $cpf = "";
    $nome = "";
    
    if(isset($_POST['parametro'])){
        $parametro = $_POST['parametro'];
        if($parametro == "cpf"){
            $cpf = trim($_POST['campo']);
        }else{
            $nome = trim($_POST['campo']);
        }
    }
    
?>
<script src="consulta_individual.js" type="text/javascript"></script>
<br>
<div class="container">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4>Cruzamento entre Trilhas - Consulta Individual</h4>
            </div>
            <div class="panel-body">
                
                <form id="form1" name="form1" method="POST" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cpf">CPF/CNPJ/Num. Processo:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="cpf" name="cpf" placeholder="" value="<?php echo $cpf; ?>">
                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-default" onclick="return consultar();">Consultar</button>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cpf">Nome/Razão Social:<br>(pode ser somente parte)</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="nome" name="nome" placeholder="" value="<?php echo $nome; ?>">
                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-default" onclick="return consultar_nome();">Consultar</button>
                        </div>
                    </div>
                    
                    <input type="hidden" name="operacao" value="consultar" />
                    <input type="hidden" name="tipo" id="tipo" value="" />
                    <input type="hidden" name="detalhe" id="detalhe" value='<?php echo @$_REQUEST['detalhe']; ?>' />
                </form>
                
                <table id="tbl_trilhas" class="display" width="100%" style="display: none; font-size: 90%;">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Trilha</th>
                            <th>CPF/CNPJ/Processo</th>
                            <th>Nome/Descrição</th>
                            <th>Órgão</th>
                            <th style="text-align: center;">Dt Publicação</th>
                            <th>Status Registro</th>
                            <th>Resultado Final</th>
                            <th style="width: 25px"></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Trilha</th>
                            <th>CPF/CNPJ/Processo</th>
                            <th>Nome/Descrição</th>
                            <th>Órgão</th>
                            <th style="text-align: center;">Dt Publicação</th>
                            <th>Status Registro</th>
                            <th>Resultado Final</th>
                            <th style="width: 25px"></th>
                        </tr>
                    </tfoot>
                    <tbody id="tb_trilhas">
                        
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
    
<form name="form2" id="form2" action='../registros/exibir.php' method="POST" target="_blank">
    <input type="hidden" name="operacao" id="operacao" value="" />
    <input type="hidden" name="filtro" id="filtro" value="" />
    <input type="hidden" name="Orgao" id="Orgao" value="" />
    <input type="hidden" name="CodTrilha" id="CodTrilha" value="" />
    <input type="hidden" name="NomeTrilha" id="NomeTrilha" value="" />
    <input type="hidden" name="detalhe" id="detalhe" value="" />
    <input type="hidden" name="acao" id="acao" value="consulta" />
</form>

<?php
    // pagemaincontent recebe o conteudo do buffer
    $pagemaincontent = ob_get_contents(); 

    // Descarta o conteudo do Buffer
    ob_end_clean(); 

    //Include com o Template
    include("../../master/master.php");
    include('../../master/datatable.php');