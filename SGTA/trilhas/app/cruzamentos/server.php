<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");
require_once("../../obj/conexao.php");
require_once("../../obj/orgaos.php");


$objcon = new conexao();
$objorgao = new orgaos();

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    switch ($array["operacao"]) {
        case "consultar":
            $cpf_cnpj = $_POST['cpf'];
            $nome = $_POST['nome'];
            $tipo = $_POST['tipo'];
            
            if($tipo == "1"){
                $busca = ltrim($cpf_cnpj, '0');
            }else{
                $busca = $nome;
            }
                
            require_once("../../obj/usuario.php");
            $objusu = new usuario();
            
            $objusu->consulta_autorizacoes($_SESSION['sessao_id'], -1);
            if($objusu->erro != ""){
                throw new Exception($objusu->erro);
            }
            
            $autorizacoes = $objusu->array;
            // andre aqui post detalhe - ver se necessita 
            require_once("../registros/cruzamentos.php");
            
            $html = "";
            
            if($tipo == "1"){
                
                for ($index = 0; $index < count($sql); $index++){
                    $query = $objcon->select($sql[$index]);
                    if($objcon->erro != ""){
                        throw new Exception($objcon->erro);
                    }
                    while ($row = mysqli_fetch_assoc($query)){
                        $tab[] = $row;
                    }
                    $query = "";
                }
            }else{
                $sqle = "";
                for ($index = 0; $index < count($sql); $index++){
                    $sqle = $sqle . "<br><br>".$sql2[$index];
                }
                //throw new Exception($sqle);
                for ($index = 0; $index < count($sql2); $index++){
                    $query = $objcon->select($sql2[$index]);
                    if($objcon->erro != ""){
                        throw new Exception($objcon->erro);
                    }
                    while ($row = mysqli_fetch_assoc($query)){
                        $tab[] = $row;
                    }
                    $query = "";
                }
            }
            
            $m = "";
                        
            if(isset($tab)){
                for ($index = 0; $index < count($tab); $index++){
                    
                    if($m != $tab[$index]['CodRegistro'].$tab[$index]['CodOrgao'].$tab[$index]['CPF_CNPJ_Proc'] && $tab[$index]['StatusRegistro'] != "rascunho"){
                        $m = $tab[$index]['CodRegistro'].$tab[$index]['CodOrgao'].$tab[$index]['CPF_CNPJ_Proc'];
                        
                        $resultadofinal = "";
            
                        if($tab[$index]['QuarentenaAte'] != ""){
                            $resultadofinal = "Em Monitoramento até: ".  data_br($tab[$index]['QuarentenaAte']);
                        }else if($tab[$index]['NumProcAuditoria'] != "" && $tab[$index]['TipoProcesso'] == "auditoria"){
                            $resultadofinal = "Auditoria. Processo: ".$tab[$index]['NumProcAuditoria'];
                        }else if($tab[$index]['NumProcAuditoria'] != "" && $tab[$index]['TipoProcesso'] == "inspeção"){
                            $resultadofinal = "Inspeção. Processo: ".$tab[$index]['NumProcAuditoria'];
                        }else if($tab[$index]['SolicitadoAuditoria'] == true){
                            $resultadofinal = "Recomendado Abertura de Processo de Inspeção/Auditoria.";
                        }else if($tab[$index]['StatusRegistro'] == "concluído"){
                            $resultadofinal = "Arquivado";
                        }
                        
                        $CodTipo = $tab[$index]['CodTipo'];
                        $permitido = false;
                        
                        if($autorizacoes[0]['qtd'] > 0){
                            for ($index1 = 0; $index1 < count($autorizacoes); $index1++){
                                if($autorizacoes[$index1]['CodTipo'] == $CodTipo){
                                    $permitido = true;
                                }
                            }
                        }
                        
                        if($permitido == true){
                            $link = '<a href="#" title="Abre o Registro em um Nova Aba" class="consultar" onclick="return abrir_registro(0, \''.base64_encode($tab[$index]['CodTrilha'] * 2999).'\', \''.$tab[$index]['NomeTrilha'].'\', \''.$tab[$index]['TabelaDetalhe'].'\', \''.$tab[$index]['CPF_CNPJ_Proc'].'\')"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>';
                        }else{
                            $link = '<i class="fa fa-eye fa-lg" aria-hidden="true" style="color:DarkGray;" title="Seu perfil não tem acesso a esta área."></i>';
                        }
                        
                        $html = $html.
                        '<tr>
                            <td style="text-align: right;">'.$tab[$index]['CodTrilha'].'</td>
                            <td>'.$tab[$index]['NomeTrilha'].'</td>
                            <td>'.$tab[$index]['CPF_CNPJ_Proc'].'</td>
                            <td>'.$tab[$index]['Nome_Descricao'].'</td>
                            <td>'.$objorgao->consulta_orgao($tab[$index]['CodOrgao']).'</td>
                            <td style="text-align: center;">'.data_br($tab[$index]['DataTrilha']).'</td>
                            <td>'.$tab[$index]['StatusRegistro'].'</td>
                            <td>'.$resultadofinal.'</td>
                            <td>'.$link.'</td>
                        </tr>';
                    }
                    
                    
                }   
            }
            
            $array['html'] = $html;
            
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}
    

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}