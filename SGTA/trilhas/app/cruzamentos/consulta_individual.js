var minha_tabela;
var destino = "server.php";
var execute_cod = 0;

var ordem = [
                [0, "asc" ]
            ];

var data = [
            null,
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "date-uk"},
            {"sType": "string"},
            {"sType": "string"}
        ];

$(document).ready(function(){
    if($("#cpf").val() != ""){
        $("#tipo").val("1");
        formdata = $("#form1").serialize();
        submit_form(destino);
    }else if($("#nome").val() != ""){
        $("#tipo").val("2");
        formdata = $("#form1").serialize();
        submit_form(destino);
    }
});

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "consultar":
                if(typeof(minha_tabela) != "undefined"){
                    minha_tabela.destroy();
                }
                $("#tb_trilhas").html(retorno.html);
                datatable_("tbl_trilhas", 8, ordem, -1, data, "Cruzamento entre Trilhas - Consulta CPF.CNPJ.Num. Processo: " + $("#cpf").val(), "Cruzamento entre Trilhas - Consulta CPF.CNPJ.Num. Processo: " + $("#cpf").val()); 
                $("#tbl_trilhas").fadeIn();
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../inicio/inicio.php");
            break;
        case 2:
            
            break
    }
    execute_cod = 0;
}

function consultar(){
    $("#cpf").val($.trim($("#cpf").val()));
    if($("#cpf").val() == ""){
        $("#msg_alerta").text("Preencha o campo CPF/CNPJ/Num. Processo.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    $("#tipo").val("1");
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}

function consultar_nome(){
    //$("#nome").val($.trim($("#nome").val()));
    if($("#nome").val() == ""){
        $("#msg_alerta").text("Preencha o campo Nome/Razão Social.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    $("#tipo").val("2");
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}

function abrir_registro(CodOrgao, CodTrilha, NomeTrilha, TabDetalhe, Filtro){
    
    $("#Orgao").val(CodOrgao);
    $("#CodTrilha").val(CodTrilha);
    $("#NomeTrilha").val(NomeTrilha);
    $("#detalhe").val(TabDetalhe);
    $("#filtro").val(Filtro);
    document.getElementById("form2").submit();
    return false;
}