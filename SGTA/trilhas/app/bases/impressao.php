<?php 

    ob_start();
   

?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Impressão</title>
        <link href="tabelas.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
        <htmlpagefooter name="myFooter">
            <hr style="margin-bottom: 1px;">
            <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; 
             font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">
                <tr>
                    <td width="33%" style=" border-width: 0px; border-color:  #FFFFFF; border-style:  none;"><span style="font-weight: bold; font-style: italic;">IMPRESSÃO</span></td>
                    <td width="33%" align="center" style="font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">Página: {PAGENO} de {nbpg}.</td>
                    <td width="33%" style="text-align: right;  border-width: 0px; border-color:  #FFFFFF; border-style:  none;"><?php echo date("d-m-Y H:i:s") ?></td>
                </tr>
            </table>
        </htmlpagefooter>

        <sethtmlpagefooter name="myFooter" page="1" value="on" show-this-page="1"></sethtmlpagefooter>
        
        <table border="0" style="width: 100%;">
            <tr>
                <td style="width: 33%;">
                    <img src="../../images/logo_cge_2.png" width="260" style="float: right; display: block;" />
                </td>
                <td style="width: 34%; text-align: center;">
                    <img src="../../images/logo_uf_1.JPG" width="150" height="80" /> 
                </td>
                <td style="width: 33%; text-align: right;">
                    <img src="../../images/bannerobservatorio.png" width="230"/>
                </td>
            </tr>
        </table>
        <br><br>
        
        <?php
            if(!isset($_POST['tabela_html'])){
                echo 'OPERAÇÃO INVÁLIDA';
                goto fim;
            }
            
            echo $_POST['tabela_html'];
            
            
            fim:
        ?>

    </body>
</html>


<?php

    $formato = 'A4'; //A4 para retrato e A4-L para paisagem
    $margin_left = 10;
    $margin_right = 10;
    $margin_top = 16.3;
    $margin_bottom = 15;
    $margin_header = 11;
    $margin_footer = 9;

    //$html = urldecode($_SESSION['_htmlPDF']);
    $html = ob_get_clean();
    define('MPDF_PATCH', '../../plugin/mpdf60/');        
    include(MPDF_PATCH.'mpdf.php');
    $mpdf = new mPdf('',$formato,'','',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,'');
    $mpdf->allow_charset_conversion=true;
    $mpdf->charset_in='UTF-8';
    $mpdf->useSubstitutions = false;
    $mpdf->simpleTables = true;
    $mpdf->WriteHTML($html);
    $mpdf->Output();
    
    exit();
    
