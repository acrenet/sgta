<?php

/*
 * Tipos de formato:
 * texto, data, moeda, decimal
 * Para data e moeda o tamanho não é observado, preencher com 0.
 */

$var[] = array(
            "coluna" => "Data de Registro do Produto",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Descrição do Produto ou Serviço",
            "formato" => "texto",
            "tamanho" => 60,
            "descrição" => "Contem a descrição completa do produto ou serviço do pagamento.",
            "requerido" => "sim"
            );

$var[] = array(
            "coluna" => "Descrição do Item",
            "formato" => "texto",
            "tamanho" => 60,
            "descrição" => "Contem a descrição completa do item da compra.",
            "requerido" => "sim"
            );
            
$var[] = array(
            "coluna" => "Quantidade de Item",
            "formato" => "decimal",
            "tamanho" => 7,
            "descrição" => "Contem a quantidade por item contratado",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Unidade de medida",
            "formato" => "texto",
            "tamanho" => 10,
            "descrição" => "Contem a unidade de medida do item",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Tipo do estoque",
            "formato" => "texto",
            "tamanho" => 1,
            "descrição" => "Contem “E” para entrada e “S” para saída",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Valor do produto",
            "formato" => "moeda",
            "tamanho" => 14,
            "descrição" => "Contem o valor do produto, sendo que a célula deve ser preenchida com o valor inteiro mais 2 (duas) casas decimais, exemplo: 539,00",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Saldo no estoque para o produto",
            "formato" => "decimal",
            "tamanho" => 12,
            "descrição" => "Contem o saldo do produto no estoque, sendo que a célula deve ser preenchida com o valor inteiro mais 2 (duas) casas decimais, exemplo: 139,00 ",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Descrição do Objeto (item)",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contém a descrição completa do objeto item da compra.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "CPF do fornecedor",
            "formato" => "decimal", //texto, data, moeda, decimal
            "tamanho" => 11, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o CPF (11 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000).",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "CNPJ do fornecedor",
            "formato" => "decimal", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o CNPJ (14 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000000).",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Razão social do fornecedor",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o razão social do fornecedor.",
            "requerido" => "sim" //sim, não
        );
