<?php

try{
    ini_set('precision', '15');
    
    $__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
    require_once("../../code/funcoes.php");
    
    if(isset($_POST['operacao'])){
        $operacao = $_POST['operacao'];
    }else{
        $operacao = "indefinido";
    }
    
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "",
        "noticia" => "",
        "retorno" => ""
    );
    
    switch ($array["operacao"]) {
        case "load":
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $query = $obj_uploads->consulta_modelos();
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            $modelos = '<option value="0"></option>';
            
            while ($row = mysqli_fetch_array($query)){
                if($row['Status'] == "válido"){
                    $modelos = $modelos.'<option value="'.$row['CodModelo'].'">'.$row['Modelo'].'</option>';
                }     
            }
            
            $array['modelos'] = $modelos;
            
            //throw new Exception("orgao: ".$_SESSION['sessao_nome_orgao']);
            
            $array['unidade'] = $_SESSION['sessao_nome_orgao'];
            gerar_tabela();

            retorno();
            break;
        case "validar":
            $Modelo = $_POST['Tipo'];
            
            if($Modelo == 0){
                $array['erro'] = "Selecione o Tipo do Arquivo Primeiro.";
                retorno();
            }
            
            if(!isset($_FILES['uploadfile-0'])){
                $array['erro'] = "Arquivo não recebido.";
                retorno();
            }
            
            $Sigla = sanitizeString($_SESSION['sessao_sigla_orgao']);
            $Sigla = str_replace(",","",$Sigla);
            $Sigla = str_replace(" ","_",$Sigla);
            
            $Id = $_SESSION['sessao_id'];
            
            $Arquivo = $Id."_".$Sigla.".xlsx";
            
            $tmpFilePath = $_FILES['uploadfile-0']['tmp_name'];
            $fileName = $_FILES['uploadfile-0']['name'];
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            
            $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/bases";
            
            $newFilePath = $targetDir . "/" . $Arquivo;
            
            if (file_exists($newFilePath)) {
                unlink($newFilePath);
            }

            if(move_uploaded_file($tmpFilePath, $newFilePath) == false) {
                $array['erro'] = "Não foi possível realizar o upload do arquivo: $newFilePath.  ". print_r($_FILES);
            }
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $query = $obj_uploads->consulta_modelos($Modelo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            $row = mysqli_fetch_array($query);
            
            $template = $row['Template'];
            
            validar_arquivo($template, $newFilePath, $Modelo);
            
            retorno();
            break;
        case "upload":
            
            $Ano = $_POST['Ano'];
            $Periodo = $_POST['Periodo'];
            $Observacao = $_POST['Observacao'];
            $Modelo = $_POST['Tipo'];
            $Tamanho = $_POST['Tamanho'];
            
            $Tamanho = intval($Tamanho / 1024000);
            
            $Sigla = sanitizeString($_SESSION['sessao_sigla_orgao']);
            $Sigla = str_replace(",","",$Sigla);
            $Sigla = str_replace(" ","_",$Sigla);
            
            $Id = $_SESSION['sessao_id'];
            
            $Arquivo = $Id."_".$Sigla.".xlsx";
            
            $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/bases";
            
            $newFilePath = $targetDir . "/" . $Arquivo;
            
            if($Modelo == 0){
                $array['erro'] = "Selecione o Tipo do Arquivo na lista.";
                retorno();
            }
            
            if($Periodo == 0){
                $array['erro'] = "Selecione o Período na lista.";
                retorno();
            }
            
            if($Ano == 0){
                $array['erro'] = "Selecione o Ano na lista.";
                retorno();
            }
            
            if (! file_exists($newFilePath)) {
                $array['erro'] = "Arquivo não encontrado, valide o arquivo novamente.";
                retorno();
            }
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $query = $obj_uploads->consulta_modelos($Modelo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            $row = mysqli_fetch_array($query);
            
            $template = $row['Template'];
            $NomeModelo = $row['Modelo'];
            
            validar_arquivo($template, $newFilePath, $Modelo);
            
            if($array['tem_erro'] == "sim"){
                throw new Exception("Foram detectados erros no arquivo, a operação não pode ser efetuada.");
            }
            
            $Arquivo = ".xlsx";
            
            $Sigla = sanitizeString($_SESSION['sessao_sigla_orgao']);
            $Sigla = str_replace(",","",$Sigla);
            $Sigla = str_replace(" ","_",$Sigla);
            
            switch ($Modelo){
                case 0:
                    throw new Exception('Selecione o tipo do arquivo primeiro.');
                case 1:
                    $Arquivo = "Pagamentos.xlsx";
                    break;
                case 2:
                    $Arquivo = "Licitacoes.xlsx";
                    break;
                case 3:
                    $Arquivo = "Estoque.xlsx";
                    break;
                case 4:
                    $Arquivo = "Contratos.xlsx";
                    break;
                case 5:
                    $Arquivo = "Folha_de_Pagamentos.xlsx";
                    break;
                case 6:
                    $Arquivo = "Reequilibrio_Contratual.xlsx";
                    break;
                case 7:
                    $Arquivo = "Beneficiaros_Desoneracao.xlsx";
                    break;
                case 8:
                    $Arquivo = "Medicos_PJ.xlsx";
                    break;
                case 9:
                    $Arquivo = "Medicos_Horario_Estabelecido.xlsx";
                    break;
                case 10:
                    $Arquivo = "Medicos_Plantonistas.xlsx";
                    break;
                default:
                    $NomeModelo = str_replace(",","",$NomeModelo);
                    $NomeModelo = str_replace(" ","_",$NomeModelo);
                    $NomeModelo = sanitizeString($NomeModelo);
                    $Arquivo = $NomeModelo . ".xlsx";
                    break;
            }
            
            $Arquivo = $Ano."_".str_replace(" ","_",$Periodo)."_".$Sigla."_".$Arquivo;
            $Usuario = $_SESSION['sessao_nome'];
            $CodOrgao = $_SESSION['sessao_orgao'];
            
            $CodUpload = $obj_uploads->salva_arquivo($CodOrgao, $Periodo, $Ano, $Modelo, $Usuario, $Arquivo, $Tamanho, $Observacao);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }

            $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/bases/" . $CodOrgao;
            
            $retorno = true;
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }
            
            $targetDir = $targetDir . "/" . $CodUpload;
            
            $retorno = true;
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }
            
            $newFile = $targetDir . "/" . $Arquivo;
            
            if (file_exists($newFile)) {
                unlink($newFile);
            }
            
            if(rename($newFilePath, $newFile) == false){
                $array['erro'] = "Não foi possível mover o arquivo.";
                retorno();
            }
            
            gerar_tabela();

            retorno();
            break;
        case "esquema":
            $CodModelo = $_POST['tipo'];
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $query = $obj_uploads->consulta_modelos($CodModelo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            $row = mysqli_fetch_array($query);
            
            $NomeModelo = $row['Modelo'];
            $Orientacao = $row['Orientacao'];
            
            if($row['Template'] != ""){
                if(file_exists($_SERVER['DOCUMENT_ROOT']."/trilhas/app/bases/".$row['Template'])){
                    //carregar o modelo.
                    require_once $row['Template'];
                }else{
                    goto nao_existe;
                }      
            }else{
                nao_existe:

                require_once("../../obj/uploads.php");
                $obj_uploads  = new uploads();

                $query = $obj_uploads->consulta_campos($CodModelo);
                if($obj_uploads->erro != ""){
                    throw new Exception($obj_uploads->erro);
                }
                if(mysqli_num_rows($query) == 0){
                    throw new Exception("Nenhuma coluna cadastrada para o modelo.");
                }

                while ($row = mysqli_fetch_array($query)){

                    if($row['Requerido'] == true){
                        $requerido = "sim";
                    }else{
                        $requerido = "não";
                    }

                    $var[] = array(
                        "coluna" => $row['Coluna'],
                        "formato" => $row['Formato'],
                        "tamanho" => $row['Tamanho'],
                        "descrição" => $row['Descricao'],
                        "requerido" => $requerido
                    );
                }
            }
            
            $html = '<h4 style="line-height: 1.5;"><b>Requisitos para exportação da base "'.$NomeModelo.'".<br>Todas estas colunas deverão estar presentes no arquivo do Excel.</b></h4>
                    <h4 style="line-height: 1.5;">Orientações:<br>'.$Orientacao.'</h4>
                     <table class="tab1 table table-striped table-condensed" style="font-size: 100%;">
                        <thead>
                            <tr>
                                <th>Colunas</th>
                                <th>Descrição</th>
                                <th>Formato</th>
                                <th style="text-align: right;">Tamanho Máximo</th>
                                <th>Preenchimento Obrigatório</th>
                            </tr>
                        </thead>
                        <tbody>';
            
            for ($index = 0; $index < count($var); $index++){

                if($var[$index]['requerido'] == "não"){
                    $cor = "black";
                }else{
                    $cor = "MediumVioletRed";
                }

                $html = $html.  '<tr>
                                    <td style="color:'.$cor.';">'.$var[$index]['coluna'].'</td>
                                    <td>'.$var[$index]['descrição'].'</td>
                                    <td>'.$var[$index]['formato'].'</td>
                                    <td style="text-align: right;">'.$var[$index]['tamanho'].'</td>
                                    <td>'.$var[$index]['requerido'].'</td>    
                                </tr>';
            }
            
            $html = $html.  '</tbody>
                        </table>';
            
            $array['mensagem'] = $html;
            
            retorno();
            break;
        case "periodos":
            $CodModelo = $_POST['Tipo'];
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $query = $obj_uploads->consulta_modelos($CodModelo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            $row = mysqli_fetch_array($query);
            
            switch ($row['Periodicidade']){
                case "anual":
                    $periodos = '<option value="0"></option>'
                              . '<option value="1">Anual</option>';
                    break;
                case "semestral":
                    $periodos = '<option value="0"></option>'
                              . '<option value="1">1º Semestre</option>'
                              . '<option value="2">2º Semestre</option>';
                    break;
                case "trimestral":
                    $periodos = '<option value="0"></option>'
                              . '<option value="1">1º Trimestre</option>'
                              . '<option value="2">2º Trimestre</option>'
                              . '<option value="3">3º Trimestre</option>'
                              . '<option value="4">4º Trimestre</option>';
                    break;
                case "bimestral":
                    $periodos = '<option value="0"></option>'
                              . '<option value="1">1º Bimestre</option>'
                              . '<option value="2">2º Bimestre</option>'
                              . '<option value="3">3º Bimestre</option>'
                              . '<option value="4">4º Bimestre</option>'
                              . '<option value="5">5º Bimestre</option>'
                              . '<option value="6">6º Bimestre</option>';
                    break;
                case "mensal":
                    $periodos = '<option value="0"></option>'
                              . '<option value="1">Janeiro</option>'
                              . '<option value="2">Fevereiro</option>'
                              . '<option value="3">Março</option>'
                              . '<option value="4">Abril</option>'
                              . '<option value="5">Maio</option>'
                              . '<option value="6">Junho</option>'
                              . '<option value="7">Julho</option>'
                              . '<option value="8">Agosto</option>'
                              . '<option value="9">Setembro</option>'
                              . '<option value="10">Outubro</option>'
                              . '<option value="11">Novembro</option>'
                              . '<option value="12">Dezembro</option>';
                    break; 
            }
            
            $array['periodos'] = $periodos;
            
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
    retorno();
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}

function sanitizeString($str) {
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
    //$str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
    //$str = preg_replace('/[^a-z0-9]/i', '_', $str);
    //$str = preg_replace('/_+/', '_', $str); // ideia do Bacco :)
    return $str;
}



function validar_arquivo($template, $caminho, $CodModelo){
    try{
        global $array;
        
        $error_list = "";
        
        require_once("../../code/funcoes.php");
        
        if($template != ""){
            if(file_exists($_SERVER['DOCUMENT_ROOT']."/trilhas/app/bases/".$template)){
                //carregar o modelo.
                require_once $template;
            }else{
                goto nao_existe;
            }      
        }else{
            nao_existe:
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $query = $obj_uploads->consulta_campos($CodModelo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            if(mysqli_num_rows($query) == 0){
                throw new Exception("Nenhuma coluna cadastrada para o modelo.");
            }
            
            while ($row = mysqli_fetch_array($query)){
                
                if($row['Requerido'] == true){
                    $requerido = "sim";
                }else{
                    $requerido = "não";
                }
                
                $var[] = array(
                    "coluna" => $row['Coluna'],
                    "formato" => $row['Formato'],
                    "tamanho" => $row['Tamanho'],
                    "descrição" => $row['Descricao'],
                    "requerido" => $requerido
                );
            }
        }
              
        // carregar a classe PHPExcel
        
        require_once '../../plugin/PHPExcel.php';
        require_once '../../plugin/PHPExcel/Reader/Excel2007.php';
        
        class MyReadFilter implements PHPExcel_Reader_IReadFilter
            {
                public function readCell($column, $row, $worksheetName = '') {
                    // Read title row and rows 20 - 30
                    if ($row == 1 || ($row >= 1 && $row <= 10000)) {
                        return true;
                    }

                    return false;
                }
            }
        
       
        // definir a abertura do ficheiro em modo só de leitura
        $objReader = PHPExcel_IOFactory::createReaderForFile($caminho);
        
        //$objReader = new PHPExcel_Reader_Excel2007();
        
        $objReader->setReadFilter( new MyReadFilter() );
        $objReader->setReadDataOnly(true);
       
        $objPHPExcel = $objReader->load($caminho);
       
        $objPHPExcel->setActiveSheetIndex(0);

        //$colunas = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
        $total_linhas = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
       
        $total_colunas = count($var);
       
        /*
        if ($total_linhas > 20){
            $total_linhas = 20;
        }
        */
       
        $html = "";
        $error_list = "";
        
        $contagem_erros = 0;
        
        

        // navegar na linha
        for($linha=1; $linha<=$total_linhas; $linha++){
            $html = $html .  "<tr>";
            $html_linha = "";
                // navegar nas colunas da respectiva linha
            $erro_na_linha = false;
            for($coluna=0; $coluna < $total_colunas; $coluna++){

                if($linha==1){
                    $erro_na_linha = true;
                    
                    if($coluna == 0){
                        $html_linha = $html_linha .  "<th style='width: 10px;'><b>" . $linha . "</b></th>";
                    }

                    $cor = "black";

                    switch ($var[$coluna]["formato"]){
                        case "decimal":
                            $html_linha = $html_linha .  "<th style='text-align: right; color: $cor;'>".$var[$coluna]["coluna"]."</th>";
                            break;
                        case "texto":
                           $html_linha = $html_linha .  "<th style='color: $cor;'>".$var[$coluna]["coluna"]."</th>";
                            break;
                        case "data":
                            $html_linha = $html_linha .  "<th style='text-align: center; color: $cor;'>".$var[$coluna]["coluna"]."</th>";
                            break;
                        case "moeda":
                            $html_linha = $html_linha .  "<th style='text-align: right; color: $cor;'>".$var[$coluna]["coluna"]."</th>";
                            break;
                    }

                }else{
                    

                    // escreve os dados da tabela  
                    $cell = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();

                    switch ($var[$coluna]["formato"]){
                        case "decimal":
                            $cell = substr(trim($cell),0,$var[$coluna]["tamanho"]);
                            $cell = str_replace(".", "", $cell);
                            $cell = str_replace(",", ".", $cell);
                            if(is_numeric($cell)){
                                $cell = "<td style='text-align: right;color:DodgerBlue;'>".$cell."</td>";
                            }elseif($cell == "" && $var[$coluna]["requerido"] == "não"){
                                $cell = "<td style='background-color: LightSteelBlue;'>".$cell."</td>";
                            }else{
                                $erro_na_linha = true;
                                $error_list = "sim";
                                $cell = "<td style='text-align: right; background-color: LightSalmon; color:red; font-weight: bold;'>".$cell."</td>";
                            }
                            break;
                        case "texto":
                            $cell = str_replace("'", "", trim($cell));
                            if($cell != ""){
                                $cell = "<td>".substr($cell,0,$var[$coluna]["tamanho"])."</td>";
                            }elseif($cell == "" && $var[$coluna]["requerido"] == "não"){
                                $cell = "<td style='background-color: LightSteelBlue;'>".$cell."</td>";
                            }else{
                                $erro_na_linha = true;
                                $error_list = "sim";
                                $cell = "<td style='background-color: LightSalmon; color:red; font-weight: bold;'>".substr($cell,0,$var[$coluna]["tamanho"])."</td>";
                            }

                            break;
                        case "data":
                            if(is_numeric($cell)){
                                $cell = $cell + 1;
                                $cell = date('d/m/Y', PHPExcel_Shared_Date::ExcelToPHP($cell));
                                
                                if (DateTime::createFromFormat('Y-m-d', formata_data_mysql($cell)) === FALSE) {
                                    //não é data
                                    goto nao_e_data;
                                }else{
                                    $databr = new DateTime(formata_data_mysql($cell));
                                    $one = new DateTime('1900-01-01');
                                    $two = new DateTime('2050-12-31');
                                    if($databr < $one || $databr > $two){
                                        //não é data
                                        goto nao_e_data;
                                    }
                                }
                                
                                $cell = "<td style='text-align: center; color:SlateBlue;'>".$cell."</td>";
                            }elseif($cell == "" && $var[$coluna]["requerido"] == "não"){
                                $cell = "<td style='background-color: LightSteelBlue;'>".$cell."</td>";
                            }else{
                                nao_e_data:
                                $erro_na_linha = true;
                                $error_list = "sim";
                                $cell = "<td style='text-align: center; background-color: LightSalmon; color:red; font-weight: bold;'>".$cell."</td>";
                            }

                            break;
                        case "moeda":
                            if(is_numeric($cell)){
                                $numeromysql = numero_br($cell);
                                $numeromysql = str_replace(".", "", $numeromysql);
                                $numeromysql = str_replace(",", ".", $numeromysql);
                                $cell = "<td style='text-align: right;color:ForestGreen;'>".numero_br($cell)."</td>";
                            }elseif($cell == "" && $var[$coluna]["requerido"] == "não"){
                                $cell = "<td style='background-color: LightSteelBlue;'>".$cell."</td>";
                            }else{
                                $erro_na_linha = true;
                                $error_list = "sim";
                                $cell = "<td style='text-align: right; background-color: LightSalmon;color:red; font-weight: bold;'>".$cell."</td>";
                            }

                            break;
                    }
                    
                    if($coluna == 0){
                        $html_linha = "<td><b>" . $linha . "</b></td>";
                    }

                    $html_linha = $html_linha .  $cell;

                }
            }
            $html_linha = $html_linha .  "</tr>";
            
            if($erro_na_linha == true && $contagem_erros <= 100){
                $html = $html . $html_linha;
                $contagem_erros ++;
            }
            

        }

        pulo:

        $info = "";

        if($error_list == "sim"){
            $array['tem_erro'] = "sim";
            $info = "<br><b>Na tabela abaixo foram destacados em vermelho os erros que impedem a gravação. (limitado a 100 linhas):<br></b>";
            $array['contem_erro'] = true;
        }else{
            $array['tem_erro'] = "não";
            $info = "<br><b>Nenhum erro encontrado!<br></b>";
            $array['html'] = "";
            $array['contem_erro'] = false;
        }


        $array['html'] = $html;
        $array['info'] = $info;
       
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
        retorno();
    }
}

function gerar_tabela(){
    try{
        global $array;
        
        require_once("../../obj/uploads.php");
        $obj_uploads  = new uploads();
        
        $CodOrgao = $_SESSION['sessao_orgao'];
        
        if($CodOrgao == $_SESSION['unidade_controle']){
            $query = $obj_uploads->consulta_uploads(-1);
        }else{
            $query = $obj_uploads->consulta_uploads($CodOrgao);
        }
        
        $html = "";
        
        if(mysqli_num_rows($query) > 0){
            while ($row = mysqli_fetch_array($query)){
                $html = $html . '<tr>
                                <td><a href="#" onclick="return filtrar_tabela(\'tbl_anexos\',\''.$row['SiglaOrgao'].'\')">'.$row['SiglaOrgao'].'</a></td>
                                <td>'.$row['Referencia'].'</td>
                                <td><a href="#" onclick="return filtrar_tabela(\'tbl_anexos\',\''.$row['Modelo'].'\')">'.$row['Modelo'].'</a></td>
                                <td style="text-align:center;">'.data_br($row['Data']).'</td>
                                <td><a href="#" onclick="return filtrar_tabela(\'tbl_anexos\',\''.$row['Usuario'].'\')">'.$row['Usuario'].'</a></td>
                                <td>'.$row['Arquivo'].'</td>
                                <td style="text-align: right;">'.$row['Tamanho'].' mb</td>
                                <td>'.$row['Observacao'].'</td>
                                <td><a href="../../intra/bases/'.$row['CodOrgao']."/".$row['CodUpload'].'/'.$row['Arquivo'].'" target="_blank" title="Baixar arquivo" class="editar"><i class="fa fa-download fa-lg" aria-hidden="true"></i></a></td>
                          </tr>';
            }
        }
   
        $array['tabela'] = $html;
        
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
        retorno();
    }
}