<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if($perfil == 3 || $perfil == 4 || $perfil == 7){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Upload de Base de Dados";
?>
<link href="form_upoload.css" rel="stylesheet" type="text/css"/>
<script src="upload.js?id=0" type="text/javascript"></script>
<br>
<div class="container">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Nova Base de Dados</h4></div>
            <div class="panel-body">
                <div class="well well-sm" style="text-align: justify;">
                    <p style="font-weight: bold; font-size: 120%">
                        Instruções para o Upload.
                    </p>
                    <ul>
                        <li>
                            A aplicação aceita apenas arquivos do Excel 2007 ou posterior (xlsx).
                        </li>
                        <li>
                            A aplicação verifica apenas a primeira planilha, para que não haja dúvidas, deixe apenas uma planilha no arquivo.
                        </li>
                        <li>
                            A planilha contendo os dados não poderá ter linhas em branco ou títulos na parte superior. Assim a primeira linha
                            deverá ser obrigatoriamente os títulos das colunas e a partir da segunda linha os dados.
                        </li>
                        <li>
                            Para cada tipo de arquivo o SGTA informa qual o conteúdo necessário, para isso  basta clicar no botão "Esquema do Arquivo".
                        </li>
                        <li>
                            O tamanho limite do arquivo é de 20mb. Para conteúdos maiores fracione o arquivo em 2 ou mais partes. Informe a ocorrência
                            no campos "Observação para o TCE".
                        </li>
                        <li>
                            Ao efetuar o upload o Sistema verificará as informações contidas no arquivo do Excel, seu tipo de dado (texto, moeda,
                            data, etc.) O arquivo deverá estar rigorosamente compatível com o que for solicitado pelo Sistema ou será rejeitado.
                        </li>
                        <li>
                            Campos de data e números deverão estar formatados no padrão brasileiro, o Excel deverá reconhecê-los como datas e números
                            e não como texto.
                        </li>
                        <li>
                            Os dados constantes no arquivo deverão estar compatíveis com o período selecionado.
                        </li>
                        <li>
                            Finalizado o upload o arquivo aparecerá na tabela de Bases de Dados Enviadas. Caso o arquivo enviado esteja incorreto, pode-se 
                            enviar outro referente ao mesmo período.
                        </li>
                    </ul>
                </div>
                
                <button type="button" class="btn btn-success" onclick="novo_upload();">Novo Upload de Arquivo</button>
                
                <br>

            </div>
        </div>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Bases de Dados Enviadas</h4></div>
            <div class="panel-body">
                
                <table id="tbl_anexos" class="display" width="100%">
                    <thead>
                        <tr>
                            <th>Unidade</th>
                            <th>Referência</th>
                            <th>Base</th>
                            <th style="text-align:center;">Data</th>
                            <th>Usuário</th>
                            <th>Arquivo</th>
                            <th style="text-align:right;">Tamanho</th>
                            <th>Observação</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="tb_anexos">
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Unidade</th>
                            <th>Referência</th>
                            <th>Base</th>
                            <th style="text-align:center;">Data</th>
                            <th>Usuário</th>
                            <th>Arquivo</th>
                            <th style="text-align:right;">Tamanho</th>
                            <th>Observação</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>
    </div>
</div>

<div id="popup_upload" title="Upload de Arquivo" style="overflow: hidden; margin-bottom: 40px;" >
    <form id="form_upload" name="form_upload" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="unidade">Unidade:</label>
                    <input id="nm_unidade" name="unidade" type="text" class="form-control input-sm" value="" disabled="">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="cmb_tipo">Tipo do Arquivo:</label>
                    <select name="cmb_tipo" id="cmb_tipo" class="form-control input-sm" onchange="verificar();">
                        
                    </select>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="cmb_periodos">Referência:</label>
                    <select name="cmb_periodos" id="cmb_periodos" class="form-control input-sm">
                        <option value="0"></option>
                        
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="cmb_ano">Ano:</label>
                    <select name="cmb_ano" id="cmb_ano" class="form-control input-sm">
                        <option value="0"></option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
            <label for="obs">Observação para o TCE:</label>
            <textarea name="obs" id="1_obs" rows="4" cols="20" class="form-control input-sm" onkeyup="contar_caracteres('1_obs', 'contador', 1000)" maxlength="1000" placeholder="(opcional)"></textarea>
            <input type="text" class="form-control" id="contador" name="contador" style="width: 60px; float: right;" disabled="">
        </div>
        
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <span class="btn btn-primary btn-file"><span class="fileupload-new">Selecione o Arquivo &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>
                <span class="fileupload-exists">Alterar &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>         <input id="uploadfile" name="uploadfile" type="file" onchange="validar_arquivo(true)"/></span>
            <span class="fileupload-preview"></span>
            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
        </div>
        
        <div class="row" style="margin-bottom: 5px;">
            <div class="col-sm-8" style="font-size: 120%;">
                <button type="button" class="btn btn-default" onclick='consultar_esquema();'>Esquema do Arquivo&nbsp;<span class="fa fa-info-circle fa-lg"></span></button>
                <button type="button" class="btn btn-info" onclick='validar();'>Validar Arquivo&nbsp;<span class="fa fa-check fa-lg"></span></button>
            </div>
        </div>
        
        <div id="div_validacao" style="max-height: 300px; overflow-x: scroll; overflow-y: scroll; display: none;">
            <br>
            <div id="div_info" style="font-size: 120%;">
            
            </div>

            <table id="tbl_excel" class='table table-striped table-bordered table-hover table-condensed' style="font-size: 80%;">

            </table>
        </div>
        
        <input type="hidden" name="CodOrgao" id="CodOrgao" value="599" />
        <input type="hidden" name="Tamanho" id="Tamanho" value="0" />
        
        <hr>
        
    </form>
    
    <div id="div_confirmar" style="text-align: right; display: none;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick='$("#popup_upload").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        <button type="button" class="btn btn-success" onclick='subir_arquivo();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
</div>

<div id="popup_esquema" title="Arquivo do Excel" style="overflow: hidden; margin-bottom: 40px;" >
    <div id="div_esquema" style="font-size: 80%;">
        
    </div>
    <hr>
    <div id="div_confirmar" style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-info" onclick='imprimir_esquema();'>Imprimir &nbsp;<span class="fa fa-print"></span></button>
        <button type="button" class="btn btn-primary" onclick='$("#popup_esquema").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
    </div>    
</div>

<form name="form1" id="form1" action="impressao.php" target="_blank" method="POST">
    <input type="hidden" name="tabela_html" id="tabela_html" value="" />
</form>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');