<?php

try{
    
    $__servidor = true;
$operacao = "";
    require_once("../../code/verificar.php");
    if($__autenticado == false){
        $array = array(
            "operacao" => $operacao,
            "erro" => "",
            "alerta" => "A sessão expirou, será necessário realizar um novo login.",
            "noticia" => "",
            "retorno" => ""
        );
        retorno();
    }
    require_once("../../code/funcoes.php");
    
    if(isset($_POST['operacao'])){
        $operacao = $_POST['operacao'];
    }else{
        $operacao = "indefinido";
    }
    
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "",
        "noticia" => "",
        "retorno" => ""
    );
    
    switch ($array["operacao"]) {
        case "load":

            gerar_tabela();
            
            retorno();
            break;
        case "atualizar":
            
            gerar_tabela();
            
            retorno();
            break;
        case "cadastra_modelo":
            
            $CodModelo = $_POST['CodModelo'];
            $Modelo = $_POST['Modelo'];
            $Periodicidade = $_POST['Periodicidade'];
            $Template = $_POST['Template'];
            $Orientacao = $_POST['Orientacao'];
            
            if($Modelo == ''){
                throw new Exception("Informe um nome para o modelo.");
            }
            
            if($Periodicidade == -1){
                throw new Exception("Selecione a periodicidade na lista.");
            }        
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $id = $obj_uploads->cadastra_modelo($CodModelo, $Modelo, $Template, $Orientacao, $Periodicidade);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            if($CodModelo == -1){
                $array['CodModelo'] = $id;
                $CodModelo = $id;
            }else{
                $array['CodModelo'] = $CodModelo;
            }
            
            gerar_tabela();
            
            gerar_tbl_campos($CodModelo);
            
            retorno();
            break;
        case "editar_modelo":
            $CodModelo = $_POST['CodModelo'];
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $row = $obj_uploads->consulta_modelo($CodModelo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            $array['CodModelo'] = $row['CodModelo'];
            $array['Modelo'] = $row['Modelo'];
            $array['Template'] = $row['Template'];
            $array['Orientacao'] = $row['Orientacao'];
            $array['Periodicidade'] = $row['Periodicidade'];
            
            gerar_tbl_campos($CodModelo);
            
            retorno();
            break;
        case "cadastra_campo":
            $CodCampo = $_POST['CodCampo'];
            $CodModelo = $_POST['CodModelo'];
            $Coluna = $_POST['Coluna'];
            $Formato = $_POST['Formato'];
            $Tamanho = $_POST['Tamanho'];
            $Descricao = $_POST['Descricao'];
            $Requerido = $_POST['Requerido'];
            
            if($Coluna == ''){
                throw new Exception("Preencha um nome para o campo.");
            }
            
            if($Formato == -1){
                throw new Exception("Selecione um formato na lista.");
            }
            
            if(! is_numeric($Tamanho)){
                throw new Exception("Informe um tamanho válido para o campo.");
            }
            
            if($Requerido == -1){
                throw new Exception("Informe se o campo é requerido ou não na lista.");
            }
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $obj_uploads->cadastra_campo($CodCampo, $CodModelo, $Coluna, $Formato, $Tamanho, $Descricao, $Requerido);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            gerar_tbl_campos($CodModelo);
            
            retorno();
            break;
        case "reposicionar_campo":
            $CodCampo = $_POST['CodCampo'];
            $CodModelo = $_POST['CodModelo'];
            $direcao = $_POST['Direcao'];
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $obj_uploads->reposiciona_campo($CodCampo, $CodModelo, $direcao);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            gerar_tbl_campos($CodModelo);
            
            retorno();
            break;
        case "editar_campo":
            $CodCampo = $_POST['CodCampo'];
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $row = $obj_uploads->consulta_campo($CodCampo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            $array['CodCampo'] = $CodCampo;
            $array['Coluna'] = $row['Coluna'];
            $array['Formato'] = $row['Formato'];
            $array['Tamanho'] = $row['Tamanho'];
            $array['Descricao'] = $row['Descricao'];
            $array['Requerido'] = $row['Requerido'];
            
            retorno();
            break;
        case "excluir_campo":
            $CodCampo = $_POST['CodCampo'];
            $CodModelo = $_POST['CodModelo'];
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $obj_uploads->exclui_campo($CodCampo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            gerar_tbl_campos($CodModelo);
            
            retorno();
            break;
        case "excluir_modelo":
            $CodModelo = $_POST['CodModelo'];
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $obj_uploads->excluir_modelo($CodModelo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            gerar_tabela();
            
            retorno();
            break; 
        case "retornar_modelo":
            $CodModelo = $_POST['CodModelo'];
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $obj_uploads->retornar_modelo($CodModelo);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            gerar_tabela();
            
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
    retorno();
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}

function gerar_tabela(){
    try{
        global $array;
        
        require_once("../../obj/uploads.php");
        $obj_uploads  = new uploads();

        $query = $obj_uploads->consulta_modelos(-1);
        if($obj_uploads->erro != ""){
            throw new Exception($obj_uploads->erro);
        }
        
        $html = "";
        
        if(mysqli_num_rows($query) > 0){
            while ($row = mysqli_fetch_array($query)){
               
                if ($row['Status'] == "válido"){
                    $style = "";
                    $icone = '<a href="#" title="Excluir Modelo" class="excluir" onclick="return excluir_modelo('.$row['CodModelo'].');"><span class="fa fa-times fa-lg"></span></a>';
                }else{
                    $style = "style='color: red; font-weight: bold'";
                    $icone = '<a href="#" title="Retornar Modelo" class="verde" onclick="return retornar_modelo('.$row['CodModelo'].');"><span class="fa fa-undo fa-lg"></span></a>';
                }
                
                $html = $html . '<tr '.$style.'>
                                    <td>'.$row['Modelo'].'</td>
                                    <td>'.$row['Periodicidade'].'</td>
                                    <td>'.$row['Template'].'</td>
                                    <td>'.$row['Orientacao'].'</td>
                                    <td style="text-align: right;">'.$obj_uploads->contar_campos($row['CodModelo']).'</td>    
                                    <td>
                                        <a href="#" title="Editar Modelo" class="editar" onclick="return editar_modelo('.$row['CodModelo'].');"><span class="fa fa-pencil fa-lg"></span></a> &nbsp;
                                        '.$icone.'
                                    </td>
                                </tr>';
            }
        }
   
        $array['tabela'] = $html;
        
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
        retorno();
    }
}

function gerar_tbl_campos($CodModelo){
    try{
        global $array;
        
        require_once("../../obj/uploads.php");
        $obj_uploads  = new uploads();

        $query = $obj_uploads->consulta_campos($CodModelo);
        if($obj_uploads->erro != ""){
            throw new Exception($obj_uploads->erro);
        }
        
        $html = "";
        
        if(mysqli_num_rows($query) > 0){
            
            $p = 0;
            
            while ($row = mysqli_fetch_array($query)){
                $p++;
                
                if($row['Requerido'] == 1){
                    $row['Requerido'] = "<span style='color: darkblue; font-weight: bold;'>Sim</span>";
                }else{
                    $row['Requerido'] = "<span style='color: red; font-weight: bold;'>Não</span>";
                }
                
                $html = $html . '<tr>
                                    <td style="text-align: right; width: 15px;">'.$p.'</td>
                                    <td>'.$row['Coluna'].'</td>
                                    <td>'.$row['Formato'].'</td>
                                    <td>'.$row['Tamanho'].'</td>
                                    <td>'.$row['Descricao'].'</td>
                                    <td>'.$row['Requerido'].'</td>
                                    <td style="width: 100px;">
                                        <a href="#" title="Subir Posição" class="consultar" onclick="return subir_posicao('.$row['CodCampo'].', '.$row['CodModelo'].');"><span class="fa fa-arrow-up fa-lg"></span></a>&nbsp;
                                        <a href="#" title="Descer Posição" class="consultar" onclick="return descer_posicao('.$row['CodCampo'].', '.$row['CodModelo'].');"><span class="fa fa-arrow-down fa-lg"></span></a>&nbsp;
                                        <a href="#" title="Editar Campo" class="editar" onclick="return editar_campo('.$row['CodCampo'].');"><span class="fa fa-pencil fa-lg"></span></a>&nbsp;
                                        <a href="#" title="Excluir Campo" class="excluir" onclick="return excluir_campo('.$row['CodCampo'].', '.$row['CodModelo'].');"><span class="fa fa-times fa-lg"></span></a>
                                    </td>
                                </tr>';
            }
        }
   
        $array['tb_campos'] = $html;
    
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
        retorno();
    }
}

