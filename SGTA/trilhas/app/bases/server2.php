<?php

try{
    
    $__servidor = true;
$operacao = "";
    require_once("../../code/verificar.php");
    if($__autenticado == false){
        $array = array(
            "operacao" => $operacao,
            "erro" => "",
            "alerta" => "A sessão expirou, será necessário realizar um novo login.",
            "noticia" => "",
            "retorno" => ""
        );
        retorno();
    }
    require_once("../../code/funcoes.php");
    
    if(isset($_POST['operacao'])){
        $operacao = $_POST['operacao'];
    }else{
        $operacao = "indefinido";
    }
    
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "",
        "noticia" => "",
        "retorno" => ""
    );
    
    switch ($array["operacao"]) {
        case "load":

            gerar_tabela();
            
            retorno();
            break;
        case "atualizar":
            
            gerar_tabela();
            
            retorno();
            break;
        case "modificar":
            
            $acao = $_POST['acao'];
            $CodUpload = $_POST['codigo'];
            
            require_once("../../obj/uploads.php");
            $obj_uploads  = new uploads();
            
            $obj_uploads->alterar_Status($CodUpload, $acao);
            if($obj_uploads->erro != ""){
                throw new Exception($obj_uploads->erro);
            }
            
            gerar_tabela();
            
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
    retorno();
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}

function gerar_tabela(){
    try{
        global $array;
        
        require_once("../../obj/uploads.php");
        $obj_uploads  = new uploads();
        
        $CodOrgao = $_SESSION['sessao_orgao'];
        
        if($CodOrgao == $_SESSION['unidade_controle']){
            $query = $obj_uploads->consulta_uploads(-1);
        }else{
            $query = $obj_uploads->consulta_uploads($CodOrgao);
        }
        
        $html = "";
        
        if(mysqli_num_rows($query) > 0){
            while ($row = mysqli_fetch_array($query)){
                
                if($row['Status'] == "concluído"){
                    $cor = "color: darkgreen;";
                }elseif($row['Status'] == "rejeitado"){
                    $cor = "color: red;";
                }else{
                    $cor = "";
                }
                
                $html = $html . '<tr>
                                <td style="'.$cor.'"><a href="#" onclick="return filtrar_tabela(\'tbl_anexos\',\''.$row['SiglaOrgao'].'\')">'.$row['SiglaOrgao'].'</a></td>
                                <td style="'.$cor.'">'.$row['Referencia'].'</td>
                                <td style="'.$cor.'"><a href="#" onclick="return filtrar_tabela(\'tbl_anexos\',\''.$row['Modelo'].'\')">'.$row['Modelo'].'</a></td>
                                <td style="text-align:center; '.$cor.'">'.data_br($row['Data']).'</td>
                                <td style="'.$cor.'"><a href="#" onclick="return filtrar_tabela(\'tbl_anexos\',\''.$row['Usuario'].'\')">'.$row['Usuario'].'</a></td>
                                <td style="'.$cor.'">'.$row['Arquivo'].'</td>
                                <td style="text-align: right; '.$cor.'">'.$row['Tamanho'].' mb</td>
                                <td style="'.$cor.'">'.$row['Observacao'].'</td>
                                <td>
                                    <a href="../../intra/bases/'.$row['CodOrgao']."/".$row['CodUpload'].'/'.$row['Arquivo'].'" target="_blank" title="Baixar arquivo" class="editar"><i class="fa fa-download fa-lg" aria-hidden="true"></i></a>&nbsp
                                    <a href="#" target="_blank" title="Confirmar Carga do Arquivo" onclick="return confirmar('.$row['CodUpload'].');" class="consultar"><i class="fa fa-check-circle fa-lg" aria-hidden="true"></i></a>&nbsp
                                    <a href="#" target="_blank" title="Rejeitar Arquivo" onclick="return rejeitar('.$row['CodUpload'].');" class="excluir"><i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i></a>
                                </td>
                          </tr>';
            }
        }
       
   
        $array['tabela'] = $html;
        
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
        retorno();
    }
}

