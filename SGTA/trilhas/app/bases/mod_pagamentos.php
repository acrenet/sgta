<?php

/*
 * Tipos de formato:
 * texto, data, moeda, decimal
 * Para data e moeda o tamanho não é observado, preencher com 0.
 */

$var[] = array(
            "coluna" => "Numero da ordem de pagamento",
            "formato" => "texto",
            "tamanho" => 25,
            "descrição" => "Contem o numero da ordem de pagamento realizada.",
            "requerido" => "sim"
            );
            
$var[] = array(
            "coluna" => "Data do pagamento",
            "formato" => "data",
            "tamanho" => 25,
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012. ",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Numero da Licitação",
            "formato" => "texto",
            "tamanho" => 25,
            "descrição" => "Contem o numero da Licitação realizada e registrada na planilha de LICITAÇÃO.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Numero do processo de pagamento",
            "formato" => "texto",
            "tamanho" => 25,
            "descrição" => "Contem o numero do processo de pagamento que circula internamente.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Numero do processo de pagamento no SEPNET",
            "formato" => "texto",
            "tamanho" => 20,
            "descrição" => "Contem o numero do processo do SEPNET.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Descrição do Produto ou Serviço",
            "formato" => "texto",
            "tamanho" => 60,
            "descrição" => "Contem a descrição completa do produto ou serviço do pagamento.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "CPF do credor",
            "formato" => "decimal", //texto, data, moeda, decimal
            "tamanho" => 11, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o CPF (11 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000).",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "CNPJ do credor",
            "formato" => "decimal", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o CNPJ (14 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000000).",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Razão social do credor",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o razão social do credor.",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Valor Total Pago",
            "formato" => "moeda", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o valor total pago, sendo que a célula deve ser preenchida com o valor inteiro mais 2 (duas) casas decimais, exemplo: 639,00",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Fonte do Recurso",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 30, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem a descrição da fonte de recurso, a exemplo: Próprio, PAC, Convênio Ministério, Convênio Prefeituras e outros.",
            "requerido" => "sim" //sim, não
        );