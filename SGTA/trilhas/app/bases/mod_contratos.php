<?php

/*
 * Tipos de formato:
 * texto, data, moeda, decimal
 * Para data e moeda o tamanho não é observado, preencher com 0.
 */

$var[] = array(
            "coluna" => "Número do Contrato/Aditivo",
            "formato" => "texto",
            "tamanho" => 25,
            "descrição" => "Contém o número do Contrato realizado entre a empresa e o vencedor da licitação.",
            "requerido" => "sim"
            );
            
$var[] = array(
            "coluna" => "Numero da Licitação",
            "formato" => "texto",
            "tamanho" => 25,
            "descrição" => "Contém o número da Licitação realizada e registrada na planilha de LICITAÇÃO",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Número do Processo",
            "formato" => "texto",
            "tamanho" => 25,
            "descrição" => "Contém o número do processo que circula internamente.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Número do SEPNET",
            "formato" => "texto",
            "tamanho" => 20,
            "descrição" => "Contém o número do processo do SEPNET.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Data Vigência Inicial",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Data Vigência Final",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Descrição do Objeto (item)",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contém a descrição completa do objeto item da compra.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Razão social do vencedor ou contratado",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contém o razão social do vencedor ou contratado para execução do trabalho, registrado no Contrato efetuado junto a ele.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "CPF do vencedor ou contratado",
            "formato" => "decimal", //texto, data, moeda, decimal
            "tamanho" => 11, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o CPF (11 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000).",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "CNPJ do vencedor ou contratado",
            "formato" => "decimal", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o CNPJ (14 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000000).",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Valor do Contrato",
            "formato" => "moeda", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o valor total do contrato, sendo que a célula deve ser preenchida com o valor inteiro mais 2 (duas) casas decimais, exemplo: 515,39",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Tipo do contrato",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 1, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o tipo de contrato, sendo: C -> Contrato e A -> Aditivo.",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Número do Contrato",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 25, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Se o campo tipo do Contrato estiver preenchido com “A”, este campo deverá estar preenchido com o numero do contrato principal a que se refere este aditivo.",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Data do serviço",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Data da entrega",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Local da Obra",
            "formato" => "texto",
            "tamanho" => 60,
            "descrição" => "Contem a localização da obra",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Assunto (Categoria)",
            "formato" => "texto",
            "tamanho" => 60,
            "descrição" => "Contem o assunto ou categoria, a exemplo: Água tratada ou Esgotamento sanitário.",
            "requerido" => "não"
        );

