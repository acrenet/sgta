var execute_cod = 0;
var destino = "server3.php";
var minha_tabela;

$(document).ready(function(){
    
    popup_modelo_();
    popup_campo_();
    
    var data = new FormData();
    data.append("operacao","load");
    formdata = data;
    submit_file("server3.php");
});

var ordem = [
                [0, "asc" ]
            ];

var data = [
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            null,
            null
        ];

function popup_modelo_(){
    $("#popup_modelo").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 1000,
        maxWidth: 1000,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function popup_campo_(){
    $("#popup_campo").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 600,
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function resposta_up(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                $("#tb_modelos").html(retorno.tabela);
                datatable_("tbl_modelos", 5, ordem, -1, data, "modelos de arquivos", "modelos");
                break;
            case "cadastra_modelo":
                $("#popup_pergunta").dialog("close");
                $("#CodModelo").val(retorno.CodModelo);
                minha_tabela.destroy();
                $("#tb_modelos").html(retorno.tabela);
                datatable_("tbl_modelos", 5, ordem, -1, data, "modelos de arquivos", "modelos");
                $("#tb_campos").html(retorno.tb_campos);
                $("#div_campos").slideDown();
                $("#msg_sucesso").html("Informações gravadas com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "editar_modelo":
                $("#CodModelo").val(retorno.CodModelo);
                $("#Modelo").val(retorno.Modelo);
                $("#Template").val(retorno.Template);
                $("#Orientacao").val(retorno.Orientacao);
                $("#Periodicidade").val(retorno.Periodicidade);
                $("#tb_campos").html(retorno.tb_campos);
                $("#div_campos").show();
                $("#popup_modelo").dialog("open"); 
                break;
            case "cadastra_campo":
                $("#popup_pergunta").dialog("close");
                $("#popup_campo").dialog("close");
                document.getElementById("form_campo").reset();
                $("#tb_campos").html(retorno.tb_campos);
                break;
            case "editar_campo":
                document.getElementById("form_campo").reset();
                $("#CodCampo").val(retorno.CodCampo);
                $("#Coluna").val(retorno.Coluna);
                $("#Formato").val(retorno.Formato);
                $("#Tamanho").val(retorno.Tamanho);
                $("#Descricao").val(retorno.Descricao);
                $("#Requerido").val(retorno.Requerido);
                $("#popup_campo").dialog("open");
                break;
            case "excluir_campo":
                $("#popup_pergunta").dialog("close");
            case "reposicionar_campo":
                $("#tb_campos").html(retorno.tb_campos);
                break;
            case "excluir_modelo":
                minha_tabela.destroy();
                $("#tb_modelos").html(retorno.tabela);
                datatable_("tbl_modelos", 5, ordem, -1, data, "modelos de arquivos", "modelos");
                break;
            case "retornar_modelo":
                minha_tabela.destroy();
                $("#tb_modelos").html(retorno.tabela);
                datatable_("tbl_modelos", 5, ordem, -1, data, "modelos de arquivos", "modelos");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    
}

function cadastrar(){
    document.getElementById("form_modelo").reset();
    $("#tb_campos").html("");
    $("#div_campos").hide();
    $("#CodModelo").val(-1);
    $("#popup_modelo").dialog("open");
}

function salvar(){
    
    var data = new FormData();

    data.append("operacao", "cadastra_modelo");
    data.append("CodModelo", $("#CodModelo").val());
    data.append("Modelo", $("#Modelo").val());
    data.append("Periodicidade", $("#Periodicidade").val());
    data.append("Template", $("#Template").val());
    data.append("Orientacao", $("#Orientacao").val());
    
    formdata = data;

    $("#msg_pergunta").text("Os dados estão corretos?");
    $("#popup_pergunta").dialog("open");
    
}

function editar_modelo(cod){
    var data = new FormData();
    
    data.append("operacao", "editar_modelo");
    data.append("CodModelo", cod);
    formdata = data;
    submit_file("server3.php");
    return false;
}

function cadastrar_campo(){
    if($("#CodModelo").val() == -1){
        $("#msg_alerta").html("Antes de adicionar um novo campo salve o modelo primeiro.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    document.getElementById("form_campo").reset();
    $("#CodCampo").val(-1);
    $("#popup_campo").dialog("open");
}

function editar_campo(CodCampo){
    var data = new FormData();
    data.append("operacao", "editar_campo");
    data.append("CodCampo", CodCampo);
    formdata = data;
    submit_file("server3.php");
    return false;
}

function excluir_campo(CodCampo, CodModelo){
    var data = new FormData();
    data.append("operacao", "excluir_campo");
    data.append("CodCampo", CodCampo);
    data.append("CodModelo", CodModelo);
    formdata = data;
    
    $("#msg_pergunta").text("Você tem certeza da exclusão do campo?");
    $("#popup_pergunta").dialog("open");
    return false;
}

function subir_posicao(CodCampo, CodModelo){
    var data = new FormData();
    data.append("operacao", "reposicionar_campo");
    data.append("Direcao", "acima");
    data.append("CodCampo", CodCampo);
    data.append("CodModelo", CodModelo);
    formdata = data;
    submit_file("server3.php");
    return false;
}

function descer_posicao(CodCampo, CodModelo){
    var data = new FormData();
    data.append("operacao", "reposicionar_campo");
    data.append("Direcao", "abaixo");
    data.append("CodCampo", CodCampo);
    data.append("CodModelo", CodModelo);
    formdata = data;
    submit_file("server3.php");
    return false;
}

function salvar_campo(){
    var data = new FormData();

    data.append("operacao", "cadastra_campo");
    data.append("CodModelo", $("#CodModelo").val());
    data.append("CodCampo", $("#CodCampo").val());
    data.append("Coluna", $("#Coluna").val());
    data.append("Formato", $("#Formato").val());
    data.append("Tamanho", $("#Tamanho").val());
    data.append("Requerido", $("#Requerido").val());
    data.append("Descricao", $("#Descricao").val());
    
    formdata = data;

    $("#msg_pergunta").text("Os dados estão corretos?");
    $("#popup_pergunta").dialog("open");
}

function excluir_modelo(CodModelo){
    var data = new FormData();
    data.append("operacao", "excluir_modelo");
    data.append("CodModelo", CodModelo);
    formdata = data;
    submit_file("server3.php");
    return false;
}

function retornar_modelo(CodModelo){
        var data = new FormData();
    data.append("operacao", "retornar_modelo");
    data.append("CodModelo", CodModelo);
    formdata = data;
    submit_file("server3.php");
    return false;
}

function pergunta_ok(){
    submit_file("server3.php");
}