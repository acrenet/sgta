<?php

/*
 * Tipos de formato:
 * texto, data, moeda, decimal
 * Para data e moeda o tamanho não é observado, preencher com 0.
 */

$var[] = array(
            "coluna" => "CNPJ do Órgão",
            "formato" => "decimal",
            "tamanho" => 14,
            "descrição" => "Contem o CNPJ (14 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000000).",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Nome do Órgão",
            "formato" => "texto",
            "tamanho" => 50,
            "descrição" => "Contém o nome do Órgão.",
            "requerido" => "sim"
            );

$var[] = array(
            "coluna" => "Código do Órgão",
            "formato" => "decimal",
            "tamanho" => 8,
            "descrição" => "Contém o código do órgão.",
            "requerido" => "sim"
            );
            
$var[] = array(
            "coluna" => "CPF do Funcionário",
            "formato" => "decimal",
            "tamanho" => 11,
            "descrição" => "Contem o CPF (11 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000).",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Nome do Funcionário",
            "formato" => "texto",
            "tamanho" => 60,
            "descrição" => "Contém o nome do funcionário.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Nome do Cargo",
            "formato" => "texto",
            "tamanho" => 60,
            "descrição" => "Contem o Nome do Cargo do funcionário",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Data do exercício no cargo",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Data da admissão",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Nome da Função",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o Nome da Função do funcionário",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Data da nascimento",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Situação Funcional",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 30, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem a situação funcional, a exemplo: Inativo e  Ativo",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "UF lotação",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 2, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem a UF de lotação do funcionário",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Nome do Município",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 50, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o nome do município de lotação do funcionário.",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Valor da remuneração do funcionário",
            "formato" => "moeda", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "A célula deve ser preenchida com o valor inteiro mais 2 (duas) casas decimais, exemplo: 3.239,00 ",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Valor do desconto do funcionário",
            "formato" => "moeda", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "A célula deve ser preenchida com o valor inteiro mais 2 (duas) casas decimais, exemplo: 3.239,00 ",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Valor líquido do funcionário",
            "formato" => "moeda", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "A célula deve ser preenchida com o valor inteiro mais 2 (duas) casas decimais, exemplo: 3.239,00 ",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Nome do regime jurídico",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem informações a exemplo: Requisitado Estatutário de órgão externo, Contrato Temporário, Efetivo empregado público,Cedido para outro órgão e outros.",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Decreto de Nomeação",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 30, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o numero do decreto de nomeação do funcionário.",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Carga Horária",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 3, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem a carga horária do funcionário",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Lei do Cargo",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 30, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem a lei de criação do cargo",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Local de Lotação",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 50, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o local de lotação do funcionário",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Unidade de Lotação",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 50, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem a unidade de lotação do funcionário",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Setor de Lotação",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 50, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o setor de lotação do funcionário",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Disposição/Cessão",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 30, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem a disposição ou cessão do funcionário",
            "requerido" => "não" //sim, não
        );

$var[] = array(
            "coluna" => "Origem da disposição",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 30, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem a origem da disposição do funcionário",
            "requerido" => "não" //sim, não
        );

$var[] = array(
            "coluna" => "Destino da disposição",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 30, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o destino da disposição do funcionário",
            "requerido" => "não" //sim, não
        );

$var[] = array(
            "coluna" => "Ônus da disposição",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 30, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o ônus da disposição do funcionário",
            "requerido" => "não" //sim, não
        );

$var[] = array(
            "coluna" => "Nome da Mãe do Funcionário",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o nome da mãe do funcionário",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "PIS/PASEP",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 20, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o numero do PIS/PASEP do funcionário",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "NIT",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 20, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o numero do NIT do funcionário",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Tipo de Inatividade",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 30, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem informações do tipo da inatividade do funcionário, podendo ser: Aposentadoria Compulsória, Aposentadoria por Invalidez, Aposentadoria por tempo de serviço, Reformado, Pensionista e etc.).",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Data da aposentadoria",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Contem a data de início da aposentadoria do funcionário, Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Data do óbito",
            "formato" => "data",
            "tamanho" => 10,
            "descrição" => "Contem a data do óbito do funcionário, Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "referência do Último Recebimento",
            "formato" => "decimal",
            "tamanho" => 6,
            "descrição" => "Contem a referência do último recebimento do funcionário, possuindo MMAAAA.",
            "requerido" => "sim"
        );

