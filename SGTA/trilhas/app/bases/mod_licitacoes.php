<?php

/*
 * Tipos de formato:
 * texto, data, moeda, decimal
 * Para data e moeda o tamanho não é observado, preencher com 0.
 */

$var[] = array(
            "coluna" => "Número da Licitação",
            "formato" => "texto",
            "tamanho" => 25,
            "descrição" => "Contém o número da Licitação realizada e registrada na planilha de LICITAÇÃO.",
            "requerido" => "sim"
            );
            
$var[] = array(
            "coluna" => "Número do Processo",
            "formato" => "texto",
            "tamanho" => 25,
            "descrição" => "Contem o numero do processo que circula internamente.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Número do SEPNET",
            "formato" => "texto",
            "tamanho" => 20,
            "descrição" => "Contém o número do processo do SEPNET.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Modalidade da Licitação",
            "formato" => "texto",
            "tamanho" => 60,
            "descrição" => "Contém a descrição da modalidade da licitação.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Tipo da Licitação",
            "formato" => "texto",
            "tamanho" => 30,
            "descrição" => "Contém a descrição do tipo de licitação.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Data da Licitação",
            "formato" => "data", //texto, data, moeda, decimal
            "tamanho" => 10, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Formato DD/MM/AAAA, exemplo de um dia: 01/01/2012. ",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Descrição do Objeto",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contém a descrição completa do objeto da compra.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Descrição do Item",
            "formato" => "texto", //texto, data, moeda, decimal
            "tamanho" => 60, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contém a descrição completa do item da compra.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Valor adjudicado",
            "formato" => "moeda", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contém o valor adjudicado do total do contrato, sendo que a célula deve ser preenchida com o valor inteiro mais 2 (duas) casas decimais, exemplo: 526,84",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Valor do item",
            "formato" => "moeda", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contém o valor por item contratado, sendo que a célula deve ser preenchida com o valor inteiro mais 2 (duas) casas decimais, exemplo: 515,39 ",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Quantidade de itens",
            "formato" => "decimal", //texto, data, moeda, decimal
            "tamanho" => 7, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contém a quantidade por item contratado",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "CPF do participante",
            "formato" => "decimal", //texto, data, moeda, decimal
            "tamanho" => 11, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o CPF (11 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000).",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "CNPJ do participante",
            "formato" => "decimal", //texto, data, moeda, decimal
            "tamanho" => 14, //Para data e moeda o tamanho não é observado, preencher com 0.
            "descrição" => "Contem o CNPJ (14 bytes) com valor numérico, sendo que deve ser preenchido com zeros a esquerda, ficando o campo quando zerado (00000000000000).",
            "requerido" => "sim" //sim, não
        );

$var[] = array(
            "coluna" => "Indicador do Vencedor",
            "formato" => "texto",
            "tamanho" => 1,
            "descrição" => "Contem “V” para o vencedor.",
            "requerido" => "não"
        );
