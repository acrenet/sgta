var execute_cod = 0;
var destino = "server.php";
var minha_tabela;
var nome_unidade;

$(document).ready(function(){
    
    popup_upload_();
    popup_esquema_();
    
    var data = new FormData();
    data.append("operacao","load");
    formdata = data;
    submit_file("server.php");
});

var ordem = [
                [3, "desc" ]
            ];

var data = [
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "date-uk"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            null
        ];

!function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)

function popup_upload_(){
    $("#popup_upload").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 1000,
        maxWidth: 1000,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function popup_esquema_(){
    $("#popup_esquema").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 1000,
        maxWidth: 1000,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function validar_arquivo(ocultar){
    
    var upl = $("#uploadfile");

    
    var files = upl[0].files;
    var log = "";
    var erro  = false;
    var msg = "";
    var ext = "";

    for (var i = 0; i < files.length; i++){
        log = log + '"' + files[i].name + '"  ';
        if(files[i].size > 20480000){
            erro = true;
            msg = msg + "O arquivo ''" + files[i].name + "'' ultrapaça o limite de 10 megabytes.<br>";
        }
        
        $("#Tamanho").val(files[i].size);

        ext = files[0].name.split('.').pop();

        var str = ",xlsx";
        if(str.indexOf(ext) == -1){
            erro = true;
            msg = msg + "O arquivo ''" + files[i].name + "'' possui um formato não permitido.<br>É permitido apenas arquivos do excel ''xlsx''.";
        }   
    }
    
    if(ocultar == true){
        $("#div_confirmar").slideUp();
        $("#div_validacao").slideUp();
    }

    if(erro == true){
        $("#msg_erro").html(msg);
        $("#popup_erro").dialog("open");
        return false;
    }else{
        return files.length;
    }

}

function subir_arquivo(){
    var resposta = validar_arquivo(false);
    if (resposta === false){
        return false;
    }else if(resposta == 0){
        $("#msg_alerta").html("Nenhum arquivo selecionado.<br>Selecione um arquivo primeiro.");
        $("#popup_alerta").dialog("open");
        return false;
    }else{

        var data = new FormData();
        
        //jQuery.each(jQuery('#uploadfile')[0].files, function(i, file) {
        //    data.append('uploadfile-'+i, file);
        //});
        
        data.append("CodOrgao", $("#CodOrgao").val());
        data.append("Ano", $("#cmb_ano").val());
        data.append("Periodo", $("#cmb_periodos").val());
        data.append("Tipo", $("#cmb_tipo").val());
        data.append("Observacao", $("#1_obs").val());
        data.append("Tamanho", $("#Tamanho").val());
        data.append("operacao","upload");

        formdata = data;
        
        $("#msg_pergunta").text("Confirma o envio do arquivo?");
        $("#popup_pergunta").dialog("open");
    }
}

function pergunta_ok(){
    submit_file("server.php");
}

function resposta_up(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                
                $("#cmb_tipo").html(retorno.modelos);
                //console.log("unidade --> " + $("#nm_unidade").val() + " --> " + retorno.unidade);
                $("#nm_unidade").val(retorno.unidade);
                nome_unidade = retorno.unidade;
                $("#tb_anexos").html(retorno.tabela);
                datatable_("tbl_anexos", 8, ordem, -1, data, "upload de arquivos", "anexos");
                break;
            case "upload":
                
                minha_tabela.destroy();
                $("#tb_anexos").html(retorno.tabela);
                datatable_("tbl_anexos", 8, ordem, -1, data, "upload de arquivos", "anexos");
                $("#div_confirmar").hide();
                $("#div_validacao").hide();
                document.getElementById("form_upload").reset();
                $("#nm_unidade").val(nome_unidade);
                $("#popup_pergunta").dialog("close");
                $("#popup_upload").dialog("close");
                $("#msg_sucesso").html("Arquivo Anexado com Sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "esquema":
                $("#div_esquema").html(retorno.mensagem);
                $("#popup_esquema").dialog("open");
                
                break;
            case "validar":
                $("#tbl_excel").html(retorno.html);
                $("#div_info").html(retorno.info);
                $("#div_validacao").slideDown();
                if(retorno.contem_erro == false){
                    $("#div_confirmar").slideDown();
                }else{
                    $("#div_confirmar").slideUp();
                }
                
                break;
            case "periodos":
                $("#cmb_periodos").html(retorno.periodos);
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function novo_upload(){
    document.getElementById("form_upload").reset();
    $("#nm_unidade").val(nome_unidade);
    $("#div_validacao").hide();
    $('#popup_upload').dialog('open');
}

function consultar_esquema(){
    
    if($("#cmb_tipo").val() == 0){
        $("#msg_alerta").html("Selecione o Tipo do Arquivo na Lista.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    
    var data = new FormData();
    data.append("operacao","esquema");
    data.append("tipo", $("#cmb_tipo").val());

    formdata = data;
    submit_file(destino);
}

function validar(){
    
    var resposta = validar_arquivo(false);
    if (resposta === false){
        return false;
    }else if(resposta == 0){
        $("#msg_alerta").html("Nenhum arquivo selecionado.<br>Selecione um arquivo primeiro.");
        $("#popup_alerta").dialog("open");
        return false;
    }else{

        var data = new FormData();
        jQuery.each(jQuery('#uploadfile')[0].files, function(i, file) {
            data.append('uploadfile-'+i, file);
        });
        
        data.append("operacao","validar");
        data.append("Tipo", $("#cmb_tipo").val());

        formdata = data;
        submit_file(destino);
    }
}

function verificar(){
    $("#div_confirmar").slideUp();
    $("#div_validacao").slideUp();
    $("#cmb_periodos").html('<option value="0"></option>');
    
    if($("#cmb_tipo").val() != 0){
        var data = new FormData();
        data.append("operacao","periodos");
        data.append("Tipo", $("#cmb_tipo").val());

        formdata = data;
        submit_file(destino);
    }      
}

function efetuar_download(arquivo){
    var data = new FormData();
    data.append("operacao","periodos");
    data.append("Tipo", $("#cmb_tipo").val());

    formdata = data;
    submit_file(destino);
}

function imprimir_esquema(){
    $("#tabela_html").val($("#div_esquema").html());
    document.getElementById("form1").submit();
}

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "esquema":
                $("#msg_texto").html(retorno.mensagem);
                $("#popup_mensagem").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}


function resposta_continua(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "esquema":
                $("#msg_texto").html(retorno.mensagem);
                $("#popup_mensagem_continua").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    
}