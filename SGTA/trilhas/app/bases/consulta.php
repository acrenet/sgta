<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if($perfil > 2){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Consulta Uploads";
?>

<script src="consulta.js" type="text/javascript"></script>

<br>
<div class="container-fluid">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Bases de Dados Enviadas &nbsp; <a href="#" title="Atualizar Tabela." onclick="return atualizar();"><i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color: white;"></i></a></h4></div>
            <div class="panel-body">
                
                <table id="tbl_anexos" class="display" width="100%">
                    <thead>
                        <tr>
                            <th>Unidade</th>
                            <th>Referência</th>
                            <th>Base</th>
                            <th style="text-align:center;">Data</th>
                            <th>Usuário</th>
                            <th>Arquivo</th>
                            <th style="text-align:right;">Tamanho</th>
                            <th>Observação</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="tb_anexos">
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Unidade</th>
                            <th>Referência</th>
                            <th>Base</th>
                            <th style="text-align:center;">Data</th>
                            <th>Usuário</th>
                            <th>Arquivo</th>
                            <th style="text-align:right;">Tamanho</th>
                            <th>Observação</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>
    </div>
</div>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');