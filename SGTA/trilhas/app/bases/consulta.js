var execute_cod = 0;
var destino = "server2.php";
var minha_tabela;
var unidade;

$(document).ready(function(){
    var data = new FormData();
    data.append("operacao","load");
    formdata = data;
    submit_file("server2.php");
});

var ordem = [
                [3, "desc" ]
            ];

var data = [
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "date-uk"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            null
        ];
        
function resposta_up(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                $("#tb_anexos").html(retorno.tabela);
                datatable_("tbl_anexos", 8, ordem, -1, data, "upload de arquivos", "anexos");
                break;
            case "atualizar":
                
                minha_tabela.destroy();
                $("#tb_anexos").html(retorno.tabela);
                datatable_("tbl_anexos", 8, ordem, -1, data, "upload de arquivos", "anexos");
                
                break;
            case "modificar":
                
                minha_tabela.destroy();
                $("#tb_anexos").html(retorno.tabela);
                datatable_("tbl_anexos", 8, ordem, -1, data, "upload de arquivos", "anexos");
                
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function atualizar(){
    var data = new FormData();
    data.append("operacao","atualizar");
    formdata = data;
    submit_file("server2.php");
    return false;
}

function confirmar(cod){
    var data = new FormData();
    data.append("operacao","modificar");
    data.append("acao","confirmar");
    data.append("codigo",cod);
    
    formdata = data;
    submit_file("server2.php");
    return false;
}

function rejeitar(cod){
    var data = new FormData();
    data.append("operacao","modificar");
    data.append("acao","rejeitar");
    data.append("codigo",cod);
    formdata = data;
    submit_file("server2.php");
    return false;
}
