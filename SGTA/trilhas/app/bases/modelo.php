<?php
//Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if($perfil > 3){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Consulta Uploads";
?>
<script src="modelo.js" type="text/javascript"></script>
<br>

<div class="container">
    <button type="button" class="btn btn-primary" onclick="cadastrar();">Cadastrar Novo Modelo &nbsp;<span class="fa fa-plus-circle fa-lg"></span></button>
    <br><br>
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Modelos Cadastrados</h4></div>
            <div class="panel-body">
                <table id="tbl_modelos" class="display" width="100%">
                    <thead>
                        <tr>
                            <th>Modelo</th>
                            <th>Periodicidade</th>
                            <th>Plugin</th>
                            <th>Orientação</th>
                            <th>Qtd Colunas</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="tb_modelos">
                        
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<div id="popup_modelo" title="Cadastro de Modelo" style="overflow: hidden; margin-bottom: 40px;" >
    <form id="form_modelo" name="form_upload" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="Modelo">Nome do Modelo:</label>
                    <input id="Modelo" name="Modelo" type="text" class="form-control input-sm" maxlength="100">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="Periodicidade">Periodicidade:</label>
                    <select name="Periodicidade" id="Periodicidade" class="form-control input-sm">
                        <option value="-1"></option>
                        <option value="mensal">Mensal</option>
                        <option value="bimestral">Bimestral</option>
                        <option value="trimestral">Trimestral</option>
                        <option value="semestral">Semestral</option>
                        <option value="anual">Anual</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="Template">Arquivo de Configuração (se houver):</label>
                    <input id="Template" name="Template" type="text" class="form-control input-sm" maxlength="50">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="Orientacao">Observação:</label>
                    <input id="Orientacao" name="Orientacao" type="text" class="form-control input-sm" maxlength="500">
                </div>
            </div>
        </div>
        
        <input type="hidden" name="CodModelo" id="CodModelo" value="" />
        
    </form>
    
    <hr>
    <div id="div_confirmar" style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick='$("#popup_modelo").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        <button type="button" class="btn btn-success" onclick='salvar();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
    <div id="div_campos" style="display: none;">
        <hr>
        <br>
        <button type="button" class="btn btn-info" onclick="cadastrar_campo();">Adicionar Novo Campo &nbsp;<span class="fa fa-plus"></span></button> (Necessário apenas se não houver arquivo de configuração)
        <br>
        <br>
        <label>Campos do Modelo:</label>
        <br>
        <table id="tbl_campos" class="table table-condensed table-striped table-bordered table-hover" style="font-size: 80%;">
            <thead>
                <tr>
                    <th>Ord</th>
                    <th>Nome</th>
                    <th>Formato</th>
                    <th>Tamanho</th>
                    <th>Descrição</th>
                    <th>Requerido</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="tb_campos">
                
            </tbody>
        </table>
        <br>
        <br>
    </div>
</div>

<div id="popup_campo" title="Cadastro de Campo" style="overflow: hidden; margin-bottom: 40px;" >
    <form id="form_campo" name="form_campo" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="Coluna">Nome do Campo:</label>
                    <input id="Coluna" name="Coluna" type="text" class="form-control input-sm" maxlength="200">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="Formato">Formato:</label>
                    <select name="Formato" id="Formato" class="form-control input-sm">
                        <option value="-1"></option>
                        <option value="texto">Texto</option>
                        <option value="data">Data</option>
                        <option value="moeda">Moeda</option>
                        <option value="decimal">Decimal</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="Tamanho">Tamanho:</label>
                    <input id="Tamanho" name="Tamanho" type="text" class="form-control input-sm" value="50">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="Descricao">Descrição:</label>
                    <input id="Descricao" name="Descricao" type="text" class="form-control input-sm" maxlength="1000">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="Requerido">Requerido:</label>
                    <select name="Requerido" id="Requerido" class="form-control input-sm">
                        <option value="-1"></option>
                        <option value="1">Sim</option>
                        <option value="0">Não</option>
                    </select>
                </div>
            </div>
        </div>
        
        <input type="hidden" name="CodCampo" id="CodCampo" value="" />
        
    </form>
    
    <hr>
    <div id="div_confirmar" style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick='$("#popup_campo").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        <button type="button" class="btn btn-success" onclick='salvar_campo();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
    
</div>


<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');