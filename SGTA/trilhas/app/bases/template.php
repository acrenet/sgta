<?php

/*
 * Tipos de formato:
 * texto, data, moeda, decimal
 * Para data e moeda o tamanho não é observado, preencher com aspas duplas "".
 */

$var[] = array(
            "coluna" => "CPF_CNPJ_Proc",
            "formato" => "decimal",
            "tamanho" => 18,
            "descrição" => "CPF do Servidor, somente números.",
            "requerido" => "sim"
            );
            
$var[] = array(
            "coluna" => "Nome_Descricao",
            "formato" => "texto",
            "tamanho" => 300,
            "descrição" => "Nome do Servidor",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "CodOrgao",
            "formato" => "decimal",
            "tamanho" => 6,
            "descrição" => "Código do Órgão no RHNET.",
            "requerido" => "sim"
        );

$var[] = array(
            "coluna" => "Nome_Orgao",
            "formato" => "texto",
            "tamanho" => 200,
            "descrição" => "Nome do Órgão.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Data_Cadastro",
            "formato" => "data",
            "tamanho" => "",
            "descrição" => "Padrão brasileiro: dd/mm/aaaa.",
            "requerido" => "não"
        );

$var[] = array(
            "coluna" => "Saldo_Devedor",
            "formato" => "moeda",
            "tamanho" => "",
            "descrição" => "Padrão brasileiro. Ex: 6.150,38",
            "requerido" => "não"
        );

