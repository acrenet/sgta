<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if ($__autenticado == false) {
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");
require_once("../../obj/registros.php");

$obj_registros = new registros();

if (isset($_POST['operacao'])) {
    $operacao = $_POST['operacao'];
} else {
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {

    $unidade_controle = $_SESSION['unidade_controle'];

    switch ($array["operacao"]) {

          //SGTA 173
          case "load_registros_removidos":                    
           
            $CodTrilha = $_POST['CodTrilha'];
            $detalhe = $_REQUEST['detalhe'];
   
            $obj_registros->consulta_registros_excluidos($CodTrilha);
        
            if($obj_registros->erro != ""){
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $query = $obj_registros->query;
         
            gera_tabela_registros_excluidos($query,$detalhe);
        
            
            retorno();
            break;

        case "observacao_cge":                   //----------------------------------------------------------------------------------------------
            $CodRegistro = $_POST['CodRegistro'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $registro[] = mysqli_fetch_assoc($obj_registros->query);

            $array['registro'] = $registro;

            retorno();
            break;
        case "gravar_observacao_cge":
           
            $CodRegistro = $_POST['CodRegistro'];
            $ObsCGE = $_POST['obs'];
            $agrupar = $_POST['agrupar'];
            $array['rowindex'] = $_POST['rowindex'];
            $rowdata = json_decode($_POST['rowdata']);

            $qtdcolunas = count($rowdata);
           
            $status = $obj_registros->consulta_status($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            if ($status == "concluído") {
                $array['erro'] = "Operação inválida 1.";
                retorno();
            }

            $obj_registros->obs_cge($CodRegistro, $ObsCGE, 0);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $ObsCGE = '<span title="Cadastrado por: ' . $_SESSION['sessao_nome'] . '">' . $ObsCGE . '</span>';

            $rowdata[$qtdcolunas - 4] = $ObsCGE; //Observação CGE
            $array['rowdata'] = $rowdata;

            retorno();
            break;
        case "listar_anexos":
            $CodRegistro = $_POST['CodRegistro'];

            $cpf = $_SESSION['sessao_id'];
            $perfil = $_SESSION['sessao_perfil'];

            if ($perfil > 3) {
                $obj_registros->consulta_registro($CodRegistro);
                if ($obj_registros->erro != "") {
                    $array['erro'] = $obj_registros->erro;
                    retorno();
                }
                $query = $obj_registros->query;
                $row = mysqli_fetch_array($query);

                if ($row["StatusRegistro"] == "em análise" || $row["StatusRegistro"] == "concluído") {
                    $exibir = false;
                } else {
                    $exibir = true;
                }
            } else {
                $exibir = true;
            }

            $anexos = $obj_registros->listar_anexos($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $array['anexos'] = "";

            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();

            if ($anexos[0]['qtd'] > 0) {
                for ($index = 0; $index < count($anexos); $index++) {
                    if ($anexos[$index]["url"] != "") {

                        $nome = $obj_usuario->consulta_nome($anexos[$index]['Usuario']);

                        if ($exibir == true) {
                            $CodAnexo = base64_encode($anexos[$index]['CodAnexo'] * 399);

                            if ($cpf == $anexos[$index]['Usuario']) {
                                $excluir = '<a href="#" class="vermelho" title="Excluir o anexo" onclick="return excluir_arquivo(\'' . $CodAnexo . '\')"><span class="fa fa-times-circle"></span></a>';
                            } else {
                                $excluir = '<span style="color: LightGrey;" class="fa fa-times-circle"></span>';
                            }
                        } else {
                            $excluir = '<span style="color: LightGrey;" class="fa fa-times-circle"></span>';
                        }

                        $array['anexos'] = $array['anexos'] .
                            '<tr>
                                <td>
                                    <a href="' . $anexos[$index]['url'] . '" target="_blank">' . $anexos[$index]['NomeAnexo'] . '</a>&nbsp
                                    ' . $excluir . '    
                                </td>
                                <td>' . $anexos[$index]['DescricaoAnexo'] . '</td>
                                <td>' . $nome . '</td>
                                <td>' . $anexos[$index]['Etapa'] . '</td>
                                <td>' . data_br($anexos[$index]['Horario']) . '</td>
                            </tr>';
                    }
                }
            }

            retorno();
            break;
        case "recusar_trilha":
            $CodTrilha = $_POST['CodTrilha'];

            require_once("../../obj/trilhas.php");
            $obj_trilhas = new trilhas();
            $obj_trilhas->altera_status($CodTrilha, "pendente");
            if ($obj_trilhas->erro != "") {
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }

            $af = $obj_registros->alterar_status($CodTrilha, "rascunho");
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $obj_trilhas->consulta_trilha($CodTrilha);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $query = $obj_trilhas->query;
            $row = mysqli_fetch_array($query);

            require_once("../../obj/email.php");
            $obj_email = new email();

            //$obj_email->enviarEmails("Devolução de Trilha", "Uma trilha em homologação foi devolvida, consulte o sistema para mais detalhes.", $unidade_controle, 1, $row['CodTipo'], 10);
            $obj_email->enviarEmail("Devolução de Trilha", "Uma trilha em homologação foi devolvida, consulte o sistema para mais detalhes.", "nie@tce.sc.gov.br");
            //sgta-20 enviar mensagem individual?
            $obj_email->enviarMensagens("Devolução de Trilha", "Uma trilha em homologação foi devolvida, consulte o sistema para mais detalhes.", $unidade_controle, 1, $row['CodTipo']);

            retorno();
            break;

        case "enviar_registro":

            require_once '../../obj/ordem_servico.php';
            $obj_os = new ordem_servico();
            // SGTA 173 - distribui individualmente o registro e notifica

            $CodRegistro = $_POST['CodRegistroContinua'];
   
            $obj_registros->consulta_registro($CodRegistro);
 
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $query = $obj_registros->query;
            $dados = [];
            while ($rowDados = mysqli_fetch_array($query)) {
                $dados[] = $rowDados;
            }
            //die(var_dump($dados));
            $row = null;

            foreach(   $dados as  $row ){

            
                $obj_os->exclui_os_registro($row['CodRegistro']);
                if ($obj_os->erro != "") {
                    $array['erro'] = $obj_os->erro;
                    retorno();
                }
           
    
                $obj_os->cria_os_registro($row['CodRegistro']);
                if ($obj_os->erro != "") {
                    $array['erro'] = $obj_os->erro;
                    retorno();
                }
       
                $obj_registros->mudar_status($row['CodRegistro'],'pendente');

               
                if ($obj_registros->erro != "") {
                    $array['erro'] = $obj_registros->erro;
               
                    retorno();
                 
                }
           
                
                $erro = "";
                $alerta = "";
                $msg = "";

                require_once("../../obj/email.php");
                $obj_email = new email();
             
                $assunto = "Novo registro de auditoria disponível.";
                $mensagem = "Sr(a) Supervisor(a), uma novo registro de auditoria em área de sua competência encontra-se disponível para análise no Sistema de Trilha de Auditoria.";
                $mensagem = $mensagem . "<br><br><i>Informações do Registro:</i><br><br>";
                $mensagem = $mensagem . "Código: <b>" . $row['CodRegistro']  . "</b><br>";
                $mensagem = $mensagem . "CPF/CNPJ: <b>" . $row['CPF_CNPJ_Proc']  . "</b><br>";
                $mensagem = $mensagem . "Nome: <b>" . $row['Nome_Descricao']  . "</b><br>";
                $mensagem = $mensagem . "É de responsabilidade do Supervisor responder ou cadastrar/designar quais Analistas ficarão responsáveis pela justificativa.<br><br>";
                $mensagem = $mensagem . "Para acessar o sistema " . '<a href = "' . $_SESSION['virtual'] . '#/sgta" target = "_blank">clique aqui.</a><br><br>';
                $mensagem = $mensagem . "<i>Este é uma mensagem automática, em caso de dúvidas entre em contato com o suporte.</i>";

                $obj_email->enviarEmail($assunto, $mensagem, $row['CodOrgao'], 5, $row['CodTipo'], 'individual');
               // $obj_email->enviarMensagens($assunto, $mensagem, $row['CodOrgao'], 5, $row['CodTipo']);

                if ($obj_email->erro != "") {
                    $erro = $erro . $obj_email->erro . "<br>";
                }
                $alerta = $alerta . $obj_email->alerta;
                $msg = $msg . $obj_email->mensagem;

            }
            if ($erro != "") {
                $array['erro_email'] = "Erros no envio dos e-mails:<br>" . $erro;
            } else {
                $array['erro_email'] = "";
            }

            if ($alerta != "") {
                $array['alerta_email'] = "<br><u>Problemas de comunicação com os seguintes órgãos:</u><br>" . $alerta;
            } else {
                $array['alerta_email'] = "";
            }

            $array['mensagem_email'] = "<br><u>E-mails de aviso e notificação na sala virtual foram enviados para os órgãos:</u><br>" . $msg;
           

            retorno();

        break;
        case "enviar_registros":

            $CodTrilha = $_POST['CodTrilha'];
            require_once '../../obj/ordem_servico.php';
            $obj_os = new ordem_servico();
            // SGTA 173 - distribui registros e notifica
           
            //$query  - faz a consulta dos registros  da trilha que é para distribuir
   
            $obj_registros->consulta_registros_continuos($CodTrilha);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $query = $obj_registros->query;
            $dados = [];
            while ($rowDados = mysqli_fetch_array($query)) {
                $dados[] = $rowDados;
            }
            
            $row = null;

            foreach(   $dados as  $row ){

            
                $obj_os->exclui_os_registro($row['CodRegistro']);
                if ($obj_os->erro != "") {
                    $array['erro'] = $obj_os->erro;
                    retorno();
                }
           
    
                $obj_os->cria_os_registro($row['CodRegistro']);
                if ($obj_os->erro != "") {
                    $array['erro'] = $obj_os->erro;
                    retorno();
                }
       
                $obj_registros->mudar_status($row['CodRegistro'],'pendente');

               
                if ($obj_registros->erro != "") {
                    $array['erro'] = $obj_registros->erro;
                 
                }

                
                $erro = "";
                $alerta = "";
                $msg = "";

                require_once("../../obj/email.php");
                $obj_email = new email();
             
                $assunto = "Novo registro de auditoria disponível.";
                $mensagem = "Sr(a) Supervisor(a), uma novo registro de auditoria em área de sua competência encontra-se disponível para análise no Sistema de Trilha de Auditoria.";
                $mensagem = $mensagem . "<br><br><i>Informações do Registro:</i><br><br>";
                $mensagem = $mensagem . "Código: <b>" . $row['CodRegistro']  . "</b><br>";
                $mensagem = $mensagem . "CPF/CNPJ: <b>" . $row['CPF_CNPJ_Proc']  . "</b><br>";
                $mensagem = $mensagem . "Nome: <b>" . $row['Nome_Descricao']  . "</b><br>";
                $mensagem = $mensagem . "É de responsabilidade do Supervisor responder ou cadastrar/designar quais Analistas ficarão responsáveis pela justificativa.<br><br>";
                $mensagem = $mensagem . "Para acessar o sistema " . '<a href = "' . $_SESSION['virtual'] . '#/sgta" target = "_blank">clique aqui.</a><br><br>';
                $mensagem = $mensagem . "<i>Este é uma mensagem automática, em caso de dúvidas entre em contato com o suporte.</i>";

                $obj_email->enviarEmail($assunto, $mensagem, $row['CodOrgao'], 5, $row['CodTipo'], 'individual');
               // $obj_email->enviarMensagens($assunto, $mensagem, $row['CodOrgao'], 5, $row['CodTipo']);

                if ($obj_email->erro != "") {
                    $erro = $erro . $obj_email->erro . "<br>";
                }
                $alerta = $alerta . $obj_email->alerta;
                $msg = $msg . $obj_email->mensagem;

            }
            if ($erro != "") {
                $array['erro_email'] = "Erros no envio dos e-mails:<br>" . $erro;
            } else {
                $array['erro_email'] = "";
            }

            if ($alerta != "") {
                $array['alerta_email'] = "<br><u>Problemas de comunicação com os seguintes órgãos:</u><br>" . $alerta;
            } else {
                $array['alerta_email'] = "";
            }

            $array['mensagem_email'] = "<br><u>E-mails de aviso e notificação na sala virtual foram enviados para os órgãos:</u><br>" . $msg;

            retorno();
            
            break;
        case "enviar_trilha":

            require_once("../../obj/trilhas.php");
            $obj_trilhas = new trilhas();


            $CodTrilha = $_POST['CodTrilha'];
            $Instrucao = $_POST['Instrucao'];
            $NomeTrilha = $_POST['NomeTrilha'];

            require_once '../../obj/ordem_servico.php';
            $obj_os = new ordem_servico();


            $obj_os->exclui_os($CodTrilha);
            if ($obj_os->erro != "") {
                $array['erro'] = $obj_os->erro;
                retorno();
            }


            $obj_os->cria_os($CodTrilha);
            if ($obj_os->erro != "") {
                $array['erro'] = $obj_os->erro;
                retorno();
            }

            $obj_trilhas->altera_status($CodTrilha, "distribuído", $Instrucao);
            if ($obj_trilhas->erro != "") {
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }

            $af = $obj_registros->alterar_status($CodTrilha, "pendente");
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $array['qtd_registros'] = $af;

            $obj_trilhas->consulta_trilha($CodTrilha);
            if ($obj_trilhas->erro != "") {
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }

            $query = $obj_trilhas->query;
            $row = mysqli_fetch_array($query);
            $CodTipo = $row['CodTipo'];
            $Area = $row['DescricaoTipo'];
            $NomeTrilha = $row['NomeTrilha'];

            $obj_trilhas->consulta_distribuicao($CodTrilha);
            $query = $obj_trilhas->query;

            $erro = "";
            $alerta = "";
            $msg = "";

            require_once("../../obj/email.php");
            $obj_email = new email();
            // SGTA-20 - enviar notificação de envento aqui

            while ($row = mysqli_fetch_array($query)) {
                $assunto = "Nova trilha de auditoria disponível.";
                $mensagem = "Sr(a) Supervisor(a), uma nova trilha de auditoria em área de sua competência encontra-se disponível para análise no Sistema de Trilha de Auditoria.";
                $mensagem = $mensagem . "<br><br><i>Informações da Trilha:</i><br><br>";
                $mensagem = $mensagem . "Código: <b>$CodTrilha</b><br>";
                $mensagem = $mensagem . "Área: <b>$Area</b><br>";
                $mensagem = $mensagem . "Nome: <b>$NomeTrilha</b><br>";
                $mensagem = $mensagem . "Inconsistências Encontradas: <b>" . $row['QtdRegistros'] . "</b><br>";
                $mensagem = $mensagem . "É de responsabilidade do Supervisor responder ou cadastrar/designar quais Analistas ficarão responsáveis pela justificativa.<br><br>";
                $mensagem = $mensagem . "Para acessar o sistema " . '<a href = "' . $_SESSION['virtual'] . '#/sgta" target = "_blank">clique aqui.</a><br><br>';
                $mensagem = $mensagem . "<i>Este é uma mensagem automática, em caso de dúvidas entre em contato com o suporte.</i>";

                $obj_email->enviarEmails($assunto, $mensagem, $row['CodOrgao'], 5, $CodTipo, 'individual');
                $obj_email->enviarMensagens($assunto, $mensagem, $row['CodOrgao'], 5, $CodTipo);

                if ($obj_email->erro != "") {
                    $erro = $erro . $obj_email->erro . "<br>";
                }
                $alerta = $alerta . $obj_email->alerta;
                $msg = $msg . $obj_email->mensagem;

                //$obj_email->enviarEmails($assunto, $mensagem, $row['CodOrgao'], 6, $CodTipo);
            }

            if ($erro != "") {
                $array['erro_email'] = "Erros no envio dos e-mails:<br>" . $erro;
            } else {
                $array['erro_email'] = "";
            }

            if ($alerta != "") {
                $array['alerta_email'] = "<br><u>Problemas de comunicação com os seguintes órgãos:</u><br>" . $alerta;
            } else {
                $array['alerta_email'] = "";
            }

            $array['mensagem_email'] = "<br><u>E-mails de aviso e notificação na sala virtual foram enviados para os órgãos:</u><br>" . $msg;

            retorno();
            break;
        case "salvar_instrucao":
            $CodTrilha = $_POST['CodTrilha'];
            $Instrucao = $_POST['Instrucao'];

            require_once("../../obj/trilhas.php");
            $obj_trilhas = new trilhas();
            $obj_trilhas->salvar_instrucao($CodTrilha, $Instrucao);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            retorno();
            break;
        case "rejeitar_registro":

            $uxCPF = $_SESSION['sessao_id'];
            $CodRegistro = $_POST['CodRegistro'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $row = mysqli_fetch_array($obj_registros->query);

            if ($row['StatusRegistro'] == "concluído") {
                throw new Exception("Ação inválida para o status do registro.");
            }

            $obj_registros->mudar_status($CodRegistro, "excluído");
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            //SGTA 173 trilha continua
            //gravar na tabela a exclusão do registro

            $obj_registros->registra_exclusao_registro($CodRegistro,$row['ObsCGE'] ,$row['CodTrilha'] ,$row['CodOrgao'] ,$row['CPF_CNPJ_Proc'],$uxCPF );

        

            retorno();
            break;
        case "reincluir_registro":
            $CodRegistro = $_POST['CodRegistro'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $row = mysqli_fetch_array($obj_registros->query);

            if ($row['StatusRegistro'] == "concluído") {
                throw new Exception("Ação inválida para o status do registro.");
            }

            $obj_registros->mudar_status($CodRegistro, "pré-análise");
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            retorno();
            break;
        case "excluir_anexo":
            $CodAnexo = $_POST['CodAnexo'];

            require_once("../../obj/trilhas.php");
            $obj_trilhas = new trilhas();
            $obj_trilhas->excluir_anexo($CodAnexo);
            if ($obj_trilhas->erro != "") {
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }

            retorno();
            break;
        case "excluir_arquivo":
            $CodAnexo = $_POST['CodAnexo'];
            $CodAnexo = base64_decode($CodAnexo) / 399;

            $obj_registros->excluir_anexo($CodAnexo);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            retorno();
            break;
        case "justificar_inconsistencia":
            //andre - ver o poque anexar arquivo comprobatorio não esta descriptografando, tem que ver se isso é um desenvolvimento não concluido
            $CodRegistro = $_POST['CodRegistro'];
            // $CodRegistro = base64_decode($CodRegistro);
            // $CodRegistro = substr($CodRegistro, (strlen($CodRegistro) -1) * -1);
            //  $CodRegistro = $CodRegistro / 1378;

            $cpf_sessao = $_SESSION['sessao_id'];
            $tipo = $_POST['tipo'];
            $array['rowindex'] = $_POST['rowindex'];
            $rowdata = json_decode($_POST['rowdata']);

            $qtdcolunas = count($rowdata);

            require_once("../../obj/justificativas.php");
            $obj_justificativas = new justificativas();

            $reg = $obj_justificativas->consultar_justificativas($CodRegistro, "Justificativa de Inconsistência");
            if ($obj_justificativas->erro != "") {
                $array['erro'] = $obj_justificativas->erro;
                retorno();
            }

            $justificativasalva = '~!';

            if ($reg[0]['qtd'] > 0) {
                $justificativasalva = $reg[0]['Just'];
            }

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query  = $obj_registros->query;

            $row[] = mysqli_fetch_assoc($query);

            if ($row[0]['JustificativaOrgao'] == $justificativasalva) {
                $row[0]['JustificativaOrgao'] = '  *** a justificativa que havia aqui foi arquivada, você pode consultar todas as justificativas e pareceres deste registro clicando no link "Justificativas/Pareceres" logo abaixo. ***';
            }

            $cpf_reserva = $row[0]['ReservadoPor'];

            $array['mensagem'] = "";

            $array['tipo'] = $tipo;

            $array['recarregar'] = false;

            if ($tipo == "sem reserva") {
                if ($cpf_reserva != "") {
                    $array['mensagem'] = "Atenção: Este registro foi reservado por outro usuário.";
                    retorno();
                }
                $obj_registros->reservar_registro($CodRegistro, $cpf_sessao, "em justificativa");
                if ($obj_registros->erro != "") {
                    $array['erro'] = $obj_registros->erro;
                    retorno();
                }
                $array['recarregar'] = true;
            } else  if ($tipo == "reserva propria") {
                if ($cpf_sessao != $cpf_reserva) {
                    $array['mensagem'] = "Atenção: Outro usuário retirou a reserva de seu nome.";
                    retorno();
                }
            } else  if ($tipo == "reservado por outro") {
                $obj_registros->reservar_registro($CodRegistro, $cpf_sessao, "em justificativa");
                if ($obj_registros->erro != "") {
                    $array['erro'] = $obj_registros->erro;
                    retorno();
                }
                $array['recarregar'] = true;
            }

            if ($array['recarregar'] == true) {
                $rowdata[$qtdcolunas - 4] = "<b>" . $_SESSION['sessao_nome'] . "</b>"; //Reservado Por
                $rowdata[$qtdcolunas - 3] = "<span style='color: darkblue;'>em justificativa</span>"; //Status
            }

            if ($row[0]['JustificativaConcluida'] == true && $row[0]['StatusRegistro'] != "devolvido") {
                $array['mensagem'] = "Atenção: Este registro teve a justificativa concluída por outro usuário.";
                retorno();
            }

            $row[0]['ValorADevolver'] = numero_br($row[0]['ValorADevolver']);

            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();

            if ($row[0]['UsuarioOrgao'] != "") {
                $row[0]['UsuarioOrgao'] = $obj_usuario->consulta_nome($row[0]['UsuarioOrgao']);
            }

            $array['rowdata'] = $rowdata;
            $array['registro'] = $row;

            retorno();
            break;
        case "consultar_justificativa":
            $CodRegistro = $_POST['CodRegistro'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query  = $obj_registros->query;

            $row[] = mysqli_fetch_assoc($query);

            $row[0]['ValorADevolver'] = numero_br($row[0]['ValorADevolver']);

            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();

            if ($row[0]['UsuarioOrgao'] != "") {
                $row[0]['UsuarioOrgao'] = $obj_usuario->consulta_nome($row[0]['UsuarioOrgao']);
            }

            if ($row[0]['DataConclusaoJustificativa'] != "") {
                $row[0]['UsuarioOrgao'] = $row[0]['UsuarioOrgao'] . " em " . data_br($row[0]['DataConclusaoJustificativa']);
            }

            $array['registro'] = $row;

            retorno();
            break;
        case "concluir_analise": // SGTA-97 ANDRE ATENÇÃO
            $CodRegistro = $_POST['CodRegistro'];
            // $CodRegistro = base64_decode($CodRegistro);
            // $CodRegistro = substr($CodRegistro, (strlen($CodRegistro) -1) * -1);
            // $CodRegistro = $CodRegistro / 1378;

            $CodParecer = $_POST['parecer'];
            $JustificativaOrgao = $_POST['obs'];
            $ValorADevolver = $_POST['valor_a_devolver'];

            $JustificativaOrgao = trim($JustificativaOrgao);
            if ($JustificativaOrgao == "") {
                $array['erro'] = $array['erro'] . "Preencha o campo Justificativa.<br>";
            }

            $ValorADevolver = str_replace(".", "", $ValorADevolver);
            $ValorADevolver = str_replace(",", ".", $ValorADevolver);

            if ($CodParecer == 6) {
                $array['erro'] = $array['erro'] . "Selecione um Parecer no Combo.<br>";
            }

            if ($CodParecer == 2 || $CodParecer == 4) {
                if (!is_numeric($ValorADevolver)) {
                    $array['erro'] = $array['erro'] . "O Valor a Devolver é Inválido.<br>";
                } elseif ($ValorADevolver <= 0) {
                    $array['erro'] = $array['erro'] . "O Valor a Devolver é Inválido.<br>";
                }
            } else {
                $ValorADevolver = 0;
            }

            if ($array['erro'] != "") {
                retorno();
            }

            $status = $obj_registros->consulta_status($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            if ($status != "em justificativa") {
                $array['erro'] = "Operação inválida 2.";
                retorno();
            }

            $obj_registros->salvar_justificativa($CodRegistro, $CodParecer, $JustificativaOrgao, $ValorADevolver, true);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            require_once("../../obj/email.php");
            $obj_email = new email();

            try {
                $obj_registros->consulta_registro($CodRegistro);
                if ($obj_registros->erro == "") {
                    $query = $obj_registros->query;
                    $row = mysqli_fetch_array($query);

                    if ($row['UsuarioCGE'] == "") { //nova resposta, envia e-mail aos gestores.

                        $assunto = "Nova Justificativa de Inconsistência. Trilha Id. " . $row['CodTrilha'] . ", CPF/CNPJ/Processo: " . $row['CPF_CNPJ_Proc'] . ".";
                        $mensagem = "Um registro de inconsistência foi justificado e está aguardando a distribuição para um Auditor.<br><br>";
                        $mensagem = $mensagem . "Trilha: <b>" . $row['NomeTrilha'] . "</b><br>";
                        $mensagem = $mensagem . "Órgão/Unidade: <b>" . $row['NomeOrgao'] . "</b><br>";
                        $mensagem = $mensagem . "Para acessar o sistema " . '<a href = "' . $_SESSION['virtual'] . '#/sgta" target = "_blank">clique aqui.</a><br><br>';

                        $CodTipo = $row['CodTipo'];
                        //andre - ultimo paramentro, individual ou grupo
                        $obj_email->enviarEmails($assunto, $mensagem, $unidade_controle, 2, $CodTipo, 'individual');
                        //SGTA-20
                        $obj_email->enviarMensagens($assunto, $mensagem, $unidade_controle, 2, $CodTipo);
                    } else { //devolução, envia e-mail ao auditor responsável.
                        require_once("../../obj/usuario.php");
                        $obj_usuario = new usuario();
                        $obj_usuario->consulta_nome($row['UsuarioCGE']);
                        $emaildestinatario = $obj_usuario->email;

                        $assunto = "Um registro de inconsistência devolvido por você foi respondido.";
                        $mensagem = "Detalhes: <br>";
                        $mensagem = $mensagem . "Trilha: <b>" . $row['NomeTrilha'] . "</b><br>";
                        $mensagem = $mensagem . "Órgão/Unidade: <b>" . $row['NomeOrgao'] . "</b><br>";
                        $mensagem = $mensagem . "CPF/CNPJ/Processo: <b>" . $row['CPF_CNPJ_Proc'] . "</b><br>";
                        $mensagem = $mensagem . "Grato. ";

                        $obj_email->enviarEmail($assunto, $mensagem, $emaildestinatario);
                    }
                } else {
                    $obj_email->enviarEmail("Erro no script 02", $obj_registros->erro, "nie@tce.sc.gov.br");
                }
            } catch (Exception $exc) {
                $array['erro'] = $exc->getMessage();
                retorno();
                //$obj_email->enviarEmail("Erro no script 01", $exc->getMessage(), "<email adm do sistema>");
            }

            require_once("../../obj/trilhas.php");
            $obj_trilhas = new trilhas();
            $obj_trilhas->verifica_status(0, 0, $CodRegistro);
            if ($obj_trilhas->erro != "") {
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }

            require_once("../../obj/justificativas.php");
            $obj_justificativas = new justificativas();

            $obj_justificativas->incluir_justificativa($CodRegistro, "Justificativa de Inconsistência", $JustificativaOrgao);
            if ($obj_justificativas->erro != "") {
                $array['erro'] = $obj_justificativas->erro;
                retorno();
            }

            $array['mensagem'] = "Justificativa de Inconsistência salva e concluída com sucesso.";

            retorno();
            break;
        case "salvar_analise":
            $CodRegistro = $_POST['CodRegistro'];
            //$CodRegistro = base64_decode($CodRegistro);
            // $CodRegistro = substr($CodRegistro, (strlen($CodRegistro) -1) * -1);
            // $CodRegistro = $CodRegistro / 1378;

            $CodParecer = $_POST['parecer'];
            $JustificativaOrgao = $_POST['obs'];
            $ValorADevolver = $_POST['valor_a_devolver'];

            $ValorADevolver = str_replace(".", "", $ValorADevolver);
            $ValorADevolver = str_replace(",", ".", $ValorADevolver);

            if (!is_numeric($ValorADevolver)) {
                $array['erro'] = $array['erro'] . "O Valor a Devolver é Inválido.<br>";
            } elseif ($ValorADevolver < 0) {
                $array['erro'] = $array['erro'] . "O Valor a Devolver é Inválido.<br>";
            }

            if ($array['erro'] != "") {
                retorno();
            }

            $obj_registros->salvar_justificativa($CodRegistro, $CodParecer, $JustificativaOrgao, $ValorADevolver, false);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $array['mensagem'] = "Justificativa de Inconsistência salva com sucesso.";

            retorno();
            break;
        case "analisar_justificativa":
            $CodRegistro = $_POST['CodRegistro'];
            $CodTrilha = $_POST['CodTrilha'];
            $cpf_sessao = $_SESSION['sessao_id'];
            $tipo = $_POST['tipo'];


            require_once("../../obj/justificativas.php");
            $obj_justificativas = new justificativas();

            $reg = $obj_justificativas->consultar_justificativas($CodRegistro, "Análise de Justificativa");
            if ($obj_justificativas->erro != "") {
                $array['erro'] = $obj_justificativas->erro;
                retorno();
            }

            $justificativasalva = '~!';

            if ($reg[0]['qtd'] > 0) {
                $justificativasalva = $reg[0]['Just'];
            }

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query  = $obj_registros->query;

            $row[] = mysqli_fetch_assoc($query);

            if ($row[0]['JustificativaCGE'] == $justificativasalva) {
                $row[0]['JustificativaCGE'] = '  *** o parecer que havia aqui foi arquivado, você pode consultar todas as justificativas e pareceres deste registro clicando no link "Justificativas/Pareceres" logo abaixo. ***';
            } else {
                $row[0]['JustificativaCGE'] = $row[0]['JustificativaCGE'];
            }

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $cpf_reserva = $row[0]['ReservadoPor'];

            $array['mensagem'] = "";

            $array['tipo'] = $tipo;

            $array['recarregar'] = false;

            if ($tipo == "sem reserva") {
                if ($cpf_reserva != "") {
                    $array['mensagem'] = "Atenção: Este registro foi reservado por outro usuário.";
                    retorno();
                }
                $obj_registros->reservar_registro($CodRegistro, $cpf_sessao, "em análise");
                if ($obj_registros->erro != "") {
                    $array['erro'] = $obj_registros->erro;
                    retorno();
                }
                $array['recarregar'] = true;
            } else  if ($tipo == "reserva propria") {
                if ($cpf_sessao != $cpf_reserva) {
                    $array['mensagem'] = "Atenção: Outro usuário retirou a reserva de seu nome.";
                    retorno();
                }
                if ($row[0]['StatusRegistro'] == "justificado") {
                    $obj_registros->reservar_registro($CodRegistro, $cpf_sessao, "em análise", true);
                    if ($obj_registros->erro != "") {
                        $array['erro'] = $obj_registros->erro;
                        retorno();
                    }
                    $array['recarregar'] = true;
                }
            } else  if ($tipo == "reservado por outro") {
                $obj_registros->reservar_registro($CodRegistro, $cpf_sessao, "em análise");
                if ($obj_registros->erro != "") {
                    $array['erro'] = $obj_registros->erro;
                    retorno();
                }
                $array['recarregar'] = true;
            }

            //SGTA-84
            if ($row[0]['AnaliseConcluida'] == true && $row[0]['StatusRegistro'] == "concluído") {
                $array['mensagem'] = "Atenção: Este registro teve a análise concluída por outro usuário.";
                retorno();
            }

            $row[0]['ValorADevolver'] = numero_br($row[0]['ValorADevolver']);
            $row[0]['DataConclusaoJustificativa'] = data_br($row[0]['DataConclusaoJustificativa']);
            $row[0]['QuarentenaAte'] = data_br($row[0]['QuarentenaAte']);

            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();

            if ($row[0]['UsuarioOrgao'] != "") {
                $row[0]['UsuarioOrgao'] = $obj_usuario->consulta_nome($row[0]['UsuarioOrgao']) . ". Fone: " . $obj_usuario->telefone . ". E-mail: " . $obj_usuario->email;
            }

            if ($row[0]['UsuarioCGE'] != "") {
                $row[0]['UsuarioCGE'] = $obj_usuario->consulta_nome($row[0]['UsuarioCGE']) . ". Fone: " . $obj_usuario->telefone . ". E-mail: " . $obj_usuario->email;
            }

            if ($row[0]['ReservadoPor'] != "") {
                $row[0]['ReservadoPor'] = $obj_usuario->consulta_nome($row[0]['ReservadoPor']);
            }

            $row[0]['JustificativaOrgao'] = str_replace("\r", '<br>', $row[0]['JustificativaOrgao']);

            $row[0]['ValorRessarcido'] = numero_br($row[0]['ValorRessarcido']);
            $row[0]['EconomiaMensal'] = numero_br($row[0]['EconomiaMensal']);

            $cpf = $_SESSION['sessao_id'];
            $obj_registros->consulta_reservados($cpf, $CodTrilha);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query = $obj_registros->query;

            $reg = "";

            if (mysqli_num_rows($query) > 0) {
                while ($row2 = mysqli_fetch_array($query)) {
                    if ($row2['CodRegistro'] != $CodRegistro) {
                        $reg = $reg .
                            '<input type="checkbox" name="regsel[]" value="' . $row2['CodRegistro'] . '" /> <a href="#" onclick="return detalhes(\'' . $row2['CodRegistro'] . '\');" title="Ver detalhes do registro">' . $row2['CPF_CNPJ_Proc'] . ' &nbsp; ' . $row2['Nome_Descricao'] . '</a><br>';
                    }
                }
            }

            $array['regs'] = $reg;

            $array['registro'] = $row;

            retorno();
            break;
        case "salvar_validacao":
            //SGTA-42 
            $CodRegistro = $_POST['CodRegistro'];
            $comando = $_POST['comando'];
            $JustificativaCGE = $_POST['parecer'];
            $NumProcAuditoria = $_POST['numprocauditoria'];

            $Instrumentos = $_POST['instrumentoslegais'];
            $Sistemas = $_POST['sistemasgerenciais'];
            $Capacitacao = $_POST['capacitacao'];
            $Devolucao = $_POST['devolucao'];
            $ValorRessarcido = $_POST['valorressarcido'];
            $EconomiaMensal = $_POST['economiamensal'];

            $InstrumentosLegaisDesc = $_POST['instrumentoslegaisdesc'];
            $SistemasGerenciaisDesc = $_POST['sistemasgerenciaisdesc'];
            $CapacitacaoDesc = $_POST['capacitacaodesc'];
            $Favorecido = $_POST['favorecido'];


            $ResultadoAuditoria = "expedição=$Instrumentos[0];sistemas=$Sistemas[0];capacitação=$Capacitacao[0];devolução=$Devolucao[0]";

            if (isset($_POST['regsel'])) {
                $CodRegs = $_POST['regsel'];
            }

            $CodRegs[] = $CodRegistro;

            if (isset($_POST['dtquarentena'])) {
                $QuarentenaAte = $_POST['dtquarentena'];
                $QuarentenaAte = isdate($QuarentenaAte);
            } else {
                $QuarentenaAte = false;
            }

            $UsuarioCGE = $_SESSION['sessao_id'];
            $ResultadoFinal = $_POST['resultadofinal'];

            $JustificativaCGE = trim($JustificativaCGE);

            if ($comando != "salvar_justificativa") {
                if ($JustificativaCGE == "") {
                    $array['erro'] = $array['erro'] . "Preencha o campo ''Parecer:''<br>";
                }
            }

            if ($comando == "quarentena" && $QuarentenaAte == false) {
                $array['erro'] = $array['erro'] . "Preencha o campo ''Em Monitoramento Até:''<br>";
            }

            $ValorRessarcido = str_replace(".", "", $ValorRessarcido);
            $ValorRessarcido = str_replace(",", ".", $ValorRessarcido);
            if (is_numeric($ValorRessarcido) == false) {
                $ValorRessarcido = "0";
            }

            $EconomiaMensal = str_replace(".", "", $EconomiaMensal);
            $EconomiaMensal = str_replace(",", ".", $EconomiaMensal);
            if (is_numeric($EconomiaMensal) == false) {
                $EconomiaMensal = "0";
            }

            if ($ValorRessarcido < 0) {
                $ValorRessarcido = "0";
            }

            if ($EconomiaMensal < 0) {
                $EconomiaMensal = 0;
            }

            /*
            if($comando == "abrir_auditoria"){
                if(!is_numeric($NumProcAuditoria)){
                    $array['erro'] = $array['erro']."O número do processo de auditoria é inválido.<br>";
                }
            }
            */

            if ($array['erro'] != "") {
                retorno();
            }

            if ($comando != "abrir_auditoria" && $comando != "salvar_justificativa") {
                $NumProcAuditoria = "null";
            }

            $status = $obj_registros->consulta_status($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            if ($status != "em análise") {
                $array['erro'] = "Operação inválida.";
                retorno();
            }


            if ($comando != "salvar_justificativa") { //conclusão

                require_once("../../obj/justificativas.php");
                $obj_justificativas = new justificativas();

                for ($index = 0; $index < count($CodRegs); $index++) {
                    $CodRegistro = $CodRegs[$index];

                    $id = $obj_justificativas->incluir_justificativa($CodRegistro, "Análise de Justificativa", $JustificativaCGE);
                    if ($obj_justificativas->erro != "") {
                        $array['erro'] = $obj_justificativas->erro;
                        retorno();
                    }

                    $obj_registros->salvar_analise($comando, $CodRegistro, $JustificativaCGE, $UsuarioCGE, $QuarentenaAte, $ResultadoFinal, $NumProcAuditoria, "auditoria", $id, $ResultadoAuditoria, $ValorRessarcido, $EconomiaMensal, $InstrumentosLegaisDesc, $SistemasGerenciaisDesc, $CapacitacaoDesc, $Favorecido);
                    if ($obj_registros->erro != "") {
                        $array['erro'] = $obj_registros->erro;
                        retorno();
                    }
                }

                require_once("../../obj/trilhas.php");
                $obj_trilhas = new trilhas();
                $obj_trilhas->verifica_status(0, 0, $CodRegistro);
                if ($obj_trilhas->erro != "") {
                    $array['erro'] = $obj_trilhas->erro;
                    retorno();
                }
            } else { //apenas salvando

                $obj_registros->salvar_analise($comando, $CodRegistro, $JustificativaCGE, $UsuarioCGE, $QuarentenaAte, $ResultadoFinal, $NumProcAuditoria, "auditoria", "null", $ResultadoAuditoria, $ValorRessarcido, $EconomiaMensal, $InstrumentosLegaisDesc, $SistemasGerenciaisDesc, $CapacitacaoDesc, $Favorecido);
                if ($obj_registros->erro != "") {
                    $array['erro'] = $obj_registros->erro;
                    retorno();
                }
            }

            if ($comando != "salvar_justificativa") {
                $obj_registros->consulta_registro($CodRegistro);
                if ($obj_registros->erro != "") {
                    $array['erro'] = $obj_registros->erro;
                    retorno();
                }

                $query = $obj_registros->query;

                $row = mysqli_fetch_array($query);
                $CodTipo = $row['CodTipo'];
                $CodOrgao = $row['CodOrgao'];
                $CodPerfil = 5;

                $NomeTrilha = $row['NomeTrilha'];
                $Identificador = $row['CPF_CNPJ_Proc'];
                $Nome = $row['Nome_Descricao'];

                require_once("../../obj/usuario.php");
                $obj_usuario = new usuario();

                $obj_usuario->consulta_nome($row['UsuarioOrgao']);
                $destino = $obj_usuario->email;

                require_once("../../obj/autorizacoes.php");
                $obj_autorizacoes = new autorizacoes();

                $query = $obj_autorizacoes->consulta($CodTipo, $CodOrgao, $CodPerfil);
                if ($obj_autorizacoes->erro != "") {
                    $array['erro'] = $obj_autorizacoes->erro;
                    retorno();
                }
                //andre, ver aqui se vai grupo ou individual

                if (mysqli_num_rows($query) > 0) {
                    while ($row = mysqli_fetch_array($query)) {

                        if (strpos($destino, $row['Email']) === false) {
                            $destino = $destino . ",";
                            $destino = $destino . $row['Email'];
                        }
                    }
                }
            }

            require_once("../../obj/email.php");
            $obj_email = new email();


            if ($comando == "salvar_justificativa") {
                $array['mensagem'] = "Informações gravadas com sucesso.";
            } elseif ($comando == "recusar_justificativa") {
                $mensagem = "Uma justificativa de inconsistência foi recusada.<br><br> Trilha: $NomeTrilha. <br>Identificador: $Identificador. <br>Nome/Descrição: $Nome."
                    . "<br><br>Consulte o Sistema de Gestão de Trilhas de Auditoria para mais detalhes.";
                $obj_email->enviarEmail("Devolução de registro de inconsistência.", $mensagem, $destino);
                //stga-20 sgta-42
                $obj_email->enviarMensagens("Devolução de registro de inconsistência.", $mensagem, $CodOrgao, $CodPerfil, $CodTipo);

                $array['mensagem'] = "Justificativa recusada com sucesso. E-mails informativos e notificação na Sala Virtual foram enviados ao órgão.";
            } elseif ($comando == "quarentena") {
                $mensagem = "Uma justificativa de inconsistência foi colocado em monitoramento. Trilha: $NomeTrilha. Identificador: $Identificador. Nome/Descrição: $Nome."
                    . "<br>Consulte o Sistema de Gestão de Trilhas de Auditoria para mais detalhes.";
                $obj_email->enviarEmail("Registro de Inconsistência em Monitoramento.", $mensagem, $destino);
                $array['mensagem'] = "Justificativa aceita e colocada em monitoramento com sucesso. E-mails informativos foram enviados ao órgão.";
            } elseif ($comando == "aceitar_justificativa") {
                $array['mensagem'] = "Justificativa aceita e registro arquivado com sucesso. E-mails informativos foram enviados ao órgão.";
                $mensagem = "Uma justificativa de inconsistência foi aceita pelo TCE e o registro foi arquivado. Trilha: $NomeTrilha. Identificador: $Identificador. Nome/Descrição: $Nome."
                    . "<br>Consulte o Sistema de Gestão de Trilhas de Auditoria para mais detalhes.";
                $obj_email->enviarEmail("Registro de Inconsistência Arquivado.", $mensagem, $destino);
            } elseif ($comando == "abrir_auditoria") {
                $mensagem = "Foi recomendado a abertura de processo Inspeção/Auditoria em um registro de inconsistência. Trilha: $NomeTrilha. CPF/CNPJ/Numero Processo: $Identificador. Nome/Descrição: $Nome."
                    . "<br>Consulte o Sistema de Gestão de Trilhas de Auditoria para mais detalhes.";

                $obj_email->enviarEmail("Recomendado abertura de processo de Inspeção/Auditoria.", $mensagem, $destino);
                //andre - ver se esse email vai para grupo ou individual  
                $obj_email->enviarEmails("Recomendado abertura de processo de Inspeção/Auditoria.", $mensagem, $unidade_controle, 2, $CodTipo, 'individual');


                $array['mensagem'] = "Recomendado Abertura de Processo de Inspeção/Auditoria. E-mails informativos foram enviados ao órgão.";
            }

            retorno();
            break;
        case "andamento":
            $CodRegistro = $_POST['CodRegistro'];
            $cpf_sessao = $_SESSION['sessao_id'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query  = $obj_registros->query;

            $row[] = mysqli_fetch_assoc($query);

            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();

            if ($row[0]['UsuarioOrgao'] != "") {
                $row[0]['UsuarioOrgao'] = $obj_usuario->consulta_nome($row[0]['UsuarioOrgao']) . ". Fone: " . $obj_usuario->telefone . ". E-mail: " . $obj_usuario->email . ".";
            } else {
                $row[0]['UsuarioOrgao'] = "";
            }

            if ($row[0]['UsuarioCGE'] != "") {
                $row[0]['UsuarioCGE'] = $obj_usuario->consulta_nome($row[0]['UsuarioCGE']) . ". Fone: " . $obj_usuario->telefone . ". E-mail: " . $obj_usuario->email . ".";
            } else {
                $row[0]['UsuarioCGE'] = "";
            }

            $status = $row[0]['StatusRegistro'];

            switch ($status) {
                case "pendente":
                    $row[0]['StatusRegistro'] = "<span style='color: darkorange; font-weight: bold;'>" . $row[0]['StatusRegistro'] . "</span>";
                    break;
                case "em justificativa":
                    $row[0]['StatusRegistro'] = "<span style='color: darkblue; font-weight: bold;'>" . $row[0]['StatusRegistro'] . "</span>";
                    break;
                case "justificado":
                    $row[0]['StatusRegistro'] = "<span style='color: darkgreen; font-weight: bold;'>" . $row[0]['StatusRegistro'] . "</span>";
                    break;
                case "em análise":
                    $row[0]['StatusRegistro'] = "<span style='color: blue; font-weight: bold;'>" . $row[0]['StatusRegistro'] . "</span>";
                    break;
                case "concluído":
                    $row[0]['StatusRegistro'] = "<span style='color: magenta; font-weight: bold;'>" . $row[0]['StatusRegistro'] . "</span>";
                    break;
                case "devolvido":
                    $row[0]['StatusRegistro'] = "<span style='color: red; font-weight: bold;'>" . $row[0]['StatusRegistro'] . "</span>";
                    break;
            }

            $row[0]['ResultadoFinal'] = "";

            if ($row[0]['QuarentenaAte'] != "") {
                $row[0]['ResultadoFinal'] = "Em Monitoramento até: " .  data_br($row[0]['QuarentenaAte']);
            } else if ($row[0]['NumProcAuditoria'] != "" && $row[0]['TipoProcesso'] == "auditoria") {
                $row[0]['ResultadoFinal'] = "Auditoria. Processo: " . $row[0]['NumProcAuditoria'];
            } else if ($row[0]['NumProcAuditoria'] != "" && $row[0]['TipoProcesso'] == "inspeção") {
                $row[0]['ResultadoFinal'] = "Inspeção. Processo: " . $row[0]['NumProcAuditoria'];
            } else if ($row[0]['SolicitadoAuditoria'] == true) {
                $row[0]['ResultadoFinal'] = "Recomendado Abertura de Processo de Inspeção/Auditoria.";
            } else if ($row[0]['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>") {
                $row[0]['ResultadoFinal'] = "Arquivado";
            }

            $row[0]['JustificativaOrgao'] = str_replace("\r", '<br>', $row[0]['JustificativaOrgao']);
            $row[0]['JustificativaCGE'] = str_replace("\r", '<br>', $row[0]['JustificativaCGE']);
            $row[0]['ValorADevolver'] = numero_br($row[0]['ValorADevolver']);
            $row[0]['DataConclusaoJustificativa'] = data_br($row[0]['DataConclusaoJustificativa']);
            $row[0]['DataConclusaoAnalise'] = data_br($row[0]['DataConclusaoAnalise']);
            $row[0]['QuarentenaAte'] = data_br($row[0]['QuarentenaAte']);

            $row[0]['MotivoSolicitacao'] = str_replace("\r", '<br>', $row[0]['MotivoSolicitacao']);
            $row[0]['ParecerSolicitacao'] = str_replace("\r", '<br>', $row[0]['ParecerSolicitacao']);

            require_once("../../obj/andamentos.php");
            $obj_andamentos = new andamentos();
            $andamentos = $obj_andamentos->consultar($CodRegistro);
            if ($obj_andamentos->erro != "") {
                $array['erro'] = $obj_andamentos->erro;
                retorno;
            }

            $ocorrencias = "";

            if ($andamentos[0]['qtd'] > 0) {
                for ($index = 0; $index < count($andamentos); $index++) {
                    $ocorrencias = $ocorrencias .
                        '<tr>
                            <td>' . $andamentos[$index]['DataAndamento'] . '</td>
                            <td>' . $andamentos[$index]['Descricao'] . '</td>
                            <td>' . $andamentos[$index]['UsuarioNome'] . '</td>
                            <td>' . $andamentos[$index]['Contato'] . '</td>
                        </tr>';
                }
            }

            $row[0]['Ocorrencias'] = $ocorrencias;

            $observacoes = $obj_andamentos->consultar_observacoes($CodRegistro, true);
            if ($obj_andamentos->erro != "") {
                $array['erro'] = $obj_andamentos->erro;
                retorno;
            }

            $ocorrencias = "";

            $CodPerfil = $_SESSION['sessao_perfil'];

            if ($observacoes[0]['qtd'] > 0) {
                for ($index = 0; $index < count($observacoes); $index++) {

                    $CodObs = $observacoes[$index]['CodObs'];

                    if ($CodPerfil < 3 && $index > 0) {
                        if ($observacoes[$index]['StatusObservacao'] == 'privado') {
                            $botao = '<br><a href="#" title="Exibir Observação aos Órgãos" class="verde" onclick="return mostrar_observacao(' . $CodObs . ');"><span class="fa fa-plus-circle fa-lg"></span></a>';
                            $cor = 'style="color: red;"';
                        } else {
                            $botao = '<br><a href="#" title="Ocultar Observação para os Órgãos" class="excluir" onclick="return ocultar_observacao(' . $CodObs . ');"><span class="fa fa-trash fa-lg"></span></a>';
                            $cor = '';
                        }
                    } else {
                        if ($observacoes[$index]['StatusObservacao'] == 'privado' && $index > 0) {
                            $cor = 'style="color: red;"';
                        } else {
                            $cor = '';
                        }
                        $botao = '';
                    }

                    if ($observacoes[$index]['StatusObservacao'] == 'público' || $CodPerfil < 5 || $index == 0) {
                        $ocorrencias = $ocorrencias .
                            '<tr ' . $cor . '>
                                    <td>' . $observacoes[$index]['Data'] . $botao . '</td>
                                <td>' . str_replace("\r", '<br>', $observacoes[$index]['ObsCGE']) . '</td>
                                <td>' . $observacoes[$index]['Nome'] . '</td>
                                <td>' . $observacoes[$index]['Contato'] . '</td>
                            </tr>';
                    }
                }
            }

            $row[0]['Observacoes'] = $ocorrencias;

            $ResultadoAuditoria = explode(";", $row[0]['ResultadoAuditoria']);

            $expedicao = explode("=", $ResultadoAuditoria[0]);
            if ($expedicao[1] == "0") {
                $exp = "Não";
            } elseif ($expedicao[1] == "1") {
                $exp = "Sim, Atendido pelo Órgão";
            } else { //solicitado
                if ($row[0]['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>" && $row[0]['QuarentenaAte'] == "") {
                    $exp = "Sim, Não Atendido pelo Órgão";
                } else {
                    $exp = "Sim, Aguardando o Atendimento";
                }
            }
            $exp = $exp . ". " . $row[0]['InstrumentosLegaisDesc'];

            $sistemas = explode("=", $ResultadoAuditoria[1]);
            if ($sistemas[1] == "0") {
                $sis = "Não";
            } elseif ($sistemas[1] == "1") {
                $sis = "Sim, Atendido pelo Órgão";
            } else {
                if ($row[0]['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>" && $row[0]['QuarentenaAte'] == "") {
                    $sis = "Sim, Não Atendido pelo Órgão";
                } else {
                    $sis = "Sim, Aguardando o Atendimento";
                }
            }
            $sis = $sis . ". " . $row[0]['SistemasGerenciaisDesc'];

            $capacitacao = explode("=", $ResultadoAuditoria[2]);
            if ($capacitacao[1] == "0") {
                $cap = "Não";
            } elseif ($capacitacao[1] == "1") {
                $cap = "Sim, Atendido pelo Órgão";
            } else {
                if ($row[0]['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>" && $row[0]['QuarentenaAte'] == "") {
                    $cap = "Sim, Não Atendido pelo Órgão";
                } else {
                    $cap = "Sim, Aguardando o Atendimento";
                }
            }
            $cap = $cap . ". " . $row[0]['CapacitacaoDesc'];

            $devolucao = explode("=", $ResultadoAuditoria[3]);
            if ($devolucao[1] == "0") {
                $dev = "Não.";
            } elseif ($devolucao[1] == "1") {
                $dev = "Sim, Atendido pelo Órgão.";
            } else {
                if ($row[0]['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>" && $row[0]['QuarentenaAte'] == "") {
                    $dev = "Sim, Não Atendido pelo Órgão.";
                } else {
                    $dev = "Sim, Aguardando o Atendimento.";
                }
            }

            $resultado =    '<tr>
                                <td style="width: 50%;">Requerido Expedição de Instrumentos Legais:</td>
                                <td>' . $exp . '</td>
                            </tr>
                            <tr>
                                <td>Requerido Criação/Reformulação de Sistemas:</td>
                                <td>' . $sis . '</td>
                            </tr>
                            <tr>
                                <td>Requerido Capacitação Técnica dos Agentes:</td>
                                <td>' . $cap . '</td>
                            </tr>
                            <tr>
                                <td>Houve Devolução de Numerário:</td>
                                <td>' . $dev . '</td>
                            </tr>
                            <tr>
                                <td>Valor Ressarcido:</td>
                                <td>' . numero_br($row[0]['ValorRessarcido']) . '</td>
                            </tr>
                            <tr>
                                <td>Economia Mensal:</td>
                                <td>' . numero_br($row[0]['EconomiaMensal']) . '</td>
                            </tr>';
            $row[0]['ResultadoDaAnalise'] = $resultado;

            if ($row[0]['ResultadoFinal'] == "") {
                $row[0]['ResultadoDaAnalise'] = "";
            }

            $array['registro'] = $row;

            retorno();
            break;
        case "consulta_justificativas":
            $CodRegistro = $_POST['CodRegistro'];

            require_once("../../obj/justificativas.php");
            $obj_justificativas = new justificativas();

            $row = $obj_justificativas->consultar_justificativas($CodRegistro);
            if ($obj_justificativas->erro != "") {
                $array['erro'] = $obj_justificativas->erro;
                retorno();
            }

            $html = "";

            if ($row[0]['qtd'] > 0) {
                for ($index = 0; $index < count($row); $index++) {
                    $html = $html .
                        '<tr>
                            <td>' . $row[$index]['DataJustificativa'] . '</td>
                            <td>' . $row[$index]['TipoJustificativa'] . '</td>
                            <td>' . $row[$index]['Justificativa'] . '</td>
                            <td>' . $row[$index]['UsuarioNome'] . '</td>
                            <td>' . $row[$index]['Contato'] . '</td>
                        </tr>';
                }
            }

            $array['html'] = $html;

            retorno();
            break;
        case "solicitar_alteracao_justificativa":
            $CodRegistro = $_POST['CodRegistro'];
            $cpf = $_SESSION['sessao_id'];

            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();

            $array['nome'] = $obj_usuario->consulta_nome($cpf);

            retorno();
            break;
        case "analisar_pedido_alteracao":
            $CodRegistro = $_POST['CodRegistro'];
            $cpf = $_SESSION['sessao_id'];

            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();

            $array['nome'] = $obj_usuario->consulta_nome($cpf);

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query  = $obj_registros->query;

            $row[] = mysqli_fetch_assoc($query);

            $row[0]['SolicitadoPor'] = $obj_usuario->consulta_nome($row[0]['SolicitadoPor']);

            $array['registro'] = $row;

            retorno();
            break;
        case "gravar_pedido_alteracao":
            $CodRegistro = $_POST['CodRegistro'];
            $MotivoSolicitacao = $_POST['motivo'];

            $MotivoSolicitacao = trim($MotivoSolicitacao);
            if ($MotivoSolicitacao == "") {
                $array['alerta'] = "É obrigatório o preenchimento do motivo.";
                retorno();
            }

            $obj_registros->solicitar_alteracao($CodRegistro, $MotivoSolicitacao);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $array['mensagem'] = "Pedido de alteração gravado com sucesso.<br> Você receberá um e-mail e será notificado na Sala Virtual quando seu pedido for analisado.";

            retorno();
            break;
        case "aceitar_pedido_alteracao":
            $CodRegistro = $_POST['CodRegistro'];
            $ParecerSolicitacao = $_POST['resposta'];

            $ParecerSolicitacao = trim($ParecerSolicitacao);
            if ($ParecerSolicitacao == "") {
                $array['alerta'] = "É obrigatório o preenchimento do campo resposta.";
                retorno();
            }

            $obj_registros->responder_alteracao($CodRegistro, $ParecerSolicitacao, true);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            require_once("../../obj/trilhas.php");
            $obj_trilhas = new trilhas();
            $obj_trilhas->verifica_status(0, 0, $CodRegistro);
            if ($obj_trilhas->erro != "") {
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }

            $obj_registros->consulta_registro($CodRegistro);
            $query = $obj_registros->query;
            $row = mysqli_fetch_array($query);

            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();

            $obj_usuario->consulta_nome($row['SolicitadoPor']);
            $Email = $obj_usuario->email;

            $AnalisadoPor = $obj_usuario->consulta_nome($row['AnalisadoPor']);

            require_once("../../obj/email.php");
            $obj_email = new email();

            //SGTA-81 - adicionado o codigo na mensagem para a sala virtual
            $assunto = "Pedido de Alteração de Justificativa.";
            $mensagem = "Seu pedido de alteração de justificativa foi aceito.<br><br>"
                . "Trilha: " . $row['NomeTrilha'] . ".<br><br>"
                . "Código: " . $row['CodTrilha'] . ".<br><br>"
                . "Analisado Por: $AnalisadoPor.<br><br>"
                . "Resposta do Auditor:<br><br>" . str_replace("\r", '<br>', $ParecerSolicitacao);

            $enviado = $obj_email->enviarEmail($assunto, $mensagem, $Email);

            $obj_email->enviarMensagens($assunto, $mensagem, $row['CodOrgao'], 5, $row['CodTipo']);

            if ($enviado == true) {
                $array['mensagem'] = "Dados gravados com sucesso. Um e-mail informativo e uma notificação na Sala Virtual foram enviados ao solicitante ($Email).";
            } else {
                $array['mensagem'] = "Dados gravados com sucesso. Não foi possível enviar um e-mail ao solicitante ($Email).";
            }

            retorno();
            break;
        case "negar_pedido_alteracao":
            $CodRegistro = $_POST['CodRegistro'];
            $ParecerSolicitacao = $_POST['resposta'];

            $obj_registros->responder_alteracao($CodRegistro, $ParecerSolicitacao, false);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $obj_registros->consulta_registro($CodRegistro);
            $query = $obj_registros->query;
            $row = mysqli_fetch_array($query);

            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();

            $obj_usuario->consulta_nome($row['SolicitadoPor']);
            $Email = $obj_usuario->email;

            $AnalisadoPor = $obj_usuario->consulta_nome($row['AnalisadoPor']);

            require_once("../../obj/email.php");
            $obj_email = new email();

            //SGTA-81 - adicionado o codigo na mensagem para a sala virtual
            $assunto = "Pedido de Alteração de Justificativa.";
            $mensagem = "Seu pedido de alteração de justificativa foi negado.<br><br>"
                . "Trilha: " . $row['NomeTrilha'] . ".<br><br>"
                . "Código: " . $row['CodTrilha'] . ".<br><br>"
                . "Analisado Por: $AnalisadoPor.<br><br>"
                . "Resposta do Auditor:<br><br>" . str_replace("\r", '<br>', $ParecerSolicitacao);

            $enviado = $obj_email->enviarEmail($assunto, $mensagem, $Email);

            $obj_email->enviarMensagens($assunto, $mensagem, $row['CodOrgao'], 5, $row['CodTipo']);

            if ($enviado == true) {
                $array['mensagem'] = "Dados gravados com sucesso. Um e-mail informativo e uma notificação na Sala Virtual foram enviados ao solicitante ($Email).";
            } else {
                $array['mensagem'] = "Dados gravados com sucesso. Não foi possível enviar um e-mail ao solicitante ($Email).";
            }

            retorno();
            break;
        case "consulta_complementar":
            $CodRegistro = $_POST['CodRegistro'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $query = $obj_registros->query;
            $row = mysqli_fetch_array($query);

            /*
            if($row['StatusRegistro'] != "concluído"){
                $array['erro'] = "Operação inválida.";
                retorno();
            }
            */

            $array['não_analisado'] = false;

            if ($row['DataPublicacao'] != "") {
                if ($row["StatusRegistro"] == "pendente" || $row["StatusRegistro"] == "em justificativa") {

                    $one = new DateTime($row['DataPublicacao']);
                    $two = new DateTime();
                    $dateInterval = $one->diff($two);
                    $dias = $dateInterval->days;
                    if ($dias > 30) {

                        $array['nao_analisado'] = true;
                    }
                }
            }

            if ($row["QuarentenaAte"] == "") {
                $array["QuarentenaAte"] = "";
                $array['monitoramento'] = false;
            } else {
                $array["QuarentenaAte"] = data_br($row["QuarentenaAte"]);
                $array['monitoramento'] = true;
            }

            $array['ResultadoAuditoria'] = $row['ResultadoAuditoria'];
            $array['ValorRessarcido'] = numero_br($row['ValorRessarcido']);
            $array['EconomiaMensal'] = numero_br($row['EconomiaMensal']);

            $array['InstrumentosLegaisDesc'] = $row['InstrumentosLegaisDesc'];
            $array['SistemasGerenciaisDesc'] = $row['SistemasGerenciaisDesc'];
            $array['CapacitacaoDesc'] = $row['CapacitacaoDesc'];

            retorno();
            break;
        case "acoes_complementares":
            $CodRegistro = $_POST['CodRegistro'];
            $acao = $_POST['acao'];
            $JustificativaCGE = $_POST['parecer'];
            $QuarentenaAte = $_POST['dtquarentena'];
            $NumProcAuditoria = $_POST['numprocauditoria'];

            $QuarentenaAte = isdate($QuarentenaAte);

            $Instrumentos = $_POST['instrumentoslegais'];
            $Sistemas = $_POST['sistemasgerenciais'];
            $Capacitacao = $_POST['capacitacao'];
            $Devolucao = $_POST['devolucao'];
            $ValorRessarcido = $_POST['valorressarcido'];
            $EconomiaMensal = $_POST['economiamensal'];

            $InstrumentosLegaisDesc = $_POST['instrumentoslegaisdesc'];
            $SistemasGerenciaisDesc = $_POST['sistemasgerenciaisdesc'];
            $CapacitacaoDesc = $_POST['capacitacaodesc'];

            $ResultadoAuditoria = "expedição=$Instrumentos[0];sistemas=$Sistemas[0];capacitação=$Capacitacao[0];devolução=$Devolucao[0]";

            $ValorRessarcido = str_replace(".", "", $ValorRessarcido);
            $ValorRessarcido = str_replace(",", ".", $ValorRessarcido);
            if (is_numeric($ValorRessarcido) == false) {
                $ValorRessarcido = "0";
            }

            $EconomiaMensal = str_replace(".", "", $EconomiaMensal);
            $EconomiaMensal = str_replace(",", ".", $EconomiaMensal);
            if (is_numeric($EconomiaMensal) == false) {
                $EconomiaMensal = "0";
            }

            if ($ValorRessarcido < 0) {
                $ValorRessarcido = "0";
            }

            if ($EconomiaMensal < 0) {
                $EconomiaMensal = 0;
            }

            if ($acao == "alterar_prazo" && $QuarentenaAte == false) {
                $array['erro'] = "Preencha o campo ''Em Monitoramento Até:''";
                retorno();
            } else if ($acao == "abrir_auditoria") {
                if (!is_numeric($NumProcAuditoria)) {
                    //$array['erro'] = $array['erro']."O número do processo de auditoria é inválido.";
                    //retorno();
                }
            }

            $obj_registros->acoes_complementares($CodRegistro, $acao, $JustificativaCGE, $QuarentenaAte, $NumProcAuditoria, $ResultadoAuditoria, $ValorRessarcido, $EconomiaMensal, $InstrumentosLegaisDesc, $SistemasGerenciaisDesc, $CapacitacaoDesc);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            if ($acao != "reabrir_processo" && $acao != "invocar_processo") {
                require_once("../../obj/justificativas.php");
                $obj_justificativas = new justificativas();

                $obj_justificativas->incluir_justificativa($CodRegistro, "Análise de Justificativa", $JustificativaCGE);
                if ($obj_justificativas->erro != "") {
                    $array['erro'] = $obj_justificativas->erro;
                    retorno();
                }
            }

            retorno();
            break;
        case "ordem_servico": //consulta
            $CodRegistro = $_POST['CodRegistro'];

            require_once("../../obj/ordem_servico.php");
            $obj_os = new ordem_servico();

            $os = $obj_os->consulta_os($CodRegistro);
            if ($obj_os->erro != "") {
                $array['erro'] = $obj_os->erro;
                retorno();
            }

            if ($os != false) {

                $html = "";

                for ($index = 0; $index < count($os); $index++) {

                    if ($os[$index]['Ocorrencias'] == 0) {
                        $os[$index]['Ocorrencias'] = 1;
                        $html = $html .
                            '
                         <tr>
                            <td rowspan="' . $os[$index]["Ocorrencias"] . '" style="vertical-align: middle; width: 90px;">' . $os[$index]["NumeroOS"] . '</td>
                            <td rowspan="' . $os[$index]["Ocorrencias"] . '" style="vertical-align: middle;">' . $os[$index]["Resultado"] . '</td>
                            <td rowspan="' . $os[$index]["Ocorrencias"] . '" style="vertical-align: middle;">' . $os[$index]["Status"] . '</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        ';
                    } else {

                        $html = $html .
                            '
                         <tr>
                            <td rowspan="' . $os[$index]["Ocorrencias"] . '" style="vertical-align: middle; width: 90px;">' . $os[$index]["NumeroOS"] . '</td>
                            <td rowspan="' . $os[$index]["Ocorrencias"] . '" style="vertical-align: middle;">' . $os[$index]["Resultado"] . '</td>
                            <td rowspan="' . $os[$index]["Ocorrencias"] . '" style="vertical-align: middle;">' . $os[$index]["Status"] . '</td>
                            <td>' . $os[$index]["Registros"][0]['Data'] . '</td>
                            <td>' . $os[$index]["Registros"][0]['CPF'] . '</td>
                            <td>' . $os[$index]["Registros"][0]['Nome'] . '</td>
                            <td>' . $os[$index]["Registros"][0]['Historico'] . '.</td>
                        </tr>
                        ';

                        for ($index1 = 1; $index1 < count($os[$index]["Registros"]); $index1++) {
                            $html = $html .
                                '
                             <tr>
                                <td>' . $os[$index]["Registros"][$index1]['Data'] . '</td>
                                <td>' . $os[$index]["Registros"][$index1]['CPF'] . '</td>
                                <td>' . $os[$index]["Registros"][$index1]['Nome'] . '</td>
                                <td>' . $os[$index]["Registros"][$index1]['Historico'] . '</td>
                            </tr>
                            ';
                        }
                    }

                    $array['html'] = $html;
                }
            } else {
                $array['html'] = "<tr><td>Sem registro.</td></tr>";
            }

            //throw new Exception("--> ".$array['html']);

            retorno();
            break;
        case "mesclar_consulta":
            $CodRegistro = $_POST['CodRegistro'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query = $obj_registros->query;
            $row = mysqli_fetch_array($query);

            if ($row['StatusRegistro'] == "concluído") {
                throw new Exception("Ação inválida para o status do registro.");
            }


            $array['cpf'] = $row['CPF_CNPJ_Proc'];
            $array['nome'] = $row['Nome_Descricao'];

            $obj_registros->consulta_mesclar($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query = $obj_registros->query;

            $html = '<option value=""></option>';

            if (mysqli_num_rows($query) > 0) {
                while ($row = mysqli_fetch_array($query)) {
                    $html = $html . '<option value="' . $row['CodRegistro'] . '">' . $row['CPF_CNPJ_Proc'] . ' - ' . $row['Nome_Descricao'] . '</option>';
                }
            }

            $array['html'] = $html;

            retorno();
            break;
        case "mesclar_save":
            $CodRegistro = $_POST['CodRegistro'];
            $CodMesclar = $_POST['disponivel'];

            $obj_registros->mesclar_registros($CodRegistro, $CodMesclar);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $array['mensagem'] = "Registros mesclados com sucesso.";

            retorno();
            break;
        case "consultar_msg":
            $CodRegistro = $_POST['CodRegistro'];

            gera_tabela_mensagens($CodRegistro);

            retorno();
            break;
        case "equipe_msg":

            $CodRegistro = $_POST['CodRegistro'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $row = mysqli_fetch_array($obj_registros->query);

            require_once '../../obj/autorizacoes.php';
            $obj_autorizacoes = new autorizacoes();

            //SGTA-58
            $query = $obj_autorizacoes->consulta_equipe_msg($row['CodTipo'], $unidade_controle);
            if ($obj_autorizacoes->erro != "") {
                throw new Exception($obj_autorizacoes->erro);
            }

            $html = "";

            while ($row = mysqli_fetch_array($query)) {
                if ($row['StatusTipo'] == "ativo" && $row['CodPerfil'] < 4 && $row['CodPerfil'] > 1) {
                    $html = $html . '<input type="checkbox" name="email[]" value="' . $row['Email'] . '" /> ' . $row['Nome'] . ' (' . $row['NomePerfil'] . ')<br>';
                }
            }

            $array['html'] = $html;

            retorno();
            break;
        case "cadastrar_msg":
            $CodRegistro = $_POST['CodRegistro'];
            $Mensagem = $_POST['msg'];
            $Prioridade = $_POST['prioridade'];

            if (isset($_POST['email'])) {
                $Emails = $_POST['email'];
            } else {
                $Emails = "";
            }

            $Mensagem = trim($Mensagem);
            if ($Mensagem == "") {
                $array['Alerta'] = "A mensagem está vazia";
                retorno();
            }

            require_once("../../obj/mensagens.php");
            $obj_msg = new mensagens();

            $obj_msg->incluir_mensagem($CodRegistro, $Mensagem, $Prioridade, $Emails);
            if ($obj_msg->erro != "") {
                $array['erro'] = $obj_msg->erro;
                retorno();
            }

            gera_tabela_mensagens($CodRegistro);

            $array['mensagem'] = "Dados Gravados com sucesso.";

            retorno();
            break;
        case "editar_msg":
            $CodMensagem = $_POST['CodMensagem'];

            require_once("../../obj/mensagens.php");
            $obj_msg = new mensagens();

            $query = $obj_msg->consultar_mensagem($CodMensagem);
            if ($obj_msg->erro != "") {
                $array['erro'] = $obj_msg->erro;
                retorno();
            }

            $row = mysqli_fetch_array($query);

            $array['Mensagem'] = $row['Mensagem'];
            $array['Prioridade'] = $row['Prioridade'];

            $CodRegistro = $_POST['CodRegistro'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $row = mysqli_fetch_array($obj_registros->query);

            require_once '../../obj/autorizacoes.php';
            $obj_autorizacoes = new autorizacoes();

            $query = $obj_autorizacoes->consulta($row['CodTipo'], $unidade_controle);
            if ($obj_autorizacoes->erro != "") {
                throw new Exception($obj_autorizacoes->erro);
            }

            $html = "";

            while ($row = mysqli_fetch_array($query)) {
                if ($row['StatusTipo'] == "ativo" && $row['CodPerfil'] < 4 && $row['CodPerfil'] > 1) {
                    $html = $html . '<input type="checkbox" name="email[]" value="' . $row['Email'] . '" /> ' . $row['Nome'] . ' (' . $row['NomePerfil'] . ')<br>';
                }
            }

            $array['html'] = $html;

            retorno();
            break;
        case "salvar_msg":
            $CodRegistro = $_POST['CodRegistro'];
            $CodMensagem = $_POST['CodMensagem'];
            $Mensagem = $_POST['msg'];
            $Prioridade = $_POST['prioridade'];

            if (isset($_POST['email'])) {
                $Emails = $_POST['email'];
            } else {
                $Emails = "";
            }

            require_once("../../obj/mensagens.php");
            $obj_msg = new mensagens();

            $query = $obj_msg->editar_mensagem($CodRegistro, $CodMensagem, $Mensagem, $Prioridade, $Emails);
            if ($obj_msg->erro != "") {
                $array['erro'] = $obj_msg->erro;
                retorno();
            }

            gera_tabela_mensagens($CodRegistro);

            $array['mensagem'] = "Dados Gravados com sucesso.";

            retorno();
            break;
        case "excluir_msg":
            $CodRegistro = $_POST['CodRegistro'];
            $CodMensagem = $_POST['CodMensagem'];

            require_once("../../obj/mensagens.php");
            $obj_msg = new mensagens();

            $obj_msg->apagar_mensagem($CodMensagem);
            if ($obj_msg->erro != "") {
                $array['erro'] = $obj_msg->erro;
                retorno();
            }

            gera_tabela_mensagens($CodRegistro);

            $array['mensagem'] = "Mensagem excluída com sucesso.";

            retorno();
            break;
        case "consultar_dist":
            $CodRegistro = $_POST['CodRegistro'];
            $CodTipo = $_POST['CodTipo'];

            $perfil = $_SESSION['sessao_perfil'];

            if ($perfil > 2) {
                throw new Exception("Seu perfil não permite esta ação.");
            }

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $row = mysqli_fetch_array($obj_registros->query);

            if ($row['StatusRegistro'] == "concluído") {
                throw new Exception("Ação inválida para o status do registro.");
            }

            $array['CodTrilha'] = $row['CodTrilha'];
            $CodTrilha = $row['CodTrilha'];
            $detalhe = $row['TabelaDetalhe'];

            $html = "";

            require_once("../../obj/monitoramento.php");
            $obj_monitoramento = new monitoramento;


            $unidade_controle = $_SESSION['unidade_controle'];

            $tab = $obj_monitoramento->selecionar_usuarios($unidade_controle, $CodTipo, "pré-análise");
            if ($obj_monitoramento->erro != "") {
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }

            if ($tab != false) {
                for ($index = 0; $index < count($tab); $index++) {

                    $html = $html .
                        '<tr>
                        <td>' . $tab[$index]['Nome'] . '</td>
                        <td>' . $tab[$index]['NomePerfil'] . '</td>
                        <td style="text-align: right">' . $tab[$index]['qtd'] . '</td>
                        <td><a href="#" class="verde" onclick="return vincular_registro(' . $CodRegistro . ',' . $tab[$index]['cpfSGI'] . ')"><i class="fa fa-check-square-o fa-lg" aria-hidden="true"></i></a></td>
                    </tr>';
                }
            }

            $array['html'] = $html;

            $obj_registros->consulta_registros($CodTrilha, $detalhe);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $query = $obj_registros->query;

            while ($row0 = mysqli_fetch_assoc($query)) {
                $row3[] = $row0;
            }

            $cpf = "";
            $orgao = "";

            //sgta 173 problema de agrupamento de registros

            for ($index = 0; $index < count($row3); $index++) {
                if ($cpf == $row3[$index]['CPF_CNPJ_Proc'] && $orgao == $row3[$index]['CodOrgao']  && $CodPerfil > 1 ) {
                    $row3[$index]['incluir'] = false;
                } else {
                    $cpf = $row3[$index]['CPF_CNPJ_Proc'];
                    $orgao = $row3[$index]['CodOrgao'];
                    $row3[$index]['incluir'] = true;
                }
            }

            $sortArray = array();
            foreach ($row3 as $recordset) {
                foreach ($recordset as $key => $value) {
                    if (!isset($sortArray[$key])) {
                        $sortArray[$key] = array();
                    }
                    $sortArray[$key][] = $value;
                }
            }

            $orderby = "Nome_Descricao"; //change this to whatever key you want from the array 
            array_multisort($sortArray[$orderby], SORT_ASC, $row3);

            $html = "";
            $n = 1;

            for ($index = 0; $index < count($row3); $index++) {

                if ($row3[$index]['incluir'] == true) {
                    if ($row3[$index]['CodRegistro'] != $CodRegistro) {
                        if ($row3[$index]['StatusRegistro'] == "pré-análise") {
                            if ($row3[$index]['ReservadoPor'] == "") {
                                $html = $html .
                                    '<tr>
                                        <td><input type="checkbox" name="registros[]" value="' . $row3[$index]['CodRegistro'] . '" /></td>
                                        <td>' . $n . '</td>
                                        <td>' . $row3[$index]['CPF_CNPJ_Proc'] . '</td>
                                        <td>' . $row3[$index]['Nome_Descricao'] . '</td>
                                        <td>' . $row3[$index]['SiglaOrgao'] . '</td>
                                    </tr>';
                            }
                        }
                    }
                    $n++;
                }
            }

            $array['html2'] = $html;

            retorno();
            break;
        case "vincular_dist":
            $cpf = $_POST['filtro'];
            $CodRegistro = $_POST['CodRegistro'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $row = mysqli_fetch_array($obj_registros->query);

            if ($row['StatusRegistro'] == "concluído") {
                throw new Exception("Ação inválida para o status do registro.");
            }

            $obj_registros->reservar_registro($CodRegistro, $cpf, false);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            if (isset($_POST['registros'])) {
                $registros = $_POST['registros'];

                for ($index = 0; $index < count($registros); $index++) {
                    $CodRegistro = $registros[$index];

                    $obj_registros->reservar_registro($CodRegistro, $cpf, false);
                    if ($obj_registros->erro != "") {
                        $array['erro'] = $obj_registros->erro;
                        retorno();
                    }
                }
            }

            retorno();
            break;
        case "concluir_pre_analise":
            $CodRegistro = $_POST['CodRegistro'];
            $cpf = $_SESSION['sessao_id'];
            $perfil = $_SESSION['sessao_perfil'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $query = $obj_registros->query;
            $row = mysqli_fetch_array($query);

            if ($row['ReservadoPor'] != "") {
                if ($perfil > 2) {
                    throw new Exception("Seu perfil não permite esta ação.");
                }
            }

            if ($row['StatusRegistro'] == "concluído") {
                throw new Exception("Ação inválida para o status do registro.");
            }

            $obj_registros->concluir_pre_analise($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            retorno();
            break;
        case "auditoria_consulta":
            $CodRegistro = $_POST['CodRegistro'];
            $cpf = $_SESSION['sessao_id'];
            $perfil = $_SESSION['sessao_perfil'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $query = $obj_registros->query;
            $row = mysqli_fetch_array($query);

            if ($row['ReservadoPor'] != "") {
                if ($perfil > 2) {
                    throw new Exception("Seu perfil não permite esta ação.");
                }
            }

            if ($row['StatusRegistro'] == "concluído") {
                throw new Exception("Ação inválida para o status do registro.");
            }

            $array['cpf'] = $row['CPF_CNPJ_Proc'];
            $array['nome'] = $row['Nome_Descricao'];

            retorno();
            break;
        case "auditoria_pre_analise":
            $CodRegistro = $_POST['CodRegistro'];
            $JustificativaCGE = $_POST['obs'];
            $UsuarioCGE = $_SESSION['sessao_id'];
            $perfil = $_SESSION['sessao_perfil'];

            $obj_registros->consulta_registro($CodRegistro);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $row = mysqli_fetch_array($obj_registros->query);

            if ($row['StatusRegistro'] == "concluído") {
                throw new Exception("Ação inválida para o status do registro.");
            }

            $obj_registros->salvar_analise("abrir_auditoria", $CodRegistro, $JustificativaCGE, $UsuarioCGE, "", 3, "", "");
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $NomeTrilha = $row['NomeTrilha'];
            $Identificador = $row['CPF_CNPJ_Proc'];
            $Nome = $row['Nome_Descricao'];

            $mensagem = "Foi recomendado a abertura de processo Inspeção/Auditoria em um registro de inconsistência. Trilha: $NomeTrilha. CPF/CNPJ/Numero Processo: $Identificador. Nome/Descrição: $Nome."
                . "<br>Consulte o Sistema de Gestão de Trilhas de Auditoria para mais detalhes.";

            require_once("../../obj/email.php");
            $obj_email = new email();

            $obj_email->enviarEmails("Recomendação de abertura de processo de Inspeção/Auditoria.", $mensagem, $unidade_controle, 2, $row['CodTipo'], 'grupo');
            $obj_email->enviarEmail("Recomendação de abertura de processo de Inspeção/Auditoria.", $mensagem, "nie@tce.sc.gov.br");
            $obj_email->enviarMensagens("Recomendação de abertura de processo de Inspeção/Auditoria", $mensagem, $unidade_controle, 2, $row['CodTipo']);

            retorno();
            break;
        case "redistribuir_consulta":
            $CodRegistro = $_POST['CodRegistro'];
            $CodTipo = $_POST['CodTipo'];

            $html = "";

            require_once("../../obj/monitoramento.php");
            $obj_monitoramento = new monitoramento();
            $tab = $obj_monitoramento->selecionar_usuarios($unidade_controle, $CodTipo);
            if ($obj_monitoramento->erro != "") {
                $array['erro'] = $obj_monitoramento->erro;
                retorno();
            }

            if ($tab != false) {
                for ($index = 0; $index < count($tab); $index++) {

                    $html = $html .
                        '<tr>
                        <td>' . $tab[$index]['Nome'] . '</td>
                        <td>' . $tab[$index]['NomePerfil'] . '</td>
                        <td style="text-align: right">' . $tab[$index]['qtd'] . '</td>
                        <td><a href="#" class="editar" title="Redistribuir para este Auditor" tabIndex="-1" onclick="return redistribuir_para(' . $CodRegistro . ',' . $tab[$index]['cpfSGI'] . ',\'' . $tab[$index]['Nome'] . '\')"><i class="fa fa-hand-o-up fa-lg" aria-hidden="true"></i></a></td>
                    </tr>';
                }
            }

            $array['html'] = $html;

            retorno();
            break;
        case "redistribuir_para":
            $CodRegistro = $_POST['CodRegistro'];
            $cpf = $_POST['CodTipo'];

            $obj_registros->reservar_registro($CodRegistro, $cpf, false, true);
            if ($obj_registros->erro != "") {
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            require_once("../../obj/email.php");
            $obj_email = new email();

            try {
                $obj_registros->consulta_registro($CodRegistro);
                if ($obj_registros->erro == "") {
                    $query = $obj_registros->query;
                    $row = mysqli_fetch_array($query);

                    require_once("../../obj/usuario.php");
                    $obj_usuario = new usuario();

                    $obj_usuario->consulta_nome($cpf);
                    $emaildestinatario = $obj_usuario->email;

                    $assunto = "Um registro de inconsistência foi enviado para sua análise.";
                    $mensagem = "Detalhes: <br>";
                    $mensagem = $mensagem . "Trilha: <b>" . $row['NomeTrilha'] . "</b><br>";
                    $mensagem = $mensagem . "Órgão/Unidade: <b>" . $row['NomeOrgao'] . "</b><br>";
                    $mensagem = $mensagem . "CPF/CNPJ/Processo: <b>" . $row['CPF_CNPJ_Proc'] . "</b><br>";
                    //$mensagem = $mensagem."Para acessar o sistema ".'<a href = "http://sgta.tce.sc.gov.br" target = "_blank">clique aqui.</a><br><br>';
                    $mensagem = $mensagem . "Grato. ";

                    $obj_email->enviarEmail($assunto, $mensagem, $emaildestinatario);
                } else {
                    //andre - coloquei meu email para monitorar problemas no envio
                    $obj_email->enviarEmail("Erro no script 04", $obj_registros->erro, "atriches@gmail.com");
                }
            } catch (Exception $exc) {
                $obj_email->enviarEmail("Erro no script 03", $exc->getMessage(), "atriches@gmail.com");
                $array['erro'] = "aqui 5";
                retorno();
            }

            require_once("../../obj/ordem_servico.php");
            $obj_os = new ordem_servico();
            $obj_os->transfere_os($CodRegistro, $cpf);
            if ($obj_os->erro != "") {
                $array['erro'] = $obj_os->erro;
                retorno();
            }

            retorno();
            break;
        case "salvar_mensagem_trilha":
            $CodMensagem = $_POST['CodMensagem'];
            $CodTrilha = $_POST['CodTrilha'];
            $Mensagem = $_POST['msg'];

            require_once("../../obj/mensagens_trilha.php");
            $obj_msg = new mensagens_trilha();

            if ($CodMensagem == -1) {
                $obj_msg->incluir_mensagem($CodTrilha, $Mensagem);
                if ($obj_msg->erro != "") {
                    throw new Exception($obj_msg->erro);
                }
            } else {
                $obj_msg->editar_mensagem($CodMensagem, $Mensagem);
                if ($obj_msg->erro != "") {
                    throw new Exception($obj_msg->erro);
                }
            }

            $html = $obj_msg->gera_tabela_mensagens_trilha($CodTrilha);
            if ($obj_msg->erro != "") {
                throw new Exception($obj_msg->erro);
            }

            $array['html_msg_trilhas'] = $html;

            $array['mensagem'] = "Dados gravados com sucesso.";

            retorno();
            break;
        case "alterar_mensagem_trilha":
            $CodMensagem = $_POST['CodMensagem'];

            require_once("../../obj/mensagens_trilha.php");
            $obj_msg = new mensagens_trilha();

            $query = $obj_msg->consultar_mensagem($CodMensagem);
            if ($obj_msg->erro != "") {
                throw new Exception($obj_msg->erro);
            }

            $array['mensagem_trilha'] = "";

            if (mysqli_num_rows($query) > 0) {
                $row = mysqli_fetch_array($query);
                $array['mensagem_trilha'] = $row['Mensagem'];
            }

            retorno();
            break;
        case "excluir_mensagem_trilha":
            $CodMensagem = $_POST['CodMensagem'];
            $CodTrilha = $_POST['CodTrilha'];

            require_once("../../obj/mensagens_trilha.php");
            $obj_msg = new mensagens_trilha();

            $obj_msg->apagar_mensagem($CodMensagem);
            if ($obj_msg->erro != "") {
                throw new Exception($obj_msg->erro);
            }

            $html = $obj_msg->gera_tabela_mensagens_trilha($CodTrilha);
            if ($obj_msg->erro != "") {
                throw new Exception($obj_msg->erro);
            }

            $array['html_msg_trilhas'] = $html;

            $array['mensagem'] = "Mensagem excluída com sucesso.";

            retorno();
            break;
        case "ocultar_observacao":
            $Acao = "ocultar";
        case "mostrar_observacao":
            if ($array['operacao'] == "mostrar_observacao") {
                $Acao = "exibir";
            }
            $CodObs = $_POST['CodObs'];
            $CodRegistro = $_POST['CodRegistro'];

            require_once("../../obj/andamentos.php");
            $obj_andamentos = new andamentos();

            $obj_andamentos->exibir_observacoes($CodObs, $Acao);
            if ($obj_andamentos->erro != "") {
                $array['erro'] = $obj_andamentos->erro;
                retorno;
            }



            $observacoes = $obj_andamentos->consultar_observacoes($CodRegistro, true);
            if ($obj_andamentos->erro != "") {
                $array['erro'] = $obj_andamentos->erro;
                retorno;
            }

            $ocorrencias = "";

            $CodPerfil = $_SESSION['sessao_perfil'];

            if ($observacoes[0]['qtd'] > 0) {
                for ($index = 0; $index < count($observacoes); $index++) {

                    $CodObs = $observacoes[$index]['CodObs'];

                    if ($CodPerfil < 3 && $index > 0) {
                        if ($observacoes[$index]['StatusObservacao'] == 'privado') {
                            $botao = '<br><a href="#" title="Exibir Observação aos Órgãos" class="verde" onclick="return mostrar_observacao(' . $CodObs . ');"><span class="fa fa-plus-circle fa-lg"></span></a>';
                            $cor = 'style="color: red;"';
                        } else {
                            $botao = '<br><a href="#" title="Ocultar Observação para os Órgãos" class="excluir" onclick="return ocultar_observacao(' . $CodObs . ');"><span class="fa fa-trash fa-lg"></span></a>';
                            $cor = '';
                        }
                    } else {
                        if ($observacoes[$index]['StatusObservacao'] == 'privado' && $index > 0) {
                            $cor = 'style="color: red;"';
                        } else {
                            $cor = '';
                        }
                        $botao = '';
                    }

                    if ($observacoes[$index]['StatusObservacao'] == 'público' || $CodPerfil < 5 || $index == 0) {
                        $ocorrencias = $ocorrencias .
                            '<tr ' . $cor . '>
                                    <td>' . $observacoes[$index]['Data'] . $botao . '</td>
                                <td>' . str_replace("\r", '<br>', $observacoes[$index]['ObsCGE']) . '</td>
                                <td>' . $observacoes[$index]['Nome'] . '</td>
                                <td>' . $observacoes[$index]['Contato'] . '</td>
                            </tr>';
                    }
                }
            }

            $array['Observacoes'] = $ocorrencias;

            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage() . ". Linha: " . $ex->getLine();
    retorno();
}

function gera_tabela_registros_excluidos($query,$detalhe){
    
    try{
        global  $array;

        $html = "";
        if(mysqli_num_rows($query) > 0){
            require_once("../../obj/registros.php");
            require_once("../../obj/usuario.php");
         //   $obj_registros = new registros();
 
            $obj_usuario = new usuario();
            
            while ($row = mysqli_fetch_array($query)){

               // die(var_dump($row));
                $varauxUsuario = null;
                $varauxUsuario  = $obj_usuario->consulta_nome($row['usuario']);
           
                $a =$m =$d = "";
                list($a,$m,$d) = explode("-",$row['data_exclusa']);
                 
                $html = $html .  
                '<tr>
                    <td>'.$row['CodRegistro'].'</td>
                    <td>'.$row['CPF_CNPJ_Proc'].'</td>
                    <td id="nt_'.$row['CodRegistro'].'">'.$row['Nome_Descricao'].'</td>
                    <td>'.   $varauxUsuario .'</td>
                    <td>'.$d."/".$m."/".$a.'</td>
                    <td>'.$row['ObsCGE'].'</td>
             
                    <td>  
                    <a href="#" title="Exibir mais detalhes." onclick="return detalhes('.$row['CodRegistro'].',\'' . $detalhe  . '\');" class="incluir">
                    <i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a> &nbsp;

                    <a href="#" title="Consultar Justificativas/Pareceres/Eventos." onclick="return consulta_andamento('.$row['CodRegistro'].');" class="marron">
                    <i class="fa fa-search fa-lg" aria-hidden="true"></i></a>

                    <a href="#" title="Retornar o registro a trilha." onclick="return retornar_registro_trilha('.$row['CodRegistro'].',' . $row['CodTrilha']. ',' . $row['CodOrgao']. ' );" class="marron">
                    <i class="fa fa-backward fa-lg" aria-hidden="true"></i></a>
                    
                    </td>
                </tr>';        
            }
        }      

        $array['tabela'] = $html;
        $array["body"] = $html;
        
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage();
        retorno();
    }
}

function gera_tabela_mensagens($CodRegistro)
{
    global $array;
    try {

        require_once("../../obj/usuario.php");
        $obj_usuario = new usuario();

        require_once("../../obj/mensagens.php");
        $obj_msg = new mensagens();

        $query = $obj_msg->buscar_mensagens($CodRegistro);
        if ($obj_msg->erro != "") {
            $array['erro'] = $obj_msg->erro;
            retorno();
        }

        $Usuario = $_SESSION['sessao_id'];

        $body = "";

        if (mysqli_num_rows($query) > 0) {

            while ($row = mysqli_fetch_array($query)) {

                $row['Mensagem'] = str_replace("\r", '<br>', $row['Mensagem']);

                if ($row['Prioridade'] == "normal") {
                    $tr = "<tr style='color: black;'>";
                } elseif ($row['Prioridade'] == "alto") {
                    $tr = "<tr style='color: darkorange; font-weight: bold;'>";
                } else {
                    $tr = "<tr style='color: red; font-weight: bold;'>";
                }

                $body = $body . $tr .
                    '
                    <td>' . $row['CodMensagem'] . '</td>
                    <td>' . $obj_usuario->consulta_nome($row['Usuario']) . '</td>
                    <td>' . data_br($row['DataMensagem']) . '</td>
                    <td style="text-align: justify;">' . $row['Mensagem'] . '</td>
                ';

                if ($row['Usuario'] == $Usuario) {
                    $body = $body .
                        '<td>
                            <a href="#" title="Alterar Mensagem" onclick="return alterar_msg(' . $row['CodMensagem'] . ');" class="editar">
                                <i class="fa fa-edit fa-lg" aria-hidden="true"></i></a>
                            <a href="#" title="Excluir Mensagem" onclick="return excluir_msg(' . $row['CodMensagem'] . ');" class="excluir">
                                                            <i class="fa fa-times-circle fa-lg" aria-hidden="true"></i></a>
                        </td>
                    </tr>';
                } else {
                    $body = $body .
                        '<td>
                        </td>
                    </tr>';
                }
            }
        }

        $array["body"] = $body;
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage();
        retorno();
    }
}

function retorno()
{
    global $array;

    usleep(100000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}
