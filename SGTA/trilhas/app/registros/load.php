<?php
// andre aqui $_REQUEST['detalhe']  
    $anexos = $obj_trilhas->listar_anexos($CodTrilha);
    if($obj_trilhas->erro != ""){
        $array['erro'] = $obj_trilhas->erro;
        retorno();
    }
    $array['anexos'] = "";
    
    $cpf_user = $cpf;
    
    $CodOrgaoSessao = $_SESSION['sessao_orgao'];

    if($anexos[0]['qtd'] > 0){
        for ($index = 0; $index < count($anexos); $index++){
            if($anexos[$index]["url"] != ""){

                if($cpf == $anexos[$index]['Usuario']){
                    $excluir = '<a href="#" class="excluir" title="Excluir o anexo" onclick="return excluir_anexo('.$anexos[$index]['CodAnexo'].')"><span class="fa fa-times-circle"></span></a>';
                }else{
                    $excluir = '<span style="color: LightGrey;" class="fa fa-times-circle"></span>';
                }

                if($CodPerfil < 5 || $anexos[$index]['Restrito'] == false){

                    $nome = $obj_usuario->consulta_nome($anexos[$index]['Usuario']);
                    $CodOrgaoUploader = $obj_usuario->CodOrgao;
                    
                       //SGTA-134 - Exibição de arquivos em "anexos" librar visualização
                    //if($CodPerfil > 4 && $CodOrgaoUploader != $CodOrgaoSessao){//não exibe o arquivo
                        //goto Pulo;
                  //  }
                    
                    if($anexos[$index]['Restrito'] == true){
                        $array['anexos'] = $array['anexos'] .
                            '<tr>
                                <td>
                                    <a href="'.$anexos[$index]['url'].'" target="_blank">'.$anexos[$index]['NomeAnexo'].'</a>&nbsp
                                    '.$excluir.' &nbsp<span style="color: Blue;" class="fa fa-info-circle" title="Este anexo é visível apenas para servidores do TCE"></span>    
                                </td>
                                <td>'.$anexos[$index]['DescricaoAnexo'].'</td>
                                <td>'.$nome.'</td>
                            </tr>';
                    }else{
                        $array['anexos'] = $array['anexos'] .
                            '<tr>
                                <td>
                                    <a href="'.$anexos[$index]['url'].'" target="_blank">'.$anexos[$index]['NomeAnexo'].'</a>&nbsp
                                    '.$excluir.'    
                                </td>
                                <td>'.$anexos[$index]['DescricaoAnexo'].'</td>
                                <td>'.$nome.'</td>
                            </tr>';
                    }
                    
                    Pulo:
                    
                }
            }
        }
    }
    
    $obj_trilhas->consulta_trilha($CodTrilha);
    if($obj_trilhas->erro != ""){
        $array['erro'] = $obj_trilhas->erro;
        retorno();
    }

    $row = mysqli_fetch_array($obj_trilhas->query);

    $info[] = $row;
    
    if($CodPerfil < 5){
        
        $url = "/trilhas/intra/carga/" . $CodTrilha . "/" . $info[0]['TabelaDetalhe'] . ".xlsx";
        $excluir = '<span style="color: LightGrey;" class="fa fa-times-circle"></span>';
        
        $array['anexos'] = $array['anexos'] .
            '<tr>
                <td>
                    <a href="'.$url.'" target="_blank">'.$info[0]['TabelaDetalhe'] . ".xlsx".'</a>&nbsp
                    '.$excluir.'    
                </td>
                <td>Arquivo Original da Trilha (visível somente para os Servidores do TCE.)</td>
                <td>Administrador</td>
            </tr>';
    }
    
    if($info[0]['DataPublicacao'] != ""){//pulicado
        $info[0]['StatusTrilha'] = $info[0]['StatusTrilha'] . "  - publicada em: " .data_br($info[0]['DataPublicacao']);
    }else if($info[0]['DataLiberacao'] != "" && $info[0]['StatusTrilha'] != "pendente"){
        $info[0]['StatusTrilha'] = $info[0]['StatusTrilha'] . "  - em homologação desde: " .data_br($info[0]['DataLiberacao']);
    }

    
    $info[0]['DataInicial'] = data_br($info[0]['DataInicial']);
    $info[0]['DataFinal'] = data_br($info[0]['DataFinal']);
    $info[0]['Instrucao'] = base64_decode($info[0]['Instrucao']);

    $array["info"] = $info;

    $qtd = $obj_registros->contar_registros($CodTrilha, $CodOrgao);
    if($obj_registros->erro != ""){
        $array['erro'] = $obj_registros->erro;
        retorno();
    }

    $array['qtdregistros'] = $qtd['registros'];
    $array['justificados'] = $qtd['justificados'];
    $array['pendentes'] = $qtd['pendentes'];
    $array['economiagerada'] = 0;

    $cpf = "";
    $orgao = "";
    $cpfuser = $_SESSION['sessao_id'];

    $tab = "";

    $linha = 0;
    
    $DataPublicacao = $row3[0]['DataPublicacao'];
    
    require_once("../../../obj/mensagens.php");
    $obj_msg = new mensagens();
    
    $msgs = $obj_msg->contar_mensagens($CodTrilha);
    if($obj_msg->erro != ""){
        $array['erro'] = $obj_msg->erro;
        retorno();
    }
    
    $perfil = $_SESSION['sessao_perfil'];

    for ($index = 0; $index < count($row3); $index++){
        
        $row3[$index]['Flags'] = "";
        
        if($row3[$index]['QuarentenaAte'] != ""){
            $row3[$index]['Flags'] = $row3[$index]['Flags'] . '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\'monitoramento\')"><i class="fa fa-flag fa-lg" aria-hidden="true" title="Justificativa em '
                    . 'Monitoramento até: '.data_br($row3[$index]['QuarentenaAte']).'. busca: monitoramento" style="color: darkorange;"></i></a><span '
                    . 'style="display: none;">monitoramento</span>&nbsp';
            $QuarentenaAte = new DateTime($row3[$index]['QuarentenaAte']);
            $Hoje = new DateTime();
            if($QuarentenaAte < $Hoje){
                $row3[$index]['Flags'] = $row3[$index]['Flags'] . '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\'prazo monitoramento vencido\')"><i class="fa fa-flag-checkered fa-lg" aria-hidden="true" title="Monitoramento com '
                    . 'prazo vencido. busca: prazo monitoramento vencido" style="color: darkorange;"></i></a><span '
                    . 'style="display: none;">prazo monitoramento vencido</span>&nbsp';
            }
        }
        if($row3[$index]['SolicitadoAlteracao'] == true){
            $row3[$index]['Flags'] = $row3[$index]['Flags'] . '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\'solicitado alteracao\')"><i class="fa fa-flag fa-lg" aria-hidden="true" title="Solicitado alteração '
                    . 'de justificativa por: '.$obj_usuario->consulta_nome($row3[$index]['SolicitadoPor']).'. busca: solicitado alteração" '
                    . 'style="color: blue;"></i></a><span style="display: none;">solicitado alteração</span>&nbsp';
        }
        if($row3[$index]['SolicitadoAlteracao'] == true && $row3[$index]['SolicitacaoAceita'] == true){
            $row3[$index]['Flags'] = $row3[$index]['Flags'] . '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\'alteracao aceita\')"><i class="fa fa-flag fa-lg" aria-hidden="true" title="Autorizado alteração '
                    . 'de justificativa por: '.$obj_usuario->consulta_nome($row3[$index]['AnalisadoPor']).'. busca: alteração aceita" '
                    . 'style="color: green;"></i></a><span style="display: none;">alteração aceita</span>&nbsp';
        }
        if($row3[$index]['SolicitadoAlteracao'] == true && $row3[$index]['SolicitacaoAceita'] == 0  && $row3[$index]['SolicitacaoAceita'] != ""){
            $row3[$index]['Flags'] = $row3[$index]['Flags'] . '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\'alteracao negada\')"><i class="fa fa-flag fa-lg" aria-hidden="true" title="Negado alteração de '
                    . 'justificativa por: '.$obj_usuario->consulta_nome($row3[$index]['AnalisadoPor']).'. busca: alteração negada" '
                    . 'style="color: indianred;"></i></a><span style="display: none;">alteração negada</span>&nbsp';
        }
        if($row3[$index]['NumProcAuditoria'] != ""){
            $row3[$index]['Flags'] = $row3[$index]['Flags'] . '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\'processo auditoria\')"><i class="fa fa-flag-checkered fa-lg" aria-hidden="true" title="Aberto processo de '
                    . $row3[$index]['TipoProcesso'] . ' nr: '.$row3[$index]['NumProcAuditoria'].' por: '.$obj_usuario->consulta_nome($row3[$index]['UsuarioCGE']).'. busca: processo auditoria" '
                    . 'style="color: red;"></i></a><span style="display: none;">processo auditoria</span>&nbsp';
        }else if($row3[$index]['SolicitadoAuditoria'] != false){
            $row3[$index]['Flags'] = $row3[$index]['Flags'] . '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\'recomendado auditoria\')"><i class="fa fa-flag fa-lg" aria-hidden="true" title="Recomendado Abertura de'
                    . ' Processo de Inspeção/Auditoria por: '.$obj_usuario->consulta_nome($row3[$index]['UsuarioCGE']).'. busca: recomendado auditoria" '
                    . 'style="color: indigo;"></i></a><span style="display: none;">recomendado auditoria</span>&nbsp';
        }
        
        $nao_analisado = false;
        
        if ($DataPublicacao != ""){
            if($row3[$index]["StatusRegistro"] == "pendente" || $row3[$index]["StatusRegistro"] == "em justificativa"){
                
                if($row3[$index]["TipoTrilha"] == "pós"){
                    $prazo = $prazo_legal_pos;
                }else{
                    $prazo = $prazo_legal_pre;
                }
                
                $prazo = $prazo + $row3[$index]["Prorrogado"];
                
                $one= new DateTime($DataPublicacao);
                $two = new DateTime();
                $dateInterval = $one->diff($two);
                $dias = $dateInterval->days;
                if($dias > $prazo){
                    $row3[$index]['Flags'] = $row3[$index]['Flags'] . '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\'descumprimento prazo\')"><i class="fa fa-flag-checkered fa-lg" aria-hidden="true" title="Prazo legal '
                            . 'para justificativa esgotado, decorridos '.$dias.' dias desde a publicação da trilha. busca: descumprimento prazo" '
                            . 'style="color: purple;"></i></a><span style="display: none;">descumprimento prazo</span>&nbsp';
                    $nao_analisado = true;
                }
            }
        }
        
        //SGTA 173 - aqui o motivo do agrupamento, na verdade não exibi o registro
        if($cpf == $row3[$index]['CPF_CNPJ_Proc'] && $orgao == $row3[$index]['CodOrgao'] && $CodPerfil > 1){
            $row3[$index]['incluir'] = false;
        }else{
            $cpf = $row3[$index]['CPF_CNPJ_Proc'];
            $orgao = $row3[$index]['CodOrgao'];
            $row3[$index]['incluir'] = true;
            $linha++;
        }
        
        if($row3[$index]['incluir'] == true){
            $ReservadoPor = $row3[$index]['ReservadoPor'];
            $RespObs = $row3[$index]['RespObs'];
            
            if($row3[$index]["StatusRegistro"] == "concluído"){
                $array['economiagerada'] = $array['economiagerada'] + $row3[$index]['ValorRessarcido'] + ($row3[$index]['EconomiaMensal'] * 12);
            }

            if($RespObs != "" && $row3[$index]['ObsCGE'] != ""){
                $row3[$index]['RespObs'] = "<span style='color:SteelBlue;'>Cadastrado Por: " . $obj_usuario->consulta_nome($RespObs) . ". Fone: " . $obj_usuario->telefone . ". Email: " . $obj_usuario->email."</span>";
                $row3[$index]['ObsCGE'] =  $row3[$index]['ObsCGE'] . "<br>" . $row3[$index]['RespObs'];

            }

            if($ReservadoPor != ""){
                $row3[$index]['ReservadoPor'] = $obj_usuario->consulta_nome($row3[$index]['ReservadoPor']);
                if($acao == "consulta"){
                    $row3[$index]['ReservadoPor'] = "Reservado por: " . $row3[$index]['ReservadoPor'] . $obj_usuario->telefone . ". Email: " . $obj_usuario->email;
                }
            }else{
                $ReservadoPor = 0;
            }

            $row3[$index]['ObsCGE'] = str_replace("\r", '<br>', $row3[$index]['ObsCGE']);

            $PossuiAnexos = false;

            if($row3[$index]['QtdAnexos'] != ""){
                $row3[$index]['anexos'] = $row3[$index]['QtdAnexos'].' <a href="#" title="Visualizar os arquivos anexados." onclick="return visualizar_anexos('.$row3[$index]['CodRegistro'].')"><i class="fa fa-file-text-o fa-lg fa-fw" aria-hidden="true"></i></a>';
            }else{
                $row3[$index]['anexos'] = '0 <i class="fa fa-file-text-o fa-lg fa-fw" aria-hidden="true" style="color: LightGrey;"></i>';
            }
            
            if($CodPerfil < 5){
                
                $qtdmsg = 0;
                
                if($msgs != "zero"){
                    
                    for ($id = 0; $id < count($msgs); $id++){
                        
                        if($msgs[$id]["CodRegistro"] == $row3[$index]['CodRegistro']){
                            $qtdmsg = $msgs[$id]["QtdMensagens"];
                        }
                        
                    }
                }
                
                if($qtdmsg == 0){
                    $row3[$index]['anexos'] = $row3[$index]['anexos'] . '<br>'.$qtdmsg.' <a href="#" title="Visualizar/Incluir Mensagens. (Visível somente aos servidores do TCE)." onclick="return visualizar_mensagens('.$row3[$index]['CodRegistro'].')"><i class="fa fa-comments-o fa-lg fa-fw" aria-hidden="true" style="color: DarkGreen;"></i></a>';
                }else{
                    $row3[$index]['anexos'] = $row3[$index]['anexos'] . '<br>'.$qtdmsg.' <a href="#" title="Visualizar/Incluir Mensagens. (Visível somente aos servidores do TCE)." onclick="return visualizar_mensagens('.$row3[$index]['CodRegistro'].')"><i class="fa fa-comments-o fa-lg fa-fw" aria-hidden="true" style="color: DarkRed;"></i></a>';
                }
            }
            
            $row3[$index]['cor'] = "black";
            $row3[$index]['cor2'] = "";
            $row3[$index]['span'] = "";
            $row3[$index]['funcionario'] = "";

            if($row3[$index]['StatusRegistro'] == "pendente"){
                $row3[$index]['cor'] = "darkorange";
            }else if($row3[$index]['StatusRegistro'] == "excluído"){
                $row3[$index]['cor'] = "red";
                $row3[$index]['cor2'] = 'style="color: red; font-weight: bold;"';
            }else if($row3[$index]['StatusRegistro'] == "em justificativa"){
                $row3[$index]['cor'] = "darkblue";
                $row3[$index]['cor2'] = "";
                $row3[$index]['funcionario'] = "Reservado Por: " . $obj_usuario->consulta_nome($ReservadoPor) . ". Fone: " . $obj_usuario->telefone . ". Email: " . $obj_usuario->email;
            }else if($row3[$index]['StatusRegistro'] == "justificado"){
                $row3[$index]['cor'] = "darkgreen";
                $row3[$index]['cor2'] = "";
                $row3[$index]['funcionario'] = "Justificado Por: " . $obj_usuario->consulta_nome($row3[$index]['UsuarioOrgao']) . ". Fone: " . $obj_usuario->telefone . ". Email: " . $obj_usuario->email;
            }else if($row3[$index]['StatusRegistro'] == "concluído"){
                $row3[$index]['cor'] = "Magenta";
                $row3[$index]['cor2'] = "";
                $row3[$index]['funcionario'] = "Analisado Por: " . $obj_usuario->consulta_nome($row3[$index]['UsuarioCGE']) . ". Fone: " . $obj_usuario->telefone . ". Email: " . $obj_usuario->email;
            }else if($row3[$index]['StatusRegistro'] == "devolvido"){
                $row3[$index]['cor'] = "red";
                $row3[$index]['cor2'] = "";
                $row3[$index]['funcionario'] = "Devolvido Por: " . $obj_usuario->consulta_nome($row3[$index]['UsuarioCGE']) . ". Fone: " . $obj_usuario->telefone . ". Email: " . $obj_usuario->email;
            }else if($row3[$index]['StatusRegistro'] == "em análise"){
                $row3[$index]['cor'] = "blue";
                $row3[$index]['cor2'] = "";
                $row3[$index]['funcionario'] = "Reservado Por: " . $obj_usuario->consulta_nome($ReservadoPor) . ". Fone: " . $obj_usuario->telefone . ". Email: " . $obj_usuario->email;
            }

            $row3[$index]['RespCGE'] = "";
            
            if($acao == "consulta"){
                
                $row3[$index]['icone'] =    '<a href="#" title="Exibir mais detalhes." onclick="return detalhes('.$row3[$index]['CodRegistro'].',\'' .@$_REQUEST['detalhe']  . '\');" class="incluir">
                                                <i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a>';

                //SGTA-85
                if($row3[$index]["StatusRegistro"] != "concluído" or $CodPerfil <= 2 ) {
                    $row3[$index]['icone'] .=    '<a href="#" title="Anexar um arquivo." onclick="return anexar_arquivo('.$row3[$index]['CodRegistro'].',\'registro\','.$linha.');" class="rosa">
                    <i class="fa fa-paperclip fa-lg" aria-hidden="true"></i></a>';
                }

                                            

                $row3[$index]['icone'] .=  '<a href="#" title="Consultar Justificativas/Pareceres/Eventos" onclick="return consulta_andamento('.$row3[$index]['CodRegistro'].');" class="marron">
                                            <i class="fa fa-search fa-lg" aria-hidden="true"></i></a>
                                            <a href="#" title="Imprimir Justificativas/Pareceres/Eventos" onclick="return imprimir_andamento('.$row3[$index]['CodRegistro'].');" class="azul">
                                             <i class="fa fa-print fa-lg" aria-hidden="true"></i></a>';
                
            }else if($acao == "pré-análise"){
                
                if($row3[$index]['StatusRegistro'] != "excluído" && $row3[$index]['Pre_Analisado'] == 1){
                    $row3[$index]['cor'] = "green";
                    $row3[$index]['cor2'] = 'style="color: green; font-weight: bold;"';
                }
               
                
                if (($CodPerfil > 2 && $ReservadoPor != $cpfuser) || ($row3[$index]['StatusRegistro'] != "pré-análise" && $CodPerfil > 2)){
                    $row3[$index]['icone'] =    '<a href="#" title="Exibir mais detalhes." onclick="return detalhes('.$row3[$index]['CodRegistro'].',\'' .@$_REQUEST['detalhe']  . '\');" class="incluir">
                                                    <i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a>
                                                <a href="#" title="Incluir uma observação no registro." onclick="return info('.$row3[$index]['CodRegistro'].',1,'.$index.');" class="editar">
                                                        <i class="fa fa-info-circle fa-lg fa-fw" aria-hidden="true"></i></a>
                                                <i style="color: silver;" class="fa fa-minus-circle fa-lg" aria-hidden="true"></i> ';
                                           
                        //SGTA-85
                        if($row3[$index]["StatusRegistro"] != "concluído" or $CodPerfil <= 2 ) {
                                $row3[$index]['icone'] .=   ' <a href="#" title="Anexar um arquivo." onclick="return anexar_arquivo('.$row3[$index]['CodRegistro'].',\'registro\','.$linha.');" class="rosa">
                                <i class="fa fa-paperclip fa-lg" aria-hidden="true"></i></a>';
                        }
                                               
                                               
                        $row3[$index]['icone'] .='<i style="color: silver;" class="fa fa-recycle fa-lg" aria-hidden="true"></i>
                                    <i style="color: silver;" class="fa fa-random fa-lg" aria-hidden="true"></i>
                                    <i style="color: silver;" class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i>
                                    <i style="color: silver;" class="fa fa-check-circle fa-lg" aria-hidden="true"></i>
                                    <i style="color: silver;" class="fa fa-user-secret fa-lg" aria-hidden="true"></i>
                                    <a href="#" title="Consultar Justificativas/Pareceres/Eventos." onclick="return consulta_andamento('.$row3[$index]['CodRegistro'].');" class="marron">
                                        <i class="fa fa-search fa-lg" aria-hidden="true"></i></a>
                                    ';
                }else{
                    if($CodPerfil <= 2){

                        //SGTA-62
                        // <a href="#" title="Solicitar Abertura de Processo de Inspeção/Auditoria Direto" onclick="return auditoria_consulta('.$row3[$index]['CodRegistro'].');" class="especial">
                        //<i class="fa fa-user-secret fa-lg fa-fw" aria-hidden="true"></i></a>
                        $row3[$index]['icone'] =    '<a href="#" title="Exibir mais detalhes." onclick="return detalhes('.$row3[$index]['CodRegistro'].',\'' .@$_REQUEST['detalhe']  . '\');" class="incluir">
                                                        <i class="fa fa-plus-circle fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    <a href="#" title="Incluir uma observação no registro." onclick="return info('.$row3[$index]['CodRegistro'].',1,'.$index.');" class="editar">
                                                        <i class="fa fa-info-circle fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    <a href="#" title="Remover do resultado da trilha este registro, ele não será enviado ao órgão." onclick="return rejeitar('.$row3[$index]['CodRegistro'].',' .$row3[$index]['CodTrilha'] . ');" class="excluir">
                                                        <i class="fa fa-minus-circle fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    <a href="#" title="Anexar um arquivo." onclick="return anexar_arquivo('.$row3[$index]['CodRegistro'].',\'registro\','.$linha.');" class="rosa">
                                                        <i class="fa fa-paperclip fa-lg fa-fw" aria-hidden="true"></i></a>';
                                                   
                            if(!$continua) {
                                $row3[$index]['icone'] .=   ' <a href="#" title="Reincluir o registro no resultado da trilha." onclick="return reincluir('.$row3[$index]['CodRegistro'].');" class="marron">
                                <i class="fa fa-recycle fa-lg fa-fw" aria-hidden="true"></i></a>
                                <a href="#" title="Mesclar registro com outro." onclick="return mesclar('.$row3[$index]['CodRegistro'].');" class="cyan">
                                <i class="fa fa-random fa-lg fa-fw" aria-hidden="true"></i></a>';
                            }
                          
                                                   
                                                   
                        $row3[$index]['icone'] .=   ' <a href="#" title="Distribuir o registro a um gestor" onclick="return distribuir_registro('.$row3[$index]['CodRegistro'].','.$row3[$index]['CodTipo'].');" class="editar">
                                                        <i class="fa fa-hand-pointer-o fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    <a href="#" title="Concluir Análise" onclick="return concluir_pre_analise('.$row3[$index]['CodRegistro'].','.$ReservadoPor.','.$linha.');" class="verde">
                                                        <i class="fa fa-check-circle fa-lg fa-fw" aria-hidden="true"></i></a>
                                                   
                                                       

                                                    <a href="#" title="Consultar Justificativas/Pareceres/Eventos." onclick="return consulta_andamento('.$row3[$index]['CodRegistro'].');" class="marron">
                                                        <i class="fa fa-search fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    '; 

                                                    //SGTA 173 - testa se é continua e coloca o icone
                                                     //SGTA 173
                        //verifica se tem registro em continuos
                        $obj_registros->verifica_registro_rascunho($row3[$index]['CodRegistro']);
                        $query = $obj_registros->query;
                        $varContinuo = mysqli_fetch_assoc($query);
                
                        if($continua && @count($varContinuo)>0){//testar se e distribuida

                            $row3[$index]['icone'] .= '<a href="#" title="Enviar Registro ao Orgãos" onclick="return enviar_registro('.$row3[$index]['CodRegistro'].');" class="black">
                                                        <i class="fa fa-fast-forward fa-lg fa-fw" aria-hidden="true"></i></a>';
                                                        
                        }
                        
                    }else{
                           //SGTA-62
                        // <a href="#" title="Solicitar Abertura de Processo de Inspeção/Auditoria Direto" onclick="return auditoria_consulta('.$row3[$index]['CodRegistro'].');" class="especial">
                        //<i class="fa fa-user-secret fa-lg fa-fw" aria-hidden="true"></i></a>
                        $row3[$index]['icone'] =    '<a href="#" title="Exibir mais detalhes." onclick="return detalhes('.$row3[$index]['CodRegistro'].',\'' .@$_REQUEST['detalhe']  . '\');" class="incluir">
                                                        <i class="fa fa-plus-circle fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    <a href="#" title="Incluir uma observação no registro." onclick="return info('.$row3[$index]['CodRegistro'].',1,'.$index.');" class="editar">
                                                        <i class="fa fa-info-circle fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    <a href="#" title="Remover do resultado da trilha este registro, ele não será enviado ao órgão." onclick="return rejeitar('.$row3[$index]['CodRegistro'].',' .$row3[$index]['CodTrilha'] .');" class="excluir">
                                                        <i class="fa fa-minus-circle fa-lg fa-fw" aria-hidden="true"></i></a>';
                        //SGTA-85
                        if($row3[$index]["StatusRegistro"] != "concluído" or $CodPerfil <= 2 ) {
                                $row3[$index]['icone'] .= '<a href="#" title="Anexar um arquivo." onclick="return anexar_arquivo('.$row3[$index]['CodRegistro'].',\'registro\','.$linha.');" class="rosa">
                                                        <i class="fa fa-paperclip fa-lg fa-fw" aria-hidden="true"></i></a>';
                        }
                        
                        $row3[$index]['icone'] .='<a href="#" title="Reincluir o registro no resultado da trilha." onclick="return reincluir('.$row3[$index]['CodRegistro'].');" class="marron">
                                                        <i class="fa fa-recycle fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    <a href="#" title="Mesclar registro com outro." onclick="return mesclar('.$row3[$index]['CodRegistro'].');" class="cyan">
                                                        <i class="fa fa-random fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    <i style="color: silver;" class="fa fa-hand-pointer-o fa-lg fa-fw" aria-hidden="true"></i>
                                                    <a href="#" title="Concluir Análise" onclick="return concluir_pre_analise('.$row3[$index]['CodRegistro'].','.$ReservadoPor.','.$linha.');" class="verde">
                                                        <i class="fa fa-check-circle fa-lg fa-fw" aria-hidden="true"></i></a>
                                                   
                                                       

                                                    <a href="#" title="Consultar Justificativas/Pareceres/Eventos." onclick="return consulta_andamento('.$row3[$index]['CodRegistro'].');" class="marron">
                                                        <i class="fa fa-search fa-lg fa-fw" aria-hidden="true"></i></a>
                                                    '; 
                    }      
                }
                
            }elseif($acao == "análise"){
                
                if($row3[$index]['StatusRegistro'] !== "em justificativa" && $row3[$index]['StatusRegistro'] !== "pendente" && $row3[$index]['StatusRegistro'] !== "devolvido"){
                    $row3[$index]['icone'] =    '<i class="fa fa-pencil-square fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                    if($row3[$index]['StatusRegistro'] == "concluído" || $row3[$index]['AnalisadoPor'] != "" || $row3[$index]['SolicitadoPor'] != ""){
                        $row3[$index]['icone'] = $row3[$index]['icone'] . ' <i class="fa fa-weixin fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                    }else{
                        $row3[$index]['icone'] = $row3[$index]['icone'] . ' <a href="#" title="Solicitar Alteração de Justificativa" onclick="return solicitar_alteracao_justificativa('.$row3[$index]['CodRegistro'].');" class="cyan">                                                      <i class="fa fa-weixin fa-lg fa-fw" aria-hidden="true"></i></a>';
                    }
                }else{
                    
                    $Fator = substr($row3[$index]['CodRegistro'], -1);
                    $CodRegBase64 = $row3[$index]['CodRegistro'] * 1378;
                    $CodRegBase64 = base64_encode($Fator . $CodRegBase64);

                    //andre - porque codificar isso? esqueceram de alterar o campo no banco de dados?
                    //primeiro parametro da justificar_inconsistencia foi trocado de $CodRegBase64 para $row3[$index]['CodRegistro'] - verificar
                    
                    $row3[$index]['icone'] =    '<a href="#" title="Justificar Inconsistência" onclick="return justificar_inconsistencia(\''.$row3[$index]['CodRegistro'].'\','.$ReservadoPor.','.$linha.','.$row3[$index]['CodRegistro'].');" class="editar">
                                                <i class="fa fa-pencil-square fa-lg fa-fw" aria-hidden="true"></i></a>
                                                <i class="fa fa-weixin fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                }

                $auxIcone =  $row3[$index]['icone'];

                $row3[$index]['icone'] =    '<a href="#" title="Exibir mais detalhes." onclick="return detalhes('.$row3[$index]['CodRegistro'].',\'' .@$_REQUEST['detalhe']  . '\');" class="incluir">
                                                <i class="fa fa-plus-circle fa-lg fa-fw" aria-hidden="true"></i></a>';
                                             
                //SGTA-85
                if($row3[$index]["StatusRegistro"] != "concluído" or $CodPerfil <= 2 ) {
                    $row3[$index]['icone'] .='<a href="#" title="Anexar um arquivo." onclick="return anexar_arquivo('.$row3[$index]['CodRegistro'].',\'registro\','.$linha.');" class="rosa">
                                        <i class="fa fa-paperclip fa-lg fa-fw" aria-hidden="true"></i></a>';
                } elseif($row3[$index]['QuarentenaAte'] != "" && $row3[$index]['StatusRegistro'] == "concluído" ) { //habilitar anexar arquivo em monitoramento

                    $row3[$index]['icone'] .='<a href="#" title="Anexar um arquivo." onclick="return anexar_arquivo('.$row3[$index]['CodRegistro'].',\'registro\','.$linha.');" class="rosa">
                    <i class="fa fa-paperclip fa-lg fa-fw" aria-hidden="true"></i></a>';
                }

                $row3[$index]['icone'] .= $auxIcone.'
                                            <a href="#" title="Consultar Justificativas/Pareceres/Eventos." onclick="return consulta_andamento('.$row3[$index]['CodRegistro'].');" class="marron">
                                                <i class="fa fa-search fa-lg fa-fw" aria-hidden="true"></i></a>
                                            <a href="#" title="Imprimir Justificativas/Pareceres/Eventos" onclick="return imprimir_andamento('.$row3[$index]['CodRegistro'].');" class="azul">
                                                <i class="fa fa-print fa-lg fa-fw" aria-hidden="true"></i></a>';
                
            }elseif($acao == "validação"){

                if($row3[$index]["StatusRegistro"] == "quarentena" || $row3[$index]["StatusRegistro"] == "concluído" || $row3[$index]["StatusRegistro"] == "em análise" || $row3[$index]["StatusRegistro"] == "devolvido" || $row3[$index]["StatusRegistro"] == "justificado"){
                    if ($ReservadoPor != 0) {
                        $respcge = $obj_usuario->consulta_nome($ReservadoPor);
                        $row3[$index]['RespCGE'] = '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\''.$respcge.'\')">'.$respcge.'</a>';
                    } elseif ($row3[$index]["UsuarioCGE"] != "") {
                        $respcge = $obj_usuario->consulta_nome($row3[$index]["UsuarioCGE"]);
                        $row3[$index]['RespCGE'] = '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\''.$respcge.'\')">'.$respcge.'</a>';
                    } else {
                        $row3[$index]['ReservadoPor'] = "";
                    }
                }
                
                //ATENÇÃO andre
                //SGTA-84 
                if($row3[$index]["StatusRegistro"] == "concluído" || $nao_analisado == true) {
                    $acoes = '&nbsp<a href="#" title="Ações Complementares" onclick="return acoes_complementares('.$row3[$index]['CodRegistro'].')"><i class="fa fa-compass fa-lg fa-fw" aria-hidden="true"></i></a>';
                } else {
                    $acoes = '&nbsp<i class="fa fa-compass fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                }
                
                //SGTA-73 , SGTA-84 
                //&& $ReservadoPor == $cpf_user - qualquer coisa apagar
                if($row3[$index]['SolicitadoAlteracao'] == true && $perfil == 2 && $row3[$index]['StatusRegistro'] !== "concluído" && $row3[$index]['StatusRegistro'] != "em justificativa" &&  $row3[$index]['AnalisadoPor'] != "" && $ReservadoPor == $cpf_user){

                    $justificar = '<a href="#" title="Validar Justificativa" onclick="return validar_justificativa('.$row3[$index]['CodRegistro'].','.$ReservadoPor.','.$linha.');" class="editar">
                    <i class="fa fa-thumbs-up fa-lg fa-fw" aria-hidden="true"></i></a>';

                      //SGTA-89 - tabalhar aqui - ve se fica and $row3[$index]['StatusRegistro'] == "justificado" 
                     // if( is_null($row3[$index]['SolicitacaoAceita'] || $row3[$index]['SolicitacaoAceita']==true)  and $row3[$index]['StatusRegistro'] == "justificado"  ){
                       //   $justificar = '<i class="fa fa-thumbs-up fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                      // }
                }
                elseif( $row3[$index]['SolicitadoAlteracao'] == true && $row3[$index]['AnalisadoPor'] == "" ) {

                    $justificar = '<i class="fa fa-thumbs-up fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                   
                    //andré, ver se isso fica ($row3[$index]['StatusRegistro'] == "justificado") SGTA-93 /se se fica o status em concluido
                    if( $perfil == 2 && $ReservadoPor != $cpf_user &&  $ReservadoPor !="") {
                        $justificar = '<a href="#" title="Redistribuir o registro." onclick="return redistribuir_registro('.$row3[$index]['CodRegistro'].','.$ReservadoPor.','.$row3[$index]['CodTipo'].');" class="editar">
                        <i class="fa fa-paper-plane fa-lg fa-fw" aria-hidden="true"></i></a>';
                    }

                } elseif ($row3[$index]['StatusRegistro'] != "justificado" && $row3[$index]['StatusRegistro'] != "em análise") {
                    $justificar = '<i class="fa fa-thumbs-up fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                } elseif($ReservadoPor == "") {
                    $justificar = '<i class="fa fa-thumbs-up fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                } elseif($ReservadoPor != $cpf_user && $perfil != 2) {
                    $justificar = '<i class="fa fa-thumbs-up fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                } elseif($ReservadoPor != $cpf_user && $perfil == 2) { //*****aqui solicitar redistribuição.
                    $justificar = '<a href="#" title="Redistribuir o registro." onclick="return redistribuir_registro('.$row3[$index]['CodRegistro'].','.$ReservadoPor.','.$row3[$index]['CodTipo'].');" class="editar">
                                    <i class="fa fa-paper-plane fa-lg fa-fw" aria-hidden="true"></i></a>';
                } else {
                    $justificar = '<a href="#" title="Validar Justificativa" onclick="return validar_justificativa('.$row3[$index]['CodRegistro'].','.$ReservadoPor.','.$linha.');" class="editar">
                                    <i class="fa fa-thumbs-up fa-lg fa-fw" aria-hidden="true"></i></a>';
                }

                if(($row3[$index]['StatusRegistro'] != "justificado" && $row3[$index]['StatusRegistro'] != "em análise") || $ReservadoPor != $cpf_user){
                    $row3[$index]['icone'] = $justificar.
                                '<i class="fa fa-commenting fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>
                                '.$acoes.'
                                <a href="#" title="Consultar Justificativas/Pareceres/Eventos." onclick="return consulta_andamento('.$row3[$index]['CodRegistro'].');" class="marron">
                                    <i class="fa fa-search fa-lg fa-fw" aria-hidden="true"></i></a>
                                <a href="#" title="Imprimir Justificativas/Pareceres/Eventos" onclick="return imprimir_andamento('.$row3[$index]['CodRegistro'].');" class="azul">
                                    <i class="fa fa-print fa-lg fa-fw" aria-hidden="true"></i></a>';
                } else {
                    
                    if($row3[$index]['SolicitadoAlteracao'] == true && $row3[$index]['AnalisadoPor'] == "") {
                        $row3[$index]['icone'] = $justificar.
                                    '<a href="#" title="Analisar Pedido de Alteração" onclick="return analisar_pedido_alteracao('.$row3[$index]['CodRegistro'].');" class="steelblue">
                                    <i class="fa fa-commenting fa-lg fa-fw" aria-hidden="true"></i></a>
                                    '.$acoes.'
                                    <a href="#" title="Consultar Justificativas/Pareceres/Eventos." onclick="return consulta_andamento('.$row3[$index]['CodRegistro'].');" class="marron">
                                        <i class="fa fa-search fa-lg fa-fw" aria-hidden="true"></i></a>
                                    <a href="#" title="Imprimir Justificativas/Pareceres/Eventos" onclick="return imprimir_andamento('.$row3[$index]['CodRegistro'].');" class="azul">
                                        <i class="fa fa-print fa-lg fa-fw" aria-hidden="true"></i></a>';
                    } else {
                        $row3[$index]['icone'] = $justificar.
                                    '<i class="fa fa-commenting fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>
                                    '.$acoes.'
                                    <a href="#" title="Consultar Justificativas/Pareceres/Eventos." onclick="return consulta_andamento('.$row3[$index]['CodRegistro'].');" class="marron">
                                        <i class="fa fa-search fa-lg fa-fw" aria-hidden="true"></i></a>
                                    <a href="#" title="Imprimir Justificativas/Pareceres/Eventos" onclick="return imprimir_andamento('.$row3[$index]['CodRegistro'].');" class="azul">
                                        <i class="fa fa-print fa-lg fa-fw" aria-hidden="true"></i></a>';
                    }
                    
                        
                }
                
                if($row3[$index]['StatusRegistro'] != "concluído"){
                    $obs = '<a href="#" title="Incluir uma observação no registro." onclick="return info('.$row3[$index]['CodRegistro'].',1,'.$index.');" class="editar">
                                                <i class="fa fa-info-circle fa-lg fa-fw" aria-hidden="true"></i></a>';
                }else{
                    $obs = '<i class="fa fa-info-circle fa-lg fa-fw" aria-hidden="true" style="color: lightgray;"></i>';
                }
                

                $auxIcone2 =  $row3[$index]['icone'];

                $row3[$index]['icone'] =    '<a href="#" title="Exibir mais detalhes." onclick="return detalhes('.$row3[$index]['CodRegistro'].',\'' .@$_REQUEST['detalhe']  . '\');" class="incluir">
                                                <i class="fa fa-plus-circle fa-lg fa-fw" aria-hidden="true"></i></a>'.$obs;
                //SGTA-85
                if($row3[$index]["StatusRegistro"] != "concluído" or $CodPerfil <= 2 ) {
                    $row3[$index]['icone'] .=' <a href="#" title="Anexar um arquivo." onclick="return anexar_arquivo('.$row3[$index]['CodRegistro'].',\'registro\','.$linha.');" class="rosa">
                                                <i class="fa fa-paperclip fa-lg fa-fw" aria-hidden="true"></i></a>';
                }
                
                $row3[$index]['icone'] .=    $auxIcone2;

            }
            
            if($row3[$index]['QuarentenaAte'] != "" && $row3[$index]['StatusRegistro'] == "concluído" ){
                $row3[$index]['cor'] = "darkorange";
                $row3[$index]['StatusRegistro'] = "monitoramento";
            }
            
            $row3[$index]['StatusRegistro'] = '<a href="#" style="color: '.$row3[$index]['cor'].'" onclick="return filtrar_tabela(\'tbl_trilha\',\''.$row3[$index]['StatusRegistro'].'\')">'.$row3[$index]['StatusRegistro'].'</a>';
        }
    }
    
    $array['economiagerada'] = numero_br($array['economiagerada']);
    
    $sortArray = array(); 
    
    //die(var_dump($row3));
    if (!isset($_classificar)){
        if(count($row3)>0){
            foreach($row3 as $recordset){ 
                foreach($recordset as $key=>$value){ 
                    if(!isset($sortArray[$key])){ 
                        $sortArray[$key] = array(); 
                    } 
                    $sortArray[$key][] = $value; 
                } 
            } 
            
            $orderby = "Nome_Descricao"; //change this to whatever key you want from the array 

            array_multisort($sortArray[$orderby],SORT_ASC,$row3); 
        }
    
      
    }
        
    