var pergunta_ok_acao;

function submit_data(destino){
    $("#popup_dialogo").dialog("close");
    $("#loading").show();
    $("#imgloader").show();
    $.ajax({
        type: 'post',
        url: destino,
        data: formdata,
        cache: false,
        timeout: 300000,
        success: function(data){
            try{
                try{
                    data = $.parseJSON(data);
                }catch(err){
                    $("#msg_erro").html("ERRO AO DECODIFICAR O PACOTE RECEBIDO DO SERVIDOR: ->" + data + "<-");
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.noticia !== ""){
                    alert("Notícia: " + data.noticia);
                    return false;
                }
                if(data.erro !== ""){
                    $("#msg_erro").html(data.erro);
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.alerta !== ""){
                    $("#msg_alerta").html(data.alerta);
                    $("#popup_alerta").dialog("open");
                    return false;
                }
                
                server(data);
                    
            }
            catch(err){
                $("#msg_erro").html(err + "   " + data);
                $("#popup_erro").dialog("open");
                return false;
            } 
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            $("#msg_erro").html(errorThrown);
            $("#popup_erro").dialog("open");
            return false;
        },
        complete: function(XMLHttpRequest, status){
            $("#loading").hide();
            $("#imgloader").hide();
            return false;
        }
    });
};

function submit_arquivo(destino){
    $("#popup_dialogo").dialog("close");
    $("#loading").show();
    $("#imgloader").show();
    $.ajax({
        type: 'post',
        url: destino,
        data: formdata,
        cache: false,
        timeout: 120000,
        contentType: false,
        processData: false,
        success: function(data){
            try{
                try{
                    data = $.parseJSON(data);
                }catch(err){
                    $("#msg_erro").html("ERRO AO DECODIFICAR O PACOTE RECEBIDO DO SERVIDOR: ->" + data + "<-");
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.noticia !== ""){
                    alert("Notícia: " + data.noticia);
                    return false;
                }
                if(data.erro !== ""){
                    $("#msg_erro").html(data.erro);
                    $("#popup_erro").dialog("open");
                    return false;
                }
                if(data.alerta !== ""){
                    $("#msg_alerta").html(data.alerta);
                    $("#popup_alerta").dialog("open");
                    return false;
                }
                
                server(data);
                    
            }
            catch(err){
                $("#msg_erro").html(err + "   " + data);
                $("#popup_erro").dialog("open");
                return false;
            } 
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            $("#msg_erro").html(errorThrown);
            $("#popup_erro").dialog("open");
            return false;
        },
        complete: function(XMLHttpRequest, status){
            $("#loading").hide();
            $("#imgloader").hide();
            return false;
        }
    });
};

function server(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "observacao_cge":
                retorno_info(retorno);
                break;
            case "gravar_observacao_cge":
                retorno_observacao_cge(retorno);
                break;
            case "listar_anexos":
                retorno_visualizar_anexos(retorno);
                break;
            case "upload":
                retorno_subir_arquivo(retorno);
                break;
            case "upload_trilha":
                retorno_subir_arquivo(retorno);
                break;
            case "upload_trilha_continuo":
                retorno_subir_arquivo_continuo(retorno);
                break;
            case "justificar_inconsistencia":
                retorno_justificar_inconsistencia(retorno);
                break;
            case "consultar_justificativa":
                retorno_consultar_justificativa(retorno);
                break;
            case "concluir_analise":
                retorno_analise(retorno);
                break;
            case "salvar_analise":
                retorno_analise(retorno);
                break;    
            case "recusar_trilha":
                execute_cod = 1;
                $("#msg_sucesso").html("Trilha recusada com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "enviar_trilha":
                execute_cod = 1;
                
                var msg = "Trilha distribuída aos órgãos com sucesso.<br>";
                
                if(retorno.erro_email != ""){
                    msg = msg + retorno.erro_email;
                }
                
                if(retorno.alerta_email != ""){
                    msg = msg + retorno.alerta_email;
                }
                
                msg = msg + retorno.mensagem_email;
                
                $("#msg_sucesso").html(msg);
                $("#popup_sucesso").dialog("open");
                break;
            case "enviar_registros":
                    execute_cod = 1;
                    
                    var msg = "Registros distribuídos aos órgãos com sucesso.<br>";
                    
                    if(retorno.erro_email != ""){
                        msg = msg + retorno.erro_email;
                    }
                    
                    if(retorno.alerta_email != ""){
                        msg = msg + retorno.alerta_email;
                    }
                    
                    msg = msg + retorno.mensagem_email;
                    
                    $("#msg_sucesso").html(msg);
                    $("#popup_sucesso").dialog("open");
                    break;
            case "enviar_registro":
                        //SGTA 173
                        execute_cod = 1;
                        
                        var msg = "Registro distribuído ao órgão com sucesso.<br>";
                        
                        if(retorno.erro_email != ""){
                            msg = msg + retorno.erro_email;
                        }
                        
                        if(retorno.alerta_email != ""){
                            msg = msg + retorno.alerta_email;
                        }
                        
                        msg = msg + retorno.mensagem_email;
                        
                        $("#msg_sucesso").html(msg);
                        $("#popup_sucesso").dialog("open");
                        break;
            case "rejeitar_registro":
                execute_cod = 2; 
                $("#msg_sucesso").html("Registro Rejeitado com Sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "reincluir_registro":
                execute_cod = 2;
                $("#msg_sucesso").html("Registro Re-incluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "excluir_anexo":
                execute_cod = 2;
                $("#msg_sucesso").html("Arquivo excluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "excluir_arquivo":
                execute_cod = 2;
                $("#popup_anexos").dialog("close");
                $("#msg_sucesso").html("Arquivo excluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "analisar_justificativa":
                analisar_justificativa_retorno(retorno);
                break;
            case "salvar_validacao":
                retorno_salvar_validacao(retorno);
                break;
            case "andamento":
                consulta_andamento_retorno(retorno);
                break;
            case "consulta_justificativas":
                consulta_justificativas_retorno(retorno);
                break;
            case "solicitar_alteracao_justificativa":
                retorno_alteracao(retorno);
                break;
            case "analisar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "gravar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "aceitar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "negar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "salvar_instrucao":
                retorno_salvar_instrucao();
                break;
            case "consulta_complementar":
                retorno_acoes_complementares(retorno);
                break;
            case "acoes_complementares":
                retorno_salvar_complementar(retorno);
                break;
            case "ordem_servico":
                retorno_consulta_ost(retorno);
                break;
            case "mesclar_consulta":
                retorno_mesclar(retorno);
                break;
            case "mesclar_save":
                retorno_mesclar_save(retorno);
                break;    
            case "consultar_msg":
                retorno_visualizar_mensagens(retorno);
                break;
            case "equipe_msg":
                retorno_cadastrar_msg(retorno);
                break;
            case "cadastrar_msg":
                retorno_salvar_msg(retorno);
                break;
            case "salvar_msg":
                retorno_salvar_msg(retorno);
                break;
            case "editar_msg":
                retorno_alterar_msg(retorno);
                break;
            case "excluir_msg":
                retorno_excluir_msg(retorno);
                break;
            case "consultar_dist":
                retorno_distribuir_registro(retorno);
                break;
            case "vincular_dist":
                retorno_vincular_registro(retorno);
                break;
            case "concluir_pre_analise":
                retorno_concluir_pre_analise(retorno);
                break;
            case "auditoria_consulta":
                retorno_auditoria_consulta(retorno);
                break;
            case "auditoria_pre_analise":
                retorno_auditoria_pre_analise(retorno);
                break;
            case "redistribuir_consulta":
                retorno_redistribuir_consulta(retorno);
                break;
             case "redistribuir_para":
                retorno_redistribuir_para(retorno);
                break;  
            case "salvar_mensagem_trilha":
                retorno_salvar_mensagem_trilha(retorno);
                break;
            case "alterar_mensagem_trilha":
                retorno_alterar_mensagem_trilha(retorno);
                break;
            case "excluir_mensagem_trilha":
                retorno_excluir_mensagem_trilha(retorno);
                break;
            case "mostrar_observacao":
                retorno_alterar_observacao(retorno);
                break;
            case "ocultar_observacao":
                retorno_alterar_observacao(retorno);
                break;    
            default:
                alert("js: Operação Inválida --> ");
        };
    }
}



function pergunta_ok(){
    
    switch (pergunta_ok_acao){
        case 1:
            $("#popup_pergunta").dialog("close");
            submit_data("server.php");
            break;
        case 2:
            submit_arquivo("form_upload/server.php");
            break;
        case 3:
            submit_data("server.php");
            break;    
        default:
            alert("js: Operação Inválida.");
    };
    
        
}

function recusar_trilha(cod){
    $("#operacao").val("recusar_trilha");
    formdata = $("#form1").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<label style='color: red;'>Você tem certeza que gostaria de recusar esta trilha?</label>");
    $("#popup_pergunta").dialog("open");
}

function enviar_trilha(cod){
    
    var nicE = new nicEditors.findEditor('editor');
    var str = nicE.getContent();
    
    $("#Instrucao").val(str.trim());
   
    if($("#Instrucao").val() == "" || $("#Instrucao").val() == "<br>"){
        $("#msg_alerta").html("É necessário preencher as instruções para a justificativa.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    
    $("#operacao").val("enviar_trilha");
    formdata = $("#form1").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<label style='color: darkgreen;'>Você tem certeza que deseja distribuir a trilha aos órgãos?</label><br><label style='color: darkorange;'>Foi(Foram) enviado(s) o(s) Ofício(s) de comunicação?</label><br>Obs: Esta operação não poderá ser desfeita.");
    $("#popup_pergunta").dialog("open");
}

function enviar_registros(){
    //SGTA 173
    $("#operacao").val("enviar_registros");
    formdata = $("#form1").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<label style='color: darkgreen;'>Você tem certeza que deseja distribuir os registros aos órgãos?</label><br><label style='color: darkorange;'>Foi(Foram) enviado(s) o(s) Ofício(s) de comunicação?</label><br>Obs: Esta operação não poderá ser desfeita.");
    $("#popup_pergunta").dialog("open");
}

function enviar_registro(cod){
    //SGTA 173
    $("#operacao").val("enviar_registro");
    $("#CodRegistroContinua").val(cod);
    formdata = $("#form1").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<label style='color: darkgreen;'>Você tem certeza que deseja distribuir o registro selecionado ao órgão?</label><br><label style='color: darkorange;'>Foi(Foram) enviado(s) o(s) Ofício(s) de comunicação?</label><br>Obs: Esta operação não poderá ser desfeita.");
    $("#popup_pergunta").dialog("open");
}


function salvar_instrucao(){
    var nicE = new nicEditors.findEditor('editor');
    var str = nicE.getContent();
    
    $("#Instrucao").val(str.trim());
   
    if($("#Instrucao").val() == "" || $("#Instrucao").val() == "<br>"){
        $("#msg_alerta").html("É necessário preencher as instruções para a justificativa.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    
    $("#operacao").val("salvar_instrucao");
    formdata = $("#form1").serialize();
    
    submit_data("server.php");
}

function retorno_salvar_instrucao(){
    $("#msg_sucesso").html("Salvo.");
    $("#popup_sucesso").dialog("open");
}

function imprimir_instrucao(){
    var nicE = new nicEditors.findEditor('editor');
    var str = nicE.getContent();
    
    $("#Instrucao").val(str.trim());
    
    document.getElementById("form1").target = "_blank";
    document.getElementById("form1").action = "reports/instrucoes.php";
    document.getElementById("form1").submit();
    document.getElementById("form1").target = "";
}

function rejeitar(cod,codTrilha){
    
    var texto = $("#td_obscge_" + cod).text();
    if(texto == ""){
        $("#msg_alerta").text("Antes de remover o registro é importante que se cadastre no campo observação o motivo da rejeição.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    
    $("#operacao").val("rejeitar_registro");
    $("#CodRegistro").val(cod);
    $("#CodTrilha").val(codTrilha);
    formdata = $("#form1").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<label style='color: red;'>Você tem certeza que gostaria de remover o registro do resultado da trilha?</label><br><b>É importante que se cadastre no campo observação o motivo da rejeição do registro.</b>");
    $("#popup_pergunta").dialog("open");
    return false;
}

function reincluir(cod){
    $("#operacao").val("reincluir_registro");
    $("#CodRegistro").val(cod);
    formdata = $("#form1").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<label>Gostaria de Reincluir o registro na trilha?</label>");
    $("#popup_pergunta").dialog("open");
    return false;
}

function excluir_anexo(CodAnexo){
    $("#CodAnexo").val(CodAnexo);
    $("#operacao").val("excluir_anexo");
    formdata = $("#form1").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<span style='color: red;'>Você tem certeza que gostaria de excluir este arquivo?</span>");
    $("#popup_pergunta").dialog("open");
    return false;
}

function cruzamento_enviar(parametro){
    if($("#cruzamento_campo").val() == ""){
        $("#msg_alerta").text("Por favor preencha o campo ''Valor''.");
        $("#popup_alerta").dialog("open");
        return false;
    }
    $("#cruzamento_parametro").val(parametro);
    document.getElementById("form_cruzamento").submit();
}

function imprimir_andamento(Cod){
    $("#5_CodRegistro").val(Cod);
    document.getElementById("5_form1").submit();
    return false;
}

function cadastrar_mensagem_trilha(){
    $("#14_msg").val("");
    $("#14_CodMensagem").val(-1);
    $("#14_operacao").val("salvar_mensagem_trilha");
    $("#popup_nova_mensagem_trilha").dialog("open");
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../inicio/inicio.php");
            break;
        case 2:
            $("#operacao").val("update");
            formdata = $("#form1").serialize();
            submit_form(destino);
            break;
        case 3:
            $("#operacao").val("load");
            formdata = $("#form1").serialize();
            submit_form(destino);
            break;
    }
    execute_cod = 0;
}