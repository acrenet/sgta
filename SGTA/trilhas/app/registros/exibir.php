<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $CodPerfil = $_SESSION['sessao_perfil'];
    $CodOrgao = $_SESSION['sessao_orgao'];
    $CPF = $_SESSION['sessao_id'];
    

    if(!isset($_POST['detalhe'])){
        Header("Location: /trilhas/app/inicio/negado.php?op=4");
        die();
    }
    
    if(isset($_POST['filtro'])){
        $filtro = $_POST['filtro'];
    }else{
        $filtro = "";
    }
    
  
    $Orgao = $_POST['Orgao'];
    $CodTrilha = base64_decode($_POST['CodTrilha'])/2999;
    $NomeTrilha = $_POST['NomeTrilha'];
    $detalhe = $_POST['detalhe'];
    $acao = $_POST['acao'];
    
    $operacao = "load";
    
    $exibircriterios = "";
    
    if($CodPerfil > 2){
        $exportarexcel = "display: none;";
    }else{
        $exportarexcel = "";
    }

    if($acao == "pré-análise"){
        if($CodPerfil > 3){
            Header("Location: /trilhas/app/inicio/negado.php?op=1");
            die();
        }
        $_titulo = "Trilha: $NomeTrilha, Id: $CodTrilha - Homologação";
    }else if($acao == "análise"){
        if($CodPerfil != 5 && $CodPerfil != 6 && $CodPerfil != 1){
            Header("Location: /trilhas/app/inicio/negado.php?op=2");
            die();
        }
        $_titulo = "Trilha: $NomeTrilha, Id: $CodTrilha - Análise";
        $exibircriterios = "display: none;";
    }else if($acao == "validação"){
        if($CodPerfil > 3){
            Header("Location: /trilhas/app/inicio/negado.php?op=3");
            die();
        }
        $_titulo = "Trilha: $NomeTrilha, Id: $CodTrilha - Auditoria";
    }else{
        $_titulo = "Trilha: $NomeTrilha, $CodTrilha - Consulta";
        if($CodPerfil > 3){
            $exibircriterios = "display: none;";
        }     
    }
    
    require_once '../../obj/conexao.php';
    $objcon = new conexao();

    $lista = "";
    
    if($CodPerfil < 4){
     
        try{
            $sql = "Select Distinct CodOrgao, NomeOrgao From view_registros Where CodTrilha = $CodTrilha And StatusRegistro <> 'excluído' Order By NomeOrgao";
           
            $query = $objcon->select($sql);
            if($objcon->erro != ""){
                echo $objcon->erro;
            }else{
                $sql = "Select CodTipo From view_trilhas Where CodTrilha = $CodTrilha";
               
                $query2 = $objcon->select($sql);
                if($objcon->erro != ""){
                    echo $objcon->erro;
                }else{
                    $row = mysqli_fetch_array($query2);
                    $CodTipo = $row['CodTipo'];
                    $sql = "Select Distinct CodOrgao From view_autorizacoes Where CodPerfil = 5 And CodTipo = $CodTipo";
                    
                 

                    $query2 = $objcon->select($sql);
                    if($objcon->erro != ""){
                        echo $objcon->erro;
                    }else{
                        $orgaos_autorizados[] = -1;
                        while ($row = mysqli_fetch_assoc($query2)){
                            $orgaos_autorizados[] = $row;
                        }
                        while ($row = mysqli_fetch_array($query)){
                            $possui_cadastrado = false;
                            $SiglaOrgao = $row["NomeOrgao"];
                            for ($index = 0; $index < count($orgaos_autorizados); $index++){
                                if($row['CodOrgao'] == $orgaos_autorizados[$index]["CodOrgao"]){
                                    $possui_cadastrado = true; 
                                }
                            }
                            if($possui_cadastrado == false){
                                //incluir na lista
                                $lista .= "<li class='list-group-item'>$SiglaOrgao</li>";

                            }
                        }
                        
                    }      
                }    
            }
        } catch (Exception $ex){
            echo $ex->getMessage();
        }
    }
    if($lista != ""){
        $lista = "<h4 style='color: red; text-align: justify; font-weight: bold;'>ATENÇÃO: Os seguintes órgãos/unidades administrativas não possuem nenhum supervisor cadastrado "
                . "para a área desta trilha.</h4>"
                . "<ul class='list-group'>$lista</ul>";
    }
    
    if($CodPerfil < 5){
        $exibir_msg_cge = "";
    }else{
        $exibir_msg_cge = "style='display:none;'";
    }


    //SGTA 173- testar se é trilha continua

    $sql = "SELECT count(*) as CONTINUA  FROM `registros` where statusRegistro = 'rascunho' and  CodTrilha =  " . $CodTrilha;
   
                    
    $query2 = $objcon->select($sql);
    if($objcon->erro != ""){
        echo $objcon->erro;
    }else{
        $continua = false;
        while ($row = mysqli_fetch_assoc($query2)){
            if($row['CONTINUA'] >0 )
            $continua =true;
        }
    }

    //sgta 173 - mostra botao de registros excluidos
    $sql2 = "SELECT continua from trilhas where  CodTrilha =  " . $CodTrilha;
   
                    
    $query3 = $objcon->select($sql2);
    if($objcon->erro != ""){
        echo $objcon->erro;
    }else{
        $vmExibeBtn = false;
        while ($row2 = mysqli_fetch_assoc($query3)){
            if($row2['continua'] == 1 )
            $vmExibeBtn =true;
        }
    }

?>

<link href="exibir.css" rel="stylesheet" type="text/css"/>

<script src="exibir.js" type="text/javascript"></script>
<script src="../../js/nicEdit.js" type="text/javascript"></script>
<link href="../../fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="../../css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="../../css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="../../master/master.css" rel="stylesheet" type="text/css"/>


<script src="../../js/jquery-1.12.3.js" type="text/javascript"></script>
<script src="../../js/bootstrap.js" type="text/javascript"></script>
<script src="../../js/jquery-ui.js" type="text/javascript"></script>
<script src="../../master/master.js" type="text/javascript"></script>

<link rel="shortcut icon" href="../../images/favicon.ico">
<br>
 
<form name="form1" id="form1" method="POST"  target="_blank">
    <input type="hidden" name="operacao" id="operacao" value="<?php echo $operacao; ?>" />
    <input type="hidden" name="acao" id="acao" value="<?php echo $acao; ?>" />
    <input type="hidden" name="CodPerfil" id="CodPerfil" value="<?php echo $CodPerfil; ?>" />
    <input type="hidden" name="CodOrgao" id="CodOrgao" value="<?php echo $CodOrgao; ?>" />
    <input type="hidden" name="CPF" id="CPF" value="<?php echo $CPF; ?>" /> 
    <input type="hidden" name="Orgao" id="Orgao" value="<?php echo $Orgao; ?>" />
    <input type="hidden" name="CodTrilha" id="CodTrilha" value="<?php echo $CodTrilha; ?>" />
    <input type="hidden" name="NomeTrilha" id="NomeTrilha" value="<?php echo $NomeTrilha; ?>" />
    <input type="hidden" name="detalhe" id="detalhe" value="<?php echo $detalhe; ?>" />
    <input type="hidden" name="CodRegistro" id="CodRegistro" value="" />
    <input type="hidden" name="CodAnexo" id="CodAnexo" value="" />
    <input type="hidden" name="Instrucao" id="Instrucao" value="" />
    <input type="hidden" name="filtro" id="filtro" value="<?php echo $filtro; ?>" />
    <input type="hidden" name="CodRegistroContinua" id="CodRegistroContinua" value="" />
    <input type="hidden" name="WhereContinuo" id="WhereContinuo" value="" />
</form>

<div class="container">
    
    <div class="well" style="font-size: 150%; font-weight: bold;" id="titulo">Trilha</div>
    
    <form class="form-horizontal" role="form" method="post">


        <div class="form-group" style="margin-bottom: 4px;">
            <label id="lbl_titulo" class="control-label col-sm-2" for="trilha">Trilha:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control input-sm" id="trilha" name="trilha" value="" style="font-weight: bold; font-size: 130%;" disabled>
            </div>
        </div>
        
        <div class="form-group" style="margin-bottom: 4px; <?php echo $exibircriterios; ?>">
            <label id="lbl_titulo" class="control-label col-sm-2" for="criterios">Critérios:</label>
            <div class="col-sm-10">
                <textarea name="criterios" id="criterios" class="form-control input-sm" rows="5" cols="20" style="background-color: #eee" readonly=""></textarea>
            </div>
        </div>

    </form>
    
    <div class="row">
        <div class="col-sm-6">
            <form class="form-horizontal" role="form" method="post">
                
                <div class="form-group" style="margin-bottom: 4px;">
                    <label class="control-label col-sm-4" for="dtini">Data Início:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control input-sm" id="dtini" name="dtini" value="" disabled>
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 4px;">
                    <label class="control-label col-sm-4" for="status">Status:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm" id="status" name="status" value="Homologação" disabled>
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 4px;">
                    <label class="control-label col-sm-4" for="totalreg">Total de Registros:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control input-sm" id="totalreg" name="totalreg" value="" disabled>
                    </div>
                </div>
                
                <div class="form-group" style="margin-bottom: 4px; <?php echo $exibircriterios; ?>">
                    <label class="control-label col-sm-4" for="economiapotencial">Expectativa Resultado:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control input-sm" id="economiapotencial" name="economiapotencial" value="" style="text-align: right;" disabled>
                    </div>
                </div>

            </form>
        </div>
        <div class="col-sm-6">
            <form class="form-horizontal" role="form" method="post">
                
                <div class="form-group" style="margin-bottom: 4px;">
                    <label class="control-label col-sm-5" for="dtter">Data Término:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control input-sm" id="dtter" name="dtter" value="" disabled>
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 4px;">
                    <label class="control-label col-sm-5" for="rega">Registros Justificados:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control input-sm" id="rega" name="rega" value="" disabled>
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 4px;">
                    <label class="control-label col-sm-5" for="regb">Registros Pendentes:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control input-sm" id="regp" name="regp" value="" disabled>
                    </div>
                </div>
                
                <div class="form-group" style="margin-bottom: 4px; <?php echo $exibircriterios; ?>">
                    <label class="control-label col-sm-5" for="economiagerada">Economia Potencial/Gerada:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control input-sm" id="economiagerada" name="economiagerada" value="" style="text-align: right;" disabled>
                    </div>
                </div>

            </form>
        </div>
        
        <div class="container">

            <div class="row">
                <div class="col-sm-2" style="text-align: right; font-size: 100%; font-weight: bold; margin-top: 5px;">Instruções para a Análise:</div>
                <div class="col-sm-10">
                    <div id="div_instrucoes" class="well well-sm" style=" background-color: white !important;">
                        <div id="div_editor" style="display: none;">
                            <textarea style="width: 100%;" cols="100" rows="15" id="editor"></textarea>
                        </div>
                        <div id="div_conteudo" style=" min-height: 50px;">

                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-2" style="text-align: right; font-size: 100%; font-weight: bold; margin-top: 5px;">Anexos:</div>
                <div class="col-sm-10">
                    <table class="table table-bordered table-striped" style="font-size: 90%;">
                        <thead>
                            <tr>
                                <th>Arquivo</th>
                                <th>Descrição</th>
                                <th>Usuário</th>
                            </tr>
                        </thead>
                        <tbody id="tb_anexos">
                            
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div id="div_mensagens_cge" class="row" <?php echo $exibir_msg_cge; ?>>
                <div class="col-sm-2" style="text-align: right; font-size: 99.7%; font-weight: bold; margin-top: 5px;">Mensagens Internas:<br><i>(Visível somente os servidores do TCE)</i></div>
                <div class="col-sm-10">
                    <div class="well well-sm" style="max-height: 200px; overflow-y: scroll; text-align: justify;">
                        <button type="button" class="btn btn-default" onclick="cadastrar_mensagem_trilha();" style="margin-bottom: 10px;">Adicionar Mensagem &nbsp;<span class="fa fa-plus fa-lg"></span></button>
                        <table class="tabla table-bordered table-striped table-condensed table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Servidor</th>
                                    <th>Mensagem</th>
                                    <th style="width: 55px;"></th>
                                </tr>
                            </thead>
                            <tbody id="tb_mensagem_trilha">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</div>
    
<div class="container">
    <?php echo $lista; ?>
</div>

<?php 

    if (file_exists("$detalhe/menu.php")) {
        include("$detalhe/menu.php");
    }elseif (file_exists("$detalhe/$detalhe"."_menu.php")){
        include("$detalhe/$detalhe"."_menu.php");
    }

?>

<div class="container-fluid">
    
    <div class="alert alert-warning">
        <strong>Inconsistências da Trilha</strong>
    </div>
    
    <div>
        <table id="tbl_trilha"  class="display" cellspacing="0" width="100%" style="font-size: 85%;">

        </table>
    </div>
        
</div>

<div class="container-fluid" style="<?php echo $exportarexcel; ?>">
    <br>
    <button class="btn" onclick="exportar_dialogo_excel();"><i class="fa fa-file-excel-o fa-lg"></i> Exportar diálogos para o Excel</button>
    <button class="btn"  onclick="lista_supervisores();"><i class="fa fa-file-pdf-o fa-lg"></i> Lista de Supervisores da Trilha</button>
    <?php
   
    
        if($vmExibeBtn){
            echo '<button class="btn"  onclick=" return registros_removidos(' .  $CodTrilha.');"><i class="fa fa-trash-o fa-lg"></i> Registros Removidos da Trilha</button>';
        }
    ?>
 
    <script>
        function exportar_dialogo_excel(){
            document.getElementById("form1").action = "reports/mensagens.php";
            document.getElementById("form1").submit();
        }  
        function lista_supervisores(){
            document.getElementById("form1").action = "reports/supervisores.php";
            document.getElementById("form1").submit();
            return false;
        }
        function registros_removidos2(CodTrilha){
            document.getElementById("form1").action = "../trilhas/registros_removidos.php";
            document.getElementById("form1").submit();
            return false;
     }

    </script>
    <br>
</div>

<br>
<br>
<?php
    
    include("form_upload/form_upload.php");
    
    if (file_exists("$detalhe/tema.php")) {
        include("$detalhe/tema.php");
    }else{
        include("$detalhe/$detalhe.php");
    }
    
    if($CodPerfil < 5){
        include("popups/mensagens_trilha.php");
    }
    
    include("popups/anexos.php");
    include("popups/consulta.php");
    include("popups/justificativas.php");
    include("popups/mensagens.php");
    //sgta 173
    include("popups/registros_excluidos.php");
    
    if($acao == "pré-análise"){
        include("popups/observacao_cge.php");
        include("popups/mesclar.php");
        include("popups/distribuicao.php");
        include("popups/auditoria.php");
    }else if($acao == "análise"){
        include("popups/inconsistencias.php");
        include("popups/alteracao.php");
    }else if($acao == "validação"){
        include("popups/observacao_cge.php");
        include("popups/validacao.php");
        include("popups/alteracao.php");
        include("popups/complementar.php");
        include("popups/redistribuicao.php");
    }
    
    // pagemaincontent recebe o conteudo do buffer
    $pagemaincontent = ob_get_contents(); 

    // Descarta o conteudo do Buffer
    ob_end_clean(); 

    //Include com o Template
    include("../../master/master.php");
    include('../../master/datatable.php');