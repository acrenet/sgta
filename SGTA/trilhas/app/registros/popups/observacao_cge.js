$(document).ready(function(){
    
    popup_observacao_cge_();
    
});

function popup_observacao_cge_(){
    $("#popup_observacao_cge").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 600,
        maxWidth: 800,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function info(CodRegistro, Agrupar, index){
    
    if (typeof Agrupar === 'undefined') { Agrupar = "0";}
    
    rowdata = minha_tabela.row(index).data();
    
    $("#1_rowindex").val(index);
    $("#1_rowdata").val(JSON.stringify(rowdata));
     
    $("#1_agrupar").val(Agrupar);
    $("#1_operacao").val("observacao_cge");
    $("#1_CodRegistro").val(CodRegistro);
    formdata = $("#form_observacao_cge").serialize();
    submit_data("server.php");
    return false;
}

function retorno_info(retorno){
    $("#1_cpf").val(retorno.registro[0]['CPF_CNPJ_Proc']);
    $("#1_nome").val(retorno.registro[0]['Nome_Descricao']);
    $("#1_obs").val(retorno.registro[0]['ObsCGE']);
    
    if($("#1_obs").val() == ""){
        $("#1_div_mensagem").hide();
    }else{
        $("#1_div_mensagem").show();
    }
    
    $('#popup_observacao_cge').dialog('option', 'title', 'Cadastrar Observação.');
    $('#popup_observacao_cge').dialog('open');
}

function observacao_cge(){
    $("#1_operacao").val("gravar_observacao_cge");
    formdata = $("#form_observacao_cge").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("As informações estão corretas?<br>A observação ficará registrada em seu nome.");
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_observacao_cge(retorno){
    $('#popup_observacao_cge').dialog('close');
    $("#popup_pergunta").dialog("close");
    $("#msg_sucesso").text("Dados gravados com sucesso.");
    $("#popup_sucesso").dialog("open");
    
    execute_cod = 2;
    execute();
}