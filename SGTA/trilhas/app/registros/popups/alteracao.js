$(document).ready(function(){
    
    popup_alteracao_();
    
});

function popup_alteracao_(){
    $("#popup_alteracao").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 640,
        maxWidth: 800,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function solicitar_alteracao_justificativa(cod){
    $("#7_CodRegistro").val(cod);
    $("#7_operacao").val("solicitar_alteracao_justificativa");
    $("#7_motivo").val("");
    formdata = $("#form_alteracao").serialize();
    submit_data("server.php");
    return false;
}

function analisar_pedido_alteracao(cod){
    $("#7_CodRegistro").val(cod);
    $("#7_operacao").val("analisar_pedido_alteracao");
    $("#7_resposta").val("");
    formdata = $("#form_alteracao").serialize();
    submit_data("server.php");
    return false;
}

function solicitar_alteracao(){
    
    pergunta_ok_acao = 1;
    
    $("#7_operacao").val("gravar_pedido_alteracao");
    $("#msg_pergunta").html("Os dados estão corretos?");
    
    formdata = $("#form_alteracao").serialize();
    
    $("#msg_pergunta").html("Os dados estão corretos?");
    $("#popup_pergunta").dialog("open");
}

function analisar_pedido(acao){
    
    pergunta_ok_acao = 1;
    
    if(acao == 1){ //aceito
        $("#7_operacao").val("aceitar_pedido_alteracao");
        $("#msg_pergunta").html("Você aceita o pedido de alteração de justificativa?");
    }else{
        $("#7_operacao").val("negar_pedido_alteracao");
        $("#msg_pergunta").html("<span style='color: red'>Você rejeita o pedido de alteração de justificativa?</span>");
    }
    
    formdata = $("#form_alteracao").serialize();
    
    $("#popup_pergunta").dialog("open");
    
}

function retorno_alteracao(retorno){
    
    if(retorno.operacao == "solicitar_alteracao_justificativa"){
        $("#7_div1").hide();
        $("#7_btn2").hide();
        $("#7_btn3").hide();
        $("#7_nome").val(retorno.nome);
        $("#popup_alteracao").dialog("open");
    }else if(retorno.operacao == "analisar_pedido_alteracao"){
        $("#7_motivo").attr("disabled", true);
        $("#7_contador").hide();
        $("#7_btn1").hide();
        $("#7_nome").val(retorno.registro[0]["SolicitadoPor"]);
        $("#7_motivo").val(retorno.registro[0]["MotivoSolicitacao"]);
        $("#7_nome2").val(retorno.nome);
        $("#popup_alteracao").dialog("open");
    }else if(retorno.operacao == "gravar_pedido_alteracao"){
        $("#popup_alteracao").dialog("close");
        $("#msg_sucesso").html(retorno.mensagem);
        $("#popup_sucesso").dialog("open");
        execute_cod = 2;
        execute();
    }else if(retorno.operacao == "aceitar_pedido_alteracao"){
        $("#popup_alteracao").dialog("close");
        $("#msg_sucesso").html(retorno.mensagem);
        $("#popup_sucesso").dialog("open");
        execute_cod = 2;
        execute();
    }else if(retorno.operacao == "negar_pedido_alteracao"){
        $("#popup_alteracao").dialog("close");
        $("#msg_sucesso").html(retorno.mensagem);
        $("#popup_sucesso").dialog("open");
        execute_cod = 2;
        execute();
    }
 
}

