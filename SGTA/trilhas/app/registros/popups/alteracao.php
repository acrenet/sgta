<script src="popups/alteracao.js" type="text/javascript"></script>

<div id="popup_alteracao" title="Pedido de Alteração de Justificativa" style="overflow: hidden;" >
    <form id="form_alteracao" name="form_alteracao" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div id="7_row1" class="row">
            <div class="col-sm-8">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="7_nome">Solicitado Por:</label>
                    <input type="text" class="form-control input-sm" id="7_nome" name="nome" disabled="">
                </div>
            </div>
        </div>

        <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
          <label for="motivo">Motivo:</label>
          <textarea name="motivo" id="7_motivo" rows="4" cols="20" class="form-control input-sm" onkeyup="contar_caracteres('7_motivo', '7_contador', 500)" maxlength="500" placeholder="(obrigatório)"></textarea>
          <input type="text" class="form-control" id="7_contador" name="contador" style="width: 50px; float: right;" disabled="">
        </div>
        
        <div id="7_div1">
            
            <hr>
            
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                        <label for="7_nome2">Analisado Por:</label>
                        <input type="text" class="form-control input-sm" id="7_nome2" name="nome2" disabled="">
                    </div>
                </div>
            </div>

            <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
              <label for="7_resposta">Resposta:</label>
              <textarea name="resposta" id="7_resposta" rows="4" cols="20" class="form-control input-sm" onkeyup="contar_caracteres('7_resposta', '7_contador2', 500)" maxlength="500" placeholder="(obrigatório)"></textarea>
              <input type="text" class="form-control" id="7_contador2" name="contador2" style="width: 50px; float: right;" disabled="">
            </div>
        </div>

        <input type="hidden" name="operacao" id="7_operacao" value="" />
        <input type="hidden" name="CodRegistro" id="7_CodRegistro" value="" />
        
    </form>
    
    <hr>
    
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button id="7_btn0" type="button" class="btn btn-info" onclick='$("#popup_alteracao").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
        <button id="7_btn1" type="button" class="btn btn-success" onclick='solicitar_alteracao();'>Solicitar &nbsp;<span class="fa fa-check-square-o"></span></button>
        <button id="7_btn2" type="button" class="btn btn-warning" onclick='analisar_pedido(0)'>Recusar &nbsp;<span class="fa fa-times-circle"></span></button>
        <button id="7_btn3" type="button" class="btn btn-success" onclick='analisar_pedido(1);'>Aceitar &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
</div>