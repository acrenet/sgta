$(document).ready(function(){
    
    popup_auditoria_();
    
});

function popup_auditoria_(){
    $("#popup_auditoria").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 600,
        maxWidth: 800,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function auditoria_consulta(CodRegistro){
    $("#12_CodRegistro").val(CodRegistro);
    $("#12_operacao").val("auditoria_consulta");
    formdata = $("#form_auditoria").serialize();
    submit_data("server.php");
    return false;
}

function retorno_auditoria_consulta(retorno){
    $("#12_cpf").val(retorno.cpf);
    $("#12_nome").val(retorno.nome);
    $("#12_obs").val("");
    $('#popup_auditoria').dialog('open');
}

function auditoria_pre_analise(){
    $("#12_operacao").val("auditoria_pre_analise");
    formdata = $("#form_auditoria").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("Você está certo do pedido de abertura de inspeção/auditoria?<br><span style='color: red;'>A operação não poderá ser desfeita.</span>");
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_auditoria_pre_analise(retorno){
    $('#popup_auditoria').dialog('close');
    $("#popup_pergunta").dialog("close");
    $("#msg_sucesso").text("Dados gravados com sucesso.");
    $("#popup_sucesso").dialog("open");
    
    execute_cod = 2;
    execute();
}