<script src="../../js/datepicker.js" type="text/javascript"></script>
<script src="popups/validacao.js" type="text/javascript"></script>

<div id="popup_validacao" title="Analisar Justificativa." style="overflow-y: scroll; margin-bottom: 70px; width:100%; height:100%;" >
    <div class="well well-sm" style="font-size: 70%; text-align: justify;">
        <div class="row">
            <div class="col-xs-3" style="text-align: right;"><b>CPF/CNPJ/Processo:</b></div>
            <div class="col-xs-9" id="4_cpf"></div>
        </div>
        <div class="row">
            <div class="col-xs-3" style="text-align: right;"><b>Nome/Descrição:</b></div>
            <div class="col-xs-9" id="4_nome"></div>
        </div>
        <div class="row">
            <div class="col-xs-3" style="text-align: right;"><b>Justificado Por:</b></div>
            <div class="col-xs-9" id="4_justificado_por"></div>
        </div>
        <div class="row">
            <div class="col-xs-3" style="text-align: right;"><b>Data Justificativa:</b></div>
            <div class="col-xs-9" id="4_data_analise"></div>
        </div>
    
        <div class="row">
            <div class="col-xs-3" style="text-align: right;"><b>Parecer:</b></div>
            <div class="col-xs-9" id="4_conclusao"><span style="color: red;"></span></div>
        </div>
    
        <div class="row">
            <div class="col-xs-3" style="text-align: right;"><b>Valor a Devolver:</b></div>
            <div class="col-xs-9" id="4_valor_a_devolver">0,00</div>
        </div>
        <div class="row">
            <div class="col-xs-3" style="text-align: right;"><b>Justificativa:</b></div>
            <div class="col-xs-9" style="max-height: 600px; overscroll-behavior-y: auto; width: 100%;" id="4_justificativa">
                
            </div>
        </div>
    </div>
    
    <form id="form_validacao" class="form-horizontal" style="font-size: 80%; margin-left: -372px;">
        <div class="form-group" style="margin-bottom: 5px;">
            <label class="control-label col-sm-3" for="4_parecer">Parecer:</label>
            <div class="col-sm-9"> 
                <textarea class="form-control input-sm" rows="13" id="4_parecer" name="parecer"  onkeyup="contar_caracteres('4_parecer', '4_contador', 2000)" maxlength="2000" placeholder="(Preenchimento Obrigatório)"></textarea>
                <input type="text" class="form-control" id="4_contador" name="contador" style="width: 60px; float: right;" disabled="">
                <button type="button" class="btn btn-link" onclick="return anexar_arquivo2();" style="padding-left: 0px;">Anexar Arquivo</button>
                <button type="button" class="btn btn-link" onclick="return ver_arquivos_anexados2();">Visualizar Anexos</button>
                <button type="button" class="btn btn-link" onclick="return ver_detalhes2();">Ver Detalhes</button>
                <button type="button" class="btn btn-link" onclick="return consulta_justificativas($('#4_CodRegistro').val());">Justificativas/Pareceres</button>
                
            </div>
        </div>
        <div class="form-group" style="margin-bottom: 5px;">
            <label class="control-label col-sm-3" for="pwd">Ação do TCE:</label>
            <div class="col-sm-9"> 
                <select id="4_resultadofinal" name="resultadofinal" class="form-control input-sm" onchange="verifica_quarentena();" style="font-family: 'FontAwesome', Helvetica;">
                    <option value="0"></option>
                    <option value="1" style="color: darkgreen; font-family: 'FontAwesome', Helvetica;">&#xf187; Arquivar</option>
                    <option value="2" style="color: darkorange; font-family: 'FontAwesome', Helvetica;">&#xf017; Colocar Em Monitoramento</option>
                    <!--<option value="3" style="color: darkblue; font-family: 'FontAwesome', Helvetica;">&#xf07c; Recomendar Inspeção/Auditoria</option>-->
                    <option value="4" style="color: red; font-family: 'FontAwesome', Helvetica;">&#xf112; Devolver ao Órgão para Nova Justificativa</option>
                </select>
            </div>
        </div>
        
        <div class="form-group" style="margin-bottom: 5px;">
            <label class="control-label col-sm-3" for="4_favorecido">Favorecido:</label>
            <div class="col-sm-9">
              
    <select name="favorecido" id="4_favorecido" class="form-control input-sm">
         <option>Estado de Santa Catarina </option>
         <option>Empresa Pública</option>
         <option>Sociedade de Economia Mista</option>
         <option>União</option>
         <option>Abdon Batista</option>
          <option>Abelardo Luz</option>
          <option>Agrolândia</option>
          <option>Agronômica</option>
          <option>Água Doce</option>
          <option>Águas de Chapecó</option>
          <option>Águas Frias</option>
          <option>Águas Mornas</option>
          <option>Alfredo Wagner</option>
          <option>Alto Bela Vista</option>
          <option>Anchieta</option>
          <option>Angelina</option>
          <option>Anita Garibaldi</option>
          <option>Anitápolis</option>
          <option>Antônio Carlos</option>
          <option>Apiúna</option>
          <option>Arabutã</option>
          <option>Araquari</option>
          <option>Araranguá</option>
          <option>Armazém</option>
          <option>Arroio Trinta</option>
          <option>Arvoredo</option>
          <option>Ascurra</option>
          <option>Atalanta</option>
          <option>Aurora</option>
          <option>Balneário Arroio do Silva</option>
          <option>Balneário Barra do Sul</option>
          <option>Balneário Camboriú</option>
          <option>Balneário Gaivota</option>
          <option>Balneário Piçarras</option>
          <option>Balneário Rincão</option>
          <option>Bandeirante</option>
          <option>Barra Bonita</option>
          <option>Barra Velha</option>
          <option>Bela Vista do Toldo</option>
          <option>Belmonte</option>
          <option>Benedito Novo</option>
          <option>Biguaçu</option>
          <option>Blumenau</option>
          <option>Bocaina do Sul</option>
          <option>Bom Jardim da Serra</option>
          <option>Bom Jesus</option>
          <option>Bom Jesus do Oeste</option>
          <option>Bom Retiro</option>
         <option>Bombinhas</option>
          <option>Botuverá</option>
          <option>Braço do Norte</option>
          <option>Braço do Trombudo</option>
          <option>Brunópolis</option>
          <option>Brusque</option>
          <option>Caçador</option>
          <option>Caibi</option>
          <option>Calmon</option>
          <option>Camboriú</option>
          <option>Campo Alegre</option>
          <option>Campo Belo do Sul</option>
          <option>Campo Erê</option>
          <option>Campos Novos</option>
          <option>Canelinha</option>
          <option>Canoinhas</option>
          <option>Capão Alto</option>
          <option>Capinzal</option>
          <option>Capivari de Baixo</option>
          <option>Catanduvas</option>
          <option>Caxambu do Sul</option>
          <option>Celso Ramos</option>
          <option>Cerro Negro</option>
          <option>Chapadão do Lageado</option>
          <option>Chapecó</option>
          <option>Cocal do Sul</option>
          <option>Concórdia</option>
          <option>Cordilheira Alta</option>
          <option>Coronel Freitas</option>
          <option>Coronel Martins</option>
          <option>Correia Pinto</option>
          <option>Corupá</option>
          <option>Criciúma</option>
          <option>Cunha Porã</option>
          <option>Cunhataí</option>
          <option>Curitibanos</option>
          <option>Descanso</option>
          <option>Dionísio Cerqueira</option>
          <option>Dona Emma</option>
          <option>Doutor Pedrinho</option>
          <option>Entre Rios</option>
          <option>Ermo</option>
          <option>Erval Velho</option>
          <option>Faxinal dos Guedes</option>
          <option>Flor do Sertão</option>
          <option>Florianópolis</option>
          <option>Formosa do Sul</option>
          <option>Forquilhinha</option>
          <option>Fraiburgo</option>
          <option>Frei Rogério</option>
          <option>Galvão</option>
          <option>Garopaba</option>
          <option>Garuva</option>
          <option>Gaspar</option>
          <option>Governador Celso Ramos</option>
          <option>Grão Pará</option>
          <option>Gravatal</option>
          <option>Guabiruba</option>
          <option>Guaraciaba</option>
          <option>Guaramirim</option>
          <option>Guarujá do Sul</option>
          <option>Guatambu</option>
          <option>Herval dOeste</option>
          <option>Ibiam</option>
          <option>Ibicaré</option>
          <option>Ibirama</option>
          <option>Içara</option>
          <option>Ilhota</option>
          <option>Imaruí</option>
          <option>Imbituba</option>
          <option>Imbuia</option>
          <option>Indaial</option>
          <option>Iomerê</option>
          <option>Ipira</option>
          <option>Iporã do Oeste</option>
          <option>Ipuaçu</option>
          <option>Ipumirim</option>
          <option>Iraceminha</option>
          <option>Irani</option>
          <option>Irati</option>
          <option>Irineópolis</option>
          <option>Itá</option>
          <option>Itaiópolis</option>
          <option>Itajaí</option>
          <option>Itapema</option>
          <option>Itapiranga</option>
          <option>Itapoá</option>
          <option>Ituporanga</option>
          <option>Jaborá</option>
          <option>Jacinto Machado</option>
          <option>Jaguaruna</option>
          <option>Jaraguá do Sul</option>
          <option>Jardinópolis</option>
          <option>Joaçaba</option>
          <option>Joinville</option>
          <option>José Boiteux</option>
          <option>Jupiá</option>
          <option>Lacerdópolis</option>
          <option>Lages</option>
          <option>Laguna</option>
          <option>Lajeado Grande</option>
          <option>Laurentino</option>
          <option>Lauro Müller</option>
          <option>Lebon Régis</option>
          <option>Leoberto Leal</option>
          <option>Lindóia do Sul</option>
          <option>Lontras</option>
          <option>Luiz Alves</option>
          <option>Luzerna</option>
          <option>Macieira</option>
          <option>Mafra</option>
          <option>Major Gercino</option>
          <option>Major Vieira</option>
          <option>Maracajá</option>
          <option>Maravilha</option>
          <option>Marema</option>
          <option>Massaranduba</option>
          <option>Matos Costa</option>
          <option>Meleiro</option>
          <option>Mirim Doce</option>
          <option>Modelo</option>
          <option>Mondaí</option>
          <option>Monte Carlo</option>
          <option>Monte Castelo</option>
          <option>Morro da Fumaça</option>
          <option>Morro Grande</option>
          <option>Navegantes</option>
          <option>Nova Erechim</option>
          <option>Nova Itaberaba</option>
          <option>Nova Trento</option>
          <option>Nova Veneza</option>
          <option>Novo Horizonte</option>
          <option>Orleans</option>
          <option>Otacílio Costa</option>
          <option>Ouro</option>
          <option>Ouro Verde</option>
          <option>Paial</option>
          <option>Painel</option>
          <option>Palhoça</option>
          <option>Palma Sola</option>
          <option>Palmeira</option>
          <option>Palmitos</option>
          <option>Papanduva</option>
          <option>Paraíso</option>
          <option>Passo de Torres</option>
          <option>Passos Maia</option>
          <option>Paulo Lopes</option>
          <option>Pedras Grandes</option>
          <option>Penha</option>
          <option>Peritiba</option>
          <option>Pescaria Brava</option>
          <option>Petrolândia</option>
          <option>Pinhalzinho</option>
          <option>Pinheiro Preto</option>
          <option>Piratuba</option>
          <option>Planalto Alegre</option>
          <option>Pomerode</option>
          <option>Ponte Alta</option>
          <option>Ponte Alta do Norte</option>
          <option>Ponte Serrada</option>
          <option>Porto Belo</option>
          <option>Porto União</option>
          <option>Pouso Redondo</option>
          <option>Praia Grande</option>
          <option>Presidente Castello Branco</option>
          <option>Presidente Getúlio</option>
          <option>Presidente Nereu</option>
          <option>Princesa</option>
          <option>Quilombo</option>
          <option>Rancho Queimado</option>
          <option>Rio das Antas</option>
          <option>Rio do Campo</option>
          <option>Rio do Oeste</option>
          <option>Rio do Sul</option>
          <option>Rio dos Cedros</option>
          <option>Rio Fortuna</option>
          <option>Rio Negrinho</option>
          <option>Rio Rufino</option>
          <option>Riqueza</option>
          <option>Rodeio</option>
          <option>Romelândia</option>
          <option>Salete</option>
          <option>Saltinho</option>
          <option>Salto Veloso</option>
          <option>Sangão</option>
          <option>Santa Cecília</option>
          <option>Santa Helena</option>
          <option>Santa Rosa de Lima</option>
          <option>Santa Rosa do Sul</option>
          <option>Santa Terezinha</option>
          <option>Santa Terezinha do Progresso</option>
          <option>Santiago do Sul</option>
          <option>Santo Amaro da Imperatriz</option>
          <option>São Bento do Sul</option>
          <option>São Bernardino</option>
          <option>São Bonifácio</option>
          <option>São Carlos</option>
          <option>São Cristóvão do Sul</option>
          <option>São Domingos</option>
          <option>São Francisco do Sul</option>
          <option>São João Batista</option>
          <option>São João do Itaperiú</option>
          <option>São João do Oeste</option>
          <option>São João do Sul</option>
          <option>São Joaquim</option>
          <option>São José</option>
          <option>São José do Cedro</option>
          <option>São José do Cerrito</option>
          <option>São Lourenço do Oeste</option>
          <option>São Ludgero</option>
          <option>São Martinho</option>
          <option>São Miguel da Boa Vista</option>
          <option>São Miguel do Oeste</option>
          <option>São Pedro de Alcântara</option>
          <option>Saudades</option>
          <option>Schroeder</option>
          <option>Seara</option>
          <option>Serra Alta</option>
          <option>Siderópolis</option>
          <option>Sombrio</option>
          <option>Sul Brasil</option>
          <option>Taió</option>
          <option>Tangará</option>
          <option>Tigrinhos</option>
          <option>Tijucas</option>
          <option>Timbé do Sul</option>
          <option>Timbó</option>
          <option>Timbó Grande</option>
          <option>Três Barras</option>
          <option>Treviso</option>
          <option>Treze de Maio</option>
          <option>Treze Tílias</option>
          <option>Trombudo Central</option>
          <option>Tubarão</option>
          <option>Tunápolis</option>
          <option>Turvo</option>
          <option>União do Oeste</option>
          <option>Urubici</option>
          <option>Urupema</option>
          <option>Urussanga</option>
          <option>Vargeão</option>
          <option>Vargem</option>
          <option>Vargem Bonita</option>
          <option>Vidal Ramos</option>
          <option>Videira</option>
          <option>Vitor Meireles</option>
          <option>Witmarsum</option>
          <option>Xanxerê</option>
          <option>Xavantina</option>
          <option>Xaxim</option>
          <option>Zortéa</option>
    </select>

            </div>
        </div>
        
        <div id="4_divquarentena" class="form-group" style="margin-bottom: 5px; display: none; margin-left: 95px;">
          <label class="control-label col-sm-3" for="pwd">Em Monitoramento Até:</label>
          <div class="col-sm-3"> 
              <input type="text" class="form-control input-sm" id="4_dtquarentena" name="dtquarentena" placeholder="" style="width: 110px; display: inline;">
          </div>
        </div>
        <div id="4_divnumprocauditoria" class="form-group" style="margin-bottom: 5px; display: none;">
          <label class="control-label col-sm-3" for="pwd">Número do Processo:</label>
          <div class="col-sm-6"> 
              <input type="text" class="form-control input-sm" id="4_numprocauditoria" name="numprocauditoria" placeholder="" style="width: 200px; display: inline;" maxlength="18">
          </div>
        </div>
        
        <div id="4_detalheresultado" class="panel panel-default" style="display: none; margin-left: 382px;">
            <div class="panel-heading" style="font-size: 110%; font-weight: bold;">Resultado da Auditoria e Recomendações do TCE &nbsp;&nbsp;<a class="consultar" title="Instruções" onclick="$('#4_orientacao').slideToggle(); return false;"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></a></div>
            <div class="panel-body">
                <div id="4_orientacao" class="well well-sm" style="text-align: justify; display: none;">
                    <p>
                        <b>Destina-se ao acompanhamento do resultado da Trilha de Auditoria.</b><br>
                        Os itens que forem marcados como "Solicitado" tem interpretação diferente pelo sistema de acordo com o resultado da 
                        análise e influenciam nos cálculos estatísticos e nos relatórios que o sistema emite:<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1- Se o registro estiver em monitoramento: O sistema entende que o órgão acatou a recomendação porém ainda não a implementou.<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2- Se o registro foi arquivado ou foi recomendado inspeção ou auditoria: O sistema entende que o órgão deixou de 
                        cumprir ou se recusou a atender a recomendação.<br>
                    </p>
                    <p>
                        <b>Criação/Melhoria de Leis, Decretos, etc:</b> Marcar "Sim" caso tenha sido feito a recomendação de expedição de 
                        algum instrumento legal (leis, decretos, etc.) e esta foi efetivada pelo órgão/entidade, marcar "Solicitado" caso a 
                        recomendação tenha sido aceita mas ainda não implementada, o registro deverá ficar em monitoramento até a implementação. 
                        Marcar "Solicitado" caso a recomendação não tenha sido atendida, o registro deverá ser arquivado ou deverá ser 
                        solicitado a abertura de processo de inspeção ou auditoria.
                    </p>
                    <p>
                        <b>Criação/Melhorias em Sistemas:</b> Marcar "Sim" caso tenha sido feito a recomendação de criação ou 
                        reformulação de algum sistema de gerenciamento de dados e esta foi implementada pelo órgão. Marcar "Solicitado" caso 
                        a recomendação tenha sido aceita mas ainda não implementada, o registro deverá ficar em monitoramento até a implementação. 
                        Marcar "Solicitado" caso a recomendação não tenha sido atendida, o registro deverá ser arquivado ou deverá ser 
                        solicitado a abertura de processo de inspeção ou auditoria.
                    </p>
                    <p>
                        <b>Capacitação Técnica dos Agentes:</b> Marcar "Sim" caso tenha sido feito a recomendação de capacitação ou 
                        orientação de agentes/servidores envolvidos no processo e esta foi implementada pelo órgão. Marcar "Solicitado" 
                        se a recomendação foi aceita mas ainda não implementada, o registro deverá ficar em monitoramento até a implementação. 
                        Marcar "Solicitado" caso a recomendação não tenha sido atendida, o registro deverá ser arquivado ou deverá ser solicitado a 
                        abertura de processo de inspeção ou auditoria.
                    </p>
                    <p>
                        <b>Devolução de Numerário:</b> Marcar "Sim" caso tenha sido solicitado a devolução de numerário e foi efetivada pelo órgão. 
                        Marcar "Solicitado" se a recomendação será efetivada ou se estiver ocorrendo de forma parcelada, o registro 
                        deverá ficar em monitoramento até a quitação das parcelas. Marcar "Solicitado" caso a recomendação não tenha sido 
                        atendida, o registro deverá ser arquivado ou deverá ser solicitado a abertura de processo de inspeção ou auditoria.
                    </p>
                    <p>
                        <b>Economia Potencial/Gerada:</b> Preencher o valor que foi ressarcido ao Estado, caso haja apenas a promessa de devolução
                        o registro deverá permanecer em monitoramento até a efetivação da devolução.
                    </p>
                    <p>
                        <b>Economia Mensal:</b> Preencher o valor quando se puder determinar a economia mensal gerada para o Estado em
                        virtude do trabalho de auditoria.
                    </p>
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label col-sm-5" for="4_intrumentoslegais0" style="padding-top: 16px !important;">Criação/Melhoria de Leis, Decretos, etc:</label>
                    <div class="col-sm-7">
                        <div class="radio-inline" style="padding-left: 0px !important;"> 
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="instrumentoslegais[]" id="4_intrumentoslegais0" value="0" checked=""/> Não </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="instrumentoslegais[]" id="4_intrumentoslegais1" value="1" /> Sim </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="instrumentoslegais[]" id="4_intrumentoslegais2" value="2" /> Solicitado</label>
                            <input type="text" name="instrumentoslegaisdesc" id="4_instrumentoslegaisdesc" class="form-control input-sm" placeholder="Se positivo discrimine." style="width: 220px; display: inline; margin-left: 10px;" maxlength="100"/>
                        </div>
                    </div>    
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label col-sm-5" for="4_sistemasgerenciais0" style="padding-top: 16px !important;">Criação/Melhorias em Sistemas:</label>
                    <div class="col-sm-7">
                        <div class="radio-inline" style="padding-left: 0px !important;"> 
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="sistemasgerenciais[]" id="4_sistemasgerenciais0" value="0" checked=""/> Não </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="sistemasgerenciais[]" id="4_sistemasgerenciais1" value="1" /> Sim </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="sistemasgerenciais[]" id="4_sistemasgerenciais2" value="2" /> Solicitado </label>
                            <input type="text" name="sistemasgerenciaisdesc" id="4_sistemasgerenciaisdesc" class="form-control input-sm" placeholder="Se positivo discrimine." style="width: 220px; display: inline; margin-left: 10px;" maxlength="100"/>
                        </div>
                    </div>    
                </div>
                <div class="form-group" style="margin-bottom: 8px;">
                    <label class="control-label col-sm-5" for="4_capacitacao0" style="padding-top: 16px !important;">Capacitação Técnica dos Agentes:</label>
                    <div class="col-sm-7">
                        <div class="radio-inline" style="padding-left: 0px !important;"> 
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="capacitacao[]" id="4_capacitacao0" value="0" checked=""/> Não </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="capacitacao[]" id="4_capacitacao1" value="1" /> Sim </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="capacitacao[]" id="4_capacitacao2" value="2" /> Solicitado </label>
                            <input type="text" name="capacitacaodesc" id="4_capacitacaodesc" class="form-control input-sm" placeholder="Se positivo discrimine." style="width: 220px; display: inline; margin-left: 10px;" maxlength="100"/>
                        </div>
                    </div>    
                </div>
                <div class="form-group" style="margin-bottom: 11px;">
                    <label class="control-label col-sm-5" for="4_devolucao0">Devolução de Numerário:</label>
                    <div class="col-sm-7">
                        <div class="radio-inline" style="padding-left: 0px !important;"> 
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="devolucao[]" id="4_devolucao0" value="0" checked=""/> Não </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="devolucao[]" id="4_devolucao1" value="1" /> Sim </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="devolucao[]" id="4_devolucao2" value="2" /> Solicitado </label>
                        </div>
                    </div>    
                </div>
                <div class="form-group" style="margin-bottom: 3px;">
                    <label class="control-label col-sm-5" for="pwd">Economia Potencial/Gerada:</label>
                    <div class="col-sm-2"> 
                        <input type="text" class="form-control input-sm" name="valorressarcido" id="4_valorressarcido" value="" />
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label col-sm-5" for="pwd">Economia Mensal:</label>
                    <div class="col-sm-2"> 
                        <input type="text" class="form-control input-sm" name="economiamensal" id="4_economiamensal" value="" />
                    </div>
                </div> 
            </div>
        </div>

            
        <div class="form-group" style="margin-bottom: 5px; display: none;">
          <label class="control-label col-sm-3" for="email">Analisado Por:</label>
          <div class="col-sm-9">
              <input type="text" class="form-control input-sm" id="4_analisadopor" name="analisadopor" placeholder="" disabled="">
          </div>
        </div>
        
        <div id="4_divregsel" class="well well-sm" style="margin-top: 10px; font-size: 90%; max-height: 200px; overflow-y: scroll; display: none;">
            <p style="font-weight: bold; margin-bottom: 5px; line-height: 130%">Abaixo são listados os registros desta trilha que estão sob sua responsabilidade.
                Se for o caso, no momento da conclusão, selecione os registros que deseja replicar esta ação.</p>
            <div id="4_div_reservados">
                
            </div>
        </div>
            
        <input type="hidden" name="operacao" id="4_operacao" value="" />
        <input type="hidden" name="comando" id="4_comando" value="" />
        <input type="hidden" name="CodRegistro" id="4_CodRegistro" value="" />
        <input type="hidden" name="CodTrilha" id="4_CodTrilha" value="<?php echo $CodTrilha; ?>" />
        <input type="hidden" name="acao" id="4_acao" value="<?php echo $acao; ?>" />
        <input type="hidden" name="tipo" id="4_tipo" value="" />
       
    </form>
    <hr>
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-info" id="3_btn_sair" onclick='$("#popup_validacao").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
        <button type="button" class="btn btn-primary" id="3_btn_ok" onclick='salvar_validacao("salvar");' title="Salvar para continuar com a análise posteriormente.">Salvar &nbsp;<span class="fa fa-save"></span></button>
        <button type="button" class="btn btn-success" id="3_btn_concluir" onclick='salvar_validacao("concluir");' title="Conclúi a análise conforme ação selecionada.">Concluir a Análise &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
</div>