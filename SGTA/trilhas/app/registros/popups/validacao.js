$(document).ready(function(){
    
    popup_validacao_();
    
});

function popup_validacao_(){
    $("#popup_validacao").dialog({
        autoOpen: false,
        resizable: true,
        modal: true,
        width: 1600,
        height : 829,
       
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
    
    $( "#4_dtquarentena" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
}

function validar_justificativa(cod, cpf, index){
    
    $("#4_operacao").val("analisar_justificativa");
    $("#4_CodRegistro").val(cod);
    
    pergunta_ok_acao = 1;
    
    if(cpf == "0"){
        $("#4_tipo").val("sem reserva");
        formdata = $("#form_validacao").serialize();
        $("#msg_pergunta").html("O registro ficará reservado em seu nome, deseja continuar?");
        $("#popup_pergunta").dialog("open");
    }else if($("#CPF").val() != cpf) {
        $("#4_tipo").val("reservado por outro");
        formdata = $("#form_validacao").serialize();
        $("#msg_pergunta").html("Este registro encontra-se reservado.<br>Deseja transferir a reserva para o seu nome?");
        $("#popup_pergunta").dialog("open");
    }else{
        $("#4_tipo").val("reserva propria");
        formdata = $("#form_validacao").serialize();
        pergunta_ok();
    }
    
    return false;
}

function analisar_justificativa_retorno(retorno){

    //SGTA -155 - garantindo que a div de com os registros vao ser sempre esvaziadas.
    $("#4_div_reservados").html("");

    $("#popup_pergunta").dialog("close");
    if(retorno.mensagem != ""){
        $("#msg_alerta").html(retorno.mensagem);
        $("#popup_alerta").dialog("open");
        execute_cod = 2;
        execute();
        return false;
    }
    var parecer = "?";
    switch(retorno.registro[0]["CodParecer"]){
        case "1":
            parecer = "Atendido Total, Falha Formal";
            break;
        case "2":
            parecer = "Atendido Total com Valor a Devolver";
            break;
        case "3":
            parecer = "Atendido Parcial, Falha Formal";
            break;
        case "4":
            parecer = "Atendido Parcial com Valor a Devolver";
            break;
        case "5":
            parecer = "<span style='color: red;'>Não Concordo</span>";
            break;
        case "6":
            parecer = "Não Analisado";
            break;
            
    }
    
    $("#4_cpf").text(retorno.registro[0]["CPF_CNPJ_Proc"]);
    $("#4_nome").text(retorno.registro[0]["Nome_Descricao"]);
    $("#4_justificado_por").text(retorno.registro[0]["UsuarioOrgao"]);
    $("#4_data_analise").text(retorno.registro[0]["DataConclusaoJustificativa"]);
    $("#4_conclusao").html(parecer);
    $("#4_valor_a_devolver").text(retorno.registro[0]["ValorADevolver"]);
    $("#4_justificativa").html(retorno.registro[0]["JustificativaOrgao"]);
    $("#4_parecer").val(retorno.registro[0]["JustificativaCGE"]);
    $("#4_dtquarentena").val(retorno.registro[0]["QuarentenaAte"]);
    $("#4_analisadopor").val(retorno.registro[0]["ReservadoPor"]);
    $("#4_resultadofinal").val(retorno.registro[0]["ResultadoFinal"]);
    $("#4_numprocauditoria").val(retorno.registro[0]["NumProcAuditoria"]);
    
    $("#4_instrumentoslegaisdesc").val(retorno.registro[0]["InstrumentosLegaisDesc"]);
    $("#4_sistemasgerenciaisdesc").val(retorno.registro[0]["SistemasGerenciaisDesc"]);
    $("#4_capacitacaodesc").val(retorno.registro[0]["CapacitacaoDesc"]);
    
    if(retorno.registro[0]["Favorecido"] == null){
        $("#4_favorecido").val($("#4_favorecido option:first").val());
    }else{
        $("#4_favorecido").val(retorno.registro[0]["Favorecido"]);
    }
        
    var resultado = retorno.registro[0]["ResultadoAuditoria"];
    resultado = resultado.split(";");
    
    var expedicao = resultado[0];
    var sistemas = resultado[1];
    var capacitacao = resultado[2];
    var devolucao = resultado[3];
    
    expedicao = expedicao.split("=");
    sistemas = sistemas.split("=");
    capacitacao = capacitacao.split("=");
    devolucao = devolucao.split("=");
    
    switch (expedicao[1]){ 
        case "0":
            $("#4_intrumentoslegais0").prop("checked", true);
            break;
        case "1":
            $("#4_intrumentoslegais1").prop("checked", true);
            break;
        case "2":
            $("#4_intrumentoslegais2").prop("checked", true);
            break;
    }
    
    switch (sistemas[1]){ 
        case "0":
            $("#4_sistemasgerenciais0").prop("checked", true);
            break;
        case "1":
            $("#4_sistemasgerenciais1").prop("checked", true);
            break;
        case "2":
            $("#4_sistemasgerenciais2").prop("checked", true);
            break;
    }
    
    switch (capacitacao[1]){ 
        case "0":
            $("#4_capacitacao0").prop("checked", true);
            break;
        case "1":
            $("#4_capacitacao1").prop("checked", true);
            break;
        case "2":
            $("#4_capacitacao2").prop("checked", true);
            break;
    }
    
    switch (devolucao[1]){ 
        case "0":
            $("#4_devolucao0").prop("checked", true);
            break;
        case "1":
            $("#4_devolucao1").prop("checked", true);
            break;
        case "2":
            $("#4_devolucao2").prop("checked", true);
            break;
    }
    
    $("#4_valorressarcido").val(retorno.registro[0]["ValorRessarcido"]);
    $("#4_economiamensal").val(retorno.registro[0]["EconomiaMensal"]);
    
    if(retorno.regs == ""){
        $("#4_divregsel").hide();
    }else{
        $("#4_divregsel").show();
        $("#4_div_reservados").html(retorno.regs);
    }
    
    verifica_quarentena();
    
    $("#popup_validacao").dialog("open");
    
    if(retorno.recarregar == true){
        execute_cod = 2;
        execute();
    }
    
    return false;
}

function anexar_arquivo2(){
    anexar_arquivo($("#4_CodRegistro").val(),'registro');
    return false;
}

function ver_arquivos_anexados2(){
    visualizar_anexos($("#4_CodRegistro").val());
    return false;
}

function ver_detalhes2(){
    detalhes($("#4_CodRegistro").val());
    return false;
}

function salvar_validacao(acao){
    
    pergunta_ok_acao = 1;
    
    $("#4_operacao").val("salvar_validacao");
    var resultado = $("#4_resultadofinal").val();
    
    if(acao == "salvar"){
        $("#4_comando").val("salvar_justificativa");
        $("#msg_pergunta").html("As informações estão corretas?");
    }else if(acao == "concluir"){
        if(resultado == 0){
            $("#msg_alerta").html("Selecione uma ação na lista.");
            $("#popup_alerta").dialog("open");
            return false;
        }else if(resultado == 1){
            $("#4_comando").val("aceitar_justificativa");
            $("#msg_pergunta").html("<span style='color:darkgreen;'><b>Você têm certeza que gostaria de aceitar a justificativa e concluir o processo?</b></span>");
        }else if(resultado == 2){
            $("#4_comando").val("quarentena");
            $("#msg_pergunta").html("<span style='color:darkorange;'><b>Você têm certeza que gostaria de aceitar a justificativa e colocar o registro em monitoramento?</b></span>");
        }else if(resultado == 3){
            $("#4_comando").val("abrir_auditoria");
            $("#msg_pergunta").html("<span style='color:darkblue;'><b>Você está certo que deseja recomendar abertura de processo de auditoria/inspeção?</b></span>");
        }else if(resultado == 4){
            $("#4_comando").val("recusar_justificativa");
            $("#msg_pergunta").html("<span style='color:red;'><b>Você têm certeza que gostaria de recusar a justificativa e devolver a inconsistência ao órgão?</b></span>");
        }
    }
    
    formdata = $("#form_validacao").serialize();
    
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_salvar_validacao(retorno){
    execute_cod = 2;
    execute();
    $("#popup_validacao").dialog("close");
    $("#msg_sucesso").text(retorno.mensagem);
    $("#popup_sucesso").dialog("open");
}

function verifica_quarentena(){
    if($("#4_resultadofinal").val() == 2){ //monitoramento
        $("#4_divquarentena").slideDown();
        $("#4_detalheresultado").slideDown();
        $("#4_divnumprocauditoria").slideUp();
        $("#4_numprocauditoria").val("");
    }else if($("#4_resultadofinal").val() ==3 ){ //auditoria
        $("#4_divquarentena").slideUp();
        $("#4_detalheresultado").slideDown();
        //$("#4_divnumprocauditoria").slideDown();
        $("#4_dtquarentena").val("");
    }else if($("#4_resultadofinal").val() == 1 ){ //arquivar
        $("#4_divquarentena").slideUp();
        $("#4_divnumprocauditoria").slideUp();
        $("#4_detalheresultado").slideDown();
        $("#4_dtquarentena").val("");
        $("#4_numprocauditoria").val("");    
    }else{
        $("#4_divquarentena").slideUp();
        $("#4_divnumprocauditoria").slideUp();
        $("#4_detalheresultado").slideUp();
        $("#4_dtquarentena").val("");
        $("#4_numprocauditoria").val("");
    }
}