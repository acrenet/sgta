<script src="popups/inconsistencias.js" type="text/javascript"></script>

<div id="popup_inconsistencia" title="Justificar Inconsistência." style="overflow: hidden;" >
    <form id="form_inconsistencia" name="form_inconsistencia" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="3_cpf">CPF/CNPJ/Processo:</label>
                    <input id="3_cpf" name="cpf" type="text" class="form-control input-sm" disabled="">
                </div>
            </div>
            <div class="col-sm-9">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="3_nome">Nome/Descrição:</label>
                    <input type="text" class="form-control input-sm" id="3_nome" name="nome" disabled="">
                </div>
            </div>
        </div>
        
        <div class="row" style="padding: 5px; margin-bottom: 0px;">
            <label class="control-label col-sm-3" for="3_parecer" style="text-align: left; padding-left: 0px;">Parecer:</label>
            <div class="col-sm-9">
                <select name="parecer" id="3_parecer" class="form-control input-sm" style="margin-left: -12px; width: 104.8%;" onchange="zera_valor();">
                    <option value="1">Inconsistência Confirmada, Falha Formal</option>
                    <option value="2">Inconsistência Confirmada com Valor a Devolver</option>
                    <option value="3">Inconsistência Parcialmente Confirmada, Falha Formal</option>
                    <option value="4">Inconsistência Parcialmente Confirmada com Valor a Devolver</option>
                    <option value="5">Não Concordo</option>
                    <option value="6"></option>
                </select>
            </div>
        </div>
        
        <div class="row" style="padding: 5px; margin-bottom: 0px;">
            <label class="control-label col-sm-3" for="3_valor_a_devolver" style="text-align: left; padding-left: 0px;">Valor a Devolver:</label>
            <div class="col-sm-3">
              <input type="text" name="valor_a_devolver" id="3_valor_a_devolver" class="form-control input-sm" value="0,00"  style="text-align: right; margin-left: -12px;" />
            </div>
        </div>
        
        <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
          <label for="obs">Justificativa/Ações Corretivas:</label>
          <textarea name="obs" id="3_obs" rows="6" cols="20" class="form-control input-sm" onkeyup="contar_caracteres('3_obs', '3_contador', 2000)" maxlength="2000" placeholder="Caso tenha gerado um processo adminstrativo informe o número aqui."></textarea>
          <button type="button" class="btn btn-link" onclick="return anexar_arquivo_justificativa();" style="padding-left: 0px;">Anexar arquivo comprobatório.</button>
          <button type="button" class="btn btn-link" onclick="return ver_arquivos_anexados();">Visualizar Arquivos Anexados.</button>
          <button type="button" class="btn btn-link" onclick="return ver_detalhes();">Ver Detalhes.</button>
          <button type="button" class="btn btn-link" onclick="return consulta_justificativas($('#3_CodRegistro').val());">Justificativas/Pareceres</button>
          <input type="text" class="form-control" id="3_contador" name="contador" style="width: 60px; float: right;" disabled="">
        </div>
        
        <div class="row" style="padding: 5px; margin-bottom: 0px;">
            <label class="control-label col-sm-3" for="3_valor" style="text-align: left; padding-left: 0px;">Justificado Por:</label>
            <div class="col-sm-9">
              <input type="text" name="analisado_por" id="3_analisado_por" class="form-control input-sm" value="" style="margin-left: -12px; width: 104.8%;" disabled=""/>
            </div>
        </div>
            
        <input type="hidden" name="operacao" id="3_operacao" value="" />
        <input type="hidden" name="CodRegistro" id="3_CodRegistro" value="" />
        <input type="hidden" name="acao" id="3_acao" value="<?php echo $acao; ?>" />
        <input type="hidden" name="tipo" id="3_tipo" value="<?php echo $acao; ?>" />
        <input type="hidden" name="rowindex" id="3_rowindex" value="" />
        <input type="hidden" name="rowdata" id="3_rowdata" value="" />
        
        <hr>
        
    </form>
    
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-info" id="3_btn_sair" onclick='$("#popup_inconsistencia").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
        <button type="button" class="btn btn-success" id="3_btn_ok" onclick='salvar_analise();'>Salvar &nbsp;<span class="fa fa-save"></span></button>
        <button type="button" class="btn btn-primary" id="3_btn_concluir" onclick='concluir_analise();'>Concluir Justificativa &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
</div>