$(document).ready(function(){
    
    popup_mensagens_();
    popup_nova_mensagem_();
    
});

function popup_mensagens_(){
    $("#popup_mensagens").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 900,
        maxWidth: 900,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function popup_nova_mensagem_(){
    $("#popup_nova_mensagem").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 700,
        maxWidth: 700,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function visualizar_mensagens(cod){
    $("#10_CodRegistro").val(cod);
    $("#10_operacao").val("consultar_msg");
    formdata = $("#form_mensagem").serialize();
    submit_data("server.php");
    return false;
}

function retorno_visualizar_mensagens(retorno){
    
    $("#tb_msg").html(retorno.body);
    
    $("#popup_mensagens").dialog("open");
    return false;
}

function cadastrar_msg(){
    $("#10_operacao").val("equipe_msg");
    formdata = $("#form_mensagem").serialize();
    submit_data("server.php");
    return false;
}

function retorno_cadastrar_msg(retorno){
    $("#10_emails").html(retorno.html);
    $("#10_operacao").val("cadastrar_msg");
    $("#10_msg").val("");
    $("#10_prioridade").val("normal");
    $("#popup_nova_mensagem").dialog("open");
}

function alterar_msg(cod){
    $("#10_CodMensagem").val(cod);
    $("#10_operacao").val("editar_msg");
    formdata = $("#form_mensagem").serialize();
    submit_data("server.php");
    return false;
}

function retorno_alterar_msg(retorno){
    $("#10_operacao").val("salvar_msg");
    $("#10_msg").val(retorno.Mensagem);
    $("#10_prioridade").val(retorno.Prioridade);
    $("#10_emails").html(retorno.html);
    $("#popup_nova_mensagem").dialog("open");
}

function excluir_msg(cod){
    $("#10_CodMensagem").val(cod);
    $("#10_operacao").val("excluir_msg");
    formdata = $("#form_mensagem").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<span style='color: red;'>Você está certo da exclusão da mensagem?</span>");
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_excluir_msg(retorno){
    $("#tb_msg").html(retorno.body);
    execute_cod = 2;
    execute();
    $("#msg_sucesso").text(retorno.mensagem);
    $("#popup_sucesso").dialog("open");
}

function salvar_msg(){
    formdata = $("#form_mensagem").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("As informações estão corretas?");
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_salvar_msg(retorno){
    $("#tb_msg").html(retorno.body);
    execute_cod = 2;
    execute();
    $("#popup_nova_mensagem").dialog("close");
    $("#msg_sucesso").text(retorno.mensagem);
    $("#popup_sucesso").dialog("open");
}