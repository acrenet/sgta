$(document).ready(function(){
   
    popup_distribuicao_();

});

function popup_distribuicao_(){
    $("#popup_distribuicao").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 'auto',
        maxWidth: 900,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function selecionar_boxes(){
    if($("#selecionar_todos").is(':checked')){
        $('input:checkbox').prop('checked',true);
    }else{
        $('input:checkbox').prop('checked', false);
    }
}

function distribuir_registro(CodRegistro, CodTipo){
    $("#11_operacao").val("consultar_dist");
    $("#11_CodRegistro").val(CodRegistro);
    $("#11_CodTipo").val(CodTipo);
    formdata = $("#form_distribuicao").serialize();
    submit_data("server.php");
    return false;
}

function vincular_registro(CodRegistro, cpf){
    $("#11_operacao").val("vincular_dist");
    $("#11_CodRegistro").val(CodRegistro);
    $("#11_filtro").val(cpf);
    pergunta_ok_acao = 1;
    formdata = $("#form_distribuicao").serialize();
    $("#msg_pergunta").html("Você está certo desta ação?");
    $("#popup_pergunta").dialog("open");
    //submit_data("server.php");
    return false;
}

function concluir_pre_analise(CodRegistro){
    $("#11_operacao").val("concluir_pre_analise");
    $("#11_CodRegistro").val(CodRegistro);
    formdata = $("#form_distribuicao").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("Você está certo desta ação?");
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_distribuir_registro(retorno){
    $("#11_tb_usuarios").html(retorno.html);
    $("#11_tb_registros").html(retorno.html2);
    $("#CodTrilha").val(retorno.CodTrilha);
    $("#popup_distribuicao").dialog("open");
    return false;
}

function retorno_vincular_registro(retorno){
    $("#popup_distribuicao").dialog("close");
    execute_cod = 2;
    execute();
}

function retorno_concluir_pre_analise(retorno){
    execute_cod = 3;
    execute();
}