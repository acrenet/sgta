$(document).ready(function(){
    popup_anexos_(); 
});

function popup_anexos_(){
    $("#popup_anexos").dialog({
        autoOpen: false,
        resizable: true,
        modal: true,
        width: 'auto',
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function visualizar_anexos(cod){
    $("#2_CodRegistro").val(cod);
    formdata = $("#form_anexos").serialize();
    submit_data("server.php");
    return false;
}

function retorno_visualizar_anexos(retorno){
    $("#2_tb_anexos").html(retorno.anexos);
    $("#popup_anexos").dialog("open");
}

function excluir_arquivo(CodAnexo){
    $("#CodAnexo").val(CodAnexo);
    $("#operacao").val("excluir_arquivo");
    formdata = $("#form1").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<span style='color: red;'>Você tem certeza que gostaria de excluir este arquivo?</span>");
    $("#popup_pergunta").dialog("open");
    return false;
}