$(document).ready(function(){
    
    popup_inconsistencia_();
    
});

function popup_inconsistencia_(){
    $("#popup_inconsistencia").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 800,
        maxWidth: 800,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function justificar_inconsistencia(cod, cpf, index, reg){
    
    if($("#st_"+reg).text() == "analisado" || $("#st_"+reg).text() == "concluído"){
        $("#msg_alerta").html("Este registro encontra-se " + $("#st_"+reg).text() + " e não poderá ser modificado.");
        $("#popup_alerta").dialog("open");
        return false;
    }else{
        $("#3_operacao").val("justificar_inconsistencia");
        $("#3_CodRegistro").val(cod);

        rowdata = minha_tabela.row(index).data();

        $("#3_rowindex").val(index);
        $("#3_rowdata").val(JSON.stringify(rowdata));

        pergunta_ok_acao = 1;

        if(cpf == "0"){
            $("#3_tipo").val("sem reserva");
            formdata = $("#form_inconsistencia").serialize();
            $("#msg_pergunta").html("O registro ficará reservado em seu nome, deseja continuar?");
            $("#popup_pergunta").dialog("open");
        }else if($("#CPF").val() != cpf) {
            $("#3_tipo").val("reservado por outro");
            formdata = $("#form_inconsistencia").serialize();
            $("#msg_pergunta").html("Este registro encontra-se reservado.<br>Deseja transferir a reserva para o seu nome?");
            $("#popup_pergunta").dialog("open");
        }else{
            $("#3_tipo").val("reserva propria");
            formdata = $("#form_inconsistencia").serialize();
            pergunta_ok();
        }

        return false;
    }
       
}

function retorno_justificar_inconsistencia(retorno){
    
    $("#popup_pergunta").dialog("close");
    if(retorno.mensagem != ""){
        $("#msg_alerta").html(retorno.mensagem);
        $("#popup_alerta").dialog("open");
        execute_cod = 2;
        execute();
        return false;
    }
    
    $("#3_cpf").val(retorno.registro[0]["CPF_CNPJ_Proc"]);
    $("#3_nome").val(retorno.registro[0]["Nome_Descricao"]);
    $("#3_obs").val(retorno.registro[0]["JustificativaOrgao"]);
    $("#3_valor_a_devolver").val(retorno.registro[0]["ValorADevolver"]);
    $("#3_parecer").val(retorno.registro[0]["CodParecer"]);
    $("#3_analisado_por").val(retorno.registro[0]["UsuarioOrgao"]);
    
    $("#3_parecer").prop("disabled", false);
    $("#3_valor_a_devolver").prop("disabled", false);
    $("#3_obs").prop("disabled", false);
    $("#3_btn_ok").show();
    $("#3_btn_concluir").show();
    
    $("#popup_inconsistencia").dialog("open");
    
    if(retorno.recarregar == true){
        //minha_tabela
        //    .row( retorno.rowindex )
        //    .data( retorno.rowdata )
        //    .draw();
        execute_cod = 2;
        execute();
    }
}

function consultar_justificativa(cod){
    $("#3_operacao").val("consultar_justificativa");
    $("#3_CodRegistro").val(cod);
    formdata = $("#form_inconsistencia").serialize();
    submit_data("server.php");
    return false;
}

function retorno_consultar_justificativa(retorno){
    $("#3_cpf").val(retorno.registro[0]["CPF_CNPJ_Proc"]);
    $("#3_nome").val(retorno.registro[0]["Nome_Descricao"]);
    $("#3_obs").val(retorno.registro[0]["JustificativaOrgao"]);
    $("#3_valor_a_devolver").val(retorno.registro[0]["ValorADevolver"]);
    $("#3_parecer").val(retorno.registro[0]["CodParecer"]);
    $("#3_analisado_por").val(retorno.registro[0]["UsuarioOrgao"]);
    
    $("#3_parecer").prop("disabled", true);
    $("#3_valor_a_devolver").prop("disabled", true);
    $("#3_obs").prop("disabled", true);
    $("#3_btn_ok").hide();
    $("#3_btn_concluir").hide();
    
    $("#popup_inconsistencia").dialog("open");
}

function anexar_arquivo_justificativa(){
    anexar_arquivo($("#3_CodRegistro").val(),'registro');
    return false;
}

function ver_arquivos_anexados(){
    visualizar_anexos($("#3_CodRegistro").val());
    return false;
}

function ver_detalhes(){
    detalhes($("#3_CodRegistro").val());
    return false;
}

function salvar_analise(){
    $("#3_operacao").val("salvar_analise");
    formdata = $("#form_inconsistencia").serialize();
    
    pergunta_ok_acao = 1;
    
    $("#msg_pergunta").html("As informações estão corretas?");
    $("#popup_pergunta").dialog("open");
    return false;
}

function concluir_analise(){
    $("#3_operacao").val("concluir_analise");
    formdata = $("#form_inconsistencia").serialize();
    
    pergunta_ok_acao = 1;
    
    $("#msg_pergunta").html("Você tem certeza que gostaria de concluir a análise?<br><span style='color: red; font-weight: bold;'>Não será mais possível realizar alterações.</span>");
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_analise(retorno){
    execute_cod = 2;
    execute();
    $("#popup_inconsistencia").dialog("close");
    $("#msg_sucesso").text(retorno.mensagem);
    $("#popup_sucesso").dialog("open");
}

function zera_valor(){
    if($("#3_parecer").val() != 2 && $("#3_parecer").val() != 4){
        $("#3_valor_a_devolver").val("0,00");
    }
}



