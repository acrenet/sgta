$(document).ready(function(){
    
    popup_justificativas_();
    
});

function popup_justificativas_(){
    $("#popup_justificativas").dialog({
        autoOpen: false,
        resizable: true,
        modal: true,
        width: 'auto',
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
    
    $( "#4_dtquarentena" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
}

function consulta_justificativas(cod){
    
    $("#6_CodRegistro").val(cod);
    formdata = $("#6_form1").serialize();
    submit_data("server.php");
    
    return false;
}

function consulta_justificativas_retorno(retorno){
    $("#tb_justificativas").html(retorno.html);
    $("#popup_justificativas").dialog("open");
}