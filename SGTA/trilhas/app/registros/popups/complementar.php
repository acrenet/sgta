<script src="../../js/datepicker.js" type="text/javascript"></script>
<script src="popups/complementar.js" type="text/javascript"></script>

<div id="popup_complementar" title="Ações Complementares" style="overflow: hidden;" >
    <form id="form_complementar" name="form_complementar" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div>
            <button type="button" class="btn" id="8_btn1" onclick="preencher_detalhes('alterar_prazo')"><i class="fa fa-clock-o fa-3x fa-fw" aria-hidden="true" style="color:orange;" ></i></button> &nbsp; Alterar o prazo do monitoramento.
        </div>
        <br>
        <div>
            <button type="button" class="btn" id="8_btn2" onclick="preencher_detalhes('arquivar_processo')"><i class="fa fa-archive fa-3x fa-fw" aria-hidden="true" style="color:green;"></i></button> &nbsp; Encerrar o monitoramento e arquivar o registro de inconsistência.
        </div> 
        <br>
        <!--
            SGTA-38
        <div>
            <button type="button" class="btn" id="8_btn3" onclick="preencher_detalhes('abrir_auditoria')"><i class="fa fa-user-secret fa-3x fa-fw" aria-hidden="true"  style="color:darkblue;"></i></button> &nbsp; Encerrar o monitoramento e requisitar abertura de inspeção/auditoria.
        </div> 
        -->
        <br>
        <div>
            <button type="button" class="btn" id="8_btn4" onclick="preencher_detalhes('reabrir_processo')"><i class="fa fa-folder-open fa-3x fa-fw" aria-hidden="true"  style="color: darkviolet;"></i></button> &nbsp; Reabrir o registro de inconsistência.
        </div> 
        <br>
        <div>
            <button type="button" class="btn" id="8_btn5" onclick="preencher_detalhes('invocar_processo')"><i class="fa fa-step-forward fa-3x fa-fw" aria-hidden="true"  style="color: Brown;"></i></button> &nbsp; Invocar registro não justificado pelo órgão no prazo legal.
        </div>
          
    </form>
    
    <div style="text-align: right; display: none;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick='$("#popup_observacao_cge").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        <button type="button" class="btn btn-success" onclick='observacao_cge();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
</div>

<div id="popup_complementar2" title="Ações Complementares" style="overflow: hidden;" >
    <form id="form_complementar2" name="form_complementar" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        <div class="form-group" style="margin-bottom: 5px;">
            <label class="control-label col-sm-3" for="4_parecer">Parecer:</label>
            <div class="col-sm-9"> 
                <textarea class="form-control input-sm" rows="4" id="8_parecer" name="parecer"  onkeyup="contar_caracteres('8_parecer', '8_contador', 2000)" maxlength="2000" placeholder="(Preenchimento Obrigatório)"></textarea>
                <input type="text" class="form-control" id="8_contador" name="contador" style="width: 60px; float: right;" disabled="">
                <button type="button" class="btn btn-link" onclick="return consulta_justificativas($('#8_CodRegistro').val());">Justificativas/Pareceres</button>
            </div>
        </div>
        <div id="8_divquarentena" class="form-group" style="margin-bottom: 5px; display: none;">
          <label class="control-label col-sm-3" for="pwd">Em Monitoramento Até:</label>
          <div class="col-sm-3"> 
              <input type="text" class="form-control input-sm" id="8_dtquarentena" name="dtquarentena" placeholder="" style="width: 110px; display: inline;">
          </div>
        </div>
        <div id="8_divnumprocauditoria" class="form-group" style="margin-bottom: 5px; display: none;">
          <label class="control-label col-sm-3" for="pwd">Número do Processo:</label>
          <div class="col-sm-6"> 
              <input type="text" class="form-control input-sm" id="8_numprocauditoria" name="numprocauditoria" placeholder="" style="width: 200px; display: inline;" maxlength="18">
          </div>
        </div>
        
        <div id="8_detalheresultado" class="panel panel-default" style="display: none;">
            <div class="panel-heading" style="font-size: 110%; font-weight: bold;">Resultado da Auditoria e Recomendações do TCE &nbsp;&nbsp;<a class="consultar" title="Instruções" onclick="$('#8_orientacao').slideToggle(); return false;"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></a></div>
            <div class="panel-body">
                <div id="8_orientacao" class="well well-sm" style="text-align: justify; display: none;">
                    <p>
                        <b>Destina-se ao acompanhamento do resultado da Trilha de Auditoria.</b><br>
                        Os itens que forem marcados como "Solicitado" tem interpretação diferente pelo sistema de acordo com o resultado da 
                        análise e influenciam nos cálculos estatísticos e nos relatórios que o sistema emite:<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1- Se o registro estiver em monitoramento: O sistema entende que o órgão acatou a recomendação porém ainda não a implementou.<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2- Se o registro foi arquivado ou foi recomendado inspeção ou auditoria: O sistema entende que o órgão deixou de 
                        cumprir ou se recusou a atender a recomendação.<br>
                    </p>
                    <p>
                        <b>Criação/Melhoria de Leis, Decretos, etc:</b> Marcar "Sim" caso tenha sido feito a recomendação de expedição de 
                        algum instrumento legal (leis, decretos, etc.) e esta foi efetivada pelo órgão/entidade, marcar "Solicitado" caso a 
                        recomendação tenha sido aceita mas ainda não implementada, o registro deverá ficar em monitoramento até a implementação. 
                        Marcar "Solicitado" caso a recomendação não tenha sido atendida, o registro deverá ser arquivado ou deverá ser 
                        solicitado a abertura de processo de inspeção ou auditoria.
                    </p>
                    <p>
                        <b>Criação/Melhorias em Sistemas:</b> Marcar "Sim" caso tenha sido feito a recomendação de criação ou 
                        reformulação de algum sistema de gerenciamento de dados e esta foi implementada pelo órgão. Marcar "Solicitado" caso 
                        a recomendação tenha sido aceita mas ainda não implementada, o registro deverá ficar em monitoramento até a implementação. 
                        Marcar "Solicitado" caso a recomendação não tenha sido atendida, o registro deverá ser arquivado ou deverá ser 
                        solicitado a abertura de processo de inspeção ou auditoria.
                    </p>
                    <p>
                        <b>Capacitação Técnica dos Agentes:</b> Marcar "Sim" caso tenha sido feito a recomendação de capacitação ou 
                        orientação de agentes/servidores envolvidos no processo e esta foi implementada pelo órgão. Marcar "Solicitado" 
                        se a recomendação foi aceita mas ainda não implementada, o registro deverá ficar em monitoramento até a implementação. 
                        Marcar "Solicitado" caso a recomendação não tenha sido atendida, o registro deverá ser arquivado ou deverá ser solicitado a 
                        abertura de processo de inspeção ou auditoria.
                    </p>
                    <p>
                        <b>Devolução de Numerário:</b> Marcar "Sim" caso tenha sido solicitado a devolução de numerário e foi efetivada pelo órgão. 
                        Marcar "Solicitado" se a recomendação será efetivada ou se estiver ocorrendo de forma parcelada, o registro 
                        deverá ficar em monitoramento até a quitação das parcelas. Marcar "Solicitado" caso a recomendação não tenha sido 
                        atendida, o registro deverá ser arquivado ou deverá ser solicitado a abertura de processo de inspeção ou auditoria.
                    </p>
                    <p>
                        <b>Economia Potencial/Gerada:</b> Preencher o valor que foi ressarcido ao Estado, caso haja apenas a promessa de devolução
                        o registro deverá permanecer em monitoramento até a efetivação da devolução.
                    </p>
                    <p>
                        <b>Economia Mensal:</b> Preencher o valor quando se puder determinar a economia mensal gerada para o Estado em
                        virtude do trabalho de auditoria.
                    </p>
                </div>
                <div class="form-group" style="margin-bottom: 3px;">
                    <label class="control-label col-sm-5" for="8_intrumentoslegais0" style="padding-top: 16px !important;">Criação/Melhoria de Leis, Decretos, etc:</label>
                    <div class="col-sm-7">
                        <div class="radio-inline" style="padding-left: 0px !important;"> 
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="instrumentoslegais[]" id="8_intrumentoslegais0" value="0" checked=""/> Não </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="instrumentoslegais[]" id="8_intrumentoslegais1" value="1" /> Sim </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="instrumentoslegais[]" id="8_intrumentoslegais2" value="2" /> Solicitado</label>
                            <input type="text" name="instrumentoslegaisdesc" id="8_instrumentoslegaisdesc" class="form-control input-sm" placeholder="Se positivo discrimine." style="width: 220px; display: inline; margin-left: 10px;" maxlength="100"/>
                        </div>
                    </div>    
                </div>
                <div class="form-group" style="margin-bottom: 3px;">
                    <label class="control-label col-sm-5" for="8_sistemasgerenciais0" style="padding-top: 16px !important;">Criação/Melhorias em Sistemas:</label>
                    <div class="col-sm-7">
                        <div class="radio-inline" style="padding-left: 0px !important;"> 
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="sistemasgerenciais[]" id="8_sistemasgerenciais0" value="0" checked=""/> Não </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="sistemasgerenciais[]" id="8_sistemasgerenciais1" value="1" /> Sim </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="sistemasgerenciais[]" id="8_sistemasgerenciais2" value="2" /> Solicitado </label>
                            <input type="text" name="sistemasgerenciaisdesc" id="8_sistemasgerenciaisdesc" class="form-control input-sm" placeholder="Se positivo discrimine." style="width: 220px; display: inline; margin-left: 10px;" maxlength="100"/>
                        </div>
                    </div>    
                </div>
                <div class="form-group" style="margin-bottom: 8px;">
                    <label class="control-label col-sm-5" for="8_capacitacao0" style="padding-top: 16px !important;">Capacitação Técnica dos Agentes:</label>
                    <div class="col-sm-7">
                        <div class="radio-inline" style="padding-left: 0px !important;"> 
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="capacitacao[]" id="8_capacitacao0" value="0" checked=""/> Não </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="capacitacao[]" id="8_capacitacao1" value="1" /> Sim </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="capacitacao[]" id="8_capacitacao2" value="2" /> Solicitado </label>
                            <input type="text" name="capacitacaodesc" id="8_capacitacaodesc" class="form-control input-sm" placeholder="Se positivo discrimine." style="width: 220px; display: inline; margin-left: 10px;" maxlength="100"/>
                        </div>
                    </div>    
                </div>
                <div class="form-group" style="margin-bottom: 13px;" style="padding-top: 16px !important;">
                    <label class="control-label col-sm-5" for="8_devolucao0">Devolução de Numerário:</label>
                    <div class="col-sm-7">
                        <div class="radio-inline" style="padding-left: 0px !important;"> 
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="devolucao[]" id="8_devolucao0" value="0" checked=""/> Não </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="devolucao[]" id="8_devolucao1" value="1" /> Sim </label>
                            <label class="radio-inline" style="padding-top: 0px !important;"><input type="radio" name="devolucao[]" id="8_devolucao2" value="2" /> Solicitado </label>
                        </div>
                    </div>    
                </div>
                <div class="form-group" style="margin-bottom: 3px;">
                    <label class="control-label col-sm-5" for="pwd">Economia Potencial/Gerada:</label>
                    <div class="col-sm-2"> 
                        <input type="text" class="form-control input-sm" name="valorressarcido" id="8_valorressarcido" value="" />
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label col-sm-5" for="pwd">Economia Mensal:</label>
                    <div class="col-sm-2"> 
                        <input type="text" class="form-control input-sm" name="economiamensal" id="8_economiamensal" value="" />
                    </div>
                </div> 
            </div>
        </div>
        
        <input type="hidden" name="operacao" id="8_operacao" value="" />
        <input type="hidden" name="CodRegistro" id="8_CodRegistro" value="" />
        <input type="hidden" name="acao" id="8_acao" value="" />
        <input type="hidden" name="exec" id="8_exec" value="" />
        <input type="hidden" name="exec2" id="8_exec2" value="" />
    </form>
    <hr>
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-info" id="3_btn_sair" onclick='$("#popup_complementar2").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
        <button type="button" class="btn btn-success" id="3_btn_concluir" onclick='salvar_complementar();'>Salvar &nbsp;<span class="fa fa-save"></span></button>
    </div>
</div>