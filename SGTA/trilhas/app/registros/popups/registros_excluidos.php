<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<script src="popups/registros_excluidos.js" type="text/javascript"></script>

<div id="popup_registros_excluidos" title="Registros Excluidos" style="" > 


    <div class="container">
        <div class="panel-group" >
            <div class="panel panel-primary">
                <div class="panel-heading"><h4>Registros excluidos da trilha</h4></div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped table-hover table-condensed display">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>CPF_CNPJ_Proc</th>
                                <th>Nome_Descricao</th>
                                <th>Usuário</th>
                                <th>Data</th>
                                <th>Motivo Exclusão</th>
                                <th style="width: 137px;">Ações</th>
                            </tr>

                            <thead id="grid-excluidos">
                              
                            </thead>
                       
                        </thead>
                     
                    </table>
                  
    
                </div>
            </div>
        </div>
        
            <div class="panel-group" style="visibility: hidden;">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4>Registros Removidos</h4></div>
                    <div class="panel-body">
                        
                        <form id="form1" class="form-horizontal" role="form" method="post">

                            <input type="hidden" name="operacao" id="operacao" value="load_registros_removidos" />
                            <input type="hidden" name="detalhe" id="detalhe" value="<?php echo $detalhe; ?>" />
                            <input type="hidden" name="CodTrilha" id="CodTrilha" value="" />
                            <input type="hidden" name="CodRegistro" id="CodRegistro" value="" />
                            <input type="hidden" name="CodAnexo" id="CodAnexo" value="" />
                        </form>   
                    </div>
                </div>
            </div>

        </div>


</div> 

