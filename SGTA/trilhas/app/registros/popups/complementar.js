$(document).ready(function(){
    
    popup_complementar_();
    popup_complementar2_();
    
    $( "#8_dtquarentena" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
});

function popup_complementar_(){
    $("#popup_complementar").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 600,
        maxWidth: 850,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
    
}

function popup_complementar2_(){
    $("#popup_complementar2").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 850,
        maxWidth: 850,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
    
    //$( "#8_dtquarentena" ).datepicker({
      //changeMonth: true,
      //changeYear: true
    //});
}

function acoes_complementares(cod){
    $("#8_CodRegistro").val(cod);
    $("#8_operacao").val("consulta_complementar");
    formdata = $("#form_complementar2").serialize();
    submit_data("server.php");
    return false;
}

function retorno_acoes_complementares(retorno){
    
    $("#8_numprocauditoria").val("");
    $("#8_parecer").val("");
    $("#8_dtquarentena").val(retorno.QuarentenaAte);
    
    $("#8_instrumentoslegaisdesc").val(retorno.InstrumentosLegaisDesc);
    $("#8_sistemasgerenciaisdesc").val(retorno.SistemasGerenciaisDesc);
    $("#8_capacitacaodesc").val(retorno.CapacitacaoDesc);
    
    if(retorno.nao_analisado == true){
        $("#8_btn5").prop('disabled',false);
        $("#8_btn4").prop('disabled',true);
        $("#8_exec2").val(1);
    }else{
        $("#8_btn5").prop('disabled',true);
        $("#8_btn4").prop('disabled',false);
        $("#8_exec2").val(0);
    }
    
    if(retorno.monitoramento == true){
        $("#8_exec").val(1);
        $("#8_btn1").prop('disabled',false);  
        $("#8_btn2").prop('disabled',false);  
        $("#8_btn3").prop('disabled',false);  
    }else{
        $("#8_exec").val(0);
        $("#8_btn1").prop('disabled',true);
        $("#8_btn2").prop('disabled',true);
        $("#8_btn3").prop('disabled',true);
    }
    
    
    var resultado = retorno.ResultadoAuditoria;
    resultado = resultado.split(";");
    
    var expedicao = resultado[0];
    var sistemas = resultado[1];
    var capacitacao = resultado[2];
    var devolucao = resultado[3];
    
    expedicao = expedicao.split("=");
    sistemas = sistemas.split("=");
    capacitacao = capacitacao.split("=");
    devolucao = devolucao.split("=");
    
    console.log(expedicao[1] + " - " + sistemas[1] + " - " + capacitacao[1] + " - " + devolucao[1]);
    
    switch (expedicao[1]){ 
        case "0":
            $("#8_intrumentoslegais0").prop("checked", true);
            break;
        case "1":
            $("#8_intrumentoslegais1").prop("checked", true);
            break;
        case "2":
            $("#8_intrumentoslegais2").prop("checked", true);
            break;
    }
    
    switch (sistemas[1]){ 
        case "0":
            $("#8_sistemasgerenciais0").prop("checked", true);
            break;
        case "1":
            $("#8_sistemasgerenciais1").prop("checked", true);
            break;
        case "2":
            $("#8_sistemasgerenciais2").prop("checked", true);
            break;
    }
    
    switch (capacitacao[1]){ 
        case "0":
            $("#8_capacitacao0").prop("checked", true);
            break;
        case "1":
            $("#8_capacitacao1").prop("checked", true);
            break;
        case "2":
            $("#8_capacitacao2").prop("checked", true);
            break;
    }
    
    switch (devolucao[1]){ 
        case "0":
            $("#8_devolucao0").prop("checked", true);
            break;
        case "1":
            $("#8_devolucao1").prop("checked", true);
            break;
        case "2":
            $("#8_devolucao2").prop("checked", true);
            break;
    }
    
    $("#8_valorressarcido").val(retorno.ValorRessarcido);
    $("#8_economiamensal").val(retorno.EconomiaMensal);
    
    $("#popup_complementar").dialog("open");
}

function preencher_detalhes(acao){
    
    $("#8_divquarentena").hide();
    $("#8_detalheresultado").hide();
    $("#8_divnumprocauditoria").hide();
    
    $("#8_operacao").val("acoes_complementares");
    $("#8_acao").val(acao);
    
    
    
    if(acao != "reabrir_processo" && acao != "invocar_processo" && $("#8_exec").val() == 0){
        return false;
    }
    
    if(acao == "invocar_processo" && $("#8_exec2").val() == 0){
        return false;
    }
    
    if(acao == "reabrir_processo" && $("#8_exec2").val() == 1){
        return false;
    }
    
    if(acao == "alterar_prazo"){
        $("#8_divquarentena").show();
        $("#8_detalheresultado").show();
        $('#popup_complementar2').dialog('option', 'title', 'Alterar o Prazo do Monitoramento.');
        $('#popup_complementar2').dialog('open');
    }else if(acao == "arquivar_processo"){
        $("#8_detalheresultado").show();
        $('#popup_complementar2').dialog('option', 'title', 'Encerrar o Monitoramento e Arquivar o Registro.');
        $('#popup_complementar2').dialog('open');
    }else if(acao == "abrir_auditoria"){
        $("#8_detalheresultado").show();
        $('#popup_complementar2').dialog('option', 'title', 'Encerrar o Monitoramento e Requisitar Abertura de Inspeção/Auditoria.');
        $('#popup_complementar2').dialog('open');
    }else if(acao == "reabrir_processo"){
        $('#popup_complementar2').dialog('option', 'title', 'Reabrir o Registro.');
        pergunta_ok_acao = 1;
        formdata = $("#form_complementar2").serialize();
        $("#msg_pergunta").html("<span style='color:darkgreen;'><b>Você tem certeza que gostaria de reabrir o registro?</b></span>");
        $("#popup_pergunta").dialog("open");
    }else if(acao == "invocar_processo"){
        $('#popup_complementar2').dialog('option', 'title', 'Invocar o Registro.');
        pergunta_ok_acao = 1;
        formdata = $("#form_complementar2").serialize();
        $("#msg_pergunta").html("<span style='color:darkgreen;'><b>Você tem certeza que gostaria de invocar o registro não justificado?</b></span>");
        $("#popup_pergunta").dialog("open");
    }
    return false;
}

function salvar_complementar(){
    pergunta_ok_acao = 1;
    formdata = $("#form_complementar2").serialize();
    $("#msg_pergunta").html("<span'><b>As informações estão corretas?</b></span>");
    $("#popup_pergunta").dialog("open");
}

function retorno_salvar_complementar(retorno){
    $("#popup_complementar2").dialog("close");
    $("#popup_complementar").dialog("close");
    $("#msg_sucesso").text("Dados Gravados com Sucesso.");
    $("#popup_sucesso").dialog("open");
    execute_cod = 2;
    execute();
    
}
