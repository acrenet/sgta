
<script src="popups/distribuicao.js" type="text/javascript"></script>

<div id="popup_distribuicao" title="Distribuir Registro" style="overflow: hidden;">
    
    <form id="form_distribuicao" name="form1" role="form">
        
        
        <p><b>Caso deseje selecione outros registros para distribuir em lote.</b></p>   
        
        <div>
            <table class="table table-striped table-condensed" style="font-size: 85%;">
                <thead>
                    <tr>
                        <th><input type="checkbox" name="selecionar_todos" id="selecionar_todos" value="ON" onclick="selecionar_boxes();"/></th>
                        <th>Ord</th>
                        <th>Identificador</th>
                        <th>Nome/Descrição</th>
                        <th>Órgão</th>
                    </tr>
                </thead>
                <tbody id="11_tb_registros">
                    
                </tbody>
            </table>

        </div>
        <hr>
        <p class="validateTips"><b>Selecione o servidor que fará a homologação de todos os registros selecionados.</b></p>
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
                    <th>Servidor</th>
                    <th>Perfil</th>
                    <th style="text-align: right">Registros</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="11_tb_usuarios">
               
            </tbody>
        </table>
        
        <input type="hidden" name="CodRegistro" id="11_CodRegistro" value="" />
        <input type="hidden" name="CodTrilha" id="11_CodTrilha" value="" />
        <input type="hidden" name="CodTipo" id="11_CodTipo" value="" />
        <input type="hidden" name="operacao" id="11_operacao" value="" />
        <input type="hidden" name="filtro" id="11_filtro" value="" />

        <hr>
        <div style="text-align: right;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#popup_distribuicao").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        </div>
    </form>
</div>