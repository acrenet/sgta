<script src="popups/redistribuicao.js" type="text/javascript"></script>

<div id="popup_redistribuicao" title="Redistribuir Registro" style="overflow: hidden;">
    <p class="validateTips">Selecione o novo servidor que fará a análise.</p>
    <form id="form_redistribuicao" name="form1" role="form">
        
        <div>
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Servidor</th>
                        <th>Perfil</th>
                        <th>Registros</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="13_tb_usuarios">

                </tbody>
            </table>
        </div>
        
        <input type="hidden" name="CodRegistro" id="13_CodRegistro" value="" />
        <input type="hidden" name="ReservadoPor" id="13_ReservadoPor" value="" />
        <input type="hidden" name="CodTipo" id="13_CodTipo" value="" />
        <input type="hidden" name="operacao" id="13_operacao" value="" />

        <hr>
        <div style="text-align: right;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#popup_redistribuicao").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        </div>
    </form>
</div>

