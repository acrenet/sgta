<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<script src="popups/mesclar.js" type="text/javascript"></script>

<div id="popup_mesclar" title="Mesclar Registros." style="overflow: hidden;" >
    <form id="form_mesclar" name="form_mesclar" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="9_cpf">Idendificador:</label>
                    <input id="9_cpf" name="cpf" type="text" class="form-control input-sm" disabled="">
                </div>
            </div>
            <div class="col-sm-9">
                <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
                    <label for="nome">Nome/Descrição:</label>
                    <input type="text" class="form-control input-sm" id="9_nome" name="nome" disabled="">
                </div>
            </div>
        </div>

        <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
          <label for="9_disponivel">Selecione o Registro para Mesclar:</label>
          <select name="disponivel" id="9_disponivel" class="form-control input-sm">
              <option value=""></option>
              <option value="123">teste</option>   
          </select>
        </div>
        <br><br>
        <label style="color: red; font-weight: bold">ATENÇÃO: Tenha cuidado, esta operação não poderá ser desfeita.</label>
        
        <input type="hidden" name="operacao" id="9_operacao" value="" />
        <input type="hidden" name="CodRegistro" id="9_CodRegistro" value="" />
        
        <hr>
        
    </form>
    
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick='$("#popup_mesclar").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        <button type="button" class="btn btn-success" onclick='mesclar_save();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
</div>