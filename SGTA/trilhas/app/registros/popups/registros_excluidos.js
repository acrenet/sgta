$(document).ready(function(){
    
    popup_registros_excluidos_();
    
});

function popup_registros_excluidos_(){
    $("#popup_registros_excluidos").dialog({
        autoOpen: false,
        resizable: true,
        modal: false,
        height:600,
        width: 1300,
        maxWidth: 1800,
        maxHeight :1200,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

//sgta 173 popup registros excluidos

function registros_removidos(CodTrilha){

    $("#operacao").val("load_registros_removidos");
    $("#CodTrilha").val(CodTrilha);
    formdata = $("#form1").serialize();
    submit_data("server.php");
    return false;
   
}
function retorno_registros_removidos(retorno){

    $("#grid-excluidos").html(retorno.tabela);
    $("#CodTrilha").val(retorno.CodTrilha);
    
    $('#popup_registros_excluidos').dialog('option', 'title', 'Registros Removidos da Trilha');
    $('#popup_registros_excluidos').dialog('open');
} 

function retornar_registro_trilha(cod ,CodTrilha,CodOrgao){

    //SGTA 173
    $("#CodRegistro").val(cod);
    $("#CodTrilha").val(CodTrilha);
    $("#CodOrgao").val(CodOrgao);
    $("#operacao").val("retornar_registro");
    $("#acao").val("retornar_registro");

    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}

function retorno_retornar_registro_trilha( ){

    execute_cod = 2;
    execute();
    return false;

}


