<?php


?>
<script src="popups/anexos.js" type="text/javascript"></script>

<div id="popup_anexos" title="Anexos" class="container-fluid">
        
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover table-condensed" style="font-size: 80%; width: 100%;">
            <thead>
                <tr>
                    <th>Nome Anexo</th>
                    <th>Descrição</th>
                    <th>Usuário</th>
                    <th>Etapa</th>
                    <th>Data Upload</th>
                </tr>
            </thead>
            <tbody id="2_tb_anexos">
                
            </tbody>
        </table>
    </div>

        
        <hr>
        <div style="text-align: right;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-info" onclick='$("#popup_anexos").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
        </div>
        
        <form name="form_anexos" id="form_anexos" method="POST">
            <input type="hidden" name="operacao" id="2_operacao" value="listar_anexos" />
            <input type="hidden" name="CodRegistro" id="2_CodRegistro" value="" />
        </form>
</div>