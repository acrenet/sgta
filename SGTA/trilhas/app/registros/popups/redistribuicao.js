$(document).ready(function(){
   
    popup_redistribuicao_();

});

function popup_redistribuicao_(){
    $("#popup_redistribuicao").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 'auto',
        maxWidth: 900,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function redistribuir_registro(CodRegistro, ReservadoPor, CodTipo){
    $("#13_CodRegistro").val(CodRegistro);
    $("#13_CodTipo").val(CodTipo);
    $("#13_ReservadoPor").val(ReservadoPor);
    $("#13_operacao").val("redistribuir_consulta");
    formdata = $("#form_redistribuicao").serialize();
    submit_data("server.php");
    return false;
}

function retorno_redistribuir_consulta(retorno){
    $("#13_tb_usuarios").html(retorno.html);
    $("#popup_redistribuicao").dialog("open");
    return false;
}

function redistribuir_para(CodRegistro, cpf, nome){
    $("#13_operacao").val("redistribuir_para");
    $("#13_CodRegistro").val(CodRegistro);
    $("#13_CodTipo").val(cpf);
    $("#13_ReservadoPor").val(nome);
    formdata = $("#form_redistribuicao").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").text("Você está certo desta redistribuição para: " + nome);
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_redistribuir_para(retorno){
    $("#popup_redistribuicao").dialog("close");
    $("#msg_sucesso").text("Redistribuição efetuada com sucesso.");
    $("#popup_sucesso").dialog("open");
    execute_cod = 2;
    execute();
}