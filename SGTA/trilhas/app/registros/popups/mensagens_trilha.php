<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script src="popups/mensagens_trilha.js" type="text/javascript"></script>

<div id="popup_nova_mensagem_trilha" title="Cadastro de Mensagem." style="overflow: hidden;" >
    <form id="form_mensagem_trilha" name="form_mensagem_trilha" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
            <label for="14_msg">Mensagem:</label>
            <textarea name="msg" id="14_msg" rows="6" cols="20" class="form-control input-sm" onkeyup="contar_caracteres('14_msg', '14_contador', 1000)" maxlength="1000"></textarea>
            <input type="text" class="form-control" id="14_contador" name="contador" style="width: 60px; float: right;" disabled="">
        </div>
        
        
        <input type="hidden" name="CodMensagem" id="14_CodMensagem" value="" />
        <input type="hidden" name="CodTrilha" id="14_CodTrilha" value="<?php echo $CodTrilha; ?>" />
        <input type="hidden" name="operacao" id="14_operacao" value="" />
        
        <hr>
        
    </form>
    
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick='$("#popup_nova_mensagem_trilha").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        <button type="button" class="btn btn-success" onclick='salvar_mensagem_trilha();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
</div>