$(document).ready(function(){
    
    popup_consulta_();
    popup_ost_();
    
});

function popup_consulta_(){
    $("#popup_consulta").dialog({
        autoOpen: false,
        resizable: true,
        modal: true,
        width: 1200,
        maxWidth: 1600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        open: function( event, ui ) {
            rolar_para("#popup_consulta");
        }
    });
    
    $( "#4_dtquarentena" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
}

function popup_ost_(){
    $("#popup_ost").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        responsive: true,
        fluid: true,
        width: 'auto',
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function consulta_andamento(cod){
    $("#5_CodRegistro").val(cod);
    formdata = $("#5_form1").serialize();
    submit_data("server.php");
    
    return false;
}

function consulta_andamento_retorno(retorno){
    var parecer = "?";
    switch(retorno.registro[0]["CodParecer"]){
        case "1":
            parecer = "Inconsistência Confirmada, Falha Formal";
            break;
        case "2":
            parecer = "Inconsistência Confirmada com Valor a Devolver";
            break;
        case "3":
            parecer = "Inconsistência Parcialmente Confirmada, Falha Formal";
            break;
        case "4":
            parecer = "Inconsistência Parcialmente Confirmada com Valor a Devolver";
            break;
        case "5":
            parecer = "<span style='color: red;'>Não Concordo</span>";
            break;
        case "6":
            parecer = "Não Analisado";
            break;
            
    }
    
    
    $("#5_cpf").text(retorno.registro[0]["CPF_CNPJ_Proc"]);
    $("#5_nome").text(retorno.registro[0]["Nome_Descricao"]);
    $("#5_justificado_por").text(retorno.registro[0]["UsuarioOrgao"]);
    $("#5_data_justificativa").text(retorno.registro[0]["DataConclusaoJustificativa"]);
    $("#5_parecer").html(parecer);
    $("#5_valor_a_devolver").text(retorno.registro[0]["ValorADevolver"]);
    $("#5_justificativa").html(retorno.registro[0]["JustificativaOrgao"]);
    $("#5_analisado_por").text(retorno.registro[0]["UsuarioCGE"]);
    $("#5_data_analise").text(retorno.registro[0]["DataConclusaoAnalise"]);
    $("#5_justificativa2").html(retorno.registro[0]["JustificativaCGE"]);
    $("#5_status").html(retorno.registro[0]["StatusRegistro"]);
    $("#5_tb_ocorrencias").html(retorno.registro[0]["Ocorrencias"]);
    $("#5_tb_observacoes").html(retorno.registro[0]["Observacoes"]);
    $("#5_tb_resultado").html(retorno.registro[0]["ResultadoDaAnalise"]);
    $("#5_resultado_final").html(retorno.registro[0]["ResultadoFinal"]);
    
    $("#5_pedido_alteracao").html(retorno.registro[0]["MotivoSolicitacao"]);
    $("#5_parecer_solicitacao").html(retorno.registro[0]["ParecerSolicitacao"]);
    
    $("#5_favorecido").html(retorno.registro[0]["Favorecido"]);
    
    $("#popup_consulta").dialog("open");
    rolar_para("#popup_consulta");
}

function consulta_ost(cod){
    $("#5_CodRegistro2").val(cod);
    formdata = $("#5_form2").serialize();
    submit_data("server.php");
    
    return false;
}

function retorno_consulta_ost(retorno){
    $("#5_tbo_st").html(retorno.html);
    $("#popup_ost").dialog("open");
}

function rolar_para(elemento) {
    $('html, body').animate({
      scrollTop: $(elemento).offset().top-450
    }, 300);
}

function imprimir_detalhe(){
    document.getElementById("5_form1").submit();
}

function mostrar_observacao(CodObs){
    var op = $("#5_operacao").val();
    
    $("#5_operacao").val("mostrar_observacao");
    $("#5_CodObs").val(CodObs);
    
    formdata = $("#5_form1").serialize();
    
    $("#5_operacao").val(op);
    
    submit_data("server.php");
    
    return false;
}

function ocultar_observacao(CodObs){
    var op = $("#5_operacao").val();
    
    $("#5_operacao").val("ocultar_observacao");
    $("#5_CodObs").val(CodObs);
    
    formdata = $("#5_form1").serialize();
    
    $("#5_operacao").val(op);
    
    submit_data("server.php");
    
    return false;
}

function retorno_alterar_observacao(retorno){
    $("#5_tb_observacoes").html(retorno.Observacoes);
}