$(document).ready(function(){
    
    popup_nova_mensagem_trilha_();
    
});

function popup_nova_mensagem_trilha_(){
    $("#popup_nova_mensagem_trilha").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 700,
        maxWidth: 700,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function cadastrar_mensagem_trilha(){
    $("#14_msg").val("");
    $("#14_CodMensagem").val(-1);
    $("#14_operacao").val("salvar_mensagem_trilha");
    $("#popup_nova_mensagem_trilha").dialog("open");
}

function alterar_mensagem_trilha(cod){
    $("#14_CodMensagem").val(cod);
    $("#14_operacao").val("alterar_mensagem_trilha");
    formdata = $("#form_mensagem_trilha").serialize();
    submit_data("server.php");
    return false;
}

function retorno_alterar_mensagem_trilha(retorno){
    $("#14_msg").val(retorno.mensagem_trilha);
    $("#14_operacao").val("salvar_mensagem_trilha");
    $("#popup_nova_mensagem_trilha").dialog("open");
}

function salvar_mensagem_trilha(){
    formdata = $("#form_mensagem_trilha").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("As informações estão corretas?");
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_salvar_mensagem_trilha(retorno){
    $("#tb_mensagem_trilha").html(retorno.html_msg_trilhas);
    execute_cod = 2;
    execute();
    $("#popup_pergunta").dialog("close");
    $("#popup_nova_mensagem_trilha").dialog("close");
    $("#msg_sucesso").text(retorno.mensagem);
    $("#popup_sucesso").dialog("open");
}

function excluir_mensagem_trilha(cod){
    $("#14_CodMensagem").val(cod);
    $("#14_operacao").val("excluir_mensagem_trilha");
    formdata = $("#form_mensagem_trilha").serialize();
    pergunta_ok_acao = 1;
    $("#msg_pergunta").html("<span style='color: red;'>Você está certo da exclusão da mensagem?</span>");
    $("#popup_pergunta").dialog("open");
    return false;
}

function retorno_excluir_mensagem_trilha(retorno){
    $("#tb_mensagem_trilha").html(retorno.html_msg_trilhas);
    execute_cod = 2;
    execute();
    $("#popup_pergunta").dialog("close");
    $("#msg_sucesso").text(retorno.mensagem);
    $("#popup_sucesso").dialog("open");
}

