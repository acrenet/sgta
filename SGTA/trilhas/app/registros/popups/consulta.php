<script src="popups/consulta.js" type="text/javascript"></script>

<div id="popup_consulta" title="Justificativas/Análises da Inconsistência" style="margin-bottom: 30px;" >
    <div style="overflow-x: hidden;">
        <div class="well well-sm" style="font-size: 70%; text-align: justify; margin-bottom: 4px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>CPF/CNPJ/Processo:</b></div>
                <div class="col-xs-9" id="5_cpf"></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Nome/Descrição:</b></div>
                <div class="col-xs-9" id="5_nome"></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Status da Inconsistência:</b></div>
                <div class="col-xs-9" id="5_status"></div>
            </div>
        </div>
        
        <div class="well well-sm" style="font-size: 70%; text-align: justify; margin-bottom: 4px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Justificado Por:</b></div>
                <div class="col-xs-9" id="5_justificado_por"></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Data Justificativa:</b></div>
                <div class="col-xs-9" id="5_data_justificativa"></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Parecer:</b></div>
                <div class="col-xs-9" id="5_parecer"><span style="color: red;"></span></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Valor a Devolver:</b></div>
                <div class="col-xs-9" id="5_valor_a_devolver"></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Última Justificativa Cadastrada:</b></div>
                <div class="col-xs-9" style="width: 74.1%;" id="5_justificativa">
                    
                </div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Pedido de Alteração:</b></div>
                <div class="col-xs-9" style="width: 74.1%;" id="5_pedido_alteracao">
                    
                </div>
            </div>
        </div>

        <div class="well well-sm" style="font-size: 70%; text-align: justify; margin-bottom: 4px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Analisado Por:</b></div>
                <div class="col-xs-9" id="5_analisado_por"></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Data Análise:</b></div>
                <div class="col-xs-9" id="5_data_analise"></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Resultado:</b></div>
                <div class="col-xs-9" id="5_resultado_final"></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Favorecido:</b></div>
                <div class="col-xs-9" id="5_favorecido"></div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Último Parecer Cadastrado:</b></div>
                <div class="col-xs-9" style="width: 74.1%;" id="5_justificativa2">
                    
                </div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Resposta Pedido Alteração:</b></div>
                <div class="col-xs-9" style="width: 74.1%;" id="5_parecer_solicitacao">
                    
                </div>
            </div>
            <hr style="color: black; margin-top: 2px; margin-bottom: 2px; box-shadow: 0 1px 1px white; height: 2px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Resultado da Análise:</b></div>
                <div class="col-xs-9" style="width: 74.1%;" id="5_parecer_solicitacao">
                    <table class="table table-bordered table-hover">
                        <tbody id="5_tb_resultado">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div class="well well-sm" style="font-size: 90%; text-align: justify; margin-bottom: 4px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"></div>
                <div class="col-xs-9" style="width: 74.1%;">
                    <a href="#" tabindex="-1" style="color: blue;" onclick="return consulta_justificativas($('#5_CodRegistro').val());">Visualizar Justificativas e Pareceres do Registro</a>
                    <br>
                    <a href="#" tabindex="-1" style="color: blue;" onclick="return consulta_ost($('#5_CodRegistro').val());">Visualizar Ordens de Serviço do Registro</a>
                </div>
            </div>
        </div>
        
        <div class="well well-sm" style="font-size: 70%; text-align: justify; margin-bottom: 4px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Observações do TCE:</b></div>
                <div class="col-xs-9" style="width: 74.1%;">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>Observação</th>
                                <th>Usuário</th>
                                <th>Contato</th>
                            </tr>
                        </thead>
                        <tbody id="5_tb_observacoes">
                            
                        </tbody>
                    </table>   
                </div>
            </div>
        </div>
        
        <div class="well well-sm" style="font-size: 70%; text-align: justify; margin-bottom: 4px;">
            <div class="row">
                <div class="col-xs-3" style="text-align: right;"><b>Eventos:</b></div>
                <div class="col-xs-9">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>Evento</th>
                                <th>Usuário</th>
                                <th>Contato</th>
                            </tr>
                        </thead>
                        <tbody id="5_tb_ocorrencias">
                            
                        </tbody>
                    </table>   
                </div>
            </div>
        </div>
        
    </div>
        
    <hr>
    <!--andre , mudar aqui tambem  -->
    <form name="5_form1" id="5_form1" action="/trilhas/app/registros/reports/detalhe.php" method="POST" target="_blank">
        <input type="hidden" name="operacao" id="5_operacao" value="andamento" />
        <input type="hidden" name="CodRegistro" id="5_CodRegistro" value="" />
        <input type="hidden" name="CodObs" id="5_CodObs" value="" />
    </form>
    
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-success" id="3_btn_sair" onclick='imprimir_detalhe();'>Imprimir &nbsp;<span class="fa fa-print"></span></button>
        <button type="button" class="btn btn-info" id="3_btn_sair" onclick='$("#popup_consulta").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
    </div>
</div>

<div id="popup_ost" title="Consulta Ordem de Serviço"  style="overflow: hidden;">
    
    <div style="overflow-x: hidden; max-height: 400px; overflow-y: scroll;">
        <table class="table table-bordered table-striped table-hover" style="font-size: 80%;">
            <thead>
                <tr>
                    <th>Número</th>
                    <th>Resultado</th>
                    <th>Status</th>
                    <th>Data/Hora</th>
                    <th>CPF</th>
                    <th>Servidor</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody id="5_tbo_st">
                
            </tbody>
        </table>
    </div>
    
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-info" id="3_btn_sair" onclick='$("#popup_ost").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
    </div>    

    <form name="5_form2" id="5_form2" action="#" method="POST">
        <input type="hidden" name="operacao" id="5_operacao2" value="ordem_servico" />
        <input type="hidden" name="CodRegistro" id="5_CodRegistro2" value="" />
    </form>
    
</div>