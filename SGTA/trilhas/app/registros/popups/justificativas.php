<script src="popups/justificativas.js" type="text/javascript"></script>


<div id="popup_justificativas" title="Histórico de Justificativas/Pareceres." style="overflow: hidden;" >
    
    <div class="table-responsive" style="overflow-x: hidden; max-height: 400px; overflow-y: scroll;">
        <table class="table table-bordered table-striped" style="font-size: 80%;">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Tipo</th>
                    <th>Justificativa/Parecer</th>
                    <th>Usuário</th>
                    <th>Contato</th>
                </tr>
            </thead>
            <tbody id="tb_justificativas">
                
            </tbody>
        </table>
    </div>
    
    <form name="6_form1" id="6_form1" action="#" method="POST">
        <input type="hidden" name="CodRegistro" id="6_CodRegistro" value="" />
        <input type="hidden" name="operacao" id="6_operacao" value="consulta_justificativas" />
    </form>
        
    <hr>
    
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-info" id="3_btn_sair" onclick='$("#popup_justificativas").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
    </div>
</div>