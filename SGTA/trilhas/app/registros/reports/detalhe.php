<?php
    ob_start()
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Detalhes do Registro de Inconsistência</title>
        
        <style>
            body{
                font-family: arial;
            }
            div.especial{
                border-width: 0px !important; 
            }
            .imprimir{
                display: none;
            }
            table{
                border-collapse: collapse; 
                width: 100%;
            }
            
            th{
                padding: 5px;
                background-color:  silver;
                font-size: 11px;
            }
            
            td{
                padding: 5px;
                font-size: 14px;
                text-align: left;
            }
            
            .mytable tr td{
                font-size: 12px;
                border-color: black;
                border-style: solid;
                border-width: 1px;
                text-align: justify;
            }
            .mytable th{
                border-color: black;
                border-style: solid;
                border-width: 1px;
            }
        </style>
    </head>
    <body>
        
        <htmlpagefooter name="myFooter">
            <hr style="margin-bottom: 1px;">
            <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; 
             font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">
                <tr>
                    <td width="33%" style=" border-width: 0px; border-color:  #FFFFFF; border-style:  none; text-align: left;"><span style="font-weight: bold; font-style: italic;">DETALHES DO REGISTRO</span></td>
                    <td width="33%" align="center" style="font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">Página: {PAGENO} de {nbpg}.</td>
                    <td width="33%" style="text-align: right;  border-width: 0px; border-color:  #FFFFFF; border-style:  none;"><?php echo date("d-m-Y H:i:s") ?></td>
                </tr>
            </table>
        </htmlpagefooter>
    
        <sethtmlpagefooter name="myFooter" page="1" value="on" show-this-page="1"></sethtmlpagefooter>
        
        <table border="0" style="width: 100%;">
            <tr>
                <td style="width: 33%;">
                    <img src="../../../images/logo_cge.png" width="230" style="float: right; display: block;" />
                </td>
                <!--
                <td style="width: 34%; text-align: center;">
                    <img src="../../../images/logo_uf_1.JPG" width="150" height="80" /> 
                </td>
                -->
                <td style="width: 33%; text-align: right;">
                    <img src="../../../images/logo tce sc-01.jpg" width="230"/>
                </td>
            </tr>
        </table>
        
        
        <?php

            if(!isset($_POST['CodRegistro'])){
                echo 'Operação inválida.';
                goto Fim; 
            }

            $CodRegistro = $_POST['CodRegistro'];
            
            $__servidor = true;
$operacao = "";
require_once("../../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
            require_once("../../../code/funcoes.php");
            
            require_once("../../../obj/registros.php");
            $obj_registros = new registros();
            
            require_once '../../../obj/usuario.php';
            $objusu = new usuario();
            
            require_once '../../../obj/conexao.php';
            $objcon = new conexao();
            
            require_once("../../../obj/justificativas.php");
            $obj_justificativas = new justificativas();
            
            require_once("../../../obj/andamentos.php");            
            $obj_andamentos = new andamentos();
            
            $andamentos = $obj_andamentos->consultar($CodRegistro);
            if($obj_andamentos->erro != ""){
                $array['erro'] = $obj_andamentos->erro;
                retorno;
            }
            
            $ocorrencias = "";
            
            if($andamentos[0]['qtd'] > 0){
                for ($index = 0; $index < count($andamentos); $index++){
                    $ocorrencias = $ocorrencias . 
                        '<tr>
                            <td>'.$andamentos[$index]['DataAndamento'].'</td>
                            <td>'.$andamentos[$index]['Descricao'].'</td>
                            <td>'.$andamentos[$index]['UsuarioNome'].'</td>
                            <td>'.$andamentos[$index]['Contato'].'</td>
                        </tr>';
                }
            }
            
            $observacoes = $obj_andamentos->consultar_observacoes($CodRegistro);
            if($obj_andamentos->erro != ""){
                $array['erro'] = $obj_andamentos->erro;
                retorno;
            }
            
            
            
            $obscge = '';
            
            if($observacoes[0]['qtd'] > 0){
                for ($index = 0; $index < count($observacoes); $index++){
                        
                    if($observacoes[$index]['StatusObservacao'] == 'público' || $index == 0){
                        $obscge = $obscge . 
                            '<tr>
                                <td>'.$observacoes[$index]['Data'].'</td>
                                <td>'.str_replace("\r", '<br>', $observacoes[$index]['ObsCGE']).'</td>
                                <td>'.$observacoes[$index]['Nome'].'</td>
                                <td>'.$observacoes[$index]['Contato'].'</td>
                            </tr>';
                    }
                        
                }
            }
            
            
            
            

            $anexos = $obj_registros->listar_anexos($CodRegistro);
            if($obj_registros->erro != ""){
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            $array['anexos'] = "";
            
            if($anexos[0]['qtd'] > 0){
                for ($index = 0; $index < count($anexos); $index++){
                    if($anexos[$index]["url"] != ""){
                        
                        $nome = $objusu->consulta_nome($anexos[$index]['Usuario']);
                        
                        $array['anexos'] = $array['anexos'] .
                            '<tr>
                                <td>
                                    <a href="'.$anexos[$index]['url'].'" target="_blank">'.$anexos[$index]['NomeAnexo'].'</a>    
                                </td>
                                <td>'.$anexos[$index]['DescricaoAnexo'].'</td>
                                <td>'.$nome.'</td>
                                <td>'.$anexos[$index]['Etapa'].'</td>
                                <td>'.data_br($anexos[$index]['Horario']).'</td>
                            </tr>';
                    }
                }
            }
            
            $row = $obj_justificativas->consultar_justificativas($CodRegistro);
            if($obj_justificativas->erro != ""){
                $array['erro'] = $obj_justificativas->erro;
                retorno();
            }
            
            $html = "";
            
            if($row[0]['qtd'] > 0){
                for ($index = 0; $index < count($row); $index++){
                    $html = $html .
                        '<tr>
                            <td>'.$row[$index]['DataJustificativa'].'</td>
                            <td>'.$row[$index]['TipoJustificativa'].'</td>
                            <td>'.$row[$index]['Justificativa'].'</td>
                            <td>'.$row[$index]['UsuarioNome'].'</td>
                            <td>'.$row[$index]['Contato'].'</td>
                        </tr>';
                }
            }
            
            $array['html'] = $html;
            
            $sql = "Select * From view_registros Where CodRegistro = $CodRegistro";
            $query = $objcon->select($sql);
            if($objcon->erro != ""){
                echo $objcon->erro;
                goto Fim;
            }
            $row = mysqli_fetch_array($query);
            
            $status = $row['StatusRegistro'];
            
            switch ($status){
                case "pendente":
                    $row['StatusRegistro'] = "<span style='color: darkorange; font-weight: bold;'>".$row['StatusRegistro']."</span>";
                    break;
                case "em justificativa":
                    $row['StatusRegistro'] = "<span style='color: darkblue; font-weight: bold;'>".$row['StatusRegistro']."</span>";
                    break;
                case "justificado":
                    $row['StatusRegistro'] = "<span style='color: darkgreen; font-weight: bold;'>".$row['StatusRegistro']."</span>";
                    break;
                case "em análise":
                    $row['StatusRegistro'] = "<span style='color: blue; font-weight: bold;'>".$row['StatusRegistro']."</span>";
                    break;
                case "concluído":
                    $row['StatusRegistro'] = "<span style='color: magenta; font-weight: bold;'>".$row['StatusRegistro']."</span>";
                    break;
                case "devolvido":
                    $row['StatusRegistro'] = "<span style='color: red; font-weight: bold;'>".$row['StatusRegistro']."</span>";
                    break;
            }
            
            $resultadofinal = "";
            
            if($row['QuarentenaAte'] != ""){
                $resultadofinal = "Em Monitoramento até: ".  data_br($row['QuarentenaAte']);
            }else if($row['NumProcAuditoria'] != "" && $row['TipoProcesso'] == "auditoria"){
                $resultadofinal = "Auditoria. Processo: ".$row['NumProcAuditoria'];
            }else if($row['NumProcAuditoria'] != "" && $row['TipoProcesso'] == "inspeção"){
                $resultadofinal = "Inspeção. Processo: ".$row['NumProcAuditoria'];
            }else if($row['SolicitadoAuditoria'] == true){
                $resultadofinal = "Recomendado Abertura de Processo de Inspeção/Auditoria.";
            }else if($row['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>"){
                $resultadofinal = "Arquivado";
            }
            
            if($row['DescricaoParecer'] == "Não Concordo"){
                $row['DescricaoParecer'] = "<span style='color: red;'>Não Concordo</span>";
            }
            
            
            $ResultadoAuditoria = explode(";",$row['ResultadoAuditoria']);
            
            $expedicao = explode("=", $ResultadoAuditoria[0]);
            if($expedicao[1] == "0"){
                $exp = "Não";
            }elseif($expedicao[1] == "1"){
                $exp = "Sim, Atendido pelo Órgão";
            }else{
                if($row['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>" && $row['QuarentenaAte'] == ""){
                    $exp = "Sim, Não Atendido pelo Órgão";
                }else{
                    $exp = "Sim, Aguardando o Atendimento";
                }
            }
            $exp = $exp .". ". $row['InstrumentosLegaisDesc'];
             
            $sistemas = explode("=", $ResultadoAuditoria[1]);
            if($sistemas[1] == "0"){
                $sis = "Não";
            }elseif($sistemas[1] == "1"){
                $sis = "Sim, Atendido pelo Órgão";
            }else{
                if($row['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>" && $row['QuarentenaAte'] == ""){
                    $sis = "Sim, Não Atendido pelo Órgão";
                }else{
                    $sis = "Sim, Aguardando o Atendimento";
                }
            }
            $sis = $sis .". ". $row['SistemasGerenciaisDesc'];
            
            $capacitacao = explode("=", $ResultadoAuditoria[2]);
            if($capacitacao[1] == "0"){
                $cap = "Não";
            }elseif($capacitacao[1] == "1"){
                $cap = "Sim, Atendido pelo Órgão";
            }else{
                if($row['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>" && $row['QuarentenaAte'] == ""){
                    $cap = "Sim, Não Atendido pelo Órgão";
                }else{
                    $cap = "Sim, Aguardando o Atendimento";
                }
            }
            $cap = $cap .". ". $row['CapacitacaoDesc'];
            
            $devolucao = explode("=", $ResultadoAuditoria[3]);
            if($devolucao[1] == "0"){
                $dev = "Não.";
            }elseif($devolucao[1] == "1"){
                $dev = "Sim, Atendido pelo Órgão.";
            }else{
                if($row['StatusRegistro'] == "<span style='color: magenta; font-weight: bold;'>concluído</span>" && $row['QuarentenaAte'] == ""){
                    $dev = "Sim, Não Atendido pelo Órgão.";
                }else{
                    $dev = "Sim, Aguardando o Atendimento.";
                }
            }
            
            $resultado =    '<tr>
                                <td>Requerido Expedição de Instrumentos Legais:</td>
                                <td>'.$exp.'</td>
                            </tr>
                            <tr>
                                <td>Requerido Criação/Reformulação de Sistemas:</td>
                                <td>'.$sis.'</td>
                            </tr>
                            <tr>
                                <td>Requerido Capacitação Técnica dos Agentes:</td>
                                <td>'.$cap.'</td>
                            </tr>
                            <tr>
                                <td>Requerido Devolução de Numerário:</td>
                                <td>'.$dev.'</td>
                            </tr>
                            <tr>
                                <td>Valor Ressarcido:</td>
                                <td>'.numero_br($row['ValorRessarcido']).'</td>
                            </tr>
                            <tr>
                                <td>Economia Mensal:</td>
                                <td>'.numero_br($row['EconomiaMensal']).'</td>
                            </tr>';
            
            if($resultadofinal == ""){
                $resultado = "";
            }
            
        ?>
        
        <br>
        
        <h2 style="text-align: center; line-height: 150%;">SISTEMA DE GESTÃO DE TRILHAS DE AUDITORIA<br>Detalhes do Registro de Inconsistência</h2>
        
        <table border = "0">
            <tr>
                <td style="width: 30%; font-weight: bold; border-top:1pt solid silver;">Id Trilha:</td>
                <td style="border-top:1pt solid silver;"><?php echo $row['CodTrilha']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Nome da Trilha:</td>
                <td><?php echo $row['NomeTrilha']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Órgão/Unidade Adm.:</td>
                <td><?php echo $row['NomeOrgao']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">CPF/CNPJ/Processo:</td>
                <td><?php echo $row['CPF_CNPJ_Proc']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Nome/Descrição:</td>
                <td><?php echo $row['Nome_Descricao']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Questionamentos do TCE:</td>
                <td style="text-align: justify;"><?php echo str_replace("\r", '<br>', $row['ObsCGE']); ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border-bottom: 1pt solid silver;">Status da Inconsistência:</td>
                <td style="border-bottom:1pt solid silver;"><?php echo $row['StatusRegistro']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Justificado Por:</td>
                <td><?php echo $objusu->consulta_nome($row['UsuarioOrgao']); ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Data da Justificativa:</td>
                <td><?php echo data_br($row['DataConclusaoJustificativa']); ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Parecer:</td>
                <td><?php echo $row['DescricaoParecer']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border-bottom: 1pt solid silver;">Valor a Devolver:</td>
                <td style="border-bottom:1pt solid silver;"><?php echo numero_br($row['ValorADevolver']); ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Analisado Por:</td>
                <td><?php echo $objusu->consulta_nome($row['UsuarioCGE']); ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Data Análise:</td>
                <td><?php echo data_br($row['DataConclusaoAnalise']); ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Resultado:</td>
                <td><?php echo $resultadofinal; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Favorecido:</td>
                <td><?php echo $row['Favorecido']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; vertical-align: top; border-bottom:1pt solid silver;">Detalhamento do Resultado:</td>
                <td style="border-bottom:1pt solid silver;">
                    <table class="mytable">
                        <tbody>
                            <?php echo $resultado; ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        
        <br>
        <h3>Justificativas e Pareceres</h3>
        
        <table class="mytable">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Tipo</th>
                    <th>Justificativa/Parecer</th>
                    <th>Usuário</th>
                    <th>Contato</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $html; ?>
            </tbody>
        </table>
        
        <br>
        <h3>Observações do TCE</h3>
        
        <table class="mytable">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Observação</th>
                    <th>Usuário</th>
                    <th>Contato</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $obscge; ?>
            </tbody>
        </table>
        
        <br>
        <h3>Anexos do Registro</h3>
        
        <table class="mytable">
            <thead>
                <tr>
                    <th>Nome Anexo</th>
                    <th>Descrição</th>
                    <th>Usuário</th>
                    <th>Etapa</th>
                    <th>Data Upload</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $array['anexos']; ?>
            </tbody>
        </table>
        
        <br>
        <h3>Eventos</h3>
        
        <table class="mytable">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Evento</th>
                    <th>Usuário</th>
                    <th>Contato</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $ocorrencias; ?>
            </tbody>
        </table>
        
        <br>
        FIM DO RELATÓRIO.
    </body>
</html>

<?php
    Fim:
        
    $formato = 'A4'; //A4 para retrato e A4-L para paisagem
    $margin_left = 15;
    $margin_right = 10;
    $margin_top = 15;
    $margin_bottom = 15;
    $margin_header = 11;
    $margin_footer = 7;
    
    $html = ob_get_clean();
    define('MPDF_PATCH', '../../../plugin/mpdf60/');        
    include(MPDF_PATCH.'mpdf.php');
    $mpdf = new mPdf('',$formato,'','',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,'');
    $mpdf->allow_charset_conversion=true;
    $mpdf->charset_in='UTF-8';
    $mpdf->WriteHTML($html);
    //$mpdf->setFooter('Página: {PAGENO} de {nbpg}.');  
    //$mpdf->Output();
    //andre trocar D pelo I para ir para a pagina
    $mpdf->Output("Detalhes do Registro de Inconsistência.pdf","I");
    exit();
 
