<?php

    require_once '../../../code/funcoes.php';
    require_once '../../../plugin/PHPExcel.php';
    require_once '../../../plugin/PHPExcel/Writer/Excel2007.php';
    
    require_once '../../../obj/conexao.php';
    $con = new conexao();
    
    session_start();
    $CodPerfil = $_SESSION['sessao_perfil'];
    if($CodPerfil != 1){
        echo 'OPERAÇÃO INVÁLIDA';
        die();
    }        
    
    if(!isset($_POST['CodTrilha'])){
        echo 'OPERAÇÃO INVÁLIDA';
        die();
    }
    
    $CodTrilha = $_POST['CodTrilha'];
    
    $sql = "Select * From view_mensagens_trilha Where CodTrilha = $CodTrilha Order By CPF_CNPJ_Proc";
    $query = $con->select($sql);
    if($con->erro != ""){
        echo $con->erro;
        die();
    }
    
    // Instanciamos a classe
    $objPHPExcel = new PHPExcel();

    // Definimos o estilo da fonte
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
    $objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getFont()->setBold(true);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    
    
    

    // Criamos as colunas
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Mensagens constantes na trilha de ID: '.$CodTrilha.'.' )
                ->setCellValue('A3', 'Identificador' )
                ->setCellValue('B3', "Nome/Descrição" )
                ->setCellValue("C3", "Sigla Órgão" )
                ->setCellValue("D3", "Mensagem de" )
                ->setCellValue("E3", "Texto da Mensagem" )
                ->setCellValue("F3", "Data da Mensagem" );
    
    $objPHPExcel->getActiveSheet()->setTitle('Mensagens');
    

    $linha = 4;
    
    $NomeTrilha = "";
    
    $linhas = mysqli_num_rows($query);
    if($linhas > 0){
        while ($campo = mysqli_fetch_array($query)){
            
            $NomeTrilha = $campo['NomeTrilha'];
            
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A". $linha, $campo['CPF_CNPJ_Proc'])
                ->setCellValue("B". $linha, $campo['Nome_Descricao'])
                ->setCellValue("C". $linha, $campo['SiglaOrgao'])
                ->setCellValue("D". $linha, $campo['Nome'])
                ->setCellValue("E". $linha, $campo['Mensagem'])
                ->setCellValue("F". $linha, data_br($campo['DataMensagem']));

            $linha++;
        }
    } 
                
    // Cabeçalho do arquivo para ele baixar
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Mensagens constantes na trilha '.$NomeTrilha.', ID: '.$CodTrilha.' .xlsx"');
    header('Cache-Control: max-age=0');
    // Se for o IE9, isso talvez seja necessário
    header('Cache-Control: max-age=1');

    // Acessamos o 'Writer' para poder salvar o arquivo
    //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

    // Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
    $objWriter->save('php://output'); 

    exit;