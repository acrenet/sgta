 var plugin = "det001_agente_falecido_folha_pagamento";

var destino = plugin + "/server.php";
var execute_cod = 0;
var tempopassado = 0;
var rowindex = -1;
var rowdata = "";
var area1;

$(document).ready(function(){ 
    $( document ).tooltip({
        track: true,
        tooltipClass: "custom-tooltip-styling"
    });
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
    popup_form_1();
    
    setInterval(function(){ 
        tempopassado++;
        if(tempopassado > 5){
            console.log("Atualizado em Segundo Plano.");
            execute_cod = 2;
            execute();
        } 
    }, 60000);
    $("#detalhe_table").stickyTableHeaders();
});

function popup_form_1(){
    $("#popup_detalhe").dialog({
        autoOpen: false,
        resizable: true,
        modal: true,
        width: 'auto',
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        },
        open: function( event, ui ) {
            rolar_para("#registro_detalhe");
        }
    });
}

function rolar_para(elemento) {
    $('html, body').animate({
      scrollTop: $(elemento).offset().top-500
    }, 300);
}

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                $("#trilha").val(retorno.info[0]["CodTrilha"] + " - " + retorno.info[0]["NomeTrilha"]);
                $("#criterios").val(retorno.info[0]["Criterios"]);
                $("#dtini").val(retorno.info[0]["DataInicial"]);
                $("#dtter").val(retorno.info[0]["DataFinal"]);
                $("#status").val(retorno.info[0]["StatusTrilha"]);
                $("#totalreg").val(retorno.qtdregistros);
                $("#rega").val(retorno.justificados);
                $("#regp").val(retorno.pendentes);
                $("#tb_anexos").html(retorno.anexos);
                
                $("#economiagerada").val(retorno.economiagerada);
                $("#economiapotencial").val(retorno.economiapotencial);
                
                $("#tb_mensagem_trilha").html(retorno.body_mensagem);
                
                if(retorno.acao == "consulta"){
                    $("#titulo").html("Trilha - Consulta");   
                }else if(retorno.acao == "pré-análise"){
                    $("#titulo").html("Trilha - Homologação");
                }else if(retorno.acao == "análise"){
                    $("#titulo").html("Trilha - Justificar Inconsistências");
                }else if(retorno.acao == "validação"){
                    $("#titulo").html("Trilha - Analisar Justificativas");
                }
                
                if(retorno.CodPerfil <= 2){
                    $("#div_conteudo").hide();
                    $("#div_editor").show();
                    $('#editor').val(retorno.info[0]["Instrucao"]);
                    if(typeof(area1) == "undefined"){
                        area1 = new nicEditor({
                            buttonList : ['save','print','bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','fontFormat','indent','outdent','image','upload','link','unlink','forecolor','bgcolor'],
                            fullPanel : true,
                            onSave : function(content, id, instance) {
                                        salvar_instrucao();
                                      },
                            onPrint : function(content, id, instance) {
                                        imprimir_instrucao();
                                      }
                        }).panelInstance('editor');
                    }
                }else{
                    $("#div_conteudo").html(retorno.info[0]["Instrucao"]);
                }
                
                $("#tbl_trilha").html(retorno.tabela);
                
                var ordem = retorno.ordem;   
                var data = retorno.data;
                var colunas = retorno.colunas;
                
                if(typeof(minha_tabela) != "undefined"){
                    minha_tabela.destroy();
                }
                
                datatable_("tbl_trilha", retorno.colunas, ordem, -1, data);
                
                if($("#filtro").val() != ""){
                    filtrar_tabela("tbl_trilha", $("#filtro").val());
                }
                
                tempopassado = 0;
                break;
            case "update":
                $("#totalreg").val(retorno.qtdregistros);
                $("#rega").val(retorno.justificados);
                $("#regp").val(retorno.pendentes);
                $("#tb_anexos").html(retorno.anexos);
                
                $("#economiagerada").val(retorno.economiagerada);
                $("#economiapotencial").val(retorno.economiapotencial);
                
                $("#tb_mensagem_trilha").html(retorno.body_mensagem);
                
                n = retorno.update.length;
                for (i = 0; i < n; i++) {
                    minha_tabela
                        .row( i )
                        .data( retorno.update[i] );
                }
                
                minha_tabela.columns.adjust();
                minha_tabela.draw();
                tempopassado = 0;
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

var arquivoxls = "indefinido.xls";

function resposta_up(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "detalhes":
                $("#tb_detalhe").html(retorno.detalhe);
                $("#registro_detalhe").html(retorno.header);
                $("#table_caption").html(retorno.caption);
                $(".tb_header").html(retorno.thead);
                arquivoxls = retorno.arquivoxls;
                $('#popup_detalhe').dialog('open');
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../inicio/inicio.php");
            break;
        case 2:
            $("#operacao").val("update");
            formdata = $("#form1").serialize();
            submit_form(destino);
            break;
        case 3:
           
            $("#operacao").val("load");
            formdata = $("#form1").serialize();
            submit_form(destino);
            break;
    }
    execute_cod = 0;
}

function detalhes(cpf,detalhe){
    var data = new FormData();
    data.append("cpf",cpf);
    data.append("operacao","detalhes");
    data.append("detalhe",detalhe);
    formdata = data;
    submit_file(destino);
    return false;
}

function exportar_tabela(nometabela){
    $("#" + nometabela).battatech_excelexport({
        containerid: nometabela
        , datatype: 'table'
        , filename: arquivoxls
    });
}


function server(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "observacao_cge":
                retorno_info(retorno);
                break;
            case "gravar_observacao_cge":
                retorno_observacao_cge(retorno);
                break;
            case "listar_anexos":
                retorno_visualizar_anexos(retorno);
                break;
            case "upload":
                retorno_subir_arquivo(retorno);
                break;
            case "upload_trilha":
                retorno_subir_arquivo(retorno);
                break;
            case "upload_trilha_continuo":
                retorno_subir_arquivo_continuo(retorno);
                break;
            case "justificar_inconsistencia":
                retorno_justificar_inconsistencia(retorno);
                break;
            case "consultar_justificativa":
                retorno_consultar_justificativa(retorno);
                break;
            case "concluir_analise":
                retorno_analise(retorno);
                break;
            case "salvar_analise":
                retorno_analise(retorno);
                break;    
            case "recusar_trilha":
                execute_cod = 1;
                $("#msg_sucesso").html("Trilha recusada com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "enviar_trilha":
                execute_cod = 1;
                
                var msg = "Trilha distribuída aos órgãos com sucesso.<br>";
                
                if(retorno.erro_email != ""){
                    msg = msg + retorno.erro_email;
                }
                
                if(retorno.alerta_email != ""){
                    msg = msg + retorno.alerta_email;
                }
                
                msg = msg + retorno.mensagem_email;
                
                $("#msg_sucesso").html(msg);
                $("#popup_sucesso").dialog("open");
                break;
            case "enviar_registros":
                    execute_cod = 1;
                    
                    var msg = "Registros distribuídos aos órgãos com sucesso.<br>";
                    
                    if(retorno.erro_email != ""){
                        msg = msg + retorno.erro_email;
                    }
                    
                    if(retorno.alerta_email != ""){
                        msg = msg + retorno.alerta_email;
                    }
                    
                    msg = msg + retorno.mensagem_email;
                    
                    $("#msg_sucesso").html(msg);
                    $("#popup_sucesso").dialog("open");
                    break;
            case "enviar_registro":
                        execute_cod = 2;
                        
                        var msg = "Registro distribuído ao órgão com sucesso.<br>";
                        
                        if(retorno.erro_email != ""){
                            msg = msg + retorno.erro_email;
                        }
                        
                        if(retorno.alerta_email != ""){
                            msg = msg + retorno.alerta_email;
                        }
                        
                        msg = msg + retorno.mensagem_email;
                        
                        $("#msg_sucesso").html(msg);
                        $("#popup_sucesso").dialog("open");
                      //  execute();
                        break;
            case "rejeitar_registro":
                execute_cod = 2;
                $("#msg_sucesso").html("Registro Rejeitado com Sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "reincluir_registro":
                execute_cod = 2;
                $("#msg_sucesso").html("Registro Re-incluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "excluir_anexo":
                execute_cod = 2;
                $("#msg_sucesso").html("Arquivo excluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "excluir_arquivo":
                execute_cod = 2;
                $("#popup_anexos").dialog("close");
                $("#msg_sucesso").html("Arquivo excluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "analisar_justificativa":
                analisar_justificativa_retorno(retorno);
                break;
            case "salvar_validacao":
                retorno_salvar_validacao(retorno);
                break;
            case "andamento":
                consulta_andamento_retorno(retorno);
                break;
            case "consulta_justificativas":
                consulta_justificativas_retorno(retorno);
                break;
            case "solicitar_alteracao_justificativa":
                retorno_alteracao(retorno);
                break;
            case "analisar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "gravar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "aceitar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "negar_pedido_alteracao":
                retorno_alteracao(retorno);
                break;
            case "salvar_instrucao":
                retorno_salvar_instrucao();
                break;
            case "consulta_complementar":
                retorno_acoes_complementares(retorno);
                break;
            case "acoes_complementares":
                retorno_salvar_complementar(retorno);
                break;
            case "ordem_servico":
                retorno_consulta_ost(retorno);
                break;
            case "mesclar_consulta":
                retorno_mesclar(retorno);
                break;
            case "mesclar_save":
                retorno_mesclar_save(retorno);
                break;    
            case "consultar_msg":
                retorno_visualizar_mensagens(retorno);
                break;
            case "equipe_msg":
                retorno_cadastrar_msg(retorno);
                break;
            case "cadastrar_msg":
                retorno_salvar_msg(retorno);
                break;
            case "salvar_msg":
                retorno_salvar_msg(retorno);
                break;
            case "editar_msg":
                retorno_alterar_msg(retorno);
                break;
            case "excluir_msg":
                retorno_excluir_msg(retorno);
                break;
            case "consultar_dist":
                retorno_distribuir_registro(retorno);
                break;
            case "vincular_dist":
                retorno_vincular_registro(retorno);
                break;
            case "concluir_pre_analise":
                retorno_concluir_pre_analise(retorno);
                break;
            case "auditoria_consulta":
                retorno_auditoria_consulta(retorno);
                break;
            case "auditoria_pre_analise":
                retorno_auditoria_pre_analise(retorno);
                break;
            case "redistribuir_consulta":
                retorno_redistribuir_consulta(retorno);
                break;
             case "redistribuir_para":
                retorno_redistribuir_para(retorno);
                break;  
            case "salvar_mensagem_trilha":
                retorno_salvar_mensagem_trilha(retorno);
                break;
            case "alterar_mensagem_trilha":
                retorno_alterar_mensagem_trilha(retorno);
                break;
            case "excluir_mensagem_trilha":
                retorno_excluir_mensagem_trilha(retorno);
                break;
            case "mostrar_observacao":
                retorno_alterar_observacao(retorno);
                break;
            case "ocultar_observacao":
                retorno_alterar_observacao(retorno);
                break;    
            default:
                alert("js: Operação Inválida --> ");
        };
    }
}
