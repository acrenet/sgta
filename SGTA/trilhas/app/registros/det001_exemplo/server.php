<?php

$NomeTabela = "det001_exemplo";

$__servidor = true;
require_once("../../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => "",
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../../code/funcoes.php");
require_once("../../../obj/registros.php");
require_once("../../../obj/trilhas.php");
require_once("../../../obj/conexao.php");
require_once("../../../obj/usuario.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

$obj_registros = new registros();
$obj_trilhas = new trilhas();
$obj_usuario = new usuario();
$continua = false;

try {

    switch ($array["operacao"]) {
        case "update":
            
        case "load":                   //----------------------------------------------------------------------------------------------
            $CodTrilha = $_POST['CodTrilha'];
            $detalhe = $_POST['detalhe'];
            $CodOrgao = $_POST['Orgao'];
            $acao = $_POST['acao'];
            $CodPerfil = $_POST['CodPerfil'];
            
            $cpf = $_SESSION['sessao_id'];
            
            $array['CodPerfil'] = $CodPerfil;
            
            $array['acao'] = $acao;

            $obj_trilhas->consulta_trilhas(" where codTrilha =". $CodTrilha);
            if($obj_trilhas->erro != ""){
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }


            $queryTrilha = $obj_trilhas->query;

            while ($rowTrilha = mysqli_fetch_assoc($queryTrilha)){
                if($rowTrilha['continua']){
                    $continua = true;
                }
                    
            }
            
            $obj_registros->consulta_registros($CodTrilha, $detalhe, $CodOrgao);
            if($obj_registros->erro != ""){
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query = $obj_registros->query;

      

            while ($row0 = mysqli_fetch_assoc($query)){
                if($acao != "pré-análise"){
                    if($row0["RejeitadoPreAnalise"] == false){
                        $row3[] = $row0;
                    }   
                }else{
                    $row3[] = $row0;
                }

            }
            
            
            $array['data'] =    [   
                                    null, //Ordem
                                    null, //ID
                                    ["sType" => "string"], //SiglaOrgao
                                    ["sType" => "string"], //InfoA
                                    ["sType" => "string"], //InfoB
                                    ["sType" => "string"]  //InfoC

                                ];
            
            
            
            include("../load.php");
            
            $linha = 0;
            $numcolunas = 10;
            
            for ($index = 0; $index < count($row3); $index++){

                if($row3[$index]['incluir'] == true){
                    
                    $th =
                    '
                    <th style="font-size: 90%;">Ord</th>
                    <th style="font-size: 90%;">ID</th>
                    <th style="font-size: 90%;">Órgão</th>
                    <th>Informação A</th>
                    <th>Informação B</th>
                    <th>Informação C</th>
                    ';
                            
                    $td = 
                    '
                    <td style="font-size: 85%; text-align: right;">'.($linha + 1).'</td>
                    <td style="font-size: 90%;">'.$row3[$index]['CPF_CNPJ_Proc'].'</td>
                    <td style="font-size: 90%;" title="'.$row3[$index]['NomeOrgao'].'"><a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\''.$row3[$index]['SiglaOrgao'].'\')">'.$row3[$index]['SiglaOrgao'].'</a></td>    
                    <td style="font-size: 90%;">'.$row3[$index]["InfoA"].'</td>
                    <td style="font-size: 90%;">'.$row3[$index]["InfoB"].'</td>
                    <td style="font-size: 90%;">'.$row3[$index]["InfoC"].'</td>
                    ';
                    
                    $col1 = $row3[$index]['CPF_CNPJ_Proc'];
                    $col2 = '<a href="#" onclick="return filtrar_tabela(\'tbl_trilha\',\''.$row3[$index]['SiglaOrgao'].'\')">'.$row3[$index]['SiglaOrgao'].'</a>';
                    $col3 = $row3[$index]["InfoA"];
                    $col4 = $row3[$index]["InfoB"];
                    $col5 = $row3[$index]["InfoC"];
                    
                    if($acao == "consulta"){
                        
                        $array['colunas'] = $numcolunas;
                        
                        $colunas = $th .
                            '<th style="font-size: 90%;">Obs TCE (última)</th>
                            <th style="font-size: 90%;">Status</th>
                            <th style="font-size: 90%;">Flags</th>
                            <th style="font-size: 90%;">Docs</th>
                            <th style="width: 40px;">Ações</th>';
                        
                        $tab = $tab.
                            '<tr '.$row3[$index]['cor2'].'>
                                '.$td.'
                                <td style="font-size: 90%;"><span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span></td>
                                <td style="font-weight: bold; font-size: 90%;"><span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span></td>
                                <td style="text-align: right;">'.$row3[$index]['Flags'].'</td>
                                <td style="text-align: center;">'.$row3[$index]['anexos'].'</i></a></td>
                                <td>
                                    '.$row3[$index]['icone'].'
                                </td>
                            </tr>';
                        
                        $var =  [   $linha + 1, 
                                    $col1,
                                    $col2,
                                    $col3,
                                    $col4,
                                    $col5,
                                    '<span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span>',
                                    '<span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span>',
                                    $row3[$index]['Flags'],
                                    $row3[$index]['anexos'],
                                    $row3[$index]['icone']
                                ];
                        
                    }else if($acao == "pré-análise"){
                        
                        $array['colunas'] = $numcolunas;
                        
                        $colunas = $th .
                            '<th style="font-size: 90%;">Obs TCE (última)</th>
                            <th style="font-size: 90%;">Gestor Resp.</th>
                            <th style="font-size: 90%;">Status</th>
                            <th style="font-size: 90%;">Docs</th>
                            <th style="width: 75px;">Ações</th>';

                        $tab = $tab.
                            '<tr '.$row3[$index]['cor2'].'>
                                '.$td.'
                                <td id="td_obscge_'.$row3[$index]['CodRegistro'].'" style="font-size: 90%;">'.$row3[$index]['ObsCGE'].'</td>
                                <td style="font-size: 90%;">'.$row3[$index]['ReservadoPor'].'</td>    
                                <td style="font-weight: bold;"><span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span></td>
                                <td style="text-align: center;">'.$row3[$index]['anexos'].'</i></a></td>
                                <td>
                                    '.$row3[$index]['icone'].'
                                </td>
                            </tr>';
                        
                        $var =  [   $linha + 1, 
                                    $col1,
                                    $col2,
                                    $col3,
                                    $col4,
                                    $col5,
                                    $row3[$index]['ObsCGE'],
                                    $row3[$index]['ReservadoPor'],
                                    '<span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span>',
                                    $row3[$index]['anexos'],
                                    $row3[$index]['icone']
                                ];
                        
                                
                        
                    }else if($acao == "análise"){
                        
                        $array['colunas'] = $numcolunas + 1;
                        
                        $colunas = $th .
                            '<th style="font-size: 90%;">Obs TCE (última)</th>
                            <th style="font-size: 90%;">Reservado Por</th>
                            <th style="font-size: 90%;">Status</th>
                            <th style="font-size: 90%;">Flags</th>
                            <th style="font-size: 90%;">Docs</th>
                            <th style="width: 40px;">Ações</th>';

                        $tab = $tab.
                            '<tr '.$row3[$index]['cor2'].'>
                                '.$td.'
                                <td style="font-size: 90%;"><span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span></td>
                                <td style="font-weight: bold; font-size: 90%;">'.$row3[$index]['ReservadoPor'].'</td>
                                <td style="font-weight: bold;"><span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span></td>
                                <td style="text-align: right;">'.$row3[$index]['Flags'].'</td>
                                <td style="text-align: center;">'.$row3[$index]['anexos'].'</i></a></td>    
                                <td>
                                    '.$row3[$index]['icone'].'
                                </td>
                            </tr>';
                        
                        $var =  [   $linha + 1, 
                                    $col1,
                                    $col2,
                                    $col3,
                                    $col4,
                                    $col5,
                                    '<span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span>',
                                    $row3[$index]['ReservadoPor'],
                                    '<span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span>',
                                    $row3[$index]['Flags'],
                                    $row3[$index]['anexos'],
                                    $row3[$index]['icone']
                                ];
                        
                    }else if($acao == "validação"){

                        $array['colunas'] = $numcolunas + 1;
                        
                        $colunas = $th .
                            '<th style="font-size: 90%;">Obs TCE (última)</th>
                            <th style="font-size: 90%;">Resp. TCE</th>
                            <th style="font-size: 90%;">Status</th>
                            <th style="font-size: 90%;">Flags</th>
                            <th style="font-size: 90%;">Docs</th>
                            <th style="width: 60px;">Ações</th>';
                        
                        
                        
                        $tab = $tab.
                            '<tr '.$row3[$index]['cor2'].'>
                                '.$td.'
                                <td style="font-size: 90%;"><span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span></td>
                                <td style="font-weight: bold; font-size: 90%;">'.$row3[$index]['RespCGE'].'</td>
                                <td style="font-weight: bold;"><span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span></td>
                                <td style="text-align: right;">'.$row3[$index]['Flags'].'</td>
                                <td style="text-align: center;">'.$row3[$index]['anexos'].'</i></a></td>    
                                <td>
                                    '.$row3[$index]['icone'].'
                                </td>
                            </tr>';
                        
                        $var =  [   $linha + 1, 
                                    $col1,
                                    $col2,
                                    $col3,
                                    $col4,
                                    $col5,
                                    '<span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span>',
                                    $row3[$index]['RespCGE'],
                                    '<span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span>',
                                    $row3[$index]['Flags'],
                                    $row3[$index]['anexos'],
                                    $row3[$index]['icone']
                                ];
                    }
                    
                    $update[$linha] = $var;
                    
                    $linha++;
                }
            }
            
            $array['update'] = $update;
            
            $tab = '<thead>
                        <tr>
                            '.$colunas.'
                        </tr>
                    </thead>
                    <tbody>'.$tab;
            
            $tab = $tab.'</tbody>
                        <tfoot>
                            <tr>
                                '.$colunas.'
                            </tr>
                        </tfoot>';
            
            $array['tabela'] = $tab;
            
            $CodPerfil = $_SESSION['sessao_perfil'];    
            if($CodPerfil < 5){
                require_once("../../../obj/mensagens_trilha.php");
                $obj_msg = new mensagens_trilha();
                $array["body_mensagem"] = $obj_msg->gera_tabela_mensagens_trilha($CodTrilha);
                if($obj_msg->erro != ""){
                    throw new Exception($obj_msg->erro);
                }
            }else{
                $array["body_mensagem"] = "";
            }
            
            retorno();
            break;
        case "detalhes":
            $CodReg = $_POST['cpf'];
            
            $array['alerta'] = "Esta trilha é apenas de confirmação e não possui detalhes adicionais.";

            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = "server: " . $ex->getMessage();
    retorno();
}


function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}