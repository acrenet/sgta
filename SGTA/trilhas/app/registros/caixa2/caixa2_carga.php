<?php

$var[] = array(
            "coluna" => "CPF_CNPJ_Proc",
            "formato" => "decimal",
            "tamanho" => 18,
            "descrição" => ""
            );
            
$var[] = array(
            "coluna" => "Nome_Descricao",
            "formato" => "texto",
            "tamanho" => 300,
            "descrição" => ""
        );

$var[] = array(
            "coluna" => "CodOrgao",
            "formato" => "decimal",
            "tamanho" => 6,
            "descrição" => "Código do Órgão."
        );

$var[] = array(
            "coluna" => "Nome_Orgao",
            "formato" => "ignorar",
            "tamanho" => 200,
            "descrição" => "Nome do Órgão."
        );

$var[] = array(
            "coluna" => "InfoA",
            "formato" => "texto",
            "tamanho" => 100,
            "descrição" => ""
        );

$var[] = array(
            "coluna" => "InfoB",
            "formato" => "texto",
            "tamanho" => 100,
            "descrição" => ""
        );

$var[] = array(
            "coluna" => "InfoC",
            "formato" => "texto",
            "tamanho" => 100,
            "descrição" => ""
        );