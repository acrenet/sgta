<?php

$plugin = "det005_caixa2";

$var[] = array(
            "coluna" => "CPF",
            "formato" => "texto",
            "tamanho" => 45,
            "descrição" => "CPF do Servidor"
            );
            
$var[] = array(
            "coluna" => "Nome",
            "formato" => "texto",
            "tamanho" => 300,
            "descrição" => "Nome do Servidor"
        );

$var[] = array(
            "coluna" => "CodOrgao",
            "formato" => "decimal",
            "tamanho" => 6,
            "descrição" => "Código do Órgão."
        );

$var[] = array(
            "coluna" => "Nome_Orgao",
            "formato" => "ignorar",
            "tamanho" => 200,
            "descrição" => "Nome do Órgão."
        );

require_once("../../obj/conexao.php");

$objcon = new conexao();
$query = $objcon->select("describe $plugin");
while ($row = mysqli_fetch_assoc($query)){
    $esquema[] = $row;
}

for ($index = 0; $index < count($esquema); $index++){
    
    $mystring = $esquema[$index]["Type"];

    if($esquema[$index]["Field"] == "CodDetalhe" || $esquema[$index]["Field"] == "CodReg"){
        goto salto;
    }
    
    try{
        $tipo = "texto";
        $tamanho = 200;
        
        if(strpos($mystring, "varchar") !== false){
            $tipo = "texto";
            $tamanho = preg_replace("/[^0-9]/", "", $mystring);
        }elseif(strpos($mystring, "int") !== false){
            $tipo = "decimal";
            $tamanho = str_replace("int(","",$mystring);
            $tamanho = preg_replace("/[^0-9]/", "", $mystring);
        }elseif(strpos($mystring, "decimal") !== false){
            $tipo = "moeda";
            $size = str_replace("decimal(","",$mystring);
            $size = str_replace(") unsigned","",$size);
            $size = str_replace(")","",$size);
            $size = explode(",", $size);
            $tamanho = 0;
            for ($index1 = 0; $index1 < count($size); $index1++){
                $tamanho = $tamanho + preg_replace("/[^0-9]/", "", $size[$index1]);
                if($index1 == 1){
                    if($size[$index1] == 0){
                        $tipo = "decimal";
                    }
                }
            }
        }elseif(strpos($mystring, "date") !== false){
            $tipo = "data";
            $tamanho = 0;
        }else{
            $tipo = "texto";
            $tamanho = 200;
        }
    
    } catch (Exception $exc){
        //echo $exc->getTraceAsString();
    }
    
    $var[] = array(
            "coluna" => $esquema[$index]["Field"],
            "formato" => $tipo,
            "tamanho" => $tamanho,
            "descrição" => ""
        );
    
    salto:
}