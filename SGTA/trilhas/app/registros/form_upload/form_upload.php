<?php


?>

<link href="form_upload/form_upoload.css" rel="stylesheet" type="text/css"/>
<script src="form_upload/form_upload.js" type="text/javascript"></script>



<div id="form_upload" title="Upload de Arquivo" style="display:none; font-size: 80%; overflow: hidden;"">  
    <form name="form_upload1" id="form_upload1" action="#" method="POST" class="form-horizontal" style="margin: 0px; padding: 0px;">
        <div class="form-group">
            <label class="control-label col-sm-3" for="DescricaoArquivo">Descrição do arquivo:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="up_DescricaoAnexo" name="DescricaoAnexo" maxlength="300">
            </div>
        </div>
        
        
        <div class="form-group" id="div_restrito"> 
            <div class="col-sm-offset-3 col-sm-9">
                <div class="checkbox">
                    <label><input type="checkbox" id="up_Restrito" name="Restrito" checked=""> Restrito ao TCE (não será visível a outros órgãos).</label>
                </div>
            </div>
        </div>
        
        
        <div class="form-group"> 
            <div class="col-sm-offset-3 col-sm-9">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-primary btn-file"><span class="fileupload-new">Selecione o Arquivo &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>
                        <span class="fileupload-exists">Alterar &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>         <input id="uploadfile" name="uploadfile" type="file" onchange="validar_arquivo()"/></span>
                    <span class="fileupload-preview"></span>
                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                </div>
            </div>
        </div>
        <input type="hidden" name="Tipo" id="up_Tipo" value="0" />
        <input type="hidden" name="CodRegistro" id="up_CodRegistro" value="0" />
        <input type="hidden" name="rowindex" id="up_rowindex" value="0" />
        <input type="hidden" name="rowdata" id="up_rowdata" value="0" />
        <hr>
        <div style="text-align: right; font-size: 120%;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#form_upload").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
            <button type="button" class="btn btn-success" onclick='subir_arquivo();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
        </div>
    </form>
</div>

