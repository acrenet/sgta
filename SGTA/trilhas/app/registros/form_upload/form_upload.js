$(document).ready(function(){ 
    popup_form_upload();
});

function popup_form_upload(){
    $("#form_upload").dialog({
        autoOpen: false,
        resizable: false,
        width: 750, 
        modal: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

!function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)

function anexar_arquivo(cod, tipo, index){
    
    if (typeof tipo === 'undefined') { tipo = 'registro'; }
    if (typeof index === 'undefined') { index = 0; }
    
    if($("#CodPerfil").val() > 4 || tipo == 'registro'){
        $("#div_restrito").hide();
        $("#up_Restrito").prop( "checked", false );
    }else{
        $("#div_restrito").show();
        $("#up_Restrito").prop( "checked", true );
    }
    
    rowdata = minha_tabela.row(index).data();
    //console.log("row: " + rowdata);
    
    $("#up_rowindex").val(index);
    $("#up_rowdata").val(JSON.stringify(rowdata));
    
    $("#up_Tipo").val(tipo);
    $("#up_CodRegistro").val(cod);
    $("#form_upload").dialog("open");
    return false;
}

function validar_arquivo(){
    var upl = $("#uploadfile");
    var files = upl[0].files;
    var log = "";
    var erro  = false;
    var msg = "";
    var ext = "";

    for (var i = 0; i < files.length; i++){
        log = log + '"' + files[i].name + '"  ';
        if(files[i].size > 20480000){
            erro = true;
            msg = msg + "O arquivo ''" + files[i].name + "'' ultrapaça o limite de 20 megabytes.<br>";
        }

        ext = files[0].name.split('.').pop();

        var str = "txt,pdf,xls,xlsx,doc,docx,ods,odt";
        if(str.indexOf(ext) == -1){
            erro = true;
            msg = msg + "O arquivo ''" + files[i].name + "'' possui um formato não permitido.<br>São permitidos arquivos pdf, xls, xlsx, doc, docx, ods e odt.";
        }
    }

    if(erro == true){
        $("#msg_erro").html(msg);
        $("#popup_erro").dialog("open");
        return false;
    }else{
        return files.length;
    }

}

function subir_arquivo(){
    var resposta = validar_arquivo();
    if (resposta === false){
        return false;
    }else if(resposta == 0){
        $("#msg_alerta").html("Nenhum arquivo selecionado.<br>Selecione um arquivo primeiro.");
        $("#popup_alerta").dialog("open");
        return false;
    }else{

        var data = new FormData();
        jQuery.each(jQuery('#uploadfile')[0].files, function(i, file) {
            data.append('uploadfile-'+i, file);
        });
        
        if($("#up_Tipo").val() == "registro"){
            data.append("operacao","upload");
            execute_cod = 2;
        }else{
            data.append("operacao","upload_trilha");
            execute_cod = 3;
        }
        
        //alert($("#up_Restrito").is(":checked"));
        
        data.append("CodTrilha", $("#CodTrilha").val());
        data.append("acao",$("#acao").val());
        data.append("CodRegistro", $("#up_CodRegistro").val());
        data.append("DescricaoAnexo", $("#up_DescricaoAnexo").val());
        data.append("Restrito", $("#up_Restrito").is(":checked"));
        data.append("rowindex", $("#up_rowindex").val());
        data.append("rowdata", $("#up_rowdata").val());

        formdata = data;
        pergunta_ok_acao = 2;
        $("#msg_pergunta").text("Gostaria de anexar o arquivo?");
        $("#popup_pergunta").dialog("open");
    }
}

function retorno_subir_arquivo(retorno){
    
    document.getElementById("form_upload1").reset();
    $("#popup_pergunta").dialog("close");
    $("#form_upload").dialog("close");
    $("#msg_sucesso").html(retorno.resposta);
    $("#popup_sucesso").dialog("open");
    
    execute_cod = 2;
    
    execute();
}


function retorno_subir_arquivo_continuo(retorno){
    
    document.getElementById("form_upload1").reset();
    $("#popup_pergunta").dialog("close");
    $("#form_upload").dialog("close");
    $("#msg_sucesso").html(retorno.resposta);
    $("#popup_sucesso").dialog("open");
    
    execute_cod = 2;
    
    execute_continuo();
}


