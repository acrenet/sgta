<?php

$__servidor = true;
$operacao = "";
require_once("../../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../../code/funcoes.php");

require_once("../../../obj/trilhas.php");
require_once("../../../obj/orgaos.php");
require_once("../../../obj/registros.php");
require_once("../../../obj/conexao.php");


$obj_orgaos = new  orgaos();
$obj_trilhas = new trilhas();
$obj_registros = new registros();
$obj_conexao = new conexao();


if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

try {
    
    $obj_trilhas = new trilhas();
    $obj_registros = new registros();
    
    switch ($array["operacao"]) {
        case "upload":                    //--------------------------------------------------------------------------------------------------
            
            if(!isset($_FILES['uploadfile-0'])){
                $array['erro'] = "Arquivo não recebido.";
                retorno();
            }
                        
            $CodRegistro = $_POST['CodRegistro'];
            $Restrito = 0;
            $DescricaoAnexo = $_POST['DescricaoAnexo'];
            $Usuario = $_SESSION['sessao_id'];
            $Etapa = $_POST['acao'];
            $array['rowindex'] = $_POST['rowindex'];
            $rowdata = json_decode($_POST['rowdata']);
            
            $qtdcolunas = count($rowdata);
            
            if($Etapa == "consulta"){
                $obj_registros->consulta_registro($CodRegistro);
                if($obj_registros->erro != ""){
                    $array['erro'] = $obj_registros->erro;
                    retorno();
                }
                
                $query = $obj_registros->query;
               
                $row = mysqli_fetch_array($query);

                if($row["StatusRegistro"] == "em análise"){
                    $Etapa = "análise";
                }
            }
            
            $tmpFilePath = $_FILES['uploadfile-0']['tmp_name'];
            $fileName = $_FILES['uploadfile-0']['name'];
            $fileName = $fileName = sanitizeString($fileName);
            $fileName = str_replace(",","",$fileName);
            $fileName = str_replace(" ","_",$fileName);
            
            $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/registros/" . $CodRegistro;
            
            $retorno = true;
            
            $DescricaoAnexo = trim($DescricaoAnexo);
            if($DescricaoAnexo == ""){
                $array['erro'] = "É obrigatório informar a descrição do arquivo.";
                retorno();
            }
            
            $id = $obj_registros->upload_arquivo($CodRegistro, $fileName, $DescricaoAnexo, $Usuario, $Etapa);
            if($obj_registros->erro != ""){
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }                                    
            
            $targetDir = $targetDir."/".$id;
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }        
            
            $newFilePath = $targetDir . "/" . $fileName;
            
            if (file_exists($newFilePath)) {
                //unlink($newFilePath);
                $array['erro'] = "Já existe um arquivo com este nome.<br>Renomeie o arquivo.";
                retorno();
            }
            
            if(move_uploaded_file($tmpFilePath, $newFilePath) == false) {
                $array['erro'] = "Não foi possível realizar o upload do arquivo: $newFilePath.  ". print_r($_FILES);
            }
            
            $anexos = $obj_registros->listar_anexos($CodRegistro);
            if($obj_registros->erro != ""){
                $array['erro'] = $obj_registros->erro;
                retorno();
            }
            
            if($anexos[0]['qtd'] > 0){
                $celldata = $anexos[0]['qtd'].' <a href="#" title="Visualizar os arquivos anexados." onclick="return visualizar_anexos('.$CodRegistro.')"><i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i></a>';
            }else{
                $celldata = '0 <i class="fa fa-file-text-o fa-lg" aria-hidden="true" style="color: LightGrey;">';
            }
            
            $rowdata[$qtdcolunas - 2] = $celldata; //Anexos
            
            $array['rowdata'] = $rowdata;
            $array['resposta'] = "Arquivo anexado com sucesso.";

            retorno();
            break;
        case "upload_trilha":
            
            if(!isset($_FILES['uploadfile-0'])){
                $array['erro'] = "Arquivo não recebido.";
                retorno();
            }
            
            $Restrito = $_POST['Restrito'];           
            $CodTrilha = $_POST['CodRegistro'];
            $DescricaoAnexo = $_POST['DescricaoAnexo'];
            $Usuario = $_SESSION['sessao_id'];
            $Etapa = $_POST['acao'];
            
            $tmpFilePath = $_FILES['uploadfile-0']['tmp_name'];
            $fileName = $_FILES['uploadfile-0']['name'];
            $fileName = sanitizeString($fileName);
            $fileName = str_replace(",","",$fileName);
            $fileName = str_replace(" ","_",$fileName);
            
            $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/trilhas/" . $CodTrilha;
            
            $retorno = true;
            
            $DescricaoAnexo = trim($DescricaoAnexo);
            if($DescricaoAnexo == ""){
                $array['erro'] = "É obrigatório informar a descrição do arquivo.";
                retorno();
            }
            
            $id = $obj_trilhas->upload_arquivo($CodTrilha, $fileName, $DescricaoAnexo, $Usuario, $Etapa, $Restrito);
            if($obj_trilhas->erro != ""){
                $array['erro'] = $obj_trilhas->erro;
                retorno();
            }
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }                                    
            
            $targetDir = $targetDir."/".$id;
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }        
            
            $newFilePath = $targetDir . "/" . $fileName;
            
            if (file_exists($newFilePath)) {
                //unlink($newFilePath);
                $array['erro'] = "Já existe um arquivo com este nome.<br>Renomeie o arquivo.";
                retorno();
            }
            
            if(move_uploaded_file($tmpFilePath, $newFilePath) == false) {
                $array['erro'] = "Não foi possível realizar o upload do arquivo: $newFilePath.  ". print_r($_FILES);
            }
            
            $array['resposta'] = "Arquivo anexado com sucesso.";
            
            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}

function sanitizeString($str) {
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
    //$str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
    //$str = preg_replace('/[^a-z0-9]/i', '_', $str);
    //$str = preg_replace('/_+/', '_', $str); // ideia do Bacco :)
    return $str;
}


function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}