<?php
    
    $plugin = "det001_acumulo_cargo";

    $CodPerfil = $_SESSION['sessao_perfil'];
    $style="style='display:none;'";
    if($CodPerfil < 5){
        $style="";
}
    
?>
<script src="<?php echo $plugin ?>/script.js" type="text/javascript"></script>
<script src="../../js/jquery.stickytableheaders.js" type="text/javascript"></script>
<script src="../../js/jquery.battatech.excelexport.js" type="text/javascript"></script>

<div id="popup_detalhe" title="Detalhes do Registro" class="container-fluid" style="margin-bottom: 30px;">
    
    <div id="registro_detalhe" class="well well-sm" style="font-size: 80% !important;">
        
    </div>
    
    <div class="table-responsive">
        
        <table id="detalhe_table" class="table table-bordered table-striped table-hover table-condensed" style="font-size: 70%; margin-bottom: 0px;">
            <caption id="table_caption" Style="text-align: left; display: none;" class="well">
                
            </caption>
            <thead class="tb_header">
                
            </thead>
            <tbody id="tb_detalhe">
                
            </tbody>
        </table>
    </div>
    
    <div id='cruzamento_trilhas' <?php echo $style; ?>>
        <br>
        <div class="well well-sm" style="font-weight: bold;">
            <form id='form_cruzamento' class="form-inline" action="../cruzamentos/consulta_individual.php" method="POST" target="_blank">
                <div class="form-group">
                    <label for="cruzamento_campo">Cruzamento entre Trilhas - Valor:</label>
                    <input type="text" class="form-control" title="Realiza uma consulta em todas as trilhas carregadas no sistema pelo valor aqui preenchido (CPF/CNPJ/Processo ou Nome/Razão Social)." id="cruzamento_campo" name="campo">
                </div>
                <input type="hidden" name="parametro" id='cruzamento_parametro' value="" />
                <input type="hidden" name="detalhe" id='detalhe' <?php echo 'value='.$plugin ?> />
                <button type="button" onclick="cruzamento_enviar('cpf');" class="btn btn-default" title="Realiza a busca com conteúdo do campo ''Valor'', preencher somente com números.">Consultar CPF/CNPJ/Processo</button>
                <button type="button" onclick="cruzamento_enviar('nome');" class="btn btn-default" title="Realiza a busca inteligente com conteúdo do campo ''Valor'' que pode ser parte no Nome/Razão Social, não diferencia maiúsculas e acentos.">Consultar Nome/Razão Social</button>
            </form>
        </div>
    </div>
    
        <hr>
        <div style="text-align: right; ">
            <button id="btnExport" class="btn btn-success" onclick="exportar_tabela('detalhe_table');"><span class="fa fa-file-excel-o fa-lg"></span> Exportar para o Excel</button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-info" onclick='$("#popup_detalhe").dialog("close");'>Sair &nbsp;<span class="fa fa-sign-out"></span></button>
        </div>
</div>


