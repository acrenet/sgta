<?php
//Substituir pelo nome da tabela no banco de dados
$plugin = "det001_falecido_folha_pagamento";

$__servidor = true;
require_once("../../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => "",
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../../code/funcoes.php");
require_once("../../../obj/registros.php");
require_once("../../../obj/trilhas.php");
require_once("../../../obj/conexao.php");
require_once("../../../obj/usuario.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

$obj_registros = new registros();
$obj_trilhas = new trilhas();
$obj_usuario = new usuario();

try {
    

    switch ($array["operacao"]) {
        case "update":
            
        case "load":                   //----------------------------------------------------------------------------------------------
            $CodTrilha = $_POST['CodTrilha'];
            $detalhe = $_POST['detalhe'];
            $CodOrgao = $_POST['Orgao'];
            $acao = $_POST['acao'];
            $CodPerfil = $_POST['CodPerfil'];
            $cpf = $_SESSION['sessao_id'];
            
            $array['CodPerfil'] = $CodPerfil;
            $array['acao'] = $acao;
            
            $obj_registros->consulta_registros($CodTrilha, $detalhe, $CodOrgao);
            if($obj_registros->erro != ""){
                $array['erro'] = $obj_registros->erro;
                retorno();
            }

            $query = $obj_registros->query;

            while ($row0 = mysqli_fetch_assoc($query)){
                if(($acao == "pré-análise" || $acao == "consulta") && $CodPerfil < 5){
                    $row3[] = $row0;
                }else{
                    if($row0["RejeitadoPreAnalise"] == false){
                        $row3[] = $row0;
                    }
                }
            }
            
            $array['economiapotencial'] = 0;
            
            
            
        // *************** INICIO DA PARTE EDITÁVEL *****************
            
            /*
            * Aqui indicamos qual coluna queremos se seja classificada na tabela principal (ver Anexo 1 do manual).
            * Atenção: a base é zero, assim se quisermos classificar a segunda coluna então utilizamos [1, "asc" ]
            * Para classificar em órdem decrescente utilize "desc" em vez de "asc".
            */
            $ordem_classificacao[] = [2, "asc" ];
            
            /*
             * A função calculos_agrupados($campo, $funcao) é utilizada para automaticamente calcular a soma, média ou contagem dos registros.
             * Lembrando que o SGTA sempre agrupa os registros por CPF/CNPJ/Processo e Unidade, assim por exemplo, na tela principal nunca é
             * exibido mais de uma vez o cpf para o mesmo órgão. Esta função serve para percorrer os registros de cada agrupamento e realizar
             * cálculos.
             * O primeiro parâmetro "$campo" é o nome do campo no banco de dados, ele não é necessário quando a função for contar.
             * O segundo parâmetro "$funcao" é o que indica o que se deseja fazer, as possibilidades são: contar, somar, media.
             * Ex: $ValorMedia = calculos_agrupados("salario", "media")
             * Na função acima a variável $ValorMedia recebe uma matriz contendo valor médio do campo "salário" que cada CPF recebeu em um determinado
             * órgão. O período será aquele a que a trilha se referir.
             */
            
            $Contagem = calculos_agrupados("", "contar");
            $Soma = calculos_agrupados("valorPago", "somar"); //alterar para o nome do campo desejado ou remover esta linha se não for utilizar.
                         
            //Instruções para o carregamento dos registros, não alterar.
            include("../load.php");
            
            $linha = 0;
            
            for ($index = 0; $index < count($row3); $index++){
                
                /*
                * Aqui definimos qual coluna o sistema deverá somar para exibir na tela principal a economia potencial da trilha.
                * Neste exemplo está sendo somado a coluna ValorAdjudicado.
                * Caso a trilha não possual tal informação apenas remova ou comente a linha abaixo.
                */
                
                $ColunaASomar = "valorPago";
                
                $somarcoluna = true;
                
                if($row3[$index]['RejeitadoPreAnalise'] == true){
                    $somarcoluna = false;    
                }elseif(strrpos($row3[$index]['StatusRegistro'],"concluído") !== false){
                    $somarcoluna = false;
                }

                if($somarcoluna == true){
                    if(isset($ColunaASomar)){
                        if(isset($row3[$index][$ColunaASomar])){
                            $array['economiapotencial'] = $array['economiapotencial'] + $row3[$index][$ColunaASomar];
                        }  
                    }    
                }

                if($row3[$index]['incluir'] == true){
                    
                    if(isset($colunas)){
                        unset($colunas);
                    }
                    
                    $LinhaAtual = $linha + 1;
                    $IdentificadorAgrupado = $row3[$index]["CPF_CNPJ_Proc"].$row3[$index]["CodOrgao"];
                    
                    /*
                     * Aqui recuperamos a quantidade de registros para cada CPF/CNPJ/Processo que a variável $Contagem recebeu da função calculos_agrupados("", "contar")
                     * Extraímos a quantidade da matriz utilizando a variável $IdentificadorAgrupado e colocamos na variável $QtdReg conforme abaixo.
                     */
                    $QtdReg = $Contagem[$IdentificadorAgrupado];
                    /*
                     * Aqui recuperamos a soma do campo para ValorAdjudicado para cada CPF/CNPJ/Processo que a variável $Soma recebeu da função calculos_agrupados("ValorAdjudicado", "somar")
                     * Extraímos a quantidade da matriz utilizando a variável $IdentificadorAgrupado e colocamos na variável $SomaValorAdjudicado conforme abaixo.
                     */
                    $SomaValorAdjudicado = $Soma[$IdentificadorAgrupado]; //remover esta linha caso a função de soma não tenha sido utilizada.
                    
                    /*
                     * Aqui carregamos a matriz $colunas com as informações que serão exibidas na tala principal (ver Anexo 1 do manual).
                     * Para visualizar esta trilha no SGTA acesse no menu superior Gestão\Gestão de Trilhas, e clique no botão verde em formato de olho.
                     * Utilize $row3[$index]["NomeDoCampo"] sempre que quiser inserir direto na tabela o valor de um campo do banco de dados.
                     * $colunas é uma matriz associativa, seus elementos têm a seguinte função:
                     * "cabecalho" É o nome da coluna que será exibido na tela principal
                     * "campo" é o nome da variável ou do campo no banco de dados que contém a informação.
                     * "tipo" contém o tipo do dado que foi preenchido em "campo", pode ser "texto", "número" ou "data". O SGTA utiliza para a correta classificação.
                     * "estilo" qualquer elemento css para formatar a célula da tabela onde será exibido a informação.
                     * "função" utilizado para formatar os campos numéricos e de data para o padrão brasileiro, aceita os seguintes parâmetros:
                     * ->"inteiro" campos numéricos sem casa decimal. Ex: 15268 => exibe 15.248
                     * ->"numero_br" campos numéricos com casa decimal Ex: 15268.25 => exibe 15.248,25
                     * ->"data_br" campos de data no formato brasileiro Ex: 2017-10-25 => exibe 25/10/2017
                     * Os elementos "estilo" e "funcao" devem ser deixados em branco se não forem ser utilizados.
                     */
                    
                    $colunas[] = ["cabecalho"=>"Ord", "campo" => $LinhaAtual, "tipo" => "número", "estilo" => 'text-align: right;', "funcao" => ""];
                    $colunas[] = ["cabecalho"=>"CPF", "campo" => $row3[$index]['CPF_CNPJ_Proc'], "tipo" => "número", "estilo" => "", "funcao" => ""];
                    $colunas[] = ["cabecalho"=>"Nome", "campo" => $row3[$index]['Nome_Descricao'], "tipo" => "texto", "estilo" => "", "funcao" => ""];
                    $colunas[] = ["cabecalho"=>"Ente", "campo" => $row3[$index]['NomeOrgao'], "tipo" => "texto", "estilo" => "", "funcao" => ""];    
					$colunas[] = ["cabecalho"=>"Unidade Gestora", "campo" => $row3[$index]['nome_unidade_gestora'], "tipo" => "texto", "estilo" => "", "funcao" => ""];      					
					$colunas[] = ["cabecalho"=>"Data Falecimento", "campo" => $row3[$index]['dataFalecimento'], "tipo" => "data", "estilo" => 'text-align: right; color: green;', "funcao" => "data_br"];
                    $colunas[] = ["cabecalho"=>"Total Pago", "campo" => $SomaValorAdjudicado, "tipo" => "número", "estilo" => 'text-align: right; color: green;', "funcao" => "numero_br"];
                    $colunas[] = ["cabecalho"=>"Ocorrências", "campo" => $QtdReg, "tipo" => "número", "estilo" => 'text-align: right; color: green;', "funcao" => ""];
                    
        // *************** TÉRMINO DA PARTE EDITÁVEL *****************
                    
                    $numcolunas = count($colunas) + 4;
                    
                    $th = '';
                    $td = '';
                    
                    for ($index1 = 0; $index1 < count($colunas); $index1++){
                        $th = $th . '<th style="color:#555555 !important; '.$colunas[$index1]['estilo'].'">'.$colunas[$index1]['cabecalho'].'</th>';
                        $td = $td . '<td style="'.$colunas[$index1]['estilo'].'">'.formata_campo($colunas[$index1]['campo'], $colunas[$index1]['funcao']).'</td>';
                    }
                    
                    if($index == 0){
                        for ($index5 = 0; $index5 < count($colunas); $index5++){
                            if($colunas[$index5]["tipo"] == "número"){
                                $colunas_tipo[] = null;
                            }elseif($colunas[$index5]["tipo"] == "data"){
                                $colunas_tipo[] = ["sType" => "date-uk"];
                            }else{
                                $colunas_tipo[] = ["sType" => "string"];
                            }
                        }
                    }
                    
                    $var = array($linha + 1);
                        
                    for ($index4 = 1; $index4 < count($colunas); $index4++){
                        array_push($var, formata_campo($colunas[$index4]['campo'], $colunas[$index4]['funcao']));
                    }
                    
                    if($acao == "consulta"){
                       
                        array_push($var, '<span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span>');
                        array_push($var, '<span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span>');
                        array_push($var, $row3[$index]['Flags']);
                        array_push($var, $row3[$index]['anexos']);
                        array_push($var, $row3[$index]['icone']);
                        
                        $array['colunas'] = $numcolunas;
                        
                        $colunas = $th .
                            '<th style="font-size: 90%;">Obs TCE (última)</th>
                            <th style="font-size: 90%;">Status</th>
                            <th style="font-size: 90%;">Flags</th>
                            <th style="font-size: 90%;">Docs</th>
                            <th style="width: 40px;">Ações</th>';
                        
                        $tab = $tab.
                            '<tr '.$row3[$index]['cor2'].'>
                                '.$td.'
                                <td style="font-size: 90%;"><span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span></td>
                                <td style="font-weight: bold; font-size: 90%;"><span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span></td>
                                <td style="text-align: right;">'.$row3[$index]['Flags'].'</td>
                                <td style="text-align: center;">'.$row3[$index]['anexos'].'</i></a></td>
                                <td>
                                    '.$row3[$index]['icone'].'
                                </td>
                            </tr>';
                        
                    }else if($acao == "pré-análise"){
                        
                        array_push($var, $row3[$index]['ObsCGE']);
                        array_push($var, $row3[$index]['ReservadoPor']);
                        array_push($var, '<span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span>');
                        array_push($var, $row3[$index]['anexos']);
                        array_push($var, $row3[$index]['icone']);

                        $array['colunas'] = $numcolunas;
                        
                        $colunas = $th .
                            '<th style="font-size: 90%;">Obs TCE (última)</th>
                            <th style="font-size: 90%;">Gestor Resp.</th>
                            <th style="font-size: 90%;">Status</th>
                            <th style="font-size: 90%;">Docs</th>
                            <th style="width: 75px;">Ações</th>';

                        $tab = $tab.
                            '<tr '.$row3[$index]['cor2'].'>
                                '.$td.'
                                <td id="td_obscge_'.$row3[$index]['CodRegistro'].'" style="font-size: 90%;">'.$row3[$index]['ObsCGE'].'</td>
                                <td style="font-size: 90%;">'.$row3[$index]['ReservadoPor'].'</td>    
                                <td style="font-weight: bold;"><span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span></td>
                                <td style="text-align: center;">'.$row3[$index]['anexos'].'</i></a></td>
                                <td>
                                    '.$row3[$index]['icone'].'
                                </td>
                            </tr>';
                        
                        
                    }else if($acao == "análise"){
                        
                        array_push($var, '<span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span>');
                        array_push($var, $row3[$index]['ReservadoPor']);
                        array_push($var, '<span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span>');
                        array_push($var, $row3[$index]['Flags']);
                        array_push($var, $row3[$index]['anexos']);
                        array_push($var, $row3[$index]['icone']);
                        
                        $array['colunas'] = $numcolunas + 1;
                        
                        $colunas = $th .
                            '<th style="font-size: 90%;">Obs TCE (última)</th>
                            <th style="font-size: 90%;">Reservado Por</th>
                            <th style="font-size: 90%;">Status</th>
                            <th style="font-size: 90%;">Flags</th>
                            <th style="font-size: 90%;">Docs</th>
                            <th style="width: 40px;">Ações</th>';

                        $tab = $tab.
                            '<tr '.$row3[$index]['cor2'].'>
                                '.$td.'
                                <td style="font-size: 90%;"><span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span></td>
                                <td style="font-weight: bold; font-size: 90%;">'.$row3[$index]['ReservadoPor'].'</td>
                                <td style="font-weight: bold;"><span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span></td>
                                <td style="text-align: right;">'.$row3[$index]['Flags'].'</td>
                                <td style="text-align: center;">'.$row3[$index]['anexos'].'</i></a></td>    
                                <td>
                                    '.$row3[$index]['icone'].'
                                </td>
                            </tr>';
                        
                    }else if($acao == "validação"){
                        
                        array_push($var, '<span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span>');
                        array_push($var, $row3[$index]['RespCGE']);
                        array_push($var, '<span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span>');
                        array_push($var, $row3[$index]['Flags']);
                        array_push($var, $row3[$index]['anexos']);
                        array_push($var, $row3[$index]['icone']);

                        $array['colunas'] = $numcolunas + 1;
                        
                        $colunas = $th .
                            '<th style="font-size: 90%;">Obs TCE (última)</th>
                            <th style="font-size: 90%;">Resp. TCE</th>
                            <th style="font-size: 90%;">Status</th>
                            <th style="font-size: 90%;">Flags</th>
                            <th style="font-size: 90%;">Docs</th>
                            <th style="width: 60px;">Ações</th>';
                        
                        $tab = $tab.
                            '<tr '.$row3[$index]['cor2'].'>
                                '.$td.'
                                <td style="font-size: 90%;"><span title="'.$row3[$index]['RespObs'].'">'.$row3[$index]['ObsCGE'].'</span></td>
                                <td style="font-weight: bold; font-size: 90%;">'.$row3[$index]['RespCGE'].'</td>
                                <td style="font-weight: bold;"><span id="st_'.$row3[$index]['CodRegistro'].'" title="'.$row3[$index]['funcionario'].'">'.$row3[$index]['StatusRegistro'].'</span></td>
                                <td style="text-align: right;">'.$row3[$index]['Flags'].'</td>
                                <td style="text-align: center;">'.$row3[$index]['anexos'].'</i></a></td>    
                                <td>
                                    '.$row3[$index]['icone'].'
                                </td>
                            </tr>';

                    }
                    
                    $update[$linha] = $var;
                    
                    $linha++;
                }
            }
            
            if(!isset($colunas_tipo)){
                $colunas_tipo[] = null;
            }
                     
            $array['data'] = $colunas_tipo;
            $array['ordem'] = $ordem_classificacao;
            $array['update'] = $update;
            $array['economiapotencial'] = numero_br($array['economiapotencial']);
            
            $tab = '<thead>
                        <tr>
                            '.$colunas.'
                        </tr>
                    </thead>
                    <tbody>'.$tab;
            
            $tab = $tab.'</tbody>
                        <tfoot>
                            <tr>
                                '.$colunas.'
                            </tr>
                        </tfoot>';
            
            $array['tabela'] = $tab;
            
            $CodPerfil = $_SESSION['sessao_perfil'];    
            if($CodPerfil < 5){
                require_once("../../../obj/mensagens_trilha.php");
                $obj_msg = new mensagens_trilha();
                $array["body_mensagem"] = $obj_msg->gera_tabela_mensagens_trilha($CodTrilha);
                if($obj_msg->erro != ""){
                    throw new Exception($obj_msg->erro);
                }
            }else{
                $array["body_mensagem"] = "";
            }
            
            retorno();
            break;
        case "detalhes":
            $CodReg = $_POST['cpf'];
            
            require_once("../../../obj/orgaos.php");
            require_once("../../../obj/trilhas.php");
            
            $objcon = new conexao();
            $obj_orgao = new orgaos();
            $obj_trilhas = new trilhas();
            
            // ********** INICIO DA PARTE EDITÁVEL **************
            
            /*
             * Para visualizar esta trilha no SGTA acesse no menu superior Gestão\Gestão de Trilhas, e clique no botão verde em formato de olho.
             * Aqui estão os ajustes para a tela de detalhamento do registro. Para acessar esta tela veja o anexo 2 do manual.
             */
            
            //Ajustar no SQL o ORDER BY para os registros sejam exibidos na tabela de detalhe classificados.
            $sql = "SELECT $plugin.*, registros.CPF_CNPJ_Proc, registros.Nome_Descricao, registros.CodOrgao, registros.CodTrilha FROM $plugin INNER JOIN registros ON $plugin.CodReg = 
                registros.CodRegistro WHERE (((registros.CodRegistro)=$CodReg)) ORDER BY $plugin.nome_unidade_gestora;";
            
            $query = $objcon->select($sql);
            if($objcon->erro != ""){
                $array['erro'] = $objcon->erro;
                retorno();
            }
            
            while ($row = mysqli_fetch_assoc($query)){
                $arr[] = $row;
            }

            $arr[0]['NomeOrgao'] = $obj_orgao->consulta_orgao($arr[0]['CodOrgao']);
            
            //Loop para realizar contagens, somas e cálculos em geral a ser exibido no cabeçalho.
            //No caso abaixo estamos totalizando pela coluna "ValorAdjudicado".
            $qtd = 0;
            $soma = 0;
            for ($index5 = 0; $index5 < count($arr); $index5++){
                $qtd++;
                $soma = $soma + $arr[$index5]["valorPago"]; //Aqui realizando a soma pela coluna ValorAdjudicado. Remover se não for necessário.
            }
            $arr[0]['Soma'] = $soma;
            
            
            /*
             * $info é a matriz do cabeçalho da tela de detalhamento do registro (ver Anexo 3 do manual).
             * Seu funcionamento é identico ao dos anteriores descritos.
             * Seus elementos são:
             * "descrição": A descrição da informação que será exibida.
             * "campo": O nome do campo no banco de dados.
             * "estilo": qualquer elemento css para formatar a célula onde será exibido a informação.
             * "função": utilizado para formatar os campos numéricos e de data para o padrão brasileiro, aceita os seguintes parâmetros:
             * ->"inteiro" campos numéricos sem casa decimal. Ex: 15268 => exibe 15.248
             * ->"numero_br" campos numéricos com casa decimal Ex: 15268.25 => exibe 15.248,25
             * ->"data_br" campos de data no formato brasileiro Ex: 2017-10-25 => exibe 25/10/2017
             * Os elementos "estilo" e "funcao" devem ser deixados em branco se não forem ser utilizados.
             */
            
            $info[] = ["descricao" => "CPF", "campo" => "CPF_CNPJ_Proc", "estilo" => "", "funcao" => ""];
            $info[] = ["descricao" => "Nome", "campo" => "Nome_Descricao", "estilo" => "", "funcao" => ""];
			$info[] = ["descricao" =>"Data Falecimento", "campo" => "dataFalecimento",  "estilo" => "", "funcao" => "data_br"];
            $info[] = ["descricao" => "Total Pago", "campo" => "Soma", "estilo" => "", "funcao" => "numero_br"];

            /*
             * $tbl é a matriz da tabela de detalhamento do registro (ver Anexo 3 do manual).
             * Seu funcionamento é identico ao dos anteriores descritos.
             * Seus elementos são:
             * "cabecalho": É o nome da coluna que será exibido na tela principal
             * "campo": do campo no banco de dados que contém a informação.
             * "estilo": qualquer elemento css para formatar a célula da tabela onde será exibido a informação.
             * "função": utilizado para formatar os campos numéricos e de data para o padrão brasileiro, aceita os seguintes parâmetros:
             * ->"inteiro" campos numéricos sem casa decimal. Ex: 15268 => exibe 15.248
             * ->"numero_br" campos numéricos com casa decimal Ex: 15268.25 => exibe 15.248,25
             * ->"data_br" campos de data no formato brasileiro Ex: 2017-10-25 => exibe 25/10/2017
             * Os elementos "estilo" e "funcao" devem ser deixados em branco se não forem ser utilizados.
             * "agrupador": nome do campo no banco de dados que deverá ser agrupado na tabela de detalhe. É obrigatório a inclusão de cláusula
             * ORDER BY com o nome do campo no SQL que faz a consulta no banco de dados (acima) para o correto agrupamento.
             * "calculado": realiza operações em "campo" conforme o "agrupador", os parâmetros aceitos são: soma, media, contar
             * Obs: calculado só funciona quando agrupador estiver definido.
             */
            $tbl[] = ["cabecalho" => "Unidade Gestora", "campo" => "nome_unidade_gestora", "estilo" => "vertical-align: middle;", "funcao" => "", "agrupador" => "nome_unidade_gestora", "calculado" => ""];
            $tbl[] = ["cabecalho" => "Período", "campo" => "periodo", "estilo" => "", "funcao" => "", "agrupador" => "", "calculado" => ""];
            $tbl[] = ["cabecalho" => "Valor", "campo" => "valorPago", "estilo" => "text-align: right; color: green;", "funcao" => "numero_br", "agrupador" => "", "calculado" => ""];
            $tbl[] = ["cabecalho" => "Total", "campo" => "valorPago", "estilo" => "text-align: right; color: green; vertical-align: middle;", "funcao" => "numero_br", "agrupador" => "nome_unidade_gestora", "calculado" => "soma"];
            
            // ********** TÉRMINO DA PARTE EDITÁVEL **************
            


            $detalhe = '';
            
            for ($index = 0; $index < count($tbl); $index++){
                if($tbl[$index]["agrupador"] != ""){
                    
                    $campos = explode(".", $tbl[$index]["agrupador"]);
                    
                    for ($index3 = 0; $index3 < count($campos); $index3++){
                        $arr2[$tbl[$index]["agrupador"]]["campo"][$index3] = $campos[$index3];
                    }
                    
                    $memoria = "";
                    for ($index2 = 0; $index2 < count($arr); $index2++){
                        
                        $agrupador = "";
                        for ($index3 = 0; $index3 < count($campos); $index3++){
                            $agrupador = $agrupador . $arr[$index2][$campos[$index3]];
                        }
                        
                        if($memoria != $agrupador){//novo valor
                            $arr2[$tbl[$index]["agrupador"]][$agrupador]["qtd"] = 1;
                        }else{
                            $arr2[$tbl[$index]["agrupador"]][$agrupador]["qtd"] ++;
                        }
                        
                        if($tbl[$index]["calculado"] != ""){ //não existe o campo na tabela
                            
                            if($tbl[$index]["calculado"] == "soma"){
                                if($memoria != $agrupador){ //não existe o campo na tabela
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["calculo"] = $arr[$index2][$tbl[$index]["campo"]];
                                }else{
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["calculo"] = $arr2[$tbl[$index]["agrupador"]][$agrupador]["calculo"] + $arr[$index2][$tbl[$index]["campo"]];
                                }
                            }elseif($tbl[$index]["calculado"] == "media"){
                                if($memoria != $agrupador){ //não existe o campo na tabela
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["_soma"] = $arr[$index2][$tbl[$index]["campo"]];
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["_cont"] = 1;
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["calculo"] = $arr[$index2][$tbl[$index]["campo"]];
                                }else{
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["_soma"] = $arr2[$tbl[$index]["agrupador"]][$agrupador]["_soma"] + $arr[$index2][$tbl[$index]["campo"]];
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["_cont"] ++;
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["calculo"] = $arr2[$tbl[$index]["agrupador"]][$agrupador]["_soma"] / $arr2[$tbl[$index]["agrupador"]][$agrupador]["_cont"];
                                }
                            }elseif($tbl[$index]["calculado"] == "contar"){
                                if($memoria != $agrupador){ //não existe o campo na tabela
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["calculo"] = 1;
                                }else{
                                    $arr2[$tbl[$index]["agrupador"]][$agrupador]["calculo"] ++;
                                }
                            }
                                
                        }
                        
                        $memoria = $agrupador;
                    }
                }     
            }
            
            $thead = '<tr  style="background-color: lightgray;">';
            for ($index = 0; $index < count($tbl); $index++){
                $thead = $thead.'<th style="'.$tbl[$index]['estilo'].' color:black !important;">'.$tbl[$index]['cabecalho'].'</th>';
            }
            $thead = $thead.'</tr>';
            
            $header = '';
            
            
            for ($index = 0; $index < count($info); $index++){
                
                $arr[0][$info[$index]['campo']] = formata_campo($arr[0][$info[$index]['campo']], $info[$index]['funcao']);
                
                $header = $header . 
                    '
                    <div class="row">
                        <div class="col-sm-3"><b>'.$info[$index]['descricao'].':</b></div>
                        <div class="col-sm-9" style="'.$info[$index]['estilo'].'">'.$arr[0][$info[$index]['campo']].'</div>
                    </div>
                    ';
            }
            
            $obj_trilhas->consulta_trilha($arr[0]['CodTrilha']);
            if($obj_trilhas->erro != ""){
                throw new Exception($obj_trilhas->erro);
            }
            $query = $obj_trilhas->query;
            $row = mysqli_fetch_array($query);
            $array['arquivoxls'] = $row['TabelaDetalhe']." - ".$arr[0]['CPF_CNPJ_Proc'].".xls";
            
            $caption =  '
                        <label style="font-size: 150%;">
                            <b>'.strtoupper($row['NomeTrilha']).'</b>
                        </label><br><br>
                        ';
            
            for ($index = 0; $index < count($info); $index++){
                $caption = $caption . 
                    '
                    <label>
                        <b>'.$info[$index]['descricao'].': &nbsp &nbsp</b></label><label>'.$arr[0][$info[$index]['campo']].'
                    </label><br>
                    ';
            }
            
            $caption = $caption . '<br>';
            
            for ($index = 0; $index < count($arr); $index++){
                
                $detalhe = $detalhe .'<tr>';
                
                for ($index2 = 0; $index2 < count($tbl); $index2++){
                    
                    if($tbl[$index2]["calculado"] != ""){
                        
                        $agrupamento = "";
                        for ($index3 = 0; $index3 < count($arr2[$tbl[$index2]['agrupador']]["campo"]); $index3++){
                            $agrupamento = $agrupamento . $arr[$index][$arr2[$tbl[$index2]['agrupador']]["campo"][$index3]];
                        }
                        
                        $arr[$index][$tbl[$index2]['campo']] = $arr2[$tbl[$index2]["agrupador"]][$agrupamento]["calculo"];
                    }
                    
                    $agrupar = false;
                    if($tbl[$index2]['agrupador'] != ""){
                        
                        $agrupamento = "";  
                        for ($index3 = 0; $index3 < count($arr2[$tbl[$index2]['agrupador']]["campo"]); $index3++){
                            $agrupamento = $agrupamento . $arr[$index][$arr2[$tbl[$index2]['agrupador']]["campo"][$index3]];
                        }
                        
                        $agrupamento_anterior = "";
                        if($index > 0){
                            for ($index3 = 0; $index3 < count($arr2[$tbl[$index2]['agrupador']]["campo"]); $index3++){
                                $agrupamento_anterior = $agrupamento_anterior . $arr[$index -1][$arr2[$tbl[$index2]['agrupador']]["campo"][$index3]];
                            }
                        }
                            
                        if($index == 0){
                            $agrupar = true;
                        }else{
                            if($agrupamento_anterior == $agrupamento){
                                $agrupar = false;
                            }else{
                                $agrupar = true;
                            }
                        }
                        
                        if ($agrupar == true){
                            
                            $arr[$index][$tbl[$index2]['campo']] = formata_campo($arr[$index][$tbl[$index2]['campo']], $tbl[$index2]['funcao']);
                            $detalhe = $detalhe.'<td style="'.$tbl[$index2]['estilo'].'" rowspan="'.$arr2[$tbl[$index2]['agrupador']][$agrupamento]["qtd"].'">'.$arr[$index][$tbl[$index2]['campo']].'</td>';
                        }
                        
                    }else{
                        
                        $arr[$index][$tbl[$index2]['campo']] = formata_campo($arr[$index][$tbl[$index2]['campo']], $tbl[$index2]['funcao']);
                        $detalhe = $detalhe.'<td style="'.$tbl[$index2]['estilo'].'">'.$arr[$index][$tbl[$index2]['campo']].'</td>';
                        
                    }
                
                }
                    
                $detalhe = $detalhe .'</tr>';    
            }    
            
            $array['header'] = $header;
            $array['detalhe'] = $detalhe;
            $array['caption'] = $caption;
            $array['thead'] = $thead;

            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = "server: " . $ex->getMessage(). " linha: " . $ex->getLine();
    retorno();
}

function calculos_agrupados($campo, $funcao){    
    try{
        global $row3, $array;
        
        $matriz = $row3;
        for ($index = 0; $index < count($matriz); $index++){
            
            
            $agrupador = $matriz[$index]["CPF_CNPJ_Proc"].$matriz[$index]["CodOrgao"];
            
            if(isset($lista_agrupadores[$agrupador])){
                switch ($funcao){
                    case "contar":

                        $lista_agrupadores[$agrupador]++;

                        break;
                    case 'somar':

                        $lista_agrupadores[$agrupador] = $lista_agrupadores[$agrupador] + $matriz[$index][$campo];

                        break;
                    case "media":

                        $lista_agrupadores[$agrupador."soma"] = $lista_agrupadores[$agrupador."soma"] + $matriz[$index][$campo];
                        $lista_agrupadores[$agrupador."qtd"]++;
                        $lista_agrupadores[$agrupador] = $lista_agrupadores[$agrupador."soma"] / $lista_agrupadores[$agrupador."qtd"];
                        break;
                }

            }else{
                switch ($funcao){
                    case "contar":

                        $lista_agrupadores[$agrupador] = 1;

                        break;
                    case 'somar':

                        $lista_agrupadores[$agrupador] = $matriz[$index][$campo];
                        break;
                    case "media":
                        $lista_agrupadores[$agrupador."soma"] = $matriz[$index][$campo];
                        $lista_agrupadores[$agrupador."qtd"] = 1;
                        $lista_agrupadores[$agrupador] = $matriz[$index][$campo];
                        break;
                }

            }

        }

        return $lista_agrupadores;
    } catch (Exception $ex){
        $array['erro'] = $ex->getMessage() . ". Linha: " . $ex->getLine();
        return "erro";
    }
}

function formata_campo($valor, $funcao){
    try{
        if($funcao <> ''){
            switch ($funcao){
                case "numero_br":
                    $valor = numero_br($valor);
                    break;
                case "data_br":
                    $valor = data_br($valor);
                    break;
                case "inteiro":
                    $valor = inteiro($valor);
                    break;
            }
        }
        return $valor;
    } catch (Exception $ex) {
        return $valor;
    }
}


function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}