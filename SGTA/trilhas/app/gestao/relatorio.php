<?php
//Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if($perfil > 4){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Relatório de Análise";
    
    if(isset($_POST["CodRelatorio"])){
        $CodRelatorio = $_POST["CodRelatorio"];
    }else{
        echo "Operação Inválida.";
        die();
    }
?>

<style>
    div.especial2{
        border-color: #ccc; 
        border-style: solid; 
        border-width: 1px; 
        border-radius: 6px; 
        -moz-border-radius: 6px; 
        -webkit-border-radius: 6px; 
        padding: 6px;
        margin-bottom: 6px;
    }
</style>

<link href="form_upoload.css" rel="stylesheet" type="text/css"/>
<script src="relatorio.js" type="text/javascript"></script>

<br>
<div class="container">
    
    <div class="well" style="font-size: 140%; font-weight: bold;">Consulta Relatório de Análise</div>
    
    <form name="form1" id="form1" method="POST" class="form-horizontal" action="/action_page.php">
        <div class="form-group">
            <label class="control-label col-sm-1" for="area">Área:</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="area" id="area" value="" disabled="disabled"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-1" for="titulo">Título:</label>
            <div class="col-sm-11">
                <input type="text" class="form-control" name="titulo" id="titulo" value="" maxlength="500" disabled="disabled"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-1" for="status">Autores:</label>
            <div class="col-sm-4">
                <div id="div_autores" class="especial2" style="background-color: #eee;">
                    
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-1" for="status">Publicado:</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="DataPublicacao" id="DataPublicacao" value="" disabled="disabled"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-1" for="editor">Relatório:</label>
            <div class="col-sm-11">
                <div id="div_conteudo" class="especial2" style=" min-height: 50px;">

                </div>
            </div>  
        </div>
        <div class="form-group"> 
            <div class="col-sm-offset-1 col-sm-11">
                <button type="button" class="btn btn-primary" onclick="imprimir();">&nbsp Imprimir &nbsp<i class="fa fa-print fa-lg"></i>&nbsp</button>
                <button type="button" class="btn btn-success" onclick="novo_upload();">&nbsp Anexar Documento &nbsp<i class="fa fa-paperclip fa-lg"></i>&nbsp</button>
                <button type="button" class="btn btn-info" onclick="nova_mensagem();">&nbsp Cadastrar Mensagem &nbsp<i class="fa fa-comment fa-lg"></i>&nbsp</button>
            </div>
        </div>
        
        <input type="hidden" name="CodRelatorio" id="CodRelatorio" value="<?php echo $CodRelatorio; ?>" />
        <input type="hidden" name="texto" id="texto" value="" />
    </form>
    <br>
    <div class="well well-sm" style="font-size: 120%; font-weight: bold;">Anexos do Relatório</div>
    
    <table class="table table-condensed table-bordered table-striped table-hover" style="font-size: 110%">
        <thead>
            <tr>
                <th>Anexo</th>
                <th>Descrição</th>
                <th>Usuário</th>
                <th>Data</th>
            </tr>
        </thead>
        <tbody id="tb_anexos">
            
        </tbody>
    </table>
    <br>
    <div class="well well-sm" style="font-size: 120%; font-weight: bold;">Mensagens do Relatório</div>
    
    <table class="table table-condensed table-bordered table-striped table-hover" style="font-size: 110%">
        <thead>
            <tr>
                <th>Autor</th>
                <th>Data</th>
                <th>Mensagem</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="tb_mensagens">
            
        </tbody>
    </table>
    
</div>

<div id="popup_upload" title="Upload de Arquivo" style="overflow: hidden; margin-bottom: 10px;" >
    <form id="form_upload" name="form_upload" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div class="form-group">
            <label class="control-label col-sm-3" for="descricao">Descrição Documento:</label>
            <div class="col-sm-9">
                <input id="descricao" name="descricao" type="text" class="form-control input-sm" value="" maxlength="300">
            </div>
        </div>
        
        <div class="form-group"> 
            <div class="col-sm-offset-3 col-sm-9">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-primary btn-file"><span class="fileupload-new">Selecione o Arquivo &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>
                        <span class="fileupload-exists">Alterar &nbsp;<i class="fa fa-hand-pointer-o fa-lg" aria-hidden="true"></i></span>         <input id="uploadfile" name="uploadfile" type="file" onchange="validar_arquivo(true)"/></span>
                    <span class="fileupload-preview"></span>
                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                </div>
            </div>
        </div>
                       
                
        
        <hr>        
    </form>
    
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick='$("#popup_upload").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        <button type="button" class="btn btn-success" onclick='subir_arquivo();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
</div>

<div id="popup_nova_mensagem" title="Cadastro de Mensagem." style="overflow: hidden;" >
    <form id="form_mensagem" name="form_mensagem" role="form" class="form-horizontal" style="font-size: 80%;" method="post">
        
        <div class="form-group" style="padding: 5px; margin-bottom: 0px;">
            <label for="msg">Mensagem:</label>
            <textarea name="msg" id="msg" rows="6" cols="20" class="form-control input-sm" onkeyup="contar_caracteres('msg', 'contador', 1000)" maxlength="1000"></textarea>
            <input type="text" class="form-control" id="contador" name="contador" style="width: 60px; float: right;" disabled="">
        </div>
        
        
        <input type="hidden" name="CodChat" id="CodChat" value="" />
        
        <hr>
        
    </form>
    
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick='$("#popup_nova_mensagem").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
        <button type="button" class="btn btn-success" onclick='salvar_mensagem();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
    </div>
</div>

<?php
    // pagemaincontent recebe o conteudo do buffer
    $pagemaincontent = ob_get_contents(); 

    // Descarta o conteudo do Buffer
    ob_end_clean(); 

    //Include com o Template
    include("../../master/master.php");
    include('../../master/datatable.php');