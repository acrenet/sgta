var execute_cod = 0;
var destino = "server.php";
var minha_tabela;
var unidade;

$(document).ready(function(){
    var data = new FormData();
    data.append("operacao","open");
    formdata = data;
    submit_file(destino);
});

var ordem = [
                [0, "asc" ]
            ];

var data = [
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "string"},
            {"sType": "date-uk"},
            {"sType": null},
            {"sType": null}
        ];
        
function resposta_up(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "open":
                $("#tb_relatorios").html(retorno.tabela);
                datatable_("tbl_relatorios", 8, ordem, -1, data, "relatórios de gestão", "relatórios de gestão");
                break;
                
            case "atualizar":
                
                minha_tabela.destroy();
                $("#tb_relatorios").html(retorno.tabela);
                datatable_("tbl_relatorios", 8, ordem, -1, data, "relatórios de gestão", "relatórios de gestão");
                break; 
                
            case "cancelar_publicacao":    
                minha_tabela.destroy();
                $("#tb_relatorios").html(retorno.tabela);
                datatable_("tbl_relatorios", 8, ordem, -1, data, "relatórios de gestão", "relatórios de gestão");
                $("#popup_pergunta").dialog("close");
                $("#msg_sucesso").html("Publcação Cancelada com Sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function atualizar(){
    var data = new FormData();
    data.append("operacao","atualizar");
    formdata = data;
    submit_file(destino);
    return false;
}

function consultar(cod){
    $("#CodRelatorio").val(cod);
    document.getElementById("form1").action = "relatorio.php";
    document.getElementById("form1").submit();
    return false;
}

function editar(cod){
    $("#CodRelatorio").val(cod);
    document.getElementById("form1").action = "cadastro.php";
    document.getElementById("form1").submit();
    return false;
}

function excluir(cod){
    var data = new FormData();
    data.append("operacao","cancelar_publicacao");
    data.append("CodRelatorio",cod);
    formdata = data;

    $("#msg_pergunta").text("Você tem certeza que gostaria de cancelar a publicação do relatório?");
    $("#popup_pergunta").dialog("open");
    return false;
}

function pergunta_ok(){
    submit_file(destino);
}

function execute(){
    
}