<?php

try{
    
    $__servidor = true;
$operacao = "";
    require_once("../../code/verificar.php");
    if($__autenticado == false){
        $array = array(
            "operacao" => $operacao,
            "erro" => "",
            "alerta" => "A sessão expirou, será necessário realizar um novo login.",
            "noticia" => "",
            "retorno" => ""
        );
        retorno();
    }
    require_once("../../code/funcoes.php");
    require_once("../../obj/gestao.php");
    
    $ObjGestao = new gestao();
    
    if(isset($_POST['operacao'])){
        $operacao = $_POST['operacao'];
    }else{
        $operacao = "indefinido";
    }
    
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "",
        "noticia" => "",
        "retorno" => ""
    );
    
    $cpf = $_SESSION['sessao_id'];
    $CodPerfil = $_SESSION['sessao_perfil'];
    
    switch ($array["operacao"]) {
        case "load":
            $CodRelatorio = $_POST['CodRelatorio'];
            
            require_once("../../obj/trilhas.php");
            $ObjTrilhas = new trilhas();
            $ObjTrilhas->consulta_tipos(-1, $cpf);
            if($ObjTrilhas->erro != ""){
                throw new Exception($ObjTrilhas->erro);
            }
            
            $query = $ObjTrilhas->query;
            
            $html = "<option value='0'></option>";
            while ($row = mysqli_fetch_array($query)){
                $html = $html.'<option value="'.$row['CodTipo'].'">'.$row['DescricaoTipo'].'</option>';
            }
            
            if($CodRelatorio == -1){
                $array['relatorio'] = "Digite alguma coisa aqui.";
            }else{
                
                $query = $ObjGestao->carrega_relatorio($CodRelatorio);
                if($ObjGestao->erro != ""){
                    throw new Exception($ObjGestao->erro);
                }
                $array["relatorio"] = mysqli_fetch_assoc($query);
                $array["relatorio"]['Texto'] = base64_decode($array["relatorio"]['Texto']);
            }
            
            $array['html'] = $html;
            
            $query = $ObjGestao->consultar_autores($CodRelatorio);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            
            $membros[] = "-1";
            while ($row = mysqli_fetch_array($query)){
                $membros[] = $row['CPF'];
            }
            
            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();
            
            $CodOrgao = $_SESSION['unidade_controle'];
            
            $obj_usuario->consulta_usuarios($CodOrgao);
            if($obj_usuario->erro != ""){
                throw new Exception($obj_usuario->erro);
            }
            
            $query = $obj_usuario->query;
            
            while ($row = mysqli_fetch_array($query)){
                if($row['CodPerfil'] == 1 && $row['Status'] == "ativo"){
                    $servidores[] = array("CPF" => $row['CPF'], "Nome" => $row['Nome']);
                }
            }
            
            $html = "";
            for ($index = 0; $index < count($servidores); $index++){
                if($html != ""){
                    $html = $html."<br>";
                }
                $incluir = false;
                for ($index1 = 0; $index1 < count($membros); $index1++){
                    if($servidores[$index]['CPF'] == $membros[$index1]){
                        $incluir = true;
                    }
                }
                if($incluir == true){
                    $html = $html.'<input type="checkbox" name="autor[]" value="'.$servidores[$index]['CPF'].'" checked="checked" /> '.$servidores[$index]['Nome'];
                }else{
                    $html = $html.'<input type="checkbox" name="autor[]" value="'.$servidores[$index]['CPF'].'" /> '.$servidores[$index]['Nome'];
                }
            }
            
            $array['autores'] = $html;
            
            retorno();
            break;
        case "publicar":
            $publicar = true;
        case "salvar":
            $CodRelatorio = $_POST['CodRelatorio'];
            $CodTipo = $_POST['CodTipo'];
            $Titulo = $_POST['Titulo'];
            $Texto = $_POST['Texto'];
            $Autor = $cpf;
            
            if(isset($_POST['autor'])){  
                $Autores = $_POST['autor'];     
            }else{
                $Autores = "";
            }
                
            if($CodTipo == 0){
                throw new Exception("Selecione uma área para o relatório na lista.");
            }
            
            if($array["operacao"] == "salvar"){
                $publicar = false;
            }else{
                if($Autores == ""){
                    throw new Exception("É necessário selecionar pelo menos um autor na lista.");
                }
                if($Titulo == ""){
                    throw new Exception("Informe um título para o relatório.");
                }
                if($Texto == "" || $Texto == "<br>"){
                    throw new Exception("É necessário preencher um conteúdo para o relatório");
                }
            }
            
            $CodRelatorio = $ObjGestao->salva_relatorio($CodRelatorio, $CodTipo, $Titulo, $Texto, $Autor, $publicar, $Autores);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            
            $array["CodRelatorio"] = $CodRelatorio;
            
            if($publicar == true){
                
                $CodOrgao = $_SESSION['unidade_controle'];
                
                $assunto = "Novo Relatório de Análise.";
                $mensagem = "Um novo relatório de análise em área de seu interesse foi publicado.<br>"
                        . "Título: $Titulo<br>"
                        . "Encontra-se disponível para consulta no SGTA na opção Relatórios/Relatórios de Análise.<br>"
                        . "Você poderá deixar mensagens e anexar documentos neste relatório.";
                
                require_once("../../obj/email.php");
                $objemail = new email();
                //andre - ver se manda para individual
                $objemail->enviarEmails($assunto, $mensagem, $CodOrgao, 2, $CodTipo,'individual'); 
                $obj_email->enviarMensagens($assunto, $mensagem, $CodOrgao, 2, $CodTipo);
            }
                
            retorno();
            break;
        case "open":
            
            gerar_tabela($CodPerfil);
            
            retorno();
            break;
        case "cancelar_publicacao":
            $CodRelatorio = $_POST['CodRelatorio'];
            
            $ObjGestao->cancelar_publicacao($CodRelatorio);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            
            gerar_tabela($CodPerfil);
            
            retorno();
            break;
        case "abrir_relatorio":    
            $CodRelatorio = $_POST['CodRelatorio'];
            
            $query = $ObjGestao->carrega_relatorio($CodRelatorio);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            
            $reg = mysqli_fetch_assoc($query);
            $row[] = $reg;
            
            $row[0]['Texto'] = base64_decode($row[0]['Texto']);
            
            $cpf = $row[0]['Autor'];
            
            require_once("../../obj/usuario.php");
            $obj_usuario = new usuario();
            $row[0]['Autor'] = $obj_usuario->consulta_nome($cpf);
            if($obj_usuario->erro != ""){
                throw new Exception($obj_usuario->erro);
            }
            
            require_once("../../obj/trilhas.php");
            $ObjTrilhas = new trilhas();
            $ObjTrilhas->consulta_tipos($row[0]["CodTipo"], -1);
            if($ObjTrilhas->erro != ""){
                throw new Exception($ObjTrilhas->erro);
            }

            $query = $ObjTrilhas->query;
            $row2 = mysqli_fetch_array($query);
            $row[0]["Area"] = $row2['DescricaoTipo'];
            
            $row[0]['DataLiberacao'] = data_br($row[0]['DataLiberacao']);
            
            $array["relatorio"] = $row;
            
            $query = $ObjGestao->consultar_autores($CodRelatorio);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            $autores = "";
            while ($row = mysqli_fetch_array($query)){
                if($autores != ""){
                    $autores = $autores."<br>";
                }
                $autores = $autores.$row['Nome'];
            }
            
            $array['autores'] = $autores;
            
            gerar_anexos($CodRelatorio);
            gerar_mensagens($CodRelatorio);
            
            retorno();
            break;
        case "upload":
            $CodRelatorio = $_POST['CodRelatorio'];
            $Descricao = $_POST['descricao'];
            
            $tmpFilePath = $_FILES['uploadfile-0']['tmp_name'];
            $fileName = $_FILES['uploadfile-0']['name'];
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            
            $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/relatorios/".$CodRelatorio;
            
            $retorno = true;
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }
            
            $fileName = str_replace(",","",$fileName);
            $fileName = str_replace(" ","_",$fileName);
            $fileName = sanitizeString($fileName);
            
            $id = $ObjGestao->subir_arquivo($CodRelatorio, $cpf, $Descricao);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            
            $retorno = true;
            
            $targetDir = $targetDir."/".$id;
            
            if (!file_exists($targetDir)) {
                $retorno = mkdir($targetDir);
            }
            
            if($retorno == false){
                $array['erro'] = "Não foi possível criar o diretório: " . $targetDir;
                retorno();
            }
            
            $newFilePath = $targetDir . "/" . $fileName;
            
            if (file_exists($newFilePath)) {
                throw new Exception("Já existe um arquivo com este nome, exclua o arquivo existente ou renomeie o arquivo.");
            }

            if(move_uploaded_file($tmpFilePath, $newFilePath) == false) {
                $array['erro'] = "Não foi possível realizar o upload do arquivo: $newFilePath.  ". print_r($_FILES);
            }
            
            gerar_anexos($CodRelatorio);
            
            try{
                if($CodPerfil != 1){
                    $CodOrgao = $_SESSION['unidade_controle'];

                    $query = $ObjGestao->carrega_relatorio($CodRelatorio);
                    
                    $row = mysqli_fetch_array($query);

                    $CodTipo = $row['CodTipo'];
                    $Titulo = $row['Titulo'];

                    $assunto = "Novo Anexo em Relatório de Análise.";
                    $mensagem = "Foi anexado um novo documento em um relatório de análise.<br>"
                            . "Título do Relatório: $Titulo<br>"
                            . "Documento: $fileName.<br>"
                            . "Encontra-se disponível para consulta no SGTA na opção Relatórios/Relatórios de Análise.";

                    require_once("../../obj/email.php");
                    $objemail = new email();
                    //andre - emails de grupo
                    $objemail->enviarEmails($assunto, $mensagem, $CodOrgao, 1, $CodTipo,'individual');
                }
            } catch (Exception $exc){
                
            }

                
                
            
            retorno();
            break;
        case "excluir_anexo":
            $CodRelatorio = $_POST['CodRelatorio'];
            $CodAnexo = base64_decode($_POST['CodAnexo']);
            
            $ObjGestao->excluir_anexo($CodAnexo);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            
            gerar_anexos($CodRelatorio);
            
            retorno();
            break;
        case "salvar_mensagem":
            $CodRelatorio = $_POST['CodRelatorio'];
            $CodChat = $_POST['CodChat'];
            $Mensagem = $_POST['Mensagem'];
            
            if($CodChat != "-1"){
                $CodChat = base64_decode($CodChat);
            }
            
            $ObjGestao->salvar_mensagem($CodChat, $CodRelatorio, $cpf, $Mensagem);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            
            gerar_mensagens($CodRelatorio);
            
            try{
                if($CodPerfil != 1){
                    $CodOrgao = $_SESSION['unidade_controle'];

                    $query = $ObjGestao->carrega_relatorio($CodRelatorio);
                    
                    $row = mysqli_fetch_array($query);

                    $CodTipo = $row['CodTipo'];
                    $Titulo = $row['Titulo'];

                    $assunto = "Nova Mensagem em Relatório de Análise.";
                    $mensagem = "Foi cadastrado uma nova mensagem em um relatório de análise.<br>"
                            . "Título do Relatório: $Titulo<br>"
                            . "Encontra-se disponível para consulta no SGTA na opção Relatórios/Relatórios de Análise.";

                    require_once("../../obj/email.php");
                    $objemail = new email();
                    //andre ver se envia para grupo de email
                    $objemail->enviarEmails($assunto, $mensagem, $CodOrgao, 1, $CodTipo,'individual');
                }
            } catch (Exception $ex) {

            }
            
                  
            retorno();
            break;
        case "editar_mensagem":
            $CodChat = $_POST['CodChat'];
            if($CodChat != -1){
                $CodChat = base64_decode($CodChat);
            }
            
            $query = $ObjGestao->consultar_mensagem($CodChat);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            
            $row = mysqli_fetch_array($query);
           
            $array['Mensagem'] = $row['Mensagem'];
           
            retorno();
            break;
        case "excluir_mensagem":
            $CodRelatorio = $_POST['CodRelatorio'];
            $CodChat = $_POST['CodChat'];
            $CodChat = base64_decode($CodChat);
            
            $ObjGestao->excluir_mensagem($CodChat);
            if($ObjGestao->erro != ""){
                throw new Exception($ObjGestao->erro);
            }
            
            gerar_mensagens($CodRelatorio);

            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
    retorno();
}

function gerar_mensagens($CodRelatorio){
     try{
        global $array, $cpf ;
        
        require_once("../../obj/usuario.php");
        $obj_usuario = new usuario();
        
        require_once("../../obj/gestao.php");
        $obj_gestao = new gestao();
        $query = $obj_gestao->consultar_mensagens($CodRelatorio);
        if($obj_gestao->erro != ""){
            throw new Exception($obj_gestao->erro);
        }
        
        $html = '';
        
        while ($row = mysqli_fetch_array($query)){
            
            $CodChat = base64_encode($row['CodChat']);
            $Usuario = $obj_usuario->consulta_nome($row['Usuario']);           
            $DataMensagem = date('d/m/Y H:i:s',strtotime($row['DataMensagem']));
            $Mensagem = str_replace("\r", '<br>', $row['Mensagem']);
            
            if($cpf == $row['Usuario']){
                $botoes = '<a href="#" title="Editar Mensagem" onclick="return editar_mensagem(\''.$CodChat.'\');" class="editar"><span class="glyphicon glyphicon-edit"></span></a> &nbsp
                           <a href="#" title="Excluir Mensagem" onclick="return excluir_mensagem(\''.$CodChat.'\');" class="excluir"><span class="glyphicon glyphicon-remove-circle"></span></a>';
            }else{
                $botoes = '<span class="glyphicon glyphicon-edit" style="color: silver;"></span> &nbsp<span class="glyphicon glyphicon-remove-circle" style="color: silver;"></span>';
            }

            $html = $html.
                    '<tr>
                        <td style="vertical-align: middle;">'.$Usuario.'</td>
                        <td style="vertical-align: middle;">'.$DataMensagem.'</td>
                        <td style="text-align: justify;">'.$Mensagem.'</td>
                        <td style="vertical-align: middle;">
                            '.$botoes.'
                        </td>
                    </tr>';
            
        }
        
        
    
        $array['mensagens'] = $html;
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
        retorno();
    }
}

function gerar_anexos($CodRelatorio){
    try{
        global $array, $cpf ;
        
        require_once("../../obj/usuario.php");
        $obj_usuario = new usuario();
        
        require_once("../../obj/gestao.php");
        $obj_gestao = new gestao();
        $query = $obj_gestao->consultar_anexos($CodRelatorio);
        if($obj_gestao->erro != ""){
            throw new Exception($obj_gestao->erro);
        }
        
        $html = '';
        while ($row = mysqli_fetch_array($query)){
            
            $CodAnexo = $row['CodAnexo'];
            
            if ($row['Usuario'] == $cpf){
                $btnexcluir = ' <a href="#" title="Excluir Anexo" class="excluir" onclick="return excluir_anexo(\''. base64_encode($CodAnexo).'\')"><span class="glyphicon glyphicon-remove-circle"></span></a>';
            }else{
                $btnexcluir = '';
            }
            
            $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/relatorios/" . $CodRelatorio.'/'.$CodAnexo;
                    
            if(file_exists($targetDir)){
                $diretorio = scandir($targetDir);

                foreach($diretorio as $arquivo){
                    if (!is_dir($arquivo)){

                        $NomeArquivo = $arquivo;

                    }
                }
            }
            
            $html = $html.  
                    '<tr>
                        <td><a href="../../intra/relatorios/'.$CodRelatorio.'/'.$CodAnexo.'/'.$NomeArquivo.'" target="_blank">'.$NomeArquivo.'</a>'.$btnexcluir.'</td>
                        <td>'.$row['Descricao'].'</td>
                        <td>'.$obj_usuario->consulta_nome($cpf).'</td>
                        <td>'. data_br($row['DataAnexo']).'</td>
                    </tr>';
        }
        
            
        $array['anexos'] = $html;
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
        retorno();
    }
}

function gerar_tabela($CodPerfil){
    try{
        global $array, $cpf;
        
        require_once("../../obj/usuario.php");
        $obj_usuario = new usuario();
        $obj_usuario->consulta_autorizacoes($cpf);
        if($obj_usuario->erro != ""){
            throw new Exception($obj_usuario->erro);
        }
        
        $autorizacoes = $obj_usuario->array;
        
        require_once("../../obj/gestao.php");
        $obj_gestao = new gestao();
        $query = $obj_gestao->consulta_relatorios($CodPerfil);
        if($obj_gestao->erro != ""){
            throw new Exception($obj_gestao->erro);
        }
        
        $html = "";
        $ord = 0;
        
        while ($row = mysqli_fetch_array($query)){
            
            $ord++;
            
            if($CodPerfil == 1){
                if($row["Status"] == "publicado"){
                    $comandos = '<a href="#" class="consultar" onclick="return consultar('.$row['CodRelatorio'].');" title="Consultar Relatório"><span class="fa fa-eye fa-lg"></span></a> &nbsp;
                                <span class="fa fa-edit fa-lg" style="color: silver;"></span> &nbsp;
                                <a href="#" class="excluir" onclick="return excluir('.$row['CodRelatorio'].');" title="Cancelar a Publicação do Relatório"><span class="fa fa-newspaper-o fa-lg"></span></a>';
                    $css = "color:green; font-weight: bold;";
                }else{
                    $comandos = '<a href="#" class="consultar" onclick="return consultar('.$row['CodRelatorio'].');" title="Consultar Relatório"><span class="fa fa-eye fa-lg"></span></a> &nbsp;
                                <a href="#" class="editar" onclick="return editar('.$row['CodRelatorio'].');" title="Editar Relatório"><span class="fa fa-edit fa-lg"></span></a> &nbsp;
                                <span class="fa fa-newspaper-o fa-lg" style="color: silver;"></span>';
                    $css = "color:orange; font-weight: bold;";
                }
            }else{
                $comandos = '<a href="#" class="consultar" onclick="return consultar('.$row['CodRelatorio'].');" title="Consultar Relatório"><span class="fa fa-eye fa-lg"></span></a> &nbsp;';
                $css = "color:green; font-weight: bold;";
            }
            
            $autorizado = false;
            
            if($autorizacoes[0]['qtd'] > 0){
                for ($index = 0; $index < count($autorizacoes); $index++){
                    if($row['CodTipo'] == $autorizacoes[$index]['CodTipo']){
                        $autorizado = true;
                    }
                }
            }    
             //SGTA-75 - adm pode acessar
            if($CodPerfil == 1){
                $autorizado = true;
            }
            
            if($autorizado == true){
                
                if($row['QtdAnexos'] == ""){$row['QtdAnexos'] = "0";}
                if($row['QtdChat'] == ""){$row['QtdChat'] = "0";}
                
                $html = $html.
                '<tr>
                    <td>'.$ord.'</td>
                    <td>'.$row['DescricaoTipo'].'</td>
                    <td>'.$row['Titulo'].'</td>
                    <td>'.$row['Nome'].'</td>
                    <th style="text-align:center;">'.data_br($row['DataLiberacao']).'</td>
                    <th style="text-align:right;">'.$row['QtdAnexos'].'</td>
                    <th style="text-align:right;">'.$row['QtdChat'].'</td>
                    <td style="'.$css.'">'.$row['Status'].'</td>
                    <td>
                        '.$comandos.'
                    </td>
                </tr>';
            }
                
        }
        
        $array['tabela'] = $html;
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage() . " Linha: " . $ex->getLine();
        retorno();
    }
}

function sanitizeString($str) {
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
    //$str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
    //$str = preg_replace('/[^a-z0-9]/i', '_', $str);
    //$str = preg_replace('/_+/', '_', $str); // ideia do Bacco :)
    return $str;
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}