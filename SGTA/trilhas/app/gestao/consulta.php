<?php

    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if($perfil > 4){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Consulta Relatórios";

?>

<script src="consulta.js" type="text/javascript"></script>
<br>
<div class="container-fluid">
    <div class="container">
        <div class="well" style="font-size: 140%; font-weight: bold;">Consulta Relatórios de Análise</div>
    </div>
    
    <table id="tbl_relatorios" class="display" width="100%">
        <thead>
            <tr>
                <th>Ord</th>
                <th>Área</th>
                <th>Título</th>
                <th>Cadastrado Por</th>
                <th style="text-align:center;">Data Publicação</th>
                <th style="text-align:right;">Qtd Anexos</th>
                <th style="text-align:right;">Qtd Mensagens</th>
                <th>Status</th>
                <th style="width: 70px;"></th>
            </tr>
        </thead>
        <tbody id="tb_relatorios">

        </tbody>
        <tfoot>
            <tr>
                <th>Ord</th>
                <th>Área</th>
                <th>Título</th>
                <th>Cadastrado Por</th>
                <th style="text-align:center;">Data Publicação</th>
                <th style="text-align:right;">Qtd Anexos</th>
                <th style="text-align:right;">Qtd Mensagens</th>
                <th>Status</th>
                <th></th>
            </tr>
        </tfoot>
    </table>
    
</div>

<form name="form1" id="form1" action="cadastro.php" method="POST">
    <input type="hidden" name="CodRelatorio" id="CodRelatorio" value="" />
</form>


<?php
    // pagemaincontent recebe o conteudo do buffer
    $pagemaincontent = ob_get_contents(); 

    // Descarta o conteudo do Buffer
    ob_end_clean(); 

    //Include com o Template
    include("../../master/master.php");
    include('../../master/datatable.php');