<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if($perfil > 1){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Relatório de Análise";
    
    if(isset($_POST["CodRelatorio"])){
        $CodRelatorio = $_POST["CodRelatorio"];
    }else{
        $CodRelatorio = -1;
    }
    
    //$CodRelatorio = 2;
    
?>

<script src="cadastro.js" type="text/javascript"></script>
<script src="../../js/nicEdit.js" type="text/javascript"></script>
<style>
    div.especial2{
        border-color: #ccc; 
        border-style: solid; 
        border-width: 1px; 
        border-radius: 6px; 
        -moz-border-radius: 6px; 
        -webkit-border-radius: 6px; 
        padding: 6px;
        margin-bottom: 6px;
    }
</style>

<br>
<div class="container">
    
    <div class="well" style="font-size: 140%; font-weight: bold;">Cadastro de Relatório de Análise</div>
    
    <form name="form1" id="form1" method="POST" class="form-horizontal" action="/action_page.php">
        <div class="form-group">
            <label class="control-label col-sm-1" for="area">Área:</label>
            <div class="col-sm-4">
                <select name="area" id="area" class="form-control">
                    <option value="-1"></option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-1" for="titulo">Título:</label>
            <div class="col-sm-11">
                <input type="text" class="form-control" name="titulo" id="titulo" value="" maxlength="500"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-1" for="titulo">Autores:</label>
            <div class="col-sm-11">
                <div class="especial2" id="autores">
                    
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-1" for="status">Status:</label>
            <div class="col-sm-2">
                <input type="text" class="form-control" name="status" id="status" value="rascunho" disabled="disabled"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-1" for="editor">Editor:</label>
            <div class="col-sm-11">
                <textarea style="width: 100%;" cols="100" rows="20" id="editor"></textarea>
            </div>  
        </div>
        <div class="form-group"> 
            <div class="col-sm-offset-1 col-sm-11">
                <button type="button" class="btn btn-primary" onclick="publicar();">&nbsp Publicar &nbsp<i class="fa fa-newspaper-o fa-lg"></i>&nbsp</button>
            </div>
        </div>
        
        <input type="hidden" name="CodRelatorio" id="CodRelatorio" value="<?php echo $CodRelatorio; ?>" />
        <input type="hidden" name="texto" id="texto" value="" />
    </form>
    
</div>
<br>


<?php
    // pagemaincontent recebe o conteudo do buffer
    $pagemaincontent = ob_get_contents(); 

    // Descarta o conteudo do Buffer
    ob_end_clean(); 

    //Include com o Template
    include("../../master/master.php");
    include('../../master/datatable.php');