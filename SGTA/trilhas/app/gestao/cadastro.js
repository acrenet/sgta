var area1;
var execute_cod = 0;
var destino = "server.php";

$(document).ready(function(){ 
    
    var data = new FormData();
    data.append("operacao","load");
    data.append("CodRelatorio", $("#CodRelatorio").val());
    formdata = data;
    submit_file(destino);
    
});

function resposta_up(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                
                $("#area").html(retorno.html);
                
                $("#autores").html(retorno.autores);
                
                if($("#CodRelatorio").val() == -1){
                    $('#editor').val(retorno.relatorio.Texto);
                }else{
                    console.log("entrou");
                    $('#editor').val(retorno.relatorio.Texto);
                    $('#titulo').val(retorno.relatorio.Titulo);
                    $('#area').val(retorno.relatorio.CodTipo);
                    $('#status').val(retorno.relatorio.Status);
                }
                
                if(typeof(area1) == "undefined"){
                    area1 = new nicEditor({
                        buttonList : ['save','print','bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','fontFormat','indent','outdent','image','upload','link','unlink','forecolor','bgcolor'],
                        fullPanel : true,
                        onSave : function(content, id, instance) {
                                    salvar_instrucao();
                                  },
                        onPrint : function(content, id, instance) {
                                    imprimir_instrucao();
                                  }
                    }).panelInstance('editor');
                }
                
                break;
            case "salvar":
                
                $("#CodRelatorio").val(retorno.CodRelatorio);
                $("#msg_sucesso").html("Salvo com sucesso.");
                $("#popup_sucesso").dialog("open");

                break;
            case "publicar":
                execute_cod = 1;
                $('#status').val("publicado");
                $("#CodRelatorio").val(retorno.CodRelatorio);
                $("#popup_pergunta").dialog("close");
                $("#msg_sucesso").html("Relatório publicado com sucesso.");
                $("#popup_sucesso").dialog("open");
                
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function salvar_instrucao(){
    var nicE = new nicEditors.findEditor('editor');
    var str = nicE.getContent();
    var data = new FormData();
    data.append("operacao","salvar");
    data.append("Texto", str.trim());
    data.append("Titulo", $("#titulo").val());
    data.append("CodTipo", $("#area").val());
    data.append("CodRelatorio", $("#CodRelatorio").val());
    
    var searchIDs = [];

    $("#autores input:checkbox:checked").map(function(){
      searchIDs.push($(this).val());
    });
    
    for (var i = 0; i < searchIDs.length; i++) {
        data.append('autor[]', searchIDs[i]);
    }
    
    formdata = data;
    submit_file(destino);   
}

function publicar(){
    var nicE = new nicEditors.findEditor('editor');
    var str = nicE.getContent();
    var data = new FormData();
    data.append("operacao","publicar");
    data.append("Texto", str.trim());
    data.append("Titulo", $("#titulo").val());
    data.append("CodTipo", $("#area").val());
    data.append("CodRelatorio", $("#CodRelatorio").val());
    
    var searchIDs = [];

    $("#autores input:checkbox:checked").map(function(){
      searchIDs.push($(this).val());
    });
    
    for (var i = 0; i < searchIDs.length; i++) {
        data.append('autor[]', searchIDs[i]);
    }
    
    formdata = data;
    
    $("#msg_pergunta").text("Você tem certeza da publicação do relatório?");
    $("#popup_pergunta").dialog("open");
}

function imprimir_instrucao(){
    var nicE = new nicEditors.findEditor('editor');
    var str = nicE.getContent();
    
    $("#texto").val(str.trim());
    
    document.getElementById("form1").target = "_blank";
    document.getElementById("form1").action = "impressao.php";
    document.getElementById("form1").submit();
    document.getElementById("form1").target = "";
}

function pergunta_ok(){
    submit_file(destino);
}

function execute(){
    if (execute_cod == 1){
        document.getElementById("form1").action = "consulta.php";
        document.getElementById("form1").submit();
        execute_cod = 0;
    }
}

