var execute_cod = 0;
var destino = "server.php";

$(document).ready(function(){
    
    popup_upload_();
    popup_nova_mensagem_();
    
    var data = new FormData();
    data.append("operacao","abrir_relatorio");
    data.append("CodRelatorio",$("#CodRelatorio").val());
    formdata = data;
    submit_file(destino);
});

!function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)

function popup_upload_(){
    $("#popup_upload").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 1000,
        maxWidth: 1000,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function popup_nova_mensagem_(){
    $("#popup_nova_mensagem").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 700,
        maxWidth: 700,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function novo_upload(){
    document.getElementById("form_upload").reset();
    $('#popup_upload').dialog('open');
}

function nova_mensagem(){
    document.getElementById("form_mensagem").reset();
    $('#popup_nova_mensagem').dialog('open');
    $("#CodChat").val("-1");
}

function salvar_mensagem(){
    var data = new FormData();
    data.append("CodRelatorio", $("#CodRelatorio").val());
    data.append("operacao","salvar_mensagem");
    data.append("CodChat", $("#CodChat").val());
    data.append("Mensagem", $("#msg").val());
    formdata = data;
        
    $("#msg_pergunta").text("Os dados estão corretos?");
    $("#popup_pergunta").dialog("open");
    return false;
}

function excluir_mensagem(cod){
    var data = new FormData();
    data.append("CodRelatorio", $("#CodRelatorio").val());
    data.append("operacao","excluir_mensagem");
    data.append("CodChat", cod);
    
    formdata = data;
        
    $("#msg_pergunta").text("Você tem certeza da exclusão da mensagem?");
    $("#popup_pergunta").dialog("open");
    return false;
}

function editar_mensagem(cod){
    var data = new FormData();
    data.append("CodRelatorio", $("#CodRelatorio").val());
    data.append("operacao","editar_mensagem");
    data.append("CodChat", cod);
    $("#CodChat").val(cod);
    formdata = data;
    submit_file(destino);
   
    return false;
}

function resposta_up(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "abrir_relatorio":
                
                console.log("-->" + retorno.autores);
                
                $("#area").val(retorno.relatorio[0].Area);
                $("#titulo").val(retorno.relatorio[0].Titulo);
                $("#div_autores").html(retorno.autores);
                $("#div_conteudo").html(retorno.relatorio[0].Texto);
                $("#DataPublicacao").val(retorno.relatorio[0].DataLiberacao);
                $("#tb_anexos").html(retorno.anexos);
                $("#tb_mensagens").html(retorno.mensagens);
                
                break;
            case "upload":  
                $("#tb_anexos").html(retorno.anexos);
                
                $("#popup_pergunta").dialog("close");
                $("#popup_upload").dialog("close");
                
                $("#msg_sucesso").html("Documento anexado com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "excluir_anexo":
                $("#tb_anexos").html(retorno.anexos);
                
                $("#popup_pergunta").dialog("close");
                
                $("#msg_sucesso").html("Documento excluído com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "salvar_mensagem":
                $("#tb_mensagens").html(retorno.mensagens);
                
                $("#popup_pergunta").dialog("close");
                $('#popup_nova_mensagem').dialog('close');
                
                $("#msg_sucesso").html("Dados gravados com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            case "editar_mensagem":
                
                $("#msg").val(retorno.Mensagem); 
                $('#popup_nova_mensagem').dialog('open');
                
                break;
            case "excluir_mensagem":
                
                $("#tb_mensagens").html(retorno.mensagens);
                
                $("#popup_pergunta").dialog("close");
                
                $("#msg_sucesso").html("Mensagem excluída com sucesso.");
                $("#popup_sucesso").dialog("open");
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function imprimir(){
    
    $("#texto").val($("#div_conteudo").html());
    
    document.getElementById("form1").target = "_blank";
    document.getElementById("form1").action = "impressao.php";
    document.getElementById("form1").submit();
    document.getElementById("form1").target = "";
}

function validar_arquivo(){
    
    var upl = $("#uploadfile");

    
    var files = upl[0].files;
    var log = "";
    var erro  = false;
    var msg = "";
    var ext = "";

    for (var i = 0; i < files.length; i++){
        log = log + '"' + files[i].name + '"  ';
        if(files[i].size > 20480000){
            erro = true;
            msg = msg + "O arquivo ''" + files[i].name + "'' ultrapaça o limite de 20 megabytes.<br>";
        }
        
        $("#Tamanho").val(files[i].size);

        ext = files[0].name.split('.').pop();

        var str = "xls,xlsx,doc,docx,pdf,ods,odt,rtf";
        if(str.indexOf(ext) == -1){
            erro = true;
            msg = msg + "O arquivo ''" + files[i].name + "'' possui um formato não permitido.<br>É permitido apenas arquivos de texto, planilhas e documentos pdf.";
        }   
    }

    if(erro == true){
        $("#msg_erro").html(msg);
        $("#popup_erro").dialog("open");
        return false;
    }else{
        return files.length;
    }

}

function subir_arquivo(){
    var resposta = validar_arquivo(false);
    if (resposta === false){
        return false;
    }else if(resposta == 0){
        $("#msg_alerta").html("Nenhum arquivo selecionado.<br>Selecione um arquivo primeiro.");
        $("#popup_alerta").dialog("open");
        return false;
    }else{

        var data = new FormData();
        
        jQuery.each(jQuery('#uploadfile')[0].files, function(i, file) {
            data.append('uploadfile-'+i, file);
        });
        
        data.append("CodRelatorio", $("#CodRelatorio").val());
        data.append("operacao","upload");
        data.append("descricao", $("#descricao").val());

        formdata = data;
        
        $("#msg_pergunta").text("Confirma o envio do arquivo?");
        $("#popup_pergunta").dialog("open");
    }
}

function excluir_anexo(cod){
    var data = new FormData();
    data.append("CodRelatorio", $("#CodRelatorio").val());
    data.append("operacao","excluir_anexo");
    data.append("CodAnexo",cod);
    
    formdata = data;
        
    $("#msg_pergunta").text("Você têm certeza da exclusão deste anexo?");
    $("#popup_pergunta").dialog("open");
    return false;
}

function pergunta_ok(){
    submit_file(destino);
}

function execute(){
    
}