<?php
require_once("../../obj/usuario.php");
require_once("../../obj/orgaos.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

$obj_usuario = new usuario();
$obj_orgao = new orgaos();

try {
    
    $dia = date("N");
    $hora = date("G");
    
    $b = 0;
    
    if($dia > 5){
       // $b = 1;
    }elseif($hora < 7 || $hora > 18){
       // $b = 1;
    }
    
    if(isset($_POST['comando'])){
        if($_POST['comando'] == "wake-up!"){
            $b = 0;
        }
    }
    
    if($b == 1){
        usleep(5000000);
        throw new Exception("Sistema off-line.");
    }
    //aqui andre, carregamento de dados do usuario
    switch ($array["operacao"]) {
        case "login":                   //------------------------------------------------------------------------------------------------------
            $cpf = $_POST['cpf'];
           // $cpf = substr($cpf, 0, 15);
            $cpf = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i", '', $cpf);
            
            $senha = $_POST['senha'];
            //$senha = md5($senha);
            
            if (!isset($_SESSION)) {
                session_start();
            }
            
            $token = $_SESSION['token'];
            $CSRFToken = $_POST['CSRFToken'];
            
            if($CSRFToken != $token){
                $array['erro'] = "Token Inválido.";
                retorno();
            }
            
            $obj_usuario->consulta_usuario($cpf);
            if($obj_usuario->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
            
            $query = $obj_usuario->query;
            
            if(mysqli_num_rows($query) == 0 ){//|| mysqli_num_rows($query) > 1
                $array['alerta'] = "Usuário não cadastrado.";
                retorno();
            }

            $row = mysqli_fetch_array($query);
            
            if($row["Status"] == "bloqueado"){
                $array['alerta'] = "Seu perfil foi bloqueado, procure a administração do sistema.";
                retorno();
            }
            
            if(md5($senha) == $row["Senha"]){
                
                $unidade_controle = $obj_orgao->consulta_unidade_controle();
                if($obj_orgao->erro != ""){
                    $array['erro'] = $obj_orgao->erro;
                    retorno();
                }
                
                if(!isset($_SESSION)){
                    session_start();
                }
                
                $_SESSION['unidade_controle'] = $unidade_controle;
                $_SESSION['sessao_id'] = $cpf;
                $_SESSION['sessao_nome'] = $row["Nome"];
                $_SESSION['sessao_orgao'] = $row['CodOrgao'];
                $_SESSION['sessao_nome_orgao'] = $row['NomeOrgao'];
                $_SESSION['sessao_sigla_orgao'] = $row['SiglaOrgao'];
                $_SESSION['sessao_perfil'] = $row['CodPerfil'];
                $_SESSION['sessao_funcao'] = $row['CodFuncao'];
                $_SESSION['sessao_nome_perfil'] = $row['NomePerfil'];
                $_SESSION['sessao_ultima_dica'] = $row['UltimaDica'];
                $_SESSION['sessao_exibido_dica'] = false;
                $_SESSION['sessao_time'] = time() + 3600; //1 hora para expirar a sessão
                
                if($row['UltimoAcesso'] == ""){
                    $array["resposta"] = "Seja bem vindo Sr(a): ".$row["Nome"]."<br>Este é seu primeiro acesso.";
                }else{
                    $date_oct = new DateTime($row['UltimoAcesso']);
                    $array["resposta"] = "Seja bem vindo Sr(a): ".$row["Nome"]."<br>Seu último acesso foi em ".$date_oct->format('d/m/Y')." às ".$date_oct->format('H:i:s')." hs.";
                }
                
                $obj_usuario->consulta_autorizacoes($cpf);
                if($obj_usuario->erro != ""){
                    $array['erro'] = $obj_usuario->erro;
                    retorno();
                }

                $_SESSION['sessao_autorizacoes'] = $obj_usuario->array;
                
                $obj_usuario->atualizar_acesso($cpf);
                $array['erro'] = $obj_usuario->erro;
                
            }else{
                
                if(!isset($_SESSION)){
                    session_start();
                }
                
                if( !isset($_SESSION['tentativas_senha']) ){
                    $_SESSION['tentativas_senha'] = 1;
                }else{
                    $_SESSION['tentativas_senha'] = $_SESSION['tentativas_senha'] + 1;
                }
                
                if ($_SESSION['tentativas_senha'] > 3){ //penaliza em 4 segundos para segurança.
                    sleep(4);
                }
                if ($_SESSION['tentativas_senha'] > 10){ //penaliza em 9 segundos para segurança.
                    sleep(9);
                }
                
                $array['alerta'] = "A senha não confere.";
            }

            retorno();
            break;
        case "reiniciar_senha":         //------------------------------------------------------------------------------------------------------
            $cpf = $_POST['cpf'];
            $email = $_POST['email'];
            
            $obj_usuario->resetar_senha($cpf, $email);
            if($obj_usuario->erro != ""){
                $array['erro'] = $obj_usuario->erro;
                retorno();
            }
            if($obj_usuario->alerta != ""){
                $array['alerta'] = $obj_usuario->alerta;
                retorno();
            }
            
            $array["resposta"] = "Uma nova senha foi enviada para o seu e-mail.";

            retorno();
            break;
        default:                        //------------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}


function retorno(){
    global $array;
       
    usleep(900000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}