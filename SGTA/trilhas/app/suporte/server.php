<?php

require_once("../../obj/usuario.php");
require_once("../../obj/suporte.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);

    $obj_usuario = new usuario();
    $obj_suporte = new suporte();

    session_start();

try {
    
    $dia = date("N");
    $hora = date("G");
    
    $b = 0;
    
    if($dia > 5){
        $b = 1;
    }elseif($hora < 7 || $hora > 18){
        $b = 1;
    }
    
    if($b == 1){
        usleep(5000000);
        throw new Exception("Sistema off-line.");
    }
    
    switch ($array["operacao"]) {
        case "load":                        //------------------------------------------------------------------------------------------------------

            if(isset($_SESSION['sessao_id'])){
                
                $cpf = $_SESSION['sessao_id'];
                $orgao = $_SESSION['sessao_orgao'];
                $perfil = $_SESSION['sessao_perfil'];
                
                $obj_usuario->consulta_usuario($cpf);
                if($obj_usuario->erro != ""){
                    $array['erro'] = $obj_usuario->erro;
                    retorno();
                }
                
                $query = $obj_usuario->query;
                $row = mysqli_fetch_array($query);
                
                $array['nome'] = $row['Nome'];
                $array['fone'] = $row['Telefone'];
                $array['email'] = $row['Email'];
                $array['logado'] = true;
            }else{
                $array['logado'] = false;
            }
            
            $token = md5(uniqid(rand(), TRUE));
            $_SESSION['token'] = $token;
            $array['token'] = $token;
            
            retorno();
            break;
        case "salvar":
            $token = $_SESSION['token'];
            $CSRFToken = $_POST['CSRFToken'];
            $nome = $_POST['nome'];
            $fone = $_POST['fone'];
            $email = $_POST['email'];
            $ocorrencia = $_POST['ocorrencia'];
            
            $ocorrencia = str_replace("<script>","",$ocorrencia);
            $ocorrencia = str_replace("</script>","",$ocorrencia);
            $ocorrencia = str_replace("'","",$ocorrencia);
            
            if(isset($_SESSION['sessao_id'])){ 
                $cpf = $_SESSION['sessao_id'];
                $nome = $_SESSION['sessao_nome'];
            }else{
                $cpf = 0;
            }
            
            if($CSRFToken != $token){
                $array['erro'] = "Token Inválido.";
                retorno();
            }
            
            if( !isset($_SESSION['registros']) ){
                $_SESSION['registros'] = 1;
            }else{
                $_SESSION['registros'] ++;
            }
            
            $array['warnning'] = "";
            
            if($_SESSION['registros'] > 3){
                $array['warnning'] = "O numero máximo de ocorrências registradas (3) foi atingido, tente novamente mais tarde.";
                retorno();
            }
            
            $erro = "";
            
            $nome = trim($nome);
            $fone = trim($fone);
            $email = trim($email);
            $ocorrencia = trim($ocorrencia);
            
            if($nome == ""){
                $erro = "O campo ''Nome'' está vazio. É necessário preencher este campo.<br>";
            }
            
            if($fone == "" && $email == ""){
                $erro = $erro . "Os campos ''Telefone'' e ''E-Mail'' estão vazios. É necessário preencher pelo menos um dos campos.<br>";
            }
            
            if($ocorrencia == ""){
                $erro = $erro . "O campo ''Ocorrência'' está vazio. É necessário preencher este campo.";
            }
            
            if($erro != ""){
                $array['alerta'] = $erro;
                retorno();
            }
            
            $obj_suporte->salvar($cpf, $nome, $fone, $email, $ocorrencia);
            if($obj_suporte->erro != ""){
                $array['erro'] = $obj_suporte->erro;
                retorno();
            }
            
            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}