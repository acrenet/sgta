<?php

$__servidor = true;
$operacao = "";
require_once("../../code/verificar.php");
if($__autenticado == false){
    $array = array(
        "operacao" => $operacao,
        "erro" => "",
        "alerta" => "A sessão expirou, será necessário realizar um novo login.",
        "noticia" => "",
        "retorno" => ""
    );
    retorno();
}
require_once("../../code/funcoes.php");
require_once("../../obj/suporte.php");
require_once("../../obj/usuario.php");

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);


try {
    
    switch ($array["operacao"]) {
        case "atualizar":
        case "load":                   //----------------------------------------------------------------------------------------------
            
            gera_tabela();
            
            retorno();
            break;
        case "responder":
            $CodSuporte = $_POST['CodSuporte'];
            $Resposta = $_POST['Resposta'];
            $Resposta = trim($Resposta);
            
            if($Resposta == ""){
                $array['erro'] = "Preencha o campo Resposta.";
                retorno();
            }
            
            $obj_suporte = new suporte();
            
            $obj_suporte->responder($CodSuporte, $Resposta);
            if($obj_suporte->erro != ""){
                $array['erro'] = $obj_suporte->erro;
                retorno();
            }
            
            gera_tabela();
            
            retorno();
            break;
        default:                        //--------------------------------------------------------------------------------------------------
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
    
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}


function gera_tabela(){
    global $array;
    
    $obj_suporte = new suporte();
    $obj_usuario = new usuario();
    
    try{
        
        $query =  $obj_suporte->consultar();
        if($obj_suporte->erro != ""){
            $array['erro'] = $obj_suporte->erro;
            retorno();
        }

        $html = "";

        if(mysqli_num_rows($query) > 0){
            while ($row = mysqli_fetch_array($query)){
                
                if($row['Status'] == "pendente"){
                    $cor = "red";
                    $link = '<a href="#" class="editar" title="Responder Ocorrência." onclick="return responder('.$row['CodSuporte'].');"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></a>';
                }else{
                    $cor = "green";
                    $link = '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true" style="color: silver;"></i>';
                }
                
                $html = $html . 
                '<tr>
                    <td>'.$row['Nome'].'</td>
                    <td>'.$row['Telefone'].'</td>
                    <td>'.$row['Email'].'</td>
                    <td id="txt_'.$row['CodSuporte'].'">'.str_replace("rn", '<br>', $row['Ocorrencia']).'</td>
                    <td>'.data_br($row['Horario'], true).'</td>
                    <td>'.str_replace("\r", '<br>', $row['Resposta']).'</td>
                    <td>'.data_br($row['HoraResposta'], true).'</td>
                    <td>'.$obj_usuario->consulta_nome($row['RespondidoPor']).'</td>    
                    <td style="color: '.$cor.';">'.$row['Status'].'</td>
                    <td>
                        '.$link.'
                    </td>
                </tr>';
            }    
        }
        
            

        $array['html'] = $html;
    
    } catch (Exception $ex) {
        $array["erro"] = $ex->getMessage();
        retorno();
    }
}

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}

