<?php
    //Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';
    
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if(($perfil > 2)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Trilhas - Atendimento de Ocorrências";
    
?>
<script src="atendimento.js" type="text/javascript"></script>
<br>
<div class="container-fluid">
    
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Atendimento de Ocorrências</h4></div>
            <div class="panel-body">
                <table id="tbl_suporte" class="display" style="font-size: 80%;">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>E-mail</th>
                            <th>Ocorrência</th>
                            <th>Data</th>
                            <th>Resposta</th>
                            <th>Data</th>
                            <th>Atendente</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="tb_suporte">
                        
                    </tbody>
                    <tfoot style="display: none;">
                        <tr>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>E-mail</th>
                            <th>Ocorrência</th>
                            <th>Data</th>
                            <th>Resposta</th>
                            <th>Data</th>
                            <th>Atendente</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
    
</div>

<div id="popup_form" title="Cadastrar Resposta">
    <form id="form1" name="form1" role="form">
        <p><b>Será enviado um e-mail ao solicitante com sua resposta.</b></p>
        <label for="text">Ocorrência:</label>
        <div id="div_ocorrencia">
            
        </div>
        
        <hr>
        
        <label for="email">Resposta:</label><br>
          <textarea name="Resposta" id="Resposta" rows="5" cols="20" class="form-control" maxlength="1000"></textarea>
        <hr>
        <div style="text-align: right;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" onclick='$("#popup_form").dialog("close");'>Cancelar &nbsp;<span class="fa fa-times-circle"></span></button>
            <button type="button" class="btn btn-success" onclick='salvar_resposta();'>Confirmar &nbsp;<span class="fa fa-check-square-o"></span></button>
        </div>
        <input type="hidden" name="operacao" id="operacao" value="load" />
        <input type="hidden" name="CodSuporte" id="CodSuporte" value="" />
    </form>
</div>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");
  include('../../master/datatable.php');

