var execute_cod = 0;
var destino = "server2.php";

$(document).ready(function(){
    
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
   
    popup_form_();
});

function popup_form_(){
    $("#popup_form").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 600,
        maxWidth: 600,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "responder":
                $("#popup_form").dialog("close");
                $("#msg_sucesso").text("Dados Gravados com Sucesso, um e-mail foi enviado ao solicitante com sua resposta.");
                $("#popup_sucesso").dialog("open");
            case "atualizar":
                minha_tabela.destroy();
            case "load":
                $("#tb_suporte").html(retorno.html);
                
                var ordem = [];

                var data = [
                                {"sType": "string"},
                                {"sType": "string"},
                                {"sType": "string"},
                                {"sType": "string"},
                                {"sType": "date-uk"},
                                {"sType": "string"},
                                {"sType": "date-uk"}
                            ];

                datatable_("tbl_suporte", 9, ordem, -1, data);        
                break;
                
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../inicio/inicio.php");
    }
    execute_cod = 0;
}

function responder(cod){
    $("#CodSuporte").val(cod);
    $("#div_ocorrencia").html($("#txt_"+cod).html());
    $("#Resposta").val("");
    $("#popup_form").dialog("open");
    return false;
}

function salvar_resposta(){
    $("#operacao").val("responder");
    formdata = $("#form1").serialize();
    $("#popup_dialogo").dialog("open");
}

