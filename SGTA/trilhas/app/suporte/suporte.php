<?php
    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $_titulo = "Trilhas - Pedido de Suporte";
    
    $dia = date("N");
    $hora = date("G");
    
    $b = 0;
    
    if($dia > 5){
    //    $b = 1;
    }elseif($hora < 7 || $hora > 18){
  //      $b = 1;
    }
    
?>
<script src="suporte.js" type="text/javascript"></script>
<br>
<div class="container">
    
    <?php 
        /*
        if($dia > 5){
            echo '<img src="../../images/zzz.GIF" alt=""/> <b>Sistema off-line, volte segunda-feira em horário comercial.<b>';
        }elseif($hora < 6 || $hora > 19){
            echo '<img src="../../images/zzz.GIF" alt=""/> <b>Sistema off-line, volte em horário comercial.<b>';
        }
        */
    ?>
    
    <div id="div_suporte" class="panel-group">
        <div class="panel panel-info">
            <div class="panel-heading"><h4>Pedido de Suporte.</h4></div>
            <div class="panel-body">
                <form class="form-horizontal" id="form1" name="form1" method="post">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="nome">Nome:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nome" name="nome" disabled="" maxlength="200">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="fone">Telefone:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="fone" name="fone" maxlength="100">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">E-mail:</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" id="email" name="email" maxlength="200">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="ocorrencia">Ocorrência:</label>
                        <div class="col-sm-9"> 
                            <textarea name="ocorrencia" class="form-control" id="ocorrencia" rows="6" cols="20"  maxlength="2000"></textarea>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="button" class="btn btn-primary" onclick="salvar();">Enviar <span class="fa fa-share"></span></button>
                        </div>
                    </div>
                    <input type="hidden" name="operacao" id="operacao" value="" />
                    <input type="hidden" name="CSRFToken" id="CSRFToken" value="" />
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var b = <?php echo $b; ?>;
    if (b == 1){
        $("#div_suporte").hide();
    }
</script>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");            