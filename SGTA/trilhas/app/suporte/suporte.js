var execute_cod = 0;
var destino = "server.php";

$(document).ready(function(){
    
    $("#operacao").val("load");
    formdata = $("#form1").serialize();
    submit_form(destino);
    
});

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "load":
                
                if(retorno.logado == true){
                    $("#nome").val(retorno.nome);
                    $("#fone").val(retorno.fone);
                    $("#email").val(retorno.email);
                }else{
                    $("#nome").prop("disabled", false);
                }
                
                $("#CSRFToken").val(retorno.token);
                
                break;
            case "salvar":
                
                $("#form1 :button").attr("disabled", true);
                $("#form1 :input").attr("disabled", true);
                
                if(retorno.warnning != ""){
                    $("#msg_erro").html(retorno.warnning);
                    $("#popup_erro").dialog("open");
                    break;
                }
                
                $("#msg_sucesso").html("Informações gravadas com sucesso, logo entraremos em contato.");
                $("#popup_sucesso").dialog("open");
                
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function execute(){
    switch(execute_cod){
        case 1:
            window.location.replace("../administrar/administrar.php");
    }
    execute_cod = 0;
}

function salvar(){
    
    var isDisabled = $("#nome").is(':disabled');
    
    if(isDisabled){
        $("#nome").prop("disabled", false);
    }
    
    $("#operacao").val("salvar");
    formdata = $("#form1").serialize();
    
    if(isDisabled){
        $("#nome").prop("disabled", true);
    }
    
    $("#popup_dialogo").dialog("open");
}
