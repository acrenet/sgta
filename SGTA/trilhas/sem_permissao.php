<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link href="/trilhas/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="/trilhas/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="/trilhas/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="/trilhas/master/master.css" rel="stylesheet" type="text/css"/>
        
        
        <script src="/trilhas/js/jquery-1.12.3.js" type="text/javascript"></script>
        <script src="/trilhas/js/bootstrap.js" type="text/javascript"></script>
        <script src="/trilhas/js/jquery-ui.js" type="text/javascript"></script>
        <script src="/trilhas/master/master.js" type="text/javascript"></script>
        
        <link rel="shortcut icon" href="/images/favicon.ico">

        <title>TCU</title>
        

    </head>
    <body>
        <div id="master_div">
        <div class="container-fluid" style="background-color: #222;">
            <div class="container">
                <nav class="navbar navbar-inverse" style="margin-bottom: 0px; border-color: #222;">
                    <div class="container-fluid">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                            
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        
        <style>
            .contentLogoCGE {
                margin-top: 13px;
                float: right;
                /* margin-right: 20px; */
            }
            .cgeLogo {
                color: #E84902;
                font-size: 25px;
                font-weight: bold;
                border-right: 2px solid #E84902;
                float: left;
                height: 30px;
                margin: 0px;
                padding-right: 10px;
                margin-right: 10px;
                text-shadow: 1px 1px #000;
                box-shadow: 1px 0px #000;
            } 
            .controladoriGeralDoEstado {
                float: left;
                width: 145px;
                font-weight: bold;
                font-size: 14px;
            }
        </style>
          <!-- http://www.tce.sc.gov.br/sites/all/themes/tce/media/css/img/brasao.jpg -->
  
          <?php
      
          echo '<div class="container-fluid" style="background-image: url(http://'.$_SERVER['HTTP_HOST'] .'/trilhas/images/fundo_intra1.jpg); background-repeat:no-repeat; background-size: auto; ">
              <div class="container" style="color: white;">
                  
                  <div class="row">
                      <div class="col-sm-9">
                          <br/>
                         
                           <div style="margin-bottom: 25px;"><img src="http://'.$_SERVER['HTTP_HOST'] .'/trilhas/images/logo_cge.png" alt="TRIBUNAL DE CONTAS DE SANTA CATARINA" style="max-width: 200px;"/></div>
                         
                           <div style="font-size: 200%; color: black; font-weight: bold; margin: 10px 0px -7px 0px;">Sistema de Gestão de Trilhas de Auditoria - SGTA</div>
                          <div style="font-size: 100%; color: black; font-weight: bold; margin: 0px 0px 25px 0px;">Auditorias Realizadas por Meio de Cruzamento de Bases de Dados.</div>
                         
                      </div>
                   
                  </div>
                  
                          
              </div>
          </div>';
          ?>
        
        
        <div class="container" style="font-size: 150%; color: black; font-weight: normal; margin: 0px 0px 25px 0px;  margin-top: 0%; text-align: left;"> 
            Seu usuario não esta com permissões cadastradas no TCE Virtual

        </div>
        
        
  
 
    </div>    
    </body>
</html>