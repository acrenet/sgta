<?php
if(!isset($_SESSION)){
    session_start(); 
}

if($_SERVER['HTTP_HOST'] == 'sgta.qa.tce.sc.gov.br'){
    $_SESSION['comunicacao'] = 'https://virtual.qa.tce.sc.gov.br/comunicacao/rest/';
    $_SESSION['sgi'] = 'https://virtual.qa.tce.sc.gov.br/sgi/rest/';
    $_SESSION['virtual'] = 'https://virtual.qa.tce.sc.gov.br/web/';
    $_SESSION['enviarEmail'] = false; 
    $_SESSION['diasMaxioProrrogacao'] = 180;
    $_SESSION['PrimeiroAvisoNotificacaoGestor'] = 7;
    $_SESSION['SegundoAvisoNotificacaoGestor'] = 3;

  
} else {
    $_SESSION['comunicacao'] = 'http://app2.tce.sc.gov.br/comunicacao/rest/';
    $_SESSION['sgi'] = 'http://virtual.tce.sc.gov.br/sgi/rest/';
    $_SESSION['virtual'] = 'http://virtual.tce.sc.gov.br/web/';
    $_SESSION['enviarEmail']  =  true;
    $_SESSION['diasMaxioProrrogacao']  =  180;
    $_SESSION['PrimeiroAvisoNotificacaoGestor'] = 7;
    $_SESSION['SegundoAvisoNotificacaoGestor'] = 3;
}
?>