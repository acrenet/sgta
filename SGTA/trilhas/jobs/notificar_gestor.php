<?php

require_once('../globais.php');  
require_once("../obj/email.php");
require_once("../obj/monitoramento.php");

$obj_email = new email();
$obj_monitoramento = new monitoramento();
//============================aviso de 7 dias=======================================================

    //paramento é o numero de dias antes do vencimento da data do monitoramento (esta no globais)
    $diasMonitoramento = $_SESSION['PrimeiroAvisoNotificacaoGestor'] ;
    $vmSqlMonitoramento = $obj_monitoramento->job_notificacao_monitoramento($diasMonitoramento);

//    echo '<pre>';
    
    $mensagem = null;
    $assunto = "Aviso de registros em monitoramento no S.G.T.A";
    $emaildestinatario = $RegistroSGestor[0]['EMAIL'];

    foreach($vmSqlMonitoramento  as $CpfGestor => $RegistroSGestor){
        
           //Corpo email 
        $mensagem =  null;
       // $mensagem = "<p style='padding: 0px;'>Essa mensagem vai para o e-mail: ". $RegistroSGestor[0]['EMAIL']."</p>" ;
        $mensagem .= "<p style='padding: 0px;'>Prezado(a) ".$RegistroSGestor[0]['GESTOR'] .", os seguintes registros estão com o prazo de monitoramento se encerranto <b>em " . $diasMonitoramento ." dias</b> e requerem a sua atenção:</p>"  ;
        $mensagem .= "<table border='1' style='font: small-caption; font-size: 13px;'> 
                            <tr>
                                <td><b>Id Trilha</td>
                                <td><b>Área</td>
                                <td><b>Cd. Registro</td>
                                <td><b>CPF_CNPJ_Proc</td>
                                <td><b>Nome Descricao</td>
                                <td><b>NomeOrgao</td>      
                            </tr>
                            ";
        foreach($RegistroSGestor as $key=>$Registro){
            //tabela dos registros
            $mensagem .= "  
                            <tr>
                                <td>" .$Registro['CodTrilha'] . "</td>
                                <td>" .$Registro['DescricaoTipo'] . "</td>
                                <td>" .$Registro['CodRegistro'] . "</td>  
                                <td>" .$Registro['CPF_CNPJ_Proc'] . "</td>
                                <td>" .$Registro['Nome_Descricao'] . "</td>
                                <td>" .$Registro['NomeOrgao'] . "</td>     
                            </tr>
                        ";
            
        }
        $mensagem .= "</table >
                    <p style='padding: 0px;'>Para mais detalhes, favor acessar o sistema de trilhas e auditoria.</p> ";
        $mensagem .= "<p style='padding: 0px;'><i>Este é uma mensagem automática, em caso de dúvidas entre em contato com o suporte.</i></p>";  
    }

    if(is_null($mensagem )){
        echo ('Não existe registros que satisfaçam a busca para o aviso de 7 dias.<br>');
    }

    $obj_email->enviarEmail($assunto, $mensagem, $emaildestinatario);

//============================fim aviso de 7 dias=======================================================

//============================aviso de 3 dias=======================================================

    $vmSqlMonitoramento =  null;
    $diasMonitoramento = null;
    //paramento é o numero de dias antes do vencimento da data do monitoramento (esta no globais)
    $diasMonitoramento = $_SESSION['SegundoAvisoNotificacaoGestor'] ;
    $vmSqlMonitoramento = $obj_monitoramento->job_notificacao_monitoramento($diasMonitoramento);
    $mensagem = null;
    $emaildestinatario = $RegistroSGestor[0]['EMAIL'];
    $assunto = "Aviso de registros em monitoramento no S.G.T.A";

    foreach($vmSqlMonitoramento  as $CpfGestor => $RegistroSGestor){
    
        //Corpo email 
        $mensagem =  null;
      //  $mensagem = "<p style='padding: 0px;'>Essa mensagem vai para o e-mail: ". $RegistroSGestor[0]['EMAIL']."</p>" ;
        $mensagem .= "<p style='padding: 0px;'>Prezado(a) ".$RegistroSGestor[0]['GESTOR'] .", os seguintes registros estão com o prazo de monitoramento se encerranto <b>em " . $diasMonitoramento ." dias</b> e requerem a sua atenção:</p>"  ;
        $mensagem .= "<table border='1' style='border-style: dashed;  font: small-caption; font-size: 13px;'> 
                        <tr>
                            <td><b>Id Trilha</td>
                            <td><b>Área</td>
                            <td><b>Cd. Registro</td>
                            <td><b>CPF_CNPJ_Proc</td>
                            <td><b>Nome Descricao</td>
                            <td><b>NomeOrgao</td>      
                        </tr>
                            ";
        foreach($RegistroSGestor as $key=>$Registro){
            //tabela dos registros
            $mensagem .= "  
                        <tr>
                            <td>" .$Registro['CodTrilha'] . "</td>
                            <td>" .$Registro['DescricaoTipo'] . "</td>
                            <td>" .$Registro['CodRegistro'] . "</td>  
                            <td>" .$Registro['CPF_CNPJ_Proc'] . "</td>
                            <td>" .$Registro['Nome_Descricao'] . "</td>
                            <td>" .$Registro['NomeOrgao'] . "</td>     
                        </tr>
                        ";
        }
        $mensagem .= "</table >
                    <p style='padding: 0px;'>Para mais detalhes, favor acessar o sistema de trilhas e auditoria.</p> ";
        $mensagem .= "<p style='padding: 0px;'><i>Este é uma mensagem automática, em caso de dúvidas entre em contato com o suporte.</i></p>";  
    }

    if(is_null($mensagem )){
        echo ('Não existe registros que satisfaçam a busca para o aviso de 3 dias.<br>');
    }

    $obj_email->enviarEmail($assunto, $mensagem, $emaildestinatario);

//============================fim aviso de 3 dias=======================================================
?>