<?php
setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

//INICIALIZA A SESSÃO 
if(!isset($_SESSION)){
    session_start();
}
// andre - ver se podemos adicionar $_SESSION['servidor']
if(isset($__servidor) ){
    if( !isset($_SESSION['sessao_time']) ){   
            $__autenticado = false;
    }else{ 
        if($_SESSION["sessao_time"] < time()){
            $__autenticado = false;
        }else{//A SESSÃO NÃO EXPIROU NO SISTEMA
            $_SESSION['sessao_time'] = time() + 100000; //1 hora para expirar a sessão
            $__autenticado = true;
        }
    }
    $unidade_controle = $_SESSION['unidade_controle'];
} else{
    //SE NÃO TIVER VARIÁVEIS REGISTRADAS
    //DIRECIONA PARA A TELA DE LOGIN.
    if( !isset($_SESSION['sessao_time']) ){   
        session_unset();
        echo"?????";
      //  Header("Location: /trilhas/app/login/login.php?op=1");
         Header("Location: /trilhas/sem_acesso.php");
        die();
    }else{ 
        if($_SESSION["sessao_time"] < time()){
            session_unset();
            echo"?????";
           // Header("Location: /trilhas/app/login/login.php?op=2");
            Header("Location: /trilhas/sem_acesso.php");
            die();
        }else{//A SESSÃO NÃO EXPIROU NO SISTEMA
            $_SESSION['sessao_time'] = time() + 100000  ;//andre - não exipra aqui 
            $unidade_controle = $_SESSION['unidade_controle'];

        }
    }
}

    

$prazo_legal_pos = 10;
$prazo_legal_pre = 10;