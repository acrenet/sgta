<?php

function isdate($date){
    try {
        $char = strpos($date, "/")!==false?"/":"-";
        $date_array = explode($char,$date);
        if(count($date_array)!=3){return false;}
        return checkdate($date_array[1],$date_array[0],$date_array[2])?($date_array[2] . "-" . $date_array[1] . "-" . $date_array[0]):false;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function formata_data_mysql($data){
    try {
        $data = substr($data, 6)."-".substr($data,3,2)."-".substr($data,0,2);
        return $data;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function data_br($data, $hora = false){
    try {
        
        if($hora == true){
            $data = substr($data, 8,2)."/".substr($data,5,2)."/".substr($data,0,4) . substr($data, 10);
        }else{
            $data = substr($data, 8,2)."/".substr($data,5,2)."/".substr($data,0,4);
        }
        
        if($data=="//"){
            $data="";
        }
        
        return $data;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function numero_br($numero){
    try {
        
        if(is_numeric($numero)){
            return number_format($numero,2,",",".");
        }else{
            return "";
        }
          
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function inteiro($numero){
   try {
        
        if(is_numeric($numero)){
            $numero = number_format($numero,0);
            return str_replace(",", ".", $numero);
        }else{
            return "";
        }
          
    } catch (Exception $ex) {
        return $ex->getMessage();
    } 
}

function mysql_data($nome_campo, $dt_inicial, $dt_final, $operador){
    try {
        
        $query = "";
        
        $dt_inicial = formata_data_mysql($dt_inicial);
       
        if($dt_final!=""){
            $dt_final = formata_data_mysql($dt_final);
        }    
            
        if($dt_final==""){
            $query = " TO_DAYS($nome_campo) $operador TO_DAYS(DATE('$dt_inicial')) ";
        }else{
            $query = " TO_DAYS($nome_campo) BETWEEN TO_DAYS(DATE('$dt_inicial')) AND TO_DAYS(DATE('$dt_final')) ";
        }

        return $query;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function mysql_transacao(){
    try{
        global $servidor, $nome_banco, $user, $password;
        
        $mysqli = new mysqli($servidor, $user, $password, $nome_banco);
        if (mysqli_connect_errno()){
            $erro = mysqli_connect_error();
            return "Erro: ".$erro;
        }
        
        mysqli_autocommit( $mysqli, FALSE);
        
        return $mysqli;
    } catch (Exception $ex) {
        return "Erro: ".$ex->getMessage();
    }
}

function data_extenso($Y_m_d = "", $dia_semana = true){
    
    if($Y_m_d == ""){
       $Y_m_d = date("Y-m-d");
    }
    
    $ano    = date('Y', strtotime($Y_m_d));
    $dia    = date('d', strtotime($Y_m_d))-0;
    $dsemana= date('w', strtotime($Y_m_d));
    $data   = date('n', strtotime($Y_m_d));
    
    $mes[1] ='Janeiro';
    $mes[2] ='Fevereiro';
    $mes[3] ='Março';
    $mes[4] ='Abril';
    $mes[5] ='Maio';
    $mes[6] ='Junho';
    $mes[7] ='Julho';
    $mes[8] ='Agosto';
    $mes[9] ='Setembro';
    $mes[10]='Outubro';
    $mes[11]='Novembro';
    $mes[12]='Dezembro';
    $semana[0] = 'Domingo';
    $semana[1] = 'Segunda-Feira';
    $semana[2] = 'Terça-Feira';
    $semana[3] = 'Quarta-Feira';
    $semana[4] = 'Quinta-Feira';
    $semana[5] = 'Sexta-Feira';
    $semana[6] = 'Sádado';
    
    if($dia_semana == true){
        return $semana[$dsemana].', '.$dia.' de '.$mes[$data].' de '.$ano;
    }else{
        return $dia.' de '.$mes[$data].' de '.$ano;
    }
    
}

function valida_email($email){
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return false; 
    }else{
        return true;
    }
}
    