window.chartColors = {
	red: 'rgb(204, 0, 0)',
	orange: 'rgb(255, 51, 0)',
	yellow: 'rgb(255, 204, 0)',
	green: 'rgb(0, 51, 0)',
	blue: 'rgb(0, 0, 153)',
	purple: 'rgb(77, 0, 77)',
	grey: 'rgb(102, 0, 102)'
};

window.randomScalingFactor = function() {
	return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
};