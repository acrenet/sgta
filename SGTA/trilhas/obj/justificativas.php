<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");
require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/code/funcoes.php");

class justificativas{
    private $objcon = "";
    private $objusu = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    function __construct() {
        $this->objcon=new conexao();
        $this->objusu=new usuario();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    
    public function incluir_justificativa($CodRegistro, $TipoJustificativa, $Justificativa){
        try{
            $this->erro = "";
            
            $Justificativa = mysqli_real_escape_string($this->objcon->conexao, $Justificativa);
            $Usuario = $_SESSION['sessao_id'];
            
            $sql = "Insert Into justificativas (CodRegistro, TipoJustificativa, Justificativa, Usuario) Values ($CodRegistro, '$TipoJustificativa', '$Justificativa', '$Usuario')";
            
            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
   
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consultar_justificativas($CodRegistro, $TipoJustificativa = ""){
        try{
            $this->erro = "";
            
            if($TipoJustificativa == ""){
                $sql = "Select * From justificativas Where CodRegistro = $CodRegistro Order By DataJustificativa Desc";
            }else{
                $sql = "Select * From justificativas Where CodRegistro = $CodRegistro And TipoJustificativa = '$TipoJustificativa' Order By DataJustificativa Desc";
            }
            
            
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                $reg[0]['qtd'] = 0;
            }else{
                while ($row = mysqli_fetch_assoc($query)){
                    $reg[] = $row;
                }
                $reg[0]['qtd'] = mysqli_num_rows($query);
            }
            
            if($reg[0]['qtd'] > 0){
                for ($index = 0; $index < count($reg); $index++){

                    $this->objusu->consulta_usuario($reg[$index]["Usuario"]);
                    $query = $this->objusu->query;
                    $row = mysqli_fetch_array($query);

                    $reg[$index]["UsuarioNome"] = $row["Nome"].'<br>('.$row['SiglaOrgao'].' - '.$row['NomePerfil'].')';
                    $reg[$index]["DataJustificativa"] = data_br($reg[$index]["DataJustificativa"]);
                    $reg[$index]['Just'] = $reg[$index]['Justificativa'];
                    $reg[$index]['Justificativa'] = str_replace("\r", '<br>', $reg[$index]['Justificativa']);
                    $reg[$index]["Contato"] = $row['Telefone'].'<br>'.$row['Email'];
                    
                    if($reg[$index]['TipoJustificativa'] == "Análise de Justificativa"){
                        $reg[$index]['TipoJustificativa'] = "Parecer do Auditor";
                    }
                    
                }
            }
   
            return $reg;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
}
