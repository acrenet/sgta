<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");

class suporte{
    
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function salvar($CPF, $Nome, $Telefone, $Email, $Ocorrencia){
        try{
            $this->erro = "";
            
            $Nome = mysqli_real_escape_string($this->con, $Nome);
            $Telefone = mysqli_real_escape_string($this->con, $Telefone);
            $Email = mysqli_real_escape_string($this->con, $Email);
            $Ocorrencia = mysqli_real_escape_string($this->con, $Ocorrencia);
            
            $Nome = preg_replace("/(from|select|insert|delete|update|drop|where|drop table|show tables|#|\*|--|\\\\)/i", '', $Nome);
            $Telefone = preg_replace("/(from|select|insert|delete|update|drop|where|drop table|show tables|#|\*|--|\\\\)/i", '', $Telefone);
            $Email = preg_replace("/(from|select|insert|delete|update|drop|where|drop table|show tables|#|\*|--|\\\\)/i", '', $Email);
            $Ocorrencia = preg_replace("/(from|select|insert|delete|update|drop|where|drop table|show tables|#|\*|--|\\\\)/i", '', $Ocorrencia);
            
            
            $sql = "Insert Into suporte (CPF, Nome, Telefone, Email, Ocorrencia) Values ('$CPF', '$Nome', '$Telefone', '$Email', '$Ocorrencia')";
            
            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function responder($CodSuporte, $Resposta){
        try{
            $this->erro = "";
            
            $Resposta = mysqli_real_escape_string($this->con, $Resposta);
            //$Resposta = preg_replace("/(from|select|insert|delete|update|drop|where|drop table|show tables|#|\*|--|\\\\)/i", '', $Resposta);
            
            $Data = date("Y-m-d h:m:s");
            $Nome = $_SESSION['sessao_id'];
            
            $sql = "Update suporte Set Resposta = '$Resposta', HoraResposta = '$Data', RespondidoPor = '$Nome', Status = 'respondido' Where CodSuporte = $CodSuporte";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $Resposta = str_replace("\r", '<br>', $Resposta);
            
            $sql = "Select Email, Nome, RespondidoPor From suporte Where CodSuporte = $CodSuporte";
            $query = $this->objcon->select($sql);
            $row = mysqli_fetch_array($query);
            $email = $row['Email'];
            $respondidopor = $row['RespondidoPor'];
            
            require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/email.php");
            $obj_email = new email();
            
            require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");
            $obj_usuario = new usuario();
            
            $assunto = "Resposta ao Pedido de Suporte no SGTA.";
            $mensagem = "$Resposta<br><br>";
            $mensagem = $mensagem ."Respondido Por: ".$obj_usuario->consulta_nome($respondidopor).".<br><br>";
            $mensagem = $mensagem ."<i>Este é um e-mail automático, em caso de dúvidas por favor entre em contato com o suporte.</i>"; 
            $obj_email->enviarEmail($assunto, $mensagem, $email);
            $obj_email->enviarEmail($assunto, $mensagem, "nie@tce.sc.gov.br");
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consultar(){
        try{
            $this->erro = "";

            $sql = "Select * From suporte Order By Status, Horario Desc Limit 200";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function contar(){
        try{
            $this->erro = "";
            
             $sql = "Select count(CodSuporte) as qtd From suporte Where status = 'pendente'";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            
            return $row['qtd'];
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
}
