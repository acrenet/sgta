<?php


set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {return false;}
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/andamentos.php");
require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/code/funcoes.php");

class registros{
    private $objcon = "";
    private $objand = "";
    private $con = "";
    public $erro = "";
    public $alerta = "";
    public $query = "";
    public $array = "";
    public $html = "";
    
    function __construct() {
        $this->objcon=new conexao();
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function contar_registros($CodTrilha, $CodOrgao = 0){
        try{
            $this->erro = "";
            
            if($CodOrgao == 0){
                $sql = "Select JustificativaConcluida, AnaliseConcluida, RejeitadoPreAnalise, ReservadoPor, Pre_Analisado, RespObs,StatusRegistro From view_registros Where CodTrilha = $CodTrilha";
                
            }else{
                $sql = "Select JustificativaConcluida, AnaliseConcluida, RejeitadoPreAnalise, ReservadoPor, Pre_Analisado, RespObs,StatusRegistro From view_registros Where CodTrilha = $CodTrilha And CodOrgao = $CodOrgao";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $qtd["total_registros"] = 0;
            $qtd["reg_pendentes_continuos"] = 0;
            $qtd["registros"] = 0;
            $qtd["justificados"] = 0;
            $qtd["pendentes"] = 0;
            $qtd["concluidos"] = 0;
            $qtd["rejeitados"] = 0;
            $qtd["homologados"] = 0;
            $qtd["rascunho"] = 0;
            $qtd["excluido"] =0;
                        
            if(mysqli_num_rows($query) == 0){
                return $qtd;
            }else{
                
                while ($row = mysqli_fetch_array($query)){
                    
                    $qtd["registros"]++;
                    $qtd["total_registros"]++;

                    if($row['StatusRegistro'] =='rascunho'){
                        $qtd["rascunho"]++;
                    }
                    
                    if($row['StatusRegistro'] =='excluído'){
                        $qtd["excluido"]++;
                    }


                    if($row['JustificativaConcluida'] == true){
                        $qtd["justificados"]++;
                    }
                    if($row['AnaliseConcluida'] == true){
                        $qtd["concluidos"]++;
                    }
                    if($row['JustificativaConcluida'] == false && $row["RejeitadoPreAnalise"] == false){
                        $qtd['pendentes']++;
                    }
                    if($row['RejeitadoPreAnalise'] == true){
                        $qtd["rejeitados"]++;
                        $qtd["registros"]--;
                    }
                    if($row['RespObs'] != '' || $row['Pre_Analisado'] == 1){
                        $qtd["homologados"]++;
                    }
                }
                $qtd["reg_pendentes_continuos"]  =   $qtd['total_registros'] -  ($qtd["rascunho"] + $qtd["concluidos"]+ $qtd["excluido"]) ;
                return $qtd;
            }

        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function consulta_registros($CodTrilha, $detalhe, $CodOrgao = 0, $orderby = "",$complemetnoWhere=""){
        try{
            $this->erro = "";
            
            if($CodOrgao == 0){

                $sql = "SELECT registros.*, $detalhe.*, trilhas.CodGrupo, trilhas.DataInicial, trilhas.DataFinal, trilhas.DataTrilha, trilhas.DataPublicacao,
                    trilhas.StatusTrilha, grupos_trilhas.CodTipo, trilhas.Instrucao, grupos_trilhas.NomeTrilha, grupos_trilhas.TabelaDetalhe, grupos_trilhas.VaiAoOrgao, grupos_trilhas.TipoTrilha,
                    tipos_trilhas.DescricaoTipo, orgaos.NomeOrgao, orgaos.SiglaOrgao, view_contar_anexos_registros.QtdAnexos FROM ((((registros INNER JOIN trilhas ON 
                    registros.CodTrilha = trilhas.CodTrilha) INNER JOIN (tipos_trilhas INNER JOIN grupos_trilhas ON tipos_trilhas.CodTipo = 
                    grupos_trilhas.CodTipo) ON trilhas.CodGrupo = grupos_trilhas.CodGrupo) INNER JOIN orgaos ON registros.CodOrgao = 
                    orgaos.CodOrgao) INNER JOIN $detalhe ON registros.CodRegistro = $detalhe.CodReg) LEFT JOIN view_contar_anexos_registros 
                    ON registros.CodRegistro = view_contar_anexos_registros.CodRegistro 
                    WHERE (((registros.CodTrilha)=$CodTrilha)) " . $complemetnoWhere." Order By NomeOrgao, CPF_CNPJ_Proc $orderby;";
                
            }else{
                
                $sql = "SELECT registros.*, $detalhe.*, trilhas.CodGrupo, trilhas.DataInicial, trilhas.DataFinal, trilhas.DataTrilha, trilhas.DataPublicacao,
                    trilhas.StatusTrilha, trilhas.Instrucao, grupos_trilhas.CodTipo, grupos_trilhas.NomeTrilha, grupos_trilhas.TabelaDetalhe, grupos_trilhas.VaiAoOrgao, grupos_trilhas.TipoTrilha,
                    tipos_trilhas.DescricaoTipo, orgaos.NomeOrgao, orgaos.SiglaOrgao, view_contar_anexos_registros.QtdAnexos FROM ((((registros INNER JOIN trilhas ON 
                    registros.CodTrilha = trilhas.CodTrilha) INNER JOIN (tipos_trilhas INNER JOIN grupos_trilhas ON tipos_trilhas.CodTipo = 
                    grupos_trilhas.CodTipo) ON trilhas.CodGrupo = grupos_trilhas.CodGrupo) INNER JOIN orgaos ON registros.CodOrgao = 
                    orgaos.CodOrgao) INNER JOIN $detalhe ON registros.CodRegistro = $detalhe.CodReg) LEFT JOIN view_contar_anexos_registros 
                    ON registros.CodRegistro = view_contar_anexos_registros.CodRegistro
                     WHERE (((registros.CodTrilha)=$CodTrilha) AND ((registros.CodOrgao)=$CodOrgao)) " . $complemetnoWhere." Order By CPF_CNPJ_Proc $orderby; ";
            }

           /// die($sql);
         
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }

    //SGTA 173 - função para verificar se há registros pré-análise nas trilhas continuas
    public function consulta_registros_continuos($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "SELECT * FROM `view_registros` WHERE StatusRegistro = 'rascunho' AND CodTrilha = $CodTrilha";
          //  die($sql);
            
            $query = $this->objcon->select($sql);			
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }

      //SGTA 173 - função para consultar registros excluidos
    public function consulta_registros_excluidos($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "select * from view_registros A inner join registros_deletados B on A.CodRegistro = B.CodRegistro WHERE A.CodTrilha =  $CodTrilha";
          
            
            $query = $this->objcon->select($sql);			
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }

      //SGTA 173 - função para retornar registros excluidos para a trilha
      public function retorna_registro_excluido($CodRegistro){
        try{
            $this->erro = "";
            
            $sql = "DELETE FROM `registros_deletados` WHERE `registros_deletados`.`CodRegistro` = $CodRegistro";
          
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $sql2 = "update registros set StatusRegistro = 'rascunho' where CodRegistro = $CodRegistro";
          //  die( $sql2);
            $this->objcon->execute($sql2);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            return true;

        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }


      //SGTA 173 - função para registrar exclusao do registro
      public function registra_exclusao_registro($CodRegistro,$motivo_exclusao,$CodTrilha,$CodOrgao,$CPF_CNPJ_Proc,$usuario){
        try{
            $this->erro = "";

            $vmDataHoje = date('Y-m-d H:i:s');
           // die($vmDataHoje);
            
            $sql = "INSERT INTO `registros_deletados` ( `CodRegistro`, `motivo_exclusao`, `CodTrilha`, `CodOrgao`, `CPF_CNPJ_Proc`, `usuario`, `data_exclusa`) " ;
            $sql .="  VALUES ('$CodRegistro', '$motivo_exclusao', '$CodTrilha', '$CodOrgao', '$CPF_CNPJ_Proc','$usuario','$vmDataHoje') ;";
            //die(  $sql);
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
         
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }

     //SGTA 173 - basica de consulta da view registros
     public function consulta_registros_basica($where){
        try{
            $this->erro = "";
            
            $sql = "SELECT * FROM `view_registros` WHERE $where";
            die($sql);
            
            $query = $this->objcon->select($sql);			
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }


    //SGTA 173 - função para verificar se há o registro é um continuo e esta no racunho
    public function verifica_registro_rascunho($CodRegistro){
        try{
            $this->erro = "";
            
            $sql = "SELECT * FROM `view_registros` WHERE StatusRegistro = 'rascunho' AND CodRegistro = $CodRegistro";
            // die($sql);
            
            $query = $this->objcon->select($sql);			
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }

        //SGTA 173 - regra para verificar duplicado
    //SD-13207
            /*
        Caso o registro já tenha sido inserido na trilha contínua:
        A mensagem de duplicata deve ser aplicada somente para os casos:

        1) Da duplicata estiver com status "concluída". (Verificar se está em monitoramento)
        2) Da duplicata estiver excluída.

        Regra - se voltar zero, não tem na trilha nem nos excluidos
                voltando 1 tem na trilha
                Voltando 3 tem na trilha e ta concluido tem nos excluidos
                Voltando 5 se tem nos excluidos
                Voltando 8 se 5 e 3 forem verdadeiras
                Voltarndo 9 se tudo é verdade
                tem mais combinações, mas não vou descrevalas aqui. valeu.

    */
    public function RegraVerificaDuplicado($CodTrilha,$CodOrgao,$cpf){
        try{
            $qtdDeletado = 0;
            $qtdDuplicado = 0;
            $qtdDuplicadoConcluido = 0;


            $this->erro = "";


            $sql = "select COUNT(*) as verifica from view_registros where CodTrilha = " . $CodTrilha . " and CodOrgao = " . $CodOrgao .  " and CPF_CNPJ_Proc = '" . $cpf."' ";
    
            //die( $sql );
            $query = $this->objcon->select($sql);		

            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }	

            while ($row = mysqli_fetch_array($query)) {
                if($row['verifica'] > 0){
                    $qtdDuplicado =1;
                }
            }

            
            $sql = "select COUNT(*) as verifica from view_registros where CodTrilha = " . $CodTrilha . " and CodOrgao = " . $CodOrgao .  " and CPF_CNPJ_Proc = '" . $cpf."' and StatusRegistro = 'concluido' ";
    
            //die( $sql );
            $query = $this->objcon->select($sql);		

            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }	

            while ($row = mysqli_fetch_array($query)) {
                if($row['verifica'] > 0){
                    $qtdDuplicadoConcluido =3;
                }
            }


            $sql2 = "select COUNT(*) as verifica from registros_deletados where CodTrilha = " . $CodTrilha . " and CodOrgao = " . $CodOrgao .  " and CPF_CNPJ_Proc = '" . $cpf."'  ";
            
            //die( $sql2 );
            $query2 = $this->objcon->select($sql2);		

            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }	

            while ($row2 = mysqli_fetch_array($query2)) {
                if($row2['verifica'] > 0){
                    $qtdDeletado = 5 ;
                }
            }

            $return = 0;
            $return += ( $qtdDuplicado + $qtdDeletado + $qtdDuplicadoConcluido);
            return $return;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }

    
    public function consulta_registro($CodRegistro){
        try{
            $this->erro = "";
            
            $sql = "Select * from view_registros Where CodRegistro = $CodRegistro";
            
            $query = $this->objcon->select($sql);			
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function consulta_reservados($cpf, $CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "Select CodRegistro, CPF_CNPJ_Proc, Nome_Descricao from registros Where ReservadoPor = '$cpf' And CodTrilha = $CodTrilha And StatusRegistro In ('justificado', 'em análise') Order By Nome_Descricao";
            $query = $this->objcon->select($sql);			
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function mudar_status($CodRegistro, $Status){
        try{
            $this->erro = "";
            
            if($Status == "excluído"){
                $sql = "Update registros Set StatusRegistro = '$Status', RejeitadoPreAnalise = true Where CodRegistro = $CodRegistro";
            }else{
                $sql = "Update registros Set StatusRegistro = '$Status', RejeitadoPreAnalise = false Where CodRegistro = $CodRegistro";
            }
            
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
         
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }

    public function getMaxCodigo(){
        try{
            $this->erro = "";
            
           
            $sql = "SELECT max(codRegistro) as codigo from registros";
          
            $query = $this->objcon->select($sql);			
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function alterar_status($CodTrilha, $Status){
        try{
            $this->erro = "";
               //SGTA 173
            if($Status == "pendente"){
                $sql = "Update registros Set StatusRegistro = '$Status', ReservadoPor = null Where RejeitadoPreAnalise = False And CodTrilha = $CodTrilha And StatusRegistro = 'pré-análise'";
            }elseif ($Status == "rascunho"){
                $sql = "Update registros Set StatusRegistro = '$Status' Where RejeitadoPreAnalise = False And CodTrilha = $CodTrilha And StatusRegistro = 'pré-análise'";
            }elseif ($Status == "pré-análise"){
                $sql = "Update registros Set StatusRegistro = '$Status' Where RejeitadoPreAnalise = False And CodTrilha = $CodTrilha And StatusRegistro = 'rascunho'";
            }else{
                $sql = "Update registros Set StatusRegistro = '$Status' Where RejeitadoPreAnalise = False And CodTrilha = $CodTrilha";
            }
            
            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objand=new andamentos();
            
            if($Status == "pendente"){
                $sql = "Select CodRegistro From registros Where RejeitadoPreAnalise = False And CodTrilha = $CodTrilha And StatusRegistro = 'pendente'";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                while ($row = mysqli_fetch_array($query)){
                    $this->objand->cadastrar($row['CodRegistro'], "Envio do registro para justificativa.");
                }
            }
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function obs_cge($CodRegistro, $ObsCGE, $Agrupar = 0){
        try{
            $this->erro = "";
            
            $ObsCGE = mysqli_real_escape_string($this->objcon->conexao, $ObsCGE);
            
            $CPF = $_SESSION['sessao_id'];
            
            if($Agrupar == 1){
                $sql = "Select CodTrilha, CPF_CNPJ_Proc From registros Where CodRegistro = $CodRegistro";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }

                $row = mysqli_fetch_array($query);

                $sql = "Update registros Set ObsCGE = '$ObsCGE', RespObs = $CPF Where CodTrilha = ".$row['CodTrilha']." And CPF_CNPJ_Proc = ".$row['CPF_CNPJ_Proc'];
            }else{
                 $sql = "Update registros Set ObsCGE = '$ObsCGE', RespObs = $CPF Where CodRegistro = $CodRegistro";
            }
            
            

            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $sql = "Insert Into obs_cge (CodRegistro, RespObs, ObsCge) Values ($CodRegistro, $CPF, '$ObsCGE')";
            $this->objcon->insert($sql);
            
            $this->objand=new andamentos();
            $this->objand->cadastrar($CodRegistro, "Incluído/Modificado Observação TCE");
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function consulta_obs_cge($RespObs){
        try{
            $this->erro = "";
            
            $sql = "Select * From view_obs_cge Where RespObs = $RespObs";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function reservar_registro($CodRegistro, $cpf, $StatusRegistro, $JaReservado = false){
        try{
            $this->erro = "";
            
            if($StatusRegistro == false){
                $sql = "Update registros Set ReservadoPor = $cpf Where CodRegistro = $CodRegistro";
            }else{
                $sql = "Update registros Set ReservadoPor = $cpf, StatusRegistro = '$StatusRegistro' Where CodRegistro = $CodRegistro";
            }
            
            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objand=new andamentos();
            
            if($StatusRegistro == false){
                require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");
                $obj_usuario = new usuario();
                
                if($JaReservado == false){
                    $this->objand->cadastrar($CodRegistro, "Reservado o registro de inconsistência para o servidor: ".$obj_usuario->consulta_nome($cpf));
                }else{
                    $this->objand->cadastrar($CodRegistro, "Redistribuido o registro de inconsistência para o servidor: ".$obj_usuario->consulta_nome($cpf));
                }
                
                require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/ordem_servico.php");
                $obj_os = new ordem_servico();
                $obj_os->transfere_os($CodRegistro, $cpf);
                if($obj_os->erro != ""){
                    $this->erro = $obj_os->erro;
                    return false;
                }
            }else{
                if($JaReservado == false){
                    $this->objand->cadastrar($CodRegistro, "Reservado o registro de inconsistência.");
                }
            }
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function excluir_registros($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "Select TabelaDetalhe From view_registros Where CodTrilha = $CodTrilha Limit 1";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            $TabelaDetalhe = $row['TabelaDetalhe'];
            
            $sql = "Select CodRegistro From registros Where CodTrilha = $CodTrilha";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objcon->iniciar_transacao();
            
            while ($row = mysqli_fetch_array($query)){
                $sql = "Delete From $TabelaDetalhe Where CodReg = ".$row['CodRegistro'];
                $this->objcon->execute($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    $this->objcon->rollback();
                    return false;
                }
            }
            
            $sql = "Delete From registros Where CodTrilha = $CodTrilha";
            $afectedrows = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }
                    
            $this->objcon->commit();
            
            return $afectedrows;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function upload_arquivo($CodRegistro, $NomeAnexo, $DescricaoAnexo, $Usuario, $Etapa){
        try{
            $this->erro = "";
            
            mysqli_real_escape_string($this->objcon->conexao, $DescricaoAnexo);
            //andre verifcar porque o codigo CodRegistro esta vindo como string
            $sql = "Insert Into anexos_registros (CodRegistro, NomeAnexo, DescricaoAnexo, Usuario, Origem, Etapa) Values ($CodRegistro, '$NomeAnexo', "
                    . "'$DescricaoAnexo', $Usuario, '', '$Etapa')";

                   // echo $sql;

            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objand=new andamentos();
            $this->objand->cadastrar($CodRegistro, substr("Anexado o arquivo: ".$NomeAnexo,0,100));
   
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function excluir_anexo($CodAnexo){
        try{
            $this->erro = "";
            
            $sql = "Select * From anexos_registros Where CodAnexo = $CodAnexo";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                $this->erro = "Erro: Arquivo não encontrado.";
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            $NomeAnexo = $row['NomeAnexo'];
            $CodRegistro = $row['CodRegistro'];
   
            $sql = "Delete From anexos_registros Where CodAnexo = $CodAnexo";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objand=new andamentos();
            $this->objand->cadastrar($CodRegistro, substr("Excluído o arquivo: $NomeAnexo",0,100));
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function listar_anexos($CodRegistro){
        try{
            $this->erro = "";
            
            $sql = "Select * From anexos_registros Where CodRegistro = $CodRegistro Order By Horario Desc";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $qtd = mysqli_num_rows($query);
            
            if($qtd == 0){
                $anexos[0]["qtd"] = 0; 
            }else{
                while ($row = mysqli_fetch_assoc($query)){
                    $anexos[] = $row;
                }
                $anexos[0]["qtd"] = $qtd;
                
                for ($index = 0; $index < count($anexos); $index++){
                    
                    $CodAnexo = $anexos[$index]["CodAnexo"];
                    
                    $anexos[$index]["url"] = "";

                    $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/registros/" . $CodRegistro . "/" . $CodAnexo;
                    
                    if(file_exists($targetDir)){
                        $diretorio = scandir($targetDir);
            
                        foreach($diretorio as $arquivo){
                            if (!is_dir($arquivo)){
                                
                                $anexos[$index]["url"] = "/trilhas/intra/registros/$CodRegistro/$CodAnexo/" . $arquivo;
                                
                            }
                        }
                    }
                }
            }
            
            return $anexos;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function salvar_justificativa($CodRegistro, $CodParecer, $JustificativaOrgao, $ValorADevolver, $JustificativaConcluida){
        try{
            $this->erro = "";
            
            $JustificativaOrgao = mysqli_real_escape_string($this->con, $JustificativaOrgao);
            
            //SGTA-159 - travamentdo do updtade pelo codigo do registro
            if($JustificativaConcluida == false){
                $sql = "Update registros Set CodParecer = $CodParecer, JustificativaOrgao = '$JustificativaOrgao', ValorADevolver = '$ValorADevolver' Where CodRegistro = $CodRegistro";
            }else{
                $data = date('Y-m-d');
                $cpf = $_SESSION['sessao_id'];
                
                $sql = "Select UsuarioCGE From registros Where CodRegistro = $CodRegistro";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                $row = mysqli_fetch_array($query);
                if($row['UsuarioCGE'] != ""){
                    $sql = "Update registros Set CodParecer = $CodParecer, JustificativaOrgao = '$JustificativaOrgao', ValorADevolver = '$ValorADevolver', "
                            . "JustificativaConcluida = true, UsuarioOrgao = '$cpf', DataConclusaoJustificativa = '$data', StatusRegistro = 'justificado', "
                            . "ReservadoPor = ".$row['UsuarioCGE']." Where CodRegistro = $CodRegistro";
                }else{
                    //SGTA-97 ReservadoPor = AnalisadoPor
                    $sql = "Update registros Set CodParecer = $CodParecer, JustificativaOrgao = '$JustificativaOrgao', ValorADevolver = '$ValorADevolver', "
                            . " JustificativaConcluida = true, UsuarioOrgao = '$cpf', DataConclusaoJustificativa = '$data', StatusRegistro = 'justificado', "
                            . " ReservadoPor = AnalisadoPor Where CodRegistro = $CodRegistro";
                }
                
                    
            }
           
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objand=new andamentos();
            
            if($JustificativaConcluida == true){
                $this->objand->cadastrar($CodRegistro, "Concluída a Justificativa");
            }
            
            //$this->objcon->log("Conclusão de Justificativa", $sql);
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function salvar_analise($comando, $CodRegistro, $JustificativaCGE, $UsuarioCGE, $QuarentenaAte, $ResultadoFinal, $NumProcAuditoria, 
            $TipoProcesso = "auditoria", $id = "null", $ResultadoAuditoria = "expedição=0;sistemas=0;capacitação=0;devolução=0", 
            $ValorRessarcido = "0", $EconomiaMensal = "0", $InstrumentosLegaisDesc = "", $SistemasGerenciaisDesc = "", $CapacitacaoDesc = "", $Favorecido = ""){
        try{
            $this->erro = "";
            
            $hoje = date("Y-m-d");
            
            $JustificativaCGE = mysqli_real_escape_string($this->con, $JustificativaCGE);
            $InstrumentosLegaisDesc = mysqli_real_escape_string($this->con, $InstrumentosLegaisDesc);
            $SistemasGerenciaisDesc = mysqli_real_escape_string($this->con, $SistemasGerenciaisDesc);
            $CapacitacaoDesc = mysqli_real_escape_string($this->con, $CapacitacaoDesc);
                    
            
            if($QuarentenaAte == false){
                $QuarentenaAte = "null";
            }else{
                $Quarent = data_br($QuarentenaAte);
                $QuarentenaAte = "'".$QuarentenaAte."'";
            }
            
            if($NumProcAuditoria == ""){
                $NumProcAuditoria = "null";
            }
                
            $acao = "";
            $Resultado = "";
            
            if($comando == "salvar_justificativa"){
                $sql = "Update registros Set JustificativaCGE = '$JustificativaCGE', UsuarioCGE = '$UsuarioCGE', QuarentenaAte = "
                        . "$QuarentenaAte, ResultadoFinal = $ResultadoFinal, SolicitadoAuditoria = false, NumProcAuditoria = $NumProcAuditoria, "
                        . "ResultadoAuditoria = '$ResultadoAuditoria', ValorRessarcido = '$ValorRessarcido', EconomiaMensal = '$EconomiaMensal', "
                        . "InstrumentosLegaisDesc = '$InstrumentosLegaisDesc', SistemasGerenciaisDesc = '$SistemasGerenciaisDesc', "
                        . "CapacitacaoDesc = '$CapacitacaoDesc', Favorecido = '$Favorecido' Where CodRegistro = $CodRegistro";
                $log = "";
            }else if($comando == "recusar_justificativa"){//SGTA-89
                $sql = "Update registros Set JustificativaCGE = '$JustificativaCGE', UsuarioCGE = '$UsuarioCGE', QuarentenaAte = null, "
                        . "DataConclusaoAnalise = '$hoje', ReservadoPor = null, StatusRegistro = 'devolvido', AnaliseConcluida = false, "
                        . "JustificativaConcluida = false, ResultadoFinal = $ResultadoFinal, SolicitadoAuditoria = false, NumProcAuditoria = null, "
                        . "ResultadoAuditoria = '$ResultadoAuditoria', ValorRessarcido = '$ValorRessarcido', EconomiaMensal = '$EconomiaMensal', "
                        . "InstrumentosLegaisDesc = '$InstrumentosLegaisDesc', SistemasGerenciaisDesc = '$SistemasGerenciaisDesc', "
                        . "CapacitacaoDesc = '$CapacitacaoDesc', Favorecido = '$Favorecido' Where CodRegistro = $CodRegistro";
                $log = "Devolvido o registro de inconsistência ao órgão.";
            }else if($comando == "quarentena"){
                $sql = "Update registros Set JustificativaCGE = '$JustificativaCGE', UsuarioCGE = '$UsuarioCGE', QuarentenaAte = "
                        . "$QuarentenaAte, DataConclusaoAnalise = '$hoje', ReservadoPor = null, StatusRegistro = 'concluído', AnaliseConcluida "
                        . "= true, ResultadoFinal = $ResultadoFinal, SolicitadoAuditoria = false, NumProcAuditoria = null, "
                        . "ResultadoAuditoria = '$ResultadoAuditoria', ValorRessarcido = '$ValorRessarcido', EconomiaMensal = '$EconomiaMensal', "
                        . "InstrumentosLegaisDesc = '$InstrumentosLegaisDesc', SistemasGerenciaisDesc = '$SistemasGerenciaisDesc', "
                        . "CapacitacaoDesc = '$CapacitacaoDesc', Favorecido = '$Favorecido' Where CodRegistro = $CodRegistro";
                $log = "Registro de inconsistência colocado em monitoramento até: ".  $Quarent;
                $acao = "fechar";
                $Resultado = "monitoramento";
            }else if($comando == "aceitar_justificativa"){
                $sql = "Update registros Set JustificativaCGE = '$JustificativaCGE', UsuarioCGE = '$UsuarioCGE', QuarentenaAte = "
                        . "null, DataConclusaoAnalise = '$hoje', ReservadoPor = null, StatusRegistro = 'concluído', AnaliseConcluida "
                        . "= true, ResultadoFinal = $ResultadoFinal, SolicitadoAuditoria = false, NumProcAuditoria = null, "
                        . "ResultadoAuditoria = '$ResultadoAuditoria', ValorRessarcido = '$ValorRessarcido', EconomiaMensal = '$EconomiaMensal', "
                        . "InstrumentosLegaisDesc = '$InstrumentosLegaisDesc', SistemasGerenciaisDesc = '$SistemasGerenciaisDesc', "
                        . "CapacitacaoDesc = '$CapacitacaoDesc', Favorecido = '$Favorecido' Where CodRegistro = $CodRegistro";
                $log = "Registro arquivado.";
                $acao = "fechar";
                $Resultado = "arquivo";
            }else if($comando == "abrir_auditoria"){
                $sql = "Update registros Set JustificativaCGE = '$JustificativaCGE', UsuarioCGE = '$UsuarioCGE', QuarentenaAte = "
                        . "null, DataConclusaoAnalise = '$hoje', ReservadoPor = null, StatusRegistro = 'concluído', AnaliseConcluida "
                        . "= true, ResultadoFinal = $ResultadoFinal, SolicitadoAuditoria = true, NumProcAuditoria = null, "
                        . "ResultadoAuditoria = '$ResultadoAuditoria', ValorRessarcido = '$ValorRessarcido', EconomiaMensal = '$EconomiaMensal', "
                        . "InstrumentosLegaisDesc = '$InstrumentosLegaisDesc', SistemasGerenciaisDesc = '$SistemasGerenciaisDesc', "
                        . "CapacitacaoDesc = '$CapacitacaoDesc' Where CodRegistro = $CodRegistro";
                $log = "Recomendado Abertura de Processo de Inspeção/Auditoria.";
                $acao = "fechar";
                $Resultado = "auditoria";
            }else if($comando == "confirmar_auditoria"){
                
                $CodFuncao = $_SESSION['sessao_funcao'];
                if($CodFuncao != 3){ //superintendente
                    $this->erro = "Acesso Negado. Somente servidores com função de superintendente podem executar este comando.";
                    return false;
                }
                
                if($TipoProcesso == "arquivar"){
                    $sql = "Update registros Set SolicitadoAuditoria = false, NumProcAuditoria = null, TipoProcesso = null "
                            . "Where CodRegistro = $CodRegistro";
                    $log = "Negado abertura de processo de inspeção/auditoria e arquivado o registro.";
                }else{
                    $sql = "Update registros Set SolicitadoAuditoria = true, NumProcAuditoria = '$NumProcAuditoria', TipoProcesso = '$TipoProcesso' "
                            . "Where CodRegistro = $CodRegistro";
                    $log = "Aberto processo de $TipoProcesso número: $NumProcAuditoria.";
                }
            }
            
            if($log != ""){
                $this->objand=new andamentos();
                $this->objand->cadastrar($CodRegistro, $log);
                if($this->objand->erro != ""){
                    $this->erro = $this->objand->erro;
                    return false;
                }
            }
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if($acao == "fechar"){
                require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/ordem_servico.php");
                $obj_os = new ordem_servico();
                $obj_os->fechar_os($CodRegistro, $Resultado, $id);
                if($obj_os->erro != ""){
                    $this->erro = $obj_os->erro;
                    return false;
                }
            }
            
            //$this->objcon->log("Conclusão de Análise", $sql);
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function solicitar_alteracao($CodRegistro, $MotivoSolicitacao){
        try{
            $this->erro = "";
            
            $hoje = date("Y-m-d");
            $cpf = $_SESSION['sessao_id'];
            $MotivoSolicitacao = mysqli_real_escape_string($this->con, $MotivoSolicitacao);
            
            $sql = "Update registros Set SolicitadoAlteracao = true, DataSolicitacao = '$hoje', SolicitadoPor = '$cpf', MotivoSolicitacao = "
                    . "'$MotivoSolicitacao' Where CodRegistro = $CodRegistro";
            
            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objand=new andamentos();
            $this->objand->cadastrar($CodRegistro, "Solicitado alteração de justificativa.");
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function responder_alteracao($CodRegistro, $ParecerSolicitacao, $SolicitacaoAceita){
        try{
            $this->erro = "";
            
            $hoje = date("Y-m-d");
            $cpf = $_SESSION['sessao_id'];
            $ParecerSolicitacao = mysqli_real_escape_string($this->con, $ParecerSolicitacao);
            
            $sql = "Select SolicitadoPor From registros Where CodRegistro = $CodRegistro";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            $row = mysqli_fetch_array($query);
            $SolicitadoPor = $row['SolicitadoPor'];

            //ATENÇÃO andre
            $varAuxSql = " ";
            if($SolicitadoPor != "" and $SolicitadoPor != 'NULL'){
                $varAuxSql = ", ReservadoPor = '$SolicitadoPor' ";
            }
            
            if($SolicitacaoAceita == true){
                $sql = "Update registros Set SolicitacaoAceita = true, ParecerSolicitacao = '$ParecerSolicitacao', AnalisadoPor = "
                    . "'$cpf', DataAnalise = '$hoje', StatusRegistro = 'em justificativa', JustificativaConcluida = false, "
                    . "DataConclusaoJustificativa = null  " . $varAuxSql .  " Where CodRegistro = $CodRegistro";
            }else{
                $sql = "Update registros Set SolicitacaoAceita = false, ParecerSolicitacao = '$ParecerSolicitacao', AnalisadoPor = "
                        . "'$cpf', DataAnalise = '$hoje' Where CodRegistro = $CodRegistro";
            }
            
            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objand=new andamentos();
            if($SolicitacaoAceita == true){
                $this->objand->cadastrar($CodRegistro, "Pedido de alteração de justificativa aceito.");
            }else{
                $this->objand->cadastrar($CodRegistro, "Pedido de alteração de justificativa negado.");
            }
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function consulta_status($CodRegistro){
        try{
            $this->erro = "";
            
            $sql = "Select StatusRegistro From registros Where CodRegistro = $CodRegistro";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            $row = mysqli_fetch_array($query);
            $status = $row['StatusRegistro'];
        
            return $status;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function acoes_complementares($CodRegistro, $acao, $JustificativaCGE, $QuarentenaAte, $NumProcAuditoria, $ResultadoAuditoria, $ValorRessarcido,
            $EconomiaMensal, $InstrumentosLegaisDesc = "", $SistemasGerenciaisDesc = "", $CapacitacaoDesc = ""){
        try{
            $this->erro = "";
            
            $data = date('Y-m-d');
            
            $InstrumentosLegaisDesc = mysqli_real_escape_string($this->con, $InstrumentosLegaisDesc);
            $SistemasGerenciaisDesc = mysqli_real_escape_string($this->con, $SistemasGerenciaisDesc);
            $CapacitacaoDesc = mysqli_real_escape_string($this->con, $CapacitacaoDesc);

            $cpf = $_SESSION['sessao_id'];
            $JustificativaCGE = mysqli_real_escape_string($this->con, $JustificativaCGE);
            $os = false;
            
            $this->objand=new andamentos();
            
            if($acao == "alterar_prazo"){
                $sql = "Update registros Set QuarentenaAte = '$QuarentenaAte', JustificativaCGE = '$JustificativaCGE', "
                      . "ResultadoAuditoria = '$ResultadoAuditoria', ValorRessarcido = '$ValorRessarcido', EconomiaMensal = '$EconomiaMensal', "
                        . "InstrumentosLegaisDesc = '$InstrumentosLegaisDesc', SistemasGerenciaisDesc = '$SistemasGerenciaisDesc', CapacitacaoDesc = '$CapacitacaoDesc'Where CodRegistro = $CodRegistro;";
                $this->objcon->execute($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                $this->objand->cadastrar($CodRegistro, "Alterado o prazo de monitoramento do registro para: ".  $QuarentenaAte);
            }else if($acao == "arquivar_processo"){
                $sql = "Update registros Set QuarentenaAte = null, JustificativaCGE = '$JustificativaCGE', ResultadoFinal = 1, "
                     . "ResultadoAuditoria = '$ResultadoAuditoria', ValorRessarcido = '$ValorRessarcido', EconomiaMensal = '$EconomiaMensal',"
                        . "InstrumentosLegaisDesc = '$InstrumentosLegaisDesc', SistemasGerenciaisDesc = '$SistemasGerenciaisDesc', CapacitacaoDesc = '$CapacitacaoDesc' Where CodRegistro = $CodRegistro;";
                $this->objcon->execute($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                $this->objand->cadastrar($CodRegistro, "Arquivado o registro e encerrado o monitoramento.");
            }else if($acao == "abrir_auditoria"){ //solicitar auditoria
                $sql = "Update registros Set QuarentenaAte = null, SolicitadoAuditoria = true, NumProcAuditoria = null, JustificativaCGE = '$JustificativaCGE', "
                        . "ResultadoFinal = 3, ResultadoAuditoria = '$ResultadoAuditoria', ValorRessarcido = '$ValorRessarcido', EconomiaMensal = '$EconomiaMensal',"
                        . "InstrumentosLegaisDesc = '$InstrumentosLegaisDesc', SistemasGerenciaisDesc = '$SistemasGerenciaisDesc', CapacitacaoDesc = '$CapacitacaoDesc' Where CodRegistro = $CodRegistro;";
                $this->objcon->execute($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                $this->objand->cadastrar($CodRegistro, "Encerrado o monitoramento, arquivado o registro e solicitado abertura de processo de inspeção/auditoria.");
            }else if($acao == "reabrir_processo"){ //nova os
                $sql = "Update registros Set StatusRegistro = 'em análise', ReservadoPor = '$cpf',  DataConclusaoAnalise = null, "
                        . "AnaliseConcluida = false, SolicitadoAuditoria = false Where CodRegistro = $CodRegistro;";
                $this->objcon->execute($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                $this->objand->cadastrar($CodRegistro, "Reaberto o registro de inconsistência.");
                $os = true;
            }else if($acao == "invocar_processo"){ //transferir os
                $sql = "Update registros Set StatusRegistro = 'em análise', ReservadoPor = '$cpf',  DataConclusaoAnalise = null, JustificativaOrgao = "
                        . "'Órgão/UNIDADE ADMINISTRATIVA NÃO APRESENTOU JUSTIFICATIVA NO PRAZO LEGAL', JustificativaConcluida = true, UsuarioOrgao = '$cpf', DataConclusaoJustificativa = '$data', "
                        . "AnaliseConcluida = false Where CodRegistro = $CodRegistro;";
                $this->objcon->execute($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                
                require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/justificativas.php");
                $obj_justificativas = new justificativas();
                
                $obj_justificativas->incluir_justificativa($CodRegistro, "Justificativa de Inconsistência", 'Órgão/UNIDADE ADMINISTRATIVA NÃO APRESENTOU JUSTIFICATIVA NO PRAZO LEGAL');
                if($obj_justificativas->erro != ""){
                    $this->erro = $obj_justificativas->erro;
                    return false;
                }
                
                $this->objand->cadastrar($CodRegistro, "Invocado o registro por não ter sido justificado no prazo legal.");
                
                $os = true;
            }
            
            if($os == true){
                require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/ordem_servico.php");
                $obj_os = new ordem_servico();
                $obj_os->transfere_os($CodRegistro, $cpf);
                if($obj_os->erro != ""){
                    $this->erro = $obj_os->erro;
                    return false;
                }
            }
        
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    function consulta_mesclar($CodRegistro){
        try{
            $this->erro = "";
            
            $sql = "Select CodTrilha, CodOrgao From registros Where CodRegistro = $CodRegistro";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            $row = mysqli_fetch_array($query);
            $CodTrilha = $row['CodTrilha'];
            $CodOrgao = $row['CodOrgao'];
            
            $sql = "Select CodRegistro, CPF_CNPJ_Proc, Nome_Descricao From registros Where CodTrilha = $CodTrilha And CodOrgao = $CodOrgao And CodRegistro <> $CodRegistro And StatusRegistro = 'pré-análise' Order By Nome_Descricao";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    function mesclar_registros($CodRegistro1, $CodRegistro2){
        try{
            $this->erro = "";
            
            $sql = "Select CPF_CNPJ_Proc, Nome_Descricao, TabelaDetalhe From view_registros Where CodRegistro = $CodRegistro1";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            $row = mysqli_fetch_array($query);
            $cpf1 = $row['CPF_CNPJ_Proc'];
            $nome1 = $row['Nome_Descricao'];
            $TabDetalhe = $row['TabelaDetalhe'];
            
            $sql = "Select CPF_CNPJ_Proc, Nome_Descricao From view_registros Where CodRegistro = $CodRegistro2";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            $row = mysqli_fetch_array($query);
            $cpf2 = $row['CPF_CNPJ_Proc'];
            $nome2 = $row['Nome_Descricao'];
            
            $this->objcon->iniciar_transacao();
            
            $sql = "Update andamento_registro Set CodRegistro = $CodRegistro1 Where CodRegistro = $CodRegistro2";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }
            
            $sql = "Update anexos_registros Set CodRegistro = $CodRegistro1 Where CodRegistro = $CodRegistro2";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }
            
            $sql = "Update $TabDetalhe Set CodReg = $CodRegistro1 Where CodReg = $CodRegistro2";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }
            
            $NovoCpf = substr($cpf1 . " -E- " . $cpf2, 0, 60);
            $NovoNome = substr($nome1 . " -E- " . $nome2, 0, 400);
            
            $sql = "Update registros Set CPF_CNPJ_Proc = '$NovoCpf', Nome_Descricao = '$NovoNome' Where CodRegistro = $CodRegistro1";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }
            
            $sql = "Delete From registros Where CodRegistro = $CodRegistro2";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }
            
            $this->objcon->commit();
            
            $this->objand=new andamentos();
            $this->objand->cadastrar($CodRegistro1, "Mesclado o registro com $cpf2.");
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
    public function concluir_pre_analise($CodRegistro){
        try{
            $this->erro = "";
            
            $cpf = $_SESSION['sessao_id'];
            
            $sql = "Update registros Set Pre_Analisado = true, ReservadoPor = '$cpf' Where CodRegistro = $CodRegistro";
          //  die( $sql);
            
            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objand=new andamentos();
            $this->objand->cadastrar($CodRegistro, "Concluída a homologação do registro: $CodRegistro.");
            
            require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/ordem_servico.php");
            $obj_os = new ordem_servico();
            $obj_os->transfere_os($CodRegistro, $cpf);
            if($obj_os->erro != ""){
                $this->erro = $obj_os->erro;
                return false;
            }
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return -1;
        }
    }
    
}
