<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");
require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/code/funcoes.php");


class andamentos{
    private $objcon = "";
    private $objusu = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function cadastrar($CodRegistro, $Descricao, $Detalhe = ""){
        try{
            $this->erro = "";
            
            $Usuario = $_SESSION['sessao_id'];
            
            $sql = "Insert Into andamento_registro (CodRegistro, Usuario, Descricao, Detalhe) Values ($CodRegistro, '$Usuario', '$Descricao', '$Detalhe')";
            
            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
   
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consultar($CodRegistro, $And = ""){
        try{
            $this->erro = "";

            $sql = "Select * From andamento_registro Where CodRegistro = $CodRegistro $And Order By DataAndamento Desc";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objusu=new usuario();
            
            if(mysqli_num_rows($query) == 0){
                $row[0]['qtd'] = 0;
            }else{
                while ($reg = mysqli_fetch_assoc($query)){
                    $row[] = $reg;
                }
                
                $row[0]['qtd'] = mysqli_num_rows($query);
                
                for ($index = 0; $index < count($row); $index++){
                    
                    $Data = date('d/m/Y H:i:s',strtotime($row[$index]['DataAndamento']));
                    
                    $dia = new DateTime($row[$index]['DataAndamento']);
                    $hoje = new DateTime();
                    $dias = $hoje->diff($dia);
                    
                    $row[$index]["UsuarioNome"] = $this->objusu->consulta_nome($row[$index]['Usuario']);
                    $row[$index]["DataAndamento"] = $Data;
                    $row[$index]["Dias"] = $dias->days;
                    $row[$index]["Contato"] = $this->objusu->telefone . " - ". $this->objusu->email;
                }
            }
            
            return $row;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }

    public function consultar_inconsistencia_justificativas(){
        try{
            $this->erro = "";

            $sql = " SELECT a.CodRegistro, a.DataAndamento, a.Usuario, a.Descricao, b.JustificativaOrgao FROM andamento_registro a "
            . " INNER join registros b on a.CodRegistro = b.CodRegistro where a.descricao = 'Concluída a Justificativa' and a.CodRegistro in " 
            . " (SELECT a.CodRegistro FROM registros a where a.JustificativaOrgao not in (select justificativa from justificativas)) ";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objusu=new usuario();
            
            if(mysqli_num_rows($query) == 0){
                $row[0]['qtd'] = 0;
            }else{
                while ($reg = mysqli_fetch_assoc($query)){
                    $row[] = $reg;
                }
                
                $row[0]['qtd'] = mysqli_num_rows($query);
                
                for ($index = 0; $index < count($row); $index++){
                    
                    $Data = date('d/m/Y H:i:s',strtotime($row[$index]['DataAndamento']));
                    
                    $dia = new DateTime($row[$index]['DataAndamento']);
                    $hoje = new DateTime();
                    $dias = $hoje->diff($dia);
                    
                    $row[$index]["CodRegistro"] =  $row[$index]['CodRegistro'];
                    $row[$index]["Descricao"] =  $row[$index]['Descricao'];
                    $row[$index]["JustificativaOrgao"] =  $row[$index]['JustificativaOrgao'];

                    $row[$index]["UsuarioNome"] = $this->objusu->consulta_nome($row[$index]['Usuario']);
                    $row[$index]["DataAndamento"] = $row[$index]['DataAndamento'];
                    $row[$index]["Dias"] = $dias->days;
                    $row[$index]["Contato"] = $this->objusu->telefone . " - ". $this->objusu->email;
                }
            }
            
            return $row;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consultar_observacoes($CodRegistro, $ExibirTudo = true){
        try{
            $this->erro = "";

            if($ExibirTudo == true){
                $sql = "Select * From obs_cge Where CodRegistro = $CodRegistro Order By CodObs Desc";
            }else{
                $sql = "Select * From obs_cge Where CodRegistro = $CodRegistro And StatusObservacao = 'público' Order By CodObs Desc";
            }
                
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objusu=new usuario();
            
            if(mysqli_num_rows($query) == 0){
                $row[0]['qtd'] = 0;
            }else{
                while ($reg = mysqli_fetch_assoc($query)){
                    $row[] = $reg;
                }
                
                $row[0]['qtd'] = mysqli_num_rows($query);
                
                for ($index = 0; $index < count($row); $index++){
                    
                    $Data = date('d/m/Y H:i:s',strtotime($row[$index]['Data']));
                    
                    $row[$index]["Nome"] = $this->objusu->consulta_nome($row[$index]['RespObs']);
                    $row[$index]["Data"] = $Data;
                    $row[$index]["Contato"] = $this->objusu->telefone . " - ". $this->objusu->email;
                }
            }
            
            return $row;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    
    public function exibir_observacoes($CodObs, $Acao){
        try{
            $this->erro = "";

            if($Acao == "exibir"){
                $sql = "Update obs_cge Set StatusObservacao = 'público' Where CodObs = $CodObs";
            }else{
                $sql = "Update obs_cge Set StatusObservacao = 'privado' Where CodObs = $CodObs";
            }
                
            $query = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
}
