<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");


class ordem_servico{
    private $objcon = "";
    private $objusu = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    public $numrows = 0;
    
    function __construct() {
        $this->objcon=new conexao();
        $this->objusu=new usuario();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    //SGTA 173  para a trilha continua
    public function cria_os_registro($CodRegistro){
        try{
            $this->erro = "";
            
            $cpf = $_SESSION['sessao_id'];
            $hoje = new DateTime();
            $ano = $hoje->format("Y");
            $sql = "Select NumOrdem From ordem_servico Where AnoOrdem = $ano Order By NumOrdem Desc Limit 1";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                $num_os = 1;
            }else{
                $row = mysqli_fetch_array($query);
                $num_os = $row['NumOrdem'];
                $num_os++;
            }
            
            $sql = "Select CodRegistro, CodTrilha, NomeTrilha, CPF_CNPJ_Proc, Nome_Descricao, DescricaoTipo, SiglaOrgao From view_registros "
                    . "Where StatusTrilha <> 'excluído' And CodRegistro = $CodRegistro";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objcon->iniciar_transacao();
            
            while ($row = mysqli_fetch_array($query)){
                
                $descricao = "Trilha de Auditoria: ".$row['NomeTrilha'].", Área: ".$row['DescricaoTipo'].", Código: ".$row['CodTrilha']
                        .", Identificador: ".$row['CPF_CNPJ_Proc'].", Nome/Descrição: ".$row['Nome_Descricao'].", Órgão: ".$row['SiglaOrgao'].".";
                
                
                $sql = "Select CodOrdem From ordem_servico Where CodRegistro = $CodRegistro";
                $query2 = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    $this->objcon->rollback();
                    return false;
                }
                
                if(mysqli_num_rows($query2) == 0){
                    $sql = "Insert Into ordem_servico (NumOrdem, AnoOrdem, CodRegistro, CPF, Descricao) Values ($num_os, $ano, $CodRegistro, $cpf, '$descricao')";
                    $id = $this->objcon->insert($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        $this->objcon->rollback();
                        return false;
                    }
                    
                    $sql = "Insert Into ordem_servico_historico (CodOrdem, CPF, Historico) Values ($id, $cpf, 'Cadastro da Ordem de Serviço de Trilhas (liberação do registro).')";
                    $this->objcon->insert($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        $this->objcon->rollback();
                        return false;
                    }
                    $num_os++;
                }else{
                    $row2 = mysqli_fetch_array($query2);
                    
                    $sql = "Update ordem_servico Set CPF = $cpf Where CodRegistro = $CodRegistro";
                    $this->objcon->execute($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        $this->objcon->rollback();
                        return false;
                    }

                    $sql = "Insert Into ordem_servico_historico (CodOrdem, CPF, Historico) Values (".$row2['CodOrdem'].", ".$cpf.", 'Transferência da Órdem de Serviço para o CPF: $cpf (liberação do registro).')";
                    $this->objcon->insert($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        $this->objcon->rollback();
                        return false;
                    }
                    
                    
                }
            }
            
            $this->objcon->commit();
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function cria_os($CodTrilha){
        try{
            $this->erro = "";
            
            $cpf = $_SESSION['sessao_id'];
            $hoje = new DateTime();
            $ano = $hoje->format("Y");
            $sql = "Select NumOrdem From ordem_servico Where AnoOrdem = $ano Order By NumOrdem Desc Limit 1";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                $num_os = 1;
            }else{
                $row = mysqli_fetch_array($query);
                $num_os = $row['NumOrdem'];
                $num_os++;
            }
            
            $sql = "Select CodRegistro, CodTrilha, NomeTrilha, CPF_CNPJ_Proc, Nome_Descricao, DescricaoTipo, SiglaOrgao From view_registros "
                    . "Where StatusTrilha <> 'excluído' And CodTrilha = $CodTrilha";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objcon->iniciar_transacao();
            
            while ($row = mysqli_fetch_array($query)){
                $CodRegistro = $row['CodRegistro'];
                
                $descricao = "Trilha de Auditoria: ".$row['NomeTrilha'].", Área: ".$row['DescricaoTipo'].", Código: ".$row['CodTrilha']
                        .", Identificador: ".$row['CPF_CNPJ_Proc'].", Nome/Descrição: ".$row['Nome_Descricao'].", Órgão: ".$row['SiglaOrgao'].".";
                
                
                $sql = "Select CodOrdem From ordem_servico Where CodRegistro = $CodRegistro";
                $query2 = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    $this->objcon->rollback();
                    return false;
                }
                
                if(mysqli_num_rows($query2) == 0){
                    $sql = "Insert Into ordem_servico (NumOrdem, AnoOrdem, CodRegistro, CPF, Descricao) Values ($num_os, $ano, $CodRegistro, $cpf, '$descricao')";
                    $id = $this->objcon->insert($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        $this->objcon->rollback();
                        return false;
                    }
                    
                    $sql = "Insert Into ordem_servico_historico (CodOrdem, CPF, Historico) Values ($id, $cpf, 'Cadastro da Ordem de Serviço de Trilhas (liberação da trilha).')";
                    $this->objcon->insert($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        $this->objcon->rollback();
                        return false;
                    }
                    $num_os++;
                }else{
                    $row2 = mysqli_fetch_array($query2);
                    
                    $sql = "Update ordem_servico Set CPF = $cpf Where CodRegistro = $CodRegistro";
                    $this->objcon->execute($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        $this->objcon->rollback();
                        return false;
                    }

                    $sql = "Insert Into ordem_servico_historico (CodOrdem, CPF, Historico) Values (".$row2['CodOrdem'].", ".$cpf.", 'Transferência da Órdem de Serviço para o CPF: $cpf (liberação da trilha).')";
                    $this->objcon->insert($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        $this->objcon->rollback();
                        return false;
                    }
                    
                    
                }
            }
            
            $this->objcon->commit();
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function cadastrar_os($CodRegistro){
        try{
            $this->erro = "";
            
            $cpf = $_SESSION['sessao_id'];
            $hoje = new DateTime();
            $ano = $hoje->format("Y");
            $sql = "Select NumOrdem From ordem_servico Where AnoOrdem = $ano Order By NumOrdem Desc Limit 1";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                $num_os = 1;
            }else{
                $row = mysqli_fetch_array($query);
                $num_os = $row['NumOrdem'];
                $num_os++;
            }
            
            $sql = "Select CodRegistro, CodTrilha, NomeTrilha, CPF_CNPJ_Proc, Nome_Descricao, DescricaoTipo, SiglaOrgao From view_registros "
                    . "Where CodRegistro = $CodRegistro";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            $row = mysqli_fetch_array($query);
            
            $CodRegistro = $row['CodRegistro'];
            $descricao = "Trilha: ".$row['NomeTrilha'].", Área: ".$row['DescricaoTipo'].", Código: ".$row['CodTrilha']
                    .", CPF/CNPJ/Processo: ".$row['CPF_CNPJ_Proc'].", Nome/Descrição: ".$row['Nome_Descricao'].", Órgão: ".$row['SiglaOrgao'].".";

            $sql = "Insert Into ordem_servico (NumOrdem, AnoOrdem, CodRegistro, CPF, Descricao) Values ($num_os, $ano, $CodRegistro, $cpf, '$descricao')";
            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $sql = "Insert Into ordem_servico_historico (CodOrdem, CPF, Historico) Values ($id, $cpf, 'Cadastro da Ordem de Serviço de Trilhas.')";
            $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }    
    }
    
    public function transfere_os($CodRegistro, $CPF){ 
        try{
            $this->erro = "";
            
            inicio:
                
            $sql = "Select CodOrdem, CPF From ordem_servico Where StatusOS = 'aberto' And CodRegistro = $CodRegistro";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                $this->cadastrar_os($CodRegistro);
                if($this->erro != ""){
                    return false;
                }
                goto inicio;
            }
             
            $row = mysqli_fetch_array($query);
            
            if($row['CPF'] != $CPF or isset($_POST['id']) ){
                $this->objcon->iniciar_transacao();

                $sql = "Update ordem_servico Set CPF = $CPF Where CodRegistro = $CodRegistro";
               
                $this->objcon->execute($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    $this->objcon->rollback();
                    return false;
                }

                $sql = "Insert Into ordem_servico_historico (CodOrdem, CPF, Historico) Values (".$row['CodOrdem'].", ".$_SESSION['sessao_id'].", 'Transferência da Órdem de Serviço para o CPF: $CPF.')";
                $this->objcon->insert($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    $this->objcon->rollback();
                    return false;
                }

                $this->objcon->commit();
            } 
            
        return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }    
    }
    
    public function fechar_os($CodRegistro, $Resultado, $id = "null"){
        try{
            $this->erro = "";
            
            $agora = new DateTime();
            $agora = $agora->format("Y-m-d h:i:s");
            
            $sql = "Select CodOrdem From ordem_servico Where StatusOS = 'aberto' And CodRegistro = $CodRegistro";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                return true;
            }
            
            $row = mysqli_fetch_array($query);
            
            $this->objcon->iniciar_transacao();
            
            $sql = "Update ordem_servico Set DataFechamento = '$agora', StatusOS = 'fechado', Resultado = '$Resultado', CodJustificativa = $id Where CodOrdem = ".$row['CodOrdem'];
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }

            $sql = "Insert Into ordem_servico_historico (CodOrdem, CPF, Historico) Values (".$row['CodOrdem'].", ".$_SESSION['sessao_id'].", 'Fechamento da Ordem de Serviço')";
            $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }

            $this->objcon->commit();
            
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function exclui_os($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "Select CodRegistro From view_ordem_servico Where CodTrilha = $CodTrilha";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                return true;
            }
            
            $this->objcon->iniciar_transacao();
            
            while ($row = mysqli_fetch_array($query)){
                $sql = "Delete From ordem_servico Where CodRegistro = ".$row['CodRegistro'];
                $this->objcon->execute($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    $this->objcon->rollback();
                    return false;
                }
            }
            
            $this->objcon->commit();
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    //SGTA 173 - exclui_os_registro para trilha continua
    public function exclui_os_registro($CodRegistro){
        try{
            $this->erro = "";
            
            $this->objcon->iniciar_transacao();
            
           
            $sql = "Delete From ordem_servico Where CodRegistro = ".$CodRegistro;
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }
        
            $this->objcon->commit();
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_servidores_os(){
        try{
            $this->erro = "";
            
            $sql = "Select Distinct CPF, Nome From view_ordem_servico Order By Nome";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->numrows = mysqli_num_rows($query);
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_tipos_os(){
        try{
            $this->erro = "";
            
            $sql = "Select Distinct CodTipo, DescricaoTipo From view_ordem_servico Order By DescricaoTipo";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->numrows = mysqli_num_rows($query);
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_os($CodRegistro){
        try{
            $this->erro = "";
            
            require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");
            $obj_usuario = new usuario();
            
            $sql = "Select * From view_ordem_servico Where CodRegistro = $CodRegistro";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) > 0){
                while ($row = mysqli_fetch_array($query)){
                    
                    if($row['NumOrdem'] < 10 ){
                        $row['NumOrdem'] = "000".$row['NumOrdem'];
                    }elseif($row['NumOrdem'] < 100){
                        $row['NumOrdem'] = "00".$row['NumOrdem'];
                    }elseif($row['NumOrdem'] < 1000){
                        $row['NumOrdem'] = "0".$row['NumOrdem'];
                    }
                    
                    $ordens[] = array(
                        "CodOrdem" => $row['CodOrdem'],
                        "NumeroOS" => $row['NumOrdem']."-".$row['AnoOrdem'],
                        "Resultado" => $row['Resultado'],
                        "Status" => $row['StatusOS'],
                        "Ocorrencias" => "1"
                    );

                }
                
                
                for ($index = 0; $index < count($ordens); $index++){
                    
                    $sql = "Select * From ordem_servico_historico Where CodOrdem = ".$ordens[$index]["CodOrdem"];
                    
                    $query = $this->objcon->select($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        return false;
                    }
                    $ordens[$index]["Ocorrencias"] = mysqli_num_rows($query);
                    $n = 0;
                    if(mysqli_num_rows($query) > 0){
                        while ($row = mysqli_fetch_array($query)){
                            
                            $data = new DateTime($row['DataHistorico']);
                            
                            $ordens[$index]["Registros"][$n] = array(
                                "CPF" => $row['CPF'],
                                "Nome" => $obj_usuario->consulta_nome($row['CPF']),
                                "Data" => $data->format("d-m-Y H:i:s"),
                                "Historico" => $row['Historico']
                            );
                            
                            $n++;
                        }
                    }
                }
                
            }else{
                $ordens = false;
            }
            
            return $ordens;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
}
