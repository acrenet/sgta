<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");

class gestao{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    function salva_relatorio($CodRelatorio, $CodTipo, $Titulo, $Texto, $Autor, $publicar, $Autores){
        try{
            $this->erro = "";
            
            $DataModificacao = new DateTime();
            $DataModificacao = $DataModificacao->format("Y-m-d h:i:s");
            
            $Titulo = mysqli_real_escape_string($this->objcon->conexao, $Titulo);
            $Texto = base64_encode($Texto);
            
            if($publicar == true){
                if($CodRelatorio == -1){
                    $sql = "Insert Into relatorios (CodTipo, Titulo, Texto, Autor, DataModificacao, DataLiberacao, Status) "
                         . "Values ('$CodTipo', '$Titulo', '$Texto', '$Autor', '$DataModificacao', '$DataModificacao', 'publicado')";
                    $id = $this->objcon->insert($sql);
                }else{
                    $sql = "Update relatorios Set CodTipo = '$CodTipo', Titulo = '$Titulo', Autor = '$Autor', "
                         . "Texto = '$Texto', DataModificacao = '$DataModificacao', DataLiberacao = '$DataModificacao', Status = 'publicado' Where CodRelatorio = $CodRelatorio";
                    $this->objcon->execute($sql);
                }
            }else{
                if($CodRelatorio == -1){
                    $sql = "Insert Into relatorios (CodTipo, Titulo, Texto, Autor, DataModificacao) "
                         . "Values ('$CodTipo', '$Titulo', '$Texto', '$Autor', '$DataModificacao')";
                    $id = $this->objcon->insert($sql);
                }else{
                    $sql = "Update relatorios Set CodTipo = '$CodTipo', Titulo = '$Titulo', Autor = '$Autor', "
                         . "Texto = '$Texto', DataModificacao = '$DataModificacao' Where CodRelatorio = $CodRelatorio";
                    $this->objcon->execute($sql);
                }
            }
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro." ---> ".$sql;
                return false;
            }
            
            if($CodRelatorio == -1){
                $IdRelatorio = $id;
            }else{
                $IdRelatorio = $CodRelatorio;
            }
            
            $sql = "Delete From relatorios_membros Where Codrelatorio = $IdRelatorio";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro." ---> ".$sql;
                return false;
            }
            
            if($Autores != ""){
                
                $obj_usuarios = new usuario();
                
                for ($index = 0; $index < count($Autores); $index++){
                    $CPF = $Autores[$index];
                    $Nome = $obj_usuarios->consulta_nome($CPF);
                    $sql = "Insert Into relatorios_membros (Codrelatorio, CPF, Nome) Values ($IdRelatorio, $CPF, '$Nome')";
                    $this->objcon->insert($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro." ---> ".$sql;
                        return false;
                    }
                }
                
            }
            
            if($CodRelatorio == -1){
                return $id;
            }else{
                return $CodRelatorio;
            }
            
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function carrega_relatorio($CodRelatorio){
        try{
            $this->erro = "";
            
            if($CodRelatorio == -1){
                $sql = "Select * from relatorios Order By CodRelatorio Desc";
            }else{
                $sql = "Select * from relatorios Where CodRelatorio = $CodRelatorio";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return $query;
            
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_relatorios($CodPerfil){
        try{
            $this->erro = "";
            
            if($CodPerfil == 1){
                $sql = "Select * from view_relatorios Order By CodRelatorio Desc";
            }else{
                $sql = "Select * from view_relatorios Where Status = 'publicado' Order By CodRelatorio Desc";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return $query;
            
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function cancelar_publicacao($CodRelatorio){
        try{
            $this->erro = "";
            
            $sql = "Update relatorios Set Status = 'rascunho', DataLiberacao = null Where CodRelatorio = $CodRelatorio";
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function subir_arquivo($CodRelatorio, $Usuario, $Descricao){
        try{
            $this->erro = "";
            
            $Descricao = mysqli_real_escape_string($this->objcon->conexao, $Descricao);
            
            $sql = "Insert Into relatorios_files (CodRelatorio, Usuario, Descricao) Values ($CodRelatorio, $Usuario, '$Descricao')";
            
            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consultar_anexos($CodRelatorio){
        try{
            $this->erro = "";
            
            $sql = "Select * From relatorios_files Where CodRelatorio = $CodRelatorio And Status = 'ativo' Order By CodAnexo Desc";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function excluir_anexo($CodAnexo){
        try{
            $this->erro = "";
            
            $sql = "Update relatorios_files Set Status = 'excluído' Where CodAnexo = $CodAnexo";
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consultar_mensagens($CodRelatorio){
        try{
            $this->erro = "";
            
            $sql = "Select * From relatorios_chat Where CodRelatorio = $CodRelatorio And Status = 'publicado' Order By CodChat Desc";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function salvar_mensagem($CodChat, $CodRelatorio, $Usuario, $Mensagem){
        try{
            $this->erro = "";
            
            $Mensagem = mysqli_real_escape_string($this->objcon->conexao, $Mensagem);
            
            $data = date('Y-m-d H:i:s');
            
            if($CodChat == -1){
                $sql = "Insert Into relatorios_chat (CodRelatorio, Usuario, Mensagem) Values ($CodRelatorio, $Usuario, '$Mensagem')";
                $this->objcon->insert($sql);
            }else{
                $sql = "Update relatorios_chat Set Usuario = $Usuario, Mensagem = '$Mensagem', DataMensagem = '$data' Where CodChat = $CodChat";
                $this->objcon->execute($sql);
            }

            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro." --> ". $sql;
                return false;
            }
                
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function excluir_mensagem($CodChat){
        try{
            $this->erro = "";
            
            $sql = "Update relatorios_chat Set Status = 'excluído' Where CodChat = $CodChat";
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consultar_mensagem($CodChat){
        try{
            $this->erro = "";
            
            $sql = "Select * From relatorios_chat Where CodChat = $CodChat";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consultar_autores($CodRelatorio){
        try{
            $this->erro = "";
            
            $sql = "Select * From relatorios_membros Where Codrelatorio = $CodRelatorio";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
}
