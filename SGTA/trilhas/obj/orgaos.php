<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");

class orgaos{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function consulta_orgaos($CodOrgao){
        try{
            $this->erro = "";
            /*
            $sql = "SELECT registros.CodRegistro, registros.ObsCGE, Max(andamento_registro.DataAndamento) AS DataAndamento, Max(andamento_registro.Usuario) AS Usuario
                    FROM andamento_registro INNER JOIN registros ON andamento_registro.CodRegistro = registros.CodRegistro
                    GROUP BY registros.CodRegistro, registros.ObsCGE, andamento_registro.Descricao
                    HAVING (((andamento_registro.Descricao)='Incluído/Modificado Observação TCE'))";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $CodRegistro = $row['CodRegistro'];
                $RespObs = $row['Usuario'];
                $Data = $row['DataAndamento'];
                $ObsCGE = $row['ObsCGE'];
                $sql = "Insert Into obs_cge (CodRegistro, RespObs, Data, ObsCGe) Values ($CodRegistro, '$RespObs', '$Data', '$ObsCGE')";
                $this->objcon->insert($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro . "  ". $sql;
                    return false;
                }
            }
            */
            if($CodOrgao == -1){
                $sql = "Select * From orgaos Order By NomeOrgao";
            }else{
                $sql = "Select * From orgaos Where CodOrgao = $CodOrgao Order By NomeOrgao";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_orgao($CodOrgao, $abreviar = false){
        try{
            $this->erro = "";
            
            
            $sql = "Select NomeOrgao, SiglaOrgao From orgaos Where CodOrgao = $CodOrgao Order By NomeOrgao";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            
            if($abreviar == true){
                return $row['SiglaOrgao'];
            }else{
                return $row['NomeOrgao'];
            }
            
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_unidade_controle(){
        try{
            $this->erro = "";
            
            $sql = "Select CodOrgao From orgaos Where UnidadeDeControle = true";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                $UnidadeControle = -1;
            }else{    
                $row = mysqli_fetch_array($query);
                $UnidadeControle = $row['CodOrgao'];
            }
            
            return $UnidadeControle;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function cadastra_orgao($CodOrgao, $RHNet, $ComprasNet, $SiofiNet, $NomeOrgao, $SiglaOrgao, $UnidadeDeControle,$CodUnidadeEsfinge){
        try{
            $this->erro = "";
            
            $NomeOrgao = mysqli_real_escape_string($this->objcon->conexao, $NomeOrgao);
            $SiglaOrgao = mysqli_real_escape_string($this->objcon->conexao, $SiglaOrgao);
            $ComprasNet = mysqli_real_escape_string($this->objcon->conexao, $ComprasNet);
            $SiofiNet = mysqli_real_escape_string($this->objcon->conexao, $SiofiNet);
            $CodUnidadeEsfinge = mysqli_real_escape_string($this->objcon->conexao, $CodUnidadeEsfinge);
            
            //SiofiNet - virou o codigo da unidade do sfinge
            if($CodOrgao == -1){
                $sql = "Insert Into orgaos (CodOrgao, ComprasNet, SiofiNet, NomeOrgao, SiglaOrgao, UnidadeDeControle,CodUnidadeEsfinge) "
                      . "Values ($RHNet, '$ComprasNet', '$SiofiNet', '$NomeOrgao', '$SiglaOrgao', $UnidadeDeControle,$CodUnidadeEsfinge)";
              $this->objcon->insert($sql);
            } else {
                $sql = "Update orgaos Set CodOrgao = $RHNet, ComprasNet = '$ComprasNet', SiofiNet = '$SiofiNet', NomeOrgao = '$NomeOrgao', "
                     . "SiglaOrgao = '$SiglaOrgao', UnidadeDeControle = $UnidadeDeControle, CodUnidadeEsfinge = $CodUnidadeEsfinge Where CodOrgao = $CodOrgao";
                $this->objcon->execute($sql);
            }
            
            if($this->objcon->erro != ""){
                throw new Exception($this->objcon->erro);
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function excluir_orgao($CodOrgao){
        try{
            $this->erro = "";
            
            $sql = "Select count(CodRegistro) as Qtd From registros Where CodOrgao = $CodOrgao";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                throw new Exception($this->objcon->erro);
            }
            
            $row = mysqli_fetch_array($query);
            if($row['Qtd'] > 0){
                throw new Exception("Não é possível excluir este órgão/unidade pois existem registros vinculados a ele.");
            }
            
            $sql = "Delete From orgaos Where CodOrgao = $CodOrgao";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                throw new Exception($this->objcon->erro);
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
}
