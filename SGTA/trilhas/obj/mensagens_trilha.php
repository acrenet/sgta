<?php
/**
 * Description of mensagens
 *
 * @author gustavo-ga
 */

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");

class mensagens_trilha{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function buscar_mensagens($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "Select * From trilhas_mensagens Where CodTrilha = $CodTrilha And Status <> 'excluído' Order By DataMensagem Desc";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consultar_mensagem($CodMensagem){
        try{
            $this->erro = "";
            
            $sql = "Select * From trilhas_mensagens Where CodMensagem = $CodMensagem";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function incluir_mensagem($CodTrilha, $Mensagem){
        try{
            $this->erro = "";
            
            $Usuario = $_SESSION['sessao_id'];
            $Mensagem = mysqli_real_escape_string($this->con, $Mensagem);
            
            $sql = "Insert Into trilhas_mensagens (CodTrilha, Usuario, Mensagem) Values ($CodTrilha, $Usuario, '$Mensagem')";

            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function editar_mensagem($CodMensagem, $Mensagem){
        try{
            $this->erro = "";
            
            $Mensagem = mysqli_real_escape_string($this->con, $Mensagem);
            
            $sql = "Update trilhas_mensagens Set Mensagem = '$Mensagem' Where CodMensagem = $CodMensagem";

            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function apagar_mensagem($CodMensagem){
        try{
            $this->erro = "";
            
            $sql = "Update trilhas_mensagens Set Status = 'excluído' Where CodMensagem = $CodMensagem";

            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    function gera_tabela_mensagens_trilha($CodTrilha){
        try{

            $Usuario = $_SESSION['sessao_id'];
            
            require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/code/funcoes.php");
            require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");
            $obj_usuario = new usuario();

            $query = $this->buscar_mensagens($CodTrilha);
            if($this->erro != ""){
                throw new Exception($this->erro);
            }

            $body = "";

            if(mysqli_num_rows($query) > 0){
                while ($row = mysqli_fetch_array($query)){

                    if($row['Usuario'] == $Usuario){
                        $botoes = '<a href="#" title="Alterar Mensagem" onclick="return alterar_mensagem_trilha('.$row['CodMensagem'].')" class="editar"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a> &nbsp;
                                            <a href="#" title="Excluir Mensagem" onclick="return excluir_mensagem_trilha('.$row['CodMensagem'].')" class="excluir"><i class="fa fa-times fa-lg" aria-hidden="true"></i></a>';
                    }else{
                        $botoes = '<i class="fa fa-pencil fa-lg" aria-hidden="true" style="color: silver;"></i> &nbsp;
                                   <i class="fa fa-times fa-lg" aria-hidden="true" style="color: silver;"></i>';
                    }

                    $body = $body.'<tr>
                                        <td style="font-weight: bold; font-style: italic; color: blue;">'.$obj_usuario->consulta_nome($row['Usuario']).'<br>'.data_br($row['DataMensagem']).'</td>
                                        <td>'.str_replace("\r", '<br>',$row['Mensagem']).'</td>
                                        <td>'.$botoes.'</td>
                                    </tr>';
                }
            }

            return $body;          
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            retorno();
        }
    }
    
}
