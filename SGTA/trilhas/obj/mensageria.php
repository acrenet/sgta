<?php

class mensageria {
  
    private $url;
    private $headers ;
    private $data;

    public function __construct($token){

        $this->setHeaders($token);
 
    }

    public function setData($varData){$this->data = $varData;}
    public function getData(){return $this->data;}
    public function getUrl(){return $this->url;}
    public function setUrl($varUrl){ $this->url = $varUrl;}
    public function getHeaders(){ return $this->headers;}

    public function setHeaders($varToken){
        $this->headers = array(
            'Access-Control-Allow-Origin: *', 
            'Access-Control-Allow-Methods: "GET, POST, PUT, DELETE, OPTIONS"', 
            'Access-Control-Allow-Headers: X-Requested-With, Origin, Content-Type, Accept, AUTH_TOKEN, PROFILE', 
            'content-type: application/json',
            'AUTH_TOKEN:'.$varToken
        );
    }

    public function  httpPost(){

        $curl = curl_init($this->getUrl());
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->getData()));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response);
    }

    public function httpGet(){

        $curl = curl_init($this->getUrl());
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return simplexml_load_string($response);

    }

    public function publicaEventoSgta() {
        $this->setUrl(  $_SESSION['comunicacao'] . 'eventoSgta/publicaEventoSgta');
      
        return $this->httpPost();
    }

    public function getidentificadorSfiPerfil(){
      
        $this->setUrl( $_SESSION['sgi'] . 'perfis/findPerfisPorNome');
        return $this->httpPost();
    }
}