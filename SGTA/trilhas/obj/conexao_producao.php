<?php
set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

class conexao {
    //andre , verificar a separação da autenticação do email e do banco
    public $erro = "";
    public $erro_nr = 0;
    public $sql = "";
    public $num_rows = 0;
    
    public $servidor="";
    public $database="";
    public $user="";
    public $password="";
    public $porta="3306";
    
    public $conexao = "";
    
    function __construct() {
       
        //conexão para o banco do servidor de produção
       $this->servidor = "localhost";
       $this->database = "cge_trilha_auditoria";
       $this->user = "root";
       $this->password = "";
      // $this->user = "usr-sgta";
       //$this->password = "FetEjEsyik*26";
   
        
        $this->conexao = mysqli_connect($this->servidor, $this->user, $this->password, $this->database, $this->porta);
        if (mysqli_connect_errno()){
            $this->erro = "Falha ao conectar no banco de dados: " . mysqli_connect_error();
            $this->erro_nr = mysqli_connect_errno();
            return false;
        }else{
            mysqli_set_charset($this->conexao, "utf8");
        }
    }
    
    public function iniciar_transacao(){
        try{
            mysqli_autocommit($this->conexao, FALSE);
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function commit(){
        try{
            mysqli_commit($this->conexao);
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function rollback(){
        try{
            mysqli_rollback($this->conexao);
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function fechar_conexao(){
        try{
            mysqli_close($this->conexao);
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function select($sql){
         try{
            $this->erro = "";
            
            $query = mysqli_query($this->conexao, $sql);
            if(mysqli_errno($this->conexao) != 0){
                $this->erro = mysqli_error($this->conexao);
                $this->erro_nr = mysqli_errno($this->conexao);
                $this->sql = $sql;
                return false;
            }
            
            $this->num_rows = mysqli_num_rows($query);
            
            return $query;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            $this->sql = $sql;
            return false;
        }
    }
    
    public function insert($sql){
         try{
            $this->erro = "";
            
            
            mysqli_query($this->conexao, $sql);
            if(mysqli_errno($this->conexao) != 0){
                $this->erro_nr = mysqli_errno($this->conexao);
                $this->sql = $sql;
                throw new Exception(mysqli_error($this->conexao));
            }
            
            return mysqli_insert_id($this->conexao);
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage() . "  Linha: " . $ex->getLine();
            $this->sql = $sql;
            return false;
        }
    }
    
    public function execute($sql){
        try{
            $this->erro = "";
            
            mysqli_query($this->conexao, $sql);
            if(mysqli_errno($this->conexao) != 0){
                $this->erro = mysqli_error($this->conexao);
                $this->erro_nr = mysqli_errno($this->conexao);
                $this->sql = $sql;
                return false;
            }
            
            return mysqli_affected_rows($this->conexao);
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            $this->sql = $sql;
            return false;
        }
    }
    
    public function log($comando, $descricao){

        if(!isset($_SESSION)){
            session_start();
        }
        
        $id_usuario = $_SESSION['sessao_id'];
        $nome_responsavel = $_SESSION['sessao_nome'];
        $data = date("Y-m-d H:i:s");
        
        $descricao = str_replace("'","\'",$descricao);
        
        $sql = "Insert Into log (comando, nome_responsavel, data, descricao, id_usuario) Values "
                . "('$comando', '$nome_responsavel', '$data', '$descricao', '$id_usuario')";
        
        $this->execute($sql);
        
        if($this->erro != ""){
            return $this->erro;
        }else{
            return true;
        }
    }
    
}
