<?php


set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");



class uploads{
     private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    function salva_arquivo($CodOrgao, $Periodo, $Ano, $Modelo, $Usuario, $Arquivo, $Tamanho, $Observacao){
        try{
            $this->erro = "";
            
            $query = $this->consulta_modelos($Modelo);
            if($this->erro != ""){
                return false;
            }
            $row = mysqli_fetch_array($query);
            
            
            switch ($row['Periodicidade']){
                case "anual":
                    $Referencia = $Ano;
                    break;
                case "semestral":
                    $Referencia = $Periodo . "º Semestre de $Ano";
                    break;
                case "trimestral":
                    $Referencia = $Periodo . "º Trimestre de $Ano";
                    break;
                case "bimestral":
                     $Referencia = $Periodo . "º Bimestre de $Ano";
                    break;
                case "mensal":
                    switch ($Periodo){
                        case 1:
                            $mes = "Janeiro";
                            break;
                        case 2:
                            $mes = "Fevereiro";
                            break;
                        case 3:
                            $mes = "Março";
                            break;
                        case 4:
                            $mes = "Abril";
                            break;
                        case 5:
                            $mes = "Maio";
                            break;
                        case 6:
                            $mes = "Junho";
                            break;
                        case 7:
                            $mes = "Julho";
                            break;
                        case 8:
                            $mes = "Agosto";
                            break;
                        case 9:
                            $mes = "Setembro";
                            break;
                        case 10:
                            $mes = "Outubro";
                            break;
                        case 11:
                            $mes = "Novembro";
                            break;
                        case 12:
                            $mes = "Dezembro";
                            break;
                    }
                    $Referencia = $mes . " de $Ano";
                    break; 
            }
            
            $Observacao = mysqli_real_escape_string($this->con, $Observacao);
            
            $sql = "Insert Into uploads (CodOrgao, CodPeriodo, Ano, CodModelo, Usuario, Arquivo, Tamanho, Observacao, Referencia) Values "
                 . "($CodOrgao, $Periodo, $Ano, $Modelo, '$Usuario', '$Arquivo', $Tamanho, '$Observacao', '$Referencia')";
            
            $Id = $this->objcon->insert($sql);
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $Id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function consulta_uploads($CodOrgao = -1){
        try{
            $this->erro = "";
            
            if($CodOrgao == -1){
                $sql = "Select * From view_uploads Order By Data Desc"; 
            }else{
                $sql = "Select * From view_uploads Where CodOrgao = $CodOrgao Order By Data Desc"; 
            }
            
            $query = $this->objcon->select($sql);
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function consulta_modelos($CodModelo = -1){
        try{
            $this->erro = "";
            
            if($CodModelo == -1){
                $sql = "Select * From uploads_modelos Order By Modelo";
            }else{
                $sql = "Select * From uploads_modelos Where CodModelo = $CodModelo";
            }

            $query = $this->objcon->select($sql);
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function alterar_Status($CodUpload, $acao){
        try{
            $this->erro = "";
            
            if($acao == "confirmar"){
                $sql = "Update uploads Set status = 'concluído' Where CodUpload = $CodUpload";
            }else{
                $sql = "Update uploads Set status = 'rejeitado' Where CodUpload = $CodUpload";
            }
                
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function cadastra_modelo($CodModelo, $Modelo, $Template, $Orientacao, $Periodicidade){
        try{
            $this->erro = "";
            
            $Modelo = mysqli_real_escape_string($this->con, $Modelo);
            $Template = mysqli_real_escape_string($this->con, $Template);
            $Orientacao = mysqli_real_escape_string($this->con, $Orientacao);
            
                $sql = "Select Count(CodModelo) as Qtd From uploads_modelos Where Modelo = '$Modelo' And CodModelo <> $CodModelo";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                $row = mysqli_fetch_array($query);
                if($row['Qtd'] > 0){
                    $this->erro = "Já existe um modelo cadastrado com este nome. Use outro nome ou altere o modelo existente.";
                    return false;
                }
            
            $id = 0;
            
            if($CodModelo == -1){
                $sql = "Insert Into uploads_modelos (Modelo, Template, Orientacao, Periodicidade) Values "
                        . "('$Modelo', '$Template', '$Orientacao', '$Periodicidade')";
                $id = $this->objcon->insert($sql);
            }else{
                $sql = "Update uploads_modelos Set Modelo = '$Modelo', Template = '$Template', Orientacao = '$Orientacao', "
                        . "Periodicidade = '$Periodicidade' Where CodModelo = $CodModelo";
                $this->objcon->execute($sql);
            }
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }        
    }
    
    function consulta_modelo($CodModelo){
        try{
            $this->erro = "";
            
            $sql = "Select * From uploads_modelos Where CodModelo = $CodModelo";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            $row = mysqli_fetch_array($query);
            
            return $row;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }  
    }
    
    function cadastra_campo($CodCampo, $CodModelo, $Coluna, $Formato, $Tamanho, $Descricao, $Requerido){
        try{
            $this->erro = "";
            
            $Coluna = mysqli_real_escape_string($this->con, $Coluna);
            $Formato = mysqli_real_escape_string($this->con, $Formato);
            $Descricao = mysqli_real_escape_string($this->con, $Descricao);
            
            $id = 0;
            
            if($CodCampo == -1){
                
                $sql = "Select Posicao From uploads_campos Order By Posicao Desc Limit 1";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                if(mysqli_num_rows($query) == 0){
                    $Posicao = 1;
                }else{
                    $row = mysqli_fetch_array($query);
                    $Posicao = $row['Posicao'] + 1;
                }
                
                $sql = "Insert Into uploads_campos (CodModelo, Coluna, Formato, Tamanho, Descricao, Requerido, Posicao) Values"
                        . " ($CodModelo, '$Coluna', '$Formato', $Tamanho, '$Descricao', $Requerido, $Posicao)";
                //throw new Exception($sql);
                $id = $this->objcon->insert($sql);
            }else{
                $sql = "Update uploads_campos Set Coluna = '$Coluna', Formato = '$Formato', Tamanho = $Tamanho, Descricao = '$Descricao', "
                        . "Requerido = $Requerido Where CodCampo = $CodCampo";
                $this->objcon->execute($sql);
            }
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    
    function reposiciona_campo($CodCampo, $CodModelo, $direcao){
        try{
            $this->erro = "";
            
            $sql = "Select CodCampo, Posicao From uploads_campos Where CodModelo = $CodModelo Order By Posicao";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_assoc($query)){
                $tab[] = $row;
            }
            
            for ($index = 0; $index < count($tab); $index++){
                
                if($tab[$index]['CodCampo'] == $CodCampo){
                    $Posicao_Atual = $tab[$index]['Posicao'];
                    $Index_Atual = $index;
                }
                $tab[$index]['modificado'] = false;
            }
            
            if($direcao == "acima"){
                
                $primeiro = true;
                for ($index = 0; $index < count($tab); $index++){
                    if($tab[$index]['Posicao'] < $Posicao_Atual){
                        $primeiro = false;
                        $Posicao_Inferior = $tab[$index]['Posicao'];
                        $Index_Inferior = $index;
                    }
                }
                
                if($primeiro == false){
                    
                    $tab[$Index_Atual]['Posicao'] = $Posicao_Inferior;
                    $tab[$Index_Atual]['modificado'] = true;
                    $tab[$Index_Inferior]['Posicao'] = $Posicao_Atual;
                    $tab[$Index_Inferior]['modificado'] = true;
                }
                    
            }else{ //abaixo
                
                $ultimo = true;
                for ($index = 0; $index < count($tab); $index++){
                    if($tab[$index]['Posicao'] > $Posicao_Atual){
                        $ultimo = false;
                        $Posicao_Superior = $tab[$index]['Posicao'];
                        $Index_Superior = $index;
                        goto pulo;
                    }
                }
                pulo:
                if($ultimo == false){
                    $tab[$Index_Atual]['Posicao'] = $Posicao_Superior;
                    $tab[$Index_Atual]['modificado'] = true;
                    $tab[$Index_Superior]['Posicao'] = $Posicao_Atual;
                    $tab[$Index_Superior]['modificado'] = true;
                }
                
            }
            
            for ($index = 0; $index < count($tab); $index++){
                if($tab[$index]['modificado'] == true){
                    $sql = "Update uploads_campos Set Posicao = ".$tab[$index]['Posicao']." Where CodCampo = ".$tab[$index]['CodCampo'];
                    $this->objcon->execute($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        return false;
                    }
                }
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function exclui_campo($CodCampo){
        try{
            $this->erro = "";
            
            $sql = "Delete From uploads_campos Where CodCampo = $CodCampo";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function consulta_campos($CodModelo){
        try{
            $this->erro = "";
            
            $sql = "Select * From uploads_campos Where CodModelo = $CodModelo Order By Posicao";
                
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function consulta_campo($CodCampo){
        try{
            $this->erro = "";
            
            $sql = "Select * From uploads_campos Where CodCampo = $CodCampo Order By Posicao";
                
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            
            return $row;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function contar_campos($CodModelo){
        try{
            $this->erro = "";
            
            $sql = "Select count(CodCampo) as Qtd From uploads_campos Where CodModelo = $CodModelo";
                
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            
            return $row['Qtd'];
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function excluir_modelo($CodModelo){
        try{
            $this->erro = "";
            
            $sql = "Update uploads_modelos Set Status = 'excluído' Where CodModelo = $CodModelo";
                
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    function retornar_modelo($CodModelo){
        try{
            $this->erro = "";
            
            $sql = "Update uploads_modelos Set Status = 'válido' Where CodModelo = $CodModelo";
                
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage(). ". Linha: " . $ex->getLine();
            return false;
        }
    }
    
    
}
