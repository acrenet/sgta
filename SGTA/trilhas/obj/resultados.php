<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");

class resultados{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    
    
    function cadastra_resultado($CodResultado, $CodTipo, $CodOrgao, $NumProcessoFiscalizacao, $NumProcessoComunicacao, $NumDespacho, $OficioDeComunicacao, $Objeto, $Observacoes, 
            $CadastradoPor, $DataInicioProcesso, $DataTerminoProcesso, $ValorAuditado, $TipoFiscalizacao, $InicioPeriodoAuditado, $TerminoPeriodoAuditado, $Responsaveis, $TrilhaOrigem){
         try{
            $this->erro = "";
            
            $NumProcessoFiscalizacao = mysqli_real_escape_string($this->objcon->conexao, $NumProcessoFiscalizacao);
            $NumDespacho = mysqli_real_escape_string($this->objcon->conexao, $NumDespacho);
            $Objeto = mysqli_real_escape_string($this->objcon->conexao, $Objeto);
            $Observacoes = mysqli_real_escape_string($this->objcon->conexao, $Observacoes);
            $NumProcessoComunicacao = mysqli_real_escape_string($this->objcon->conexao, $NumProcessoComunicacao);
            $OficioDeComunicacao = mysqli_real_escape_string($this->objcon->conexao, $OficioDeComunicacao);
            
            if($DataInicioProcesso != "null"){
                $DataInicioProcesso = "'".$DataInicioProcesso."'";
            }
            
            if($DataTerminoProcesso != "null"){
                $DataTerminoProcesso = "'".$DataTerminoProcesso."'";
            }
            
            if($InicioPeriodoAuditado != "null"){
                $InicioPeriodoAuditado = "'".$InicioPeriodoAuditado."'";
            }
            
            if($TerminoPeriodoAuditado != "null"){
                $TerminoPeriodoAuditado = "'".$TerminoPeriodoAuditado."'";
            }
            
            if($CodResultado == -1){
                $TipoLog = "Cadastro de Processo";
                
                $sql = "Insert Into resultados (CodTipo, CodOrgao, NumProcessoFiscalizacao, NumProcessoComunicacao, OficioDeComunicacao, NumDespacho, Objeto, Observacoes, CadastradoPor, DataInicioProcesso, DataTerminoProcesso, ValorAuditado, TipoFiscalizacao, InicioPeriodoAuditado, TerminoPeriodoAuditado, Responsaveis, CodTrilha) "
                     . "Values ($CodTipo, $CodOrgao, '$NumProcessoFiscalizacao', '$NumProcessoComunicacao', '$OficioDeComunicacao','$NumDespacho', '$Objeto', '$Observacoes', $CadastradoPor, $DataInicioProcesso, $DataTerminoProcesso, $ValorAuditado, '$TipoFiscalizacao', $InicioPeriodoAuditado, $TerminoPeriodoAuditado, '$Responsaveis', $TrilhaOrigem)";
                $id = $this->objcon->insert($sql);
                $Historico = "Cadastro de Processo nr. $NumProcessoFiscalizacao, Id: $id.";
                $CodResultado = $id;
            }else{
                $TipoLog = "Edição de Processo";
                $Historico = "Modificado Processo nr. $NumProcessoFiscalizacao, Id: $CodResultado.";
                $sql = "Update resultados Set CodTipo = $CodTipo, CodOrgao = $CodOrgao, NumProcessoFiscalizacao = '$NumProcessoFiscalizacao', NumDespacho = '$NumDespacho', "
                        . "Objeto = '$Objeto', InicioPeriodoAuditado = $InicioPeriodoAuditado, TerminoPeriodoAuditado = $TerminoPeriodoAuditado, Responsaveis = '$Responsaveis', "
                        . "CadastradoPor = $CadastradoPor, DataInicioProcesso = $DataInicioProcesso, DataTerminoProcesso = $DataTerminoProcesso, ValorAuditado = $ValorAuditado, "
                        . "NumProcessoComunicacao = '$NumProcessoComunicacao', OficioDeComunicacao = '$OficioDeComunicacao', TipoFiscalizacao = '$TipoFiscalizacao', CodTrilha = $TrilhaOrigem Where CodResultado = $CodResultado";
                $id = $this->objcon->execute($sql);
                
            }
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro." SQL: ".$sql;
                return false;
            }
            
            $this->log($CodResultado, $TipoLog, $Historico);
            
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    
    function consulta_resultados($CodResultado, $ListaAreas, $DataInicial = "", $DataFinal = ""){
         try{
            $this->erro = "";
            
            if($DataInicial != ""){
                if ($DataFinal != ""){
                    $where = "And (TO_DAYS(TerminoPeriodoAuditado) BETWEEN TO_DAYS(DATE('$DataInicial')) AND TO_DAYS(DATE('$DataFinal')))";
                }else{
                    $where = "And ((TO_DAYS(TerminoPeriodoAuditado) >= TO_DAYS(DATE('$DataInicial'))) OR (TerminoPeriodoAuditado IS NULL))";
                }   
            }else{
                if ($DataFinal != ""){
                    $where = "And (TO_DAYS(TerminoPeriodoAuditado) <= TO_DAYS(DATE('$DataFinal')))";
                }else{
                    $where = "";
                }    
            }
            
            if($CodResultado == -1){
                $sql = "Select * From view_resultados Where CodTipo In $ListaAreas $where Order By CodResultado Desc";
            }else{
                $sql = "Select * From view_resultados Where CodResultado = $CodResultado";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_anexos($CodResultado){
         try{
            $this->erro = "";

            $sql = "Select * From resultados_files Where CodResultado = $CodResultado And Status = 'válido' Order By Arquivo";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function excluir_anexo($CodAnexo, $CodResultado){
         try{
            $this->erro = "";
            
            $DataExclusao = new DateTime();
            $DataExclusao = $DataExclusao->format("Y-m-d h:i:s");

            $sql = "Update resultados_files Set Status = 'excluído', DataExclusao = '$DataExclusao' Where CodAnexo = $CodAnexo";
            
            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, "Exclusão de Arquivo", "Excluído o arquivo Id: $CodAnexo.");
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function subir_arquivo($CodResultado, $Arquivo){
        try{
            $this->erro = "";
            $Usuario = $_SESSION['sessao_id'];
            
            $sql = "Insert Into resultados_files (CodResultado, Usuario, Arquivo) Values ($CodResultado, '$Usuario', '$Arquivo')";

            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, "Upload de Arquivo", "Efetuado o upload do arquivo: $Arquivo, Id: $id.");
            
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function exibir_anexos($CodResultado){
        try{
            $this->erro = "";
                        
            $sql = "Select * From resultados_files Where CodResultado = $CodResultado And Status = 'válido'";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function cadastra_achado($CodResultado, $CodAchado, $DescricaoAchado, $PossiveisCausas){
        try{
            $this->erro = "";
            $CadastradoPor = $_SESSION['sessao_id'];
            $DescricaoAchado = mysqli_real_escape_string($this->objcon->conexao, $DescricaoAchado);
            
            if($CodAchado == -1){
                $sql = "Insert Into resultados_achados (CodResultado, DescricaoAchado, PossiveisCausas, CadastradoPor) Values "
                        . "($CodResultado, '$DescricaoAchado', '$PossiveisCausas', '$CadastradoPor')";
                
                $id = $this->objcon->insert($sql);
                $TipoLog = "Cadastro de Achado";
                $Historico = "Cadastro de Achado, Id: $id.";
            }else{
                $sql = "Update resultados_achados Set DescricaoAchado = '$DescricaoAchado', PossiveisCausas = '$PossiveisCausas', "
                       . "CadastradoPor = '$CadastradoPor' Where CodAchado = $CodAchado";
                $id = $this->objcon->execute($sql);
                $TipoLog = "Edição de Achado";
                $Historico = "Modificado o Achado Id: $CodAchado.";
            }
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, $TipoLog, $Historico);
            
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_achados($CodResultado){
        try{
            $this->erro = "";
            
            $sql = "Select * From resultados_achados Where CodResultado = $CodResultado And StatusAchado = 'válido' Order By CodAchado Desc";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_achado($CodAchado){
        try{
            $this->erro = "";
            
            $sql = "Select * From resultados_achados Where CodAchado = $CodAchado";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function excluir_achado($CodAchado, $CodResultado){
        try{
            $this->erro = "";
            
            $sql = "Update resultados_achados Set StatusAchado = 'cancelado' Where CodAchado = $CodAchado";
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, "Exclusão de Achado", "Excluído o Achado Id: $CodAchado.");
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function cadastra_recomendacao($CodRecomendacao, $CodResultado, $DevolucaoDeNumerario, $DescricaoRecomendacao, $MonitorarAte, $StatusRecomendacao, 
            $EconomiaPotencial, $EconomiaGerada, $EconomiaMensal, $Favorecido, $Observacao, $CodMonitoramento, $JustificativaGestor){
        try{
            $this->erro = "";
            
            $CadastradoPor = $_SESSION['sessao_id'];
            $DescricaoRecomendacao = mysqli_real_escape_string($this->objcon->conexao, $DescricaoRecomendacao);
            $JustificativaGestor = mysqli_real_escape_string($this->objcon->conexao, $JustificativaGestor);
            $Observacao = mysqli_real_escape_string($this->objcon->conexao, $Observacao);
            
            if($CodRecomendacao == -1){
                $sql = "Insert Into resultados_recomendacoes (CodResultado, DevolucaoDeNumerario, DescricaoRecomendacao, MonitorarAte, "
                        . "StatusRecomendacao, EconomiaPotencial, EconomiaGerada, EconomiaMensal, Favorecido, Observacao, CadastradoPor, CodMonitoramento, JustificativaGestor) Values ($CodResultado, "
                        . "$DevolucaoDeNumerario, '$DescricaoRecomendacao', $MonitorarAte, '$StatusRecomendacao', '$EconomiaPotencial', "
                        . "'$EconomiaGerada', '$EconomiaMensal', '$Favorecido', '$Observacao', '$CadastradoPor', $CodMonitoramento, '$JustificativaGestor')";
                $id = $this->objcon->insert($sql);
                $TipoLog = "Cadastro de Recomendação";
                $Historico = "Cadastro de Recomendação, Id: $id.";
            }else{
                $sql = "Update resultados_recomendacoes Set DevolucaoDeNumerario = $DevolucaoDeNumerario, DescricaoRecomendacao = '$DescricaoRecomendacao', "
                        . "MonitorarAte = $MonitorarAte, StatusRecomendacao = '$StatusRecomendacao', EconomiaPotencial = $EconomiaPotencial, "
                        . "EconomiaGerada = $EconomiaGerada, EconomiaMensal = $EconomiaMensal, Favorecido = '$Favorecido', Observacao = '$Observacao', "
                        . "CadastradoPor = '$CadastradoPor', CodMonitoramento = $CodMonitoramento, JustificativaGestor = '$JustificativaGestor' Where CodRecomendacao = $CodRecomendacao";
                $this->objcon->execute($sql);
                $TipoLog = "Edição de Recomendação";
                $Historico = "Modificado a Recomendação, Id: $CodRecomendacao.";
            }

            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $sql = "SELECT resultados_recomendacoes.CodResultado, Sum(resultados_recomendacoes.EconomiaPotencial) AS SomaPotencial, Sum(resultados_recomendacoes.EconomiaGerada) AS SomaGerada, Sum(resultados_recomendacoes.EconomiaMensal) AS SomaMensal
                    FROM resultados_recomendacoes
                    WHERE (((resultados_recomendacoes.StatusRecomendacao)<>'cancelado'))
                    GROUP BY resultados_recomendacoes.CodResultado
                    HAVING (((resultados_recomendacoes.CodResultado) = $CodResultado))";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            
            $SomaEconomiaMensal = $row['SomaMensal'];
            $SomaEconomiaPotencial = $row['SomaPotencial'];
            $SomaEconomiaGerada = $row['SomaGerada'];
            
            $sql = "Update resultados Set SomaEconomiaMensal = $SomaEconomiaMensal, SomaEconomiaPotencial = $SomaEconomiaPotencial, SomaEconomiaGerada = $SomaEconomiaGerada "
                    . "Where CodResultado = $CodResultado;";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, $TipoLog, $Historico);
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_recomendacoes($CodResultado){
        try{
            $this->erro = "";
            
            $sql = "SELECT resultados_recomendacoes.*, resultados_monitoramento.NumProcMonitoramento, resultados_monitoramento.NomeGestor
                    FROM resultados_recomendacoes LEFT JOIN resultados_monitoramento ON resultados_recomendacoes.CodMonitoramento = resultados_monitoramento.CodMonitoramento
                    WHERE (((resultados_recomendacoes.CodResultado)=$CodResultado) AND ((resultados_recomendacoes.StatusRecomendacao)<>'cancelado'))
                    ORDER BY resultados_recomendacoes.CodRecomendacao DESC;
                    ";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_recomendacao($CodRecomendacao){
        try{
            $this->erro = "";
            
            $sql = "Select * From resultados_recomendacoes Where CodRecomendacao = $CodRecomendacao";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function excluir_recomendacao($CodRecomendacao, $CodResultado){
        try{
            $this->erro = "";
            
            $sql = "Update resultados_recomendacoes Set StatusRecomendacao = 'cancelado' Where CodRecomendacao = $CodRecomendacao";
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $sql = "SELECT resultados_recomendacoes.CodResultado, Sum(resultados_recomendacoes.EconomiaPotencial) AS SomaPotencial, Sum(resultados_recomendacoes.EconomiaGerada) AS SomaGerada, Sum(resultados_recomendacoes.EconomiaMensal) AS SomaMensal
                    FROM resultados_recomendacoes
                    WHERE (((resultados_recomendacoes.StatusRecomendacao)<>'cancelado'))
                    GROUP BY resultados_recomendacoes.CodResultado
                    HAVING (((resultados_recomendacoes.CodResultado) = $CodResultado))";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) > 0){
                $row = mysqli_fetch_array($query);

                $SomaEconomiaMensal = $row['SomaMensal'];
                $SomaEconomiaPotencial = $row['SomaPotencial'];
                $SomaEconomiaGerada = $row['SomaGerada'];
            }else{
                $SomaEconomiaMensal = "0";
                $SomaEconomiaPotencial = "0";
                $SomaEconomiaGerada = "0";
            }
            
            $sql = "Update resultados Set SomaEconomiaMensal = $SomaEconomiaMensal, SomaEconomiaPotencial = $SomaEconomiaPotencial, SomaEconomiaGerada = $SomaEconomiaGerada "
                    . "Where CodResultado = $CodResultado;";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, "Exclusão de Recomendação", "Excluída a Recomendação Id: $CodRecomendacao.");
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_encaminhamentos($CodResultado){
        try{
            $this->erro = "";
            
            $sql = "Select * From resultados_encaminhamentos Where CodResultado = $CodResultado And StatusEncaminhamento <> 'cancelado' Order By CodEncaminhamento Desc";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_encaminhamento($CodEncaminhamento){
        try{
            $this->erro = "";
            
            $sql = "Select * From resultados_encaminhamentos Where CodEncaminhamento = $CodEncaminhamento";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function cadastra_encaminhamento($CodEncaminhamento, $CodResultado, $CodOrgao, $Descricao, $Destino){
        try{
            $this->erro = "";
            
            $CadastradoPor = $_SESSION['sessao_id'];
            $Descricao = mysqli_real_escape_string($this->objcon->conexao, $Descricao);
            $Destino = mysqli_real_escape_string($this->objcon->conexao, $Destino);
            
            if($CodEncaminhamento == -1){
                $sql = "Insert Into resultados_encaminhamentos (CodResultado, CodOrgao, Descricao, Destino, CadastradoPor) Values "
                        . "($CodResultado, $CodOrgao, '$Descricao', '$Destino', '$CadastradoPor')";
                $id = $this->objcon->insert($sql);
                $TipoLog = "Cadastro de Encaminhamento";
                $Historico = "Cadastrado o Encaminhamento Id: $id.";
            }else{
                $sql = "Update resultados_encaminhamentos Set CodOrgao = $CodOrgao, Descricao = '$Descricao', Destino = '$Destino', "
                        . "CadastradoPor = '$CadastradoPor' Where CodEncaminhamento = $CodEncaminhamento";
                $this->objcon->execute($sql);
                $TipoLog = "Edição de Encaminhamento";
                $Historico = "Modificado o Encaminhamento Id: $CodEncaminhamento.";
            }

            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, $TipoLog, $Historico);
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function excluir_encaminhamento($CodEncaminhamento, $CodResultado){
        try{
            $this->erro = "";
            
            $sql = "Update resultados_encaminhamentos Set StatusEncaminhamento = 'cancelado' Where CodEncaminhamento = $CodEncaminhamento";
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, "Exclusão de Encaminhamento", "Excluído o Encaminhamento Id: $CodEncaminhamento.");
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_melhorias($CodResultado){
        try{
            $this->erro = "";
            
            $sql = "Select * From resultados_melhorias Where CodResultado = $CodResultado And StatusMelhoria <> 'cancelado' Order By CodMelhoria Desc";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_melhoria($CodMelhoria){
        try{
            $this->erro = "";
            
            $sql = "Select * From resultados_melhorias Where CodMelhoria = $CodMelhoria";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function cadastra_melhoria($CodMelhoria, $CodResultado, $Melhoria, $DescricaoMelhoria){
        try{
            $this->erro = "";
            
            $CadastradoPor = $_SESSION['sessao_id'];
            $DescricaoMelhoria = mysqli_real_escape_string($this->objcon->conexao, $DescricaoMelhoria);
            
            if($CodMelhoria == -1){
                $sql = "Insert Into resultados_melhorias (CodResultado, Melhoria, DescricaoMelhoria, CadastradoPor) Values "
                        . "($CodResultado, '$Melhoria', '$DescricaoMelhoria', '$CadastradoPor')";
                $id = $this->objcon->insert($sql);
                $TipoLog = "Cadastro de Melhoria";
                $Historico = "Cadastrado a Melhoria Id: $id.";
            }else{
                $sql = "Update resultados_melhorias Set Melhoria = '$Melhoria', DescricaoMelhoria = '$DescricaoMelhoria', "
                        . "CadastradoPor = '$CadastradoPor' Where CodMelhoria = $CodMelhoria";
                $this->objcon->execute($sql);
                $TipoLog = "Edição de Melhoria";
                $Historico = "Modificado a Melhoria Id: $CodMelhoria.";
            }

            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, $TipoLog, $Historico);
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function excluir_melhoria($CodMelhoria, $CodResultado){
        try{
            $this->erro = "";
            
            $sql = "Update resultados_melhorias Set StatusMelhoria = 'cancelado' Where CodMelhoria = $CodMelhoria";
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, "Exclusão de Melhoria", "Excluído a Melhoria Id: $CodMelhoria.");
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function cadastra_monitoramento($CodMonitoramento, $CodResultado, $NumProcMonitoramento, $NomeGestor){
        try{
            $this->erro = "";
            
            $CadastradoPor = $_SESSION['sessao_id'];
            $NomeGestor = mysqli_real_escape_string($this->objcon->conexao, $NomeGestor);
            
            if($CodMonitoramento == -1){
                $sql = "Insert Into resultados_monitoramento (CodResultado, NumProcMonitoramento, NomeGestor, CadastradoPor) Values "
                        . "($CodResultado, '$NumProcMonitoramento', '$NomeGestor', '$CadastradoPor')";
                $id = $this->objcon->insert($sql);
                $TipoLog = "Cadastro de Monitoramento";
                $Historico = "Cadastrado a Monitoramento Id: $id.";
            }else{
                $sql = "Update resultados_monitoramento Set NumProcMonitoramento = '$NumProcMonitoramento', NomeGestor = '$NomeGestor', "
                        . "CadastradoPor = '$CadastradoPor' Where CodMonitoramento = $CodMonitoramento";
                $this->objcon->execute($sql);
                $TipoLog = "Edição de Monitoramento";
                $Historico = "Modificado o Monitoramento Id: $CodMonitoramento.";
            }

            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, $TipoLog, $Historico);
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_monitoramentos($CodResultado){
        try{
            $this->erro = "";
            
            $sql = 
                   "
                    SELECT resultados_monitoramento.*, resultados_recomendacoes.DescricaoRecomendacao, resultados_recomendacoes.JustificativaGestor, resultados_recomendacoes.StatusRecomendacao
                    FROM resultados_monitoramento LEFT JOIN resultados_recomendacoes ON resultados_monitoramento.CodMonitoramento = resultados_recomendacoes.CodMonitoramento
                    WHERE (resultados_recomendacoes.StatusRecomendacao <> 'cancelado' OR resultados_recomendacoes.StatusRecomendacao IS NULL) AND resultados_monitoramento.StatusMonitoramento <> 'cancelado' AND resultados_monitoramento.CodResultado = $CodResultado
                    ORDER BY resultados_monitoramento.CodMonitoramento DESC;
                   ";
            
            //throw new Exception($sql);
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function consulta_monitoramento($CodMonitoramento){
        try{
            $this->erro = "";
            
            $sql = "Select * From resultados_monitoramento Where CodMonitoramento = $CodMonitoramento";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function excluir_monitoramento($CodMonitoramento, $CodResultado){
        try{
            $this->erro = "";
            
            $sql = "Select Count(CodRecomendacao) as Qtd From resultados_recomendacoes Where CodMonitoramento = $CodMonitoramento";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            $row = mysqli_fetch_array($query);
            if($row['Qtd'] > 0){
                $this->erro = "Uma ou mais recomendações estão vinculados a este processo de monitoramento.<br>"
                        . "Antes de excluir este monitoramento será necessário remover estes vínculos;";
                return false;
            }
            
            $sql = "Update resultados_monitoramento Set StatusMonitoramento = 'cancelado' Where CodMonitoramento = $CodMonitoramento";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->log($CodResultado, "Exclusão de Monitoramento", "Excluído a Monitoramento Id: $CodMonitoramento.");
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    function resumo_processo(){
        try{
            $this->erro = "";
            
            $sql = "SELECT resultados_achados.CodResultado, Count(resultados_achados.CodAchado) AS Qtd
                    FROM resultados_achados
                    GROUP BY resultados_achados.CodResultado, resultados_achados.StatusAchado
                    HAVING (((resultados_achados.StatusAchado)='válido'));";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $resumo[$row['CodResultado']]['QtdAchados'] = $row['Qtd'];
            }
            
            $sql = "SELECT resultados_encaminhamentos.CodResultado, Count(resultados_encaminhamentos.CodEncaminhamento) AS Qtd
                    FROM resultados_encaminhamentos
                    GROUP BY resultados_encaminhamentos.CodResultado, resultados_encaminhamentos.StatusEncaminhamento
                    HAVING (((resultados_encaminhamentos.StatusEncaminhamento)='válido'));";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $resumo[$row['CodResultado']]['QtdEncaminhamentos'] = $row['Qtd'];
            }
            
            $sql = "SELECT resultados_melhorias.CodResultado, Count(resultados_melhorias.CodMelhoria) AS Qtd
                    FROM resultados_melhorias
                    GROUP BY resultados_melhorias.CodResultado, resultados_melhorias.StatusMelhoria
                    HAVING (((resultados_melhorias.StatusMelhoria)='válido'));";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $resumo[$row['CodResultado']]['QtdMelhorias'] = $row['Qtd'];
            }
            
            $sql = "SELECT resultados_files.CodResultado, Count(resultados_files.CodAnexo) AS Qtd
                    FROM resultados_files
                    GROUP BY resultados_files.CodResultado, resultados_files.Status
                    HAVING (((resultados_files.Status)='válido'));";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $resumo[$row['CodResultado']]['QtdAnexos'] = $row['Qtd'];
            }
            
            $sql = "SELECT resultados_recomendacoes.CodResultado, Count(resultados_recomendacoes.CodRecomendacao) AS Qtd
                    FROM resultados_recomendacoes
                    WHERE (((resultados_recomendacoes.StatusRecomendacao)<>'cancelado'))
                    GROUP BY resultados_recomendacoes.CodResultado;";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $resumo[$row['CodResultado']]['QtdRecomendacoes'] = $row['Qtd'];
            }
            
            $sql = "SELECT resultados_recomendacoes.CodResultado, Count(resultados_recomendacoes.CodRecomendacao) AS Qtd
                    FROM resultados_recomendacoes
                    WHERE (((resultados_recomendacoes.StatusRecomendacao)='Aguardando Atendimento'))
                    GROUP BY resultados_recomendacoes.CodResultado;";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $resumo[$row['CodResultado']]['QtdMonitoramento'] = $row['Qtd'];
            }
            
            $Hoje = new DateTime();
            $Hoje = $Hoje->format("Y-m-d");
            
            $sql = "SELECT resultados_recomendacoes.CodResultado, Count(resultados_recomendacoes.CodRecomendacao) AS Qtd "
                    . "FROM resultados_recomendacoes "
                    . "WHERE resultados_recomendacoes.StatusRecomendacao ='Aguardando Atendimento'AND TO_DAYS(resultados_recomendacoes.MonitorarAte) < TO_DAYS(DATE('$Hoje')) "
                    . "GROUP BY resultados_recomendacoes.CodResultado";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro."   $sql";
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $resumo[$row['CodResultado']]['QtdVencido'] = $row['Qtd'];
            }
            
            $sql = "SELECT resultados_recomendacoes.CodResultado, Count(resultados_recomendacoes.CodRecomendacao) AS Qtd
                    FROM resultados_recomendacoes
                    WHERE (((resultados_recomendacoes.StatusRecomendacao)='Não Atendido'))
                    GROUP BY resultados_recomendacoes.CodResultado;";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $resumo[$row['CodResultado']]['NaoAtendido'] = $row['Qtd'];
            }
            
            $sql = "SELECT resultados.CodResultado, Count(resultados.CodResultado) AS Qtd
                    FROM resultados
                    WHERE (((resultados.DataTerminoProcesso) Is Not Null))
                    GROUP BY resultados.CodResultado;";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            while ($row = mysqli_fetch_array($query)){
                $resumo[$row['CodResultado']]['Concluido'] = $row['Qtd'];
            }
            
            if(!isset($resumo)){
                $resumo = "";
            }
            
            return $resumo;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    private function log($CodResultado, $TipoLog, $Historico){
        try{
            $this->erro = "";
            
            $Usuario = $_SESSION['sessao_id'];
            
            $sql = "Insert Into resultados_log (CodResultado, TipoLog, Historico, Usuario) Values ($CodResultado, '$TipoLog', '$Historico', '$Usuario')";
            
            $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
}
