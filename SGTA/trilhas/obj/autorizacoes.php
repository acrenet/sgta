<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");

class autorizacoes{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function autorizacoes_usuario($cpf){
        try{
            $this->erro = "";
            
            $sql = "Select * From view_autorizacoes Where  ( CPF = $cpf OR cpfSGI = $cpf ) Order By DescricaoTipo";
           
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) > 0){
                while($row = mysqli_fetch_assoc($query)){
                    $permissoes[] = $row; // Inside while loop
                }
            }else{
                $permissoes[0]["CodTipo"] = -1;
            }
            
            $sql = "Select * From tipos_trilhas";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            while($row = mysqli_fetch_assoc($query)){
                $tipos[] = $row; // Inside while loop
            }
            
            for ($index = 0; $index < count($tipos); $index++){
                
                $permitido = false;
                
                for ($index1 = 0; $index1 < count($permissoes); $index1++){
                    if($permissoes[$index1]["CodTipo"] == $tipos[$index]["CodTipo"]){
                        $permitido = true;
                    }
                }
                
                if($permitido == true){
                    $tipos[$index]["permitido"] = true;
                }else{
                    $tipos[$index]["permitido"] = false;
                }
                
            }

            return $tipos;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function autoriza_usuarios($array, $cpf){
        try{
            $this->erro = "";
            
            $sql = "Delete From autorizacoes Where CPF = '$cpf'";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            for ($index = 0; $index < count($array); $index++){
                $sql = "Insert Into autorizacoes (CPF, CodTipo, Tipo) Values('$cpf', $array[$index],'indefinido')";
                $this->objcon->insert($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
            }
            
            return true;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function autoriza_usuarios_por_tipo($CodTipo, $permitidos, $negados){
        try{
            $this->erro = "";
            
            $sql = "Select * From autorizacoes Where CodTipo = $CodTipo";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) > 0){
                while ($row = mysqli_fetch_assoc($query)){
                    $reg[] = $row;
                }
            }else{
                $reg[0]["CPF"] = "99999";
            }
            
            for ($index = 0; $index < count($permitidos); $index++){
                
                $incluir = true;
                for ($index1 = 0; $index1 < count($reg); $index1++){
                    if($reg[$index1]["CPF"] == $permitidos[$index]){
                        $incluir = false;
                    }
                }
                
                if($incluir == true){
                    
                    $cpf = $permitidos[$index];
                    
                    $sql = "Insert Into autorizacoes (CPF, CodTipo, Tipo) Values('$cpf', $CodTipo,'indefinido')";
                    $this->objcon->insert($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        return false;
                    }
                }
                   
            }
            
            for ($index = 0; $index < count($negados); $index++){
                
                $excluir = false;
                for ($index1 = 0; $index1 < count($reg); $index1++){
                    if($reg[$index1]["CPF"] == $negados[$index]){
                        $excluir = true;
                    }
                }
                
                if($excluir == true){
                    
                    $cpf = $negados[$index];
                    
                    $sql = "Delete From autorizacoes Where CPF = '$cpf' And CodTipo = $CodTipo";
                    $this->objcon->execute($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        return false;
                    }
                }
                   
            }
            
            return true;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consulta($CodTipo, $CodOrgao, $CodPerfil = -1){
        try{
            $this->erro = "";
            
            if($CodPerfil == -1){
                $sql = "Select * From view_autorizacoes Where CodTipo = $CodTipo And CodOrgao = $CodOrgao Order By Nome";
            }else{
                $sql = "Select * From view_autorizacoes Where CodTipo = $CodTipo And CodOrgao = $CodOrgao And CodPerfil = $CodPerfil Order By Nome";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            return $query;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    //SGTA-58
    public function consulta_equipe_msg ($CodTipo, $CodOrgao, $CodPerfil = -1){
        try{
            $this->erro = "";
            
            if($CodPerfil == -1){
                $sql = "Select email,StatusTipo,CodPerfil,Nome,NomePerfil From view_autorizacoes Where CodTipo = $CodTipo And CodOrgao = $CodOrgao GROUP by email,StatusTipo,CodPerfil,Nome,NomePerfil Order By Nome ";
            }else{
                $sql = "Select email,StatusTipo,CodPerfil,Nome,NomePerfil From view_autorizacoes Where CodTipo = $CodTipo And CodOrgao = $CodOrgao And CodPerfil = $CodPerfil GROUP by email,StatusTipo,CodPerfil,Nome,NomePerfil Order By Nome ";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            return $query;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consulta_todas(){
        try{
            $this->erro = "";
            
            $sql = "Select cpfSGI as CPF, DescricaoTipo From view_autorizacoes Order By cpfSGI, DescricaoTipo";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            return $query;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        } 
    }


    public function trilhas_autorizadas_usuario($cpf){
        try{
            $this->erro = "";
            
            $sql = "select * from view_trilhas where CodTipo in ( Select CodTipo From view_autorizacoes Where  ( CPF = $cpf OR cpfSGI = $cpf ) ) order by CodTrilha";
           
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            while($row = mysqli_fetch_assoc($query)){
                $tipos[] = $row; // Inside while loop
            }
            
      

            return $tipos;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
}
