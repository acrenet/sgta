<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/email.php");


class usuario{
    private $objcon = "";
    private $objemail = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    public $idUsuario = "";
    public $email = "";
    public $email_geral = "";
    public $telefone = "";
    public $CodOrgao = "";
    
    function __construct() {
        $this->objcon=new conexao();
        $this->objemail=new email();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function cadastra_usuario($cpf, $nome, $telefone, $email, $lotacao, $status, $CodCargo, $CodOrgao, $CodVinculo, $CodPerfil, $CodFuncao,$email_geral){
        try{
            $this->erro = "";
            
            //$senha = mt_rand(100000,999999);
            $senha = 'Abcd1234';

            //garantir cadastramento de usuario por outros modos
            if(!$email_geral){
                $email_geral = 'nie@tce.sc.gov.br';
            }
            
            $sql = "Insert Into usuarios (CPF, Nome, Telefone, Email, Senha, Status, CodCargo, CodOrgao, CodVinculo, CodPerfil, CodFuncao, Lotacao, email_geral) Values ('$cpf', '$nome', '$telefone',"
                    . "'$email','".md5($senha)."', '$status', $CodCargo, $CodOrgao, $CodVinculo, $CodPerfil, $CodFuncao, '$lotacao' ,'$email_geral')";
            
            $insert_id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            //andre - foi dessabilitado o codigo abaixo
            /*
            if ($_SERVER['DOCUMENT_ROOT']=="/var/www/html/homtrilhas"){
                $mensagem = 'Para acessar a versão de homologação do <a href="http://www.homolog.trilhas.go.gov.br" target="_blank">Sistema de Trilhas de Auditoria</a> utilize a senha: '.$senha.'<br>Se desejar você poderá alterar a senha no menu superior Usuários/Trocar de Senha.';
            }else{
                $mensagem = 'Para acessar o <a href="http://tce.sc.gov.br" target="_blank">Sistema de Trilhas de Auditoria</a> utilize a senha: '.$senha.'<br>Se desejar você poderá alterar a senha no menu superior Usuários/Trocar de Senha.';
            }

            $this->objemail->enviarEmail("Você foi cadastrado no Sistema de Trilhas de Auditoria.", $mensagem, $email);
            $this->erro = $this->objemail->erro;
            */
            $this->objcon->log("Cadastro de Usuário", $sql);
            
            return $insert_id;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function altera_cadastro($cpf, $novo_cpf, $nome, $email, $status, $CodCargo, $CodOrgao, $CodVinculo, $CodPerfil, $CodFuncao, $telefone, $lotacao){
        try{
            $this->erro = "";
            
            $sql = "Update usuarios Set cpf = '$novo_cpf', nome = '$nome', email = '$email', status = '$status', CodCargo = $CodCargo, "
                    . "CodOrgao = $CodOrgao, CodVinculo = $CodVinculo, CodPerfil = $CodPerfil, CodFuncao = $CodFuncao, Telefone = '$telefone', Lotacao = '$lotacao' Where cpf = $cpf";
            
            $affected_rows = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objcon->log("Edição de Usuário", $sql);
            
            return $affected_rows;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_usuario($cpf,$cpfSgta="",$codOrgao=""){
        try{
            $this->erro = "";
            
            if($cpfSgta != "" and $codOrgao != ""){
                
                $sql = "Select * From view_usuarios inner join autorizacoes on view_usuarios.CPF = autorizacoes.CPF or view_usuarios.CpfSgi = autorizacoes.CPF Where ( view_usuarios.CpfSgi = '$cpf' and view_usuarios.CPF = '$cpfSgta' ) and ( view_usuarios.CodOrgao = '$codOrgao' ) ";
            } else {
                $sql = "Select * From view_usuarios inner join autorizacoes on view_usuarios.CPF = autorizacoes.CPF or view_usuarios.CpfSgi = autorizacoes.CPF Where view_usuarios.CpfSgi = '$cpf' OR  view_usuarios.CPF = '$cpf'";
            }
            
          // die($sql) ;
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }

    public function consultaCpfSgi($cpf){ 
        try{
            $this->erro = "";
            
            $sql = "SELECT usuarios.CPF,orgaos.NomeOrgao, perfis.NomePerfil ,usuarios.Nome, usuarios.cpfSGI,orgaos.CodOrgao   ".
            "FROM usuarios  inner join orgaos  on usuarios.CodOrgao = orgaos.CodOrgao ".
            "inner join perfis on usuarios.CodPerfil = perfis.CodPerfil ".
            "inner join autorizacoes on usuarios.CPF = autorizacoes.CPF " .
            "where usuarios.cpfSGI = '$cpf' ".
            "GROUP BY usuarios.CPF,orgaos.NomeOrgao, perfis.NomePerfil ,usuarios.Nome, usuarios.cpfSGI,orgaos.CodOrgao  ";
           // die($sql);
          
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_autorizacoes($cpf, $CodOrgao = -1) {
      
        try{
            $this->erro = "";
            
            if($CodOrgao == -1) {
                //SGTA-87 - Adicionar perfil para lockar permissao, ver se fica.(ATENÇÃO andre)
                $sql = "Select * From view_autorizacoes Where ( cpfSGI = '$cpf' OR CPF = '$cpf' ) and CodPerfil = ".$_SESSION['sessao_perfil']. " and CodOrgao = ". $_SESSION['sessao_orgao'];
                //die($sql);
               
            } else if($CodOrgao == -2) {
                $sql = "Select * From view_autorizacoes";
            } else {
                $sql = "Select * From view_autorizacoes Where CodOrgao = $CodOrgao";
            }
           
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            
            if(mysqli_num_rows($query) > 0){

                while( $row = mysqli_fetch_array($query)){
                    $autorizacoes[] = $row; // Inside while loop
                }

                $autorizacoes[0]['qtd'] = mysqli_num_rows($query);

            }else{

                $autorizacoes[0]['qtd'] = 0;

            }
            
            $this->array = $autorizacoes;
            
            return true;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_usuarios($CodOrgao = -1){
        try{
            $this->erro = "";
            
            if($CodOrgao == -1){
                $sql = "Select * From view_usuarios Order By nome";
            }else if($CodOrgao == -2){
                $sql = "Select * From view_usuarios Order By nome";
            }else{
                $sql = "Select * From view_usuarios Where CodOrgao = $CodOrgao Order By nome";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function atualizar_acesso($cpf){
        try{
            $this->erro = "";
            
            $data = date("Y-m-d H:i:s");
            
            $sql = "Update usuarios Set UltimoAcesso = '$data' Where cpf = '$cpf'";
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objcon->log("Login", "Entrada no sistema.");
            
            return mysqli_affected_rows($this->con);
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function resetar_senha($cpf, $email){
        try{
            $this->erro = "";
            $this->alerta = "";
            
            if($cpf == "" || $email == ""){
                $this->alerta = "Preencha os campos CPF e E-mail.";
                return false;
            }
            
            $sql = "Select * From usuarios Where CPF = '$cpf'";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if($this->objcon->num_rows == 0 || $this->objcon->num_rows > 1){
                $this->alerta = "CPF não cadastrado.";
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            if($email != $row['Email']){
                $this->alerta = "O e-mail não confere com o cadastrado no sistema.";
                return false;
            }
            
            $senha = mt_rand(100000,999999);
            //$senha = "785498";
            
            $sql = "Update usuarios Set senha = '".md5($senha)."' Where cpf = '$cpf'";
            
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $mensagem = "Para acessar o Sistema de Trilhas de Auditoria utilize a senha: $senha.<br><br>Não responda este e-mail, em caso"
                    . " de dúvidas abra um chamado no SGTA.";
            
            $retorno = $this->objemail->enviarEmail("Nova Senha no SGTA.", $mensagem, $row['Email']);
            $this->erro = $this->objemail->erro;

            return $retorno;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function trocar_senha($cpf, $senha){
        try{
            $this->erro = "";
            
            if($cpf == "" || $senha == ""){
                $this->alerta = "Preencha os campos CPF e E-mail.";
                return false;
            }
            
            $sql = "Update usuarios Set senha = '".md5($senha)."' Where cpf = '$cpf'";
            
            $affected_rows = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objcon->log("Troca de Senha", "O usuário trocou de senha.");
            
            return $affected_rows;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_nome($CPF){

        try{
            
            $this->erro = "";
            $this->email = "";
            $this->telefone = "";
            
            $CarregarLista = false;
            
            if (isset($_SESSION['ListaDeNomes'])){
                if($_SESSION["HoraLista"] < time()){
                    $CarregarLista = true;
                }
            }else{
                
                $CarregarLista = true;
            }
            
            $CPF = trim($CPF);
            
            if($CPF == ""){
                return "";
            }
            //andre carregamento de email geral 2
            if($CarregarLista == true){
                $sql = "Select CPF, Nome, Email, Telefone, CodOrgao, email_geral, cpfSGI From usuarios ";
               
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                
                $_SESSION['HoraLista'] = time() + 600;
                
                if($this->objcon->num_rows == 0){
                    $this->erro = "CPF não cadastrado.";
                    return false;
                }
                
                while ($row = mysqli_fetch_assoc($query)){
                    $lista[] = $row;
                }
                $_SESSION['ListaDeNomes'] = $lista;
            }
            
            $lista = $_SESSION['ListaDeNomes'];
            for ($index = 0; $index < count($lista); $index++){
                //sgta-28 Erro na inclusão de observação (foi alterado para cpfSGI )
                if($CPF == $lista[$index]['cpfSGI'] or $CPF == $lista[$index]['CPF'] ){
                    $this->email = $lista[$index]['Email'];
                    $this->telefone = $lista[$index]['Telefone'];
                    $this->CodOrgao = $lista[$index]['CodOrgao'];
                    $this->email_geral = $lista[$index]['email_geral'];
                    return $lista[$index]['Nome'];
               }
            }

            return "";
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function exibir_dicas($CodPerfil, $CodDica, $CPF){
        try{
            $this->erro = "";
            
            if($CodPerfil < 5){//tce
                $sql = "Select CodDica, Arquivo From dicas Where CodDica > $CodDica And DicaCGE = true And Exibir = true Order By CodDica Limit 1";
            }else{ //orgãos
                $sql = "Select CodDica, Arquivo From dicas Where CodDica > $CodDica And DicaOrgao = true And Exibir = true Order By CodDica Limit 1";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            
            $sql = "Update usuarios Set UltimaDica = ".$row['CodDica']." Where CPF = $CPF";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $_SESSION['sessao_ultima_dica'] = $row['CodDica'];
            
            return $row;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }   
    }
    
}
