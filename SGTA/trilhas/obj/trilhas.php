<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");


class trilhas{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    public $result = array();
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function consulta_tipos($CodTipo = -1, $cpf = -1){		
        try{
            $this->erro = "";
            
            if($cpf == -1){
                if($CodTipo == -1){
                    $sql = "Select * From tipos_trilhas Order By DescricaoTipo";
                }else{
                   $sql = "Select CodTipo, DescricaoTipo From view_autorizacoes Where CodTipo = $CodTipo Order By DescricaoTipo"; 
                }
            }else{
                if($CodTipo == -1){
                    $sql = "Select CodTipo, DescricaoTipo From view_autorizacoes Where ( CPF = '$cpf' OR cpfSGI = '$cpf' ) Order By DescricaoTipo";
                }else{
                    $sql = "Select CodTipo, DescricaoTipo From view_autorizacoes Where CodTipo = $CodTipo And ( CPF = '$cpf' OR cpfSGI = '$cpf' ) Order By DescricaoTipo"; 
                }
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_grupos($CodTipo = -1){		
        try{
            $this->erro = "";
            
            if($CodTipo == -1){
                $sql = "Select * From grupos_trilhas Order By NomeTrilha";
            }else{
                $sql = "Select * From grupos_trilhas Where CodTipo = $CodTipo Order By NomeTrilha";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_grupo($CodGrupo){
        try{
            $this->erro = "";
            
           
            $sql = "Select * From grupos_trilhas Where CodGrupo = $CodGrupo";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function salvar($CodTrilha, $CodGrupo, $DataInicial, $DataFinal, $Criterios, $DataTrilha, $Continua ){
        try{
            $this->erro = "";
            
            if($CodTrilha == 0){
                
                if($Criterios == ""){
                    $sql = "Select Criterios From trilhas Where CodGrupo = $CodGrupo Order By CodTrilha Desc Limit 1";
                    $query = $this->objcon->select($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        return false;
                    }

                    if(mysqli_num_rows($query) > 0){
                        $row = mysqli_fetch_array($query);
                        $Criterios = $row['Criterios'];
                    }
                }
                
                $sql = "Insert Into trilhas (CodGrupo, DataInicial, DataFinal, Criterios, DataTrilha, Registros, continua) Values "
                        . "($CodGrupo, '$DataInicial', '$DataFinal', '$Criterios', '$DataTrilha', 0,'$Continua')";
                
                $id = $this->objcon->insert($sql);
            }else{
                $sql = "Update trilhas Set CodGrupo = $CodGrupo, DataInicial = '$DataInicial', DataFinal = '$DataFinal', "
                        . "Criterios = '$Criterios', DataTrilha = '$DataTrilha', continua = '$Continua'  Where CodTrilha = $CodTrilha";
                $this->objcon->execute($sql);
                $id = $CodTrilha;
            }
            
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consulta_trilhas($Where){
        try{
            $this->erro = "";
            
            $sql = "Select * From view_trilhas $Where";
           // die($sql);
       
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }

    public function consulta_trilhas_novo($Where){
        try{
            $this->erro = "";
            
            $sql = "Select * From trilhas a INNER JOIN grupos_trilhas b on a.CodGrupo = b.CodGrupo  INNER JOIN tipos_trilhas c on b.CodTipo = c.CodTipo $Where";
 
          // die($sql ); 
            $queryAux = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
 
            while ($row = $queryAux->fetch_row()) {
                array_push($this->result,$row);
            }
            
            
            $this->query = $queryAux;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consulta_trilha($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "Select * From view_trilhas Where CodTrilha = $CodTrilha";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }

    public function conta_registros_trilha($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "Select count(*) as verifica From view_registros Where CodTrilha = $CodTrilha";
         //   die($sql);

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $row = mysqli_fetch_array($query);

            $verifica = 0 ;
            $verifica = $row["verifica"];

            return  $verifica;

        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consulta_distribuicao($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "Select * From view_distribuicao_trilhas Where CodTrilha = $CodTrilha";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function altera_status($CodTrilha, $status, $Instrucao = ""){
        try{
            $this->erro = "";
            
            $data = date('Y-m-d');
            $Instrucao = base64_encode($Instrucao);
            
            if($status == "distribuído"){
                $sql = "Update trilhas Set StatusTrilha = '$status', DataPublicacao = '$data', Instrucao = '$Instrucao' Where CodTrilha = $CodTrilha";
                $sql2 = "Update trilha_orgao Set DataDistribuicao = '$data', Status = 'distribuído' Where CodTrilha = $CodTrilha";
                
            }elseif ($status == "pré-análise"){
                
                $sql = "Select CodGrupo From trilhas Where CodTrilha  = $CodTrilha";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                $row = mysqli_fetch_array($query);
                $CodGrupo = $row["CodGrupo"];
            
                $sql = "Select Instrucao From trilhas Where CodTrilha = $CodTrilha";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                $row = mysqli_fetch_array($query);
                $Instrucao2 = $row["Instrucao"];
                
                if($Instrucao2 == ""){
                    $sql = "Select Instrucao From trilhas Where CodGrupo = $CodGrupo Order By CodTrilha Desc Limit 1";
                    $query = $this->objcon->select($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        return false;
                    }

                    if(mysqli_num_rows($query) == 0){
                        $sql = "Update trilhas Set StatusTrilha = '$status', DataLiberacao = '$data' Where CodTrilha = $CodTrilha";
                    }else{
                        $row = mysqli_fetch_array($query);
                        $Instrucao = $row['Instrucao'];
                        $sql = "Update trilhas Set StatusTrilha = '$status', DataLiberacao = '$data', Instrucao = '$Instrucao' Where CodTrilha = $CodTrilha";
                    }
                }else{
                    $sql = "Update trilhas Set StatusTrilha = '$status', DataLiberacao = '$data' Where CodTrilha = $CodTrilha";
                }
                
                $sql2 = "";
            }elseif($status == "pendente"){
                $sql = "Update trilhas Set StatusTrilha = '$status', DataDevolucao = '$data' Where CodTrilha = $CodTrilha";
                $sql2 = "";
            }else{
                $sql = "Update trilhas Set StatusTrilha = '$status' Where CodTrilha = $CodTrilha";
                $sql2 = "";
            }
            
            if($sql2 != ""){
                $this->objcon->execute($sql2);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                
                $this->verifica_status($CodTrilha, -1);
                if($this->erro != ""){
                    return false;
                }
            }

            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
   
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function salvar_instrucao($CodTrilha, $Instrucao){
        try{
            $this->erro = "";
            
            $Instrucao = base64_encode($Instrucao);

            $sql = "Update trilhas Set Instrucao = '$Instrucao' Where CodTrilha = $CodTrilha";

            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
   
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function verifica_status($CodTrilha, $CodOrgao, $CodRegistro = 0){
        
        try{
            $this->erro = "";
            
            if($CodRegistro != 0){
                $sql = "Select CodTrilha, CodOrgao From registros Where CodRegistro = $CodRegistro";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                $row = mysqli_fetch_array($query);
                $CodTrilha = $row['CodTrilha'];
                $CodOrgao = $row['CodOrgao'];
            }
            
            
            
           if($CodOrgao == -1){
               $sql = "Select CodOrgao, Status From trilha_orgao Where CodTrilha = $CodTrilha";
           }else{
               $sql = "Select CodOrgao, Status From trilha_orgao Where CodTrilha = $CodTrilha And CodOrgao = $CodOrgao"; 
           }
           
           $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            if(mysqli_num_rows($query) == 0){
                return true;
            }
            
            $n = 0;
            while ($row = mysqli_fetch_array($query)){
                $id[$n]["CodOrgao"] = $row["CodOrgao"];
                $id[$n]["Status"] = $row["Status"];
                $n++;
            }
            
            for ($index = 0; $index < count($id); $index++){

               $CodOrgao = $id[$index]["CodOrgao"];
               
                $sql = "Select * from registros Where CodTrilha = $CodTrilha And CodOrgao = $CodOrgao And StatusRegistro <> 'excluído'";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
                
                $concluido = false;
                $analisado = false;
                $pendente = false;
                
                if(mysqli_num_rows($query) == 0){
                    $concluido = true;
                }else{
                    while ($row = mysqli_fetch_array($query)){
                        if($row['StatusRegistro'] == "justificado" || $row['StatusRegistro'] == "em análise"){
                            $analisado = true;
                        }
                        if($row['StatusRegistro'] == "concluído"){
                            $concluido = true;
                        }
                        if($row['StatusRegistro'] != "justificado" && $row['StatusRegistro'] != "em análise" && $row['StatusRegistro'] != "concluído"){
                            $pendente = true;
                        }
                    }     
                }
                
                if($concluido == true){
                    $status = "concluído";
                }
                if($analisado == true){
                    $status = "justificado";
                }
                if($pendente == true){
                    $status = "distribuído";
                }
                
                if($id[$index]['Status'] != $status){
                    $data = date('Y-m-d');

                    if($status == "concluído"){
                        $sql = "Update trilha_orgao set Status = '$status', DataConclusao = '$data' Where CodTrilha = $CodTrilha And CodOrgao = $CodOrgao";
                    }elseif($status == "justificado"){
                        $sql = "Update trilha_orgao set Status = '$status', DataAnalise = '$data' Where CodTrilha = $CodTrilha And CodOrgao = $CodOrgao";
                    }elseif($status == "distribuído"){
                        $sql = "Update trilha_orgao set Status = '$status', DataDistribuicao = '$data' Where CodTrilha = $CodTrilha And CodOrgao = $CodOrgao";
                    }

                    $this->objcon->execute($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        return false;
                    }

                }
               
            }

            $sql = "Select * From trilha_orgao Where CodTrilha = $CodTrilha";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }   
            
            $concluido = false;
            $analisado = false;
            $pendente = false;
            
            
            
            while ($row = mysqli_fetch_array($query)){
                if($row['Status'] == "justificado" || $row['Status'] == "em análise"){
                    $analisado = true;
                }
                if($row['Status'] == "concluído"){
                    $concluido = true;
                }
                if($row['Status'] != "justificado" && $row['Status'] != "em análise" && $row['Status'] != "concluído"){
                    $pendente = true;
                }
            }
            
            if($concluido == true){
                $status = "concluido";
            }
            if($analisado == true){
                $status = "justificado";
            }
            if($pendente == true){
                $status = "distribuído";
            }
            
            $sql = "Select StatusTrilha From trilhas Where CodTrilha = $CodTrilha";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            $row = mysqli_fetch_array($query);
            
            if($row['StatusTrilha'] != $status){
                $sql = "Update trilhas Set StatusTrilha = '$status' Where CodTrilha = $CodTrilha";
                $this->objcon->execute($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
            }
                
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function upload_arquivo($CodTrilha, $NomeAnexo, $DescricaoAnexo, $Usuario, $Etapa, $Restrito){
        try{
            $this->erro = "";
            
            mysqli_real_escape_string($this->objcon->conexao, $DescricaoAnexo);
            
            $sql = "Insert Into anexos_trilhas (CodTrilha, NomeAnexo, DescricaoAnexo, Usuario, Etapa, Restrito) Values ($CodTrilha, '$NomeAnexo', "
                    . "'$DescricaoAnexo', $Usuario, '$Etapa', $Restrito)";

            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
   
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function excluir_anexo($CodAnexo){
        try{
            $this->erro = "";
            
            $sql = "Select * From anexos_trilhas Where CodAnexo = $CodAnexo";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                $this->erro = "Erro: Arquivo não encontrado.";
                return false;
            }
            
            $row = mysqli_fetch_array($query);
            $NomeAnexo = $row['NomeAnexo'];
            $CodTrilha = $row['CodTrilha'];
   
            $sql = "Delete From anexos_trilhas Where CodAnexo = $CodAnexo";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->objcon->log("Excluir Anexo de Trilha", "Excluído arquivo: $NomeAnexo, Id: $CodAnexo da Trilha Id: $CodTrilha.");
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function listar_anexos($CodTrilha){
        try{
            $this->erro = "";
            
           $sql = "Select * From anexos_trilhas Where CodTrilha = $CodTrilha Order By Horario Desc";
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $qtd = mysqli_num_rows($query);
            
            if($qtd == 0){
                $anexos[0]["qtd"] = 0; 
            }else{
                while ($row = mysqli_fetch_assoc($query)){
                    $anexos[] = $row;
                }
                $anexos[0]["qtd"] = $qtd;
                
                for ($index = 0; $index < count($anexos); $index++){
                    
                    $CodAnexo = $anexos[$index]["CodAnexo"];
                    
                    $anexos[$index]["url"] = "";

                    $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/trilhas/" . $CodTrilha . "/" . $CodAnexo;
                    
                    if(file_exists($targetDir)){
                        $diretorio = scandir($targetDir);
            
                        foreach($diretorio as $arquivo){
                            if (!is_dir($arquivo)){
                                
                                $anexos[$index]["url"] = "../../intra/trilhas/$CodTrilha/$CodAnexo/" . $arquivo;
                                
                            }
                        }
                    }
                }
            }
            
            return $anexos;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consulta_status_orgao($CodTrilha, $CodOrgao){
        try{
            $this->erro = "";
            
            $sql = "Select Status From trilha_orgao Where CodTrilha = $CodTrilha And CodOrgao = $CodOrgao";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) == 0){
                return "concluído";
            }
            
            $row = mysqli_fetch_array($query);
            
            return $row['Status'];
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function excluir_trilha($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "Delete From trilhas Where CodTrilha = $CodTrilha";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
}
