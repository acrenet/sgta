<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");


class grupos_trilhas{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    function consultar_grupo($CodGrupo){
        try{

            $sql = "Select * From grupos_trilhas Where CodGrupo = $CodGrupo";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                throw new Exception($this->objcon->erro);
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    function cadastrar_grupo($CodGrupo, $CodTipo, $NomeTrilha, $TabelaDetalhe, $CodigoOrgao, $TipoTrilha){
        try{
            $this->erro = "";
            
            $NomeTrilha = mysqli_real_escape_string($this->con, $NomeTrilha);
            $TabelaDetalhe = mysqli_real_escape_string($this->con, $TabelaDetalhe);
            
            if($CodGrupo == -1){
                $sql = "Insert Into grupos_trilhas (CodTipo, NomeTrilha, TabelaDetalhe, CodigoOrgao, TipoTrilha) "
                     . "Values ($CodTipo, '$NomeTrilha', '$TabelaDetalhe', '$CodigoOrgao', '$TipoTrilha')";
                $this->objcon->insert($sql);
            }else{
                $sql = "Update grupos_trilhas Set CodTipo = $CodTipo, NomeTrilha = '$NomeTrilha', TabelaDetalhe = '$TabelaDetalhe', "
                     . "CodigoOrgao = '$CodigoOrgao', TipoTrilha = '$TipoTrilha' Where CodGrupo = $CodGrupo";
                $this->objcon->execute($sql);
            }
            
            if($this->objcon->erro != ""){
                throw new Exception($this->objcon->erro);
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    function excluir_grupo($CodGrupo){
        try{

            $sql = "Select count(CodTrilha) as Qtd From trilhas Where CodGrupo = $CodGrupo";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                throw new Exception($this->objcon->erro);
            }
            
            $row = mysqli_fetch_array($query);
            if($row['Qtd'] > 0){
                throw new Exception("Já existem trilhas carregadas no sistema, a operação não pode ser efetuada.");
            }
            
            $sql = "Delete From grupos_trilhas Where CodGrupo = $CodGrupo";
            $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                throw new Exception($this->objcon->erro);
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    function cadastrar_tipo($DescricaoTipo){
        try{
            
            $DescricaoTipo = mysqli_real_escape_string($this->con, $DescricaoTipo);

            $sql = "Select count(CodTipo) as Qtd From tipos_trilhas Where DescricaoTipo Like '$DescricaoTipo'";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                throw new Exception($this->objcon->erro);
            }
            
            $row = mysqli_fetch_array($query);
            if($row['Qtd'] > 0){
                throw new Exception("Esta área já está cadastrada.");
            }
            
            $sql = "Insert Into tipos_trilhas (DescricaoTipo) Values ('$DescricaoTipo')";
            $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                throw new Exception($this->objcon->erro);
            }
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
}
