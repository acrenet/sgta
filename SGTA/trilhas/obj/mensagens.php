<?php
/**
 * Description of mensagens
 *
 * @author gustavo-ga
 */

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/email.php");

class mensagens{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function contar_mensagens($CodTrilha){
        try{
            $this->erro = "";
            
            $sql = "Select * From view_contar_mensagens Where CodTrilha = $CodTrilha";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if(mysqli_num_rows($query) > 0){
                while ($row = mysqli_fetch_assoc($query)){
                    $arr[] = $row;
                }
            }else{
                $arr = "zero";
            }

            
            return $arr;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function buscar_mensagens($CodRegistro){
        try{
            $this->erro = "";
            
            $sql = "Select * From chat_registros Where CodRegistro = $CodRegistro And Status <> 'excluído' Order By DataMensagem Desc";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consultar_mensagem($CodMensagem){
        try{
            $this->erro = "";
            
            $sql = "Select * From chat_registros Where CodMensagem = $CodMensagem";

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function incluir_mensagem($CodRegistro, $Mensagem, $Prioridade, $Emails = ""){
        try{
            $this->erro = "";
            
            $Usuario = $_SESSION['sessao_id'];
            $Mensagem2 = $Mensagem;
            $Mensagem = mysqli_real_escape_string($this->con, $Mensagem);
            
            $sql = "Insert Into chat_registros (CodRegistro, Usuario, Mensagem, Prioridade) Values ($CodRegistro, $Usuario, '$Mensagem', '$Prioridade')";

            $id = $this->objcon->insert($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if($Emails != ""){
                require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/registros.php");
                $obj_registro = new registros();
                
                require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");
                $obj_usuario = new usuario();
                
                $obj_registro->consulta_registro($CodRegistro);
                if($obj_registro->erro != ""){
                    throw new Exception($obj_registro->erro); 
                }
                $query = $obj_registro->query;
                $row = mysqli_fetch_array($query);
                
                $Autor = $obj_usuario->consulta_nome($Usuario);
                $CodTrilha = $row['CodTrilha'];
                $NomeTrilha = $row['NomeTrilha'];
                $StatusTrilha = $row['StatusTrilha'];
                $Cpf = $row['CPF_CNPJ_Proc'];
                $Nome = $row['Nome_Descricao'];
                
                $Mensagem = str_replace("\r", '<br>', $Mensagem2);
                
                $Texto = "Uma nova mensagem foi cadastrada no Sistema de Gestão de Trilhas de Auditoria e você foi selecionado para"
                        . "receber este aviso.<br><br>";
                $Texto = $Texto."<b>Autor da Mensagem:</b>  $Autor<br>";
                $Texto = $Texto."<b>Id da Trilha:</b>  $CodTrilha<br>";
                $Texto = $Texto."<b>Nome da Trilha:</b>  $NomeTrilha<br>";
                $Texto = $Texto."<b>Status da Trilha:</b>  $StatusTrilha<br>";
                $Texto = $Texto."<b>CPF/CNPJ/Processo:</b>  $Cpf<br>";
                $Texto = $Texto."<b>Nome/Razão Social/Descrição:</b>  $Nome<br>";
                $Texto = $Texto."<b>Mensagem:</b><br>$Mensagem<br>";
                $Texto = $Texto."<br><b>Este é um e-mail automático, não o responda.</b>";
                
                $obj_email = new email();
                foreach( $Emails as $key => $email ) {
                    $obj_email->enviarEmail("Nova mensagem no Sistema de Gestão de Trilhas de Auditoria", $Texto, $email);
                }
            }
            
            return $id;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function editar_mensagem($CodRegistro, $CodMensagem, $Mensagem, $Prioridade, $Emails = ""){
        try{
            $this->erro = "";
            
            $Usuario = $_SESSION['sessao_id'];
            $Mensagem2 = $Mensagem;
            $Mensagem = mysqli_real_escape_string($this->con, $Mensagem);
            
            $sql = "Update chat_registros Set Mensagem = '$Mensagem', Prioridade = '$Prioridade' Where CodMensagem = $CodMensagem";

            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            if($Emails != ""){
                require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/registros.php");
                $obj_registro = new registros();
                
                require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/usuario.php");
                $obj_usuario = new usuario();
                
                $obj_registro->consulta_registro($CodRegistro);
                if($obj_registro->erro != ""){
                    throw new Exception($obj_registro->erro); 
                }
                $query = $obj_registro->query;
                $row = mysqli_fetch_array($query);
                
                $Autor = $obj_usuario->consulta_nome($Usuario);
                $CodTrilha = $row['CodTrilha'];
                $NomeTrilha = $row['NomeTrilha'];
                $StatusTrilha = $row['StatusTrilha'];
                $Cpf = $row['CPF_CNPJ_Proc'];
                $Nome = $row['Nome_Descricao'];
                
                $Mensagem = str_replace("\r", '<br>', $Mensagem2);
                
                $Texto = "Uma mensagem foi modificada no Sistema de Gestão de Trilhas de Auditoria e você foi selecionado para"
                        . "receber este aviso.<br><br>";
                $Texto = $Texto."<b>Autor da Mensagem:</b>  $Autor<br>";
                $Texto = $Texto."<b>Id da Trilha:</b>  $CodTrilha<br>";
                $Texto = $Texto."<b>Nome da Trilha:</b>  $NomeTrilha<br>";
                $Texto = $Texto."<b>Status da Trilha:</b>  $StatusTrilha<br>";
                $Texto = $Texto."<b>CPF/CNPJ/Processo:</b>  $Cpf<br>";
                $Texto = $Texto."<b>Nome/Razão Social/Descrição:</b>  $Nome<br>";
                $Texto = $Texto."<b>Mensagem:</b><br>$Mensagem<br>";
                $Texto = $Texto."<br><b>Este é um e-mail automático, não o responda.</b>";
                
                $obj_email = new email();
                foreach( $Emails as $key => $email ) {
                    $obj_email->enviarEmail("Nova mensagem no Sistema de Gestão de Trilhas de Auditoria", $Texto, $email);
                }
            }
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function apagar_mensagem($CodMensagem){
        try{
            $this->erro = "";
            
            $sql = "Update chat_registros Set Status = 'excluído' Where CodMensagem = $CodMensagem";

            $af = $this->objcon->execute($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            return $af;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
}
