<?php
set_error_handler(function ($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');
require_once($_SERVER['DOCUMENT_ROOT'] . "/trilhas/obj/conexao.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/trilhas/obj/autorizacoes.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/trilhas/obj/mensageria.php");

class email
{

    private $objcon = "";
    private $con = "";
    private $objaut = "";

    public $erro = "";
    public $alerta = "";
    public $mensagem = "";

    function __construct()
    {
        $this->objcon = new conexao();
        $this->objaut = new autorizacoes();

        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    //andre - verificar essa função
    public function enviarEmail($assunto, $mensagem, $emaildestinatario, $emailalt = "")
    {

        if (!isset($_SESSION)) {
            session_start();
        }

        try {
            $this->erro = "";
            $headers = "";


            if (!$_SESSION['enviarEmail']) {
                return true;
                $this->alerta = "Impossível enviar e-mail em localhost.<br>";
            }

            $emailsender = "noreply@tce.sc.gov.br";

            //Para (Linux)
            $headers .= "From: " . $emailsender . "\n";
            // Este sempre deverá existir para garantir a exibição correta dos caracteres
            $headers .= "MIME-Version: 1.1\n";
            // Email alternativa
            if ($emailalt != "") {
                $headers .= "Bcc: " . $emailalt . "\n";
            }
            // Para enviar o e-mail em formato texto com codificação de caracteres Europeu Ocidental (usado no Brasil)
            $headers .= "Content-type: text/html; charset=utf-8\n";
            // E-mail que receberá a resposta quando se clicar no 'Responder' de seu leitor de e-mails
            $headers .= "Return-Path: Trilhas TCE <" . $emailsender . ">\n"; // return-path
            // para enviar a mensagem em prioridade máxima
            $headers .= "X-Priority: 1\n";

            //bora garrantir
            $emaildestinatario = 'atriches@gmail.com';

            // Se for Postfix 
            $retorno = mail($emaildestinatario, $assunto, $mensagem, $headers, "-r" . $emailsender);

            // Se "não for Postfix"	
            if (!$retorno) {
                //Para Windows
                $headers = "";
                // Email alternativa
                $headers .= "Bcc: " . $emailalt . "\r\n";
                $headers .= "Return-Path: " . $emailsender . "\r\n";
                $headers .= "MIME-Version: 1.1\r\n";
                $headers .= "Content-type: text/html; charset=utf-8\r\n";
                $headers .= "From: " . $emailsender . "	\r\n"; // remetente
                $retorno = mail($emaildestinatario, $assunto, $mensagem, $headers);
            }

            return $retorno;
        } catch (Exception $exc) {
            $this->erro = $exc->getTraceAsString();
        }
    }

    public function enviarEmails($assunto, $mensagem, $CodOrgao, $CodPerfil, $CodTipo, $TipoEnvio)
    {
        try {
            $this->erro = "";
            $this->alerta = "";
            $this->mensagem = "";

            if (!isset($_SESSION)) {
                session_start();
            }

            //andre - desenv e homologação
            if (!$_SESSION['enviarEmail']) {
                $this->alerta = "Impossível enviar e-mail em localhost.<br>";
                return true;
            }

            $query = $this->objaut->consulta($CodTipo, $CodOrgao, $CodPerfil);

            if ($this->objaut->erro != "") {
                $this->erro = $this->objaut->erro;
                return false;
            }

            require_once($_SERVER['DOCUMENT_ROOT'] . "/trilhas/obj/orgaos.php");
            $obj_orgao = new orgaos();
            $obj_orgao->consulta_orgaos($CodOrgao);

            if ($obj_orgao->erro != "") {
                $Orgao = $CodOrgao;
            } else {
                $query2 = $obj_orgao->query;
                $row = mysqli_fetch_array($query2);
                $Orgao = $row['SiglaOrgao'];
            }

            $erro = "";
            $Qtd = 10;

            if (mysqli_num_rows($query) == 0) {
                $this->alerta = "Não há nenhum Supervisor autorizado " . ' ('  . $Orgao . ')<br>';
            } else {
                $n = 0;
                $destino = "";

                while ($row = mysqli_fetch_array($query)) {
                    if ($n < $Qtd) {

                        if ($n > 0) {
                            $destino = $destino . ",";
                        }
                        //andre - tratar envio em massa para emails dos orgaos
                        //andre, troquei o $row['Email']; por $row['Email_Geral'];
                        if ($TipoEnvio == 'grupo') {
                            $destino = $destino . $row['Email_Geral'];
                        } else {
                            $destino = $destino . $row['Email'];
                        }

                        $n++;
                    }
                }
                $retorno = true;
                //andre - bora garantir
                $destino = null;
                $destino = 'atriches@gmail.com';

                $retorno = $this->enviarEmail($assunto, $mensagem, $destino);
                if ($this->erro != "") {
                    $erro = $this->erro . '<br>';
                }
                if ($retorno == false) {
                    $this->alerta = '(' . $Orgao . ')<br>';
                } else {
                    $this->mensagem = '(' . $Orgao . ')<br>';
                }
                usleep(100000);
            }

            $this->erro = $erro;

            return true;
        } catch (Exception $ex) {
            $this->erro = $ex->getMessage();
            return false;
        }
    }

    public function enviarMensagens($assunto, $mensagem, $CodOrgao, $CodPerfil, $CodTipo)
    {

        if (!isset($_SESSION)) {
            session_start();
        }

        //andre - desenv e homologação
        if (!$_SESSION['enviarEmail']) {
            $this->alerta = "Impossível enviar mensagens  em localhost.<br>";
            return true;
        }


        $this->erro = "";
        $this->alerta = "";
        $this->mensagem = "";

        $objMensageria = new mensageria($_SESSION['sgi_token']);

        $query = $this->objaut->consulta($CodTipo, $CodOrgao, $CodPerfil);

        if ($this->objaut->erro != "") {
            $this->erro = $this->objaut->erro;
            return false;
        }

        require_once($_SERVER['DOCUMENT_ROOT'] . "/trilhas/obj/orgaos.php");
        $obj_orgao = new orgaos();
        $obj_orgao->consulta_orgaos($CodOrgao);
        if ($obj_orgao->erro != "") {
            $Orgao = $CodOrgao;
            $this->erro = $this->obj_orgao->erro;
        } else {
            $query2 = $obj_orgao->query;
            $row = mysqli_fetch_array($query2);
            $Orgao = $row['SiglaOrgao'];
        }

        if (mysqli_num_rows($query) == 0) {
            $this->alerta = "Não há nenhum Supervisor autorizado " . ' (' . $Orgao . ')<br>';
        } else {
            $arrayJafoi = null;
            $arrayJafoi = [];
            $cont = 1;



            while ($row = mysqli_fetch_array($query)) {


                //tratar multiplos envios;
                if (intval(array_search('SGTA - ' . $row['NomePerfil'] . '/' . $row['DescricaoTipo'] . "|" . $row['CodUnidadeEsfinge'], $arrayJafoi)) <=  0) {

                    $arrayJafoi[$cont] = 'SGTA - ' . $row['NomePerfil'] . '/' . $row['DescricaoTipo'] . "|" . $row['CodUnidadeEsfinge'];
                    $cont++;


                    $objMensageria->setData(array('nome' => 'SGTA - ' . $row['NomePerfil'] . '/' . $row['DescricaoTipo']));

                    $identificadorSfiPerfil = null;
                    $identificadorSfiPerfil = $objMensageria->getidentificadorSfiPerfil();

                    if ($identificadorSfiPerfil[0]->identificadorSfiPerfil > 0) {


                        $objMensageria->setData(
                            array(
                                'descricaoEvento' => $assunto,
                                'textoArquivo' => $mensagem,
                                'identificadorUnidadeGestora' => $row['CodUnidadeEsfinge'],
                                'identificadorPerfil' => $identificadorSfiPerfil[0]->identificadorSfiPerfil,
                                'identificadorTipoEvento' => 118
                            )
                        );

                        $auxResult = null;
                        $auxResult = $objMensageria->publicaEventoSgta();
                    } else {
                        $this->alerta .= 'Não foi possivel enviar a Msg ao Órgão (' . $Orgao . ') Motivo - identificadorSfiPerfil não esta cadastrado. <br><br>';
                    }
                }
            }

            $retorno = true;

            if ($this->erro != "") {
                $erro .= $this->erro . ' <br>';
            }
            if ($retorno == false) {
                $this->alerta .= '('  . $Orgao . ') <br>';
            } else {
                $this->mensagem .= '(' . $Orgao . ') <br>';
            }
        }

        return true;
    }
}
