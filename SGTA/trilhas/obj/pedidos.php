<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");

class pedidos{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function cadastra_pedido($dias, $motivo, $registros){
        try{
            $this->erro = "";
            
            $motivo = mysqli_real_escape_string($this->objcon->conexao, $motivo);
            
            $cpf = $_SESSION['sessao_id'];
            
            $this->objcon->iniciar_transacao();
            
            $sql = "Insert Into pedido_prorrogacao (SolicitadoPor, Dias, Justificativa) Values ($cpf, $dias, '$motivo')";
            
            $CodPedido = $this->objcon->insert($sql);
            
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                $this->objcon->rollback();
                return false;
            }
            
            
            for ($index = 0; $index < count($registros); $index++){
              

               
                $sql = "Select Prorrogado From registros Where CodRegistro = " . $registros[$index];
                //die( $sql);
               
                $query = $this->objcon->select($sql);
               
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }
               
                $row = mysqli_fetch_array($query);
               
                $Prorrogado = $row['Prorrogado'];
                //SGTA-33 mudança do comportamento da prorrogação. 
               // $dias_restantes = 30 - $Prorrogado;
                
               // if($dias > $dias_restantes){
                //    $dias_prorrogados = 30 - $Prorrogado;
                 //   $sql = "Update registros Set Prorrogado = 30 Where CodRegistro = $registros[$index]";
             //   }else{
                    $dias_prorrogados = $dias + $Prorrogado;
                    $sql = "Update registros Set Prorrogado = ".( $dias_prorrogados )." Where CodRegistro = $registros[$index]";
                  //  die($sql);
                    
               // }
               
                $this->objcon->insert($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    $this->objcon->rollback();
                    return false;
                }
                
                $sql = "Insert Into pedido_prorrogacao_registros (CodPedido, CodRegistro, Dias) Values ($CodPedido, $registros[$index], $dias_prorrogados)";
               //die($sql);
                $this->objcon->insert($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    $this->objcon->rollback();
                    return false;
                }
            }
            
            $this->objcon->commit();
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
                
            return $CodPedido;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    
    public function consulta_pedidos(){
        try{
            $this->erro = "";
            //consulta confusa    
          /*  $sql = "select `pedido_prorrogacao`.`CodPedido` AS `CodPedido`,`pedido_prorrogacao`.`SolicitadoPor` AS `SolicitadoPor`,`usuarios`.`Nome` AS `Nome`, " 
            ." `orgaos`.`CodOrgao` AS `CodOrgao`,`orgaos`.`NomeOrgao` AS `NomeOrgao`,`orgaos`.`SiglaOrgao` AS `SiglaOrgao`,`pedido_prorrogacao`.`Dias` AS `Dias`, "
            ." `pedido_prorrogacao`.`Justificativa` AS `Justificativa`,`pedido_prorrogacao`.`DataPedido` AS `DataPedido`,`pedido_prorrogacao`.`DataAnalise` AS `DataAnalise`,"
            ." `pedido_prorrogacao`.`AnalisadoPor` AS `AnalisadoPor`,`pedido_prorrogacao`.`StatusPedido` AS `StatusPedido`,`contator`.`qtd` AS `QtdRegistros` " 
            . " from (((`orgaos` join ((`pedido_prorrogacao` join `pedido_prorrogacao_registros` on(`pedido_prorrogacao`.`CodPedido` = `pedido_prorrogacao_registros`.`CodPedido`)) " 
            ." join `usuarios` on(`pedido_prorrogacao`.`SolicitadoPor` = `usuarios`.`CPF` or `pedido_prorrogacao`.`SolicitadoPor` = `usuarios`.`cpfSGI`)) on(`orgaos`.`CodOrgao` = `usuarios`.`CodOrgao`)) " 
            ." join `registros` on(`registros`.`CodOrgao` = `usuarios`.`CodOrgao`)) join (select `pedido_prorrogacao_registros`.`CodPedido` AS `codPedido`,count(`pedido_prorrogacao_registros`.`CodPedido`) AS `qtd` " 
            ." from `pedido_prorrogacao_registros` group by `pedido_prorrogacao_registros`.`CodPedido`) `contator` on(`pedido_prorrogacao_registros`.`CodPedido` = `contator`.`codPedido`)) group by `pedido_prorrogacao_registros`.`CodPedido`";
       
        */


        $sql = " select `pedido_prorrogacao`.`CodPedido` AS `CodPedido`,`pedido_prorrogacao`.`SolicitadoPor` AS `SolicitadoPor`, "
            ."`view_autorizacoes`.`Nome` AS `Nome`,`orgaos`.`CodOrgao` AS `CodOrgao`,`orgaos`.`NomeOrgao` AS `NomeOrgao`,`orgaos`.`SiglaOrgao` AS `SiglaOrgao`, "
            ."`pedido_prorrogacao`.`Dias` AS `Dias`, `pedido_prorrogacao`.`Justificativa` AS `Justificativa`,`pedido_prorrogacao`.`DataPedido` AS `DataPedido`, "
            ."`pedido_prorrogacao`.`DataAnalise` AS `DataAnalise`, `pedido_prorrogacao`.`AnalisadoPor` AS `AnalisadoPor`, `pedido_prorrogacao`.`StatusPedido` AS `StatusPedido`, "
            ."`contator`.`qtd` AS `QtdRegistros` "

            ." from  pedido_prorrogacao INNER JOIN pedido_prorrogacao_registros on pedido_prorrogacao.CodPedido = pedido_prorrogacao_registros.CodPedido "
            ." inner join view_autorizacoes on pedido_prorrogacao.SolicitadoPor = view_autorizacoes.CPF OR pedido_prorrogacao.SolicitadoPor = view_autorizacoes.cpfSGI "
            ." inner join registros on view_autorizacoes.CodOrgao = registros.CodOrgao "
            ." inner join orgaos on view_autorizacoes.CodOrgao = orgaos.CodOrgao "
            ." join (select `pedido_prorrogacao_registros`.`CodPedido` AS `codPedido`,count(`pedido_prorrogacao_registros`.`CodPedido`) AS `qtd` "
                    ." from `pedido_prorrogacao_registros` group by `pedido_prorrogacao_registros`.`CodPedido`) `contator` on(`pedido_prorrogacao_registros`.`CodPedido` = `contator`.`codPedido`) "
            ." group by `pedido_prorrogacao_registros`.`CodPedido` ";

            //die(  $sql);
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function consulta_detalhe($CodPedido){
        try{
            $this->erro = "";
            
            $sql = "Select * From view_pedidos_detalhe Where CodPedido = $CodPedido";
           // die($sql);
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
                
            return $query;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
}