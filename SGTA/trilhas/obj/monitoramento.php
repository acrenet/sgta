<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");

class monitoramento{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
    }
    
    public function consulta_monitoramento(){
        try{
            $this->erro = "";
            
            $cpf = $_SESSION['sessao_id'];
            $CodPerfil = $_SESSION['sessao_perfil'];
            
            if($CodPerfil == 3){
                $sql = "Select * From view_monitoramento Where UsuarioCGE = '$cpf' Or ReservadoPor = '$cpf'";
            }else{
                $sql = "Select * From view_monitoramento";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $this->query = $query;
            
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }

    public function job_consulta_gestores(){
        try{
            $this->erro = "";
         
            $sql = "SELECT DISTINCT CPF,cpfSGI,Nome,Email FROM usuarios where CodPerfil = 2";   
           // die ($sql);
            $query = $this->objcon->select($sql);
       
            if($this->objcon->erro != ""){
            
                $this->erro = $this->objcon->erro;
                return false;
            }
          
            return ($query);
            
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }

    public function job_notificacao_monitoramento($diasMonitoramento){

            $this->erro = "";
            $hoje = new DateTime();
            $hoje = $hoje->format('Y-m-d');
            $vmauxGestores = $this->job_consulta_gestores();
            $auxNotificação = array();         

            while ($LocalGestor = mysqli_fetch_assoc( $vmauxGestores)){
           
                    $and = null;
                    $auxSql = null;
                    $and = $this->job_areas_perfil($LocalGestor['cpfSGI']);
                
                    if($this->erro != ""){
                        $this->erro .= $this->objcon->erro;
                    }
                
                   
                    //$auxSql  = " SELECT * FROM `view_registros` where DataConclusaoAnalise is null  and QuarentenaAte is not null " ;
                    $auxSql  = "SELECT * FROM `view_registros` where DataConclusaoAnalise is null and  (QuarentenaAte -  CURDATE()) = " . $diasMonitoramento . " and QuarentenaAte is not null";
                    //echo   $auxSql ;
                    $query = $this->objcon->select($auxSql);
            
                    if($this->objcon->erro != ""){
                        $this->erro .= $this->objcon->erro;
                    }

                    if(mysqli_num_rows($query) > 0){
            
                        while ($RegistroGestor = mysqli_fetch_assoc( $query)){
                          
                    
                            $auxNotificação[$LocalGestor['cpfSGI']][] = array(
                                                                    'GESTOR'=>$LocalGestor['Nome'],
                                                                    'EMAIL'=>$LocalGestor['Email'],
                                                                    'CodTrilha'=>$RegistroGestor['CodTrilha'],
                                                                    'DescricaoTipo'=>$RegistroGestor['DescricaoTipo'],
                                                                    'NomeOrgao'=>$RegistroGestor['NomeOrgao'],
                                                                    'CodRegistro'=>$RegistroGestor['CodRegistro'],
                                                                    'Nome_Descricao'=>$RegistroGestor['Nome_Descricao'],
                                                                    'CPF_CNPJ_Proc'=>$RegistroGestor['CPF_CNPJ_Proc']
                                                                    );
                    
                        }
                }
                
            }      
            return  $auxNotificação;
    }   
    
    public function consulta_pendentes($prazo_legal_pre, $prazo_legal_pos){
        try{
            $this->erro = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];
            $cpf = $_SESSION['sessao_id'];
            
            $and = " ";
            
            if($CodPerfil > 4){
                $CodOrgao = $_SESSION['sessao_orgao'];
                $and = " And CodOrgao = $CodOrgao ";
            }
            
            if($CodPerfil > 1){        
                $sql = "Select CodTipo From view_autorizacoes Where ( CPF = '$cpf' OR cpfSGI = '$cpf' ) " . $and ;
              
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }

                if(mysqli_num_rows($query) > 0){
                    $and = $and . " And CodTipo In (";
                    $n=0;
                    while ($row = mysqli_fetch_array($query)){
                        if($n > 0){
                            $and  = $and . ",";
                        }

                        $and  = $and . $row['CodTipo'];

                        $n++;
                    }
                    $and  = $and . ")";
                }else{
                    $and = $and . " And CodTipo = 999999";
                }
            }
            
            $data_base = new DateTime("-$prazo_legal_pre day");
            $data_base = $data_base->format('Y-m-d');
            
            $qtd = 0;
            
            $sql = "Select * From view_registros Where (StatusRegistro = 'pendente' Or StatusRegistro = 'em justificativa') $and "
                    . " And TipoTrilha = 'pré' And  ( TO_DAYS(DataPublicacao) + Prorrogado ) < TO_DAYS(DATE('$data_base')) ";
            // die( $sql );
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $qtd = mysqli_num_rows($query);
            
            if(mysqli_num_rows($query)>0){
                while ($row = mysqli_fetch_assoc($query)){
                    $reg[] = $row;
                }
            }
            
            $data_base = new DateTime("-$prazo_legal_pos day");
            $data_base = $data_base->format('Y-m-d');
            
            $sql = "Select * From view_registros Where (StatusRegistro = 'pendente' Or StatusRegistro = 'em justificativa') $and"
                    . " And TipoTrilha = 'pós' And  ( TO_DAYS(DataPublicacao) + Prorrogado ) < TO_DAYS(DATE('$data_base'))  ";

                //  die($sql);
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $qtd = $qtd + mysqli_num_rows($query);
            
            $qtd = mysqli_num_rows($query);
            
            if(mysqli_num_rows($query)>0){
                while ($row = mysqli_fetch_assoc($query)){
                    $reg[] = $row;
                }
            }
            
            if(!isset($reg)){
                $reg = 0;
            }
            
            $this->query = $reg;
            
            return $qtd;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function contar_monitoramento(){
        try{
            $this->erro = "";
           
            $cpf = $_SESSION['sessao_id'];
            $CodPerfil = $_SESSION['sessao_perfil'];

            $hoje = new DateTime();
            $hoje = $hoje->format('Y-m-d');
            
            $and = $this->areas_perfil();
            if($this->erro != ""){
                return 0;
            }
            

            if($CodPerfil == 3){
                $sql = "Select Count(CodRegistro) as Qtd From view_registros Where UsuarioCGE = '$cpf' And TO_DAYS(QuarentenaAte) < TO_DAYS(DATE('$hoje'))";
            }else{
                $sql = "Select Count(CodRegistro) as Qtd From view_registros Where TO_DAYS(QuarentenaAte) < TO_DAYS(DATE('$hoje')) $and";
            }
           // die ($sql);
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $row = mysqli_fetch_array($query);
            
            return $row['Qtd'];
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function contar_pendentes($prazo_legal_pre, $prazo_legal_pos){
        try{
            $this->erro = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];
            
            if($CodPerfil > 4){
                $CodOrgao = $_SESSION['sessao_orgao'];
                $and = " And CodOrgao = $CodOrgao ";
            }else{
                $and = " ";
            }
            
            $data_base = new DateTime("-$prazo_legal_pre day");
            $data_base = $data_base->format('Y-m-d');
            $hoje = new DateTime();

            //SGTA-75 Adm pode tudo
            if($CodPerfil != 1){
                $and = $and . $this->areas_perfil();
            }
            
            
            if($this->erro != ""){
                return 0;
            }
            
            $sql = "Select DataPublicacao, Prorrogado From view_registros Where (StatusRegistro = 'pendente' Or StatusRegistro = 'em justificativa') $and "
                    . " And TipoTrilha = 'pré' And   TO_DAYS(DataPublicacao) + Prorrogado  < TO_DAYS(DATE('$data_base'))  ";
            
               //  die($sql);
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return 0;
            }

            $qtd = 0;
            
            if(mysqli_num_rows($query) > 0){
                while ($row = mysqli_fetch_array($query)){
                    $prorrogado = $row['Prorrogado'];
                    $data_publicacao = new DateTime($row["DataPublicacao"]);
                    $data_publicacao->add(new DateInterval("P$prazo_legal_pre"."D"));
                    $data_publicacao->add(new DateInterval("P$prorrogado"."D"));
                    if($data_publicacao < $hoje){
                        $qtd = $qtd + 1;
                    }
                }
            }
            
            
            $data_base = new DateTime("-$prazo_legal_pos day");
            $data_base = $data_base->format('Y-m-d');
            
            $sql = "Select DataPublicacao, Prorrogado From view_registros Where (StatusRegistro = 'pendente' Or StatusRegistro = 'em justificativa') $and "
                    . " And TipoTrilha = 'pós' And   TO_DAYS(DataPublicacao) + Prorrogado  < TO_DAYS(DATE('$data_base') ) ";
                
               //  die($sql);
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return 0;
            }

            if(mysqli_num_rows($query) > 0){
                while ($row = mysqli_fetch_array($query)){
                    $prorrogado = $row['Prorrogado'];
                    $data_publicacao = new DateTime($row["DataPublicacao"]);
                    $data_publicacao->add(new DateInterval("P$prazo_legal_pos"."D"));
                    $data_publicacao->add(new DateInterval("P$prorrogado"."D"));
                   
                    if($data_publicacao < $hoje){

                        $qtd = $qtd + 1;
                    }
                }
            }
            
            return $qtd;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
        public function consulta_nao_justificados(){
        try{
            $this->erro = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];
            $cpf = $_SESSION['sessao_id'];
            
            $and = " ";
            
            if($CodPerfil == 3){
                return 0;
            }
            
            if($CodPerfil > 4){
                $CodOrgao = $_SESSION['sessao_orgao'];
                $and = " And CodOrgao = $CodOrgao ";
            }
                
            if($CodPerfil > 1){        
                $sql = "Select CodTipo From view_autorizacoes Where ( CPF = '$cpf' OR cpfSGI = '$cpf' ) " . $and;
                //die($sql);
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }

                if(mysqli_num_rows($query) > 0){
                    $and = $and . " And CodTipo In ( ";
                    $n=0;
                    while ($row = mysqli_fetch_array($query)){
                        if($n > 0){
                            $and  = $and . ",";
                        }

                        $and  = $and . $row['CodTipo'];

                        $n++;
                    }
                    $and  = $and . ") ";
                }else{
                    $and = $and . " And CodTipo = 999999";
                }
            }        
                    
            $sql = "Select * From view_registros Where StatusRegistro In ('pendente', 'em justificativa') $and ";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $this->query = $query;
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    public function contar_nao_justificados(){
        try{
            $this->erro = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];

            $and = " ";
            
            if($CodPerfil > 4){
                $CodOrgao = $_SESSION['sessao_orgao'];
                $and = " And CodOrgao = $CodOrgao ";
            }
                
            $and = $and . $this->areas_perfil();
            if($this->erro != ""){
                return 0;
            }  
                    
            $sql = "Select count(CodRegistro) as Qtd From view_registros Where StatusRegistro In ('pendente', 'em justificativa') " . $and;
            //die($sql);
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $row = mysqli_fetch_array($query);
            
            $qtd =  $row['Qtd'];
            
            return $qtd;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consulta_nao_analisados(){
        try{
            $this->erro = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];
            $cpf = $_SESSION['sessao_id'];
            

            /*
            $and = "";
            
            if($CodPerfil > 4){
                return 0;
            }*/
             //SGTA-95
            $and = " ";
            
            if($CodPerfil > 4){
                $CodOrgao = $_SESSION['sessao_orgao'];
                $and = " And CodOrgao = $CodOrgao ";
            }

            if($CodPerfil > 1){        
                //SGTA-95
                $sql = "Select CodTipo From view_autorizacoes Where ( CPF = '$cpf' OR cpfSGI = '$cpf' ) " .  $and ;

                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }

                if(mysqli_num_rows($query) > 0){
                    $and = $and . " And CodTipo In (";
                    $n=0;
                    while ($row = mysqli_fetch_array($query)){
                        if($n > 0){
                            $and  = $and . ",";
                        }

                        $and  = $and . $row['CodTipo'];

                        $n++;
                    }
                    $and  = $and . ") ";
                }else{
                    $and  = $and . " And CodTipo  = 999999 ";
                }
            }        
                    
            $sql = "Select * From view_registros Where StatusRegistro In ('justificado', 'em análise') And ReservadoPor Is Not Null " . $and ;
            //die($sql);
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $this->query = $query;
            return true;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function contar_nao_analisados(){
       
        try{
            $this->erro = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];
            $cpf = $_SESSION['sessao_id'];
            
          /*  $and = "";
            
            if($CodPerfil > 4){
                return 0;
            }*/

              //SGTA-95
              $and = " ";
            
              if($CodPerfil > 4){
                  $CodOrgao = $_SESSION['sessao_orgao'];
                  $and = " And CodOrgao = $CodOrgao ";
              }
            
            if($CodPerfil > 1){      
                //SGTA-95  
                $sql = "Select CodTipo From view_autorizacoes Where ( CPF = '$cpf' OR cpfSGI = '$cpf' ) " .  $and;
                //die($sql);
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }

                if(mysqli_num_rows($query) > 0){
                    $and = $and . " And CodTipo In (";
                    $n=0;
                    while ($row = mysqli_fetch_array($query)){
                        if($n > 0){
                            $and  = $and . ",";
                        }

                        $and  = $and . $row['CodTipo'];

                        $n++;
                    }
                    $and  = $and . " ) ";
                }else{
                    $and = $and . " And CodTipo = 999999 ";
                }
            }        
                    
            $sql = "Select count(CodRegistro) as Qtd From view_registros Where StatusRegistro In ('justificado', 'em análise') And ReservadoPor Is Not Null " . $and;
        
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $row = mysqli_fetch_array($query);
            
            $qtd =  $row['Qtd'];
            
            return $qtd;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
    public function consulta_nao_distribuidos(){
        try{
            $this->erro = "";
            
            $and = $this->areas_perfil();
            if($this->erro != ""){
                return false;
            }

            $sql = "Select * From view_registros Where StatusRegistro = 'justificado' And ReservadoPor is Null " . $and;
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $this->query = $query;

            return mysqli_num_rows($query);

        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }  
    }

    
    public function consulta_nao_distribuidos2(){
        try{
            $this->erro = "";
            
            $and = $this->areas_perfil();
            if($this->erro != ""){
                return false;
            }

            $sql = "Select CodRegistro,CodTipo,CodOrgao,CodTrilha From view_registros Where StatusRegistro = 'justificado' And ReservadoPor is Null " . $and;
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $this->query = $query;
            $auxResult =[];
            while ($row = mysqli_fetch_array($query)){
                $auxResult[] = $row ;
            }
            return $auxResult;

        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }  
    }
    
    public function contar_nao_distribuidos(){
        try{
            $this->erro = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];
            
            if($CodPerfil <= 2){
                
                $and = $this->areas_perfil();
                if($this->erro != ""){
                    return false;
                }

                $sql = "Select count(CodRegistro) as Qtd From view_registros Where StatusRegistro = 'justificado' And ReservadoPor is Null $and";
                $query = $this->objcon->select($sql);
                if($this->objcon->erro != ""){
                    $this->erro = $this->objcon->erro;
                    return false;
                }

                $row = mysqli_fetch_array($query);

                $qtd =  $row['Qtd'];

               return $qtd;
            }else{
                return 0;
            }
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }  
    }
    
    public function contar_pedidos_auditoria(){
        try{
            $this->erro = "";
            
            $CodPerfil = $_SESSION['sessao_perfil'];
            
            if($CodPerfil > 2){
                return 0;
            }
            
            $and = $this->areas_perfil();
            if($this->erro != ""){
                return false;
            }

            $sql = "Select count(CodRegistro) as Qtd From view_registros Where SolicitadoAuditoria = true And StatusRegistro = 'concluído' And NumProcAuditoria Is null And ReservadoPor is Null $and";
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $row = mysqli_fetch_array($query);

            return $row['Qtd'];
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }  
    }
    
    public function consulta_pedidos_auditoria(){
        try{
            $this->erro = "";
            
            $and = $this->areas_perfil();
            if($this->erro != ""){
                return false;
            }

            $sql = "Select * From view_registros Where SolicitadoAuditoria = true And StatusRegistro = 'concluído' And NumProcAuditoria Is null And ReservadoPor is Null " . $and;
         
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            $this->query = $query;

            return mysqli_num_rows($query);

        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }  
    }
    
    function areas_perfil(){
        try{
            $this->erro = "";
            
            $cpf = $_SESSION['sessao_id'];
            $CodPerfil = $_SESSION['sessao_perfil'];
            $and = "";
            
            //SGTA-95
            $and =" ";
            if($CodPerfil > 4){

                $CodOrgao = $_SESSION['sessao_orgao'];
                $and = "And CodOrgao = $CodOrgao";
            }

            $sql = "Select CodTipo From view_autorizacoes Where ( CPF = '$cpf' OR cpfSGI = '$cpf' ) ". $and;
            //die($sql);

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }

            if(mysqli_num_rows($query) > 0){
                $and = $and . " And CodTipo In (";
                $n=0;
                while ($row = mysqli_fetch_array($query)){
                    if($n > 0){
                        $and  = $and . ",";
                    }

                    $and  = $and . $row['CodTipo'];

                    $n++;
                }
                $and  = $and . ") ";
            }else{
                $and = $and . " And CodTipo = 999999 ";
            }
            
            return $and;
            //echo $and;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }  
    }

    function job_areas_perfil($cpf){
        try{
            $this->erro = "";
               
            $sql = "Select CodTipo From view_autorizacoes Where ( CPF = '$cpf' OR cpfSGI = '$cpf' ) ";
         //   die($sql);

            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
          
            $and = "";
            if(mysqli_num_rows($query) > 0){
                
                $and = $and . " And CodTipo In (";
              
                $n=0;
                while ($row = mysqli_fetch_array($query)){
                    if($n > 0){
                        $and  = $and . ",";
                    }

                    $and  = $and . $row['CodTipo'];

                    $n++;
                }
              
                $and  = $and . ") ";
              
            }
            
            //die($and);
           return $and;
            
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }  
    }
    
    public function selecionar_usuarios($CodOrgao, $CodTipo, $StatusRegistro = ""){
        try{
            $this->erro = "";
            
            if($CodTipo == -1){//todos
                $sql = "Select * From view_autorizacoes Where CodOrgao = $CodOrgao And CodPerfil In (2,3)";
            }else{
                $sql = "Select * From view_autorizacoes Where CodOrgao = $CodOrgao And CodTipo = $CodTipo And CodPerfil In (2,3)";
            }
            
            $query = $this->objcon->select($sql);
            if($this->objcon->erro != ""){
                $this->erro = $this->objcon->erro;
                return false;
            }
            
            $numrows = mysqli_num_rows($query);
            
            if($numrows > 0){
                while ($row = mysqli_fetch_array($query)){
                    $tab[] = $row;
                }
            
                for ($index = 0; $index < count($tab); $index++){
                    
                    if($StatusRegistro == ""){
                        $sql = "Select Count(CodRegistro) as qtd From registros Where ReservadoPor = '".$tab[$index]['cpfSGI']."' And StatusRegistro In ('justificado','em análise')";
                    }else{
                        $sql = "Select Count(CodRegistro) as qtd From registros Where ReservadoPor = '".$tab[$index]['cpfSGI']."' And StatusRegistro = '$StatusRegistro'";
                    }
                   
                   // echo($sql.'<br><br>');
                    
                    $query = $this->objcon->select($sql);
                    if($this->objcon->erro != ""){
                        $this->erro = $this->objcon->erro;
                        return false;
                    }
                    $row = mysqli_fetch_array($query);
                    $tab[$index]['qtd'] = $row['qtd'];
                }
            }else{
                return false;
            }
            
            
            return $tab;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        } 
    }
    
}
