<?php

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');

require_once($_SERVER['DOCUMENT_ROOT']."/trilhas/obj/conexao.php");

class rest_service{
    private $objcon = "";
    private $con = "";
    
    public $erro = "";
    public $alerta = "";
    
    public $query = "";
    public $array = "";
    public $html = "";
    
    public $headers;
    public $service_url;
    public $password;
    public $sufixo_url;
    public $https = true;
    public $info_server_cod = 0;
    public $info_server_msg = "";
    public $erro_server_cod = 0;
    public $erro_server_msg = 0;
    
    function __construct() {
        $this->objcon=new conexao();
        
        $this->con = mysqli_connect($this->objcon->servidor, $this->objcon->user, $this->objcon->password, $this->objcon->database, $this->objcon->porta);
        mysqli_set_charset($this->con, "utf8");
        
        $this->password = "Authorization: Basic MTE2MzVASU5GX0NHRUA2N1chOzQ9eTI6Qy4yPWJGO2o6NkYwRkQ0O0tjZiVeaS4qaA==";
        $this->headers[] = 'Content-Type: application/json';
        $this->service_url = "https://contex-ws-desenv.tce.go.gov.br";
    }
    
    function request_get(){
        try{
            $this->erro = "";
            
            $url = $this->service_url.$this->sufixo_url;        
            $curl = curl_init($url);
            
            if($this->password != ""){
                $this->headers[] = $this->password;
            }
            
            curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($curl, CURLOPT_POST, false); 
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            
            if($this->https == true){
                curl_setopt($curl, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
            }else{
                curl_setopt($curl, CURLOPT_PROTOCOLS, CURLPROTO_HTTP);
            }
            
            $resposta = curl_exec($curl);
            
            $this->info_server_cod = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $this->info_server_msg = $this->info_server($this->info_server_cod);
            
            if($resposta === false){
                $this->erro_server_cod = curl_errno($curl);
                $this->erro_server_msg = curl_error($curl);
            }
            
            curl_close($curl);
            
            return $resposta;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
    }
    
    private function info_server($retorno){
        try{
            $this->erro = "";
        
            switch ($retorno){
                case 200:
                    $msg = "Sucesso.";
                    break;
                case 401:
                    $msg = "Falha na autenticação.";
                    break;
                case 403:
                    $msg = "Registro não pertence ao usuário.";
                    break;
                case 404:
                    $msg = "Registro não encontrado.";
                    break;
                case 400:
                    $msg = "Erro interno.";
                    break;
                default:
                    $msg = "Erro não definido.";
                    break;
            }
        
            return $msg;
        } catch (Exception $ex){
            $this->erro = $ex->getMessage();
            return false;
        }
        
    }
}
