<?php
//Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if(!($perfil == 1 || $perfil == 2)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Trilhas - Cadastrar Usuários";
    
    require_once('../../obj/pedidos.php');

    $obj_pedido = new pedidos();
    $query = $obj_pedido->consulta_pedidos();
    if($obj_pedido->erro != ""){
        echo $obj_pedido->erro;
        goto fim;
    }
    
    
    $targetDir = $_SERVER['DOCUMENT_ROOT']."/trilhas/intra/pedidos/";
                    
                    
    
    
?>

<script src="../../js/datepicker.js" type="text/javascript"></script>
<script src="inicio.js" type="text/javascript"></script>

<br>

<div class="container-fluid">

    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Consulta Pedidos de Prorrogação de Prazo</h4></div>
            <div class="panel-body">
                <table class="table table-bordered table-condensed table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Órgão ou Entidade</th>
                            <th>Titular Solicitante</th>
                            <th style="text-align: center;">Data Pedido</th>
                            <th style="text-align: right;">Dias</th>
                            <th>Justificativa</th>
                            <th>Anexo</th>
                            <th>Status</th>
                            <th style="text-align: right;">Qtd Registros</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(mysqli_num_rows($query) == 0){
                                echo '<tr><td colspan="9">Nenhum pedido de prorrogação encontrado.</td></td>';
                            }else{
                                while ($row = mysqli_fetch_array($query)){
                                    
                                    $CodPedido = $row['CodPedido'];
                                    
                                    $url = "";
                                    
                                    if(file_exists($targetDir . $CodPedido)){
                                        
                                        $diretorio = scandir($targetDir . $CodPedido);
                                        
                                        foreach($diretorio as $arquivo){
                                            if (!is_dir($arquivo)){
                                                $url = "../../intra/pedidos/$CodPedido/" . $arquivo;
                                            }
                                        }
                                        
                                    }
                                    
                                    
                                    echo
                                    '
                                        <tr>
                                            <td>'.$row['NomeOrgao'].'</td>
                                            <td>'.$row['Nome'].'</td>
                                            <td style="text-align: center;">'.date('d/m/Y H:i:s', strtotime($row['DataPedido'])).'</td>
                                            <td style="text-align: right;">'.$row['Dias'].'</td>
                                            <td>'.str_replace("\r", '<br>', $row['Justificativa']).'</td>
                                            <td><a href="'.$url.'" target="_blank">'.basename($url).'</a></td>
                                            <td>'.$row['StatusPedido'].'</td>
                                            <td style="text-align: right;">'.$row['QtdRegistros'].'</td>    
                                            <td><a href="#" class="consultar" title="Ver Detalhes" onclick="return ver_detalhes('.$row['CodPedido'].');"><span class="fa fa-eye fa-lg"></span></a></td>
                                        </tr>
                                    ';
                                }
                            }
                        ?>
                                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>            
</div>

<div id="popup_form" title="Registros do Pedido de Prorrogação de Prazo">    
    <div style="max-height: 500px; overflow-y: scroll;">
        <table class="table table-striped table-hover table-condensed" style="font-size: 90%;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Trilha</th>
                    <th>CPF/CNPJ/Proc</th>
                    <th>Nome/Descrição</th>
                    <th style="text-align: right;">Dias Solicitados</th>
                </tr>
            </thead>
            <tbody id="tb_registros">
                
            </tbody>
        </table>

    </div>
    
    <hr>
    <div style="text-align: right;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick='$("#popup_form").dialog("close");'>Fechar &nbsp;<span class="fa fa-times-circle"></span></button>
    </div>
</div>

<form name="form1" id="form1" method="POST">
    <input type="hidden" name="operacao" value="consulta" />
    <input type="hidden" name="CodPedido" id="CodPedido" value="0" />
</form>

<?php
fim:
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");