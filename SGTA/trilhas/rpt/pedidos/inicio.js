var destino = "server.php";

$(document).ready(function() {
    popup_form_();
});

function popup_form_(){
    $("#popup_form").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 'auto',
        maxWidth: 1000,
        responsive: true,
        fluid: true,
        show: {
            effect: "scale",
            duration: 200
        },
        hide: {
            effect: "scale",
            duration: 200
        }
    });
}

function resposta(retorno){
    if(retorno != false){
        switch (retorno.operacao){
            case "consulta":
                
                $("#tb_registros").html(retorno.html);
                $("#popup_form").dialog("open");
                
                break;
            default:
                alert("js: Operação Inválida.");
        };
    }
}

function ver_detalhes(cod){
    $("#CodPedido").val(cod);
    formdata = $("#form1").serialize();
    submit_form(destino);
    return false;
}
