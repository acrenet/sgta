<?php 

    ob_start();
    
    require_once '../../code/funcoes.php';

?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ordem de Serviço de Trilhas</title>
        <link href="../tabelas.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
        <htmlpagefooter name="myFooter">
            <hr style="margin-bottom: 1px;">
            <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; 
             font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">
                <tr>
                    <td width="33%" style=" border-width: 0px; border-color:  #FFFFFF; border-style:  none;"><span style="font-weight: bold; font-style: italic;">ORDEM DE SERVIÇO DE TRILHAS</span></td>
                    <td width="33%" align="center" style="font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">Página: {PAGENO} de {nbpg}.</td>
                    <td width="33%" style="text-align: right;  border-width: 0px; border-color:  #FFFFFF; border-style:  none;"><?php echo date("d-m-Y H:i:s") ?></td>
                </tr>
            </table>
        </htmlpagefooter>

        <sethtmlpagefooter name="myFooter" page="1" value="on" show-this-page="1"></sethtmlpagefooter>
        
        <table border="0" style="width: 100%;">
            <tr>
                <td style="width: 50%;">
                    <img src="../../images/logo_cge_2.png" width="350" style="float: right; display: block;" />
                </td>
                <td style="width: 50%; text-align: right;">
                    <img src="../../images/logo_uf_1.JPG" width="150" height="80" /> 
                </td>
            </tr>
        </table> 
        <br><br>
        <p style="text-align: center; font-size: 16px; margin-bottom: 10px; font-weight: bold;">
                RELATORIO DE ORDEM DE SERVIÇO DE TRILHAS
        </p>
        <br><br>
        
        <?php
            if(!isset($_POST['cadastroini'])){
                echo 'OPERAÇÃO INVÁLIDA';
                goto fim;
            }
            
            $cadastroini = $_POST['cadastroini'];
            $cadastrofim = $_POST['cadastrofim'];
            $fechamentoini = $_POST['fechamentoini'];
            $fechamentofim = $_POST['fechamentofim'];
            $status = $_POST['status'];
            $servidor = $_POST['servidor'];
            $area = $_POST['area'];
            
            
            $Where = "CodRegistro > 0";
            
            $log = "";
        
            if($cadastroini != "" && $cadastrofim != ""){
                $Where = $Where . " And TO_DAYS(DataOrdem) BETWEEN TO_DAYS(DATE('".formata_data_mysql($cadastroini)."')) AND TO_DAYS(DATE('".formata_data_mysql($cadastrofim)."'))";
                $log = "Data de Cadastro entre: $cadastroini e $cadastrofim.<br>";
            }elseif ($cadastroini != ""){
                $Where = $Where . " And TO_DAYS(DataOrdem) >= TO_DAYS(DATE('".formata_data_mysql($cadastroini)."'))";
                $log = "Data de Cadastro maior ou igual a $cadastroini.<br>";
            }elseif ($cadastrofim != ""){
                $Where = $Where . " And TO_DAYS(DataOrdem) <= TO_DAYS(DATE('".formata_data_mysql($cadastrofim)."'))";
                $log = "Data de Cadastro menor ou igual a $cadastroini.<br>";
            }

            if($fechamentoini != "" && $fechamentofim != ""){
                $Where = $Where . " And TO_DAYS(DataFechamento) BETWEEN TO_DAYS(DATE('".formata_data_mysql($fechamentoini)."')) AND TO_DAYS(DATE('".formata_data_mysql($fechamentofim)."'))";
                $log = $log. "Data de Fechamento entre: $fechamentoini e $fechamentofim.<br>";
            }elseif ($fechamentoini != ""){
                $Where = $Where . " And TO_DAYS(DataFechamento) >= TO_DAYS(DATE('$".formata_data_mysql($fechamentoini)."'))";
                $log = $log."Data de Fechameno maior ou igual a $fechamentoini.<br>";
            }elseif ($fechamentofim != ""){
                $Where = $Where . " And TO_DAYS(DataFechamento) <= TO_DAYS(DATE('".formata_data_mysql($fechamentofim)."'))";
                $log = $log."Data de Fechamento menor ou igual a $fechamentofim.<br>";
            }

            if($status != "todos"){
                $Where = $Where . " And StatusOS = '$status'";
                $log = $log."Status: $status.<br>";
            }

            if($servidor != "todos"){
                $Where = $Where . " And CPF = '$servidor'";
                $log = $log."Responsável: $servidor.<br>";
            }
            
            if($area != "todas"){
                $Where = $Where . " And DescricaoTipo = '$area'";
                $log = $log."Área: $area.<br>";
            }
            
            if($log == ""){
                $log = "Exibindo todos os registros.<br>";
            }
            
            $sql = "Select * From view_ordem_servico Where $Where Order By DataOrdem, DataFechamento";
            
            //echo $sql."<br><br>$log";
            
            require_once '../../obj/conexao.php';
            $con = new conexao();
            
            $query = $con->select($sql);
            if($con->erro != ""){
                echo $con->erro;
                goto fim;
            }
            
            $hoje = new DateTime();
            
            $somaabertos = 0;
            $contabertos = 0;
            $somafechados = 0;
            $contfechados = 0;
            
            if($con->num_rows == 0){
                $tabela = "";
            }else{
                while ($row = mysqli_fetch_array($query)){

                    if($row['NumOrdem'] < 10 ){
                        $row['NumOrdem'] = "000".$row['NumOrdem'];
                    }elseif($row['NumOrdem'] < 100){
                        $row['NumOrdem'] = "00".$row['NumOrdem'];
                    }elseif($row['NumOrdem'] < 1000){
                        $row['NumOrdem'] = "0".$row['NumOrdem'];
                    }

                    $abertura = new DateTime($row['DataOrdem']);

                    if($row['StatusOS'] == "aberto"){
                        $intervalo = $abertura->diff($hoje);
                        $dias = $intervalo->days;
                        $contabertos ++;
                        $somaabertos = $somaabertos + $dias;
                    }else{
                        $fechamento = new DateTime($row['DataFechamento']);
                        $intervalo = $abertura->diff($fechamento);
                        $dias = $intervalo->days;
                        $contfechados++;
                        $somafechados = $somafechados + $dias;
                    }

                    $tabela[] = array(
                                        "CodigoOS" => $row['CodOrdem'],
                                        "NumeroOS" => $row['NumOrdem']."-".$row['AnoOrdem'],
                                        "ResponsavelCPF" => $row['CPF'],
                                        "ResponsavelNome" => $row['Nome'],
                                        "Perfil" => $row['NomePerfil'],
                                        "DataAbertura" => data_br($row['DataOrdem']),
                                        "DataFechamento" => data_br($row['DataFechamento']),
                                        "DescricaoOS" => $row['Descricao'],
                                        "ResultadoFinal" => $row['Resultado'],
                                        "JustificativaDoAuditor" => $row['Justificativa'],
                                        "StatusOS" => $row['StatusOS'],
                                        "Dias" => $dias
                                     );
                }
            }
            //echo 'linhas: '.$con->num_rows.'<br>';
            //echo $sql."<br><br>$log";
            //echo print_r($tabela);
            //goto fim;
            
            if($contabertos > 0){
                $mediaabertos = intval($somaabertos/$contabertos);
            }else{
                $mediaabertos = 0;
            }
            
            if($contfechados > 0){
                $mediafechados = intval($somafechados/$contfechados);
            }else{
                $mediafechados = 0;
            }
            
        ?>
        
        <div style="font-size: 14px; line-height: 150%;">
            <b>CRITÉRIOS:</b><br>
            <?php 
                echo $log;
            ?>
        </div>
        
        <br>
        
        <div style="font-size: 14px; line-height: 150%;">
            <b>Média de dias das OS em aberto: <?php echo $mediaabertos; ?></b><br>
            <b>Média de dias das OS fechadas: <?php echo $mediafechados; ?></b><br>
        </div>
        
        <br>
        <p style="margin-bottom: 5px;"><?php echo $con->num_rows; ?> registros encontrados.</p>
        <table class="tab1" style="width: 100%">
            <thead>
                <tr>
                    <th>Nr. OS</th>
                    <th>Resp. CPF</th>
                    <th>Resp. Nome</th>
                    <th>Perfil</th>
                    <th>Data Abertura</th>
                    <th>Data Fech.</th>
                    <th>Descrição</th>
                    <th>Resultado</th>
                    <th>Justificativa do Auditor</th>
                    <th>Status</th>
                    <th>Dias</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    if($tabela != ""){
                        for ($index = 0; $index < count($tabela); $index++){

                            echo
                            '
                            <tr>
                                <td>'.$tabela[$index]['NumeroOS'].'</td>
                                <td>'.$tabela[$index]['ResponsavelCPF'].'</td>
                                <td>'.$tabela[$index]['ResponsavelNome'].'</td>
                                <td>'.$tabela[$index]['Perfil'].'</td>
                                <td style="text-align: center;">'.$tabela[$index]['DataAbertura'].'</td>
                                <td style="text-align: center;">'.$tabela[$index]['DataFechamento'].'</td>
                                <td>'.$tabela[$index]['DescricaoOS'].'</td>
                                <td>'.$tabela[$index]['ResultadoFinal'].'</td>
                                <td>'.$tabela[$index]['JustificativaDoAuditor'].'</td>
                                <td>'.$tabela[$index]['StatusOS'].'</td>
                                <td style="text-align: right;">'.$tabela[$index]['Dias'].'</td>
                            </tr>
                            ';

                        }
                    }
                
                ?>
            </tbody>
        </table>
        <p style="text-align: right; margin-top: 5px;">Fim do relatório.</p>
        

        
        <?php
            fim:
        ?>
    </body>
</html>


<?php
    
    
    $formato = 'A4-L'; //A4 para retrato e A4-L para paisagem
    $margin_left = 10;
    $margin_right = 10;
    $margin_top = 16.3;
    $margin_bottom = 15;
    $margin_header = 11;
    $margin_footer = 9;

    //$html = urldecode($_SESSION['_htmlPDF']);
    $html = ob_get_clean();
    define('MPDF_PATCH', '../../plugin/mpdf60/');        
    include(MPDF_PATCH.'mpdf.php');
    $mpdf = new mPdf('',$formato,'','',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,'');
    $mpdf->allow_charset_conversion=true;
    $mpdf->charset_in='UTF-8';
    $mpdf->useSubstitutions = false;
    $mpdf->simpleTables = true;
    $mpdf->WriteHTML($html);
     //andre trocar D pelo I para ir para a pagina
    $mpdf->Output('Pedido de Prorogação de Prazo.pdf','I');

    
    exit();
    
