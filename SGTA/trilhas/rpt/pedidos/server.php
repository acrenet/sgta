<?php

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = "indefinido";
}

$array = array(
    "operacao" => $operacao,
    "erro" => "",
    "alerta" => "",
    "noticia" => "",
    "retorno" => ""
);


try {
    
    switch ($array["operacao"]) {
        case "consulta":
            $CodPedido = $_POST['CodPedido'];
            
            require_once('../../obj/pedidos.php');
    
            $obj_pedido = new pedidos();
            $query = $obj_pedido->consulta_detalhe($CodPedido);
            if($obj_pedido->erro != ""){
                $array['erro'] = $obj_pedido->erro;
                retorno();
            }
            
            $html = "";
            
            while ($row = mysqli_fetch_array($query)){
                $html = $html.
                '
                    <tr>
                    <td>'.$row['CodTrilha'].'</td>
                    <td>'.$row['NomeTrilha'].'</td>
                    <td>'.$row['CPF_CNPJ_Proc'].'</td>
                    <td>'.$row['Nome_Descricao'].'</td>
                    <td style="text-align: right;">'.$row['Dias'].'</td>
                </tr>
                ';
            }
            
            $array['html'] = $html;
            
            retorno();
            break;
        default:
            $array["alerta"] = "Operação: ''$operacao'' Inválida.";
            retorno();
            break;
    }
    
} catch (Exception $ex) {
    $array["erro"] = $ex->getMessage();
    retorno();
}
    

function retorno(){
    global $array;
       
    usleep(300000); //Previne ataque de DDoS.
    echo json_encode($array);
    exit();
}