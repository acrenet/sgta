<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>SGTA - Impressão</title>
        <script src="../../js/Chart.bundle.js" type="text/javascript"></script>
        <script src="../../js/utils.js" type="text/javascript"></script>
    </head>
    <body>
        
        <?php 
            
            require_once '../../code/funcoes.php';
            
            session_start();
            
            $perfil = $_SESSION['sessao_perfil'];
            $CodOrgao = $_SESSION['sessao_orgao'];
            
            if(isset($_POST['trilhas'])){
                $CodTrilha = $_POST['trilhas'];

                $codigos = "";
                $primeiro = true;
                foreach( $CodTrilha as $key => $Cod ) {
                    $codigos = $Cod;
                }
            }elseif(isset($_POST['CodTrilha'])){
                $codigos = $_POST['CodTrilha'];
            }else{

                echo 'OPERAÇÃO INVÁLIDA, SELECIONE AS TRILHAS QUE DESEJA INCLUIR NO RELATÓRIO.';
                goto fim;
            }
            
            if($perfil < 5){
                $sql = "Select * From view_registros Where CodTrilha = $codigos And StatusTrilha <> 'Pendente' And StatusRegistro <> 'Excluído' Order By CodTrilha Desc, Nome_Descricao";
            }else{
                $sql = "Select * From view_registros Where CodTrilha = $codigos And StatusTrilha <> 'Pendente' And StatusRegistro <> 'Excluído' And CodOrgao = $CodOrgao Order By CodTrilha Desc, Nome_Descricao";
            }
            
            require_once '../../obj/usuario.php';
            $usu = new usuario();
            
            require_once '../../obj/conexao.php';
            $con = new conexao();
            
            $query = $con->select($sql);
            if($con->erro != ""){
                echo $con->erro;
                goto fim;
            }
            
            if($con->num_rows == 0){
                $tabela = "";
            }else{
                while ($row = mysqli_fetch_array($query)){
                    
                    
                    if($row['StatusTrilha'] == "pré-análise"){
                        
                        $row['StatusRegistro'] = "pré-análise";
                        
                        if($row['ReservadoPor'] != ''){//foi distribuido a um auditor.

                            if($row['Pre_Analisado'] == 1){
                                $row['StatusRegistro'] = "pré-analisado";
                                
                            }elseif($row['RespObs'] != ''){
                                $row['StatusRegistro'] = "pré-analisado";
                                
                            }else{
                                $row['StatusRegistro'] = "pré-análise";
                            }
   
                        }elseif($row['RespObs'] != ''){
                            $row['StatusRegistro'] = "pré-analisado";
                            
                        }
                    }
                    
                    $tabela[] = array(
                                        "CodTrilha" => $row['CodTrilha'],
                                        "NomeTrilha" => $row['NomeTrilha'],
                                        "DataPublicacao" => data_br($row['DataPublicacao']),
                                        "DataPublicacaoUs" => $row['DataPublicacao'],
                                        "DataLiberacao" => data_br($row['DataLiberacao']),
                                        "DataLiberacaoUs" => $row['DataLiberacao'],
                                        "StatusRegistro" => $row['StatusRegistro'],
                                        "StatusTrilha" => $row['StatusTrilha'],
                                        "QtdRegistros" => "0",
                                        "AreaTrilha" => $row['DescricaoTipo'],
                                     );   
                    
                }
            }
            
            
            $m_codtrilha = "";
            for ($index = 0; $index < count($tabela); $index++){
                if($m_codtrilha != $tabela[$index]['CodTrilha']){
                    $trilhas[$tabela[$index]['CodTrilha']] = 1;
                    $homologados[$tabela[$index]['CodTrilha']] = 0;
                    $justificados[$tabela[$index]['CodTrilha']] = 0;
                    $concluidos[$tabela[$index]['CodTrilha']] = 0; 
                    $nomes[$tabela[$index]['CodTrilha']] = $tabela[$index]["CodTrilha"] . "- " . $tabela[$index]["NomeTrilha"];
                }else{
                    $trilhas[$tabela[$index]['CodTrilha']]++;
                }
                $m_codtrilha = $tabela[$index]['CodTrilha'];

                if($tabela[$index]["StatusRegistro"] == "justificado" || $tabela[$index]["StatusRegistro"] == "em análise" || $tabela[$index]["StatusRegistro"] == "concluído"){
                    $justificados[$tabela[$index]['CodTrilha']]++;
                }

                if($tabela[$index]["StatusRegistro"] == "concluído"){
                    $concluidos[$tabela[$index]['CodTrilha']]++;
                }
                
                if($tabela[$index]["StatusRegistro"] != "pré-análise"){
                    $homologados[$tabela[$index]['CodTrilha']]++;
                }

            }
            
            $Hoje = new DateTime();
            if($tabela[0]['DataPublicacaoUs'] != ""){
                $Data = new DateTime($tabela[0]['DataPublicacaoUs']);
                $Intervalo = $Data->diff($Hoje);
                $Dias = $Intervalo->days;
            }else{
                $Dias = "";
            }

            if($Dias == 0){
                $Dias = " ";
            }
            
            if($tabela[0]['DataLiberacaoUs'] != ""){
                $Data = new DateTime($tabela[0]['DataLiberacaoUs']);
                $Intervalo = $Data->diff($Hoje);
                $Dias2 = $Intervalo->days;
            }else{
                $Dias2 = "";
            }

            if($Dias2 == 0){
                $Dias2 = " ";
            }
        
        ?>
        
        <script>
            
            var color = Chart.helpers.color;
            var barChartData = {

                datasets: [
                    {
                        label: 'Quantidade de Registros da Trilha                                                                                ',
                        backgroundColor: color(window.chartColors.purple).alpha(0.8).rgbString(),
                        borderColor: window.chartColors.purple,
                        borderWidth: 1,
                        data: [<?php echo count($tabela); ?>]
                    },
                    {
                        label: 'Quantidade de Registros Validados pelo TCE                                                              ',
                        backgroundColor: color(window.chartColors.blue).alpha(0.8).rgbString(),
                        borderColor: window.chartColors.blue,
                        borderWidth: 1,
                        data: [<?php echo $homologados[$tabela[0]['CodTrilha']]; ?>]
                    },
                    {
                        label: 'Quantidade de Registros Justificados pelos Órgãos                                                      ',
                        backgroundColor: color(window.chartColors.green).alpha(0.8).rgbString(),
                        borderColor: window.chartColors.green,
                        borderWidth: 1,
                        data: [<?php echo $justificados[$tabela[0]['CodTrilha']]; ?>]
                    },
                    {
                        label: 'Quantidade de Registros Justificados pelos Órgãos e Concluídos pelo TCE               ',
                        backgroundColor: color(window.chartColors.red).alpha(0.8).rgbString(),
                        borderColor: window.chartColors.red,
                        borderWidth: 1,
                        data: [<?php echo $concluidos[$tabela[0]['CodTrilha']]; ?>]
                    }
                ]

            };
            
            var config2 = {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        position: 'bottom'
                    },
                    scales: {
                        yAxes: [{
                                    display: true,
                                    ticks: {
                                        max: <?php echo count($tabela) + 5; ?>,
                                        beginAtZero:true
                                    },
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Quantidade'
                                    }
                                }]
                    },
                    events: false,
                    tooltips: {
                        enabled: false
                    },
                    hover: {
                        animationDuration: 0
                    },
                    animation: {
                        duration: 1,
                        onComplete: function () {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;
                            ctx.font = Chart.helpers.fontString(14, "bold", Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];                            
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            });
                        }
                    }
                }
            };

        var colorNames = Object.keys(window.chartColors);
            
        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(ctx, config2);
        };
        </script>
        
        <div id="headerpage" style="width: 800px; margin: auto auto;">

            <table border="0" style="width: 100%;">
                <tr>
                    <td style="width: 33%;">
                        <img src="../../images/logo_cge_2.png" width="200" style="float: right; display: block;" />
                    </td>
                    <td style="width: 34%; text-align: center;">
                    <?php
                        echo '<img src="http://'.$_SERVER['HTTP_HOST'] .'/trilhas/images/logo_cge.png" width="120" />';
                    ?>
                        
                    </td>
                    <td style="width: 33%; text-align: right;">
                        <img src="../../images/bannerobservatorio.png" width="200"/>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <br>
            <br>
            <p style="text-align: center; font-size: 22px; margin-bottom: -20px; font-weight: bold; margin-top: 0px; line-height: 180%; font-family: arial;">
                    SISTEMA DE GESTÃO DE TRILHAS DE AUDITORIA<br>
                    <span style="text-align: center; font-size: 18px; font-weight: bold;">
                        GRÁFICO DE ACOMPANHAMENTO DE TRILHAS
                    </span> 
            </p>
        </div>
            
        <div id="container" style="width: 800px; margin: auto auto;">

            <br><br><br>
            
            <div style="font-size: 13px; line-height: 150%; font-weight: bold; border-style: solid; border-width: 1px; border-color: silver;
            padding: 3px; margin-bottom: 5px; background-color: #eeeeee; display: block; float: left; width: 100%; "> 
                <table border="0" style=" font-family: arial; font-size: 14px;">
                        <tr>
                            <td style="width: 320px;">Área da Trilha:</td>
                            <td><?php echo $tabela[0]['AreaTrilha']; ?></td>
                        </tr>
                        <tr>
                            <td>Id/Nome da Trilha:</td>
                            <td><?php echo $tabela[0]['CodTrilha']." - ".$tabela[0]['NomeTrilha']; ?></td>
                        </tr>
                        <tr>
                            <td>Data da Entrega da Trilha à Área Responsável:</td>
                            <td><?php echo $tabela[0]['DataLiberacao']; ?></td>
                        </tr>
                        <tr>
                            <td>Dias Passados da Entrega:</td>
                            <td><?php echo $Dias2; ?></td>
                        </tr>
                        <tr>
                            <td>Data de Encaminhamento ao Órgão:</td>
                            <td><?php echo $tabela[0]['DataPublicacao']; ?></td>
                        </tr>
                        <tr>
                            <td>Dias Passados do Encaminhamento:</td>
                            <td><?php echo $Dias; ?></td>
                        </tr>
                </table>
            </div>
            <br>
            <hr>
            <canvas id="canvas" width="500" height="600" style="max-height: 600px; "></canvas>
            <hr>
        </div>
        <?php 
            fim:
        ?>
    </body>
</html>
