<?php

//Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
 
    try{
        $perfil = $_SESSION['sessao_perfil'];

        $_titulo = "Trilhas - Acompanhamento";

        require_once('../../obj/trilhas.php');
        $obj_trilhas = new trilhas();
      
        $obj_trilhas->consulta_tipos(-1);
        if($obj_trilhas->erro != ""){
            $array['erro'] = $obj_trilhas->erro;
            retorno();
        }
        $query = $obj_trilhas->query;
       //andre - tratar se mensagem para se caso não houver trilhar que satisfacam a condição do sql
      
        $obj_trilhas->consulta_trilhas_novo("Where StatusTrilha <> 'Pendente' Order By CodTrilha Desc");
        if($obj_trilhas->erro != ""){
         
            $array['erro'] = $obj_trilhas->erro;
            retorno();
        }
        $query2 = $obj_trilhas->query;
        $n = 0;

        if(count($obj_trilhas->result)  >0){

            for ($i = 0; $i < count($obj_trilhas->result) ; $i++) {
        
                //$obj_trilhas->result[$i][0] - CodTrilha
                //$obj_trilhas->result[$i][14] - NomeTrilha
                //$obj_trilhas->result[$i][21] - DescricaoTipo/AreaTrilha
                //$obj_trilhas->result[$i][20] - CodTipo
                //$obj_trilhas->result[$i][11] - StatusTrilha
                $array[$i] = array("CodTrilha" => $obj_trilhas->result[$i][0], "NomeTrilha" => $obj_trilhas->result[$i][14], "AreaTrilha" => $obj_trilhas->result[$i][21], "CodTipo" => $obj_trilhas->result[$i][20], "StatusTrilha" => $obj_trilhas->result[$i][11]);
            }
     
        } else {
         
            $array['erro'] = 'Não há trilhas pendentes';
           // continue;
           // retorno();
        }
  
    } catch (Exception $ex) {
            echo $ex->getMessage(). " linha: " . $ex->getLine();
    }
?>

<script>
    var json = <?php echo json_encode($array) ?>;
    //ANDRE
    function preenche_trilhas(){
        html = "";
        for(var i = 0; i < json.length; i++) {
            var obj = json[i];
            var CodTipo = $( "#area" ).val();
            console.log(json);
            console.log(CodTipo + " =  " + obj.CodTipo);
            if( parseInt(CodTipo) == parseInt(obj.CodTipo) ){
                console.log(obj.CodTrilha + "  " + obj.NomeTrilha);
                html = html + '<input type="checkbox" name="trilhas[]" value="' + obj.CodTrilha + '" /> ' + obj.CodTrilha + " - " + obj.StatusTrilha + " - " + obj.NomeTrilha + '<br>';
            }    
        }
        $( "#divtrilhas" ).html(html);
    }
    
    function submeter_form(){
    
    }
</script>

<div class="container">
    <br>
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Acompanhamento de Trilhas</h4></div>
            <div class="panel-body">
                <h4><b>Selecione e Área e Depois as trilhas de deseja incluir no relatório</b></h4>
                <br>
                <form id="form1" class="form-horizontal" role="form" method="post" target="_blank" action="acompanhamento.php">
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="nome">Área:</label>
                        <div class="col-sm-4">
                        <!--ANDRE-->
                            <select name="area" id="area" class="form-control" onchange="preenche_trilhas();">
                                <?php 
                                    while($row = mysqli_fetch_array($query)){
                                        echo '<option value="'.$row['CodTipo'].'">'.$row['DescricaoTipo'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="nome">Trilhas Disponíveis:</label>
                        <div id="divtrilhas" class="col-sm-9">
                            <input type="checkbox" name="trilhas[]" value="10" /> Nome da Trilha
                        </div>
                    </div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-info" onclick="" style="width: 120px;">Imprimir &nbsp;&nbsp;<i class="fa fa-print fa-lg"></i></button>  
                        </div>
                    </div>

                    <input type="hidden" name="operacao" id="operacao" value="" />
                </form>
            </div>
        </div>
    </div>            
</div>
<script>
    preenche_trilhas();
</script>

<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
 include("../../master/master.php");