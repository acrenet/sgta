<?php 
    
    ob_start();
    
    require_once '../../code/funcoes.php';

?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Acompanhamento de Trilhas</title>
        <link href="../tabelas.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
        <htmlpagefooter name="myFooter">
            <hr style="margin-bottom: 1px;">
            <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; 
             font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">
                <tr>
                    <td width="33%" style=" border-width: 0px; border-color:  #FFFFFF; border-style:  none;"><span style="font-weight: bold; font-style: italic;">ACOMPANHAMENTO DE TRILHAS</span></td>
                    <td width="33%" align="center" style="font-weight: bold; font-style: italic; border-width: 0px; border-color:  #FFFFFF; border-style:  none;">Página: {PAGENO} de {nbpg}.</td>
                    <td width="33%" style="text-align: right;  border-width: 0px; border-color:  #FFFFFF; border-style:  none;"><?php echo date("d-m-Y H:i:s") ?></td>
                </tr>
            </table>
        </htmlpagefooter>

        <sethtmlpagefooter name="myFooter" page="1" value="on" show-this-page="1"></sethtmlpagefooter>
     
        <table border="0" style="width: 100%;" >
            <tr>
            <td style="width: 50%;">
            <?php 
                echo '<img src="http://'.$_SERVER['HTTP_HOST'] .'/trilhas/images/logo_cge.png" width="150" height="80" /> ';
            ?>
                    
                </td>
             
                <td style="width: 50% ; text-align: right;" >
                    <table border="0" style="margin-top: 20px;">
                        <tr>
                            <td>
                                <img src="../../images/logo_cge_2.png" width="150" style="float: right; display: block;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../../images/bannerobservatorio.png" width="150"/>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        </table>
        
        <br>
        <p style="text-align: center; font-size: 22px; margin-bottom: -20px; font-weight: bold; margin-top: 0px; line-height: 180%;">
                SISTEMA DE GESTÃO DE TRILHAS DE AUDITORIA<br>
                <span style="text-align: center; font-size: 18px; font-weight: bold;">
                    RELATÓRIO DE ACOMPANHAMENTO DE TRILHAS
                </span> 
        </p>
        
        <br>
        
        <?php
        
        try{
            
            session_start();
            
            $perfil = $_SESSION['sessao_perfil'];
            $CodOrgao = $_SESSION['sessao_orgao'];
            
            if(isset($_POST['trilhas'])){
                $CodTrilha = $_POST['trilhas'];

                $codigos = "";
                $primeiro = true;
                foreach( $CodTrilha as $key => $Cod ) {
                    if($primeiro == false){
                        $codigos = $codigos.",";
                    }
                    $codigos = $codigos.$Cod;
                    $primeiro = false;
                }
            }elseif(isset($_POST['CodTrilha'])){
                $codigos = $_POST['CodTrilha'];
            }else{
                echo 'OPERAÇÃO INVÁLIDA, SELECIONE AS TRILHAS QUE DESEJA INCLUIR NO RELATÓRIO.';
                goto fim;
            }
            //somente usuarios com nivel 4 ou acima podem ver tudo
            if($perfil < 5){
                $sql = "Select * From view_registros A inner join orgaos B on A.CodOrgao = B.CodOrgao Where A.CodTrilha In ($codigos) And A.StatusTrilha <> 'Pendente' And A.StatusRegistro <> 'Excluído' Order By A.CodTrilha Desc, A.Nome_Descricao";
            }else{
                $sql = "Select * From view_registros A inner join orgaos B on A.CodOrgao = B.CodOrgao Where A.CodTrilha In ($codigos) And A.StatusTrilha <> 'Pendente' And A.StatusRegistro <> 'Excluído' And A.CodOrgao = $CodOrgao Order By A.CodTrilha Desc, A.Nome_Descricao";
            }
           
            require_once '../../obj/usuario.php';
            $usu = new usuario();
            
            require_once '../../obj/conexao.php';
            $con = new conexao();
            
            $query = $con->select($sql);
            if($con->erro != ""){
                echo $con->erro;
                goto fim;
            }
            
            if($con->num_rows == 0){
                $tabela = "";
            }else{
                while ($row = mysqli_fetch_array($query)){
                    
                    $CodRegistro = $row['CodRegistro'];
                    $Hoje = new DateTime();
                    $LocalRegistro = "";
                    $Responsavel = "";
                    $DataEnvio = "";
                    $Dias = "";
                    $resultadofinal = "";
            
                    if($row['QuarentenaAte'] != ""){
                        $LocalRegistro = "TCE - Aguardando o prazo do monitoramento até: ". data_br($row['QuarentenaAte']);
                        $resultadofinal = ":<br>Em Monitoramento até: ". data_br($row['QuarentenaAte']);
                        $Responsavel = $usu->consulta_nome($row['UsuarioCGE']);
                    }else if($row['NumProcAuditoria'] != "" && $row['TipoProcesso'] == "auditoria"){
                        $LocalRegistro = "Concluído após abertura de processo de auditoria nº. ".$row['NumProcAuditoria'];           //*************************
                        $resultadofinal = ":<br>Auditoria. Processo: ".$row['NumProcAuditoria'];
                        $Responsavel = $usu->consulta_nome($row['UsuarioCGE']);
                    }else if($row['NumProcAuditoria'] != "" && $row['TipoProcesso'] == "inspeção"){
                        $LocalRegistro = "Concluído após abertura de processo de inspeção nº. ".$row['NumProcAuditoria'];            //*************************
                        $resultadofinal = ":<br>Inspeção. Processo: ".$row['NumProcAuditoria'];
                        $Responsavel = $usu->consulta_nome($row['UsuarioCGE']);
                    }else if($row['SolicitadoAuditoria'] == true){
                        $resultadofinal = ":<br>Recomendado Abertura de Processo de Inspeção/Auditoria.";
                        $LocalRegistro = "TCE - Aguardando Decisão do Superintendente para Abertura de Processo de Inspeção/Auditoria.";   //*************************
                        $Responsavel = "Superintendente do Área da Trilha";
                        $sql = "Select DataAndamento From andamento_registro Where CodRegistro = $CodRegistro And Descricao = 'Recomendado Abertura de Processo de Inspeção/Auditoria.' Order By CodAndamento Desc Limit 1";
                        $query2 = $con->select($sql);
                        if($con->erro != ""){
                            echo $con->erro;
                            goto fim;
                        }
                        if($con->num_rows > 0){
                            $row2 = mysqli_fetch_array($query2);
                            $DataEnvio = data_br($row2['DataAndamento']);
                            $Data = new DateTime($row2['DataAndamento']);
                            $Intervalo = $Data->diff($Hoje);
                            $Dias = $Intervalo->days;
                        }
                    }else if($row['StatusRegistro'] == "concluído"){
                        $LocalRegistro = "Concluído após a justificativa do órgão.";    //*************************
                        $resultadofinal = ":<br>Registro Arquivado";
                        $Responsavel = $usu->consulta_nome($row['UsuarioCGE']);
                    }else if($row['StatusRegistro'] == "justificado" || $row['StatusRegistro'] == "em análise"){
                        
                        if($row['StatusRegistro'] == "justificado"){
                            $row['StatusRegistro'] = "justificado pelo órgão";
                            $LocalRegistro = "Justificado pelo órgão, aguardando análise pelo TCE.";
                        }else{
                            $row['StatusRegistro'] = "justificado pelo órgão, em análise pelo auditor";
                            $LocalRegistro = "justificado pelo órgão, em análise pelo auditor do TCE.";
                        }
                        
                        if($row['ReservadoPor'] == ""){
                            $LocalRegistro = "TCE - Aguardando Distribuição por parte da superintendência ou gerência.";
                            $Responsavel = "Gestor da Trilha no TCE (superintendente ou gerente)";
                            $DataEnvio = data_br($row['DataConclusaoJustificativa']);
                            $Data = new DateTime($row['DataConclusaoJustificativa']);
                            $Intervalo = $Data->diff($Hoje);
                            $Dias = $Intervalo->days;
                        }else{
                            $LocalRegistro = "TCE - Aguardando Análise pelo Auditor";
                            $Responsavel = $usu->consulta_nome($row['ReservadoPor']);
                            $sql = "Select DataAndamento From andamento_registro Where CodRegistro = $CodRegistro And Descricao Like 'Reservado o registro de inconsistência para o servidor%' Order By CodAndamento Desc Limit 1";
                            $query2 = $con->select($sql);
                            if($con->erro != ""){
                                echo $con->erro;
                                goto fim;
                            }
                            if($con->num_rows > 0){
                                $row2 = mysqli_fetch_array($query2);
                                $DataEnvio = data_br($row2['DataAndamento']);
                                $Data = new DateTime($row2['DataAndamento']);
                                $Intervalo = $Data->diff($Hoje);
                                $Dias = $Intervalo->days;
                            }
                        }
                        
                    }else if($row['StatusTrilha'] == "pré-análise"){
                        if($row['DataLiberacao'] != ""){
                            $Data = $row['DataLiberacao'];
                        }else{
                            $Data = $row['DataTrilha'];
                        }
                        $DataEnvio = data_br($Data);
                        $Data = new DateTime($Data);
                        $Intervalo = $Data->diff($Hoje);
                        $Dias = $Intervalo->days;
                        $LocalRegistro = "TCE - Aguardando distribuição para ser analisado (pré-análise).";
                        
                        $row['StatusRegistro'] = "aguardando a pré-análise";
                        
                        if($row['ReservadoPor'] != ''){//foi distribuido a um auditor.

                            $sql = "Select DataAndamento From andamento_registro Where Descricao Like 'Reservado o registro de inconsistência "
                                 . "para o servidor:%' And CodRegistro = ".$row['CodRegistro']." Order By CodAndamento Desc Limit 1";
                            $query2 = $con->select($sql);
                            if($con->erro != ""){
                                echo $con->erro;
                                goto fim;
                            }
                            if($con->num_rows > 0){
                                $row2 = mysqli_fetch_array($query2);
                                $DataEnvio = data_br($row2['DataAndamento']);
                                $Data = new DateTime($row2['DataAndamento']);
                                $Intervalo = $Data->diff($Hoje);
                                $Dias = $Intervalo->days;
                            }
                            
                            if($row['Pre_Analisado'] == 1){
                                $row['StatusRegistro'] = "pré-analisado pelo TCE";
                                $LocalRegistro = "TCE - Aguardando o envio da trilha para o órgão, por parte da superintedência ou gerência.";
                                $Responsavel = "Gestor da Trilha no TCE (superintendente ou gerente)";
                                $sql = "Select DataAndamento From andamento_registro Where Descricao = 'Concluída a homologação do registro:' And CodRegistro = ".$row['CodRegistro']
                                 . " Order By CodAndamento Desc Limit 1";
                                $query2 = $con->select($sql);
                                if($con->erro != ""){
                                    echo $con->erro;
                                    goto fim;
                                }
                                if($con->num_rows > 0){
                                    $row2 = mysqli_fetch_array($query2);
                                    $DataEnvio = data_br($row2['DataAndamento']);
                                    $Data = new DateTime($row2['DataAndamento']);
                                    $Intervalo = $Data->diff($Hoje);
                                    $Dias = $Intervalo->days;
                                }
                            }elseif($row['RespObs'] != ''){
                                $row['StatusRegistro'] = "pré-analisado pelo TCE";
                                $LocalRegistro = "TCE - Aguardando o envio da trilha para o órgão, por parte da superintedência ou gerência.";
                                $Responsavel = "Gestor da Trilha no TCE (superintendente ou gerente)";
                                $sql = "Select DataAndamento From andamento_registro Where Descricao = 'Incluído/Modificado Observação TCE' And CodRegistro = ".$row['CodRegistro']
                                 . " Order By CodAndamento Desc Limit 1";
                                $query2 = $con->select($sql);
                                if($con->erro != ""){
                                    echo $con->erro;
                                    goto fim;
                                }
                                if($con->num_rows > 0){
                                    $row2 = mysqli_fetch_array($query2);
                                    $DataEnvio = data_br($row2['DataAndamento']);
                                    $Data = new DateTime($row2['DataAndamento']);
                                    $Intervalo = $Data->diff($Hoje);
                                    $Dias = $Intervalo->days;
                                }
                            }else{
                                $row['StatusRegistro'] = "aguardando a pré-análise";
                                $LocalRegistro = "TCE - Aguardando a conclusão da pré-análise.";
                                $Responsavel = $usu->consulta_nome($row['ReservadoPor']);
                            }
   
                        }elseif($row['RespObs'] != ''){
                            $row['StatusRegistro'] = "pré-analisado pelo TCE";
                            $LocalRegistro = "TCE - Aguardando o envio da trilha para o órgão, por parte da superintedência ou gerência.";
                            $Responsavel = "Gestor da Trilha no TCE (superintendente ou gerente)"; 
                            $sql = "Select DataAndamento From andamento_registro Where Descricao = 'Incluído/Modificado Observação TCE' And CodRegistro = ".$row['CodRegistro']
                                 . " Order By CodAndamento Desc Limit 1";
                            $query2 = $con->select($sql);
                            if($con->erro != ""){
                                echo $con->erro;
                                goto fim;
                            }
                            if($con->num_rows > 0){
                                $row2 = mysqli_fetch_array($query2);
                                $DataEnvio = data_br($row2['DataAndamento']);
                                $Data = new DateTime($row2['DataAndamento']);
                                $Intervalo = $Data->diff($Hoje);
                                $Dias = $Intervalo->days;
                            }
                        }else{
                            $Responsavel = "Gestor da Trilha no TCE (superintendente ou gerente)";
                        }
                    }else{
                        if($row['StatusRegistro'] == "pendente"){
                            $row['StatusRegistro'] = "pendente de justificativa pelo órgão";
                            $LocalRegistro = "Pendente de justificativa pelo órgão";
                        }
                        if($row['StatusRegistro'] == "em justificativa"){
                            $row['StatusRegistro'] = "em justificativa pelo órgão";
                            $LocalRegistro = "Em justificativa pelo órgão";
                        }
                        
                        $LocalRegistro = "Aguardando a Justificativa pelo Órgão.";
                        $DataEnvio = data_br($row['DataPublicacao']);
                        $Data = new DateTime($row['DataPublicacao']);
                        $Intervalo = $Data->diff($Hoje);
                        $Dias = $Intervalo->days;
                        //$Responsavel = $usu->consulta_nome($row['UsuarioOrgao']);
                        
                        if($Responsavel == ""){
                            $Responsavel = "Supervisor da Trilha no Órgão";
                        }
                    }
                    
                    $tabela[] = array(
                                        "CodTrilha" => $row['CodTrilha'],
                                        "NomeTrilha" => $row['NomeTrilha'],
                                        "CPF_CNPJ_PROC" => $row['CPF_CNPJ_Proc'],
                                        "Nome_Descricao" => $row['CPF_CNPJ_Proc'].":<br>".$row['Nome_Descricao'],
                                        "SiglaOrgao" => $row['SiglaOrgao'],
                                        "SiofiNet" => $row['SiofiNet'],
                                        "DataPublicacao" => data_br($row['DataPublicacao']),
                                        "DataPublicacaoUs" => $row['DataPublicacao'],
                                        "StatusRegistro" => $row['StatusRegistro'],
                                        "StatusTrilha" => $row['StatusTrilha'],
                                        "ResultadoFinal" => $resultadofinal,
                                        "LocalRegistro" => $LocalRegistro,
                                        "Responsavel" => $Responsavel,
                                        "DataEnvio" => $DataEnvio,
                                        "Dias" => $Dias,
                                        "QtdRegistros" => "0",
                                        "AreaTrilha" => $row['DescricaoTipo'],
                                        "ObsCGE" => str_replace("\r", '<br>', $row['ObsCGE']),
                                        "ResultadoAuditoria" => $row['ResultadoAuditoria'],
                                        "ValorRessarcido" => $row['ValorRessarcido'],
                                        "EconomiaMensal" => $row['EconomiaMensal']
                                     );
                }
                
                
                $m_codtrilha = "";
                for ($index = 0; $index < count($tabela); $index++){
                    if($m_codtrilha != $tabela[$index]['CodTrilha']){
                        $trilhas[$tabela[$index]['CodTrilha']] = 1;
                        $homologados[$tabela[$index]['CodTrilha']] = 0;
                        $justificados[$tabela[$index]['CodTrilha']] = 0;
                        $concluidos[$tabela[$index]['CodTrilha']] = 0; 
                        $atendidas[$tabela[$index]['CodTrilha']] = 0;
                        $aguardando[$tabela[$index]['CodTrilha']] = 0;
                        $recusadas[$tabela[$index]['CodTrilha']] = 0;
                        $economiamensal[$tabela[$index]['CodTrilha']] = 0;
                        $valorressarcido[$tabela[$index]['CodTrilha']] = 0;
                        $nomes[$tabela[$index]['CodTrilha']] = $tabela[$index]["CodTrilha"] . "- " . $tabela[$index]["NomeTrilha"];
                    }else{
                        $trilhas[$tabela[$index]['CodTrilha']]++;
                    }
                    $m_codtrilha = $tabela[$index]['CodTrilha'];
                    
                    if($tabela[$index]["StatusRegistro"] == "justificado pelo órgão" || $tabela[$index]["StatusRegistro"] == "justificado pelo órgão, em análise pelo auditor" || strpos($tabela[$index]["StatusRegistro"], "concluído") !== false){
                        $justificados[$tabela[$index]['CodTrilha']]++;
                    }

                    
                    $ResultadoAuditoria = explode(";",$tabela[$index]["ResultadoAuditoria"]);
                       
                    $Valor = explode("=", $ResultadoAuditoria[0]);
                    $expedicao = $Valor[1];

                    $Valor = explode("=", $ResultadoAuditoria[1]);
                    $sistemas = $Valor[1];

                    $Valor = explode("=", $ResultadoAuditoria[2]);
                    $capacitacao = $Valor[1];

                    $Valor = explode("=", $ResultadoAuditoria[3]);
                    $devolucao = $Valor[1];

                    if(strpos($tabela[$index]["LocalRegistro"], "TCE - Aguardando o prazo do monitoramento") !== false){
                        if($expedicao == 1){
                            $atendidas[$tabela[$index]['CodTrilha']] ++;
                        }elseif($expedicao == 2){
                            $aguardando[$tabela[$index]['CodTrilha']] ++;
                        }

                        if($sistemas == 1){
                            $atendidas[$tabela[$index]['CodTrilha']] ++;
                        }elseif($sistemas == 2){
                            $aguardando[$tabela[$index]['CodTrilha']] ++;
                        }

                        if($capacitacao == 1){
                            $atendidas[$tabela[$index]['CodTrilha']] ++;
                        }elseif($capacitacao == 2){
                            $aguardando[$tabela[$index]['CodTrilha']] ++;
                        }

                        if($devolucao == 1){
                            $atendidas[$tabela[$index]['CodTrilha']] ++;
                        }elseif($devolucao == 2){
                            $aguardando[$tabela[$index]['CodTrilha']] ++;
                        }
                        
                        $economiamensal[$tabela[$index]['CodTrilha']] = $economiamensal[$tabela[$index]['CodTrilha']] + $tabela[$index]["EconomiaMensal"];
                        $valorressarcido[$tabela[$index]['CodTrilha']] = $valorressarcido[$tabela[$index]['CodTrilha']] + $tabela[$index]["ValorRessarcido"];
                        

                    }else{
                        if($expedicao == 1){
                            $atendidas[$tabela[$index]['CodTrilha']] ++;
                        }elseif($expedicao == 2){
                            $recusadas[$tabela[$index]['CodTrilha']] ++;
                        }

                        if($sistemas == 1){
                            $atendidas[$tabela[$index]['CodTrilha']] ++;
                        }elseif($sistemas == 2){
                            $recusadas[$tabela[$index]['CodTrilha']] ++;
                        }

                        if($capacitacao == 1){
                            $atendidas[$tabela[$index]['CodTrilha']] ++;
                        }elseif($capacitacao == 2){
                            $recusadas[$tabela[$index]['CodTrilha']] ++;
                        }

                        if($devolucao == 1){
                            $atendidas[$tabela[$index]['CodTrilha']] ++;
                        }elseif($devolucao == 2){
                            $recusadas[$tabela[$index]['CodTrilha']] ++;
                        }
                    }
                    
                    
                    if(strpos($tabela[$index]["StatusRegistro"], "concluído") !== false){
                        $concluidos[$tabela[$index]['CodTrilha']]++; 
                        
                        $economiamensal[$tabela[$index]['CodTrilha']] = $economiamensal[$tabela[$index]['CodTrilha']] + $tabela[$index]["EconomiaMensal"];
                        $valorressarcido[$tabela[$index]['CodTrilha']] = $valorressarcido[$tabela[$index]['CodTrilha']] + $tabela[$index]["ValorRessarcido"];
                        
                    }
                    if($tabela[$index]["StatusRegistro"] == "aguardando a pré-análise" || $tabela[$index]["StatusRegistro"] == "pré-analisado pelo TCE"){
                        if($tabela[$index]['StatusRegistro'] == "pré-analisado pelo TCE"){
                            $homologados[$tabela[$index]['CodTrilha']]++;
                        }
                    }else{
                        $homologados[$tabela[$index]['CodTrilha']]++;
                    }
                    
                }
                
            }
            
            echo 
            '
            <p style="font-weight: bold; line-height: 150%;">
                Trilhas deste relatório:<br>
            ';
            
             //SGTA-135 - Exibição da data de prazo final para justificativa
             if(@isset($nomes)){
                foreach ($nomes as $key => $value){
                    echo $value."<br>";
                }   
            }
               
            
            echo '</p>';
            
            
            $m_codtrilha = "";
            $primeiro = true;
            for ($index = 0; $index < count($tabela); $index++){
                
                $tabela[$index]["StatusRegistro"] = "";
                
                if($m_codtrilha != $tabela[$index]['CodTrilha']){
                    $n = 1;
                    //nova tabela

                    if($tabela[$index]['DataPublicacaoUs'] != ""){
                        $Data = new DateTime($tabela[$index]['DataPublicacaoUs']);
                        $Intervalo = $Data->diff($Hoje);
                        $Dias = $Intervalo->days;
                    }else{
                        $Dias = "";
                    }
                    
                    if($Dias == 0){
                        $Dias = "-";
                    }

                    if($primeiro == false){
                        echo
                            '</tbody>
                        </table></div>
                        <br><br>';
                    }

                    $primeiro = false;

                    /*
                     * 150 ---- 100
                     *  25 ---- x 
                     *  x = 25*100/150
                     */

                    $QtdRegistros = $trilhas[$tabela[$index]['CodTrilha']];
                    $PercHomologados = " (".numero_br($homologados[$tabela[$index]['CodTrilha']] * 100 / $QtdRegistros) . "%)";
                    $PercJustificados = " (".numero_br($justificados[$tabela[$index]['CodTrilha']] * 100 / $QtdRegistros) . "%)";
                    $PercConcluidos = " (".numero_br($concluidos[$tabela[$index]['CodTrilha']] * 100 / $QtdRegistros) . "%)";
                    
                    
                    $QtdRecomendacoes = $atendidas[$tabela[$index]['CodTrilha']] + $aguardando[$tabela[$index]['CodTrilha']] + $recusadas[$tabela[$index]['CodTrilha']];
                    
                    if($homologados[$tabela[$index]['CodTrilha']] == 0){
                        $homologados[$tabela[$index]['CodTrilha']] = "-";
                    }
                    
                    if($justificados[$tabela[$index]['CodTrilha']] == 0){
                        $justificados[$tabela[$index]['CodTrilha']] = "-";
                    }
                    
                    if($concluidos[$tabela[$index]['CodTrilha']] == 0){
                        $concluidos[$tabela[$index]['CodTrilha']] = "-";
                    }
                    
                    if($atendidas[$tabela[$index]['CodTrilha']] == 0){
                        $atendidas[$tabela[$index]['CodTrilha']] = "-";
                    }
                    
                    if($aguardando[$tabela[$index]['CodTrilha']] == 0){
                        $aguardando[$tabela[$index]['CodTrilha']] = "-";
                    }
                    
                    if($recusadas[$tabela[$index]['CodTrilha']] == 0){
                        $recusadas[$tabela[$index]['CodTrilha']] = "-";
                    }
                    
                    if($QtdRecomendacoes == 0){
                        $QtdRecomendacoes = "-";
                    }
                    
                    
                    echo
                    '
                    <div style="font-size: 13px; line-height: 150%; font-weight: bold; border-style: solid; border-width: 1px; 
                    padding: 3px; margin-bottom: 5px; background-color: #eee; display: block; float: left; width: 100%;"> 
                        <div style="display: block; float: left; width: 75%;">
                            <div style="display: block; float: left; width: 19%;">
                                Área da Trilha:<br>
                                Nome da Trilha:<br>
                                Qtd Recomendações:<br>    
                                
                                <u>Recomendações:</u><br>
                                 - Atendidas:<br>
                                 - Aguardando:<br>
                                 - Recusadas:<br>
                            </div>
                            <div style="display: block; float: left; width: 81%;">
                                '.$tabela[$index]['AreaTrilha'].'<br>
                                '.$tabela[$index]['CodTrilha'].' - '.substr($tabela[$index]['NomeTrilha'],0,88).' <br>
                                '.$QtdRecomendacoes.'<br><br>
                                
                                '.$atendidas[$tabela[$index]['CodTrilha']].'<br>
                                '.$aguardando[$tabela[$index]['CodTrilha']].'<br>
                                '.$recusadas[$tabela[$index]['CodTrilha']].'<br>    
                            </div>
                        </div>
                        <div style="display: block; float: left; width: 24%;">
                            <div style="display: block; float: left; width: 60%;">
                                Data da Publicação:<br>
                                Dias da Publicação:<br>
                                Qtd Registros:<br>
                                Qtd Homologados:<br>
                                Qtd Justificados:<br>
                                Qtd Concluídos:<br>
                                Economia Mensal:<br>
                                Economia Gerada:<br>
                            </div>
                            <div style="display: block; float: left; width: 39%;">
                                '.$tabela[$index]['DataPublicacao'].'<br>
                                '.$Dias.'<br>
                                '.$trilhas[$tabela[$index]['CodTrilha']].'<br>
                                '.$homologados[$tabela[$index]['CodTrilha']]. $PercHomologados .'<br>    
                                '.$justificados[$tabela[$index]['CodTrilha']]. $PercJustificados .'<br>
                                '.$concluidos[$tabela[$index]['CodTrilha']]. $PercConcluidos .'<br>
                                '.numero_br($economiamensal[$tabela[$index]['CodTrilha']]).'<br>
                                '.numero_br($valorressarcido[$tabela[$index]['CodTrilha']]).'<br>
                            </div>
                        </div>
                            

                    </div>
                    <div style="clear: both;">
                    <table class="tab1" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Ord</th>
                                <th>Identificação</th>
                                <th>Órgão</th>
                                <th>Cod. Un.Sfinge</th>
                                <th>Questionamentos do TCE</th>
                                <th>Condição Atual</th>
                                <th>Atual Responsável</th>
                                <th>Dias com o Atual Resp.</th>
                                <th>Recomendações</th>
                                <th>Resultado</th>
                            </tr>
                        </thead>
                        <tbody>';
                    $m_codtrilha = $tabela[$index]['CodTrilha'];
                }else{
                    $n++;
                }
                
                $ResultadoAuditoria = explode(";",$tabela[$index]["ResultadoAuditoria"]);
                       
                $Valor = explode("=", $ResultadoAuditoria[0]);
                $expedicao = $Valor[1];

                $Valor = explode("=", $ResultadoAuditoria[1]);
                $sistemas = $Valor[1];

                $Valor = explode("=", $ResultadoAuditoria[2]);
                $capacitacao = $Valor[1];

                $Valor = explode("=", $ResultadoAuditoria[3]);
                $devolucao = $Valor[1];


                if($expedicao != 0 || $sistemas != 0 || $capacitacao != 0 || $devolucao != 0){

                    //$tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<br><b>Recomedações:</b>";
                    if(strpos($tabela[$index]["LocalRegistro"], "TCE - Aguardando o prazo do monitoramento") !== false){
                        if($expedicao != 0){
                            if($expedicao == 1){
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Expedição de Instrumentos Legais:</b> Sim, Atendido pelo Órgão.<br>";
                            }else{
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Expedição de Instrumentos Legais:</b> Sim, Aguardando o Atendimento<br>";
                            } 
                        }
                        if($sistemas != 0){
                            if($sistemas == 1){
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Criação/Reformulação de Sistemas: </b>Sim, Atendido pelo Órgão.<br>";
                            }else{
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Criação/Reformulação de Sistemas: </b>Sim, Aguardando o Atendimento<br>";
                            } 
                        }
                        if($capacitacao != 0){
                            if($capacitacao == 1){
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Capacitação Técnica dos Agentes: </b>Sim, Atendido pelo Órgão.<br>";
                            }else{
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Capacitação Técnica dos Agentes: </b>Sim, Aguardando o Atendimento<br>";
                            } 
                        }
                        if($devolucao != 0){
                            if($devolucao == 1){
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Devolução de Numerário: </b>Sim, Atendido pelo Órgão.<br>";
                            }else{
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Devolução de Numerário: </b>Sim, Aguardando o Atendimento<br>";
                            } 
                        }
                    }else{ //concluído
                        if($expedicao != 0){
                            if($expedicao == 1){
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Expedição de Instrumentos Legais: </b>Sim, Atendido pelo Órgão.<br>";
                            }else{
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Expedição de Instrumentos Legais: </b>Sim, <i>NÃO</i> Atendimento pelo Órgão<br>";
                            } 
                        }
                        if($sistemas != 0){
                            if($sistemas == 1){
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Criação/Reformulação de Sistemas: </b>Sim, Atendido pelo Órgão.<br>";
                            }else{
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Criação/Reformulação de Sistemas: </b>Sim, <i>NÃO</i> Atendimento pelo Órgão<br>";
                            } 
                        }
                        if($capacitacao != 0){
                            if($capacitacao == 1){
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Capacitação Técnica dos Agentes: </b>Sim, Atendido pelo Órgão.<br>";
                            }else{
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Capacitação Técnica dos Agentes: </b>Sim, <i>NÃO</i> Atendimento pelo Órgão<br>";
                            } 
                        }
                        if($devolucao != 0){
                            if($devolucao == 1){
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Devolução de Numerário: </b>Sim, Atendido pelo Órgão.";
                            }else{
                                $tabela[$index]['StatusRegistro'] = $tabela[$index]['StatusRegistro']."<b>- Requerido Devolução de Numerário: </b>Sim, <i>NÃO</i> Atendimento pelo Órgão";
                            } 
                        }
                    }
                }
                
                if(strlen($tabela[$index]['Nome_Descricao']) > 400){
                    $tabela[$index]['Nome_Descricao'] = substr($tabela[$index]['Nome_Descricao'],0 , 399) . " (...)";
                }

                if(strlen($tabela[$index]['ObsCGE']) > 600){
                    $tabela[$index]['ObsCGE'] = substr($tabela[$index]['ObsCGE'],0 ,599) . " (...)";
                }
                
                echo
                '<tr>
                    <td style="text-align: right;">'.$n.'</td>
                    <td>'.$tabela[$index]['Nome_Descricao'].'</td>
                    <td>'.$tabela[$index]['SiglaOrgao'].'</td> 
                    <td>'.$tabela[$index]['SiofiNet'].'</td>
                    <td>'.$tabela[$index]['ObsCGE'].'</td>
                    <td>'.$tabela[$index]['LocalRegistro'].'</td>
                    <td>'.$tabela[$index]['Responsavel'].'</td>
                    <td style="text-align: right; font-weight: bold;">'.$tabela[$index]['Dias'].'</td>
                    <td>'.$tabela[$index]['StatusRegistro'].'</td>
                    <td><b>Ecomonia:</b><br>-Mensal: '.numero_br($tabela[$index]['EconomiaMensal']).'<br>-Gerada: '.numero_br($tabela[$index]['ValorRessarcido']).'</td>    
                </tr>';
            }
            echo
                    '</tbody>
                </table></div>';
        } catch (Exception $ex) {
            echo $ex->getMessage(). " linha: " . $ex->getLine();
            goto fim;
        }
    ?>
        <br>
        <div style="font-size: 13px; line-height: 150%; border-style: solid; border-width: 1px; 
                    padding: 3px; margin-bottom: 5px; background-color: #eee; display: block; float: left; width: 100%;"> 
            
            <b>OBSERVAÇÕES:</b><br>
            <b>Registros Homologados:</b> São aqueles que passaram por uma análise prévia pelos auditores do TCE antes do envio para
            justificativa do órgão.<br>
            <b>Registros Justificados:</b> São aqueles que foram justificados pelo órgao e estão aptos para serem analisados pelos
            auditores do TCE.<br>
            <b>Registros Concluídos:</b> São aqueles que foram analisados pelos auditores do TCE após a justificativa pelo órgão. Podendo
            a justificativa ter sido aceita ou não.
            
        </div>
   
        <p style="text-align: right; margin-top: 5px;  clear: both;">Fim do relatório.</p>
        

        
        <?php
            fim:
        ?>
    </body>
</html>


<?php

    $formato = 'A4-L'; //A4 para retrato e A4-L para paisagem
    $margin_left = 10;
    $margin_right = 10;
    $margin_top = 16.3;
    $margin_bottom = 15;
    $margin_header = 11;
    $margin_footer = 9;

    //$html = urldecode($_SESSION['_htmlPDF']);
    $html = ob_get_clean();
   /*
    define('MPDF_PATCH', '../../plugin/mpdf60/');        
    include(MPDF_PATCH.'mpdf.php');
    $mpdf = new mPdf('',$formato,'','',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,'');
    $mpdf->allow_charset_conversion=true;
    $mpdf->charset_in='UTF-8';
    $mpdf->useSubstitutions = false;
    $mpdf->simpleTables = true;
    $mpdf->WriteHTML($html);
    //andre trocar D pelo I para ir para a pagina
    //$mpdf->Output('Acompanhamento de Trilhas.pdf','I');
    */
    
    exit($html);
    
