<?php
//Verificará se a nossa sessão está ativa
    require_once '../../code/verificar.php';

    //Ativa o Buffer que armazena o conteúdo principal da página
    ob_start();
    
    $perfil = $_SESSION['sessao_perfil'];
    
    if(!($perfil == 1 || $perfil == 2)){
        Header("Location: /trilhas/app/inicio/negado.php");
        die();
    }
    
    $_titulo = "Trilhas - Cadastrar Usuários";
    
    require_once('../../obj/ordem_servico.php');
    require_once('../../obj/usuario.php');
    
    $obj_os = new ordem_servico();
    $query = $obj_os->consulta_servidores_os(); 
    if($obj_os->erro != ""){
        echo $obj_os->erro;
        die();
    }
    
    $query2 = $obj_os->consulta_tipos_os(); 
    if($obj_os->erro != ""){
        echo $obj_os->erro;
        die();
    }
    
    
?>

<script src="../../js/datepicker.js" type="text/javascript"></script>
<script src="inicio.js" type="text/javascript"></script>

<br>

<div class="container">
    
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4>Relatório de Ordem de Serviço de Trilhas</h4></div>
            <div class="panel-body">
                <h4><b>Todos os Campos são de Preenchimento Opcional.</b></h4>
                <br>
                <form id="form1" class="form-horizontal" role="form" method="post" target="_blank" action="ordem_servico_trilha.php">

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="cpf">Data de Criação Inicio/Fim:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="cadastroini" name="cadastroini" readonly="readonly" style="background-color: #eee !important;">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="cadastrofim" name="cadastrofim" style="background-color: #eee !important;">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="cpf">Data de Fechamento Inicio/Fim:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="fechamentoini" name="fechamentoini" style="background-color: #eee !important;">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="fechamentofim" name="fechamentofim" style="background-color: #eee !important;">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="nome">Servidor:</label>
                        <div class="col-sm-4">
                            <select name="servidor" id="servidor" class="form-control">
                                <option value="todos" >Todos</option>
                                <?php 
                                    while ($row = mysqli_fetch_array($query)){
                                        echo '<option value="'.$row['CPF'].'" >'.$row['Nome'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="nome">Área:</label>
                        <div class="col-sm-4">
                            <select name="area" id="area" class="form-control">
                                <option value="todas" >Todas</option>
                                <?php 
                                    while ($row = mysqli_fetch_array($query2)){
                                        echo '<option value="'.$row['DescricaoTipo'].'" >'.$row['DescricaoTipo'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="nome">Status:</label>
                        <div class="col-sm-2">
                            <select name="status" id="status" class="form-control">
                                <option value="todos" >Todos</option>
                                <option value="aberto" >Aberto</option>
                                <option value="fechado" >Fechado</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-info" onclick="" style="width: 120px;">Imprimir &nbsp;&nbsp;<i class="fa fa-print fa-lg"></i></button>
                            <button type="button" class="btn btn-success" onclick="submeter_form();" style="width: 120px;">Excel &nbsp;&nbsp;<i class="fa fa-file-excel-o fa-lg"></i></button>   
                        </div>
                    </div>

                    <input type="hidden" name="operacao" id="operacao" value="" />
                </form>
            </div>
        </div>
    </div>            
</div>


<?php
  // pagemaincontent recebe o conteudo do buffer
  $pagemaincontent = ob_get_contents(); 

  // Descarta o conteudo do Buffer
  ob_end_clean(); 
  
  //Include com o Template
  include("../../master/master.php");