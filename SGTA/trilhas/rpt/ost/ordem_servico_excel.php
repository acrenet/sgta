<?php
    require_once '../../code/funcoes.php';
    require_once '../../plugin/PHPExcel.php';
    require_once '../../plugin/PHPExcel/Writer/Excel2007.php';
    
    if(!isset($_POST['cadastroini'])){
        echo 'OPERAÇÃO INVÁLIDA';
        die();
    }
    
    $cadastroini = $_POST['cadastroini'];
    $cadastrofim = $_POST['cadastrofim'];
    $fechamentoini = $_POST['fechamentoini'];
    $fechamentofim = $_POST['fechamentofim'];
    $status = $_POST['status'];
    $servidor = $_POST['servidor'];
    $area = $_POST['area'];

    $Where = "CodRegistro > 0";

    $log = "";

    if($cadastroini != "" && $cadastrofim != ""){
        $Where = $Where . " And TO_DAYS(DataOrdem) BETWEEN TO_DAYS(DATE('".formata_data_mysql($cadastroini)."')) AND TO_DAYS(DATE('".formata_data_mysql($cadastrofim)."'))";
        $log = "Data de Cadastro entre: $cadastroini e $cadastrofim.   ";
    }elseif ($cadastroini != ""){
        $Where = $Where . " And TO_DAYS(DataOrdem) >= TO_DAYS(DATE('".formata_data_mysql($cadastroini)."'))";
        $log = "Data de Cadastro maior ou igual a $cadastroini.   ";
    }elseif ($cadastrofim != ""){
        $Where = $Where . " And TO_DAYS(DataOrdem) <= TO_DAYS(DATE('".formata_data_mysql($cadastrofim)."'))";
        $log = "Data de Cadastro menor ou igual a $cadastroini.   ";
    }

    if($fechamentoini != "" && $fechamentofim != ""){
        $Where = $Where . " And TO_DAYS(DataFechamento) BETWEEN TO_DAYS(DATE('".formata_data_mysql($fechamentoini)."')) AND TO_DAYS(DATE('".formata_data_mysql($fechamentofim)."'))";
        $log = $log. "Data de Fechamento entre: $fechamentoini e $fechamentofim.   ";
    }elseif ($fechamentoini != ""){
        $Where = $Where . " And TO_DAYS(DataFechamento) >= TO_DAYS(DATE('$".formata_data_mysql($fechamentoini)."'))";
        $log = $log."Data de Fechameno maior ou igual a $fechamentoini.   ";
    }elseif ($fechamentofim != ""){
        $Where = $Where . " And TO_DAYS(DataFechamento) <= TO_DAYS(DATE('".formata_data_mysql($fechamentofim)."'))";
        $log = $log."Data de Fechamento menor ou igual a $fechamentofim.   ";
    }

    if($status != "todos"){
        $Where = $Where . " And StatusOS = '$status'";
        $log = $log."Status: $status.   ";
    }

    if($servidor != "todos"){
        $Where = $Where . " And CPF = '$servidor'";
        $log = $log."Responsável: $servidor.   ";
    }

    if($area != "todas"){
        $Where = $Where . " And DescricaoTipo = '$area'";
        $log = $log."Área: $area.";
    }
            
    if($log == ""){
        $log = "Exibindo todos os registros.   ";
    }

    $sql = "Select * From view_ordem_servico Where $Where Order By DataOrdem, DataFechamento";

    //echo $sql."<br><br>$log";

    require_once '../../obj/conexao.php';
    $con = new conexao();

    $query = $con->select($sql);
    if($con->erro != ""){
        echo $con->erro;
        die();
    }

    $hoje = new DateTime();

    $somaabertos = 0;
    $contabertos = 0;
    $somafechados = 0;
    $contfechados = 0;
    
    if($con->num_rows == 0){
        $tabela = "";
    }else{
        while ($row = mysqli_fetch_array($query)){

            if($row['NumOrdem'] < 10 ){
                $row['NumOrdem'] = "000".$row['NumOrdem'];
            }elseif($row['NumOrdem'] < 100){
                $row['NumOrdem'] = "00".$row['NumOrdem'];
            }elseif($row['NumOrdem'] < 1000){
                $row['NumOrdem'] = "0".$row['NumOrdem'];
            }

            $abertura = new DateTime($row['DataOrdem']);

            if($row['StatusOS'] == "aberto"){
                $intervalo = $abertura->diff($hoje);
                $dias = $intervalo->days;
                $contabertos ++;
                $somaabertos = $somaabertos + $dias;
            }else{
                $fechamento = new DateTime($row['DataFechamento']);
                $intervalo = $abertura->diff($fechamento);
                $dias = $intervalo->days;
                $contfechados++;
                $somafechados = $somafechados + $dias;
            }

            if($row['Resultado'] == ""){
                $row['Resultado'] = " ";
            }

            $tabela[] = array(
                                "CodigoOS" => $row['CodOrdem'],
                                "NumeroOS" => $row['NumOrdem']."-".$row['AnoOrdem'],
                                "ResponsavelCPF" => $row['CPF'],
                                "ResponsavelNome" => $row['Nome'],
                                "Perfil" => $row['NomePerfil'],
                                "DataAbertura" => data_br($row['DataOrdem']),
                                "DataFechamento" => data_br($row['DataFechamento']),
                                "DescricaoOS" => $row['Descricao'],
                                "ResultadoFinal" => $row['Resultado'],
                                "JustificativaDoAuditor" => $row['Justificativa'],
                                "StatusOS" => $row['StatusOS'],
                                "Dias" => $dias
                             );
        }
    }

        
    if($contabertos > 0){
        $mediaabertos = intval($somaabertos/$contabertos);
    }else{
        $mediaabertos = 0;
    }

    if($contfechados > 0){
        $mediafechados = intval($somafechados/$contfechados);
    }else{
        $mediafechados = 0;
    }
    
    // Instanciamos a classe
    $objPHPExcel = new PHPExcel();
    
    // Definimos o estilo da fonte
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
    $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(14);
    $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setSize(14);
    $objPHPExcel->getActiveSheet()->getStyle('A6:K6')->getFont()->setBold(true);
    
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(50);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    
    
    // Criamos as colunas
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Ordens de Serviço de Trilhas' )
                ->setCellValue('A3', 'Critérios:')
                ->setCellValue('A4', $log)
                ->setCellValue('A6', 'Nr. OS' )
                ->setCellValue('B6', "Resp. CPF" )
                ->setCellValue("C6", "Resp. Nome" )
                ->setCellValue("D6", "Perfil" )
                ->setCellValue("E6", "Data Abertura" )
                ->setCellValue("F6", "Data Fech." )
                ->setCellValue("G6", "Descrição" )
                ->setCellValue("H6", "Resultado" )
                ->setCellValue("I6", "Justificativa do Auditor" )
                ->setCellValue("J6", "Status" )
                ->setCellValue("K6", "Dias" );
    $objPHPExcel->getActiveSheet()->setTitle('Ordem De Serviço');
    
    $linha = 7;
    
    if($tabela != ""){
        for ($index = 0; $index < count($tabela); $index++){


            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A". $linha, $tabela[$index]['NumeroOS'])
                        ->setCellValue("B". $linha, $tabela[$index]['ResponsavelCPF'])
                        ->setCellValue("C". $linha, $tabela[$index]['ResponsavelNome'])
                        ->setCellValue("D". $linha, $tabela[$index]['Perfil'])
                        ->setCellValue("E". $linha, $tabela[$index]['DataAbertura'])
                        ->setCellValue("F". $linha, $tabela[$index]['DataFechamento'])
                        ->setCellValue("G". $linha, $tabela[$index]['DescricaoOS'])
                        ->setCellValue("H". $linha, $tabela[$index]['ResultadoFinal'])
                        ->setCellValue("I". $linha, $tabela[$index]['JustificativaDoAuditor'])
                        ->setCellValue("J". $linha, $tabela[$index]['StatusOS'])
                        ->setCellValue("K". $linha, $tabela[$index]['Dias']);

            $linha++;
        }
    }
        
    // Cabeçalho do arquivo para ele baixar
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Ordem de Serviço de Trilhas.xlsx"');
    header('Cache-Control: max-age=0');
    // Se for o IE9, isso talvez seja necessário
    header('Cache-Control: max-age=1');

    // Acessamos o 'Writer' para poder salvar o arquivo
    //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

    // Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
    $objWriter->save('php://output'); 

    exit;