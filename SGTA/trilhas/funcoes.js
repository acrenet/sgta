window.onmessage = function(event) {
   
    window.localStorage.clear();            
    window.localStorage.setItem('token_local', event.data.substring(12,  event.data.length));  
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function logar(cpf,tipo){

    var url = null;
    var cpfSgi = null;
    var dadosCpf = null;
    var codOrgao = null;
   
    if(tipo == 'multiplo'){

        dadosCpf = document.getElementById('dadosUsuario').value.split("|");
        cpf = dadosCpf[0];
        cpfSgi =  dadosCpf[1];
        codOrgao = dadosCpf[2];
        url = '../login/server.php';

    } else {
        url ='app/login/server.php';
    }

    $.ajax({
            type: "post",
            beforeSend: function(request) {
                request.setRequestHeader("Access-Control-Allow-Origin", "*");
                request.setRequestHeader("Access-Control-Allow-Methods", "GET, POST");
                request.setRequestHeader("Access-Control-Allow-Headers", "X-Requested-With");
            },
            url:url, 
            data:{  
                operacao: "login",
                cpf:cpf,
                cpf_sgi : cpfSgi,
                codOrgao : codOrgao,
                sgi_token : window.localStorage.getItem('token_local')
             },
         
            success: function( data2 ) {
             
                resultado =  JSON.parse(data2)  ; 
           
                if( resultado.retorno =='logado') {
                    //redirecionar  para inicio 
                    //andre, mudar variaveis de ambiente para produção
                    //console.log('vai para o inicio');
                   
                    window.location.href="http://"+window.location.host+"/trilhas/app/inicio/inicio.php";
                    return;
                } else {
                    window.location="http://"+window.location.host+"/trilhas/sem_acesso.php";
                }

            }
    })
}


function getCodPerfil(ArrayPerfils){

    var permissoes = _.find(ArrayPerfils,function(permissao){
       return  permissao.authority.match('.*\\SGTA\\b.*');
    });

 
    if(permissoes === undefined){
        window.location="http://"+window.location.host+"/trilhas/sem_permissao.php";
    }
    
    switch (permissoes.authority) {
        case 'SGTA - ADMINISTRADOR':
            return 1;
            break;
        case 'SGTA - GESTOR':
            return 2;
            break;
        case 'SGTA - AUDITOR':
            return 3;
            break;
        case 'SGTA - CONSULTA_CGE':
            return 4;
            break;
        case 'SGTA - CONSULTACGE':
            return 4;
            break;
        case 'SGTA - SUPERVISOR':
            return 5;
        case 'SGTA - ANALISTA':
            return 6;
        case 'SGTA - CONSULTA_ORGAO':
            return 7;
            break;
        case 'SGTA - UPLOAD':
            return 8;
            break;
        default:
            return 0;
        }
}